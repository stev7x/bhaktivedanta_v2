###################
Standarisasi Pengerjaan
###################

1. Buat Role langsung melalui database pada table aauth_role

    - untuk role yang akan dijadikan parent linknya diisi dengan '#'
    - untuk child role linknya : base_usrl()/nama_parent/nama_controller
    - nama_parent = nama folder modul
    - untuk single link (tanpa child) linknya : base_url()/nama_modul
    - nama_modul = nama folder modul, untuk single link nama controllernya disamakan dengan nama folder modul
    - order_layout untuk sorting posisi menu di UI

2. Buat Perm langsung melalui database pada table aauth_perms

    - yang pertama buat perm untuk view (wajib). formatnya : apabila punya parent -> parent/modul_child/controller_view
    - apabila dibutuhkan untuk action lain seperti create, delete, update, maka tambahkan perm dengan akhiran _create atau _delete atau _update atau dll
    - jangan lupa role_idnya diisi dengan role_id yg bersangkutan

3. relasikan perm_to_group pada table aauth_perm_to_group

    - untuk saat ini semua perm yang sudah dibuat direlasikan ke group_id = 1
    - inputnya : perm_id, group_id

4. Buat folder modul sesuai dengan nama parent yang dibuat di link aauth_role tanpa spasi

5. di dalam folder modul buat folder controllers, models, views untuk menyimpan file aplikasi.

    - nama controller diawali huruf besar
    - nama model diawali huruf besar dan diakhir akhiran _model
    - nama view huruf kecil semua
    
6. untuk custom js bagi masing-masing menu, buat file .js di assets/dist/js/pages/folder_modul/file.js

    - nama folder_modul disamakan dengan nama folder modul di application, 
    - nama file.js disamakan dengan nama controller yang dituju tapi menggunakan huruf kecil semua.
    - jadi nanti link url dan link js sama.
    - link url : base_url()/configuration/user_management
    - link js : base_url()/assets/dist/js/pages/configuration/user_management.js