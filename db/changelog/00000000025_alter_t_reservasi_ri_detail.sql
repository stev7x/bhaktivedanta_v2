ALTER TABLE `t_det_reservasi_ri`
	CHANGE COLUMN `rencana_tindakan_id` `tindakan_id` INT(11) NOT NULL;
	
	
ALTER TABLE `t_det_reservasi_ri`
	DROP FOREIGN KEY `fk_det_reservasi_ri`;

ALTER TABLE `t_det_reservasi_ri`
	ADD CONSTRAINT `fk_det_reservasi_ri` FOREIGN KEY (`tindakan_id`) REFERENCES `m_tindakan` (`daftartindakan_id`) ON UPDATE CASCADE ON DELETE CASCADE;

#got error rencana tindakan

ALTER TABLE `t_det_reservasi_ri`
	ADD COLUMN `reservasi_ri_id` INT(11) NOT NULL,
	ADD CONSTRAINT `fk_det_reservasi_reserverasi_ri` FOREIGN KEY (`reservasi_ri_id`) REFERENCES `t_reservasi_ri` (`reservasi_ri_id`) ON UPDATE CASCADE ON DELETE CASCADE;