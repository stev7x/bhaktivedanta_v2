RENAME TABLE `m_det_tindakan_lab` TO `m_nilai_rujukan_tindakan_lab`;


ALTER TABLE `m_tindakan_lab`
	CHANGE COLUMN `harga` `harga_non_bpjs` INT(11) NULL DEFAULT NULL,
	ADD COLUMN `harga_cyto` INT(11) NULL DEFAULT NULL AFTER `harga_bpjs`,
	ADD COLUMN `harga_non_cyto` INT(11) NULL DEFAULT NULL AFTER `harga_cyto`;
