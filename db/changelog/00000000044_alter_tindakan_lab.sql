

ALTER TABLE `t_tindakan_lab`
	ADD COLUMN `bahan_diterima` DATETIME NULL DEFAULT NULL,
	ADD COLUMN `waktu_hasil_pemeriksaan` DATETIME NULL DEFAULT NULL,
	ADD COLUMN `alasan_penundaan_id` INT NULL DEFAULT NULL,
	ADD COLUMN `alasan_penundaan_lainnya` TEXT NULL DEFAULT NULL ;
