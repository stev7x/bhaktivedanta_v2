/* https://gitlab.com/jd-i/puri-bunda-app/issues/62 by kfsl */
CREATE VIEW v_count_ketersediaan_kamar
    AS
(SELECT kelaspelayanan_id, SUM(status_bed) AS tersedia FROM m_kamarruangan WHERE status_aktif = 1
GROUP BY kelaspelayanan_id);

-- INSERT KELAS PELAYANAN YANG BELUM DIBUAT
INSERT INTO `m_kelaspelayanan` (`kelaspelayanan_nama`) VALUES ('HCU');
INSERT INTO `m_kelaspelayanan` (`kelaspelayanan_nama`) VALUES ('Bayi Pathologi');
INSERT INTO `m_kelaspelayanan` (`kelaspelayanan_nama`) VALUES ('Isolasi');

-- INSERT KAMAR DUMMY MENGGUNAKAN KAMAR DHARMAWATI
INSERT INTO `m_kamarruangan` (`kamarruangan_id`,`poliruangan_id`, `kelaspelayanan_id`, `status_bed`, `status_aktif`, `no_bed`, `no_kamar`) VALUES ('7', '22', '5', '1', '1', '1', '1');
INSERT INTO `m_kamarruangan` (`kamarruangan_id`,`poliruangan_id`, `kelaspelayanan_id`, `status_bed`, `status_aktif`, `no_bed`, `no_kamar`) VALUES ('8', '23', '8', '1', '1', '1', '1');
INSERT INTO `m_kamarruangan` (`kamarruangan_id`,`poliruangan_id`, `kelaspelayanan_id`, `status_bed`, `status_aktif`, `no_bed`, `no_kamar`) VALUES ('9', '24', '9', '1', '1', '1', '1');
INSERT INTO `m_kamarruangan` (`kamarruangan_id`,`poliruangan_id`, `kelaspelayanan_id`, `status_bed`, `status_aktif`, `no_bed`, `no_kamar`) VALUES ('10', '25', '10', '0', '1', '1', '1');
INSERT INTO `m_kamarruangan` (`kamarruangan_id`,`poliruangan_id`, `kelaspelayanan_id`, `status_bed`, `status_aktif`, `no_bed`, `no_kamar`) VALUES ('11', '26', '5', '1', '1', '1', '1');
INSERT INTO `m_kamarruangan` (`kamarruangan_id`,`poliruangan_id`, `kelaspelayanan_id`, `status_bed`, `status_aktif`, `no_bed`, `no_kamar`) VALUES ('12', '27', '5', '1', '1', '1', '1');
INSERT INTO `m_kamarruangan` (`kamarruangan_id`,`poliruangan_id`, `kelaspelayanan_id`, `status_bed`, `status_aktif`, `no_bed`, `no_kamar`) VALUES ('13', '28', '8', '1', '1', '1', '1');

/* DETAIL DUMMY DATA
data nomor (kelaspelayanan_id):
1 1
2 2
3 3
5 VIP
8 HCU
9 Bayi Pathologi
10 Isolasi
<<<<<<< HEAD
=======

>>>>>>> master
 */
