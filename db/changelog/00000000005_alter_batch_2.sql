ALTER TABLE `t_reservasi`
	ADD COLUMN `alasan_id` INT(11) NULL DEFAULT NULL AFTER `is_reschedule`,
	ADD COLUMN `catatan` VARCHAR(2500) NULL DEFAULT NULL AFTER `alasan_id`,
	ADD COLUMN `sumber_reservasi` VARCHAR(25) NULL DEFAULT NULL AFTER `catatan`;
	
ALTER TABLE `t_pendaftaran`
	ADD COLUMN `sumber_reservasi` VARCHAR(25) NULL DEFAULT NULL;


ALTER TABLE `t_diagnosapasien`
	ADD COLUMN `dokter_id_2` INT(11) NULL DEFAULT NULL ,
	ADD COLUMN `dokter_id_3` INT(11) NULL DEFAULT NULL;
	
ALTER TABLE `t_tindakanpasien`
	ADD COLUMN `user_id` INT(11) NULL DEFAULT NULL;	
	

ALTER TABLE `t_resepturpasien`
	ADD COLUMN `signa_left` INT(11)  NULL DEFAULT NULL,
	ADD COLUMN `signa_right` INT(11)  NULL DEFAULT NULL,
	ADD COLUMN `sediaan_obat_id` INT(11)  NULL DEFAULT NULL;	