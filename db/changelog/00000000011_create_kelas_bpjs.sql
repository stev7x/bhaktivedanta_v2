CREATE TABLE `m_kelas_bpjs` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(50) NOT NULL,
	`deskripsi` VARCHAR(1200) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;


ALTER TABLE `t_pembayaran`
	ADD COLUMN `kelas_bpjs_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `t_pembayaran`
	ADD CONSTRAINT `t_pembayaran_kelas_bpjs_id_fk` FOREIGN KEY (`kelas_bpjs_id`) REFERENCES `m_kelas_bpjs` (`id`);
	