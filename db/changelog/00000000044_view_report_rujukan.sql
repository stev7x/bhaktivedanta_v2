create
or replace
view `v_report_rujukan` as (
select
    `t_pendaftaran`.`tgl_pendaftaran` as `tgl_pendaftaran`,
    `m_pasien`.`pasien_nama` as `pasien_nama`,
    `m_perujuk`.`nama_perujuk` as `nama_perujuk`,
    `m_perujuk`.`alamat_kantor` as `alamat_perujuk`,
    `m_perujuk`.`telp_hp` as `telp_hp`,
    `t_pendaftaran`.`catatan_rujukan` as `catatan_rujukan`
from
    (( `t_pendaftaran`
left join `m_pasien` on
    (( `t_pendaftaran`.`pasien_id` = `m_pasien`.`pasien_id` )))
left join `m_perujuk` on
    (( `t_pendaftaran`.`id_perujuk` = `m_perujuk`.`id_perujuk` )))
where
    ( `t_pendaftaran`.`is_rujuk_pulang` = 'true' ));


INSERT INTO `aauth_role` (`role_id`, `role_name`, `url`, `parent`, `order_layout`, `icon`) VALUES ('106', 'Rujukan', 'laporan/rujukan', '47', '6', NULL);
INSERT INTO `aauth_perms` (`id`, `name`, `definition`, `role_id`) VALUES ('276', 'rujukan_view', 'Report Rujukan View', '106');
INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('276', '1');