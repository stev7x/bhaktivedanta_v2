CREATE TABLE `m_det_racikan_barang_farmasi` (
	`det_racikan_barang_farmasi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`barang_farmasi_id` INT(11)  NOT NULL COMMENT 'where obat',
	`jumlah` INT(11)  NOT NULL,
	`sediaan_obat_id` INT(11)  NOT NULL,
	`keterangan` VARCHAR(500) NULL DEFAULT NULL,
	INDEX `fk_racikan_barang_farmasi_id` (`det_racikan_barang_farmasi_id`),
	CONSTRAINT `fk_det_racikan_barang_farmasi_barang_farmasi` FOREIGN KEY (`barang_farmasi_id`) REFERENCES `m_barang_farmasi` (`id_barang`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_det_racikan_barang_farmasi_sediaan_obat` FOREIGN KEY (`sediaan_obat_id`) REFERENCES `m_sediaan_obat` (`id_sediaan`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
