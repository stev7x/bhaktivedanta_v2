ALTER TABLE `t_barang_keluar_farmasi`
	ADD COLUMN `instalasi_id` INT NULL DEFAULT NULL AFTER `keterangan`,
	ADD CONSTRAINT `barang_masuk_location_FK1` FOREIGN KEY (`instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`);
	
	
ALTER TABLE `t_barang_masuk_farmasi`
	ADD COLUMN `instalasi_id` INT NULL DEFAULT NULL AFTER `keterangan`,
	ADD CONSTRAINT `barang_keluar_location_FK1` FOREIGN KEY (`instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`);	
	
	
	
ALTER TABLE `t_stok_farmasi`
	ADD COLUMN `instalasi_id` INT NULL DEFAULT NULL,
	ADD CONSTRAINT `stok_farmasi_location_FK1` FOREIGN KEY (`instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`);		