CREATE TABLE `t_riwayat_transfer` (
	`riwayat_transfer_id` INT(11) NOT NULL AUTO_INCREMENT,
	`pendaftaran_id` INT(11) NOT NULL,
	`instalasi_id` INT(11) NOT NULL,
	`instalasi_tujuan_id` INT(11) NOT NULL,
	`tanggal` DATETIME NOT NULL,
	`catatan` TEXT NULL DEFAULT NULL,
	PRIMARY KEY (`riwayat_transfer_id`),
	INDEX `pendaftaran_riwayat_transfer_FK1` (`pendaftaran_id`),
	CONSTRAINT `instalasi_awal_riwayat_transferFK1` FOREIGN KEY (`instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `instalasi_tujuan_riwayat_transferFK1` FOREIGN KEY (`instalasi_tujuan_id`) REFERENCES `m_instalasi` (`instalasi_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `pendaftaran_riwayat_transfer_FK4` FOREIGN KEY (`pendaftaran_id`) REFERENCES `t_pendaftaran` (`pendaftaran_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
