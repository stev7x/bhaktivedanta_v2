CREATE TABLE `t_tindakan_radiologi_kesimpulan` (
	`tindakan_radiologi_kesimpulan_id` INT(11) NOT NULL,
	`pendaftaran_id` INT(11) NOT NULL,
	`kesimpulan` TEXT NOT NULL,
	PRIMARY KEY (`tindakan_radiologi_kesimpulan_id`),
	INDEX `pendaftaran_id` (`pendaftaran_id`),
	CONSTRAINT `pendaftaran_kesimpulan_tindakan_radiologi` FOREIGN KEY (`pendaftaran_id`) REFERENCES `t_pendaftaran` (`pendaftaran_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


ALTER TABLE `t_tindakan_radiologi_kesimpulan`
	CHANGE COLUMN `tindakan_radiologi_kesimpulan_id` `tindakan_radiologi_kesimpulan_id` INT(11) NOT NULL AUTO_INCREMENT FIRST;