ALTER TABLE `t_pendaftaran`
	ADD COLUMN `kamarruangan_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `t_pendaftaran`
	ADD CONSTRAINT `t_pendaftaran_kamar_ruangan_id_fk` FOREIGN KEY (`kamarruangan_id`) REFERENCES `m_kamarruangan` (`kamarruangan_id`);
	
