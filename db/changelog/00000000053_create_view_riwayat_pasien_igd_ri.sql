CREATE VIEW `v_riwayat_pasien_igd` AS
SELECT
`t_pendaftaran`.`tgl_pendaftaran` AS `tgl_pendaftaran`,
`t_pendaftaran`.`no_pendaftaran` AS `no_pendaftaran`,
`t_pendaftaran`.`pendaftaran_id` AS `pendaftaran_id`,
`t_pendaftaran`.`pasien_id` AS `pasien_id`,
`m_pasien`.`no_rekam_medis` AS `no_rekam_medis`,
`m_pasien`.`pasien_nama` AS `pasien_nama`,
`m_pasien`.`pasien_alamat` AS `pasien_alamat`,
`m_poli_ruangan`.`nama_poliruangan` AS `nama_poliruangan`,
`t_pendaftaran`.`jam_periksa` AS `jam_periksa`,
`m_dokter`.`NAME_DOKTER` AS `NAME_DOKTER`
FROM
t_pendaftaran
LEFT JOIN t_tindakanpasien ON (t_tindakanpasien.pendaftaran_id = t_pendaftaran.pendaftaran_id)
LEFT JOIN m_tindakan ON (m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id)
LEFT JOIN m_poli_ruangan ON (m_poli_ruangan.poliruangan_id = t_pendaftaran.poliruangan_id)
LEFT JOIN m_pasien ON (m_pasien.pasien_id = t_pendaftaran.pasien_id)
LEFT JOIN t_diagnosapasien ON (t_diagnosapasien.pendaftaran_id = t_pendaftaran.pendaftaran_id)
LEFT JOIN m_dokter ON (m_dokter.id_M_DOKTER = t_pendaftaran.dokter_id)
WHERE 
`t_pendaftaran`.`status_pasien` = 0 and `t_pendaftaran`.`instalasi_id` = 2
group by `t_pendaftaran`.`tgl_pendaftaran` 
order by `t_pendaftaran`.`tgl_pendaftaran`;

CREATE VIEW `v_riwayat_pasien_ri` AS
SELECT
`t_pendaftaran`.`tgl_pendaftaran` AS `tgl_pendaftaran`,
`t_pendaftaran`.`no_pendaftaran` AS `no_pendaftaran`,
`t_pendaftaran`.`pendaftaran_id` AS `pendaftaran_id`,
`t_pendaftaran`.`pasien_id` AS `pasien_id`,
`m_pasien`.`no_rekam_medis` AS `no_rekam_medis`,
`m_pasien`.`pasien_nama` AS `pasien_nama`,
`m_pasien`.`pasien_alamat` AS `pasien_alamat`,
`m_poli_ruangan`.`nama_poliruangan` AS `nama_poliruangan`,
`t_pendaftaran`.`jam_periksa` AS `jam_periksa`,
`m_dokter`.`NAME_DOKTER` AS `NAME_DOKTER`
FROM
t_pendaftaran
LEFT JOIN t_tindakanpasien ON (t_tindakanpasien.pendaftaran_id = t_pendaftaran.pendaftaran_id)
LEFT JOIN m_tindakan ON (m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id)
LEFT JOIN m_poli_ruangan ON (m_poli_ruangan.poliruangan_id = t_pendaftaran.poliruangan_id)
LEFT JOIN m_pasien ON (m_pasien.pasien_id = t_pendaftaran.pasien_id)
LEFT JOIN t_diagnosapasien ON (t_diagnosapasien.pendaftaran_id = t_pendaftaran.pendaftaran_id)
LEFT JOIN m_dokter ON (m_dokter.id_M_DOKTER = t_pendaftaran.dokter_id)
WHERE 
`t_pendaftaran`.`status_pasien` = 0 and `t_pendaftaran`.`instalasi_id` = 3
group by `t_pendaftaran`.`tgl_pendaftaran` 
order by `t_pendaftaran`.`tgl_pendaftaran`;

