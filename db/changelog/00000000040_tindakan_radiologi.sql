ALTER TABLE `t_tindakanradiologi`
	ADD COLUMN `hasil_pemeriksaan_foto` VARCHAR(250) NULL DEFAULT NULL COMMENT 'path folder hasil upload foto' AFTER `type_pembayaran`,
	ADD COLUMN `hasil_pemeriksaan` TEXT NULL DEFAULT NULL COMMENT 'detail pemeriksaan' AFTER `hasil_pemeriksaan_foto`,
	ADD COLUMN `detail_kesimpulan` TEXT NULL DEFAULT NULL COMMENT 'hasil pemeriksaan' ;