ALTER TABLE `t_pendaftaran`
	ADD COLUMN `aktual_instalasi_id` INT(11) NULL DEFAULT NULL COMMENT 'instalasi id terakhir' AFTER `id_perujuk`,
	ADD CONSTRAINT `t_pendaftaran_aktual_instalasi_id_FK` FOREIGN KEY (`aktual_instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`);