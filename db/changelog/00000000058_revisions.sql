CREATE TABLE `t_resepturpasien_detail_racikan` (
	`resepturpasien_detail_racikan_id` INT(11) NOT NULL AUTO_INCREMENT,
	`barang_farmasi_id` INT(11) NOT NULL COMMENT 'where obat',
	`jumlah` INT(11) NOT NULL,
	`sediaan_obat_id` INT(11) NOT NULL,
	`keterangan` VARCHAR(500) NULL DEFAULT NULL,
	`reseptur_id` INT(11) NOT NULL COMMENT 'reseptur id, jika ada racikan',
	`jumlah_obat` INT(11) NOT NULL,
	INDEX `fk_racikan_barang_farmasi_id` (`resepturpasien_detail_racikan_id`),
	INDEX `fk_resepturpasien_detail_racikan_barang_farmasi` (`barang_farmasi_id`),
	INDEX `fk_resepturpasien_detail_racikan_sediaan_obat` (`sediaan_obat_id`),
	INDEX `fk_resepturpasien_detail_racikan_reseptur` (`reseptur_id`),
	CONSTRAINT `fk_resepturpasien_detail_racikan_barang_farmasi` FOREIGN KEY (`barang_farmasi_id`) REFERENCES `m_barang_farmasi` (`id_barang`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_resepturpasien_detail_racikan_reseptur` FOREIGN KEY (`reseptur_id`) REFERENCES `t_resepturpasien` (`reseptur_id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_resepturpasien_detail_racikan_sediaan_obat` FOREIGN KEY (`sediaan_obat_id`) REFERENCES `m_sediaan_obat` (`id_sediaan`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


INSERT INTO `m_instalasi` (`instalasi_id`, `instalasi_nama`) VALUES ('10', 'Fisiologi Anak');	