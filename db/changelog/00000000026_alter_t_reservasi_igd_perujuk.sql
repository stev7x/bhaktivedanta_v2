ALTER TABLE `t_reservasi_igd`
	ADD COLUMN `perujuk_id` INT NULL DEFAULT NULL AFTER `catatan`,
	ADD CONSTRAINT `reservasi_id_perujuk_fk_1` FOREIGN KEY (`perujuk_id`) REFERENCES `m_perujuk` (`id_perujuk`) ON UPDATE NO ACTION ON DELETE NO ACTION;
	
ALTER TABLE `m_perujuk`
	CHANGE COLUMN `telp_hp` `telp_hp` VARCHAR(50) NULL DEFAULT NULL AFTER `alamat_kantor`;	