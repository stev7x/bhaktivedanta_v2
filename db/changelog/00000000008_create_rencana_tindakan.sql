CREATE TABLE `m_rencana_tindakan` (
	`rencana_tindakan_id` INT(11) NOT NULL AUTO_INCREMENT,
	`rencana_tindakan_nama` VARCHAR(200) NOT NULL,
	`kelompoktindakan_id` INT(11) NOT NULL,
	PRIMARY KEY (`rencana_tindakan_id`),
	INDEX `RENCANA_TINDAKAN_ID` (`kelompoktindakan_id`),
	CONSTRAINT `m_rencana_tindakan_kelompoktindakan_ibfk_1` FOREIGN KEY (`kelompoktindakan_id`) REFERENCES `m_kelompoktindakan` (`kelompoktindakan_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
