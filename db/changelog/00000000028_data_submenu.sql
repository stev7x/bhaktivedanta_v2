INSERT INTO aauth_role VALUES (null, 'Umum', '#', 6, 1, NULL);
INSERT INTO aauth_role VALUES (null, 'Kepegawaian', '#', 6, 2, NULL);
INSERT INTO aauth_role VALUES (null, 'Obat-obatan', '#', 6, 3, NULL);

/* UPDATE SPESIFICATION FOR UMUM */
UPDATE aauth_role SET parent=98, order_layout=1 WHERE role_id=18;
UPDATE aauth_role SET parent=98, order_layout=2 WHERE role_id=87;
UPDATE aauth_role SET parent=98, order_layout=3 WHERE role_id=22;
UPDATE aauth_role SET parent=98, order_layout=4 WHERE role_id=8;
UPDATE aauth_role SET parent=98, order_layout=5 WHERE role_id=23;
UPDATE aauth_role SET parent=98, order_layout=6 WHERE role_id=24;
UPDATE aauth_role SET parent=98, order_layout=7 WHERE role_id=91;

/* UPDATE SPESIFICATION FOR KEPEGAWAIAN */
UPDATE aauth_role SET parent=99, order_layout=1 WHERE role_id=10;
UPDATE aauth_role SET parent=99, order_layout=2 WHERE role_id=40;
UPDATE aauth_role SET parent=99, order_layout=3 WHERE role_id=96;
UPDATE aauth_role SET parent=99, order_layout=4 WHERE role_id=62;
UPDATE aauth_role SET parent=99, order_layout=5 WHERE role_id=25;
UPDATE aauth_role SET parent=99, order_layout=6 WHERE role_id=28;
UPDATE aauth_role SET parent=99, order_layout=7 WHERE role_id=66;
UPDATE aauth_role SET parent=99, order_layout=8 WHERE role_id=95;
UPDATE aauth_role SET parent=99, order_layout=9 WHERE role_id=27;
UPDATE aauth_role SET parent=99, order_layout=10 WHERE role_id=9;
UPDATE aauth_role SET parent=99, order_layout=11 WHERE role_id=21;
UPDATE aauth_role SET parent=99, order_layout=12 WHERE role_id=63;

/* UPDATE SPESIFICATION FOR Obat Obatan */
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=73;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=81;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=92;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=54;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=19;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=75;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=67;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=71;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=26;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=74;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=69;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=55;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=68;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=72;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=70;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=7;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=76;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=80;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=65;
UPDATE aauth_role SET parent=100, order_layout=1 WHERE role_id=94;


INSERT INTO aauth_perms VALUES(null, 'master_data_umum_view', '> Umum', 98);
INSERT INTO aauth_perms VALUES(null, 'master_data_kepegawaian_view', '> Kepegawaian', 99);
INSERT INTO aauth_perms VALUES(null, 'master_data_obat_obatan_view', '> Obat-Obatan', 100);


INSERT INTO aauth_perm_to_group VALUES(259, 1);
INSERT INTO aauth_perm_to_group VALUES(260, 1);
INSERT INTO aauth_perm_to_group VALUES(261, 1);