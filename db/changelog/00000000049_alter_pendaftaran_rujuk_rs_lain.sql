ALTER TABLE `t_pendaftaran`
	ADD COLUMN `alasan_rujuk_id` INT(11) NULL DEFAULT NULL COMMENT 'm alasan rujuk' AFTER `aktual_instalasi_id`,
	ADD COLUMN `catatan_assestmen_rujukan` VARCHAR(5000) NULL DEFAULT NULL COMMENT 'hal yg harus diperhatikan' AFTER `alasan_rujuk_id`;
	
ALTER TABLE `t_pendaftaran`
	ADD CONSTRAINT `t_pendaftaran_alasan_rujuk_rs_lain_id_fk` FOREIGN KEY (`alasan_rujuk_id`) REFERENCES `m_alasan_rujuk` (`alasan_rujuk_id`);	