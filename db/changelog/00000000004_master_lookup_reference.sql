CREATE TABLE `m_lookup` (
	`lookup_id` INT(11) NOT NULL AUTO_INCREMENT,
	`code` VARCHAR(25) NULL,
	`type` VARCHAR(50) NOT NULL,
	`name` VARCHAR(100) NOT NULL,		
	`description` VARCHAR(2500) NOT NULL,
	PRIMARY KEY (`lookup_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
