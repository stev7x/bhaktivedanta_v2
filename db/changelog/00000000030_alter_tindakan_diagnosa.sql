
ALTER TABLE `t_tindakanpasien`
	ADD COLUMN `instalasi_id` INT(11) NULL DEFAULT NULL AFTER `user_id`,
	ADD CONSTRAINT `tindakan_instalasi_FK1` FOREIGN KEY (`instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
	
ALTER TABLE `t_diagnosapasien`
	ADD COLUMN `instalasi_id` INT(11) NULL DEFAULT NULL,
	ADD CONSTRAINT `diagnosa_instalasi_FK1` FOREIGN KEY (`instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;	
		
# background
ALTER TABLE `t_pendaftaran`
	ADD COLUMN `catatan_keluhan` TEXT NULL DEFAULT NULL AFTER `catatan_rujukan`;	

ALTER TABLE `t_alergi_pasien`
	ADD COLUMN `instalasi_id` INT(11) NULL DEFAULT NULL,
	ADD CONSTRAINT `alergi_instalasi_FK1` FOREIGN KEY (`instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;		

