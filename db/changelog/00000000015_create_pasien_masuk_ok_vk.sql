CREATE TABLE `t_pasien_masuk_ok` (
	`pasien_masuk_ok_id` INT(11) NOT NULL AUTO_INCREMENT,
	`kelaspelayanan_id` INT(11) NOT NULL,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
	`no_masukpenunjang` VARCHAR(30) NOT NULL,
	`tgl_masukpenunjang` DATETIME NOT NULL,
	`no_urutperiksa` VARCHAR(5) NULL DEFAULT NULL,
	`statusperiksa` VARCHAR(30) NOT NULL,
	`poli_ruangan_asal` INT(11) NULL DEFAULT NULL,
	`poli_ruangan_id` INT(11) NOT NULL,
	`dokterpengirim_id` INT(11) NULL DEFAULT NULL,
	`create_time` DATETIME NOT NULL,
	`create_by` INT(11) NOT NULL,
	`status_keberadaan` VARCHAR(20) NULL DEFAULT NULL,
	`catatan` VARCHAR(10000) NULL DEFAULT NULL,
	`tanggal_rencana_tindakan` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`pasien_masuk_ok_id`),
	INDEX `kelaspelayanan_id` (`kelaspelayanan_id`),
	INDEX `pendaftaran_id` (`pendaftaran_id`),
	INDEX `pasien_id` (`pasien_id`),
	INDEX `poli_ruangan_id` (`poli_ruangan_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;


CREATE TABLE `t_pasien_masuk_vk` (
	`pasien_masuk_vk_id` INT(11) NOT NULL AUTO_INCREMENT,
	`kelaspelayanan_id` INT(11) NOT NULL,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
	`no_masukpenunjang` VARCHAR(30) NOT NULL,
	`tgl_masukpenunjang` DATETIME NOT NULL,
	`no_urutperiksa` VARCHAR(5) NULL DEFAULT NULL,
	`statusperiksa` VARCHAR(30) NOT NULL,
	`poli_ruangan_asal` INT(11) NULL DEFAULT NULL,
	`poli_ruangan_id` INT(11) NOT NULL,
	`dokterpengirim_id` INT(11) NULL DEFAULT NULL,
	`create_time` DATETIME NOT NULL,
	`create_by` INT(11) NOT NULL,
	`status_keberadaan` VARCHAR(20) NULL DEFAULT NULL,
	`catatan` VARCHAR(10000) NULL DEFAULT NULL,
	`tanggal_rencana_tindakan` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`pasien_masuk_vk_id`),
	INDEX `kelaspelayanan_id` (`kelaspelayanan_id`),
	INDEX `pendaftaran_id` (`pendaftaran_id`),
	INDEX `pasien_id` (`pasien_id`),
	INDEX `poli_ruangan_id` (`poli_ruangan_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
