ALTER TABLE `t_pembayaran`
	ADD COLUMN `jumlah_claim` INT(11) NULL DEFAULT NULL AFTER `kelas_bpjs_id`;
	
ALTER TABLE `t_pembayarankasir`
	CHANGE COLUMN `no_asuransi_1` `no_asuransi_1` VARCHAR(50) NULL DEFAULT NULL AFTER `nama_asuransi_1`,
	CHANGE COLUMN `no_asuransi_2` `no_asuransi_2` VARCHAR(50) NULL DEFAULT NULL AFTER `nama_asuransi_2`,
	ADD COLUMN `diskon_persen` DECIMAL(2,2) NULL DEFAULT NULL AFTER `status`,
	ADD COLUMN `diskon_total` INT(11) NULL DEFAULT NULL AFTER `diskon_persen`;	