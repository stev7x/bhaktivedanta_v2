CREATE TABLE `m_alasan_rujuk` (
	`alasan_rujuk_id` INT(11) NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(250) NULL DEFAULT NULL,
	`code` VARCHAR(5) NULL DEFAULT NULL,
	`keterangan` TEXT NULL DEFAULT NULL,
	PRIMARY KEY (`alasan_rujuk_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


CREATE TABLE `m_alasan_penundaan` (
	`alasan_penundaan_id` INT(11) NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(250) NULL DEFAULT NULL,
	`code` VARCHAR(5) NULL DEFAULT NULL,
	`keterangan` TEXT NULL DEFAULT NULL,
	PRIMARY KEY (`alasan_penundaan_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
