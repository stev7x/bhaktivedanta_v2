INSERT INTO `aauth_role` (`role_id`, `role_name`, `url`, `parent`, `order_layout`, `icon`) VALUES (98, 'Komponen Tarif', 'masterdata/komponen_tarif', '6', '100', NULL);

INSERT INTO `aauth_perms` (`id`, `name`, `definition`, `role_id`) VALUES (NULL, 'master_data_komponen_tarif_view', 'View Masterdata Komponen Tarif', '103');

INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('270', '1');

INSERT INTO `aauth_perms` (`id`, `name`, `definition`, `role_id`) VALUES (NULL, 'master_data_komponen_tarif_create', 'Create Masterdata Komponen Tarif', '103');

INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('271', '1');

INSERT INTO `aauth_perms` (`id`, `name`, `definition`, `role_id`) VALUES (NULL, 'master_data_komponen_tarif_update', 'Update Masterdata Komponen Tarif', '103'), (NULL, 'master_data_komponen_tarif_delete', 'Delete Masterdata Komponen Tarif', '103');

INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('272', '1'), ('273', '1');
