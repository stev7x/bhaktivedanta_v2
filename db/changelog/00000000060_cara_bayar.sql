CREATE TABLE `m_cara_bayar` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
   `type_pembayaran` VARCHAR(50) NOT NULL,
	`kode` VARCHAR(25) NOT NULL,
	`nama` VARCHAR(100) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

ALTER TABLE `m_cara_bayar`
	CHANGE COLUMN `type_pembayaran` `type_pembayaran` VARCHAR(50) NOT NULL COMMENT 'Pribadi/Asuransi/BPJS' AFTER `id`;