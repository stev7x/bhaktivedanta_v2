CREATE TABLE `m_tindakan_komponen_tarif` (
	`tindakan_komponen_tarif_id` INT(11) NOT NULL AUTO_INCREMENT,
	`daftar_tindakan_id` INT(11) NOT NULL,
	`komponentarif_id` INT(11) NOT NULL,
	`harga_bpjs_cyto` INT(11) NULL DEFAULT NULL,
	`harga_bpjs_non_cyto` INT(11) NULL DEFAULT NULL,
	`harga_umum_cyto` INT(11) NULL DEFAULT NULL,
	`harga_umum_non_cyto` INT(11) NULL DEFAULT NULL,
   PRIMARY KEY  (`tindakan_komponen_tarif_id`),
	INDEX `tindakan_komponen_tarif_id_idx1` (`tindakan_komponen_tarif_id`),
	INDEX `tindakan_komponen_tarif_daftar_tindakan_id_idx2` (`daftar_tindakan_id`),
	INDEX `tindakan_komponen_tarif_komponentarif_id_idx3` (`komponentarif_id`),
	CONSTRAINT `tindakan_komponen_tarif_komponen_tarif_fk1` FOREIGN KEY (`komponentarif_id`) REFERENCES `m_komponentarif` (`komponentarif_id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `tindakan_komponen_tarif_tindakan_fk1` FOREIGN KEY (`daftar_tindakan_id`) REFERENCES `m_tindakan` (`daftartindakan_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


ALTER TABLE `m_det_tindakan_bhp`
	ADD PRIMARY KEY (`det_tindakan_bhp_id`);


ALTER TABLE `m_tindakan`
	ADD COLUMN `total_harga_bpjs_cyto` INT(11) NULL DEFAULT NULL AFTER `icd9_cm_id`,
	ADD COLUMN `total_harga_bpjs_non_cyto` INT(11) NULL DEFAULT NULL AFTER `total_harga_bpjs_cyto`,
	ADD COLUMN `total_harga_umum_cyto` INT(11) NULL DEFAULT NULL AFTER `total_harga_bpjs_non_cyto`,
	ADD COLUMN `total_harga_umum_non_cyto` INT(11) NULL DEFAULT NULL AFTER `total_harga_umum_cyto`;