create
or replace
view `v_report_igd` as (
select
    date_format( `t_reservasi_igd`.`tanggal_reservasi`, '%Y %M' ) as `priode_bulan`,
    `t_reservasi_igd`.`status` as `status`,
    count( `t_reservasi_igd`.`reservasi_igd_id` ) as `total`
from
    `t_reservasi_igd`
group by
    `t_reservasi_igd`.`status`,
    date_format( `t_reservasi_igd`.`tanggal_reservasi`, '%Y%M' ));

INSERT INTO `aauth_perms` (`id`, `name`, `definition`, `role_id`) VALUES (NULL, 'laporan_igd_view', 'Report IGD View', '107');
INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('285', '1');