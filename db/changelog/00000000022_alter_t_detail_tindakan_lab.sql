ALTER TABLE `t_det_tindakan_lab`
	CHANGE COLUMN `jenis_tindakan` `jenis_tindakan` TINYINT(2) NULL DEFAULT NULL COMMENT 'metode tindakan' AFTER `keterangan`,
	ADD COLUMN `nilai_rujukan` VARCHAR(100) NULL DEFAULT NULL AFTER `type_pembayaran`,
	ADD COLUMN `satuan_rujukan` VARCHAR(50) NULL DEFAULT NULL AFTER `nilai_rujukan`;



ALTER TABLE `t_det_tindakan_lab`
	ADD COLUMN `bahan_diterima` DATETIME NULL DEFAULT NULL AFTER `satuan_rujukan`,
	ADD COLUMN `hasil_diterima` DATETIME NULL DEFAULT NULL AFTER `bahan_diterima`,
	ADD COLUMN `alasan_penundaan_id` INT NULL DEFAULT NULL AFTER `hasil_diterima`,
	ADD COLUMN `alasan_penundaan_lainnya` TEXT NULL DEFAULT NULL AFTER `alasan_penundaan_id`;
