CREATE TABLE `t_reservasi_ri` (
	`reservasi_ri_id` INT(11) NOT NULL AUTO_INCREMENT,
	`kode_booking` VARCHAR(50) NULL DEFAULT NULL,
	`no_urut` INT(11) NULL DEFAULT NULL,
	`no_rekam_medis` VARCHAR(50) NULL DEFAULT NULL,
	`nama_pasien` VARCHAR(100) NULL DEFAULT NULL,
	`alamat_pasien` TEXT NULL DEFAULT NULL,
	`jenis_kelamin` VARCHAR(25) NULL DEFAULT NULL,
	`tanggal_lahir` DATE NULL DEFAULT NULL,
	`asuransi1` VARCHAR(50) NULL DEFAULT NULL,
	`asuransi2` VARCHAR(50) NULL DEFAULT NULL,
	`kelas_pelayanan_id` INT(11) NULL DEFAULT NULL,
	`dokter_id` INT(11) NULL DEFAULT NULL,
	`jam` VARCHAR(50) NULL DEFAULT NULL,
	`tanggal_reservasi` DATE NULL DEFAULT NULL,
	`metode_registrasi` VARCHAR(50) NULL DEFAULT NULL,
	`keterangan` TEXT NULL DEFAULT NULL,
	`status` VARCHAR(25) NULL DEFAULT NULL,
	`status_verif` TINYINT(2) NULL DEFAULT NULL,
	`tgl_verif` DATETIME NULL DEFAULT NULL,
	`tgl_created` DATETIME NULL DEFAULT NULL,
	`no_pendaftaran` VARCHAR(50) NULL DEFAULT NULL,
	`status_pasien` VARCHAR(50) NULL DEFAULT NULL,
	`no_telp` VARCHAR(20) NULL DEFAULT NULL,
	`is_reschedule` TINYINT(1) NULL DEFAULT NULL,
	`alasan_id` INT(11) NULL DEFAULT NULL,
	`catatan` VARCHAR(2500) NULL DEFAULT NULL,
	`poliruangan_id` INT(11) NULL DEFAULT NULL,
	`kelaspelayanan_id` INT(11) NULL DEFAULT NULL,
	`kamarruangan_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`reservasi_ri_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;


ALTER TABLE `t_reservasi_ri`
	ADD CONSTRAINT `t_reservasi_ri_poliruangan_id_fk` FOREIGN KEY (`poliruangan_id`) REFERENCES `m_poli_ruangan` (`poliruangan_id`),
		ADD CONSTRAINT `t_reservasi_ri_kelaspelayanan_id_fk` FOREIGN KEY (`kelaspelayanan_id`) REFERENCES `m_kelaspelayanan` (`kelaspelayanan_id`),
			ADD CONSTRAINT `t_reservasi_ri_kamarruangan_id_fk` FOREIGN KEY (`kamarruangan_id`) REFERENCES `m_kamarruangan` (`kamarruangan_id`);	

