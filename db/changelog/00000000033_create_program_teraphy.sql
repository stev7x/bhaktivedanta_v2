CREATE TABLE `m_program_therapy` (
	`program_therapy_id` INT(11) NOT NULL AUTO_INCREMENT,
	`kode` VARCHAR(50) NULL DEFAULT NULL,
	`nama` TEXT NULL DEFAULT NULL,
	PRIMARY KEY (`program_therapy_id`),
	INDEX `program_therapy_id` (`program_therapy_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
