-- aauth_role
INSERT INTO `aauth_role` (`role_id`, `role_name`, `url`, `parent`, `order_layout`) VALUES ('97', 'Reservasi IGD', 'rekam_medis/reservasi_igd', '36', '2');

-- aauth_perms
INSERT INTO `aauth_perms` (`id`, `name`, `role_id`) VALUES ('255', 'rekam_medis_reservasi_igd_view', '97');
INSERT INTO `aauth_perms` ( `name`, `role_id`) VALUES ( 'rekam_medis_reservasi_igd_create', '97');
INSERT INTO `aauth_perms` ( `name`, `role_id`) VALUES ( 'rekam_medis_reservasi_igd_update', '97');
INSERT INTO `aauth_perms` (`name`, `role_id`) VALUES ('rekam_medis_reservasi_igd_delete', '97');

--

INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('255', '1');
INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('256', '1');
INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('257', '1');
INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES ('258', '1');