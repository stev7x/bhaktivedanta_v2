CREATE TABLE `m_det_tindakan_lab` (
	`det_tindakan_lab_id` INT(11) NOT NULL AUTO_INCREMENT,
	`tindakan_lab_id` INT(11) NOT NULL,
	`jenis_kelamin` VARCHAR(50) NULL DEFAULT NULL,
	`kategori_usia` VARCHAR(50) NULL DEFAULT NULL,
	`jenis_cairan` VARCHAR(50) NULL DEFAULT NULL,
    `nilai_rujukan_bawah` FLOAT(25) NOT NULL,
    `nilai_rujukan_atas` FLOAT(25) NOT NULL,
    `satuan` VARCHAR(25) NULL DEFAULT NULL,
    	PRIMARY KEY (`det_tindakan_lab_id`),
	CONSTRAINT `det_tindakan_lab_tindakan_lab_id_fk` FOREIGN KEY (`tindakan_lab_id`) REFERENCES `m_tindakan_lab` (`tindakan_lab_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
