CREATE TABLE `m_alergi` (
	`alergi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`kode_alergi` VARCHAR(50) NULL DEFAULT NULL,
	`nama_alergi` TEXT NULL DEFAULT NULL,
	PRIMARY KEY (`alergi_id`),
	INDEX `alergi_id` (`alergi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


CREATE TABLE `t_alergi_pasien` (
	`alergi_pasien_id` INT(11) NOT NULL AUTO_INCREMENT,
	`alergi_id` INT(11) NOT NULL,
	`tanggal` DATE NOT NULL,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
	PRIMARY KEY (`alergi_pasien_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


ALTER TABLE `t_alergi_pasien`
	ADD CONSTRAINT `alergi_alergi_pasien_FK1` FOREIGN KEY (`alergi_id`) REFERENCES `m_alergi` (`alergi_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	ADD CONSTRAINT `pendaftaran_alergi_pasien_FK2` FOREIGN KEY (`pendaftaran_id`) REFERENCES `t_pendaftaran` (`pendaftaran_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	ADD CONSTRAINT `pasien_alergi_pasien_FK3` FOREIGN KEY (`pasien_id`) REFERENCES `m_pasien` (`pasien_id`);