CREATE TABLE `m_kuisioner` (
	`kuisioner_id` INT(11) NOT NULL AUTO_INCREMENT,	
	`pertanyaan` VARCHAR(2500) NULL DEFAULT NULL,
	`kode_pertanyaan` VARCHAR(25) NULL DEFAULT NULL,	
	PRIMARY KEY (`kuisioner_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `m_kuisioner_jawaban` (
	`kuisioner_jawaban_id` INT(11) NOT NULL AUTO_INCREMENT,
	`kuisioner_id` INT(11) NOT NULL,
	`jawaban` VARCHAR(2500) NULL DEFAULT NULL,
	PRIMARY KEY (`kuisioner_jawaban_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `t_ok_surgerical_safety_checklist` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
	`kuisioner_id` INT(11) NULL DEFAULT NULL,
	`jawaban` VARCHAR(2500) NULL DEFAULT NULL,
	`jawaban_lain` VARCHAR(2500) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


CREATE TABLE `t_ok_anestesi_pre_medikasi` (
	`anestesi_pre_medikasi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`anestesi_id` INT(11) NOT NULL,
	`nama_obat` VARCHAR(500) NULL DEFAULT NULL,
	`dosis_pemberian` VARCHAR(50) NULL DEFAULT NULL,
	`cara_pemberian` VARCHAR(150) NULL DEFAULT NULL,
	`waktu_pemberian` VARCHAR(150) NULL DEFAULT NULL,
	`reaksi` VARCHAR(500) NULL DEFAULT NULL,
	PRIMARY KEY (`anestesi_pre_medikasi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `t_ok_anestesi_pra_induksi` (
	`anestesi_pra_induksi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`anestesi_id` INT(11) NOT NULL,	
	`ku` VARCHAR(50) NULL DEFAULT NULL,
	`tensi` VARCHAR(50) NULL DEFAULT NULL,
	`urine` VARCHAR(50) NULL DEFAULT NULL,
	`kesadaran` VARCHAR(50) NULL DEFAULT NULL,
	`nadi` VARCHAR(50) NULL DEFAULT NULL,
	`lain_lain` VARCHAR(50) NULL DEFAULT NULL,
	`pernafasan` VARCHAR(50) NULL DEFAULT NULL,
	`perfusi` VARCHAR(50) NULL DEFAULT NULL,
	`suhu` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`anestesi_pra_induksi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `t_ok_intra_anestesi_induksi` (
	`intra_anestesi_induksi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`anestesi_id` INT(11) NOT NULL,
	`induksi` VARCHAR(150) NULL DEFAULT NULL,
	PRIMARY KEY (`intra_anestesi_induksi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `t_ok_anestesi_pasca` (
	`detail_observasi_id` INT(11) NOT NULL AUTO_INCREMENT,
			`anestesi_id` INT(11) NOT NULL,
	`masuk_jam` DATETIME NULL DEFAULT NULL,
	`keadaan_umum_kesadaran` VARCHAR(50) NULL DEFAULT NULL,
  `keadaan_umum_reflek` VARCHAR(50) NULL DEFAULT NULL,
  `keadaan_umum_akral` VARCHAR(50) NULL DEFAULT NULL,
  `keadaan_umum_lain_lain` VARCHAR(150) NULL DEFAULT NULL,
  `pernafasan` VARCHAR(150) NULL DEFAULT NULL,
  `tekanan_darah` DECIMAL(5,2) NULL DEFAULT NULL,
	`nadi` DECIMAL(5,2) NULL DEFAULT NULL,
	`suhu_badan` DECIMAL(5,2) NULL DEFAULT NULL,
	`spo` DECIMAL(5,2) NULL DEFAULT NULL,
  `rr` DECIMAL(5,2) NULL DEFAULT NULL,
  `waktu` DATETIME NULL DEFAULT NULL,
  `infus` VARCHAR(150) NULL DEFAULT NULL,
  `urine` VARCHAR(150) NULL DEFAULT NULL,
  `muntah` VARCHAR(150) NULL DEFAULT NULL,
  `skala_nyeri` VARCHAR(150) NULL DEFAULT NULL,
  `bromage_score` VARCHAR(150) NULL DEFAULT NULL,
  `aldrete_score` VARCHAR(150) NULL DEFAULT NULL,
  `skala_nyeri_pain_assestment_tool_score` VARCHAR(50) NULL DEFAULT NULL,
  `skala_nyeri_pain_assestment_tool_kategori` VARCHAR(50) NULL DEFAULT NULL,
  `aldrete_score_kesadaran` VARCHAR(50) NULL DEFAULT NULL,
  `aldrete_score_pernafasan` VARCHAR(50) NULL DEFAULT NULL,
  `aldrete_score_tensi` VARCHAR(50) NULL DEFAULT NULL,
  `aldrete_score_pergerakan` VARCHAR(50) NULL DEFAULT NULL,
  `aldrete_score_warna_kulit` VARCHAR(50) NULL DEFAULT NULL,
  `total_nilai_aldrete_score` VARCHAR(150) NULL DEFAULT NULL,
  `total_nilai_bromage_score` VARCHAR(150) NULL DEFAULT NULL,
  `total_nilai_skala_nyeri` VARCHAR(150) NULL DEFAULT NULL,
  `rekomendasi` VARCHAR(150) NULL DEFAULT NULL,
  `kekuar_jam` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`detail_observasi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
