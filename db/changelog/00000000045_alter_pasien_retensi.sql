ALTER TABLE `m_pasien`
	ADD COLUMN `is_retention` TINYINT NOT NULL DEFAULT '0' COMMENT '0  belum di retensi; 1 sudah diretensi' AFTER `berat_bayi`,
	ADD COLUMN `no_rekam_medis_lama` TINYINT NOT NULL DEFAULT '0' AFTER `is_retention`;