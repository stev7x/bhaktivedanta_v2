-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: db_puribunda
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aauth_role`
--

DROP TABLE IF EXISTS `aauth_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aauth_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order_layout` int(11) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aauth_role`
--

LOCK TABLES `aauth_role` WRITE;
/*!40000 ALTER TABLE `aauth_role` DISABLE KEYS */;
INSERT INTO `aauth_role` VALUES (1,'Home','dashboard',0,1,' ti-home p-r-10'),(2,'Configuration','#',0,14,'icon-settings p-r-10'),(3,'User Management','configuration/user_management',2,89,'mdi-action-account-circle'),(4,'Rekam Medis','#',0,3,'ti-calendar p-r-10'),(5,'Pendaftaran Pasien','rekam_medis/pendaftaran',36,17,'mdi-action-account-circle'),(6,'Master Data','#',0,13,'fa fa-tachometer p-r-10'),(7,'Paket Operasi','masterdata/paket_operasi',100,69,'mdi-action-account-circle'),(8,'Kamar','masterdata/kamar',98,53,'mdi-action-account-circle'),(9,'Poli/Ruangan','masterdata/poli_ruangan',99,66,'mdi-action-account-circle'),(10,'Dokter','masterdata/dokter',99,57,'mdi-action-account-circle'),(12,'Rawat Jalan','#',0,5,'fa fa-wheelchair p-r-10'),(13,'Rawat Inap','#',0,7,'fa fa-hospital-o p-r-10'),(14,'Rawat Darurat','#',0,6,'fa fa-ambulance p-r-10'),(15,'Pasien Rawat Jalan','rawatjalan/pasien_rj',12,26,NULL),(16,'Pasien Rawat Inap','rawatinap/pasien_ri',13,29,NULL),(17,'Pasien Rawat Darurat','rawatdarurat/pasien_rd',14,28,NULL),(18,'Agama','masterdata/agama',98,50,NULL),(19,'Daftar Tindakan','masterdata/daftar_tindakan',100,70,NULL),(21,'Provinsi','masterdata/provinsi',99,67,NULL),(22,'Kabupaten','masterdata/kabupaten',98,52,NULL),(23,'Kecamatan','masterdata/kecamatan',98,54,NULL),(24,'Kelurahan','masterdata/kelurahan',98,55,NULL),(25,'Kelas Pelayanan','masterdata/kelas_pelayanan',99,61,NULL),(26,'Instalasi','masterdata/instalasi',100,71,NULL),(27,'Pendidikan','masterdata/pendidikan',99,65,NULL),(28,'Kelas Poli/Ruangan','masterdata/kelas_poli_ruangan',99,62,NULL),(29,'Kasir','#',0,9,' ti-money p-r-10'),(30,'Rawat Jalan','kasir/kasirrj',29,35,NULL),(31,'Rawat Inap','kasir/kasirri',29,34,NULL),(32,'Rawat Darurat','kasir/kasirrd',29,33,NULL),(33,'Penunjang','#',0,8,'fa fa-stethoscope p-r-10'),(34,'Radiologi','penunjang/radiologi',33,32,NULL),(35,'Laboratorium','penunjang/laboratorium',33,31,NULL),(36,'Pendaftaran','#',0,2,'icon-people p-r-10'),(37,'Pend. Paket Operasi','rekam_medis/pend_operasi',36,18,NULL),(38,'All Pasien','rekam_medis/list_pasien',4,19,NULL),(39,'Info Rawat Inap','rekam_medis/info_pasienri',4,20,NULL),(40,'Dokter Ruangan','masterdata/dokter_ruangan',99,58,NULL),(42,'Pasien Rawat Jalan','rekam_medis/pasienrj',4,25,NULL),(43,'Pasien IGD','rekam_medis/pasienrd',4,21,NULL),(44,'Pasien Rawat Inap','rekam_medis/pasienri',4,24,NULL),(45,'Pasien Operasi','rekam_medis/pasien_operasi',4,22,NULL),(46,'Pasien Penunjang','rekam_medis/pasien_penunjang',4,23,NULL),(47,'Laporan','#',0,12,'icon-chart p-r-10'),(48,'Rekam Medis','laporan/laporan_rm',47,43,NULL),(49,'Pasien Operasi','rawatinap/pasien_operasi',13,30,NULL),(50,'Radiologi','laporan/laporanrad',47,44,NULL),(51,'Laboratorium','laporan/laporanlab',47,45,NULL),(52,'Keuangan','laporan/keuangan',47,46,NULL),(53,'Group Management','configuration/group_management',2,90,NULL),(54,'Daftar Diagnosa ICD-10','masterdata/daftar_diagnosa',100,72,NULL),(55,'Kelompok Diagnosa','masterdata/kelompok_diagnosa',100,73,NULL),(57,'Inventori','#',0,11,'fa fa-cubes p-r-10'),(58,'Barang','inventori/barang',57,42,NULL),(62,'Karyawan','masterdata/karyawan',99,60,NULL),(63,'Unit Kerja','masterdata/unit_kerja',99,68,NULL),(65,'Tindakan Lab','masterdata/tindakan_lab',100,74,NULL),(66,'Kelas Terapi','masterdata/kelas_terapi',99,63,NULL),(67,'Golongan Obat','masterdata/golongan_obat',100,75,NULL),(68,'Kelompok Obat','masterdata/kelompok_obat',100,76,NULL),(69,'Kategori Obat','masterdata/kategori_obat',100,77,NULL),(70,'Lasa','masterdata/lasa',100,78,NULL),(71,'Indikasi','masterdata/indikasi_obat',100,79,NULL),(72,'Kontra Indikasi','masterdata/kontra_indikasi_obat',100,80,NULL),(73,'Auto Stop','masterdata/auto_stop_obat',100,81,NULL),(74,'Interaksi','masterdata/interaksi_obat',100,82,NULL),(75,'Efek Samping','masterdata/efek_samping_obat',100,83,NULL),(76,'Sediaan','masterdata/sediaan_obat',100,84,NULL),(77,'Farmasi','#',0,10,'fa fa-plus p-r-10'),(78,'Barang Masuk','farmasi/barang_masuk',77,36,NULL),(79,'Barang Keluar','farmasi/barang_keluar',77,37,NULL),(80,'Supplier','masterdata/supplier',100,85,NULL),(81,'Barang Farmasi','masterdata/daftar_barang_farmasi',100,86,NULL),(82,'Barang Stok','farmasi/barang_stok',77,38,NULL),(83,'Floor Stok','farmasi/floor_stok',77,39,NULL),(87,'Bahasa','masterdata/bahasa',98,51,NULL),(88,'Stok Barang','apotek/stok_barang_apotek',84,3,NULL),(89,'Barang Floor Stock Masuk','farmasi/barang_floor_stok_masuk',77,40,NULL),(90,'Barang Floor Stock Keluar','farmasi/barang_floor_stok_keluar',77,41,NULL),(91,'Sekolah','masterdata/sekolah',98,56,NULL),(92,'Daftar Diagnosa','masterdata/list_diagnosa',100,87,NULL),(93,'Reservasi','rekam_medis/reservasi',36,16,NULL),(94,'Vaksinasi','masterdata/vaksinasi',100,88,NULL),(95,'Lab','masterdata/lab',99,64,NULL),(96,'Jadwal Dokter','masterdata/jadwal_dokter',99,59,NULL),(97,'Reservasi IGD','rawatdarurat/reservasi_igd',14,27,NULL),(98,'Umum','#',6,47,NULL),(99,'Kepegawaian','#',6,48,NULL),(100,'Obat-Obatan','#',6,49,NULL),(104,'High Care Unit','hcu',0,91,'fa fa-user-md p-r-10'),(105,'Daftar Pasien Retensi','daftar_pasien_retensi',0,4,'fa fa-book p-r-10');
/*!40000 ALTER TABLE `aauth_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-12  7:53:56
