ALTER TABLE `t_stok_farmasi`
	ADD COLUMN `barang_id` INT(11) NULL DEFAULT NULL AFTER `is_bpjs`,
	ADD CONSTRAINT `stok_barang_barang_farmasi_id_FK1` FOREIGN KEY (`barang_id`) REFERENCES `m_barang_farmasi` (`id_barang`);
	


UPDATE t_stok_farmasi
INNER JOIN m_barang_farmasi ON t_stok_farmasi.kode_barang = m_barang_farmasi.kode_barang
SET t_stok_farmasi.barang_id = m_barang_farmasi.id_barang;	