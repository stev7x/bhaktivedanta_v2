DROP TABLE IF EXISTS `t_det_reservasi_ri`;

DROP TABLE IF EXISTS `t_det_pendaftaran_rencana_tindakan`;


CREATE TABLE `t_det_reservasi_ri` (
	`det_reservasi_ri_id` INT(11) NOT NULL AUTO_INCREMENT,
	`tindakan_id` INT(11)  NOT NULL,
			PRIMARY KEY (`det_reservasi_ri_id`),
	CONSTRAINT `fk_det_reservasi_tindakan_ri` FOREIGN KEY (`tindakan_id`) REFERENCES `m_tindakan` (`daftartindakan_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `t_det_pendaftaran_rencana_tindakan` (
	`det_pendaftaran_tindakan_id` INT(11) NOT NULL AUTO_INCREMENT,
	`tindakan_id` INT(11)  NOT NULL,
				PRIMARY KEY (`det_pendaftaran_tindakan_id`),
	CONSTRAINT `fkdet_pendaftaran_rencana_tindakan_ri` FOREIGN KEY (`tindakan_id`) REFERENCES `m_tindakan` (`daftartindakan_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


ALTER TABLE `t_reservasi_ri`
	DROP COLUMN `kelaspelayanan_id`,
	DROP FOREIGN KEY `t_reservasi_ri_kelaspelayanan_id_fk`;