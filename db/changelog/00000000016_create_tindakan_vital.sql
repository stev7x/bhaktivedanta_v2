CREATE TABLE `t_det_pendaftaran_tanda_vital` (
	`det_pendaftaran_tanda_vital_id` INT(11) NOT NULL AUTO_INCREMENT,
	`tekanan_darah` INT(11) NOT NULL,
	`nadi` INT(11) NOT NULL,
	`respitory` INT(11) NOT NULL,
	`suhu` INT(11) NOT NULL,
	PRIMARY KEY (`det_pendaftaran_tanda_vital_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
