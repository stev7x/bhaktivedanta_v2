CREATE TABLE `t_assestment_therapy_pasien` (
	`assestment_therapy_id` INT(11) NOT NULL AUTO_INCREMENT,
	`program_therapy_id` INT(11) NOT NULL,
	`assestment_pasien_id` INT(11) NOT NULL,
	PRIMARY KEY (`assestment_therapy_id`),
	CONSTRAINT `assestment_program_therapy_id_FK3` FOREIGN KEY (`program_therapy_id`) REFERENCES `m_program_therapy` (`program_therapy_id`)  ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `therapy_assestment_pasien_id_FK2` FOREIGN KEY (`assestment_pasien_id`) REFERENCES `t_assestment_pasien` (`assestment_pasien_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
