CREATE TABLE `t_assestment_pasien` (
	`assestment_pasien_id` INT(11) NOT NULL AUTO_INCREMENT,
	`instalasi_id` INT(11) NOT NULL,
	`pendaftaran_id` INT(11) NULL DEFAULT NULL,
	`pasien_id` INT(11) NOT NULL,
	`tensi` DECIMAL(5,2) NULL DEFAULT NULL,	
	`nadi_1` DECIMAL(5,2) NULL DEFAULT NULL,	
	`suhu` DECIMAL(5,2) NULL DEFAULT NULL,	
	`nadi_2` DECIMAL(5,2) NULL DEFAULT NULL,	
   `penggunaan` DECIMAL(5,2) NULL DEFAULT NULL,	
   `saturasi` DECIMAL(5,2) NULL DEFAULT NULL,
   `nyeri` VARCHAR(250) NULL DEFAULT NULL,	
   `numeric_wong_baker` VARCHAR(250) NULL DEFAULT NULL,	
   `resiko_jatuh` VARCHAR(250) NULL DEFAULT NULL,	
	PRIMARY KEY (`assestment_pasien_id`),
	INDEX `pendaftaran_assestment_pasien_FK2` (`pendaftaran_id`),
	INDEX `pasien_assestment_pasien_FK3` (`pasien_id`),
	CONSTRAINT `instalasi_assestment_pasien_FK1` FOREIGN KEY (`instalasi_id`) REFERENCES `m_instalasi` (`instalasi_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `pasien_assestment_pasien_FK2` FOREIGN KEY (`pasien_id`) REFERENCES `m_pasien` (`pasien_id`),
	CONSTRAINT `pendaftaran_assestment_pasien_FK3` FOREIGN KEY (`pendaftaran_id`) REFERENCES `t_pendaftaran` (`pendaftaran_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
