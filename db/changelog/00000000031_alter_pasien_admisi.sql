ALTER TABLE `t_pasienadmisi`
	ADD COLUMN `instalasi_asal_id` INT(11) NULL DEFAULT NULL AFTER `dokterperawat_id`,
	ADD COLUMN `instalasi_tujuan_id` INT(11) NULL DEFAULT NULL AFTER `instalasi_asal_id`,
	ADD COLUMN `catatan` TEXT NULL DEFAULT NULL AFTER `instalasi_tujuan_id`,
	ADD CONSTRAINT `admisi_intalasi_asal_fk1` FOREIGN KEY (`instalasi_asal_id`) REFERENCES `m_instalasi` (`instalasi_id`),
	ADD CONSTRAINT `admisi_instalasi_tujuan_fk2` FOREIGN KEY (`instalasi_tujuan_id`) REFERENCES `m_instalasi` (`instalasi_id`);
