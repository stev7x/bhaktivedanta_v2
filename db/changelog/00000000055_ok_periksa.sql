CREATE TABLE `t_ok_tindakan_info` (
	`tindakan_info_id` INT(11) NOT NULL AUTO_INCREMENT,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
	`dokter_operator_1` INT(11) NULL DEFAULT NULL,
	`dokter_operator_2` INT(11) NULL DEFAULT NULL,
	`dokter_operator_3` INT(11) NULL DEFAULT NULL,
	`dokter_anestesi` INT(11) NULL DEFAULT NULL,
	`assisten_anestesi` INT(11) NULL DEFAULT NULL,
	`assisten_bedah` INT(11) NULL DEFAULT NULL,
	`jenis_assisten_bedah` VARCHAR(25) NULL DEFAULT NULL,
	`instrumen` INT(11) NULL DEFAULT NULL,
	`jenis_instrumen` VARCHAR(25) NULL DEFAULT NULL,
	`dokter_anak` INT(11) NULL DEFAULT NULL,
	`bidan` INT(11) NULL DEFAULT NULL,
	`assisten_onlop` INT(11) NULL DEFAULT NULL,
	`jenis_assisten_onloop` VARCHAR(25) NULL DEFAULT NULL,
	`waktu_awal_periksa` DATETIME NULL DEFAULT NULL,
	`waktu_akhir_periksa` DATETIME NULL DEFAULT NULL,
	`alasan_penundaan_id` INT(11) NULL DEFAULT NULL,
	`catatan_penundaan` TEXT NULL DEFAULT NULL ,
	PRIMARY KEY (`tindakan_info_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `t_ok_instruksi_pasca_anestesi` (
	`instruksi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
	`monitor_kesadaran_setiap` VARCHAR(500) NULL DEFAULT NULL,
	`monitor_kesadaran_selama` VARCHAR(500) NULL DEFAULT NULL,
	`posisi` VARCHAR(2500) NULL DEFAULT NULL,
   `infus_cairan_transfusi` TEXT NULL DEFAULT NULL,
   `antibiotik_dan_obat_lain` TEXT NULL DEFAULT NULL,
    `bila_mual_muntah` TEXT NULL DEFAULT NULL,
	`bila_kesakitan` TEXT NULL DEFAULT NULL ,
	`minum_makan` TEXT NULL DEFAULT NULL ,
	`urine` TEXT NULL DEFAULT NULL ,
	`lain_lain` TEXT NULL DEFAULT NULL ,
	PRIMARY KEY (`instruksi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


CREATE TABLE `t_ok_laporan` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
   `infus_cairan_transfusi` TEXT NULL DEFAULT NULL,
   `diaganosa_pre_operasi` TEXT NULL DEFAULT NULL,
    `diagnosa_post_operas` TEXT NULL DEFAULT NULL,
	`jaringan_ang_dieksisi` TEXT NULL DEFAULT NULL ,
	`nama_macam_operasi` TEXT NULL DEFAULT NULL ,
	`isi_laporan_operasi` TEXT NULL DEFAULT NULL ,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


CREATE TABLE `t_ok_observasi` (
	`observasi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
   `pemantauan` TEXT NULL DEFAULT NULL,
	PRIMARY KEY (`observasi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


CREATE TABLE `t_ok_detail_observasi` (
	`detail_observasi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`waktu` DATETIME NULL DEFAULT NULL,
	`tensi` DECIMAL(5,2) NULL DEFAULT NULL,
	`nadi` DECIMAL(5,2) NULL DEFAULT NULL,
	`nafas` DECIMAL(5,2) NULL DEFAULT NULL,
	`suhu` DECIMAL(5,2) NULL DEFAULT NULL,
	`rencana_tindakan` TEXT NULL DEFAULT NULL,
   `implementasi` TEXT NULL DEFAULT NULL,
   `evaluasi` TEXT NULL DEFAULT NULL,
	PRIMARY KEY (`detail_observasi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `t_ok_anestesi` (
	`anestesi_id` INT(11) NOT NULL AUTO_INCREMENT,
	`pendaftaran_id` INT(11) NOT NULL,
	`pasien_id` INT(11) NOT NULL,
	`diagnosa_pra_bedah` VARCHAR(1000) NULL DEFAULT NULL,
	`jenis_pembedahan` VARCHAR(1000) NULL DEFAULT NULL,
	`lama_operasi` INT(11) NULL DEFAULT NULL,
	`diagnosa_pasca_bedah` VARCHAR(1000) NULL DEFAULT NULL,
	`jenis_anestesi` VARCHAR(1000) NULL DEFAULT NULL,
	`lama_anestesi` INT(11) NULL DEFAULT NULL,
	`keadaan_pra_bedah` VARCHAR(1000) NULL DEFAULT NULL,
	`tinggi_badan` INT(11) NULL DEFAULT NULL,
	`berat_badan` DECIMAL(5,2) NULL DEFAULT NULL,
	`golongan_darah` VARCHAR(2) NULL DEFAULT NULL,
	`terakhir_makan` VARCHAR(30) NULL DEFAULT NULL,
	`minum` VARCHAR(30) NULL DEFAULT NULL,
	`tensi` DECIMAL(5,2) NULL DEFAULT NULL,
	`nadi` DECIMAL(5,2) NULL DEFAULT NULL,
	`nafas` DECIMAL(5,2) NULL DEFAULT NULL,
	`suhu` DECIMAL(5,2) NULL DEFAULT NULL,
   	`anestesi_dengan` VARCHAR(1000) NULL DEFAULT NULL,
	`relaksasi_dengan` VARCHAR(1000) NULL DEFAULT NULL,
	`teknis_anestesi` VARCHAR(1000) NULL DEFAULT NULL,
	`teknik_khusus` VARCHAR(1000) NULL DEFAULT NULL,
	`pernafasan` VARCHAR(1000) NULL DEFAULT NULL,
	`posisi` VARCHAR(1000) NULL DEFAULT NULL,
	`infus` VARCHAR(1000) NULL DEFAULT NULL,
	`penyulit_selama_pembedahan` VARCHAR(1000) NULL DEFAULT NULL,
	`keadaan_akhir_pembedahan` VARCHAR(1000) NULL DEFAULT NULL,
	`pernafasan_jalan_nafas` VARCHAR(1000) NULL DEFAULT NULL,
	`airway_panen_obtruksi` VARCHAR(50) NULL DEFAULT NULL,
	`gigi` VARCHAR(50) NULL DEFAULT NULL,
	`buka_mulut` DECIMAL(5,2) NULL DEFAULT NULL,
	`malapati` VARCHAR(50) NULL DEFAULT NULL,
	`gerak_leher` VARCHAR(50) NULL DEFAULT NULL,
	`jarak_methothyroid` DECIMAL(5,2) NULL DEFAULT NULL,
	`suara_nafas_versikuler` VARCHAR(50) NULL DEFAULT NULL,
	`suara_nafas_ronchi_1` VARCHAR(50) NULL DEFAULT NULL,
	`suara_nafas_ronchi_2` VARCHAR(50) NULL DEFAULT NULL,
	`suara_nafas_wheezing_1` VARCHAR(50) NULL DEFAULT NULL,
	`suara_nafas_wheezing_2` VARCHAR(50) NULL DEFAULT NULL,
	`riwayat_asma` VARCHAR(1000) NULL DEFAULT NULL,
	`riwayat_alergi` VARCHAR(1000) NULL DEFAULT NULL,
	`cardiologi_ekg` VARCHAR(1000) NULL DEFAULT NULL,
	`xray_thorax` VARCHAR(1000) NULL DEFAULT NULL,
	`pulmonologi` VARCHAR(1000) NULL DEFAULT NULL,
	`echo` VARCHAR(1000) NULL DEFAULT NULL,
	`sirkulasi` VARCHAR(1000) NULL DEFAULT NULL,
	`cyaosis` VARCHAR(1000) NULL DEFAULT NULL,
	`perfusi` VARCHAR(1000) NULL DEFAULT NULL,
	`crt` VARCHAR(1000) NULL DEFAULT NULL,
	`saraf_gsc` VARCHAR(1000) NULL DEFAULT NULL,
	`saraf_aypu` VARCHAR(1000) NULL DEFAULT NULL,
	`lateralisasi` VARCHAR(1000) NULL DEFAULT NULL,
	`gastrointestinal` TEXT NULL DEFAULT NULL,
	`ginjal` TEXT NULL DEFAULT NULL,
	`metabolik` TEXT NULL DEFAULT NULL,
	`hati` TEXT NULL DEFAULT NULL,
	`medikasi_pra_bedah` TEXT NULL DEFAULT NULL,
	`masalah_anestesi` TEXT NULL DEFAULT NULL,
	`masalah_bedah` TEXT NULL DEFAULT NULL,
	`waktu_lahir_bayi` DATETIME NULL DEFAULT NULL,
	`tinggi_badan_bayi` INT(11) NULL DEFAULT NULL,
	`berat_badan_bayi` DECIMAL(5,2) NULL DEFAULT NULL,
	`jenis_kelamin_bayi` VARCHAR(25) NULL DEFAULT NULL,
	`as_1_menit` VARCHAR(50) NULL DEFAULT NULL,
	`as_5_menit` VARCHAR(50) NULL DEFAULT NULL,
	`tindakan_khusus_pasca_bedah` VARCHAR(1000) NULL DEFAULT NULL,
	`penyulit_pasca_bedah` VARCHAR(1000) NULL DEFAULT NULL,
	`hipersevitas` VARCHAR(1000) NULL DEFAULT NULL,
	`kematian` VARCHAR(1000) NULL DEFAULT NULL,
	`kesimpulan_sirkulasi` VARCHAR(1000) NULL DEFAULT NULL,
	`kesimpulan_paru` VARCHAR(1000) NULL DEFAULT NULL,
	`kesimpulan_hati` VARCHAR(1000) NULL DEFAULT NULL,
	`kesimpulan_ginjal` VARCHAR(1000) NULL DEFAULT NULL,
	`kesimpulan_ssp` VARCHAR(1000) NULL DEFAULT NULL,
	`kesimpulan_darah` VARCHAR(1000) NULL DEFAULT NULL,
	`kesimpulan_nutrisi` VARCHAR(1000) NULL DEFAULT NULL,
	`kesimpulan_lain_lain` VARCHAR(1000) NULL DEFAULT NULL,
	`rencana` TEXT NULL DEFAULT NULL,
	`anestesi_lainnya` TEXT NULL DEFAULT NULL,
	`respirasi` VARCHAR(500) NULL DEFAULT NULL,
	`stadium_ops` VARCHAR(500) NULL DEFAULT NULL,
	`jumlah_cairan` VARCHAR(1000) NULL DEFAULT NULL,
	`cairan_keluar` VARCHAR(1000) NULL DEFAULT NULL,
	`catatan` VARCHAR(1000) NULL DEFAULT NULL,
	`pendarahan` VARCHAR(1000) NULL DEFAULT NULL,
	PRIMARY KEY (`anestesi_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;