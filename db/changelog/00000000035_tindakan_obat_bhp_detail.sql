CREATE TABLE `t_tindakan_pasien_obat_detail` (
	`tindakan_pasien_obat_detail_id` INT(11) NULL DEFAULT NULL,
	`tindakan_pasien_id` INT(11) NULL DEFAULT NULL,
	`barang_farmasi_id` INT(11) NULL DEFAULT NULL,
	`jumlah` INT(11) NULL DEFAULT NULL,
	`sediaan_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`tindakan_pasien_obat_detail_id`),
	INDEX `tindakan_pasien_obat_detail_id_idx` (`tindakan_pasien_obat_detail_id`),
	INDEX `tindakan_pasien_obat_tindakan_pasien_id_idx` (`tindakan_pasien_id`),
	CONSTRAINT `tindakan_pasien_obat_fk_1` FOREIGN KEY (`tindakan_pasien_id`) REFERENCES `t_tindakanpasien` (`tindakanpasien_id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `barang_farmasi_pasien_obat_fk_2` FOREIGN KEY (`barang_farmasi_id`) REFERENCES `m_barang_farmasi` (`id_barang`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `sediaan_pasien_obat_fk_3` FOREIGN KEY (`sediaan_id`) REFERENCES `m_sediaan_obat` (`id_sediaan`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


CREATE TABLE `t_tindakan_pasien_bhp_detail` (
	`tindakan_pasien_bhp_detail_id` INT(11) NULL DEFAULT NULL,
	`tindakan_pasien_id` INT(11) NULL DEFAULT NULL,
	`barang_farmasi_id` INT(11) NULL DEFAULT NULL,
	`jumlah` INT(11) NULL DEFAULT NULL,
	`sediaan_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`tindakan_pasien_bhp_detail_id`),
	INDEX `tindakan_pasien_bhp_detail_id_idx` (`tindakan_pasien_bhp_detail_id`),
	INDEX `tindakan_pasien_bhp_tindakan_pasien_id_idx` (`tindakan_pasien_id`),
	CONSTRAINT `tindakan_pasien_bhp_fk_1` FOREIGN KEY (`tindakan_pasien_id`) REFERENCES `t_tindakanpasien` (`tindakanpasien_id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `barang_farmasi_pasien_bhp_fk_2` FOREIGN KEY (`barang_farmasi_id`) REFERENCES `m_barang_farmasi` (`id_barang`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `sediaan_pasien_bhp_fk_3` FOREIGN KEY (`sediaan_id`) REFERENCES `m_sediaan_obat` (`id_sediaan`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
