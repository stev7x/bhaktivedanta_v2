INSERT INTO aauth_role VALUES (null, 'Alasan Rujuk', 'masterdata/alasan_rujuk', 6, 2, NULL);
INSERT INTO aauth_role VALUES (null, 'Alasan Penundaan', 'masterdata/alasan_penolakan', 6, 3, NULL);
#Alasan Rujuk
INSERT INTO aauth_perms VALUES(null, 'master_data_alasan_rujuk_view', 'View Alasan Rujuk', 100);
INSERT INTO aauth_perms VALUES(null, 'master_data_alasan_rujuk_create', 'Alasan Rujuk Create', 100);
INSERT INTO aauth_perms VALUES(null, 'master_data_alasan_rujuk_update', 'Alasan Rujuk Update', 100);
INSERT INTO aauth_perms VALUES(null, 'master_data_alasan_rujuk_delete', 'Alasan Rujuk Delete', 100);

INSERT INTO aauth_perm_to_group VALUES(264, 1);
INSERT INTO aauth_perm_to_group VALUES(265, 1);
INSERT INTO aauth_perm_to_group VALUES(266, 1);
INSERT INTO aauth_perm_to_group VALUES(267, 1);


#Alasan Penolakan
INSERT INTO aauth_perms VALUES(null, 'master_data_alasan_penolakan_view', 'Alasan Penolakan View', 101);
INSERT INTO aauth_perms VALUES(null, 'master_data_alasan_penolakan_create', 'Alasan Penolakan Create', 101);
INSERT INTO aauth_perms VALUES(null, 'master_data_alasan_penolakan_update', 'Alasan Penolakan Update', 101);
INSERT INTO aauth_perms VALUES(null, 'master_data_alasan_penolakan_delete', 'Alasan Penolakan Delete', 101);

INSERT INTO aauth_perm_to_group VALUES(268, 1);
INSERT INTO aauth_perm_to_group VALUES(269, 1);
INSERT INTO aauth_perm_to_group VALUES(270, 1);
INSERT INTO aauth_perm_to_group VALUES(271, 1);

#Laporan BHP
INSERT INTO aauth_role VALUES (null, 'Laporan BHP', 'laporan/laporan_bhp', 47, 5, NULL);

INSERT INTO aauth_perms VALUES(null, 'laporan_bhp_view', 'Laporan BHP View', 105);

INSERT INTO aauth_perm_to_group VALUES(275, 1);