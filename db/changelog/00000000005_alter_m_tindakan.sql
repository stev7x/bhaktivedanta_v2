ALTER TABLE `m_tindakan`
	ADD COLUMN `icd9_cm_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `m_tindakan`
	ADD CONSTRAINT `m_tindakan_icd9_cm_id_fk` FOREIGN KEY (`icd9_cm_id`) REFERENCES `m_icd_9_cm` (`diagnosa_id`);