ALTER TABLE `t_pendaftaran`
	ADD COLUMN `is_rujuk_pulang` TINYINT NULL DEFAULT NULL AFTER `catatan_keluhan`,
	ADD COLUMN `id_perujuk` INT(11) NULL DEFAULT NULL COMMENT 'rujukan saat dipulangkan' AFTER `is_rujuk_pulang`,
	ADD CONSTRAINT `id_perujuk_pendaftaran_fk2` FOREIGN KEY (`id_perujuk`) REFERENCES `m_perujuk` (`id_perujuk`);