ALTER TABLE `m_det_tindakan_bhp`
	CHANGE COLUMN `jumlah` `jumlah` INT(11) NULL AFTER `barang_farmasi_id`,
	CHANGE COLUMN `sediaan_obat_id` `sediaan_obat_id` INT(11) NULL AFTER `jumlah`,
	ADD COLUMN `daftar_tindakan_id` INT(11) NOT NULL AFTER `sediaan_obat_id`;



ALTER TABLE `m_det_tindakan_bhp`
	ADD CONSTRAINT `fk_det_tindakan_bhp_m_tindakan` FOREIGN KEY (`daftar_tindakan_id`) REFERENCES `m_tindakan` (`daftartindakan_id`) ON UPDATE CASCADE ON DELETE CASCADE;