<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Print Chasmic</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <style>
            #table_header{
                width: 100%;
                text-align: center;
            }
            .table_utama{
                width: 100%;
                /*overflow-x: auto;*/
                border-collapse: collapse;
                /*border-spacing: 0;*/
                
            }

            img{
                /*width: 90px;*/
                width: 100%;
                height: 37;
            }
            p{
                line-height: 5%;
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        <table id="table_header" border="1px"> 
            <tr>
                <td rowspan="6" width="25%"><img src="<?= base_url() ?>/assets/plugins/images/logo-puribunda.png" alt="puri_bunda"></td>
                <td rowspan="6" width="50%"> 
                   <p>CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p>RUMAH SAKIT IBU DAN ANAK PURI BUNDA</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>SIMPANG SULFAT UTARA 60A MALANG - 65124</small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>(0341) 480047, 477511 Fax. (0341) 485990</small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p>website</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p>MALANG - JAWA TIMUR</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                
                </td>
                <td rowspan="6" width="25%"><img src="<?= base_url() ?>/assets/plugins/images/logo-BPJS-Kesehatan.png" alt="bpjs"></td>
            </tr>
           
        </table>
        <br><br>
        <table border="0px" width="100%" id="table_utama">   
            <tr>
                <td style="text-align:left;" width="25%">KODE RS : </td>
                <td width="25%">...</td>
                <td width="25%" style="text-align: left;">KELAS RS : </td>
                <td width="25%">...</td>
            </tr>
        </table>
        <table border="1" width="100%" class="table_utama"> 
            <tr>
                <td>1</td>
                <td width="20%">NO.REKAM MEDIS</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td> 
                <td width="50">&nbsp;</td>
                <td width="20%" style="text-align: center">NO.ID</td>
                <td width="50">&nbsp;</td> 
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td> 
            </tr>
            <tr>
                <td>2</td>
                <td width="20%">NO KARTU</td>
                <td colspan="13">0112341244</td>
            </tr>
            <tr>
                <td>3</td>
                <td width="20%">NAMA PASIEN</td>
                <td colspan="13">TRI AGU</td>
            </tr>
            <tr>
                <td>4</td>
                <td width="20%">TANGGAL LAHIR</td>
                <td colspan="13">13/8/1995</td>
            </tr>
            <tr>
                <td>5</td>
                <td width="20%">JENIS KELAMIN</td>
                <td colspan="5">
                    <input type="checkbox" name="tes" />LAKI-LAKI
                </td>
                <td colspan="8">
                    <input type="checkbox" name="tes" />PEREMPUAN
                </td>
            </tr>
            <tr>
                <td>6</td>
                <td width="20%">UMUR</td>
                <td colspan="13">21 BULAN</td>
            </tr>
            <tr>
                <td>7</td>
                <td width="20%">UMUR(BAYI)</td>
                <td colspan="5">HARI</td>
                <td colspan="8">0</td>
            </tr>
            <tr>
                <td>8</td>
                <td width="20%">BERAT LAHIR</td>
                <td colspan="5">GRAM</td>
                <td colspan="8">0</td>
            </tr>
            <tr>
                <td>9</td>
                <td width="20%">JENIS PERAWATAN</td> 
                <td colspan="5"><input type="checkbox" name="">RAWAT INAP</td>
                <td colspan="8" style="border-left: 0px"><input type="checkbox" name="">RAWAT INAP</td>
            </tr>
            <tr>
                <td>10</td>
                <td width="20%">KELAS PERAWATAN</td> 
                <td colspan="5"><input type="checkbox" name="">KELAS 1</td>
                <td colspan="2" ><input type="checkbox" name="">KELAS 2</td>
                <td colspan="6" ><input type="checkbox" name="">KELAS 2</td>
            </tr>
            <tr>
                <td>11</td>
                <td width="20%">TANGGAL MASUK</td> 
                <td colspan="5">11/2/2018</td>
                <td colspan="8" >TGL/BL/TH</td>
            </tr>
            <tr>
                <td>12</td>
                <td width="20%">TANGGAL KELUAR</td> 
                <td colspan="5">11/2/2018</td>
                <td colspan="8" >TGL/BL/TH</td>
            </tr>
            <tr>
                <td>13</td>
                <td width="20%">LAMA DIRAWAT</td> 
                <td colspan="2">1</td>
                <td colspan="3" >HARI</td>
                <td colspan="8" >Jumlah hari perawatan = tgl keluar - tgl masuk + 1</td>
            </tr>
            <tr>
                <td>14</td>
                <td width="20%">TOTAL BIAYA CBG</td> 
                <td colspan="13">&nbsp;</td>
            </tr>
            <tr>
                <td>15</td>
                <td width="20%">KODE CBG-JKN</td> 
                <td colspan="5">&nbsp;</td>
                <td colspan="8">Diisi Oleh Rekam Medis;</td>
            </tr>
            <tr>
                <td rowspan="3">16</td>
                <td width="20%" rowspan="3">CARA PULANG</td>  
                <td colspan="5"><input type="checkbox" name="">1.SEMBUH</td>
                <td colspan="8"><input type="checkbox" name="">4.MENINGGAL DUNIA</td>
            </tr>
            <tr>
                <td colspan="5"><input type="checkbox" name="">2.DIRUJUK</td>
                <td colspan="8"><input type="checkbox" name="">5.TIDAK TAHU</td>
            </tr>
            <tr>
                <td colspan="13"><input type="checkbox" name="">3.PULANG PAKSA</td>
            </tr>
            <tr>
                <td rowspan="2">17</td>
                <td rowspan="2" width="20%" style="text-align: center"><B>DIAGNOSA UTAMA</B></td>
                <td colspan="9" ></td> 
                <td colspan="5" style="text-align: center;"><B>CODE ICD-10</B></td>
            </tr>
            <tr>
                <td colspan="9">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>



            <tr>
                <td rowspan="5">18</td>
                <td rowspan="5" width="20%" style="text-align: center;" ><b>DIAGNOSA SEKUNDER</b></td>
                <td>1</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td>2</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td>3</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td>4</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td>5</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>

            <tr>
                <td>19</td>
                <td colspan="7" style="text-align: center;"><b>PROCEDUR / TINDAKAN</b></td>
                <td colspan="3" style="text-align: center;">TANGGAL</td>
                <td colspan="5" style="text-align: center;"><b>CODE ICD-9-CM</b></td>
            </tr>
            <tr>
                <td>20</td>
                <td colspan="7"></td>
                <td colspan="3"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td> 
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>


        </table>     

        <table width="100%" >
            
            <tr>
                <td width="10%" valign="top" style="text-align: right;">Catatan: </td>
                <td valign="top">   
                    <p >Diagnosa diisi oleh dokter yang merawat</p> 
                    <p>Untuk pasien yang dilakukan tindakan disertakan tanggal dilakukan tindakan tersebut</p> 
                    <p>  Beri tanda  ( √ )  pilihan </p> 
                </td>
            </tr>
        </table>
        <table width="100%" >
            <tr> 
                <td width="50%" style="text-align: center;"><h3> <input type="checkbox" name=""> <b>SEVERITY LEVEL 3</b></h3></td> 
                <td style="text-align: center;">
                    <p>DOKTER</p>
                    <p>PENANGGUNG JAWAB PASIEN (DPJP)</p><br><br><br><br>
                    <p>______________________________</p>
                </td>
            </tr>

        </table>

       
    </body>
</html>