			  <?php

			  $message_type = $this->session->flashdata('message_type');
			  $messages = $this->session->flashdata('messages');

			  ?>

			  <?php 
			  if($message_type == 'error'):
			  ?>
			  <div class="alert alert-danger alert-dismissable">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
	            <?php echo $messages;?>
	          </div>
	          <?php 
	          elseif($message_type == 'info'):
	          ?>
	          <div class="alert alert-info alert-dismissable">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <h4><i class="icon fa fa-info"></i> Alert!</h4>
	            <?php echo $messages;?>
	          </div>
	          <?php 
	          elseif($message_type == 'warning'):
	          ?>
	          <div class="alert alert-warning alert-dismissable">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
	            <?php echo $messages;?>
	          </div>
	          <?php 
	          elseif($message_type == 'success'):
	          ?>
	          <div class="alert alert-success alert-dismissable">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
	            <?php echo $messages;?>
	          </div>
	          <?php
	          endif;
	          ?>