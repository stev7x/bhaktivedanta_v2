
        <!-- Left navbar-header --> 
        <div class="navbar-default sidebar" role="navigation" style="position: fixed;">
            <div class="sidebar-nav navbar-collapse" id="slimtest4">
                <ul class="nav" id="side-menu"> 
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group --> 
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="<?php echo base_url() ?>assets/plugins/images/users/d1.jpg" alt="user-img" class="img-circle" style="float:left" > 
                        <span class="hide-menu">  
                            <span class="n-user" >
                             <b><?php echo $users->username; ?></b> |   
                            </span>
                            <span class="r-user"> 
                                <?php echo $groups[0]->name; ?>  
                            </span>
                            <span class="fa arrow"></span>
                            
                        </span>  
                        </a>
                        <ul class="nav nav-second-level"> 
<!--                             <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
                            <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li> -->
                            <li><a href="<?php echo site_url('login/do_logout')?>"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>
                    <li class="nav-small-cap m-t-10">--- Main Menu</li>
                    <?php
                        $list_menus = build_tree($list_menu_sidebar);
                        print_tree($list_menus, TRUE);
                    ?> 
                    <li>&nbsp;</li>
                    <li>&nbsp;</li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->