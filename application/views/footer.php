    <style>
        #card-notif{
            background:rgba(0,172,200, 0.70);
            /* opacity: 0.7; */
            /* color:white; */
            font-weight: bold;
            margin-top:10px;
            width:35rem;
            border-radius: 10px;
        }

        #card-show{
            position: fixed;
            right: 10px;
            bottom: 65px;
            z-index: 9999;
        }
/*
        #notif{
            font-weight: bold;
            color:white;
            
        }*/
        #card-notif:hover{
            background-color :#01c0c8;
            box-shadow:3px 3px 40px white;
            opacity: 1;
            
        }

    </style>
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-xs-6 notification" id="card-show">
                
            </div>
        </div>
    </div>
   <!--  <div class="notif">
        <div id="alertbottomright" style="display: block;" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-bottom-right">
            <img src="<?php echo base_url()?>assets/plugins/images/ic_notif.png" class="img" alt="img"><a href="#" class="closed">&times;</a>
            <h4><b>Administrator</b></h4>  Pasien baru telah terdaftar pada 10:30:15
        </div>
    </div> -->
    
    


    <footer class="footer text-center"> 2018 &copy; PT Jaya Data Informatika </footer>
    </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery --> 
    <script language="javascript">
    var ci_baseurl = '<?php echo base_url();?>'; 
    var csrf_name = '<?php echo $this->security->get_csrf_token_name()?>';
    var csrf_hash = '<?php echo $this->security->get_csrf_hash()?>'; 
    </script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url()?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
     <!-- <script src="<?php echo base_url()?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script> -->
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url()?>assets/js/jquery.slimscroll.js"></script>
    <!-- <script src="<?php echo base_url()?>assets/js/materialize.js"></script>   -->
    <!--Wave Effects -->
    <script src="<?php echo base_url()?>assets/js/waves.js"></script>
    <!-- toast -->
    <script src="<?php echo base_url()?>assets/js/toastr.js"></script>
    <!-- custom -->
    <script src="<?php echo base_url()?>assets/js/custom.min.js"></script>  
    <!-- datepicker -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> 
    <!-- <script src="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-datepicker/datepicker-id.js"></script>  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.id.min.js"></script>
    
    <script src="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
 
    <!-- <script src="<?php echo base_url()?>assets/js/editor.dataTables.js"></script> -->
    <!-- switchery -->   
    <script src="<?php echo base_url()?>assets/plugins/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    
    

    <!-- Sparkline chart JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- jquery ui -->  
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery-ui-1.12.1.custom/jquery-ui.js"></script>    
    <!-- JS Plug JSTree --> 
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jstree/dist/jstree.min.js"></script>
    <!-- jQuery peity -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/peity/jquery.peity.init.js"></script>
        <!-- morris -->      
    <script src="<?php echo base_url()?>assets/plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/morrisjs/morris.js"></script>    
    <!-- Custom Theme JavaScript -->
              
     <?php  
        if(!empty($js1)){
            echo $js1;
        }else{
            echo '';     
        }

        if(!empty($js2)){
            echo $js2;
        }else{
            echo '';
        }    

    $js_files = uri_string();
    $js_exists = file_exists(FCPATH . 'assets/dist/js/pages/'. $js_files . '.js');

    if ( !empty($js_files) && $js_exists):
        ?>
        <script src="<?php echo base_url()?>assets/dist/js/pages/<?php echo $js_files;?>.js"></script>
        <?php
    endif;
   ?>
    <!-- SweetAlert2 --> 
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweatalert2/sweatalert2.min.js" type="text/javascript"></script>      
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script> -->
    <script src="<?php echo base_url()?>assets/js/validator.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
      
    <!-- Clock Plugin JavaScript -->
    <script src="<?= base_url()?>assets/plugins/bower_components/clockpicker/jquery-clockpicker.js"></script>      

    <script>
         jQuery(document).ready(function () {
                // Switchery
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                $('.js-switch').each(function () {
                    new Switchery($(this)[0], $(this).data());
                });
                // For select 2
                $(".select2").select2();
                // $('.selectpicker').selectpicker();
                jQuery('.mydatepicker, #datepicker').datepicker({       
                    autoclose: true
                    , todayHighlight: true
                    , format:'d MM yyyy' 
                });


                jQuery('.mydatepicker2').datepicker({
                    autoclose: true
                    , todayHighlight: true
                    , format:'d M yyyy' 
                }); 

                $('.datepicker-id').datepicker({
                    autoclose: true
                    , todayHighlight: true
                    , format:'dd MM yyyy'
                    , language :'id'
                }); 

                var date = new Date();
                // var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

                $('.datepick').datepicker({
                    autoclose: true
                    ,changeMonth: true
                    ,changeYear: true
                    ,format:'mm/dd/yyyy'
                    // ,startDate: new Date()
                    ,todayHighlight: true
                    ,language: 'id'
                }); 


                jQuery('.datepicker-autoclose').datepicker({
                    autoclose: true
                    , todayHighlight: true
                    });
                jQuery('#datepicker-autoclose2').datepicker({
                    autoclose: true
                    , todayHighlight: true
                    });
                $('.datepicker-autoclose3').datepicker({
                    autoclose: true
                    , todayHighlight: true ,
                    });
                

                 function toggleFullScreen() {
                    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                      (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                      if (document.documentElement.requestFullScreen) {
                        document.documentElement.requestFullScreen();
                      }
                      else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                      }
                      else if (document.documentElement.webkitRequestFullScreen) {
                        document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                      }
                    } 
                    else {
                      if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                      }
                      else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                      }
                      else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                      }
                    }
                  }

                  $('#toggle-fullscreen').click(function() {
                    toggleFullScreen();
                  });


                function numbersOnly(event){
                    var charCode = (event.which) ? event.which : event.keyCode;
                //    console.log(charCode);
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                        return false;
                    return true;
                } 
                $('#slimtest4').slimScroll({
                        color: 'gray' 
                        , size: '10px'  
                        , height: '100%'  
                        , alwaysVisible: false 
                    }); 
                $('#slimtest5').slimScroll({
                        color: 'gray' 
                        , size: '10px'   
                        , height: '564px'  
                        , alwaysVisible: false 
                    }); 
                    
            });

            $('.clockpicker').clockpicker({
            donetext: 'Done'
            , }).find('input').change(function () {
                console.log(this.value);
            });



        
            function loadNotif(){
                $.ajax({
                    url : ci_baseurl + 'notif/notif/get_data_notification',   
                    method:'GET',   
                    dataType:'json', 
                    success:function(data){ 
                        var ret = data.load;
        
                        if(ret == 'success'){
                            console.log('hadir sukseskan notif'); 
                            $('.notification').html(data.data);
                            // $("#alertbottomright"+data.id).fadeToggle(350);            

                            // $('#card-notif').show(2000);             
                            // $('#tes').show(500);  
                            // $('#title_notif').text(data.title);
                            // $('#description_notif').text(data.description);
                            // $('button#printStiker1').attr('onclick','printData('+pasien_id+',"stiker_rm")');
                            // $('a#unseen').attr('onclick','unseen('+data.id+')');              
                        }else{ 
                            console.log('tidak berhasil'); 
                        }       

                    }
                }); 
            }
            // loadNotif();  


            // setInterval(function(){
            //      loadNotif();      
            // }, 5000);   

            function unseen(id){     
                console.log('isi : '+id)
                $.ajax({  
                    url   : ci_baseurl + 'notif/notif/unseen_notif',
                    method:'GET',    
                    data  :{id:id}, 
                    dataType:'json',
                    success:function(data){
                        var ret = data.view;
                        if(ret == 'SUDAH DILIHAT'){  
                            console.log('hadir di unseen');
                                    // table_global.columns.adjust().draw();    
                        }else{   
                            console.log('belum dilihat');
                        }
                    }
                });
            } 

            
            function closeNotif(id){  
                $.ajax({  
                    url   : ci_baseurl + 'notif/notif/unseen_notif',
                    method:'GET',    
                    data  :{id:id}, 
                    dataType:'json',
                    success:function(data){
                        var ret = data.view;     
                        if(ret == 'SUDAH DILIHAT'){  
                            console.log('hadir di hide_notif');  
                            // $('#card_show').fadeOut(500);   
                            $('.notif'+id).fadeOut(500);   
                            // table_global.columns.adjust().draw();    
                            // location.reload();    
 
                        }else{      
                            console.log('tidak hadir di hide_notif');
                        }
                    }
                });
            }

    </script>
    <!--Style Switcher -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
     <!-- wysuhtml5 Plugin JavaScript --> 
    <script src="<?php echo base_url()?>assets/plugins/bower_components/tinymce/tinymce.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
    <script src="<?php echo base_url()?>assets/js/custom-file-input.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src='https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.1/lodash.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>  
    <script src="<?php echo base_url()?>assets/plugins/bower_components/moment/moment-with-locales.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/moment/moment-with-locales.min.js"></script>

    <!-- tambahan danis -->
    
    
    <!-- tambahan danis -->
  
</body>

</html>
