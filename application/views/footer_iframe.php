   <!-- /#wrapper -->
    <!-- jQuery -->
    <script language="javascript">
    var ci_baseurl = '<?php echo base_url();?>';
    var csrf_name = '<?php echo $this->security->get_csrf_token_name()?>';
    var csrf_hash = '<?php echo $this->security->get_csrf_hash()?>';
    </script> 
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
     
     
    <!-- Bootstrap Core JavaScript -->  
    <script src="<?php echo base_url()?>assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url()?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>      
    
    
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->   
    <script src="<?php echo base_url()?>assets/js/jquery.slimscroll.js"></script>
    <!-- <script src="<?php echo base_url()?>assets/js/materialize.js"></script>   -->
    <!--Wave Effects --> 
    <script src="<?php echo base_url()?>assets/js/waves.js"></script>  
    <script src="<?php echo base_url()?>assets/js/custom.min.js"></script>  
    <script src="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  
     <script src="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>  
     <!-- bootstap editable -->                     
    <script src="<?php echo base_url() ?>assets/plugins/bower_components/bootstrap3-editable/js/bootstrap-editable.js"></script> 
    <!-- jeditable --> 
    <!-- <script src="<?php echo base_url() ?>assets/plugins/bower_components/Jeditable/jquery.jeditable.js"></script> -->




     <!-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>    -->
     <!-- editor datatables -->  
    <!-- <script src="<?php echo base_url()?>assets/plugins/bower_components/Editor-PHP-1.7.0/js/dataTables.editor.js"></script>  -->
  
     <?php
    $js_files = uri_string();
    $js_exists = file_exists(FCPATH . 'assets/dist/js/pages/'. $js_files . '.js');

    if ( !empty($js_files) && $js_exists):
        ?>  
        <script src="<?php echo base_url()?>assets/dist/js/pages/<?php echo $js_files;?>.js"></script>
        <?php
    endif;
   ?>   
    


    <!--Morris JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/morrisjs/morris.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- jQuery peity -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/peity/jquery.peity.init.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>assets/js/dashboard1.js"></script>   
    <!-- SweetAlert2 --> 
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweatalert2/sweatalert2.min.js" type="text/javascript"></script>   

    <script src="<?php echo base_url()?>assets/js/validator.js"></script>    
    <script src="<?php echo base_url()?>assets/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="<?= base_url()?>assets/plugins/bower_components/clockpicker/jquery-clockpicker.js"></script>      

   
    <script>       
         jQuery(document).ready(function () {
                // Switchery
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                $('.js-switch').each(function () {
                    new Switchery($(this)[0], $(this).data());
                });
                // For select 2
                $(".select2").select2();
                // $('.selectpicker').selectpicker();
                jQuery('.mydatepicker, #datepicker').datepicker({
                    autoclose: true
                    , todayHighlight: true
                    , format:'d MM yyyy'
                    , max : true     
                });  
                jQuery('.mydatepicker1').datepicker({
                    autoclose: true 
                    , todayHighlight: true
                    , format:'DD, d MM yyyy'
                    , max : true       
                });    
                  

                jQuery('.datepicker-autoclose').datepicker({
                    autoclose: true
                    , todayHighlight: true 
                    });   
                jQuery('#datepicker-autoclose2').datepicker({
                    autoclose: true
                    , todayHighlight: true
                    });  
                $('.datepicker-autoclose3').datepicker({
                    autoclose: true  
                    , todayHighlight: true ,   
                    });   

            $('.clockpicker').clockpicker({
            donetext: 'Done'
            , }).find('input').change(function () {
                console.log(this.value);
            });      





                 function toggleFullScreen() {
                    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                      (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                      if (document.documentElement.requestFullScreen) {
                        document.documentElement.requestFullScreen();
                      }
                      else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                      }
                      else if (document.documentElement.webkitRequestFullScreen) {
                        document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                      }
                    }
                    else {
                      if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                      }
                      else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                      }
                      else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                      }
                    }
                  }
  
                  $('#toggle-fullscreen').click(function() {
                    toggleFullScreen();
                  });

                  function numbersOnly(event){
                        var charCode = (event.which) ? event.which : event.keyCode;
                    //    console.log(charCode);
                        if (charCode > 31 && (charCode < 48 || charCode > 57))
                            return false;
                        return true;
                    }

            });

         $('#slimtest1').slimScroll({
              color: 'gray' 
              , size: '10px'  
              , height: '100%'  
              , alwaysVisible: false 
          });   


             

             $(document).ready(function () {
                        if ($("#mymce").length > 0) {
                            tinymce.init({
                                selector: "textarea#mymce"
                                , theme: "modern"
                                , height: 300
                                , plugins: [
               "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker"
               , "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking"
               , "save table contextmenu directionality emoticons template paste textcolor"
               ]
                                , toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright  alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons"
                            , });
                        }
                    });  

    $('.datepicker-id').datepicker({
        autoclose: true
        , todayHighlight: true
        , format:'dd MM yyyy'
        , language :'id'
    }); 
  
    </script>   
    <!-- wysuhtml5 Plugin JavaScript --> 
    <script src="<?php echo base_url()?>assets/plugins/bower_components/tinymce/tinymce.min.js"></script>     
    <!--Style Switcher -->   
    <script src="<?php echo base_url()?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script> 
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/kirim_penunjangrd.js"></script>
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/permintaan_rawatinap.js"></script>
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/kirim_rawatinap.js"></script>
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/kirim_vkrd.js"></script>
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/kirim_okrd.js"></script>
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/pasien_rd.js"></script> 
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/kirim_hcurd.js"></script>
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/kirim_isolasird.js"></script>  
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/kirim_pathologird.js"></script>  
    <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/rujuk_rslain.js"></script>  

 

    
 