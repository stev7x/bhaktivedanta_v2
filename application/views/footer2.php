</div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->



  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START FOOTER -->
  <footer class="page-footer">
    <div class="footer-copyright">
      <div class="container">
        <span>Copyright © 2017 <a class="grey-text text-lighten-4" href="#" target="_blank">RS Puri Bunda</a> All rights reserved.</span>
        <span class="right">Developed by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">GeeksLabs</a></span>
        </div>
    </div>
  </footer>
  <!-- END FOOTER -->



    <!-- ================================================
    Scripts
    ================================================ -->
    <script language="javascript">
    var ci_baseurl = '<?php echo base_url();?>';
    var csrf_name = '<?php echo $this->security->get_csrf_token_name()?>';
    var csrf_hash = '<?php echo $this->security->get_csrf_hash()?>';
    </script>
    <!-- jQuery Library -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/jquery-ui.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/materialize.js"></script>
    <!--prism-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins/chartist-js/chartist.min.js"></script>
    <!--jquery formatter-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins/formatter/jquery.formatter.min.js"></script>
    <!-- data-tables -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins/data-tables/dataTables.rowsGroup.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins/data-tables/data-tables-script.js"></script>-->
    <!--alert js-->
    <script src="<?php echo base_url()?>assets/dist/js/plugins/alert/jquery.alerts.js" type="text/javascript"></script>

	<!-- JS Plug JSTree -->
	<script src="<?php echo base_url()?>assets/dist/js/plugins/jstree/dist/jstree.min.js"></script>
    <?php
    $js_files = uri_string();
    $js_exists = file_exists(FCPATH . 'assets/dist/js/pages/'. $js_files . '.js');

    if ( !empty($js_files) && $js_exists):
        ?>
        <script src="<?php echo base_url()?>assets/dist/js/pages/<?php echo $js_files;?>.js"></script>
        <?php
    endif;
   ?>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <!--<script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/custom-script.js"></script>-->
    <script language="javascript">
//        $(document).ready(function(){
//            $(".dropdown-content.select-dropdown li").on( "click", function() {
//                var that = this;
//                setTimeout(function(){
//                if($(that).parent().hasClass('active')){
//                        $(that).parent().removeClass('active');
//                        $(that).parent().hide();
//                }
//                },100);
//            });
//        });
        $(function(e){
            var url=window.location;
            console.log(url);
            $('.bold a').each(function(e){
                var link = $(this).attr('href');
                if(link==url){
                    $(this).parents('li').addClass('active');
                    $(this).parents('.collapsible-body').attr('style','display:block;');
                    $(this).closest('.bold').addClass('active');
                }
            });
        });

        function numbersOnly(event){
            var charCode = (event.which) ? event.which : event.keyCode;
        //    console.log(charCode);
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</body>

</html>
