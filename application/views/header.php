<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/plugins/images/favicon.png">
    <title>Bhaktivedanta Medical</title>

    <!-- bootstrap -->
    <link href="<?php echo base_url()?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- jquery ui css -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- select-2 -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <!-- morris -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/morrisjs/morris.css" rel="stylesheet" type="text/css" />
    <!-- switchery -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <!-- DatePicker -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
     <!-- CSS Plug JSTree -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/bower_components/jstree/dist/themes/default/style.min.css">
    <!-- animation CSS -->
    <link href="<?php echo base_url()?>assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>assets/css/style2.css" rel="stylesheet">
    <!-- data-table -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- SweetAlert2 -->
     <link href="<?php echo base_url()?>assets/plugins/bower_components/sweatalert2/sweatalert2.min.css" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css"> -->
    <!-- color CSS -->
    <link href="<?php echo base_url()?>assets/css/colors/megna.css" id="theme" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
    <!-- editor datatables -->
    <link href="<?php echo base_url()?>assets/css/editor.datatables.css" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="<?php echo base_url()?>assets/plugins/bower_components/clockpicker/jquery-clockpicker.min.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <style type="text/css">
     /* scroller browser */
    .no::-webkit-inner-spin-button,
    .no::-webkit-outer-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* style ketersediaan kamar */
    .ketersediaan-box{
      /* margin-right: 100px; */
      float: right;
    }



    .ketersediaan-wrap{
      float: right;
      width: 50px;
    }
    .ketersediaan-text{
      text-align: center;
      font-size: 12px;
      color: white;
    }
    .round{
      width: 28px;
      height: 28px;
      margin: 7px 10px 2px 10px;
      padding: 5px 0px 10px 0px;
      border-radius: 50%;
      position: relative;
      text-align: center;
      /* border: 1px solid white; */
      /* outline-style: solid; */
      /* vertical-align: middle; */
      color: white;
      /* box-shadow: 0px 0px 3px 3px black; */
    }

    .round-checkin{
      width: 150px;
      height: 35px;
      margin: 9px 10px 2px 10px;
      padding: 5px 0px 10px 0px;
      position: relative;
      text-align: center;
      font-weight: bold;
      /* border: 1px solid white; */
      /* outline-style: solid; */
      /* vertical-align: middle; */
      color: #004447;
      
      /* box-shadow: 0px 0px 3px 3px black; */
      border-radius: 32px; 
      -moz-border-radius: 32px; 
      -webkit-border-radius: 32px; 
      border: 3px solid #006064;
          }
      
      .round-checkin:hover {
        background-color: #0091ea;
      }

    .red{
      background-color: rgb(213, 0, 0);
    }
    .green{
      background-color: rgb(76, 175, 80);
    }
    .black{
      background-color: #00bfa5;
    }

    .form-control{
      max-height:unset!important;
    }
    /* tambahan danis */
    .dropdown-action{
        display: inline-block;
        height: 40px;
        width: 170px;
        background-color: #31CDA7;
        border-radius: 0px;
        box-shadow: none;
        color: white;
        max-width: 100%;
        position: relative;
        transition: 0.3s;
    }
    .dropdown-action:hover{
        background: #00c292;
    }
    .dropdown-action .title-dropdown{
        text-align: center;
        display: block;
        line-height: 40px;
        cursor: pointer;
        font-size: 100%;
        font-weight: 500;
    }
    .dropdown-action .arrow{
        position: absolute;
        top: 50%;
        right: 0;
        margin-right: 15px;
        transform: translateY(-50%);
        cursor: pointer;
        font-size: 10px;
    }
    .dropdown-menu-action{
        width: 100%;
        background: white;
        border: 1px solid #31CDA7;
        position: absolute;
        padding : 5px;
        top: 40px;
        display: none;
        box-sizing: border-box;
        z-index: 999;
    }
    /* .dropdown-menu-action .btn-success{
        width: 100%;
    } */
    .dropdown-menu-action button{
        margin: 5px 0;
        width: 100%;
    }
    .d-block{
        display:block;
    }
    .bg-title{
      margin-top: 60px;
    }
    /* tambahan danis */
    </style>
    <!-- tambahan danis -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).on("click",".dropdown-action", function() {
            $(".table-responsive").css({
                "overflow-y" : "auto"
            });
        });
    </script>
    <!-- tambahan danis -->
</head>

<body id="body">
    <!-- Preloader -->
    
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0" style="position: fixed;">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="<?php echo site_url('dashboard'); ?>"><b><img src="<?php echo base_url()?>assets/plugins/images/logo2.png" style="max-width:200px;max-height:75px;margin-top:-5px;margin-left:7px" alt="home" /></b></a></div>
                <div style="float:right;">
                  <ul class="nav navbar-top-links navbar-right hidden-xs m-r-10">
                      <li><a href="javascript:void(0)" id="toggle-fullscreen"><i class="fa fa-arrows-alt"></i></a></li>
                  </ul>
                </div>
<!--
data nomor jenis kamar:
1 1
2 2
3 3
5 VIP
8 HCU
9 Bayi Pathologi
10 Isolasi -->
                
                  
                  <div class="ketersediaan-box">
                    <?php if ($this->session->tempdata('status_checkin')=="CHECKIN") { ?>
                      <div class="round-checkin black" id="btn_checkout" style="text-color:white;">
                          Checkout
                      </div>
                    <?php } else { ?>
                      <div class="round-checkin black" id="btn_checkin" style="text-color:white;">
                          Chekin
                      </div>
                    <?php }?>
                  </div>
                </div>
            </div>
        </nav>

<div id="modal_checkin" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog modal-lg">  
    <?php echo form_open('#',array('id' => 'frmCheckin'))?>
       <div class="modal-content" style="margin-top:150px;">    
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Absen Pegawai</h2> 
            </div>    
            <div class="modal-body">    
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Nama<span style="color: red">*</span></label>
                        <input type="text"  name="nama" class="form-control" placeholder="Nama" value=<?php echo $users->nama_lengkap; ?> readonly>
                    </div>  
                    <div class="form-group col-md-12">
                        <label>To Do List<span style="color: red">*</span></label>
                        <textarea id="todo" name="todo" class="form-control" rows="6"> </textarea>   
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Datetime<span style="color: red">*</span></label>
                        <input type="text" id="absent_time" name="absent_time" class="form-control" placeholder="Nama">
                    </div>  

                    <div class="form-group col-md-12">
                      <label>Branch<span style="color: red">*</span></label>
                      <input type="text" id="branch" name="branch" class="form-control" readonly value="<?php echo $this->session->tempdata('data_session')[1]; ?>">
                    </div>  
                </div> 

               
                
                <div class="row" style="margin-bottom:30px;margin-top:20px;">
                    <div class="col-md-12">  
                     
                    <?php if ($this->session->tempdata('status_checkin')=="CHECKIN") { ?>
                        <button type="button" class="btn btn-success pull-right" id="saveCheckout"><i class="fa fa-floppy-o p-r-10"></i>Simpan</button>
                    <?php } else { ?> 
                      <button type="button" class="btn btn-success pull-right" id="saveCheckin"><i class="fa fa-floppy-o p-r-10"></i>Simpan</button>
                    <?php } ?>
                    </div>
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>

<script> 
$("#btn_checkin").click(function(){
  $('#modal_checkin').modal('show');
}); 

$("#btn_checkout").click(function(){
  $('#modal_checkin').modal('show');
}); 

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function startTime() {
    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth();
    var day = today.getMonth();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    // add a zero in front of numbers<10
    m = checkTime(m);
    s = checkTime(s);
    $('#absent_time').val(today);
    t = setTimeout(function () {
        startTime()
    }, 500);
}
startTime();

$('button#saveCheckin').click(function(){
        swal({
          text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){ 
            $.ajax({
                    url: ci_baseurl + "api/checkin/do_checkin",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#frmCheckin').serialize(),
                    success: function(data) {
                      var link = window.location.href;
                    
                      location.replace(link);
                    }
                }); 
            
          swal( 
            'Berhasil!',
            'Data Berhasil Disimpan.', 
          ) 
        })  
    });

    $('button#saveCheckout').click(function(){
        swal({
          text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){ 
            $.ajax({
                    url: ci_baseurl + "api/checkin/do_checkout",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#frmCheckin').serialize(),
                    success: function(data) {
                      var link = window.location.href;
                      
                      location.replace(link);
                    }
                }); 
            
          swal( 
            'Berhasil!',
            'Data Berhasil Disimpan.', 
          ) 
        })  
    });
</script>