<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function build_tree(array $elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent'] == $parentId) {
            $children = build_tree($elements, $element['role_id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }
 
    return $branch;
}

function print_tree($tree, $first=FALSE) {
    if(!is_null($tree) && count($tree) > 0) {
        foreach($tree as $treeNode) {
            if (isset($treeNode['children'])) {
                $purl = current_url().$treeNode['url'];
                echo '<li>'.anchor($purl,'<i class="'.$treeNode['icon'].'"></i><span class="hide-menu">'.$treeNode['role_name'].'<span class="fa arrow"></span></span>','class="waves-effect active"'); 
                echo '<ul class="nav nav-second-level collapse">';
                foreach($treeNode['children'] as $treeNodeChild) {
                    if (isset($treeNodeChild['children'])) {
                        $purlChild = current_url().$treeNodeChild['url'];
                        echo '<li>'.anchor($purlChild,'<span class="hide-menu">'.$treeNodeChild['role_name'].'<span class="fa arrow"></span></span>','class="waves-effect active"'); 
                        echo '<ul class="nav nav-third-level collapse">';
                        foreach($treeNodeChild['children'] as $children) {
                            $purlChildren = $children['url'];
                            echo '<li>'.anchor($purlChildren,'<i class="'.$children['icon'].'"></i><span class="hide-menu">'.$children['role_name'].'</span>','class="waves-effect active"') . '</li>'; 
                        }
                        echo '</ul>';
                        echo '</li>';
                    }
                    else {
                        $purlChild = $treeNodeChild['url'];
                        echo '<li>'.anchor($purlChild,'<span class="hide-menu">'.$treeNodeChild['role_name'].'</span>','class="waves-effect active"') . '</li>'; 
                    }
                }
                echo '</ul>';
                echo '</li>';
            }
            else {
                $purl = $treeNode['url'];
                echo '<li>'.anchor($purl,'<i class="'.$treeNode['icon'].'"></i><span class="hide-menu">'.$treeNode['role_name'].'</span>','class="waves-effect active"') . '</li>'; 
            }
        }
        // if($first){
        //     echo '';    
        // }else{
        //     // echo '<div class="collapsible-body">';
        //     // echo '<ul>';  
        // }
        // foreach($tree as $node) {
        //     $url = $node['url'];  
        //     if($url == '#'){
        //         $purl = current_url().$node['url'];
        //     }else{
        //         $purl = $node['url'];
        //     }
             
        //     if(isset($node['children'])){    
            	// if($first){
                //     // echo '<ul>';    
                     
                //     echo '<li>';      
                //     echo ''.anchor($purl,'<i class="'.$node['icon'].'"></i><span class="hide-menu">'.$node['role_name'].'<span class="fa arrow"></span></span>','class="waves-effect"');  
                //     echo '<ul class="nav nav-second-level collapse">';          
                //     // echo '<li>'.anchor($purl,'<i class="'.$node['icon'].'"></i>asdasd'.$node['role_name'],'class="waves-effectttttttt"');
                // }
        //         print_tree($node['children']);
        //     }else{ 
        //     	if($first){   
        //             echo '<li>'.anchor($purl,'<i class="'.$node['icon'].'"></i><span class="hide-menu">'.$node['role_name'].'</span>','class="waves-effect active"'); 
        //         }else{       
        //             echo '<li>'.anchor($purl, $node['role_name']);
        //         }    
        //     }    
        //     if(isset($node['children'])){
        //         echo '</ul>';
        //         echo '</li>'; 
        //         // echo '</li>';
        //     }else{
        //         echo '</li>';
        //     }
        // }
        // if($first){
        // echo '';    
        // }else{
        //     // echo '</ul>';
        //     // echo '</div>';
        // }
    }
}