
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">List Pasien Calon Retensi</h4> 
                </div>
                <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                    <ol class="breadcrumb"> 
                        <li><a href="index.html">HCU</a></li>
                        <li class="active"> Pasien Retensi</li>
                    </ol> 
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-info1"> 
                        <div class="panel-heading"> Data Seluruh Pasien </div>
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="inputName1" class="control-label"></label>
                                        <dl class="text-right">  
                                            <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                            <dd>Ketik / pilih untuk mencari atau filter data </dd>  
                                        </dl> 
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="form-group col-md-5">
                                                <label for="">RM AWAL</label>
                                                <!-- <input type="text" name="rm_awal" id="rm_awal" class="form-control autocomplete"> -->
                                                <!-- <ul class="dropdown-menu txtcountry" style="margin-left:15px;margin-right:0px;" role="menu" aria-labelledby="dropdownMenu"  id="DropdownCountry"></ul> -->
                                                <!-- <label for="no_rm" class="control-label">Nomor Rekam Medis<span style="color: red;">*</span></label>      -->
                                                <input type="text" id="no_rm_awal" maxlength="8" name="no_rm_awal" onkeyup="changeRM()" class="form-control autocomplete" placeholder=" Cari Nomor Rekam Medis....." >
                                                <input type="hidden" id="rm_awal" name="rm_awal">
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="">RM AKHIR</label>
                                                <input type="text" id="no_rm_akhir" name="no_rm_akhir" maxlength="8" onkeyup="changeRM()" class="form-control autocomplete" placeholder=" Cari Nomor Rekam Medis....." >
                                                <input type="hidden" name="rm_akhir" id="rm_akhir" class="form-control">

                                            </div>
                                            <div class="form-group col-md-2" style="margin-top:24px">
                                                <button class="btn btn-info" onclick="exportAllPasien()">EXPORT</button>
                                            </div>
                                            <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    
                                                <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                <b>
                                                <select name="#" class="form-control select" style="margin-bottom: 7px" id="cari_pasien" >
                                                    <option value="" disabled selected>Pilih Berdasarkan</option>
                                                    <!-- <option value="no_pendaftaran">No. Pendaftaran</option> -->
                                                    <option value="no_rekam_medis">No. Rekam Medis</option>
                                                    <option value="nama_pasien">Nama Pasien</option>     
                                                    <option value="pasien_alamat">Alamat Pasien</option>    
                                                    <option value="tlp_selular">No HP</option>    
                                                    <option value="no_bpjs">No BPJS</option>    
                                                    <option value="no_identitas">No Identitas</option>    
                                                </select></b>
                                                        
                                            </div>    
                                            <div class="form-group col-md-9" style="margin-top: 7px">   
                                                <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasien()" >       
                                            </div> 
                                        </div>
                                    </div>  

                                    </div><br><hr style="margin-top: -27px">

                                    <div class="table-responsive">
                                        <table id="table_list_pasien_list" class="table table-striped dataTable " cellspacing="0">     
                                            <thead>  
                                                <tr>
                                                    <th>No</th>
                                                    <th>Rekam Medis</th>
                                                    <th>Nama Pasien</th>
                                                    <th>Alamat</th>
                                                    <th>Telepon</th>
                                                    <th>No BPJS</th>
                                                    <th>Type indentitas/No Identitas</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>  
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Rekam Medis</th>
                                                    <th>Nama Pasien</th>
                                                    <th>Alamat</th>
                                                    <th>Telepon</th>
                                                    <th>BPJS</th>
                                                    <th>Type indentitas/No Identitas</th>
                                                </tr>  
                                            </tfoot>
                                        </table>
                                    </div> 
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Content -->
<?php $this->load->view('footer');?>
      