<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retensi extends CI_Model {

    public function __construct(){
        parent::__construct();
    }
    
    public function count_list_pasien_all($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas){
        $this->db->from('m_pasien');
        $this->db->where('is_retention', 0);
        $this->db->order_by('no_rekam_medis', 'desc');

        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($tlp_selular)){
            $this->db->like('tlp_selular', $tlp_selular);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_identitas)){
            $this->db->like('no_identitas', $no_identitas);
        }

        return $this->db->count_all_results();
    }
    
    public function count_list_pasien_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas){
        $this->db->from('m_pasien');
        $this->db->where('is_retention', 0);
        $this->db->order_by('no_rekam_medis', 'desc');
        
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($tlp_selular)){
            $this->db->like('tlp_selular', $tlp_selular);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_identitas)){
            $this->db->like('no_identitas', $no_identitas);
        }
        // $i = 0;
        // $search_value = $this->input->get('search');
        // if($search_value){
        //     foreach ($this->column as $item){
        //         ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
        //         $i++;
        //     }
        // }
        // $order_column = $this->input->get('order');
        // if($order_column !== false){
        //     $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        // } 
        // else if(isset($this->order)){
        //     $order = $this->order;
        //     $this->db->order_by(key($order), $order[key($order)]);
        // }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_pasien_rd', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();   
    }

    public function get_list_pasien_list($nama_pasien, $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas){
        
        // $this->db->join('t_pendaftaran','t_pendaftaran.pasien_id = m_pasien.pasien_id','left');
        $this->db->from('m_pasien');
        $this->db->where('is_retention', 0);
        $this->db->order_by('no_rekam_medis', 'desc');

        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($tlp_selular)){
            $this->db->like('tlp_selular', $tlp_selular);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_identitas)){
            $this->db->like('no_identitas', $no_identitas);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }

    public function get_list_pasien_to_export() {
        $this->db->from('m_pasien');
        $this->db->where('is_retention', 0);
        $this->db->order_by('no_rekam_medis', 'asc');
        return $this->db->get()->result();
    }
}

?>