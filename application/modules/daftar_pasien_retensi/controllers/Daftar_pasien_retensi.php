<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_pasien_retensi extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Retensi');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('retensi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "retensi_view";
        $comments = "List Pasien Retensi";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_pasien', $this->data);
    }

    public function ajax_list_list_pasien(){
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $tlp_selular = $this->input->get('tlp_selular',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $no_identitas = $this->input->get('no_identitas',TRUE);
        $list = $this->Retensi->get_list_pasien_list($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular, $no_bpjs, $no_identitas);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $list_pasien){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list_pasien->no_rekam_medis;
            $row[] = $list_pasien->pasien_nama;
            $row[] = $list_pasien->pasien_alamat;
            $row[] = $list_pasien->tlp_selular;
            $row[] = $list_pasien->no_bpjs;
            $row[] = $list_pasien->type_identitas."/".$list_pasien->no_identitas;
            
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Retensi->count_list_pasien_all($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas),
                    "recordsFiltered" => $this->Retensi->count_list_pasien_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Daftar Pasien Calon Retensi");
        $object->getActiveSheet()
                        ->getStyle("A1:G1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:G1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nomor Rekam Medis","Nama Pasien","Alamat","Telepon","No BPJS","No Identitas");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $retension_data = $this->Retensi->get_list_pasien_to_export();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($retension_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->no_rekam_medis);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->pasien_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->pasien_alamat);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, isset($row->tlp_rumah) ? $row->tlp_rumah : $row->tlp_selular);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->no_bpjs);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->type_identitas . '-' .$row->no_identitas);
            $excel_row++;
        }

         foreach(range('A','G') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Daftar Pasien Retensi.xlsx"');
        $object_writer->save('php://output');
    }
}