<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stokgudang extends CI_Controller {

    public $table_obat = "farmasi_obat";
    public $table_persediaan = "farmasi_persediaan";
    public $table_jenis_barang = "farmasi_jenis_barang";
    public $table_suplier = "farmasi_suplier";
    public $table_batch = "farmasi_batch";
    public $table_gudang_utama = "logistik_gudang_utama";
    public $table_stok_gudang_utama = "logistik_stok_gudang_utama";
    public $table_harga_beli = "farmasi_harga_obat";
    public $table_stok_limit = "farmasi_stok_limit";

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('Models');
        $this->load->model('Barang_masuk_model');

        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_masuk_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        $this->data['batch'] = $this->Models->all($this->table_batch);
        $this->data['suplier'] = $this->Models->all($this->table_suplier);
        $this->data['obat'] = $this->Models->all($this->table_obat);

        // if permitted, do logit
        $perms = "";
        $comments = "Stok Gudang Utama";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('stok_gudang', $this->data);
    }

    function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }

    function convert_stok ($stok, $p2, $p3) {
        return array(
            'p1' => @bcdiv(($stok / ($p2 * $p3)), 1, 0),
            'p2' => @bcdiv(($stok % ($p2 * $p3) / $p3), 1, 0),
            'p3' => $p3 <= 0 ? $p3 : @$stok % $p3
        );
    }

    public function ajax_list() {
        $query = "SELECT id, max(updated_at) AS updated_at, obat_id, SUM(stok_awal) AS stok_awal, SUM(stok_farmasi) AS stok_keluar, SUM(stok_akhir) AS stok_akhir, SUM(penyesuaian) AS penyesuaian FROM " . $this->table_stok_gudang_utama . " WHERE branch = '".$this->session->tempdata('data_session')[2]."' GROUP BY obat_id";
        $list = $this->Models->query($query);
        $obat = $this->Models->all($this->table_obat);
        $jenis_barang = $this->Models->all($this->table_jenis_barang);
        $persediaan = $this->Models->all($this->table_persediaan);

        $data = array();
        $no   = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value){
            $stok_awal = $this->convert_stok($value['stok_awal'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $stok_keluar = $this->convert_stok($value['stok_keluar'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $stok_akhir = $this->convert_stok($value['stok_akhir'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $penyesuaian = $this->convert_stok($value['penyesuaian'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);

            $limit = @$this->Models->show($this->table_stok_limit, $value['obat_id']);

//            if (!$this->Models->show($this->table_stok_limit, $value['obat_id'])) {
//                echo $value['obat_id'];
//                die;
//            }

//            echo "<pre>";
//            print_r($limit);
//            exit;
            if ($value['stok_akhir'] > 0) {
                if ($value['stok_akhir'] < $limit['min']) {
                    $stok_status = "Kurang";
                } else if ($value['stok_akhir'] > $limit['max']) {
                    if ($limit['max'] == 0) {
                        $stok_status = "Cukup";
                    } else {
                        $stok_status = "Lebih";
                    }
                } else {
                    $stok_status = "Cukup";
                }
            } else {
                $stok_status = "Habis";
            }

            $p_stok_awal =
                " - " . $stok_awal['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - " . $stok_awal['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - " . $stok_awal['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $p_stok_keluar =
                " - " . $stok_keluar['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - " . $stok_keluar['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - " . $stok_keluar['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $p_stok_akhir =
                " - " . $stok_akhir['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - " . $stok_akhir['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - " . $stok_akhir['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $p_penyesuaian =
                " - (" . $penyesuaian['p1'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - (" . $penyesuaian['p2'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - (" . $penyesuaian['p3'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $obat[$value['obat_id']]['nama'];
            $row[] = $jenis_barang[$obat[$value['obat_id']]['jenis_barang_id']]['nama'];
            $row[] = $p_stok_awal;
            $row[] = $p_stok_keluar;
            $row[] = $p_stok_akhir;
            $row[] = $p_penyesuaian;
            $row[] = $stok_status;
            $row[] = $obat[$value['obat_id']]['keterangan'];
            $row[] = date('d - m - Y', strtotime($value['updated_at']));
//            $row[] = '<button class="btn btn-danger btn-circle" onclick="hapusBarangMasuk('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
//            $row[] = '<button class="btn btn-info" data-toggle="modal" data-target="#modal_penyesuaian" data-backdrop="static" data-keyboard="false" title="klik untuk penyesuaian" onclick="penyesuaian('."'".$value['id']."'".')"> Penyesuaian </button>';
            //add html for action
            $data[] = $row;
        }

        $output = array(
            "draw"            => $this->input->get('draw'),
            "recordsTotal"    => count($list),
            "recordsFiltered" => count($list),
            "data"            => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_stok_list() {
        $query = "SELECT * FROM " . $this->table_stok_gudang_utama . " WHERE branch = '".$this->session->tempdata('data_session')[2]."' ORDER BY obat_id";
        $list = $this->Models->query($query);
        $obat = $this->Models->all($this->table_obat);
        $jenis_barang = $this->Models->all($this->table_jenis_barang);
        $persediaan = $this->Models->all($this->table_persediaan);
        $batch = $this->Models->all($this->table_batch);
        $gudang_utama = $this->Models->all($this->table_gudang_utama);

        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach ($list as $key => $value) {
            $stok_awal = $this->convert_stok($value['stok_awal'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $stok_keluar = $this->convert_stok($value['stok_farmasi'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $stok_akhir = $this->convert_stok($value['stok_akhir'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $penyesuaian = $this->convert_stok($value['penyesuaian'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);

            $p_stok_awal =
                " - " . $stok_awal['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - " . $stok_awal['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - " . $stok_awal['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $p_stok_keluar =
                " - " . $stok_keluar['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - " . $stok_keluar['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - " . $stok_keluar['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $p_stok_akhir =
                " - " . $stok_akhir['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - " . $stok_akhir['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - " . $stok_akhir['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $p_penyesuaian =
                " - (" . $penyesuaian['p1'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - (" . $penyesuaian['p2'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - (" . $penyesuaian['p3'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $obat[$value['obat_id']]['nama'];
            $row[] = $jenis_barang[$obat[$value['obat_id']]['jenis_barang_id']]['nama'];
            $row[] = isset($batch[$gudang_utama[$value['gudang_utama_id']]['batch_id']]['exp_date']) ? date('d - m - Y', strtotime($batch[$gudang_utama[$value['gudang_utama_id']]['batch_id']]['exp_date'])) : "[Data deleted]";
            $row[] = $p_stok_awal;
            $row[] = $p_stok_keluar;
            $row[] = $p_stok_akhir;
            $row[] = $p_penyesuaian;
            $row[] = $obat[$value['obat_id']]['keterangan'];
            $row[] = date('d - m - Y', strtotime($value['updated_at']));
//            $row[] = '<button class="btn btn-danger btn-circle" onclick="hapusBarangMasuk('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
//            $row[] = '<button class="btn btn-info" data-toggle="modal" data-target="#modal_penyesuaian" data-backdrop="static" data-keyboard="false" title="klik untuk penyesuaian" onclick="penyesuaian(' . "'" . $value['id'] . "'" . ')"> Penyesuaian </button>';
            //add html for action
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    function convert_to_number($rupiah){
        return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function ajax_stok() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_stok_gudang_utama, $id);
        $data['obat'] = $this->Models->show($this->table_obat, $data['data']['obat_id']);
        $data['persediaan'] = $this->Models->show($this->table_persediaan, $data['obat']['persediaan_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail Stok Gudang Utama ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function do_penyesuaian() {
        // form validate
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_gudang', "GUDANG ID", 'required|trim');
        $this->form_validation->set_rules('id_stok', 'STOK ID', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "farmasi_barang_masuk_view";
            $comments = "Gagal input penyesuaian dengan error sebagai berikut = '" . validation_errors('', "\n") . "'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else if ($this->form_validation->run() == TRUE) {
            $id_stok = $this->input->post('id_stok', TRUE);

            $stok_gudang = $this->Models->show($this->table_stok_gudang_utama, $id_stok);
            $obat = $this->Models->show($this->table_obat, $stok_gudang['obat_id']);

            $jml_1 = $this->input->post('penyesuaian_1', TRUE);
            $jml_2 = $this->input->post('penyesuaian_2', TRUE);
            $jml_3 = $this->input->post('penyesuaian_3', TRUE);

            $persediaan_1 = $jml_1 * ($obat['jumlah_persediaan_2'] * $obat['jumlah_persediaan_3']);
            $persediaan_2 = $jml_2 * $obat['jumlah_persediaan_3'];
            $persediaan_3 = $jml_3;

            $penyesuaian_stok = $persediaan_1 + $persediaan_2 + $persediaan_3;

            $data = array(
                'stok_akhir' => $stok_gudang['stok_akhir'] + $penyesuaian_stok,
                'penyesuaian' => $penyesuaian_stok
            );

//            echo "<pre>";
//            print_r($data);

            if ($this->Models->update($this->table_stok_gudang_utama, $data, $id_stok)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Penyesuaian berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms       = "farmasi_barang_masuk_view";
                $comments    = "Berhasil menambahkan Penyesuaian dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        } else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Barang Masuk, hubungi web administrator.');

            // if permitted, do logit
            $perms = "farmasi_barang_masuk_view";
            $comments = "Gagal menambahkan Barang Masuk dengan data berikut = '" . json_encode($_REQUEST) . "'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }

    public function do_create() {
        // form validate
        $this->load->library('form_validation');
        $this->form_validation->set_rules('batch_id', 'Batch Number', 'required|trim');
        $this->form_validation->set_rules('obat_id', 'Nama Obat', 'required|trim');
        $this->form_validation->set_rules('jenis_barang_id', 'Jenis Barang', 'required|trim');
        $this->form_validation->set_rules('suplier_id', 'Suplier', 'required|trim');
        $this->form_validation->set_rules('total', 'Total', 'required|trim');
        $this->form_validation->set_rules('status_bayar', 'Status Bayar', 'required|trim');
        $this->form_validation->set_rules('tanggal_lunas', 'Tanggal Pelunasan', 'required|trim');
        $this->form_validation->set_rules('tanggal_barang_masuk', 'Tanggal Barang Masuk', 'required|trim');
        $this->form_validation->set_rules('no_po', 'Nomor PO', 'required|trim');
        $this->form_validation->set_rules('no_invoice', 'Nomor Invoice', 'required|trim');

        $obat = $this->Models->show($this->table_obat, $this->input->post('obat_id', TRUE));

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "farmasi_barang_masuk_view";
            $comments = "Gagal input Barang Masuk pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else if ($this->form_validation->run() == TRUE)  {
            $data = array (
                'tanggal_barang_masuk'  => date('Y-m-d', strtotime($this->input->post('tanggal_barang_masuk', TRUE))),
                'no_po'                 => $this->input->post('no_po', TRUE),
                'no_invoice'            => $this->input->post('no_invoice', TRUE),
                'jml_brg_persediaan_1'  => $this->input->post('jml_brg_persediaan_1', TRUE),
                'jml_brg_persediaan_2'  => $this->input->post('jml_brg_persediaan_2', TRUE),
                'jml_brg_persediaan_3'  => $this->input->post('jml_brg_persediaan_3', TRUE),
                'harga_persediaan_1'    => $this->input->post('harga_persediaan_1', TRUE),
                'harga_persediaan_2'    => $this->input->post('harga_persediaan_2', TRUE),
                'harga_persediaan_3'    => $this->input->post('harga_persediaan_3', TRUE),
                'total'                 => $this->input->post('total', TRUE),
                'tanggal_lunas'         => date('Y-m-d', strtotime($this->input->post('tanggal_lunas', TRUE))),
                'status_bayar'          => $this->input->post('status_bayar', TRUE),
                'keterangan'            => $this->input->post('keterangan', TRUE),
                'batch_id'              => $this->input->post('batch_id', TRUE),
                'suplier_id'            => $this->input->post('suplier_id', TRUE),
                'obat_id'               => $this->input->post('obat_id', TRUE)
            );

            if($this->Models->insert($this->table_gudang_utama, $data)){
                $insert_id = $this->db->insert_id();
                $stok = ($data['jml_brg_persediaan_1'] * $obat['jumlah_persediaan_3']) + ($data['jml_brg_persediaan_2'] * ($obat['jumlah_persediaan_3'] / $obat['jumlah_persediaan_2'])) + $data['jml_brg_persediaan_3'];

                $data_stok = array (
                    'stok_awal' => $stok,
                    'stok_farmasi' => 0,
                    'stok_akhir' => $stok,
                    'keterangan' => $data['keterangan'],
                    'obat_id' => $obat['id'],
                    'gudang_utama_id' => $insert_id
                );

                $this->Models->insert($this->table_stok_gudang_utama, $data_stok);

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Barang Masuk berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms       = "farmasi_barang_masuk_view";
                $comments    = "Berhasil menambahkan Barang Masuk dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Gagal menambahkan Barang Masuk, hubungi web administrator.');

                // if permitted, do logit
                $perms = "farmasi_barang_masuk_view";
                $comments = "Gagal menambahkan Barang Masuk dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function do_delete_barang_masuk(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_masuk_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_barang_masuk = $this->input->post('id_barang_masuk', TRUE);

        $delete = $this->Barang_masuk_model->delete_barang_masuk($id_barang_masuk);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => true,
                'messages'      => 'Barang Masuk Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_masuk_delete";
            $comments = "Berhasil membatalkan Barang Masuk Pasien dengan id Barang Masuk Pasien Operasi = '". $id_barang_masuk ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_masuk_delete";
            $comments = "Gagal membatalkan data Barang Masuk Pasien dengan ID = '". $id_barang_masuk ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_detail_batch () {
        $id = $this->input->get('id', TRUE);
        $data['batch'] = $this->Models->show($this->table_batch, $id);
        $data['obat'] = $this->Models->show($this->table_obat, $data['batch']['obat_id']);
        $data['persediaan'] = $this->Models->show($this->table_persediaan, $data['obat']['persediaan_id']);
        $data['jenis_barang'] = $this->Models->show($this->table_jenis_barang, $data['obat']['jenis_barang_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail batch ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function ajax_detail_harga () {
        $wheres = array(
            'batch_id' => $this->input->get('batch_id', TRUE),
            'suplier_id' => $this->input->get('suplier_id', TRUE),
            'obat_id' => $this->input->get('obat_id', TRUE)
        );

        if (count($this->Models->where($this->table_harga_beli, $wheres)) == 1) {
            $data['data'] = $this->Models->where($this->table_harga_beli, $wheres)[0];

            $output = array(
                "success" => true,
                "message" => "Data Detail batch ",
                "data" => $data
            );
        } else {
            $output = array(
                "success" => false,
                "message" => "Data Detail batch ",
                "data" => "Data harga beli tidak ditemukan"
            );
        }

        echo json_encode($output);
    }

    public function ajax_list_daftar_barang(){
        $list = $this->Models->all($this->table_obat);
        $jenis_barang = $this->Models->all($this->table_jenis_barang);
        $persediaan = $this->Models->all($this->table_persediaan);

        $data = array();

        foreach($list as $key => $value){
            $row   = array();
            $row[] = $value['nama'];
            $row[] = $jenis_barang[$value['jenis_barang_id']]['nama'];
            $row[] = $persediaan[$value['persediaan_id']]['persediaan_1'] . ", " . $persediaan[$value['persediaan_id']]['persediaan_2'] . ", " . $persediaan[$value['persediaan_id']]['persediaan_3'];
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Barang Masuk" onclick="pilihBarang('."'".$value['id']."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw"              => $this->input->get('draw'),
            "recordsTotal"      => count($list),
            "recordsFiltered"   => count($list),
            "data"              => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_daftar_barang_by_id(){
        $kode_barang = $this->input->get('kode_barang',TRUE);
        $data_daftar_barang = $this->Barang_masuk_model->get_daftar_barang_by_id($kode_barang);
        if (sizeof($data_daftar_barang)>0){
            $daftar_barang = $data_daftar_barang[0];
            $res = array(
                "success" => true,
                "messages" => "Data Barang Masuk ditemukan",
                "data" => $daftar_barang
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data Barang Masuk tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }

    public function print_data() {
        $query = "SELECT id, max(updated_at) AS updated_at, obat_id, SUM(stok_awal) AS stok_awal, SUM(stok_farmasi) AS stok_keluar, SUM(stok_akhir) AS stok_akhir, SUM(penyesuaian) AS penyesuaian FROM " . $this->table_stok_gudang_utama . " GROUP BY obat_id";
        $list = $this->Models->query($query);
        $obat = $this->Models->all($this->table_obat);
        $jenis_barang = $this->Models->all($this->table_jenis_barang);
        $persediaan = $this->Models->all($this->table_persediaan);

        $data = array();
        $no   = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value){
            $stok_awal = $this->convert_stok($value['stok_awal'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $stok_keluar = $this->convert_stok($value['stok_keluar'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $stok_akhir = $this->convert_stok($value['stok_akhir'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
            $penyesuaian = $this->convert_stok($value['penyesuaian'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);

            $p_stok_awal =
                $stok_awal['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . ", " .
                $stok_awal['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . ", " .
                $stok_awal['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] ;

            $p_stok_keluar =
                $stok_keluar['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . ", " .
                $stok_keluar['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . ", " .
                $stok_keluar['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'];

            $p_stok_akhir =
                $stok_akhir['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . ", " .
                $stok_akhir['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . ", " .
                $stok_akhir['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'];

            $p_penyesuaian =
                "(" . $penyesuaian['p1'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . ", " .
                "(" . $penyesuaian['p2'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . ", " .
                "(" . $penyesuaian['p3'] . ") " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'];

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $obat[$value['obat_id']]['nama'];
            $row[] = $jenis_barang[$obat[$value['obat_id']]['jenis_barang_id']]['nama'];
            $row[] = $p_stok_awal;
            $row[] = $p_stok_keluar;
            $row[] = $p_stok_akhir;
            $row[] = $p_penyesuaian;
            $row[] = $obat[$value['obat_id']]['keterangan'];
            $row[] = date('d - m - Y', strtotime($value['updated_at']));
//            $row[] = '<button class="btn btn-danger btn-circle" onclick="hapusBarangMasuk('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
//            $row[] = '<button class="btn btn-info" data-toggle="modal" data-target="#modal_penyesuaian" data-backdrop="static" data-keyboard="false" title="klik untuk penyesuaian" onclick="penyesuaian('."'".$value['id']."'".')"> Penyesuaian </button>';
            //add html for action
            $data[] = $row;
        }

//        echo "<pre>";
//        print_r($data);
//
//        foreach($data as $key => $value) {
//            for ($i=0; $i<=8; $i++) {
//                echo $value[$i];
//            }
//        }

        return $data;
    }

    function exportToExcel() {
        // $tgl_awal        = $this->input->get('tgl_awal',TRUE);
        // $tgl_akhir       = $this->input->get('tgl_akhir',TRUE);
        //  if ($tgl_awal == "undefined"){ $tgl_awal = ""; } else { $tgl_awal = str_replace("%20"," ",$tgl_awal); }
//        if ($tgl_akhir == "undefined"){ $tgl_akhir = ""; } else { $tgl_akhir = str_replace("%20"," ",$tgl_akhir); }
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Stok Gudang Utama");
        $object->getActiveSheet()
            ->getStyle("A1:I1")
            ->getFont()
            ->setSize(14)
            ->setBold(true)
            ->getColor()
            ->setRGB('FFFFFF');
        $object->getActiveSheet()
            ->getStyle("A1:I1")
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('01c0c8');

        $table_columns = array("No", "Nama Barang","Jenis Barang","Stok Masuk","Stok Keluar","Stok Akhir","Penyesuaian","Keterangan","Perubahan Terakhir");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $data = $this->print_data();

        $excel_row = 2;

        foreach($data as $key => $value) {
            for ($i=0; $i<=8; $i++) {
                $object->getActiveSheet()->setCellValueByColumnAndRow($i, $excel_row, $value[$i]);
            }
            $excel_row++;
        }

        foreach(range('A','I') as $columnID) {
            $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Stok Gudang Utama.xlsx"');
        $object_writer->save('php://output');
    }

}
