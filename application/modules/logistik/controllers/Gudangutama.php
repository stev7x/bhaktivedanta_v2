<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudangutama extends CI_Controller {

    public $table_obat = "farmasi_obat";
    public $table_persediaan = "farmasi_persediaan";
    public $table_jenis_barang = "farmasi_jenis_barang";
    public $table_suplier = "farmasi_suplier";
    public $table_batch = "farmasi_batch";
    public $table_harga_beli = "farmasi_harga_obat";

    public $table_gudang_utama = "logistik_gudang_utama";
    public $table_stok_gudang_utama = "logistik_stok_gudang_utama";
    public $table_gudang_farmasi = "logistik_gudang_farmasi";
    public $table_stok_gudang_farmasi = "logistik_stok_gudang_farmasi";


    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('Models');
        $this->load->model('Barang_masuk_model');

        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_masuk_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        $this->data['batch'] = $this->Models->all($this->table_batch);
        $this->data['suplier'] = $this->Models->all($this->table_suplier);

        // if permitted, do logit
        $perms = "";
        $comments = "Gudang Utama";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('gudang_utama', $this->data);
    }

    function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }
    
    public function ajax_list() {
        $list = $this->Models->where($this->table_gudang_utama, array('branch' => $this->session->tempdata('data_session')[2]));
        $obat = $this->Models->all($this->table_obat);
        $jenis_barang = $this->Models->all($this->table_jenis_barang);
        $persediaan = $this->Models->all($this->table_persediaan);
        $suplier = $this->Models->all($this->table_suplier);
        $batch = $this->Models->all($this->table_batch);

        $data = array();
        $no   = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value){
            $detail =
                "Jenis : " . $jenis_barang[$obat[$value['obat_id']]['jenis_barang_id']]['nama'] . "<br>" .
                "Persediaan : <br>" .
                " - " . $value['jml_brg_persediaan_1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                " - " . $value['jml_brg_persediaan_2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                " - " . $value['jml_brg_persediaan_3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d - m - Y', strtotime($value['tanggal_barang_masuk']));
            $row[] = isset($batch[$value['batch_id']]['number']) ? $batch[$value['batch_id']]['number'] : "[Data deleted]";
            $row[] = isset($obat[$value['obat_id']]['nama']) ? $obat[$value['obat_id']]['nama'] : "[Data deleted]";
            $row[] = $detail;
            $row[] = $this->convert_to_rupiah($value['total']) . ",00";
            $row[] = isset($suplier[$value['suplier_id']]['nama']) ? $suplier[$value['suplier_id']]['nama'] : "[Data deleted]";
            $row[] = isset($batch[$value['batch_id']]['number']) ? date('d - m - Y', strtotime($batch[$value['batch_id']]['exp_date'])) : "[Data deleted]";
            $row[] = $value['no_po'];
            $row[] = $value['no_invoice'];
            $row[] = $value['status_bayar'] == 0 ? "BELUM LUNAS" : "LUNAS";
            $row[] = $value['status_bayar'] == 0 ? "-" : date('d - m - Y', strtotime($value['tanggal_lunas']));
            $row[] = date('d - m - Y', strtotime($value['tanggal_jatuh_tempo']));
            $row[] = $value['keterangan'];
//            $row[] = '<button class="btn btn-danger btn-circle" onclick="hapusBarangMasuk('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
            $row[] = '<button class="btn btn-warning" data-toggle="modal" data-target="#modal_bayar" data-backdrop="static" data-keyboard="false" title="klik untuk bayar" onclick="pay('."'".$value['id']."'".')" ' . ($value['status_bayar'] == 0 ? "" : "disabled") . '> Bayar </button>
                      <button class="btn btn-info" data-toggle="modal" data-target="#modal_tf" data-backdrop="static" data-keyboard="false" title="klik untuk transfer"  onclick="toFarmasi('."'".$value['id']."'".')"> Transfer </button>';
            //add html for action
            $data[] = $row;
        }

        $output = array(
            "draw"            => $this->input->get('draw'),
            "recordsTotal"    => count($list),
            "recordsFiltered" => count($list),
            "data"            => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_gudang_utama, $id);

        $output = array(
            "success" => true,
            "message" => "Data Detail Gudang Utama ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function ajax_stok() {
        $id_gudang_utama = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->where($this->table_stok_gudang_utama, array('gudang_utama_id' => $id_gudang_utama))[0];
        $data['obat'] = $this->Models->show($this->table_obat, $data['data']['obat_id']);
        $data['persediaan'] = $this->Models->show($this->table_persediaan, $data['obat']['persediaan_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail Stok Gudang Utama ",
            "data" => $data
        );

        echo json_encode($output);
    }

    function convert_to_number($rupiah){
        return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function do_create() {
        // form validate
        $this->load->library('form_validation');
        $this->form_validation->set_rules('batch_id', 'Batch Number', 'required|trim');
        $this->form_validation->set_rules('obat_id', 'Nama Obat', 'required|trim');
        $this->form_validation->set_rules('jenis_barang_id', 'Jenis Barang', 'required|trim');
        $this->form_validation->set_rules('suplier_id', 'Suplier', 'required|trim');
        $this->form_validation->set_rules('total', 'Total', 'required|trim');
        $this->form_validation->set_rules('status_bayar', 'Status Bayar', 'required|trim');
//        $this->form_validation->set_rules('tanggal_lunas', 'Tanggal Pelunasan', 'required|trim');
//        $this->form_validation->set_rules('tanggal_barang_masuk', 'Tanggal Barang Masuk', 'required|trim');
//        $this->form_validation->set_rules('tanggal_jatuh_tempo', 'Tanggal Jatuh Tempo', 'required|trim');
//        $this->form_validation->set_rules('no_po', 'Nomor PO', 'required|trim');
//        $this->form_validation->set_rules('no_invoice', 'Nomor Invoice', 'required|trim');

        $obat = $this->Models->show($this->table_obat, $this->input->post('obat_id', TRUE));

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "farmasi_barang_masuk_view";
            $comments = "Gagal input Barang Masuk pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else if ($this->form_validation->run() == TRUE)  {
            $data = array (
                'tanggal_barang_masuk'  => date('Y-m-d', strtotime($this->input->post('tanggal_barang_masuk', TRUE))),
                'no_po'                 => $this->input->post('no_po', TRUE),
                'no_invoice'            => $this->input->post('no_invoice', TRUE),
                'jml_brg_persediaan_1'  => $this->input->post('jml_brg_persediaan_1', TRUE),
                'jml_brg_persediaan_2'  => $this->input->post('jml_brg_persediaan_2', TRUE),
                'jml_brg_persediaan_3'  => $this->input->post('jml_brg_persediaan_3', TRUE),
                'harga_persediaan_1'    => $this->input->post('harga_persediaan_1', TRUE),
                'harga_persediaan_2'    => $this->input->post('harga_persediaan_2', TRUE),
                'harga_persediaan_3'    => $this->input->post('harga_persediaan_3', TRUE),
                'total'                 => $this->input->post('total', TRUE),
                'tanggal_lunas'         => date('Y-m-d', strtotime($this->input->post('tanggal_lunas', TRUE))),
                'tanggal_jatuh_tempo'   => date('Y-m-d', strtotime($this->input->post('tanggal_jatuh_tempo', TRUE))),
//                'status_bayar'          => $this->input->post('status_bayar', TRUE),
                'status_bayar'          => 1,
                'keterangan'            => $this->input->post('keterangan', TRUE),
                'batch_id'              => $this->input->post('batch_id', TRUE),
                'suplier_id'            => $this->input->post('suplier_id', TRUE),
                'obat_id'               => $this->input->post('obat_id', TRUE),
                'branch'                => $this->session->tempdata('data_session')[2]
            );

            if($this->Models->insert($this->table_gudang_utama, $data)){
                $insert_id = $this->db->insert_id();
                $persediaan_1 = $data['jml_brg_persediaan_1'] * ($obat['jumlah_persediaan_2'] * $obat['jumlah_persediaan_3']);
                $persediaan_2 = $data['jml_brg_persediaan_2'] * $obat['jumlah_persediaan_3'];
                $persediaan_3 = $data['jml_brg_persediaan_3'];

                $data_stok = array (
                    'stok_awal'         => $persediaan_1 + $persediaan_2 + $persediaan_3,
                    'stok_farmasi'      => 0,
                    'stok_akhir'        => $persediaan_1 + $persediaan_2 + $persediaan_3,
                    'penyesuaian'       => 0,
                    'keterangan'        => $obat['keterangan'],
                    'obat_id'           => $obat['id'],
                    'gudang_utama_id'   => $insert_id,
                    'branch'            => $this->session->tempdata('data_session')[2]
                );

                $this->Models->insert($this->table_stok_gudang_utama, $data_stok);

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Barang Masuk berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms       = "farmasi_barang_masuk_view";
                $comments    = "Berhasil menambahkan Barang Masuk dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Gagal menambahkan Barang Masuk, hubungi web administrator.');

                // if permitted, do logit
                $perms = "farmasi_barang_masuk_view";
                $comments = "Gagal menambahkan Barang Masuk dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function do_pay () {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'ID', 'required|trim');
        $this->form_validation->set_rules('status_bayar', 'Status Bayar', 'required|trim');
        $this->form_validation->set_rules('tanggal_lunas', 'Tanggal Pelunasan', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Pembayaran gagal dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id = $this->input->post('id', TRUE);
            $data = array(
                'tanggal_lunas' => date('Y-m-d', strtotime($this->input->post('tanggal_lunas', TRUE))),
                'status_bayar'  => $this->input->post('status_bayar', TRUE),
                'keterangan'    => $this->input->post('keterangan', TRUE)
            );

            if ($this->Models->update($this->table_gudang_utama, $data, $id)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Pembayaran berhasil diperbarui'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Pembayaran berhasil dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Pembayaran gagal, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Pembayaran gagal dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function cekdup($table, $wheres=array()) {
        if (count($this->Models->where($table, $wheres)) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function do_transfer () {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_gudang', 'ID Gudang', 'required|trim');
        $this->form_validation->set_rules('id_stok', 'ID Stok', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Transfer Barang gagal dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id_gudang = $this->input->post('id_gudang', TRUE);
            $id_stok = $this->input->post('id_stok', TRUE);

            $stok_gudang = $this->Models->show($this->table_stok_gudang_utama, $id_stok);
            $obat = $this->Models->show($this->table_obat, $stok_gudang['obat_id']);

            $jml_1 = $this->input->post('jml_brg_persediaan_1', TRUE);
            $jml_2 = $this->input->post('jml_brg_persediaan_2', TRUE);
            $jml_3 = $this->input->post('jml_brg_persediaan_3', TRUE);

            $persediaan_1 = $jml_1 * ($obat['jumlah_persediaan_2'] * $obat['jumlah_persediaan_3']);
            $persediaan_2 = $jml_2 * $obat['jumlah_persediaan_3'];
            $persediaan_3 = $jml_3;

            $tf_stok = $persediaan_1 + $persediaan_2 + $persediaan_3;

            $data_stok = array(
                'stok_farmasi'  => $stok_gudang['stok_farmasi'] + $tf_stok,
                'stok_akhir'    => $stok_gudang['stok_akhir'] - $tf_stok
            );

            if ($data_stok['stok_akhir'] < 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Jumlah transfer tidak boleh melebihi jumlah stok saat ini');

                // if permitted, do logit
                $perms = "";
                $comments = "Transfer Barang gagal dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);

                echo json_encode($res);
                exit;
            }

            if ($this->Models->update($this->table_stok_gudang_utama, $data_stok, $id_stok)) {
                $cek['gudang_utama_id'] =  $id_gudang;

                if ($this->cekdup($this->table_gudang_farmasi, $cek)) {
                    $data_gudang_farmasi = array (
                        'tanggal_transfer'       => date('Y-m-d'),
                        'jml_brg_persediaan_1'  => $jml_1,
                        'jml_brg_persediaan_2'  => $jml_2,
                        'jml_brg_persediaan_3'  => $jml_3,
                        'keterangan'            => $stok_gudang['keterangan'],
                        'gudang_utama_id'       => $stok_gudang['id'],
                        'branch'                => $this->session->tempdata('data_session')[2]
                    );

                    if ($this->Models->insert($this->table_gudang_farmasi, $data_gudang_farmasi)) {
                        $gudang_farmasi_id = $this->db->insert_id();
                        $data_stok_gudang_farmasi = array (
                            'stok_awal'         => $tf_stok,
                            'stok_keluar'       => 0,
                            'stok_akhir'        => $tf_stok,
                            'penyesuaian'       => 0,
                            'keterangan'        => $stok_gudang['keterangan'],
                            'obat_id'           => $obat['id'],
                            'gudang_farmasi_id' => $gudang_farmasi_id,
                            'branch'            => $this->session->tempdata('data_session')[2]
                        );

                        if ($this->Models->insert($this->table_stok_gudang_farmasi, $data_stok_gudang_farmasi)) {
                            $res = array(
                                'csrfTokenName' => $this->security->get_csrf_token_name(),
                                'csrfHash' => $this->security->get_csrf_hash(),
                                'success' => true,
                                'messages' => 'Transfer Barang berhasil'
                            );

                            // if permitted, do logit
                            $perms = "";
                            $comments = "Transfer Barang berhasil dengan data berikut = '". json_encode($_REQUEST) ."'.";
                            $this->aauth->logit($perms, current_url(), $comments);
                        }
                    }
                } else {
                    $data_gudang_farmasi = $this->Models->where($this->table_gudang_farmasi, $cek)[0];
                    $data_update_gudang_farmasi = array (
                        'tanggal_transfer'       => date('Y-m-d'),
                        'jml_brg_persediaan_1'  => $data_gudang_farmasi['jml_brg_persediaan_1'] + $jml_1,
                        'jml_brg_persediaan_2'  => $data_gudang_farmasi['jml_brg_persediaan_2'] + $jml_2,
                        'jml_brg_persediaan_3'  => $data_gudang_farmasi['jml_brg_persediaan_3'] + $jml_3,
                    );

                    if ($this->Models->update($this->table_gudang_farmasi, $data_update_gudang_farmasi, $data_gudang_farmasi['id'])) {
                        $wheres['gudang_farmasi_id'] = $data_gudang_farmasi['id'];
                        $data_stok_gudang_farmasi = $this->Models->where($this->table_stok_gudang_farmasi, $wheres)[0];
                        $data_update_stok_gudang_farmasi = array (
                            'stok_awal'         => $data_stok_gudang_farmasi['stok_awal'] + $tf_stok,
                            'stok_akhir'        => $data_stok_gudang_farmasi['stok_akhir'] + $tf_stok
                        );

                        if ($this->Models->update($this->table_stok_gudang_farmasi, $data_update_stok_gudang_farmasi, $data_stok_gudang_farmasi['id'])) {
                            $res = array(
                                'csrfTokenName' => $this->security->get_csrf_token_name(),
                                'csrfHash' => $this->security->get_csrf_hash(),
                                'success' => true,
                                'messages' => 'Transfer Barang berhasil'
                            );

                            // if permitted, do logit
                            $perms = "";
                            $comments = "Transfer Barang berhasil dengan data berikut = '". json_encode($_REQUEST) ."'.";
                            $this->aauth->logit($perms, current_url(), $comments);
                        }
                    }
                }
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Transfer Barang gagal, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Transfer Barang gagal dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function do_delete_barang_masuk(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_masuk_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_barang_masuk = $this->input->post('id_barang_masuk', TRUE);

        $delete = $this->Barang_masuk_model->delete_barang_masuk($id_barang_masuk);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => true,
                'messages'      => 'Barang Masuk Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_masuk_delete";
            $comments = "Berhasil membatalkan Barang Masuk Pasien dengan id Barang Masuk Pasien Operasi = '". $id_barang_masuk ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_masuk_delete";
            $comments = "Gagal membatalkan data Barang Masuk Pasien dengan ID = '". $id_barang_masuk ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_detail_batch () {
        $id = $this->input->get('id', TRUE);
        $data['batch'] = $this->Models->show($this->table_batch, $id);
        $data['obat'] = $this->Models->show($this->table_obat, $data['batch']['obat_id']);
        $data['persediaan'] = $this->Models->show($this->table_persediaan, $data['obat']['persediaan_id']);
        $data['jenis_barang'] = $this->Models->show($this->table_jenis_barang, $data['obat']['jenis_barang_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail batch ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function ajax_detail_harga () {
        $wheres = array(
            'batch_id' => $this->input->get('batch_id', TRUE),
            'suplier_id' => $this->input->get('suplier_id', TRUE),
            'obat_id' => $this->input->get('obat_id', TRUE)
        );

        if (count($this->Models->where($this->table_harga_beli, $wheres)) == 1) {
            $data['data'] = $this->Models->where($this->table_harga_beli, $wheres)[0];

            $output = array(
                "success" => true,
                "message" => "Data Detail batch ",
                "data" => $data
            );
        } else {
            $output = array(
                "success" => false,
                "message" => "Data Detail batch ",
                "data" => "Data harga beli tidak ditemukan"
            );
        }

        echo json_encode($output);
    }

    public function ajax_list_daftar_barang(){
        $list = $this->Models->all($this->table_obat);
        $jenis_barang = $this->Models->all($this->table_jenis_barang);
        $persediaan = $this->Models->all($this->table_persediaan);

        $data = array();

        foreach($list as $key => $value){
            $row   = array();
            $row[] = $value['nama'];
            $row[] = $jenis_barang[$value['jenis_barang_id']]['nama'];
            $row[] = $persediaan[$value['persediaan_id']]['persediaan_1'] . ", " . $persediaan[$value['persediaan_id']]['persediaan_2'] . ", " . $persediaan[$value['persediaan_id']]['persediaan_3'];
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Barang Masuk" onclick="pilihBarang('."'".$value['id']."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw"              => $this->input->get('draw'),
            "recordsTotal"      => count($list),
            "recordsFiltered"   => count($list),
            "data"              => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_daftar_barang_by_id(){
        $kode_barang = $this->input->get('kode_barang',TRUE);
        $data_daftar_barang = $this->Barang_masuk_model->get_daftar_barang_by_id($kode_barang);
        if (sizeof($data_daftar_barang)>0){
            $daftar_barang = $data_daftar_barang[0];
            $res = array(
                "success" => true,
                "messages" => "Data Barang Masuk ditemukan",
                "data" => $daftar_barang
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data Barang Masuk tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }

    public function data_print() {
//        $tgl_awal        = $this->input->get('tgl_awal',TRUE);
//        $tgl_akhir       = $this->input->get('tgl_akhir',TRUE);
//        $kode_barang     = $this->input->get('kode_barang',TRUE);
//        $nama_barang     = $this->input->get('nama_barang',TRUE);
//        $jenis_barang    = $this->input->get('jenis_barang',TRUE);
//        $nama_supplier   = $this->input->get('nama_supplier',TRUE);
//        $list = $this->Barang_masuk_model->get_barang_masuk_list($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier);
        // $list   = $this->Barang_masuk_model->get_barang_masuk_list();

        $list = $this->Models->all($this->table_gudang_utama);
        $obat = $this->Models->all($this->table_obat);
        $jenis_barang = $this->Models->all($this->table_jenis_barang);
        $persediaan = $this->Models->all($this->table_persediaan);
        $suplier = $this->Models->all($this->table_suplier);
        $batch = $this->Models->all($this->table_batch);

        $data = array();
        $no   = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value){
            $detail =
                $value['jml_brg_persediaan_1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . ", " .
                $value['jml_brg_persediaan_2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . ", " .
                $value['jml_brg_persediaan_3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'];

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d - m - Y', strtotime($value['tanggal_barang_masuk']));
            $row[] = $batch[$value['batch_id']]['number'];
            $row[] = $obat[$value['obat_id']]['nama'];
            $row[] = $jenis_barang[$obat[$value['obat_id']]['jenis_barang_id']]['nama'];
            $row[] = $detail == "" ? "-" : $detail;
            $row[] = $this->convert_to_rupiah($value['total']) . ",00";
            $row[] = $suplier[$value['suplier_id']]['nama'];
            $row[] = date('d - m - Y', strtotime($batch[$value['batch_id']]['exp_date']));
            $row[] = $value['no_po'];
            $row[] = $value['no_invoice'];
            $row[] = $value['status_bayar'] == 0 ? "BELUM LUNAS" : "LUNAS";
            $row[] = $value['status_bayar'] == 0 ? "-" : date('d - m - Y', strtotime($value['tanggal_lunas']));
            $row[] = date('d - m - Y', strtotime($value['tanggal_jatuh_tempo']));
            $row[] = $value['keterangan'] == "" ? "-" : $value['keterangan'];
            //add html for action
            $data[] = $row;
        }

//        echo "<pre>";
//        print_r($data);

//        foreach ($data as $key => $value) {
//            for ($i=0; $i<=8; $i++) {
//                echo $value[$i];
//            }
//        }

        return $data;
    }

    function exportToExcel() {
        // $tgl_awal        = $this->input->get('tgl_awal',TRUE);
        // $tgl_akhir       = $this->input->get('tgl_akhir',TRUE);
        //  if ($tgl_awal == "undefined"){ $tgl_awal = ""; } else { $tgl_awal = str_replace("%20"," ",$tgl_awal); }
//        if ($tgl_akhir == "undefined"){ $tgl_akhir = ""; } else { $tgl_akhir = str_replace("%20"," ",$tgl_akhir); }
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Gudang Utama");
        $object->getActiveSheet()
            ->getStyle("A1:O1")
            ->getFont()
            ->setSize(14)
            ->setBold(true)
            ->getColor()
            ->setRGB('FFFFFF');
        $object->getActiveSheet()
            ->getStyle("A1:O1")
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('01c0c8');

        $table_columns = array("No", "Tanggal Barang Masuk","Batch Number","Nama Barang", "jenis Barang","Persediaan Barang","Harga Total","Nama Suplier","Kadaluarsa","Nomor PO", "Nomor Invoice", "Status Pembayaran", "Tanggal Pelunasan", "Tanggal Jatuh Tempo", "Keterangan");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $data = $this->data_print();
//        echo "<pre>";
//        print_r($data);
//        exit;

        $excel_row = 2;
        foreach($data as $key => $value) {
            for ($i=0; $i<=14; $i++) {
                $object->getActiveSheet()->setCellValueByColumnAndRow($i, $excel_row, $value[$i]);
            }
            $excel_row++;
        }

        foreach(range('A','O') as $columnID) {
            $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Gudang Utama.xlsx"');
        $object_writer->save('php://output');
    }

}
