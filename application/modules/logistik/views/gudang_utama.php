<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
<!-- Page Content -->

<style>
    .box_form {
        border : 1px solid #e4e7ea;
        padding: 16px;
        margin-bottom: 10px;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Logistik  -  Gudang Utama</h4> </div>
            <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                <ol class="breadcrumb">
                    <li><a href="index.html">Logistik</a></li>
                    <li class="active">Gudang Utama</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Data Gudang Utama
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="inputName1" class="control-label"></label>
                                    <dl class="text-right">
                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                        <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>
                                    </dl>
                                </div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label>Tanggal Transaksi, Dari</label>
                                            <div class="input-group">
                                                <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label>Sampai</label>
                                            <div class="input-group">
                                                <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>&nbsp;</label><br>
                                            <button type="button" class="btn btn-success col-md-12" onclick="cariGudangUtama()">Cari</button>
                                        </div>
                                        <div class="form-group col-md-3" style="margin-top: 7px">
                                            <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                            <b>
                                                <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                    <option value=""  selected>Pilih Berdasarkan</option>
                                                    <option value="kode_barang">Kode Barang</option>
                                                    <option value="nama_barang">Nama Barang</option>
                                                    <option value="jenis_barang">Jenis Barang</option>
                                                    <option value="nama_supplier">Supplier</option>
                                                    <!-- <option value="status">Status</option>    -->
                                                </select></b>

                                        </div>
                                        <div class="form-group col-md-9" style="margin-top: 7px">
                                            <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                            <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariGudangUtama()" >
                                        </div>
                                    </div>
                                </div>
                            </div><br><hr style="margin-top: -27px">
                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_masuk_barang" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus p-r-10"></i>Tambah Barang Masuk</button>
                                &nbsp;
                                <button type="button" class="btn btn-warning" id="exportToExcel" onclick="exportToExcel()" ><i class="fa fa-cloud-upload p-r-10"></i>Export To Excel</button>
                            </div>
                            
                            <div class="table">
                                <table id="table_gudang_utama_list" class="table table-responsive table-striped dataTable" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Barang Masuk</th>
                                        <th>Batch Number</th>
                                        <th>Nama Barang</th>
                                        <th>Detail Barang</th>
                                        <th>Harga Total</th>
                                        <th>Nama Suplier</th>
                                        <th>Kadaluarsa</th>
                                        <th>No. PO</th>
                                        <th>No. Invoice</th>
                                        <th>Status Pembayaran</th>
                                        <th>Tanggal Pelunasan</th>
                                        <th>Tanggal Jatuh Tempo</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="4">No Data to Display</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->
    </div>
    <!-- /.container-fluid -->

    <div id="modal_add_masuk_barang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
        <div class="modal-dialog modal-large">
            <?php echo form_open('#',array('id' => 'fmCreateGudangUtama'))?>
            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

            <div class="modal-content" style="overflow:auto;height:630px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b>Tambah Barang Masuk</b></h2>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                <div>
                                    <p id="card_message" class=""></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="batch_id">Batch Number <span style="color: red;">*</span></label>
                                <select class="form-control select2" name="batch_id" id="batch_id" onchange="setBatch(this);">
                                    <option value="" selected>Pilih Batch Number</option>
                                    <?php
                                        foreach ($batch as $key => $value) {
                                            if ($value['status'] == 1) {
                                                echo "<option value=".$value['id'].">".$value['number']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Nama Barang</label>
                                <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama Barang" readonly>
                                <input type="hidden" name="obat_id" id="obat_id">
                                <input type="hidden" name="branch" id="branch" value="<?= $this->session->tempdata('data_session')[2] ?>">
                            </div>

                            <div class="form-group">
                                <label>Jenis Barang</label>
                                <input type="text" name="jenis_barang" id="jenis_barang" class="form-control" placeholder="Jenis Barang" readonly>
                                <input type="hidden" name="jenis_barang_id" id="jenis_barang_id">
                            </div>

                            <div class="form-group">
                                <label>Suplier <span style="color: red;">*</span></label>
                                <select class="form-control select2" name="suplier_id" id="suplier_id" onchange="setSuplier(this);">
                                    <option value="" selected >Pilih Suplier</option>
                                    <?php
                                        foreach ($suplier as $key => $value) {
                                            echo "<option value=".$value['id'].">".$value['nama']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Jumlah Barang <span style="color: red">*</span></label>
                                </div>
                                <div class="col-md-6">
                                    <label>Harga <span style="color: red">*</span></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="jml_brg_persediaan_1" name="jml_brg_persediaan_1" class="form-control" placeholder="P1" onchange="sumTotal()">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="jml_brg_persediaan_2" name="jml_brg_persediaan_2" class="form-control" placeholder="P2" onchange="sumTotal()">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="jml_brg_persediaan_3" name="jml_brg_persediaan_3" class="form-control" placeholder="P3" onchange="sumTotal()">
                                    </div>
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="text" id="persediaan_1" name="persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="persediaan_2" name="persediaan_2" class="form-control" placeholder="Persediaan 2" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="persediaan_3" name="persediaan_3" class="form-control" placeholder="Persediaan 3" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <input style="margin-bottom: 5px;" type="number" min="0" id="harga_persediaan_1" name="harga_persediaan_1" class="form-control" placeholder="Harga Persediaan 1" onchange="sumTotal()">
                                    <input style="margin-bottom: 5px;" type="number" min="0" id="harga_persediaan_2" name="harga_persediaan_2" class="form-control" placeholder="Harga Persediaan 2" onchange="sumTotal()">
                                    <input style="margin-bottom: 5px;" type="number" min="0" id="harga_persediaan_3" name="harga_persediaan_3" class="form-control" placeholder="Harga Persediaan 3" onchange="sumTotal()">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Total</label>
                                <input type="text" name="total" id="total" class="form-control" placeholder="Total" readonly>
                            </div>

                            <div class="form-group">
                                <label>Status Bayar <span style="color: red;">*</span></label>
                                <select class="form-control select2" name="status_bayar" id="status_bayar">
                                    <option value="0" selected>BELUM LUNAS</option>
                                    <option value="1">LUNAS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    Tanggal Pelunasan <span style="color: red;">*</span>
                                </label>
                                <div class="input-group">
                                    <input name="tanggal_lunas" id="tanggal_lunas" type="text" value=""  class="form-control datepick" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    Tanggal Jatuh Tempo Pembayaran <span style="color: red;">*</span>
                                </label>
                                <div class="input-group">
                                    <input name="tanggal_jatuh_tempo" id="tanggal_jatuh_tempo" type="text" value=""  class="form-control datepick" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    Tanggal Masuk Barang <span style="color: red;">*</span>
                                </label>
                                <div class="input-group">
                                    <input name="tanggal_barang_masuk" id="tanggal_barang_masuk" type="text" value=""  class="form-control datepick" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nomor PO</label> <span style="color: red;">*</span>
                                <input type="text" name="no_po" id="no_po" class="form-control" placeholder="Nomor PO">
                            </div>
                            <div class="form-group">
                                <label>Nomor Invoice</label> <span style="color: red;">*</span>
                                <input type="text" name="no_invoice" id="no_invoice" class="form-control" placeholder="Nomor Invoice">
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan"></textarea>
                            </div>
                        </div>
                        <br><br>
                        <div class="col-md-12">
                            <br><br>
                            <button type="button" id="saveGudangUtama"class="btn btn-success pull-right"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="modal_bayar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
        <div class="modal-dialog">
            <?php echo form_open('#',array('id' => 'fmBayarGudangUtama'))?>
            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

            <div class="modal-content" style="overflow:auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b>Bayar Barang Masuk</b></h2>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                <div>
                                    <p id="card_message" class=""></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Total Harga</label>
                                <input type="text" name="total" id="bayar_total" class="form-control" placeholder="Total" readonly>
                                <input type="hidden" name="id" id="bayar_id" value="1">
                                <input type="hidden" name="status_bayar" id="status_bayar" value="1">
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    Tanggal Pelunasan <span style="color: red;">*</span>
                                </label>
                                <div class="input-group">
                                    <input name="tanggal_lunas" id="bayar_tanggal_lunas" type="text" value=""  class="form-control datepick" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" name="keterangan" id="bayar_keterangan" placeholder="Keterangan"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <br><br>
                            <button type="button" id="payGudangUtama"class="btn btn-success pull-right"><i class="fa fa-floppy-o p-r-10"></i>BAYAR</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="modal_tf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmTransferGudangUtama'))?>
            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

            <div class="modal-content" style="overflow:auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b>Transfer Barang ke Gudang Farmasi</b></h2>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                <div>
                                    <p id="card_message" class=""></p>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="tf_id_gudang" name="id_gudang">
                        <input type="hidden" id="tf_id_stok" name="id_stok">

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <label>Jumlah Barang Saat ini</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <label>Jumlah barang yang ingin ditransfer</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="stok_jml_brg_persediaan_1" class="form-control" placeholder="P1" readonly>
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="stok_jml_brg_persediaan_2" class="form-control" placeholder="P2" readonly>
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="stok_jml_brg_persediaan_3" class="form-control" placeholder="P3" readonly>
                                    </div>
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="text" id="stok_persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="stok_persediaan_2" class="form-control" placeholder="Persediaan 2" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="stok_persediaan_3" class="form-control" placeholder="Persediaan 3" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="tf_jml_brg_persediaan_1" name="jml_brg_persediaan_1" class="form-control" placeholder="P1">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="tf_jml_brg_persediaan_2" name="jml_brg_persediaan_2" class="form-control" placeholder="P2">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="tf_jml_brg_persediaan_3" name="jml_brg_persediaan_3" class="form-control" placeholder="P3">
                                    </div>
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="text" id="tf_persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="tf_persediaan_2" class="form-control" placeholder="Persediaan 2" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="tf_persediaan_3" class="form-control" placeholder="Persediaan 3" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <br><br>
                            <button type="button" id="tfGudangUtama"class="btn btn-success pull-right"><i class="fa fa-floppy-o p-r-10"></i>TRANSFER</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <?php $this->load->view('footer');?>
