<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
<!-- Page Content -->

<style>
    .box_form {
        border : 1px solid #e4e7ea;
        padding: 16px;
        margin-bottom: 10px;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Logistik  -  Stok Gudang</h4> </div>
            <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                <ol class="breadcrumb">
                    <li><a href="index.html">Logistik</a></li>
                    <li class="active">Stok Gudang</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Data Stok Gudang
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <ul class="nav customtab nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#stok" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">STOK</span></a></li>
                                <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#penyesuaian" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">PENYESUAIAN</span></a></li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="stok">
                                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label for="inputName1" class="control-label"></label>
                                                    <dl class="text-right">
                                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                        <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>
                                                    </dl>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Tanggal Transaksi, Dari</label>
                                                            <div class="input-group">
                                                                <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <label>Sampai</label>
                                                            <div class="input-group">
                                                                <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>&nbsp;</label><br>
                                                            <button type="button" class="btn btn-success col-md-12" onclick="cariGudangUtama()">Cari</button>
                                                        </div>
                                                        <div class="form-group col-md-3" style="margin-top: 7px">
                                                            <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                            <b>
                                                                <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                                    <option value=""  selected>Pilih Berdasarkan</option>
                                                                    <option value="kode_barang">Kode Barang</option>
                                                                    <option value="nama_barang">Nama Barang</option>
                                                                    <option value="jenis_barang">Jenis Barang</option>
                                                                    <option value="nama_supplier">Supplier</option>
                                                                    <!-- <option value="status">Status</option>    -->
                                                                </select></b>

                                                        </div>
                                                        <div class="form-group col-md-9" style="margin-top: 7px">
                                                            <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                            <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariGudangUtama()" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br><hr style="margin-top: -27px">
                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <button type="button" class="btn btn-warning" id="exportToExcel" onclick="exportToExcel()" ><i class="fa fa-cloud-upload p-r-10"></i>Export To Excel</button>
                                            </div>


                                            <div class="table">
                                                <table id="table_stok_gudang_list" class="table table-responsive table-striped dataTable" cellspacing="0">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Barang</th>
                                                        <th>Jenis Barang</th>
                                                        <th>Stok Masuk</th>
                                                        <th>Stok Keluar</th>
                                                        <th>Stok Akhir</th>
                                                        <th>Penyesuaian</th>
                                                        <th>Stok Status</th>
                                                        <th>Keterangan</th>
                                                        <th>Perubahan Terakhir</th>
<!--                                                        <th>Aksi</th>-->
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td colspan="4">No Data to Display</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade in" id="penyesuaian">
                                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label for="inputName1" class="control-label"></label>
                                                    <dl class="text-right">
                                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                        <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>
                                                    </dl>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Tanggal Transaksi, Dari</label>
                                                            <div class="input-group">
                                                                <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <label>Sampai</label>
                                                            <div class="input-group">
                                                                <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>&nbsp;</label><br>
                                                            <button type="button" class="btn btn-success col-md-12" onclick="cariGudangUtama()">Cari</button>
                                                        </div>
                                                        <div class="form-group col-md-3" style="margin-top: 7px">
                                                            <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                            <b>
                                                                <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                                    <option value=""  selected>Pilih Berdasarkan</option>
                                                                    <option value="kode_barang">Kode Barang</option>
                                                                    <option value="nama_barang">Nama Barang</option>
                                                                    <option value="jenis_barang">Jenis Barang</option>
                                                                    <option value="nama_supplier">Supplier</option>
                                                                    <!-- <option value="status">Status</option>    -->
                                                                </select></b>

                                                        </div>
                                                        <div class="form-group col-md-9" style="margin-top: 7px">
                                                            <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                            <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariGudangUtama()" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><br><hr style="margin-top: -27px">

                                            <div class="table">
                                                <table id="table_penyesuaian_list" class="table table-responsive table-striped dataTable" cellspacing="0">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Barang</th>
                                                        <th>Jenis Barang</th>
                                                        <th>Kadaluarsa</th>
                                                        <th>Stok Masuk</th>
                                                        <th>Stok Keluar</th>
                                                        <th>Stok Akhir</th>
                                                        <th>Penyesuaian</th>
                                                        <th>Keterangan</th>
                                                        <th>Perubahan Terakhir</th>
<!--                                                        <th>Aksi</th>-->
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td colspan="4">No Data to Display</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->
    </div>
    <!-- /.container-fluid -->

    <div id="modal_penyesuaian" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmPenyesuaianGudangUtama'))?>
            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

            <div class="modal-content" style="overflow:auto;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b>Penyesuaian Stok Barang Gudang Utama</b></h2>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                <div>
                                    <p id="card_message" class=""></p>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="tf_id_gudang" name="id_gudang">
                        <input type="hidden" id="tf_id_stok" name="id_stok">

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <label>Jumlah Stok Akhir Barang Saat ini</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <label>Jumlah Stok Akhir Barang yang ingin disesuaikan</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="stok_jml_brg_persediaan_1" class="form-control" placeholder="P1" readonly>
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="stok_jml_brg_persediaan_2" class="form-control" placeholder="P2" readonly>
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="stok_jml_brg_persediaan_3" class="form-control" placeholder="P3" readonly>
                                    </div>
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="text" id="stok_persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="stok_persediaan_2" class="form-control" placeholder="Persediaan 2" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="stok_persediaan_3" class="form-control" placeholder="Persediaan 3" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="penyesuaian_1" name="penyesuaian_1" class="form-control" placeholder="P1">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="penyesuaian_2" name="penyesuaian_2" class="form-control" placeholder="P2">
                                        <input style="margin-bottom: 5px;" type="number" min="0" id="penyesuaian_3" name="penyesuaian_3" class="form-control" placeholder="P3">
                                    </div>
                                    <div class="col-md-6">
                                        <input style="margin-bottom: 5px;" type="text" id="tf_persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="tf_persediaan_2" class="form-control" placeholder="Persediaan 2" readonly>
                                        <input style="margin-bottom: 5px;" type="text" id="tf_persediaan_3" class="form-control" placeholder="Persediaan 3" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <br><br>
                            <button type="button" id="penyesuaianGudangUtama"class="btn btn-success pull-right"><i class="fa fa-floppy-o p-r-10"></i>SESUAIKAN</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <?php $this->load->view('footer');?>
