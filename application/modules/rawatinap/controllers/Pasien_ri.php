<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_ri extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pasien_ri_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "rawat_inap_pasienri_view";
        $comments = "List Pasien Rawat Inap";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_pasien_ri', $this->data);
    }

    public function ajax_list_pasienri(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran', TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis', TRUE);
        $no_bpjs = $this->input->get('no_bpjs', TRUE);
        $pasien_nama = $this->input->get('pasien_nama', TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat', TRUE);
        $nama_dokter = $this->input->get('nama_dokter', TRUE);
        $lokasi = $this->input->get('lokasi', TRUE);
        $list = $this->Pasien_ri_model->get_pasienri_list($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $nama_dokter, $lokasi);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $pasienri){
            $no++;
            $row = array();
            $row[] = $pasienri->no_pendaftaran;
            $row[] = '<span data-toggle="modal" data-target="#modal_riwayat_penyakit_pasien" data-backdrop="static" data-keyboard="false" title="klik untuk Riwayat Penyaakit Pasien"  onclick="riwayatPenyakitPasien('."'".$pasienri->pasien_id."'".')">'.$pasienri->no_rekam_medis."/ \n".$pasienri->pasien_nama.'</span>';
            $row[] = $pasienri->no_bpjs;
            $row[] = date('d M Y H:i:s', strtotime($pasienri->tgl_masukadmisi));
            $row[] = $pasienri->jenis_kelamin;
            $row[] = $pasienri->umur;
            $row[] = $pasienri->type_pembayaran;
            $row[] = $pasienri->kelaspelayanan_nama;
            $row[] = $pasienri->NAME_DOKTER;
            $row[] = $pasienri->poli_ruangan;
            $row[] = $pasienri->no_kamar." / ".$pasienri->no_bed;
            $row[] = $pasienri->lokasi;
            $row[] = $pasienri->statusrawat;

            if(trim(strtoupper($pasienri->statusrawat)) == "DIPULANGKAN" || trim(strtoupper($pasienri->statusrawat)) == "SUDAH PULANG" || trim(strtoupper($pasienri->statusrawat)) == "BATAL RAWAT" || trim(strtoupper($pasienri->statusrawat)) == "SEDANG DI PENUNJANG"){
                $row[] = '<button type="button" class="btn btn-default" title="Klik untuk pindah kamar" onclick="pindahKamar('."'".$pasienri->pendaftaran_id."'".')" disabled>Pindah</button>';
                $row[] = '<button type="button" class="btn btn-default" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRi('."'".$pasienri->pendaftaran_id."'".')" disabled>Periksa</button>';
                $row[] = '<button type="button" class="btn btn-default" title="Klik untuk kirim ke penunjang" onclick="kirimKePenunjang('."'".$pasienri->pendaftaran_id."'".')" disabled>Penunjang</button>';
                $row[] = '<button type="button" class="btn btn-default" title="Klik untuk pulangkan pasien" onclick="pulangkanPasienRi('."'".$pasienri->pendaftaran_id."'".')" disabled>Pulangkan</button>';
                $row[] = '<button type="button" class="btn btn-default btn-circle" title="Klik untuk membatalkan perawatan pasien" onclick="batalRawat('."'".$pasienri->pendaftaran_id."'".')" disabled><i class="fa fa-times"></i></button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pindah kamar" data-toggle="modal" data-target="#modal_pindah_kamar" data-backdrop="static" data-keyboard="false" onclick="pindahKamar('."'".$pasienri->pendaftaran_id."'".')">Pindah</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pemeriksaan pasien" data-toggle="modal" data-target="#modal_periksa_pasienri" data-backdrop="static" data-keyboard="false" onclick="periksaPasienRi('."'".$pasienri->pendaftaran_id."'".')">Periksa</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk kirim ke penunjang" data-toggle="modal" data-target="#modal_kirim_penunjang" data-backdrop="static" data-keyboard="false" onclick="kirimKePenunjang('."'".$pasienri->pendaftaran_id."'".')">Penunjang</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pulangkan pasien" id="PulangkanPasienRi" onclick="PulangkanPasienRi('."'".$pasienri->pendaftaran_id."'".')">Pulangkan</button>';
                $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk membatalkan perawatan pasien" id="batalRawat" onclick="batalRawat('."'".$pasienri->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>';
            }
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Pasien_ri_model->count_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $nama_dokter, $lokasi),
            "recordsFiltered" => $this->Pasien_ri_model->count_all_list_filtered($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $nama_dokter, $lokasi),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);

    }
    public function periksa_pasienri($pendaftaran_id){
        $pasienri['list_pasien'] = $this->Pasien_ri_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('v_periksa_pasienri', $pasienri);
    }
    public function ajax_list_diagnosa_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_ri_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-trash"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_diagnosa_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_diagnosa','Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('nama_diagnosa','Nama Diagnosa', 'required|trim');
        $this->form_validation->set_rules('jenis_diagnosa','Jenis Diagnosa', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $nama_diagnosa = $this->input->post('nama_diagnosa', TRUE);
            $jenis_diagnosa = $this->input->post('jenis_diagnosa', TRUE);
            $kode_diagnosa = $this->input->post('kode_diagnosa', TRUE);

            $data_diagnosapasien = array(
                'nama_diagnosa' => $nama_diagnosa,
                'tgl_diagnosa' => date('Y-m-d'),
                'kode_diagnosa' => $kode_diagnosa,
                'jenis_diagnosa' => $jenis_diagnosa,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'dokter_id' => $dokter,
                'diagnosa_by' => 'Rawat Inap'
            );

            $query = $this->db->get_where('t_diagnosapasien', array('pendaftaran_id'=>$pendaftaran_id,'1'=>$jenis_diagnosa));
            if ($query->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Diagnosa Utama Hanya Bisa Diinput Satu Kali');

                    // if permitted, do logit
                    $perms = "master_data_dokter_ruangan_view";
                    $comments = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $ins = $this->Pasien_ri_model->insert_diagnosapasien($pendaftaran_id, $data_diagnosapasien);
                //$ins=true;
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Diagnosa berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "rawat_inap_pasienri_view";
                    $comments = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "rawat_inap_pasienri_view";
                    $comments = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }

    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_ri_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            //$row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanpasien_id."'".')"><i class="fa fa-trash"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_tindakan_list(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $is_bpjs = $this->input->get('type_pembayaran',TRUE);
        $list_tindakan = $this->Pasien_ri_model->get_tindakan($kelaspelayanan_id,$is_bpjs);

        $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_tarif_tindakan(){
        $daftartindakan_id = $this->input->get('daftartindakan_id',TRUE);
        $tarif_tindakan = $this->Pasien_ri_model->get_tarif_tindakan($daftartindakan_id);

        $res = array(
            "list" => $tarif_tindakan,
            "success" => true
        );

        echo json_encode($res);
    }

    public function do_create_tindakan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Diagnosa', 'required');
        $this->form_validation->set_rules('tindakan','Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jumlah Tindakan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $dokter_anastesi = $this->input->post('dokter_anastesi', TRUE);
            $perawat = $this->input->post('perawat', TRUE);
            $tindakan = $this->input->post('tindakan', TRUE);
            $jml_tindakan = $this->input->post('jml_tindakan', TRUE);
            $subtotal = $this->input->post('subtotal', TRUE);
            $is_cyto = $this->input->post('is_cyto', TRUE);
            $totalharga = $this->input->post('totalharga', TRUE);
            $data_pendaftaran = $this->Pasien_ri_model->get_pendaftaran_pasien($pendaftaran_id);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'daftartindakan_id' => $tindakan,
                'jml_tindakan' => $jml_tindakan,
                'total_harga_tindakan' => $subtotal,
                'is_cyto' => $is_cyto == '1' ? '1' : '0',
                'total_harga' => $totalharga,
                'dokter_id' => $dokter,
                'dokteranastesi_id' => $dokter_anastesi == 'null' ? NULL : $dokter_anastesi,
                'perawat_id' => $perawat == 'null' ? NULL : $perawat,
                'tgl_tindakan' => date('Y-m-d'),
                'ruangan_id' => $data_pendaftaran->poliruangan_id
            );

            $ins = $this->Pasien_ri_model->insert_tindakanpasien($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_inap_pasienri_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_inap_pasienri_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosapasien_id = $this->input->post('diagnosapasien_id', TRUE);

        $delete = $this->Pasien_ri_model->delete_diagnosa($diagnosapasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function do_delete_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakanpasien_id = $this->input->post('tindakanpasien_id', TRUE);

        $delete = $this->Pasien_ri_model->delete_tindakan($tindakanpasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function ajax_list_resep_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_ri_model->get_resep_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $resep){
            $tgl_ = date_create($resep->tgl_reseptur);
            $row = array();
            $row[] = date_format($tgl_, 'd-M-Y');
            $row[] = $resep->nama_obat;
            $row[] = $resep->qty;
            $row[] = $resep->harga_jual;
            $row[] = $resep->satuan;
            $row[] = $resep->signa;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus Resep" onclick="hapusResep('."'".$resep->reseptur_id."'".')"><i class="fa fa-trash"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    function autocomplete_obat(){
        $val_obat = $this->input->get('val_obat');
        if(empty($val_obat)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }
        $obat_list = $this->Pasien_ri_model->get_obat_autocomplete($val_obat);
        if(count($obat_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($obat_list); $i++){
                $list[] = array(
                    "id" => $obat_list[$i]->obat_id,
                    "value" => $obat_list[$i]->nama_obat,
                    "deskripsi" => $obat_list[$i]->jenisoa_nama,
                    "satuan" => $obat_list[$i]->satuan,
                    "harga_netto" => $obat_list[$i]->harga_modal,
                    "harga_jual" => $obat_list[$i]->harga_jual,
                    "current_stok" => $obat_list[$i]->qtystok
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Obat ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Obat Not Found"
                );
        }
        echo json_encode($res);
    }

    public function do_create_resep_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_resep','Tanggal Resep', 'required');
        $this->form_validation->set_rules('nama_obat','Nama Obat', 'required|trim');
        $this->form_validation->set_rules('qty_obat','Jumlah Obat', 'required');
        $this->form_validation->set_rules('harga_jual','Harga Obat', 'required');
        $this->form_validation->set_rules('satuan_obat','Satuan Obat', 'required');
        $this->form_validation->set_rules('signa_obat','Signa Obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal input resep pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $obat_id = $this->input->post('obat_id', TRUE);
            $nama_obat = $this->input->post('nama_obat', TRUE);
            $qty_obat = $this->input->post('qty_obat', TRUE);
            $harga_jual = $this->input->post('harga_jual', TRUE);
            $satuan_obat = $this->input->post('satuan_obat', TRUE);
            $signa_obat = $this->input->post('signa_obat', TRUE);

            $data_reseppasien = array(
                'obat_id' => $obat_id,
                'qty' => $qty_obat,
                'harga_jual' => $harga_jual,
                'satuan' => $satuan_obat,
                'signa' => $signa_obat,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'dokter_id' => $dokter,
                'tgl_reseptur' => date('Y-m-d'),
            );

            //start transaction
            $this->db->trans_begin();

            $ins = $this->Pasien_ri_model->insert_reseppasien($data_reseppasien);
            //$ins=true;
            if($ins){
                $updatestok = array(
                    'penjualandetail_id' => $ins,
                    'obat_id' => $obat_id,
                    'tglstok_out' => date('Y-m-d'),
                    'qtystok_out' => $qty_obat,
                    'create_time' => date('Y-m-d H:i:s'),
                    'create_user' => $this->data['users']->id
                );

                $this->Pasien_ri_model->insert_stokobatalkes_keluar($updatestok);
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Resep, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienri_view";
                $comments = "Gagal menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $this->db->trans_commit();
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Resep berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienri_view";
                $comments = "Berhasil menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_resep(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        //start transaction
        $this->db->trans_begin();

        $reseptur_id = $this->input->post('reseptur_id', TRUE);

        $delete = $this->Pasien_ri_model->delete_resep($reseptur_id);
        if($delete){
            $this->Pasien_ri_model->delete_stok_resep($reseptur_id);
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Resep Pasien dengan ID = '". $reseptur_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $this->db->trans_commit();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Resep Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Resep Pasien dengan id Resep Pasien = '". $reseptur_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_batal_periksa(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $delete = $this->Pasien_ri_model->delete_periksa($pendaftaran_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id  = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function do_pulangkan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
        $update = $this->Pasien_ri_model->pulangkan_pasien($pendaftaran_id);
        if($update){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id  = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function get_kelasruangan_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_kelasruangan = $this->Pasien_ri_model->get_kelas_poliruangan($poliruangan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        foreach($list_kelasruangan as $list_kelas){
            $list .= "<option value='".$list_kelas->kelaspelayanan_id."'>".$list_kelas->kelaspelayanan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kamarruangan(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $list_kamarruangan = $this->Pasien_ri_model->get_kamar_aktif($poliruangan_id, $kelaspelayanan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kamar</option>";
        foreach($list_kamarruangan as $list_kamar){
            $status = $list_kamar->status_bed == '1' ? 'IN USE' : 'OPEN';
            if($list_kamar->status_bed == '0'){
                $list .= "<option value='".$list_kamar->kamarruangan_id."'>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }else{
                $list .= "<option value='".$list_kamar->kamarruangan_id."' disabled>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function do_pindah_kamar(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        $this->form_validation->set_rules('poli_ruangan','Ruangan', 'required');
        $this->form_validation->set_rules('kelas_pelayanan','Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('kamarruangan','Kamar', 'required');
        $this->form_validation->set_rules('dokter_pindah','Dokter', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal memindahkan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);
            $dokter = $this->input->post('dokter_pindah', TRUE);

            $pasienadmisi = $this->Pasien_ri_model->get_pasien_admisi($pendaftaran_id);
            $lama_kamar = $this->Pasien_ri_model->hitung_lama_rawat_kamar($pasienadmisi->pasienadmisi_id, $pasienadmisi->poli_ruangan_id, $pasienadmisi->kamarruangan_id);
            //start transaction kirim ke RI
            $this->db->trans_begin();

            $datamasukkmr = array(
                'tgl_keluarkamar' => date('Y-m-d H:i:s'),
                'lama_rawat_kamar' => $lama_kamar
            );
            //update t_masukkamar old
            $this->Pasien_ri_model->update_masukkamar($datamasukkmr, $pasienadmisi->pasienadmisi_id, $pasienadmisi->poli_ruangan_id, $pasienadmisi->kamarruangan_id);

            //update m_kamarruangan
            $updatekamar_old['status_bed'] = '0';
            $this->Pasien_ri_model->update_kamar($updatekamar_old, $pendaftaran_id);

            //insert t_tindakanpasien jasa kamar
            $daftartindakan = JASA_KAMAR;
            $kelaspelayanan_old = $pasienadmisi->kelaspelayanan_id;
            $poliruangan_old = $pasienadmisi->poli_ruangan_id;
            $row_tind = $this->Pasien_ri_model->get_tarif_kamar($daftartindakan, $kelaspelayanan_old);
            $totalharga = intval($lama_kamar)* intval($row_tind->harga_tindakan);

            $datatindakan = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasienadmisi->pasien_id,
                'daftartindakan_id' => $row_tind->daftartindakan_id,
                'jml_tindakan' => $lama_kamar,
                'total_harga_tindakan' => $totalharga,
                'is_cyto' => 0,
                'total_harga' => $totalharga,
                'dokter_id' => null,
                'tgl_tindakan' => date('Y-m-d'),
            );
            $ins_tindakan = $this->Pasien_ri_model->insert_tindakanpasien($pendaftaran_id, $datatindakan);

            if ($ins_tindakan){
                //insert t_masukkamar new
                $masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $pasienadmisi->pasienadmisi_id;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasien_ri_model->insert_masukkamar($masukkamar);

                //update t_pasienadmisi
                $updateadmisi['kelaspelayanan_id'] = $kelaspelayanan;
                $updateadmisi['poli_ruangan_asal'] = $poliruangan_old;
                $updateadmisi['poli_ruangan_id'] = $poliruangan;
                $updateadmisi['kamarruangan_id'] = $kamarruangan;
                $updateadmisi['dokter_id'] = $dokter;
                $upd_admisi = $this->Pasien_ri_model->update_pasienadmisi($pendaftaran_id, $updateadmisi);

                if($upd_admisi){
                    //update m_kamarruangan new
                    $updatekamar['status_bed'] = '1';
                    $this->Pasien_ri_model->update_kamar($updatekamar, $pendaftaran_id);
                }
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal memindahkan pasien, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_inap_pasienri_view";
                $comments = "Gagal memindahkan pasien dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $this->db->trans_commit();
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Pasien berhasil dipindahkan'
                );

                // if permitted, do logit
                $perms = "rawat_inap_pasienri_view";
                $comments = "Pasien berhasil dipindahkan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    function get_dokter_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_dokter = $this->Pasien_ri_model->get_dokter($poliruangan_id);
        if(count($list_dokter) > 1){
            $list = "<option value=\"\" disabled selected>Pilih Dokter</option>";
        }else{
            $list = "";
        }
        foreach($list_dokter as $list_dktr){
            $list .= "<option value='".$list_dktr->id_M_DOKTER."'>".$list_dktr->NAME_DOKTER."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    public function kirim_ke_penunjang(){
        $is_permit = $this->aauth->control_no_redirect('rawat_inap_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id_p','Pendaftaran', 'required');
        $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_inap_pasienri_view";
            $comments = "Gagal mengirim pasien ke penunjang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id_p', TRUE);
            $penunjang_id = $this->input->post('penunjang_id', TRUE);
            $last_pendaftaran = $this->Pasien_ri_model->get_last_pendaftaran($pendaftaran_id);

            if(count($last_pendaftaran) > 0){
                $datapenunjang['kelaspelayanan_id'] = $last_pendaftaran->kelaspelayanan_id;
                $datapenunjang['pendaftaran_id'] = $last_pendaftaran->pendaftaran_id;
                $datapenunjang['pasien_id'] = $last_pendaftaran->pasien_id;
                $datapenunjang['no_masukpenunjang'] = $last_pendaftaran->no_pendaftaran;
                $datapenunjang['tgl_masukpenunjang'] = date('Y-m-d H:i:s');
                $datapenunjang['statusperiksa'] = "ANTRIAN";
                $datapenunjang['poli_ruangan_asal'] = $last_pendaftaran->poliruangan_id;
                if($penunjang_id == 6 ){
                    $statuspenunjang = "RADIOLOGI";
                }else {
                    $statuspenunjang = "LABORATORIUM";
                }
                $datapenunjang['poli_ruangan_id']   = $penunjang_id;
                $datapenunjang['status_keberadaan'] = $statuspenunjang;
                $datapenunjang['dokterpengirim_id'] = $last_pendaftaran->dokter_id;
                $datapenunjang['create_time'] = date('Y-m-d H:i:s');
                $datapenunjang['create_by'] = $this->data['users']->id;
                $insert_penunjang = $this->Pasien_ri_model->insert_penunjang($datapenunjang);
            }

            if(isset($insert_penunjang)){
                $this->Pasien_ri_model->update_to_penunjang($pendaftaran_id);
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Pasien Berhasil dikirim ke penunjang'
                );
                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Pasien Berhasil dikirim ke penunjang";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal mengirim pasien ke penunjang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal mengirim pasien ke penunjang";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

	public function ajax_list_obat(){
		$data_obat = $this->Pasien_ri_model->get_list_obat();

		$data = array();
		foreach($data_obat as $obat){
			$row = array();

			$row[] = $obat->nama_obat;
			$row[] = $obat->satuan;
			$row[] = $obat->jenisoa_nama;
			$row[] = number_format($obat->harga_jual);
			$row[] = number_format($obat->qtystok);
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihObat('."'".$obat->obat_id."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" => $this->Pasien_ri_model->count_list_obat(),
			"recordsFiltered" => $this->Pasien_ri_model->count_list_filtered_obat(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}

	public function ajax_get_obat_by_id(){
		$id_obat = $this->input->get('id_obat',TRUE);

		$data_obat = $this->Pasien_ri_model->get_obat_by_id($id_obat);
		if (sizeof($data_obat)>0){
			$obat = $data_obat[0];

			$res = array(
				"success" => true,
				"messages" => "Data obat ditemukan",
				"data" => $obat
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data obat tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
	}


}
