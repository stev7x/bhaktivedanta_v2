<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->poli_ruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->umur; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->poli_ruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Diagnosa</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Tindakan</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-reseptur" class="nav-link" aria-controls="reseptur" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Pemberian Resep</span></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content"> 
                <div role="tabpanel" class="tab-pane active" id="diagnosa">
                    <table id="table_diagnosa_pasienri" class="table table-striped dataTable" cellspacing="0">
                        <thead>  
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>   
                                <th>Keterangan</th>
                                <th>Batal / Hapus</th>
                            </tr> 
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="5">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>  
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>   
                                <th>Keterangan</th>
                                <th>Batal / Hapus</th>
                            </tr> 
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif1" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="card_message1" class=""></p>
                            </div>
                        </div>
                        <div class="form-group col-md-4">   
                            <label class="control-label">
                            Input Diagnosa<span style="color: red;"> *</span></label>
                            <input id="kode_diagnosa" type="text" name="kode_diagnosa" value="" type="text" class="form-control" placeholder="Kode Diagnosa">
                        </div>     
                        <div class="form-group col-md-4">   
                            <label class="control-label">
                            Nama Diagnosa<span style="color: red;"> *</span></label>
                            <input id="nama_diagnosa" type="text" name="nama_diagnosa" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                        <div class="form-group col-md-4">   
                            <label class="control-label">
                         Jenis Diagnosa<span style="color: red;"> *</span></label>
                            <select id="jenis_diagnosa" name="jenis_diagnosa" class="form-control" >
                                <option value="" disabled selected>Pilih Jenis Diagnosa</option>
                                <option value="1">Diagnosa Utama</option>
                                <option value="2">Diagnosa Tambahan</option>
                            </select> 
                        </div>  
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-success" id="saveDiagnosa">
                                <i class="fa fa-floppy-o"></i> SIMPAN
                            </button>  
                        </div>

                        <?php echo form_close();?>  
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tabs-tindakan"> 

                    <table id="table_tindakan_pasienri" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Cyto</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Cyto</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>
                    
                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />    
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif2" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="card_message2" class=""></p>
                            </div>
                        </div>
                    <div class="col-md-6">
                        <div class="form-group">   
                            <label class="control-label">
                            Input Tindakan<span style="color: red;"> *</span></label>
                            <select id="tindakan" name="tindakan" class="form-control select2" onchange="getTarifTindakan()">
                            <option value=""  selected>Pilih Tindakan</option>
                        </select> 
                            <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Cyto</label> 
                            <select id="is_cyto" name="is_cyto" class="form-control" onchange="hitungHarga()">
                                <option value="" disabled selected>Pilih Cyto</option> 
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>  
                    </div>
                    <div class="form-group col-md-6">   
                        <label class="control-label">
                        Jumlah Tindakan<span style="color: red;"> *</span></label>
                         <input id="jml_tindakan" name="jml_tindakan" class="form-control" value="" type="number" onkeyup="hitungHarga()" min="1" placeholder="Jumlah Tindakan">  
                        <input type="hidden" id="subtotal" name="subtotal" value="0" readonly>
                        <input type="hidden" id="totalharga" name="totalharga" value="0" readonly>
                        <input type="hidden" id="is_cyto" name="is_cyto" value="0" readonly>
                    </div> 
                        
                          
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-success" id="saveTindakan"><i class="fa fa-floppy-o"></i> SIMPAN</button>  
                        </div>
                    
                    <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div>

                <div role="tabpanel" class="tab-pane" id="tabs-reseptur">
                    <table id="table_resep_pasienri" class="table table-striped dataTable" cellspacing="0">
                        <thead>  
                            <tr>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <th>Satuan</th>
                                <th>Signa</th>
                                <th>Batal / Hapus</th>
                            </tr> 
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="5">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>  
                            <tr>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <th>Satuan</th>
                                <th>Signa</th>
                                <th>Batal / Hapus</th>
                            </tr> 
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateResepPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delres" name="<?php echo $this->security->get_csrf_token_name()?>_delres" value="<?php echo $this->security->get_csrf_hash()?>" />  
                        <div class="row">  
                        <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif3" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="card_message3" class=""></p>
                            </div>
                        </div>
                            <div class="form-group col-sm-6">   
                                <label class="control-label">
                                Input Resep Pasien<span style="color: red;"> *</span></label>
                                    <div class="input-group custom-search-form">  
                                     <input type="text" class="form-control" placeholder="Nama Obat" name="nama_obat" id="nama_obat" class="autocomplete" value=""> <span class="input-group-btn">    
                                       <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_reseptur" data-backdrop="static" data-keyboard="false" onclick="dialogObat()" type="button">  <i class="fa fa-bars"></i> </button       
                                         </span> 
                                    </div>    
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">
                                Jumlah Obat<span style="color: red;"> *</span></label>
                                <input type="number" min="1" class="form-control" id="qty_obat" name="qty_obat" placeholder="Jumlah Obat">
                                <input id="obat_id" type="hidden" name="obat_id" value="" readonly>
                                <input id="current_stok" type="hidden" name="current_stok" value="" readonly>   
                           </div>
                            <div class="form-group col-sm-6">
                                <label class="control-label">Harga Jual</label>
                                <input type="number" class="form-control" id="harga_jual" name="harga_jual" placeholder="Harga Jual" readonly>
                                <input id="harga_netto" type="hidden" name="harga_netto" value="" readonly>
                            </div> 
  
                            <div class="form-group col-sm-6">   
                                <label class="control-label">
                                Satuan Obat<span style="color: red;"> *</span></label>
                                <select class="form-control select" id="satuan_obat" name="satuan_obat">
                                    <option value="" disabled selected>Satuan Obat</option>
                                    <option value="Btl">Btl</option>
                                    <option value="Box">Box</option>
                                    <option value="Cpl">Cpl</option>
                                    <option value="Mg">Mg</option>
                                    <option value="Ml">Ml</option>
                                    <option value="g">g</option>
                                </select>
                            </div> 

                            <div class="form-group col-sm-6">   
                                <label class="control-label">
                               Signa<span style="color: red;"> *</span></label>
                                <select id="signa_obat" name="signa_obat" class="form-control">
                                    <option value="" disabled selected>Pilih Signa</option>
                                    <option value="1x1">1x1</option>
                                    <option value="2x1">2x1</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-12"> 
                                <button type="button" class="btn btn-success" id="saveResep"><i class="fa fa-floppy-o"></i> SIMPAN</button>  
                            </div>
                        </div>  

                        <?php echo form_close();?>  
                    <div class="clearfix"></div>
                </div>

            </div>  

        </div>

    </div>

</div>

  

 
<div id="modal_reseptur" class="modal fade bs-example-modal-lg">
    <div class="modal-dialog modal-lg">  
        <div class="modal-content" style="overflow: auto;height: 330px;">     
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Obat</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">    
                   <table id="table_list_obat" class="table table-striped dataTable" cellspacing="0">   
                        <thead>
                            <tr>
                                <th>Nama Obat</th>
                                <th>Satuan</th>
                                <th>Jenis Obat</th>
                                <th>Harga Jual</th>
                                <th>Jumlah Stok</th> 
                                <th>Pilih</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="6">No Data to Display</td>
                            </tr>
                        </tbody>
                    </table>  
            </div>  
            
        </div> 
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<?php $this->load->view('footer_iframe');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatinap/pasien_ri/periksa_pasienri.js"></script>
</body>
 
</html>