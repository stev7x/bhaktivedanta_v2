<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Pasien Operasi </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Rawat Inap</a></li>
                            <li class="active">Pasien Operasi</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Data Pasien Operasi 
                            </div> 
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                        <label for="inputName1" class="control-label"></label>
                                            <dl class="text-right">  
                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                            </dl> 
                                        </div> 
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label>Tanggal Kunjungan,Dari</label>
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                     </div>
                                                </div>
                                                <div class="col-md-5">  
                                                    <label>Sampai</label>    
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                     </div>
                                                </div>
                                                <div class="col-md-2"> 
                                                    <label>&nbsp;</label><br>
                                                    <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                                </div> 
                                                <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="cari_pasien" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="no_pendaftaran">No. Pendaftaran</option>
                                                        <option value="no_rekam_medis">No. Rekam Medis</option>
                                                        <option value="no_bpjs">No. BPJS</option>   
                                                        <option value="pasien_nama">Nama Pasien</option>     
                                                        <option value="pasien_alamat">Alamat Pasien</option>
                                                        <option value="nama_dokter">Dokter PJ</option>
                                                        <option value="lokasi">Lantai</option>

                                                    </select></b>
                                                         
                                                </div>    
                                                <div class="form-group col-md-9" style="margin-top: 7px">   
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                    <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasien()" >       
                                                </div> 
                                            </div>
                                        </div> 
                                    </div><br><hr style="margin-top: -27px"> 
                                     
                                   <table id="table_list_pasienri" class="table table-striped table-responsive" cellspacing="0">  
                                        <thead>
                                            <tr> 
                                                <th>No. Pendaftaran</th>
                                                <th>No. RM / Nama Pasien</th>
                                                <th>No. BPJS</th>
                                                <th>Tanggal Admisi</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur Pasien</th>
                                                <th>Pembayaran</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Dokter PJ</th>
                                                <th>Ruangan</th>
                                                <th>No. Kamar/ No. Bad</th>
                                                <th>Lokasi</th>
                                                <th>Status Pasien</th>
                                                <th>Pindah Kamar</th>
                                                <th>Periksa Pasien</th>
                                                <th>Kirim ke Penunjang</th>
                                                <th>Pulangkan Pasien</th>
                                                <th>Batal Rawat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="16">No data to display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>     
                        </div>
                        
                    </div>
                    
                </div>
                <!--/row -->
                
            </div> 
            <!-- /.container-fluid --> 




<div id="modal_pindah_kamar" class="modal fade bs-example-modal-lg" onclick="reloadTablePasien" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">      
        <div class="modal-content" style="overflow:auto; margin-top: 120px; ">   
            <div class="modal-header" style="background: #fafafa"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Pasien</b></h2>
             </div>  
            <div class="modal-body" style="background: #fafafa">   
            <?php echo form_open('#',array('id' => 'fmPindahKamar'))?>  
            <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="" readonly> 
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="card_message1" class=""></p>
                            </div>
                        </div>   
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Poliklinik/Ruangan<span style="color: red;"> *</span></label>
                        <select id="poli_ruangan" name="poli_ruangan" class="form-control select2" onchange="getKelasPoliruangan()">
                            <option value=""  selected>Pilih Poli/Ruangan</option>
                            <?php
                            $list_ruangan = $this->Pasien_operasi_model->get_ruangan();
                            foreach($list_ruangan as $list){
                                echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kelas Pelayanan<span style="color: red;"> *</span></label>
                        <select id="kelas_pelayanan" name="kelas_pelayanan" class="form-control select2" onchange="getKamar()">
                            <option value=""  selected>Pilih Kelas Pelayanan</option>
                        </select> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Kamar<span style="color: red;"> *</span></label>
                        <select id="kamarruangan" name="kamarruangan" class="form-control select2">
                            <option value=""  selected>Pilih Kamar</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Dokter Penanggung Jawab<span style="color: red;"> *</span></label>
                        <select id="dokter_pindah" name="dokter_pindah" class="form-control select2">
                            <option value=""  selected>Pilih Dokter</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">  
                    <button type="button" class="btn btn-success pull-right" id="savePindahKamar"><i class="fa fa-floppy-o p-r-10"></i> SIMPAN </button>
                </div>
        <?php echo form_close(); ?>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal periksa -->
<div id="modal_periksa_pasienri" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">       
        <div class="modal-content" style="margin-top: 160px;overflow: hidden;">  
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Pemeriksaan Pasien</b></h2>
             </div>   
            <div class="modal-body" style="background: #fafafa">        
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>    
                </div> 
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div id="modal_kirim_penunjang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content" style="margin-top: 150px"> 
      <?php echo form_open('#',array('id' => 'fmKirimKePenunjang'))?>
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim Ke Penunjang</b></h2>
             </div>   
            <div class="modal-body" style="background: #fafafa"> 
                <div class="row">
                <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif2" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="card_message2" class=""></p>
                            </div>
                        </div>  
                    <div class="col-md-12"> 
                        <div class="form-group">
                            <label>Pilih Penunjang<span style="color: red;"> *</span></label>
                            <select id="penunjang_id" name="penunjang_id" class="form-control">
                                <option value="" disabled selected>Pilih Penunjang</option> 
                                <?php
                                $list_penunjang = $this->Pasien_operasi_model->get_list_penunjang();
                                    foreach($list_penunjang as $list){
                                        echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <input type="hidden" id="pendaftaran_id_p" name="pendaftaran_id_p">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveKirimPenunjang"><i class="fa fa-floppy-o"></i> SIMPAN</button>
                    </div>
                </div>  
            </div>
        </div>
            <?php echo form_close();?> 
    </div> 
</div>


<div id="modal_keterangan_keluar" onclick="reloadTablePasien()" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-large"> 
    <div class="modal-content">
        <div class="modal-header" style="background: #fafafa">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel"><b>Keterangan Keluar</b></h2>
         </div>
        <div class="modal-body" style="background: #fafafa">

        <div class="col-md-12">

            <div class="embed-responsive embed-responsive-16by9" style="height:500px;!important;padding: 0px !important">
            
                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
           
            </div>
            
           </div>
        
        </div>
    </div>
</div>
</div>

           

<?php $this->load->view('footer');?>
      