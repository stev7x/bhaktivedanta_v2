<?php $this->load->view('header_iframe');?>
<body>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <div class="row">
                <h5>Data Pasien</h5>
                <div class="divider"></div>
                <div>&nbsp;</div>
                <div class="col s6">
                    <div class="row">
                        <div class="input-field col s11">
                            <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                            <input id="tgl_pendaftaran" type="text" name="tgl_pendaftaran" value="<?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?>" readonly>
                            <label for="tgl_pendaftaran" class="active">Tgl. Pendaftaran</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="no_pendaftaran" type="text" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                            <label for="tgl_pendaftaran" class="active">No. Pendaftaran</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="no_rm" type="text" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                            <label for="no_rm" class="active">No. Rekam Medis</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input type="hidden" name="poliruangan_id" id="poliruangan_id" value="<?php echo $list_pasien->poliruangan_id; ?>" readonly>
                            <input id="poliruangan" type="text" name="poliruangan" class="validate" value="<?php echo $list_pasien->poli_ruangan." (".$list_pasien->no_bed.")"; ?>" readonly>
                            <label for="poliruangan" class="active">Ruangan</label>
                        </div>
                    </div>
                </div>
                <div class="col s6">
                    <div class="row">
                        <div class="input-field col s11">
                            <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>" readonly>
                            <input id="nama_pasien" type="text" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                            <label for="nama_pasien" class="active">Nama Pasien</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="umur" type="text" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                            <label for="umur" class="active">Umur</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="jenis_kelamin" type="text" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                            <label for="jenis_kelamin" class="active">Jenis Kelamin</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">
                            <input id="kelaspelayanan" type="text" name="kelaspelayanan" class="validate" value="<?php echo $list_pasien->kelaspelayanan_nama; ?>" readonly>
                            <label for="kelaspelayanan" class="active">Kelas Pelayanan</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="row">
                        <div class="col s11">
                            <label>Dokter Penanggung Jawab<span style="color: red;"> *</span></label>
                            <select id="dokter_periksa" name="dokter_periksa" class="validate browser-default" >
                                <option value="" disabled selected>Pilih Dokter</option>
                                <?php
                                $list_dokter = $this->Pasien_operasi_model->get_dokter_list();
                                foreach($list_dokter as $list){
                                    if($list_pasien->id_M_DOKTER == $list->id_M_DOKTER){
                                        echo "<option value='".$list->id_M_DOKTER."' selected>".$list->NAME_DOKTER."</option>";
                                    }else{
                                        echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                    }
                                    
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s11">
                            <label>Dokter Anastesi</label>
                            <select id="dokter_anastesi" name="dokter_anastesi" class="validate browser-default" >
                                <option value="" disabled selected>Pilih Dokter</option>
                                <?php
                                $list_anastesi = $this->Pasien_operasi_model->get_dokter_anastesi();
                                foreach($list_anastesi as $list){
                                    if($list_pasien->dokteranastesi_id == $list->id_M_DOKTER){
                                        echo "<option value='".$list->id_M_DOKTER."' selected>".$list->NAME_DOKTER."</option>";
                                    }else{
                                        echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s11">
                            <label>Perawat / Bidan</label>
                            <select id="perawat" name="perawat" class="validate browser-default" >
                                <option value="" disabled selected>Pilih Perawat</option>
                                <?php
                                $list_perawat = $this->Pasien_operasi_model->get_perawat();
                                foreach($list_perawat as $list){
                                    if($list_pasien->dokterperawat_id == $list->id_M_DOKTER){
                                        echo "<option value='".$list->id_M_DOKTER."' selected>".$list->NAME_DOKTER."</option>";
                                    }else{
                                        echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col s6">
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="type_pembayaran" type="text" name="type_pembayaran" class="validate" value="<?php echo $list_pasien->type_pembayaran; ?>" readonly>
                            <label for="type_pembayaran" class="active">Type Pembayaran</label>
                        </div>
                    </div>
                </div>
            </div>
            <div>&nbsp;</div>
            <div id="card-alert" class="card green modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="modal_card_message">SUCCESS : The page has been added.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div>&nbsp;</div>
            <div class="row">
                <div class="col s12">
                    <ul class="tabs tab-demo z-depth-1">
                        <li class="tab col s4"><a class="active" href="#diagnosa_tab">Diagnosa</a>
                        </li>
                        <li class="tab col s4"><a href="#tindakan_tab">Tindakan</a>
                        </li>
                        <li class="tab col s4"><a href="#reseptur_tab">Pemberian Resep</a>
                        </li>
                    </ul>
                </div>
                <div class="col s12">
                    <div id="diagnosa_tab" class="col s12">
                        <div>&nbsp;</div>
                        <div id="table-datatables">
                            <div class="row">
                                <div class="col s12 m4 l12">
                                    <table id="table_diagnosa_pasienri" class="responsive-table display" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Tgl. Diagnosa</th>
                                                <th>Kode Diagnosa</th>
                                                <th>Nama Diagnosa</th>
                                                <th>Keterangan</th>
                                                <th>Batal / Hapus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col s12">
                            <div>&nbsp;</div>
                            <div class="row">
                                <h6>Input Diagnosa</h6>
                                <div class="divider"></div>
                                <div>&nbsp;</div>
                                <div class="col s12">
                                    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
                                     <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                                    <div class="row">
<!--                                        <div class="input-field col s2">
                                            <input type="text" class="datepicker_diagnosa" name="tgl_diagnosa" id="tgl_diagnosa">
                                            <label for="tgl_diagnosa" class="active">Tgl. Diagnosa</label>
                                        </div>-->
                                        <div class="input-field col s4">
                                            <input id="kode_diagnosa" type="text" name="kode_diagnosa" class="validate" value="">
                                            <label for="kode_diagnosa" class="active">Kode Diagnosa</label>
                                        </div>
                                        <div class="input-field col s4">
                                            <input id="nama_diagnosa" type="text" name="nama_diagnosa" class="validate" value="">
                                            <label for="nama_diagnosa" class="active">Nama Diagnosa</label>
                                        </div>
                                        <div class="col s4">
                                            <label>Jenis Diagnosa</label>
                                            <select id="jenis_diagnosa" name="jenis_diagnosa" class="validate browser-default">
                                                <option value="" disabled selected>Pilih Jenis Diagnosa</option>
                                                <option value="1">Diagnosa Utama</option>
                                                <option value="2">Diagnosa Tambahan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 m8 l9">
                                            <button class="btn light-green waves-effect waves-light darken-4" type="button" id="saveDiagnosa">
                                                <i class="mdi-navigation-check left"></i>Simpan
                                            </button>
                                        </div>
                                    </div>
                                    <?php echo form_close()?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tindakan_tab" class="col s12">
                        <div>&nbsp;</div>
                        <div id="table-datatables">
                            <div class="row">
                                <div class="col s12 m4 l12">
                                    <table id="table_tindakan_pasienri" class="responsive-table display" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Tgl. Tindakan</th>
                                                <th>Nama Tindakan</th>
                                                <th>Jumlah Tindakan</th>
<!--                                                <th>Sub Total</th>-->
                                                <th>Cyto</th>
<!--                                                <th>Total Harga</th>-->
                                                <th>Batal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="7">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col s12">
                            <div>&nbsp;</div>
                            <div class="row">
                                <h6>Input Tindakan
                                <div class="divider"></div>
                                <div>&nbsp;</div>
                                <div class="col s12">
                                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                                    <div class="row">
<!--                                        <div class="input-field col s4">
                                            <input id="tgl_tindakan" class="datepicker_tindakan" type="text" name="tgl_tindakan" value="">
                                            <label for="tgl_tindakan" class="active">Tgl. Tindakan</label>
                                        </div>-->
<!--                                        <div class="col s4">
                                            <label>Kelas Pelayanan</label>
                                            <select id="kelas_pelayanan" name="kelas_pelayanan" class="validate browser-default" onchange="getTindakan()">
                                                <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                                                <?php
//                                                $list_kelaspelayanan = $this->Pasien_operasi_model->get_kelas_pelayanan();
//                                                foreach($list_kelaspelayanan as $list){
//                                                    echo "<option value='".$list->kelaspelayanan_id."'>".$list->kelaspelayanan_nama."</option>";
//                                                }
                                                ?>
                                            </select>
                                        </div>-->
                                        <div class="col s4">
                                            <label>Tindakan</label>
                                            <select id="tindakan" name="tindakan" class="validate browser-default" onchange="getTarifTindakan()">
                                                <option value="" disabled selected>Pilih Tindakan</option>
                                            </select>
                                            <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly>
                                        </div>
                                        <div class="input-field col s4">
                                            <input id="jml_tindakan" type="text" name="jml_tindakan" class="validate" value="" onkeypress="return numbersOnly(event);" onkeyup="hitungHarga()">
                                            <label for="jml_tindakan" class="active">Jumlah Tindakan</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="input-field col s4">
                                            <input id="subtotal" type="text" name="subtotal" class="validate" value="" placeholder="Sub Total" readonly>
                                            <label for="subtotal" class="active">Sub Total</label>
                                        </div>
                                        <div class="col s4">
                                            <label>Cyto</label>
                                            <select id="is_cyto" name="is_cyto" class="validate browser-default" onchange="hitungHarga()">
                                                <option value="" disabled selected>Pilih Cyto</option>
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        <div class="input-field col s4">
                                            <input id="totalharga" type="text" name="totalharga" class="validate" value="" placeholder="Sub Total" readonly>
                                            <label for="totalharga" class="active">Total Harga</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 m8 l9">
                                            <button class="btn light-green waves-effect waves-light darken-4" type="button" id="saveTindakan">
                                                <i class="mdi-navigation-check left"></i>Simpan
                                            </button>
                                        </div>
                                    </div>
                                    <?php echo form_close()?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="reseptur_tab" class="col s12">
                        <div>&nbsp;</div>
                        <div id="table-datatables">
                            <div class="row">
                                <div class="col s12 m4 l12">
                                    <table id="table_resep_pasienri" class="responsive-table display" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Nama Obat</th>
                                                <th>Jumlah</th>
                                                <th>Harga Jual</th>
                                                <th>Satuan</th>
                                                <th>Signa</th>
                                                <th>Batal / Hapus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="7">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col s12">
                            <div>&nbsp;</div>
                            <div class="row">
                                <h6>Input Diagnosa</h6>
                                <div class="divider"></div>
                                <div>&nbsp;</div>
                                <div class="col s12">
                                    <?php echo form_open('#',array('id' => 'fmCreateResepPasien'))?>
                                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delres" name="<?php echo $this->security->get_csrf_token_name()?>_delres" value="<?php echo $this->security->get_csrf_hash()?>" />
                                        <div class="row">
<!--                                            <div class="input-field col s4">
                                                <input id="tgl_resep" class="datepicker_resep" type="text" name="tgl_resep" value="">
                                                <label for="tgl_resep" class="active">Tgl. Resep</label>
                                            </div>-->
                                            <div class="input-field col s4">
                                                <input id="nama_obat" type="text" class="autocomplete" name="nama_obat" class="validate" value="">
                                                <label for="nama_obat" id="lbl_nama_obat" class="active">Nama Obat</label>
                                            </div>
                                            <div class="col s4">
                                                <label for="qty_obat" class="active">Jumlah Obat</label>
                                                <input id="qty_obat" type="number" name="qty_obat" class="validate" value="">
                                                <input id="obat_id" type="hidden" name="obat_id" value="" readonly>
                                                <input id="current_stok" type="hidden" name="current_stok" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s4">
                                                <input id="harga_jual"  type="text" name="harga_jual" value="">
                                                <label for="harga_jual" id="lbl_harga_jual" class="active">Harga</label>
                                                <input id="harga_netto" type="hidden" name="harga_netto" value="" readonly>
                                            </div>
                                            <div class="input-field col s4">
                                                <input id="satuan_obat" type="text" name="satuan_obat" class="validate" value="">
                                                <label for="satuan_obat" id="lbl_satuan_obat" class="active">Satuan Obat</label>
                                            </div>
                                            <div class="col s4">
                                                <label>Signa</label>
                                                <select id="signa_obat" name="signa_obat" class="validate browser-default">
                                                    <option value="" disabled selected>Pilih Signa</option>
                                                    <option value="1x1">1x1</option>
                                                    <option value="2x1">2x1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m8 l9">
                                                <button class="btn light-green waves-effect waves-light darken-4" type="button" id="saveResep">
                                                    <i class="mdi-navigation-check left"></i>Simpan
                                                </button>
                                            </div>
                                        </div>
                                    <?php echo form_close()?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatinap/pasien_operasi/periksa_pasienoperasi.js"></script>
</body>

</html>
