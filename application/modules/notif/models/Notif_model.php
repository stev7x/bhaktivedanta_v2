<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif_model extends CI_Model {

	function get_notif(){
        $this->db->from('t_notif');
        $this->db->where('view',0);   
        $this->db->order_by('id_notif','DESC');   
        $this->db->limit(3);   
        $query = $this->db->get();
        
        return $query->result();      
    }    
    function update_notif($data,$id){
         
       $update =  $this->db->update('t_notif',$data, array('id_notif' => $id));

       return $update;  
    }
}