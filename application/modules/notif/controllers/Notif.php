<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif extends CI_Controller {
	 public function __construct() {
        parent::__construct();
        // Your own constructor code 
        $this->load->library("Aauth");

        // if (!$this->aauth->is_loggedin()) {
        // 	$this->session->set_flashdata('message_type', 'error');
        //         $this->session->set_flashdata('messages', 'Please login first.');
        //         redirect('login');
        // }
        // $this->load->model('Menu_model');
        $this->load->model('Notif_model');
        $this->data['users']                = $this->aauth->get_user();
        // $this->data['groups']               = $this->aauth->get_user_groups();
        // $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->load->model('rekam_medis/list_pasien_model'); 
        $this->data['data_user'] = $this->list_pasien_model->get_data_user($this->data['users']->id);
    }

    public function get_data_notification(){
        $data = $this->Notif_model->get_notif($this->data['data_user']->name);
        $output = '';  
 
        foreach ($data as $list) {
            $output .= ' 

                            <div class="notif'.$list->id_notif.'">
                            <div id="alertbottomright" style="display: block;" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-bottom-right">
                                <img src="'.base_url().'assets/plugins/images/ic_notif.png" class="img" alt="img">
                                <a href="#" class="closed" onclick="closeNotif('.$list->id_notif.')">&times;</a>
                                <h4><b>Administrator</b></h4>  Pasien baru telah terdaftar pada '.date('H:i:s', strtotime($list->date_notif)).'
                            </div>
                            </div>
                       
            ';     
        }   
        // foreach ($data as $list) {
        // 	$output .= '     
        //                     <div id="card_notif'.$list->id_notif.'"> 
        //                         <div id="card-notif" class="card">
        //                             <div class="card-body" title="klik refresh">
        //                                 <div class="col-xs-12 pull-right">
        //                                     <button type="button" class="close" onclick="closeNotif('.$list->id_notif.')" style="padding:10px;">&times;</button>
        //                                 </div>
        //                                 <div class="col-xs-12" style="padding:10px;">
        //                                     <h5 id="notif" class="card-title"><i class="fa fa-bell-o fa-1x"></i>&nbsp;&nbsp;Anda Memiliki Pemberitahuan Baru</h5>
        //                                     <h7 id="notif">'.$list->description.'</h7><br>
        //                                     <h7>'.date('H:i:s', strtotime($list->date_notif)).' WIB</h7>
        //                                 </div>
        //                             </div>
        //                         </div>
        //                     </div>
                       
        //     ';     
        // }    
        // print_r($output);die();

        if($this->data['data_user']->name == !empty($data[0]->receiver)){  
            $res = array(    
                'data'              => $output,
                // 'title'             => $data->title, 
                // 'description'       => $data->description,  
                // 'id'                => $data->id_notif,  
                'load'              => 'success'      
            );
        }else{
            $res = array(
                'title'             => null, 
                'description'       => null,      
                'load'              => 'error' 
            );
        }
            echo json_encode($res);    
    
    }
    public function unseen_notif(){
        $id = $this->input->get('id');
        $data = array(
            'view' => 1
        );
        $push = $this->Notif_model->update_notif($data,$id);
        if($push){    
            $res = array(
                'view' => 'SUDAH DILIHAT'
            );
        }else{
            $res = array(
                'view' => 'BELUM DILIHAT'
            );
        }

        echo json_encode($res);    

    }

}