<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class list_phatologi extends CI_Controller{
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        
        $this->load->model('Menu_model');
        $this->load->model('list_phatologi_model');
        $this->load->model('rawatdarurat/Pasien_rd_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('phatologi_anak_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "phatologi_anak_view";
        $comments = "List Pasien Rawat Darurat";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_phatologi', $this->data);
    }

    public function ajax_list_pasienph(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $poliklinik = $this->input->get('poliklinik',TRUE);
        $status = $this->input->get('status',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $list = $this->list_phatologi_model->get_pasienph_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $tgl_lahir = $pasienrd->tanggal_lahir;
            $umur  = new Datetime($tgl_lahir);
            $today = new Datetime();
            $diff  = $today->diff($umur);

            $no++;
            $row = array();
            $row[] = $pasienrd->no_pendaftaran;
            $row[] = $pasienrd->no_rekam_medis;
            $row[] = $pasienrd->no_bpjs ;
            $row[] = $pasienrd->pasien_nama;
            $row[] = $pasienrd->tgl_pendaftaran;
            $row[] = $pasienrd->jenis_kelamin;
            // $row[] = $pasienrd->umur;
            $row[] = $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
            $row[] = $pasienrd->pasien_alamat;
            $row[] = $pasienrd->type_pembayaran;
            $row[] = $pasienrd->kelaspelayanan_nama;
            $row[] = $pasienrd->status_periksa;
            $row[] = !empty($pasienrd->carapulang) ? $pasienrd->carapulang : 'Not Set';
            //Action
            $row[] = '<div class="dropdown-action" onclick="$(this).children().toggleClass('."'d-block'".')">
                        <span class="title-dropdown">Pilih Proses</span><span class="arrow">▼</span>
                        <div class="dropdown-menu-action">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_pasienrd" title="Klik untuk pemeriksaan pasien" onclick="cobaTest('."'".$pasienrd->pendaftaran_id."'".')">PERIKSA</button>  
                        <button type="button" class="btn btn-success"  title="K.K OK">K.K OK</button>
                        <button type="button" class="btn btn-success"  title="K.K VK">K.K VK</button>
                        <button type="button" class="btn btn-success"  title="K.K HCU">K.K HCU</button>
                        <button type="button" class="btn btn-success"  title="K.K Pathologi Anak">K.K Pathologi Anak</button>
                        <button type="button" class="btn btn-success"  title="Rujuk Rs Lain">Rujuk Rs Lain</button>
                        <button type="button" class="btn btn-success"  title="Batal">Batal</button>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_penunjang" title="Klik untuk kirim ke penunjang">PENUNJANG</button>
                        </div>
                    </div>
                ';

            $data[] = $row;
        }
        
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->list_phatologi_model->count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "recordsFiltered" => $this->list_phatologi_model->count_all_list_filtered($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function do_create_background_pasien(){
        $is_permit = $this->aauth->control_no_redirect('phatologi_anak_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Diagnosa', 'required');
        $this->form_validation->set_rules('keluhan','keluhan', 'required');
        $this->form_validation->set_rules('paket_operasi','paket_operasi', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id          = $this->input->post('pasien_id', TRUE);
            $alergi             = $this->input->post('paket_operasi', TRUE);
            $keluhan            = $this->input->post('keluhan', TRUE);
            $data_pendaftaran   = $this->list_phatologi_model->get_pendaftaran_pasien($pendaftaran_id);
            $data_tindakanpasien = array(
                'alergi_id'             => $alergi,
                'tanggal'               => date('Y-m-d'),
                'pendaftaran_id'        => $pendaftaran_id,
                'pasien_id'             => $pasien_id,
                'instalasi_id'          => 9
            );
            $data = array(
                'catatan_keluhan' => $keluhan
            );
            $update = $this->list_phatologi_model->update_keluhan_background_pasien($pendaftaran_id, $data);
            // var_dump($data_tindakanpasien);die();
            $ins = $this->list_phatologi_model->insert_backgroundpasien($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "phatologi_anak_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "phatologi_anak_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function periksa_pasienrd($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->list_phatologi_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienrd['pendaftaran_id_'] = $pendaftaran_id;
        $this->load->view('kirim_periksa', $pasienrd);
    }

    public function do_create_diagnosa_pasien(){
        $is_permit = $this->aauth->control_no_redirect('phatologi_anak_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_diagnosa','Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('nama_diagnosa','Nama Diagnosa', 'required|trim');
        $this->form_validation->set_rules('jenis_diagnosa','Jenis Diagnosa', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id          = $this->input->post('pasien_id', TRUE);
            $diagnosa_id        = $this->input->post('diagnosa_id', TRUE);
            $dokter             = $this->input->post('dokter_id', TRUE);
            $nama_diagnosa      = $this->input->post('nama_diagnosa', TRUE);
            $kode_icd10         = $this->input->post('kode_diagnosa_icd10', TRUE);
            $nama_icd10         = $this->input->post('nama_diagnosa_icd10', TRUE);
            $kd_diagnosa        = $this->input->post('kode_diagnosa', TRUE);
            $jenis_diagnosa     = $this->input->post('jenis_diagnosa', TRUE);
            $data_diagnosapasien = array(
                'nama_diagnosa'  => $nama_diagnosa,
                'tgl_diagnosa'   => date('Y-m-d'),
                'kode_diagnosa'  => $kd_diagnosa,
                'kode_icd10'     => $kode_icd10,
                'nama_icd10'     => $nama_icd10,
                'kode_diagnosa'  => $kd_diagnosa,
                'jenis_diagnosa' => $jenis_diagnosa,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id'      => $pasien_id,
                'dokter_id'      => $dokter,
                'diagnosa_id'    => $diagnosa_id,
                'diagnosa_by'    => 'Phatologi Anak'
            );
            // cek diagnosa primer
            $query = $this->db->get_where('t_diagnosapasien', array('pendaftaran_id'=>$pendaftaran_id,'1'=>$jenis_diagnosa));
            if ($query->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Diagnosa Utama Hanya Bisa Diinput Satu Kali');

                    // if permitted, do logit 
                    $perms      = "phatologi_anak_view";
                    $comments   = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $ins = $this->list_phatologi_model->insert_diagnosapasien($pendaftaran_id, $data_diagnosapasien);
                $ins=true;
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash'      => $this->security->get_csrf_hash(),
                        'success'       => true,
                        'messages'      => 'Diagnosa berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms      = "phatologi_anak_view";
                    $comments   = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                    // if permitted, do logit
                    $perms      = "phatologi_anak_view";
                    $comments   = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
            // end diagnosa primer
        }
        echo json_encode($res);
    }

    public function ajax_list_diagnosa_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->list_phatologi_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->kode_icd10;
            $row[] = $diagnosa->nama_icd10;

            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->dokter_id;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_tindakan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('phatologi_anak_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Diagnosa', 'required');
        $this->form_validation->set_rules('tindakan','Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jenis Diagnosa', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = $this->input->post('pendaftaran_id', TRUE);
            $kelaspelayanan_id  = $this->input->post('kelaspelayanan_id', TRUE);
            $pasien_id          = $this->input->post('id_pasien', TRUE);
            $dokter             = $this->input->post('dokter_id_tindakan', TRUE);
            $tindakan           = $this->input->post('tindakan', TRUE);
            $jml_tindakan       = $this->input->post('jml_tindakan', TRUE);
            $harga_tindakan     = $this->input->post('harga_tindakan_satuan', TRUE);
            $subtotal           = $this->input->post('subtotal', TRUE);
            $totalharga         = $this->input->post('totalharga', TRUE);
            $data_pendaftaran   = $this->list_phatologi_model->get_pendaftaran_pasien($pendaftaran_id);
            $data_tindakanpasien = array(
                'pendaftaran_id'        => $pendaftaran_id,
                'kelaspelayanan_id'     => $kelaspelayanan_id,
                'pasien_id'             => $pasien_id,
                'daftartindakan_id'     => $tindakan, 
                'jml_tindakan'          => $jml_tindakan,  
                'harga_tindakan'        => $harga_tindakan,  
                'total_harga_tindakan'  => $subtotal,
                'total_harga'           => $totalharga, 
                'dokter_id'             => $dokter,
                'tgl_tindakan'          => date('Y-m-d'),
                'ruangan_id'            => $data_pendaftaran->poliruangan_id 
            );

            // var_dump($data_tindakanpasien);die();
            $ins = $this->list_phatologi_model->insert_tindakanpasien($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "phatologi_anak_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "phatologi_anak_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_list_background_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $pasien         = $this->list_phatologi_model->get_pendaftaran_pasien($pendaftaran_id); 
        $list           = $this->list_phatologi_model->get_background_pasien_list($pendaftaran_id);
        $data           = array();
        $no = 1;
        foreach($list as $tindakan){
            $row   = array();
            $row[] = $no;
            $row[] = $tindakan->nama_alergi;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus tindakan" onclick="hapusBackground('."'".$tindakan->alergi_pasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $no++;
            $data[] = $row;
        }
        $output = array(
                    "draw"              => $this->input->get('draw'),
                    "recordsTotal"      => count($list),
                    "recordsFiltered"   => count($list),
                    "data"              => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_assestment(){
        $is_permit = $this->aauth->control_no_redirect('phatologi_anak_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('Tensi','Tensi', 'required|trim');
        $this->form_validation->set_rules('Suhu','Suhu', 'required');
        // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        //$this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('Tensi', TRUE);
            $suhu = $this->input->post('Suhu', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $nadi = $this->input->post('Nadi', TRUE);
            $nadi_2 = $this->input->post('Nadi2', TRUE);
            $penggunaan = $this->input->post('Penggunaan', TRUE);
            $saturasi = $this->input->post('Saturasi', TRUE);
            $nyeri = $this->input->post('Nyeri', TRUE);
            $numeric = $this->input->post('Numeric', TRUE);
            $resiko = $this->input->post('Resiko', TRUE);
            $theraphyArr = $this->input->post('nama_program', TRUE);
            $data_pendaftaran = $this->list_phatologi_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->list_phatologi_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'instalasi_id' => '9',
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'suhu' => $suhu,
                'nadi_2' => $nadi_2,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko
            );

            $ins = $this->list_phatologi_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );
                        $rencana = $this->list_phatologi_model->insert_detassestment($data);
                    }
                }

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Insert Berhasil Loh'
                );

                // if permitted, do logit
                $perms = "phatologi_anak_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "phatologi_anak_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('phatologi_anak_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosapasien_id = $this->input->post('diagnosapasien_id', TRUE);

        $delete = $this->list_phatologi_model->delete_diagnosa($diagnosapasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_background(){
        $is_permit = $this->aauth->control_no_redirect('phatologi_anak_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosapasien_id = $this->input->post('alergi_id', TRUE);

        $delete = $this->list_phatologi_model->delete_background($diagnosapasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $pasien         = $this->list_phatologi_model->get_pendaftaran_pasien($pendaftaran_id); 
        $list           = $this->list_phatologi_model->get_tindakan_pasien_list($pendaftaran_id);
        $data           = array();
        foreach($list as $tindakan){
            $row   = array();
            $row[] = date('d M Y', strtotime($tindakan->tgl_tindakan));
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanpasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw"              => $this->input->get('draw'),
                    "recordsTotal"      => count($list),
                    "recordsFiltered"   => count($list),
                    "data"              => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_tarif_tindakan(){
        $daftartindakan_id  = $this->input->get('daftartindakan_id',TRUE);
        $tarif_tindakan     = $this->list_phatologi_model->get_tarif_tindakan($daftartindakan_id); 

 
        $res = array(   
            "list"      => $tarif_tindakan,
            "success"   => true
        );

        echo json_encode($res);
    }

    function get_tindakan_list(){  
        $kelaspelayanan_id  = $this->input->get('kelaspelayanan_id',TRUE);
        $is_bpjs            = $this->input->get('type_pembayaran',TRUE);
        $list_tindakan      = $this->list_phatologi_model->get_tindakan($kelaspelayanan_id, $is_bpjs);
        $list               = "<option value=\"\" disabled selected>Pilih Tindakan</option>";   
        foreach($list_tindakan as $list_tindak){   
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }    

        $res = array(
            "list"      => $list,
            "success"   => true
        ); 

        echo json_encode($res);
    }

    public function do_create_resep_pasien(){
        $is_permit = $this->aauth->control_no_redirect('phatologi_anak_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_resep','Tanggal Resep', 'required');
        $this->form_validation->set_rules('nama_obat','Nama Obat', 'required|trim');
        $this->form_validation->set_rules('qty_obat','Jumlah Obat', 'required');
        $this->form_validation->set_rules('harga_jual','Harga Obat', 'required');
        $this->form_validation->set_rules('satuan_obat','Satuan Obat', 'required');
        $this->form_validation->set_rules('signa_1','Signa Obat 1', 'required');
        $this->form_validation->set_rules('signa_2','Signa Obat 2', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "phatologi_anak_view";
            $comments = "Gagal input resep pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id  = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id       = $this->input->post('pasien_id', TRUE);
            $dokter          = $this->input->post('dokter_id', TRUE);
            $obat_id         = $this->input->post('obat_id', TRUE);
            $nama_obat       = $this->input->post('nama_obat', TRUE);
            $qty_obat        = $this->input->post('qty_obat', TRUE);
            $harga_jual      = $this->input->post('harga_jual', TRUE);
            $harga_netto     = $this->input->post('harga_netto', TRUE);
            $satuan_obat     = $this->input->post('satuan_obat', TRUE);
            $id_jenis_barang = $this->input->post('id_jenis_barang', TRUE);
            $dataRacikan = $this->input->post("dataRacikan", TRUE);
            $signa_obat      = $this->input->post('signa_obat', TRUE);


            $data_reseppasien = array(
                'kode_barang'    => $obat_id,
                'nama_barang'    => $nama_obat,
                'qty'            => $qty_obat,
                'harga_netto'    => $harga_netto,
                'harga_jual'     => $harga_jual,
                'id_sediaan'     => $satuan_obat,
                'id_jenis_barang'=> $id_jenis_barang,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id'      => $pasien_id,
                'dokter_id'      => $dokter,
                'tgl_reseptur'   => date('Y-m-d-H-i-s'). substr((string)microtime(), 1, 4),
                'instalasi_id'   => 1
            );

            $data_reseppasien2 = array(
                'kode_barang'    => $obat_id,
                'nama_barang'    => $nama_obat,
                'jumlah_barang'  => $qty_obat,
                'id_sediaan'     => $satuan_obat,
                'id_jenis_barang'=> $id_jenis_barang,
                'harga_satuan'   => $harga_jual,
                'tanggal_keluar' => date('Y-m-d-H-i-s'). substr((string)microtime(), 1, 4),
            );
            //start transaction
            // $this->db->trans_begin();

            $query1 = $this->db->get_where('t_apotek_stok',"stok_akhir <='$qty_obat'"); //cek stok tersedia


            if ($query1->num_rows() > 0) {
            // insert for table stok farmasi

            // $data_reseppasien2 = array(
            //     'kode_barang'    => $obat_id,
            //     'nama_barang'    => $nama_obat,
            //     'jumlah_barang'  => $qty_obat,
            //     'id_sediaan'     => $satuan_obat,
            //     'id_jenis_barang'=> $id_jenis_barang,
            //     'harga_satuan'   => $harga_jual,
            //     'tanggal_keluar' => date('Y-m-d-H-i-s'). substr((string)microtime(), 1, 4),
            // );


            // $query1 = $this->db->get_where('t_apotek_stok',"stok_akhir <='$qty_obat'"); //cek stok tersedia
            // if ($query1->num_rows() > 0) {
            $ins = $this->list_phatologi_model->insert_reseppasien($data_reseppasien);
            if ($ins) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Stok barang tidak mencukupi');

                    // if permitted, do logit

                    $perms = "phatologi_anak_view";
                    $comments = "Gagal menambahkan kode barang dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{

                // // $ins = $this->Pasien_rd_model->insert_reseppasien($data_reseppasien);
                // $ins = $this->Pasien_rd_model->insert_resep_to_apotek($data_reseppasien2);
                // $ins = $this->Pasien_rd_model->insert_reseppasien($data_reseppasien);
                // //$ins=true;
                // if($ins){
                //     // $updatestok = array(
                //     //     'penjualandetail_id' => $ins,
                //     //     'obat_id' => $obat_id,
                //     //     'tglstok_out' => date('Y-m-d'),
                //     //     'qtystok_out' => $qty_obat,
                //     //     'create_time' => date('Y-m-d H:i:s'),
                //     //     'create_user' => $this->data['users']->id
                //     // );

                //     // $this->Pasien_rd_model->insert_stokobatalkes_keluar($updatestok);
                // }

                // if ($this->db->trans_status() === FALSE){
                //     $this->db->trans_rollback();
                //     $res = array(
                //     'csrfTokenName' => $this->security->get_csrf_token_name(),
                //     'csrfHash' => $this->security->get_csrf_hash(),
                //     'success' => false,
                //     'messages' => 'Gagal menambahkan Resep, hubungi web administrator.');

                //     // if permitted, do logit
                //     $perms = "rawat_darurat_pasienrd_view";
                //     $comments = "Gagal menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                //     $this->aauth->logit($perms, current_url(), $comments);
                // }else{
                //     $this->db->trans_commit();
                //     $res = array(
                //         'csrfTokenName' => $this->security->get_csrf_token_name(),
                //         'csrfHash' => $this->security->get_csrf_hash(),
                //         'success' => true,
                //         'messages' => 'Resep berhasil ditambahkan'
                //     );

                //     // if permitted, do logit
                //     $perms = "rawat_darurat_pasienrd_view";
                //     $comments = "Berhasil menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                //     $this->aauth->logit($perms, current_url(), $comments);
                // }

                // $ins = $this->Pasien_rd_model->insert_resep_to_apotek($data_reseppasien2);
                $ins = $this->list_phatologi_model->insert_reseppasien($data_reseppasien);
                if($ins){

                    if (!empty($dataRacikan)) {
                        $dataRacikan = json_decode($dataRacikan);
                        foreach($dataRacikan as $item) {
                            $barang_farmasi_id = $item->obat->id;
                            $jumlah = $item->jumlah;
                            $sediaan_obat_id = $item->sediaan->id;
                            $reseptur_id = $ins;

                            $dataRacikanArr = array(
                                "barang_farmasi_id" => $barang_farmasi_id,
                                "jumlah" => $jumlah,
                                "sediaan_obat_id"   => $sediaan_obat_id,
                                "reseptur_id"   => $reseptur_id
                            );
                            $this->list_phatologi_model->insert_reseppasien_racikan($dataRacikanArr);
                        }
                    }
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Resep berhasil ditambahkan'
                    );

                    if (!empty($dataRacikan)) {
                        $dataRacikan = json_decode($dataRacikan);
                        foreach($dataRacikan as $item) {
                            $barang_farmasi_id = $item->obat->id;
                            $jumlah = $item->jumlah;
                            $sediaan_obat_id = $item->sediaan->id;
                            $reseptur_id = $ins;

                            $dataRacikanArr = array(
                                "barang_farmasi_id" => $barang_farmasi_id,
                                "jumlah" => $jumlah,
                                "sediaan_obat_id"   => $sediaan_obat_id,
                                "reseptur_id"   => $reseptur_id
                            );
                            $this->list_phatologi_model->insert_reseppasien_racikan($dataRacikanArr);
                        }
                    }
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Resep berhasil ditambahkan'
                    );

                    $perms = "phatologi_anak_view";
                    $comments = "Berhasil menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "phatologi_anak_view";
                    $comments = "Gagal menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }
    }

    public function ajax_list_obat(){
		$data_obat = $this->list_phatologi_model->get_list_obat();

		$data = array();
		foreach($data_obat as $obat){
			$row = array();

			$row[] = $obat->nama_barang;
			$row[] = $obat->nama_jenis;
			$row[] = $obat->nama_sediaan;
			$row[] = number_format($obat->harga_satuan);
			$row[] = number_format($obat->stok_akhir);
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihObat('."'".$obat->kode_barang."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" =>   $this->list_phatologi_model->count_list_obat(),
			"recordsFiltered" =>   $this->list_phatologi_model->count_list_filtered_obat(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
    }
    
    public function ajax_get_obat_by_id(){
		$kode_barang = $this->input->get('kode_barang',TRUE);

		$data_obat = $this->list_phatologi_model->get_obat_by_id($kode_barang);
		if (sizeof($data_obat)>0){
			$obat = $data_obat[0];

			$res = array(
				"success" => true,
				"messages" => "Data obat ditemukan",
				"data" => $obat
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data obat tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
    }
    
    public function ajax_list_resep_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->list_phatologi_model->get_resep_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $resep){
            $tgl_ = date_create($resep->tgl_reseptur);
            $row = array();
            $row[] = date_format($tgl_, 'd-M-Y');
            $row[] = $resep->nama_barang;
            $row[] = $resep->qty;
            $row[] = $resep->harga_jual;
            // $row[] = $resep->satuan;
            // $row[] = $resep->signa;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus Resep" onclick="hapusResep('."'".$resep->reseptur_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

}
    ?>