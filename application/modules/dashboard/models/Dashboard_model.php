<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct(){
        parent::__construct();
    }

    public function count_rj(){
        $this->db->select('m_pasien.no_rekam_medis, m_pasien.pasien_nama');
        $this->db->from('t_pendaftaran');
        $this->db->join('m_pasien', 'm_pasien.pasien_id = t_pendaftaran.pasien_id');
        $this->db->distinct();
        // $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime(datE('Y-m-d')." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime(datE('Y-m-d')." 23:59:59")).'"');
        $this->db->where('t_pendaftaran.status_pasien !=', 0);
        $this->db->where('t_pendaftaran.instalasi_id', 1);
        $this->db->where('t_pendaftaran.no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');
        return $this->db->count_all_results();
    }

    public function getBranchList() {
        $this->db->select('id_branch,nama');
        $this->db->from('m_branch');
        $query = $this->db->get();
        return $query->result();
    }

    public function getStatusCheckin($id,$tanggal) {
        $this->db->select('status');
        $this->db->from('t_absen');
        $this->db->where('id_user',$id);
        $this->db->like('absen_date',$tanggal);
        $this->db->order_by("absen_date", "desc");
        $query = $this->db->get();
        $ret = $query->row();
        if ($ret) {
            return $ret->status;
        }
        else {
            return "BELUM CHECKIN";
        }
        
    }

    public function checkin($data=array()){
        $insert = $this->db->insert('t_absen',$data);
        return $insert;
    }

    public function checkout($data=array()){
        $insert = $this->db->insert('t_absen',$data);
        return $insert;
    }


    public function count_ri(){
        $this->db->select('m_pasien.no_rekam_medis, m_pasien.pasien_nama');
        $this->db->from('t_pendaftaran');
        $this->db->join('m_pasien', 'm_pasien.pasien_id = t_pendaftaran.pasien_id');
        $this->db->distinct();
        // $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime(datE('Y-m-d')." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime(datE('Y-m-d')." 23:59:59")).'"');
        $this->db->where('t_pendaftaran.status_pasien !=', 0);
        $this->db->where('t_pendaftaran.instalasi_id', 3);
        return $this->db->count_all_results();
    }

    public function count_rd(){
        $this->db->select('m_pasien.no_rekam_medis, m_pasien.pasien_nama');
        $this->db->from('t_pendaftaran');
        $this->db->join('m_pasien', 'm_pasien.pasien_id = t_pendaftaran.pasien_id');
        $this->db->distinct();
        // $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime(datE('Y-m-d')." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime(datE('Y-m-d')." 23:59:59")).'"');
        $this->db->where('t_pendaftaran.status_pasien !=', 0);
        $this->db->where('t_pendaftaran.instalasi_id', 2);
        return $this->db->count_all_results();
    }

    public function count_penunjang(){
        $this->db->select('m_pasien.no_rekam_medis, m_pasien.pasien_nama');
        $this->db->from('t_pendaftaran');
        $this->db->join('m_pasien', 'm_pasien.pasien_id = t_pendaftaran.pasien_id');
        $this->db->distinct();
        // $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime(datE('Y-m-d')." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime(datE('Y-m-d')." 23:59:59")).'"');
        $this->db->where('t_pendaftaran.status_pasien !=', 0);
        $this->db->where('t_pendaftaran.instalasi_id', 4);
        return $this->db->count_all_results();
    }

}