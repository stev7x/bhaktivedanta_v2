<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Dashboard_model');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);

        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;


        // echo "<pre>";
        // print_r($this->data['groups'][0]->group_id);die();
        // echo "</pre>";
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('dashboard_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        $this->data['count_rj'] = $this->Dashboard_model->count_rj();
        $this->data['count_ri'] = $this->Dashboard_model->count_ri();
        $this->data['count_rd'] = $this->Dashboard_model->count_rd();
        $this->data['count_penunjang'] = $this->Dashboard_model->count_penunjang();
        
        // print_r($this->session->tempdata('data_session')); die();
        // // print_r($this->Dashboard_model->getStatusCheckin($this->data['users']->id,date("Y-m-d"))); die();
    	$this->load->view('dashboard', $this->data);
    }

    public function do_checkin(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('todo','Todo', 'required|trim');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
           

        }else if ($this->form_validation->run() == TRUE)  {
            $todo = $this->input->post('todo', TRUE);
            $absen_date = date("Y-m-d H:i:s");
            $user_id = $this->aauth->get_user()->id;
            $branch_id = $this->session->tempdata('data_session')[0];
            $status = 'CHECKIN';
            $data_tindakanpasien = array(
                'id_user'        => $user_id,
                'id_branch'      => $branch_id,
                'to_do'          => $todo,
                'absen_date'     => $absen_date,
                'status'         => $status
            );

            $ins = $this->Dashboard_model->checkin($data_tindakanpasien);
            
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');
                
                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
	
    }

    public function do_checkout(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('todo','Todo', 'required|trim');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
           

        }else if ($this->form_validation->run() == TRUE)  {
            $todo = $this->input->post('todo', TRUE);
            $absen_date = date("Y-m-d H:i:s");
            $user_id = $this->aauth->get_user()->id;
            $branch_id = $this->session->tempdata('data_session')[0];
            $status = 'CHECKOUT';
            $data_tindakanpasien = array(
                'id_user'        => $user_id,
                'id_branch'      => $branch_id,
                'to_do'          => $todo,
                'absen_date'     => $absen_date,
                'status'         => $status
            );

            $ins = $this->Dashboard_model->checkout($data_tindakanpasien);
            
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');
                
                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
	
    }
    
}
