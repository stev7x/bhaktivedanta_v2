<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class List_sesudah_operasi_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }

    public function get_pasienok_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $nama_dokter, $pasien_alamat,$no_bpjs,$no_pendaftaran){
        $this->db->from('v_sesudah_operasi');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs, $no_pendaftaran){
        $this->db->from('v_sesudah_operasi');
         $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        
        return $this->db->count_all_results();
    }


    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        
        return $query->result();
    }


    public function get_kamar_aktif($ruangan_id, $kelasruangan){
        $this->db->order_by('no_kamar', 'ASC');
        $query = $this->db->get_where('m_kamarruangan', array('poliruangan_id' => $ruangan_id, 'kelaspelayanan_id' => $kelasruangan, 'status_aktif' => '1'));
        
        return $query->result();
    }

    public function get_kamar_sebelumnya($pendaftaran_id){
        $this->db->select('t_masukkamar.poliruangan_id,t_masukkamar.kelaspelayanan_id,t_masukkamar.kamarruangan_id');
        $this->db->from('t_masukkamar');
        $this->db->join('t_pasienadmisi','t_pasienadmisi.pasienadmisi_id = t_masukkamar.pasienadmisi_id', 'left');
        $this->db->where('t_pasienadmisi.pendaftaran_id', $pendaftaran_id);
        
        $query = $this->db->get();

        return $query->row();


    }


    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_sesudah_operasi', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();   
    }

    public function get_theraphy_pasien(){
        $this->db->from('m_program_therapy');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }


     public function get_resep_pasien_list($pendaftaran_id){
        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_resepturpasien.id_sediaan','left');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_resepturpasien.id_jenis_barang','left');
        $this->db->from('t_resepturpasien');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }


    public function get_background_pasien_list($pendaftaran_id){
        $this->db->select('t_alergi_pasien.*, m_alergi.nama_alergi');
        $this->db->from('t_alergi_pasien');
        $this->db->join('m_alergi','m_alergi.alergi_id = t_alergi_pasien.alergi_id');
        $this->db->where('t_alergi_pasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }


    public function update_pendaftaran($data, $id){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

     public function insert_tindakanpasien_po($data=array()){
        $insert = $this->db->insert('t_tindakanpasien',$data);
        return $insert;
    }
    
     
    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_INAP));
        
        return $query->result();
    }

    public function insert_detassestment($data=array()){
       // $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_assestment_therapy_pasien', $data);

        return $insert;
    }

    public function get_look_pasien(){
        $this->db->from('m_lookup');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }


    public function get_rencana_tindakan_pasien(){
        $this->db->from('m_tindakan');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_paket($paketoperasi_id){
        $query = $this->db->get_where('m_paketoperasidetail', array('paketoperasi_id' => $paketoperasi_id));
        
        return $query->result();
    }


    public function get_diagnosa_pasien_list($pendaftaran_id){
        $this->db->from('t_diagnosapasien');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }


    public function get_tindakan_pasien_list($pendaftaran_id){
       $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama'); 
        $this->db->from('t_tindakanpasien'); 
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);   
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }


    public function get_dokter_umum(){
        $this->db->from('m_dokter');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }
      public function get_last_pendaftaran_pasien($id){
        $q = $this->db->get_where('m_pasien', array('pasien_id' => $id));
        return $q->row();
    }

      public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }


     public function insert_assestment($data=array()){
        $insert = $this->db->insert('t_assestment_pasien',$data);
        if ($insert) {
            $this->db->select("assestment_pasien_id");
            $this->db->from("t_assestment_pasien");
            $this->db->order_by("assestment_pasien_id", "desc");
            $this->db->limit(1);
            $data = $this->db->get()->row();
            return $data->assestment_pasien_id;
    }
    return 0;
}



    public function count_all_list_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $no_bpjs, $no_pendaftaran){
        $this->db->from('v_sesudah_operasi');
        
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_sesudah_operasi', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();   
    }

    public function get_theraphy_pasien(){
        $this->db->from('m_program_therapy');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    function get_sediaan_list(){
        return $this->db->get("m_sediaan_obat")->result();
    }

    public function get_barang_stok_list(){
        $this->db->select("*");
        $this->db->from("m_barang_farmasi");
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $query = $this->db->get();
        return $query->result();
    }
}


?>