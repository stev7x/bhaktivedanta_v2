<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                                 <input type="hidden" id="instalasi_id" name="instalasi_id" value="<?php echo $list_pasien->instalasi_id; ?>"> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>


  
            <div class="modal-body" style="background: #fafafa"> 
        <div class="row">
        <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                            <label>Data Kamar Sebelumnya</label>
                            </div>
                            <label>Poliklinik/Ruangan<span style="color:red">*</span></label>
                            <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="" readonly>
                            <input type="hidden" id="pasien_id" name="pasien_id" value="" readonly>
                            <input type="hidden" id="no_pendaftaran" name="no_pendaftaran" value="" readonly>
                            <?php
                                $get_kamarseb = $this->list_sesudah_operasi_model->get_kamar_sebelumnya($list_pasien->pendaftaran_id);
                            ?>
                            <select  name="poli_ruangan" class="form-control">
                                <option value=""  >Pilih Poli/Ruangan</option>
                                <?php
                                $list_ruangan = $this->list_sesudah_operasi_model->get_ruangan();
                                foreach($list_ruangan as $list){
                                    if ($list->poliruangan_id == $get_kamarseb->poliruangan_id) {
                                        echo "<option value='".$list->poliruangan_id."'selected>".$list->nama_poliruangan."</option>";
                                    }
                                    else
                                        echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                }
                                ?>
                            </select> 
                        </div>
                        <div class="form-group">
                            <label>Kelas Pelayanan<span style="color:red">*</span></label>
                            <select name="kelas_pelayanan" class="form-control">
                                <option value=""  selected>Pilih Kelas Pelayanan</option>
                                <?php
                                $list_ruangan = $this->list_sesudah_operasi_model->get_kelas_poliruangan($get_kamarseb->poliruangan_id);
                                foreach($list_ruangan as $list){
                                    if ($list->kelaspelayanan_id == $get_kamarseb->kelaspelayanan_id) {
                                        echo "<option value='".$list->kelaspelayanan_id."'selected>".$list->kelaspelayanan_nama."</option>";
                                    }
                                    else
                                        echo "<option value='".$list->kelaspelayanan_id."'>".$list->kelaspelayanan_nama."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Kamar<span style="color: red;"> *</span></label>
                            <select name="kamarruangan" class="form-control">
                                <option value=""  selected>Pilih Kamar</option>
                                  <?php
                                $list_ruangan = $this->list_sesudah_operasi_model->get_kamar_aktif($get_kamarseb->poliruangan_id,$get_kamarseb->kelaspelayanan_id);
                                foreach($list_ruangan as $list){
                                     $status = $list_kamar->status_bed == '1' ? 'IN USE' : 'OPEN';
                                    if ($list->kamarruangan_id == $get_kamarseb->kamarruangan_id) {
                                        echo "<option value='".$list->kamarruangan_id."'selected>".$list->no_kamar." - ".$list->no_bed."(".$status.")</option>";
                                    }
                                    else
                                        echo "<option value='".$list->kamarruangan_id."'>".$list->no_kamar." - ".$list->no_bed."(".$status.")</option>";
                                }
                                ?>

                            </select>
                        </div>
        </div>
                       
         
                       <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                            <label>Pengajuan Perubahan Kelas</label>
                            </div>
                            <label>Poliklinik/Ruangan<span style="color:red">*</span></label>
                            <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="" readonly>
                            <input type="hidden" id="pasien_id" name="pasien_id" value="" readonly>
                            <input type="hidden" id="no_pendaftaran" name="no_pendaftaran" value="" readonly>
                            <select id="poli_ruangan" name="poli_ruangan" class="form-control select2" onchange="getKelasPoliruangan()">
                                <option value=""  selected>Pilih Poli/Ruangan</option>
                                <?php
                                $list_ruangan = $this->list_sesudah_operasi_model->get_ruangan();
                                foreach($list_ruangan as $list){
                                    echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                }
                                ?>
                            </select> 
                        </div>
                        <div class="form-group">
                            <label>Kelas Pelayanan<span style="color:red">*</span></label>
                            <select id="kelas_pelayanan" name="kelas_pelayanan" class="form-control select2" onchange="getKamar()">
                                <option value=""  selected>Pilih Kelas Pelayanan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Kamar<span style="color: red;"> *</span></label>
                            <select id="kamarruangan" name="kamarruangan" class="form-control select2">
                                <option value=""  selected>Pilih Kamar</option>

                            </select>
                        </div>

                        <div class="form-group col-sm-13">
                            <label class="control-label">Catatan<span style="color: red;"> *</span></label>
                            <textarea class="form-control" rows="5" id="catatan"></textarea>
                    </div> 

                    </div>
                </div>
                </div>
            </div>


            <!-- <div class="row"> -->
                <div class="col-md-3" style="float: right">
                 <button type="button" class="btn btn-success col-md-12" id="savePembayaran" onclick="saveRawatInap()"><i class="fa fa-floppy-o p-r-9"></i >Ubah Kelas</button>
                 
                 </div>
                    <!-- </div>   -->
                
            
        
<!-- ClockPicker Stylesheet -->


</div>
</div>

<?php $this->load->view('footer_iframe');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/ruangoperasi/ubah_kelas.js"></script>
</body>
 
</html>  