<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="dokter_id" name="dokter_id" value="<?php echo $list_pasien->id_M_DOKTER;?>">
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="nav-item active"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Diagnosa</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#assestment" class="nav-link" aria-controls="assestment" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Assestment</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tindakans" class="nav-link" aria-controls="tindakans" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Tindakan</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#background" class="nav-link" aria-controls="background" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Background</span></a></li>
                <li role="presentation" class="nav-item"><a href="#obat" class="nav-link" aria-controls="obat" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Reseptur Obat</span></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content"> 
                <div role="tabpanel active" class="tab-pane fade in active" id="diagnosa">
                    <table id="table_diagnosa_pasienrj" class="table table-striped dataTable" cellspacing="0">
                        <thead>  
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>   
                                <th>Kode ICD 10</th>   
                                <th>Nama ICD 10</th>   
                                <th>Jenis Diagnosa</th>
                                <th>Diagnosa By</th>
                                <?php
                                    if($list_pasien->status_periksa == "SUDAH BAYAR"){}
                                    else{
                                    ?>
                                <th>Batal / Hapus</th>
                                <?php } ?>
                            </tr> 
                        </thead>
                        <tbody>
                                <tr>
                                    <td colspan="6">No Data to Display</td>
                                </tr>
                        </tbody> 
                        <tfoot>
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>
                                <th>Kode ICD 10</th>   
                                <th>Nama ICD 10</th>   
                                <th>Jenis Diagnosa</th>  
                                <th>Diagnosa By</th>
                                <?php
                                    if($list_pasien->status_periksa == "SUDAH BAYAR"){}
                                    else{
                                    ?>
                                <th>Batal / Hapus</th>
                                <?php } ?> 
                            </tr> 
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                    <input type="hidden" id="dokter_id_diagnosa" name="dokter_id_diagnosa" value="<?php echo $list_pasien->id_M_DOKTER; ?>">  
                    <input type="hidden" id="diagnosa_id" name="diagnosa_id">  
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                        <div >
                            <p id="card_message"></p>
                        </div>
                    </div>
                    <?php 
                        if($list_pasien->status_periksa == "SUDAH BAYAR"){}
                        else {
                    ?>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="control-label">
                            Kode Diagnosa<span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_diagnosa" name="kode_diagnosa" placeholder="Kode Diagnosa">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" title="Lihat List Diagnosa" data-toggle="modal" data-target="#modal_diagnosa" onclick="dialogDiagnosa()"><i class="fa fa-list"></i></button>
                                </span>
                            </div>
                        </div> 
                        <div class="form-group col-md-3">   
                            <label class="control-label">Nama Diagnosa<span style="color: red;"> *</span></label>
                            <input id="nama_diagnosa" type="text" name="nama_diagnosa" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="control-label">
                            Kode Diagnosa ICD 10<span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_diagnosa_icd10" name="kode_diagnosa_icd10" placeholder="Kode Diagnosa">
                            </div>
                        </div> 
                        <div class="form-group col-md-3">   
                            <label class="control-label">Nama Diagnosa ICD 10<span style="color: red;"> *</span></label>
                            <input id="nama_diagnosa_icd10" type="text" name="nama_diagnosa_icd10" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                        <div class="form-group col-md-3">   
                            <label class="control-label">Jenis Diagnosa<span style="color: red;"> *</span></label>
                            <select id="jenis_diagnosa" name="jenis_diagnosa" class="form-control" >
                                <option value="" disabled selected>Pilih Jenis Diagnosa</option>
                                <option value="1">Diagnosa Utama</option>
                                <option value="2">Diagnosa Tambahan</option>
                            </select> 
                        </div>  
                        <div class="form-group col-md-2">
                            <button type="button" style="width: 100%; margin-top: 26px;" class="btn btn-success" id="saveDiagnosa">
                                <i class="fa fa-plus"></i> TAMBAH
                            </button>  
                        </div>
                    </div>
                    <?php } ?>                    
                        <?php echo form_close();?>  
                    <div class="clearfix"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="background">
                    <?php echo form_open('#',array('id' => 'fmCreateBackgroundPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                    <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>
                    <div class="form-group col-md-4">   
                        <label class="control-label">Keluhan saat masuk<span style="color: red;"> *</span></label>
                        <input id="keluhan" type="text" name="keluhan" value="" type="text" class="form-control" placeholder="Silahkan isi Keluhan">
                    </div>
                    <table id="table_background_periksa_isolasi" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Alergi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Alergi</th>
                            </tr> 
                        </tfoot>
                    </table><br>
                    <div class="form-group col-md-2">   
                        <label class="control-label">Paket Operasi<span style="color: red;"> *</span></label>
                        <select id="paket_operasi" name="paket_operasi" class="form-control" >
                            <option value="" disabled selected>Pilih Alergi</option>
                            <option value="1">A</option>
                            <option value="2">B</option>
                        </select> 
                    </div> 
                    <div class="form-group col-md-2">
                            <label>&nbsp;</label>
                            <button style="width: 100%" type="button" class="btn btn-success" id="saveBackground">
                                <i class="fa fa-floppy-o"></i> TAMBAH ALERGI
                            </button>  
                    </div>
                    <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="assestment"> 
                    <div class="form-group col-md-12">
                        <label class="control-label">Tanda - Tanda Fital<span style="color: red;"> *</span></label>
                    </div>
                    <div class="form-group col-md-5">   
                        <input id="Tensi" type="text" name="Tensi" value="" type="text" class="form-control" placeholder="Tensi">
                    </div>
                    <div class="form-group col-md-1">
                        <span>mmHG</span>
                    </div>
                    <div class="form-group col-md-5">   
                        <input id="Nadi" type="text" name="Nadi" value="" type="text" class="form-control" placeholder="Nadi">
                    </div>
                    <div class="form-group col-md-1">
                        <span>x/menit</span>
                    </div>
                    <div class="form-group col-md-5">   
                        <input id="Suhu" type="text" name="Suhu" value="" type="text" class="form-control" placeholder="Suhu">
                    </div>
                    <div class="form-group col-md-1">
                    </div>

                    <div class="form-group col-md-5">   
                        <input id="Nadi2" type="text" name="Nadi2" value="" type="text" class="form-control" placeholder="Nadi">
                    </div>
                    <div class="form-group col-md-1">
                        <span>x/menit</span>
                    </div>

                    <div class="form-group col-md-5">   
                        <input id="Penggunaan" type="text" name="Penggunaan" value="" type="text" class="form-control" placeholder="Penggunaan">
                    </div>
                    <div class="form-group col-md-1">
                        <span>It/menit</span>
                    </div>

                    <div class="form-group col-md-5">   
                        <input id="Saturasi" type="text" name="Saturasi" value="" type="text" class="form-control" placeholder="Saturasi 02">
                    </div>
                    <div class="form-group col-md-1">
                        <span>%</span>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="Nyeri" name="Nyeri" class="form-control select2"> 
                                <option value=""  selected>Nyeri</option>
                                <option value="1">A</option>
                                <option value="2">B</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="Numeric" name="Numeric" class="form-control select2"> 
                                <option value=""  selected>Numeric/wong baker</option>
                                <option value="1">A</option>
                                <option value="2">B</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-3">   
                        <input id="Resiko" type="text" name="Nama_Tindakan_ICD" value="" type="text" class="form-control" placeholder="Resiko Jatuh">
                    </div>

                    <table id="table_therapy_periksa_isolasi" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Program Therapy</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Program Therapy</th>
                            </tr> 
                        </tfoot>
                    </table><br>

                    <div class="row">
                        <div class="form-group col-md-6">   
                            <label class="control-label">Pilih  Program Therapy<span style="color: red;"> *</span></label>
                            <select id="nama_program" name="nama_program" class="form-control select2" onchange="$('#nama_therapy').val($('#nama_program option:selected').text())"> 
                                <option value=""  selected>Pilih Therapy</option>
                                <?php
                                $list_umum = $this->list_sesudah_operasi_model->get_theraphy_pasien();
                                foreach($list_umum as $list){
                                    echo "<option value='".$list->program_therapy_id."'>".$list->nama."</option>";
                                }
                                ?>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group col-md-3 " style="margin-top: 26px;">
                        <a type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" onclick="changeDataTheraphy()"><i class="fa fa-floppy-o"></i> TAMBAH</a> 
                    </div>

                    <div class="form-group col-md-7">
                        <label>&nbsp;</label>
                        <button style="width: 100%" type="button" class="btn btn-success" id="RiwayatAssestment">
                            Riwayat Assestment
                        </button>  
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tindakans"> 

                    <table id="table_tindakan_pasienrj" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <?php      
                                    if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                                    else{
                                ?>
                                <th>Batal/Hapus</th> 
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>  
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <?php      
                                    if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                                    else{
                                ?>
                                <th>Batal/Hapus</th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table><br>
                    
                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_tindakan" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                        <div>
                            <p id="card_message_tindakan"></p>
                        </div>
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />    
                    <input type="hidden" name="id_pasien" id="id_pasien" value="<?php echo $list_pasien->pasien_id; ?>">
                    <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                    <input type="hidden" id="dokter_id_tindakan" name="dokter_id_tindakan" value="<?php echo $list_pasien->id_M_DOKTER; ?>">  
                        
                    <?php 
                        if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                        else{ ?>
                        <div class="form-group col-md-5">  
                            <label class="control-label">Input Tindakan<span style="color: red;"> *</span></label>  
                            <select required id="tindakan" name="tindakan" class="form-control select2" >
                                <option value=""  selected>Pilih Tindakan</option>   
                            </select>   
                            <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly>
                        </div> 
                        <div class="form-group col-md-5">   
                            <label class="control-label">Jumlah Tindakan<span style="color: red;"> *</span></label>
                             <input id="jml_tindakan" name="jml_tindakan" class="form-control" min="1" value="" type="number" onkeyup="hitungHarga()" min="1" placeholder="Jumlah Tindakan" required>  
                            <input type="hidden" id="harga_tindakan_satuan" name="harga_tindakan_satuan" value="0" readonly> 
                            <input type="hidden" id="subtotal" name="subtotal" value="0" readonly> 
                            <input type="hidden" id="totalharga" name="totalharga" value="0" readonly>
                            <input type="hidden" id="is_cyto" name="is_cyto" value="0" readonly>
                           
                        </div>  
                        <div class="form-group col-md-2">
                            <button type="button" class="btn btn-success" style="width: 100%; margin-top: 26px;" id="saveTindakan"><i class="fa fa-plus"></i> TAMBAH</button>  
                        </div> 
                    <?php } ?>        

                    <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div>

                <div role="tabpanel" class="tab-pane" id="obat">
                <table id="table_resep_pasienrd" class="table table-striped dataTable" cellspacing="0">
                        <thead>  
                            <tr>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <!-- <th>Satuan</th>
                                <th>Signa</th> -->
                                <th>Batal / Hapus</th>
                            </tr> 
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="6">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <!-- <th>Satuan</th> -->
                                <!-- <th>Signa</th> -->
                                <th>Batal / Hapus</th>
                            </tr> 
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateResepPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delres" name="<?php echo $this->security->get_csrf_token_name()?>_delres" value="<?php echo $this->security->get_csrf_hash()?>" />  
                    <input type="hidden" id="reseptur_id" name="reseptur_id">    
                        <div class="row">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif3" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                                <div>
                                    <p id="card_message3" class=""></p>
                                </div>
                            </div>  
                            <div class="form-group col-sm-3">   
                                <label class="control-label">Nama Obat<span style="color: red;"> *</span></label>
                                <div class="input-group custom-search-form">  
                                    <input type="text" class="form-control" placeholder="Nama Obat" name="nama_obat" id="nama_obat" class="autocomplete" value="">
                                    <input type="hidden" name="id_jenis_barang" id="id_jenis_barang">
                                    <span class="input-group-btn">    
                                        <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_reseptur" onclick="dialogObat()" type="button">  <i class="fa fa-bars"></i> </button >     
                                        </span> 
                                </div>    
                            </div>
                            <div class="form-group col-sm-3">   
                                <label class="control-label">Jumlah Obat<span style="color: red;"> *</span></label>
                                <input type="number" min="1" class="form-control" id="qty_obat" name="qty_obat" placeholder="Jumlah Obat">
                                <input id="obat_id" type="hidden" name="obat_id" value="" readonly>
                                <input id="current_stok" type="hidden" name="current_stok" value="" readonly>   
                           </div>
                            <div class="form-group col-sm-3">   
                                <label class="control-label">Harga Jual<span style="color: red;"> *</span></label>
                                <input type="number" class="form-control" id="harga_jual" name="harga_jual" placeholder="Harga Jual">
                                <input id="harga_netto" type="hidden" name="harga_netto" value="">
                            </div> 
                           <div class="form-group col-sm-3">   
                                <label class="control-label">Satuan Obat<span style="color: red;"> *</span></label>
                                <select required class="form-control select" id="satuan_obat" name="satuan_obat">
                                    <option value="" disabled selected>Satuan Obat</option>
                                    <?php
                                        $list_sediaan = $this->list_sesudah_operasi_model->get_sediaan_list();
                                        foreach ($list_sediaan as $list) {
                                            echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-control">
                                <input type="checkbox" name="checkbox_racikan" id="checkbox_racikan" onchange="$('#panel_racikan_obat').toggle();">
                                <label for="checkbox_racikan">Racikan</label>
                            </div>

                            <div class="panel panel-info" id="panel_racikan_obat" style="display: none">
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <table class="table table-striped" cellspacing="0">
                                            <thead>  
                                                <tr>
                                                    <th>No</th>
                                                    <th>Obat</th>
                                                    <th>Jumlah</th>
                                                    <th>Sediaan</th>
                                                    <!-- <th>Satuan</th>
                                                    <th>Signa</th> -->
                                                    <th>Hapus</th>
                                                </tr> 
                                            </thead>
                                            <tbody id="table_racikan_pasienrd">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Obat</th>
                                                    <th>Jumlah</th>
                                                    <th>Sediaan</th>
                                                    <!-- <th>Satuan</th>
                                                    <th>Signa</th> -->
                                                    <th>Hapus</th>
                                                </tr> 
                                            </tfoot>
                                        </table>

                                        <div class="alert alert-success alert-dismissable row" id="modal_notif4" style="display:none;">
                                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                                            <div>
                                                <p id="card_message4" class=""></p>
                                            </div>
                                        </div>  

                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Obat<span style="color: red;"> *</span></label>
                                                <select required class="form-control select2" id="obat_racikan" name="obat_racikan">
                                                    <option value="" disabled selected>Obat</option>
                                                    <?php
                                                        $list_obat = $this->list_sesudah_operasi_model->get_barang_stok_list();
                                                        // print_r($list_obat);
                                                        foreach ($list_obat as $list) {
                                                            echo "<option value=".$list->id_barang.">".$list->nama_barang." (".$list->kode_barang.")</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="control-label">Jumlah<span style="color: red;"> *</span></label>
                                                <input type="number" class="form-control" id="jumlah_racikan" name="jumlah_racikan" placeholder="Jumlah" value="0">
                                            </div>
                                            <div class="form-group col-sm-3">   
                                                <label class="control-label">Sediaan<span style="color: red;"> *</span></label>
                                                <select required class="form-control select" id="sediaan_racikan" name="sediaan_racikan">
                                                    <option value="" disabled selected>Sediaan</option>
                                                    <?php
                                                        $list_sediaan = $this->list_sesudah_operasi_model->get_sediaan_list();
                                                        foreach ($list_sediaan as $list) {
                                                            echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="control-label">Keterangan</label>
                                                <textarea class="form-control" placeholder="Keterangan"></textarea>
                                            </div>
                                            <div class="form-group"> 
                                                <button style="background-color: #01c0c8;" type="button" class="btn btn-success pull-right" onclick="changeDataResepRacikan()"><i class="fa fa-floppy-o"></i> TAMBAH OBAT RACIKAN</button>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="form-group col-sm-6" style="margin-top: 20px">   
                                <label class="control-label">Signa<span style="color: red;"> *</span></label>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <input type="number" class="form-control" id="signa_1" name="signa_1" value="0">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <input type="number" class="form-control" id="signa_2" name="signa_2" value="0">
                                    </div>
                                </div>
                            </div> 
                        </div>

                        <div class="form-group col-sm-2"> 
                            <button style="width: 150px; background-color: #01c0c8;" type="button" class="btn btn-success" id="saveResep"><i class="fa fa-floppy-o"></i> TAMBAH</button>  
                        </div>
                        <?php echo form_close();?>  
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer_iframe');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/ruangoperasi/periksa/kirim_periksa.js"></script>
</body>
 
</html>