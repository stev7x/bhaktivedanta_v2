<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_sesudah_operasi extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        
        $this->load->model('Menu_model');
        $this->load->model('list_sesudah_operasi_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('ruangoperasi_list_sesudah_operasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "ruangoperasi_list_sesudah_operasi_view";
        $comments = "List Pasien Rawat Darurat";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_sesudah_operasi', $this->data);
    }

    public function ajax_list_pasieniok(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $poliklinik = $this->input->get('poliklinik',TRUE);
        $status = $this->input->get('status',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis', TRUE);
        $nama_dokter = $this->input->get('nama_dokter', TRUE);
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);

        $list = $this->list_sesudah_operasi_model->get_pasienok_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $nama_dokter, $pasien_alamat,$no_bpjs,$no_pendaftaran);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $tgl_lahir = $pasienrd->tanggal_lahir;
            $umur  = new Datetime($tgl_lahir);
            $today = new Datetime();
            $diff  = $today->diff($umur);

            $no++;
            $row = array();
            $row[] = $pasienrd->no_pendaftaran;
            $row[] = $pasienrd->no_rekam_medis;
            $row[] = $pasienrd->no_bpjs ;
            $row[] = $pasienrd->pasien_nama;
            $row[] = $pasienrd->tgl_pendaftaran;
            $row[] = $pasienrd->jenis_kelamin;
            // $row[] = $pasienrd->umur;
            $row[] = $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
            $row[] = $pasienrd->pasien_alamat;
            $row[] = $pasienrd->type_pembayaran;
            $row[] = $pasienrd->kelaspelayanan_nama;
            $row[] = $pasienrd->status_periksa;
            $row[] = !empty($pasienrd->carapulang) ? $pasienrd->carapulang : 'Not Set';
            //Action
            $row[] = '<div class="dropdown-action" onclick="$(this).children().toggleClass('."'d-block'".')">
                        <span class="title-dropdown">Pilih Proses</span><span class="arrow">▼</span>
                        <div class="dropdown-menu-action">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_pasienrd" title="Klik untuk pemeriksaan pasien" onclick="cobaTest2('."'".$pasienrd->pendaftaran_id."'".')">PERIKSA</button>                          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_pasienrd" title="Klik untuk pemeriksaan pasien">PERIKSA</button> 
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_pasienrd" title="Klik untuk pemeriksaan pasien">PERIKSA</button>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_ranap_pasienrd" title="Klik untuk kirim rawat inap" onclick="kirimRanap('."'".$pasienrd->pendaftaran_id."'".')">K.K RAWAT INAP</button>  
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_ubah_kelas" title="Klik untuk kirim rawat inap" onclick="ubahKelas('."'".$pasienrd->pendaftaran_id."'".')">UBAH KELAS</button>  
                        <button type="button" class="btn btn-success"  title="K.K OK">K.K OK</button>
                        <button type="button" class="btn btn-success"  title="K.K VK">K.K VK</button>
                        <button type="button" class="btn btn-success"  title="K.K HCU">K.K HCU</button>
                        <button type="button" class="btn btn-success"  title="K.K Pathologi Anak">K.K Pathologi Anak</button>
                        <button type="button" class="btn btn-success"  title="Rujuk Rs Lain">Rujuk Rs Lain</button>
                        <button type="button" class="btn btn-success"  title="Batal">Batal</button>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_penunjang" title="Klik untuk kirim ke penunjang">PENUNJANG</button>
                        </div>
                    </div>
                ';

            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->list_sesudah_operasi_model->count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $nama_dokter, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "recordsFiltered" => $this->list_sesudah_operasi_model->count_all_list_filtered($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $nama_dokter, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function periksa_pasienrd($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->list_sesudah_operasi_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienrd['pendaftaran_id_'] = $pendaftaran_id;
        $this->load->view('kirim_periksa', $pasienrd);
    }
//}


    public function kirim_ranap($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->list_sesudah_operasi_model->get_pendaftaran_pasien($pendaftaran_id);
        //$pasienrd['pendaftaran_id_'] = $pendaftaran_id;
        $this->load->view('kirim_rawatinapok', $pasienrd);
    }


    public function ubah_kelas($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->list_sesudah_operasi_model->get_pendaftaran_pasien($pendaftaran_id);
        //$pasienrd['pendaftaran_id_'] = $pendaftaran_id;
        $this->load->view('ubah_kelas', $pasienrd);
    }



    public function ajax_list_tindakan_pasien_ok(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->list_sesudah_operasi_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            //$row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_diagnosa_pasien_ok(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->list_sesudah_operasi_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_tindakan_list(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $list_tindakan = $this->list_sesudah_operasi_model->get_tindakan($kelaspelayanan_id);
        // $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        // foreach($list_tindakan as $list_tindak){
        //     $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        // }


        if(count($list_tindakan) >= 1){
            $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        }else{
            $list = "";
        }
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";

        }


        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    public function ajax_list_background_pasien_ok(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->list_sesudah_operasi_model->get_background_pasien_list($pendaftaran_id);
        $data = array();
        $count =0;
        foreach($list as $background){
            $row = array();
            $row[]=++$count;
            $row[] = $background->nama_alergi;
            //$row[] = $tindakan->total_harga;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

     public function ajax_list_resep_pasien_ok(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->list_sesudah_operasi_model->get_resep_pasien_list($pendaftaran_id);
        $data = array();
        $count =0;
        foreach($list as $resepp){
            $row = array();
            $row[]=++$count;
            $row[] = $resepp->nama_sediaan;
            $row[] = $resepp->qty;
            $row[] = $resepp->nama_jenis;

            //$row[] = $tindakan->total_harga;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


    public function do_create_assestment_ok(){
        $is_permit = $this->aauth->control_no_redirect('ruangoperasi_list_sesudah_operasi_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('tensi','Tensi', 'required|trim');
        $this->form_validation->set_rules('suhuok','Suhu', 'required');
        // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        //$this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "ruangoperasi_list_sesudah_operasi_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('tensi', TRUE);
            $suhu = $this->input->post('suhuok', TRUE);
            $nadi = $this->input->post('nadiok', TRUE);
            $nadi_2 = $this->input->post('nadi_1ok', TRUE);
            $penggunaan = $this->input->post('penggunaanok', TRUE);
            $saturasi = $this->input->post('saturasiok', TRUE);
            $nyeri = $this->input->post('nama_nyeriok', TRUE);
            $numeric = $this->input->post('numericok', TRUE);
            $resiko = $this->input->post('resikook', TRUE);
            $theraphyArr = $this->input->post('theraphyArr', TRUE);
            $data_pendaftaran = $this->list_sesudah_operasi_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->list_sesudah_operasi_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'pendaftaran_id' => $pendaftaran_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'nadi_2' => $nadi_2,
                'suhu' => $suhu,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko,
                'pasien_id' => $data_pendaftaran->pasien_id,
                'instalasi_id' => '3',

                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->list_sesudah_operasi_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );

                        $rencana = $this->list_sesudah_operasi_model->insert_detassestment($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "ruangoperasi_list_sesudah_operasi_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "ruangoperasi_list_sesudah_operasi_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

     function get_kelasruangan_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_kelasruangan = $this->list_sesudah_operasi_model->get_kelas_poliruangan($poliruangan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        foreach($list_kelasruangan as $list_kelas){
            $list .= "<option value='".$list_kelas->kelaspelayanan_id."'>".$list_kelas->kelaspelayanan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }


    function get_kamarruangan(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $list_kamarruangan = $this->list_sesudah_operasi_model->get_kamar_aktif($poliruangan_id, $kelaspelayanan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kamar</option>";
        foreach($list_kamarruangan as $list_kamar){
            $status = $list_kamar->status_bed == '1' ? 'IN USE' : 'OPEN';
            if($list_kamar->status_bed == '0'){
                $list .= "<option value='".$list_kamar->kamarruangan_id."'>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }else{
                $list .= "<option value='".$list_kamar->kamarruangan_id."' disabled>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }


        public function do_create_ranap_ok(){
        $is_permit = $this->aauth->control_no_redirect('ruangoperasi_list_sesudah_operasi_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
      $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $instalasi_id = $this->input->post('instalasi_id', TRUE);
           // $catatan = $this->input->post('catatan', TRUE);
            $paketOperasiArr = $this->input->post('paketOperasiArr', TRUE);

            $data_pendaftaran = $this->list_sesudah_operasi_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->list_sesudah_operasi_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $dataadmisi = array(
                 'status_periksa' => 'SEDANG RAWAT INAP',
            );
            $ins = $this->list_sesudah_operasi_model->update_pendaftaran($dataadmisi, $pendaftaran_id);
            //$ins=true;
            if($ins != 0){
                $paketOperasiArr = json_decode($paketOperasiArr);
                if (count($paketOperasiArr) > 0 ) {
                    foreach ($paketOperasiArr as $item) {
                        $data = array(
                            'daftartindakan_id' =>$item->id_nama_paket,
                            'instalasi_id' => '3',
                            'pasien_id' => $ins
                        );

                        $rencana = $this->list_sesudah_operasi_model->insert_tindakanpasien_po($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }




}
?>