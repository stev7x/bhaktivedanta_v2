<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class list_fisiologi_anak_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }

    public function get_pasienph_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran){
        $this->db->from('v_fisiologi');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }
     public function get_last_pendaftaran_pasien($id){
        $q = $this->db->get_where('m_pasien', array('pasien_id' => $id));
        return $q->row();
    }

    public function count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs, $no_pendaftaran){
        $this->db->from('v_fisiologi');
         $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        
        return $this->db->count_all_results();
    }


    public function count_all_list_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $no_bpjs, $no_pendaftaran){
        $this->db->from('v_fisiologi');
        
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_fisiologi', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();   
    }

    public function get_dokter_umum(){
        $this->db->from('m_dokter');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    public function get_theraphy_pasien(){
        $this->db->from('m_program_therapy');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    function get_sediaan_list(){
        return $this->db->get("m_sediaan_obat")->result();
    }

    public function get_barang_stok_list(){
        $this->db->select("*");
        $this->db->from("m_barang_farmasi");
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $query = $this->db->get();
        return $query->result();
    }

    function get_sedia_alergi(){
        return $this->db->get("m_alergi")->result();
    }

    public function insert_diagnosapasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_diagnosapasien',$data);
        return $insert;
    }

    public function get_diagnosa_pasien_list($pendaftaran_id){
        $this->db->from('t_diagnosapasien');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function insert_tindakanpasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_tindakanpasien',$data);

        return $insert;
    }

    public function update_to_sudahperiksa($id){
        $data = array(
            'status_periksa' => 'SUDAH PERIKSA'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

    function get_tindakan_ke($id){
        $sql = 'SELECT MAX(imun_ke) AS tindakan_max FROM t_pasienimunisasi WHERE pasien_id = "'.$id.'"';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_tindakan($kelaspelayanan_id, $is_bpjs){
        // $this->db->select('m_tindakan.*');
        $this->db->from('m_tindakan');  
        $this->db->where('m_tindakan.kelaspelayanan_id',$kelaspelayanan_id);   
        $this->db->where('m_tindakan.kelompoktindakan_id',2);         
        
        // $this->db->where('m_tindakan.komponentarif_id','1');  
        if(strtoupper($is_bpjs) == strtoupper("BPJS")){ 
            $this->db->where('m_tindakan.bpjs_non_bpjs','BPJS');
        }else{  
            $this->db->where('m_tindakan.bpjs_non_bpjs','Non BPJS');   
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_pasien_list($pendaftaran_id){ 
        $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama');
        $this->db->from('t_tindakanpasien'); 
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);   
        $length = $this->input->get('length');   
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_tarif_tindakan($daftartindakan_id){
        $query = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $daftartindakan_id), 1, 0);
 
        return $query->row();   
    }

    public function insert_backgroundpasien($pendaftaran_id , $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);        
        $insert = $this->db->insert('t_alergi_pasien',$data);
        return $insert;
    }

    public function insert_theraphy_pasien($pendaftaran_id , $data=array()){
        $insert = $this->db->insert('t_assestment_therapy_pasien',$data);
        return $insert;
    }

    public function insert_assestment($data=array()){
        $insert = $this->db->insert('t_assestment_pasien',$data);
        if ($insert) {
            $this->db->select("assestment_pasien_id");
            $this->db->from("t_assestment_pasien");
            $this->db->order_by("assestment_pasien_id", "desc");
            $this->db->limit(1);
            $data = $this->db->get()->row();
            return $data->assestment_pasien_id;
        }
        return 0;
    }

    public function insert_detassestment($data=array()){
         $insert = $this->db->insert('t_assestment_therapy_pasien', $data);
         return $insert;
    }

    public function insert_reseppasien($data=array()){
        $this->db->insert('t_resepturpasien',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function insert_reseppasien_racikan($data=array()) {
        $this->db->insert("t_resepturpasien_detail_racikan", $data);
    }

    public function get_background_pasien_list($pendaftaran_id){
        $this->db->select('t_alergi_pasien.*, m_alergi.nama_alergi');
        $this->db->from('t_alergi_pasien'); 
        $this->db->join('m_alergi','m_alergi.kode_alergi = t_alergi_pasien.alergi_id');
        $this->db->where('t_alergi_pasien.pendaftaran_id',$pendaftaran_id);   
        $length = $this->input->get('length');   
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_theraphy_pasien_list($pasien_id){
        $this->db->select('t_assestment_therapy_pasien.*, m_program_therapy.nama');
        $this->db->from('t_assestment_therapy_pasien'); 
        $this->db->join('m_program_therapy','m_program_therapy.program_therapy_id = t_assestment_therapy_pasien.program_therapy_id');
        $this->db->where('t_assestment_therapy_pasien.assestment_pasien_id',$pasien_id);   
        $length = $this->input->get('length');   
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function update_keluhan_background_pasien($pendaftaran_id,$dataa=array()){
        $this->db->update('t_pendaftaran', $dataa, array('pendaftaran_id' => $pendaftaran_id));        
    }

    public function delete_background($id){
        $delete = $this->db->delete('t_alergi_pasien', array('alergi_pasien_id' => $id));
        return $delete;
    }

    public function delete_diagnosa($id){
        $delete = $this->db->delete('t_diagnosapasien', array('diagnosapasien_id' => $id));

        return $delete;
    }

    public function get_numericwong_pasien(){
        $this->db->from('m_lookup');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    function get_list_obat(){
		// $this->db->select('*');
        //$this->db->from('v_stokobatalkes');
        // $this->db->where('qtystok !=', 0);

        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_apotek_stok.id_sediaan');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_apotek_stok.id_jenis_barang');
        $this->db->from('t_apotek_stok');
		
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    function get_obat_by_id($id){
        $this->db->select('*');
        $this->db->from('t_apotek_stok');
        $this->db->where('kode_barang =', $id);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_resep_pasien_list($pendaftaran_id){
        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_resepturpasien.id_sediaan','left');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_resepturpasien.id_jenis_barang','left');
        $this->db->from('t_resepturpasien');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_list_obat(){
        $this->db->from('t_apotek_stok');
        // $this->db->from('v_stokobatalkes');
		// $this->db->where('qtystok !=', 0);
        return $this->db->count_all_results();
    }

	function count_list_filtered_obat(){
        $this->db->from('t_apotek_stok');
        // $this->db->from('v_stokobatalkes');
		// $this->db->where('qtystok !=', 0);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_list_diagnosa(){
        // $this->db->select('*');
        $this->db->select('diagnosa2_id,diagnosa_kode,diagnosa_nama');
        // $this->db->select('diagnosa2_id,diagnosa_kode,diagnosa_nama,kode_diagnosa,nama_diagnosa');
        // $this->db->join('m_diagnosa_icd10','m_diagnosa.diagnosa2_id = m_diagnosa_icd10.diagnosa_id');
        $this->db->from('m_diagnosa');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->columnListDiagnosa as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnListDiagnosa[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->orderListDokter)){
            $orderListDiagnosa = $this->orderListDokter;
            $this->db->order_by(key($orderListDiagnosa), $orderListDiagnosa[key($orderListDiagnosa)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
  }

  function get_list_diagnosa_icd10(){
    $this->db->select('diagnosa_id,kode_diagnosa,nama_diagnosa');
    $this->db->from('m_diagnosa_icd10');

    $i = 0;
    $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->columnListDiagnosaICD10 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnListDiagnosaICD10[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->orderListDokter)){
            $orderListDiagnosaICD10 = $this->orderListDokter;
            $this->db->order_by(key($orderListDiagnosaICD10), $orderListDiagnosaICD10[key($orderListDiagnosaICD10)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
    $query = $this->db->get();

    return $query->result();
    }

    function get_diagnosa_by_id($id){
        $this->db->select('diagnosa2_id,diagnosa_kode,diagnosa_nama,kode_diagnosa,nama_diagnosa');
        $this->db->join('m_diagnosa_icd10','m_diagnosa.diagnosa2_id = m_diagnosa_icd10.diagnosa_id');
        $this->db->from('m_diagnosa');
        $this->db->where('diagnosa2_id =', $id);
        $query = $this->db->get();
        return $query->result();
    }

    
}

?>