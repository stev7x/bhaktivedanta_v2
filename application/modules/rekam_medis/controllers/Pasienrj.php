<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasienrj extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pasienrj_model');
        $this->load->model('rekam_medis/list_pasien_model');

        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['data_user'] = $this->list_pasien_model->get_data_user($this->data['users']->id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrj_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "rekam_medis_pasienrj_view";
        $comments = "List Pasien Rawat Jalan";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_pasienrj', $this->data);
    }

    public function ajax_list_pasienrj(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $poliklinik = $this->input->get('poliklinik',TRUE);
        $status = $this->input->get('status',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $nama_dokter = $this->input->get('nama_dokter',TRUE);
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $nama_klinik = $this->input->get('nama_klinik',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);

        $list = $this->Pasienrj_model->get_pasienrj_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien, $dokter_id, $nama_dokter, $nama_klinik, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrj){

            $tgl_lahir = $pasienrj->tanggal_lahir;
            $umur  = new Datetime($tgl_lahir);
            $today = new Datetime();
            $diff  = $today->diff($umur);

            $no++;
            $row = array();
            $row[] = $pasienrj->no_pendaftaran;
            $row[] = $pasienrj->no_rekam_medis."/ \n".$pasienrj->pasien_nama;
            $row[] = date('d M Y H:i:s', strtotime($pasienrj->tgl_pendaftaran));
            $row[] = $pasienrj->nama_poliruangan;
            $row[] = $pasienrj->kelaspelayanan_nama;
            $row[] = $pasienrj->NAME_DOKTER;
            $row[] = $pasienrj->jenis_kelamin;
            $row[] = $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
            $row[] = $pasienrj->pasien_alamat;
            $row[] = $pasienrj->type_pembayaran;
            $row[] = $pasienrj->status_periksa;
            $row[] = !empty($pasienrj->carapulang) ? $pasienrj->carapulang : '-';
            $row[] = !empty($pasienrj->rs_rujukan) ? $pasienrj->rs_rujukan : '-';

            // if(trim(strtoupper($pasienrj->status_periksa)) == "DIPULANGKAN"){
                // $row[] = '<button type="button" class="btn btn-success" title="Klik untuk melihat diagnosa" data-toggle="modal" data-target="#modal_diagnosa_sudahpulang" onclick="periksaPasienRjSudahPulang('."'".$pasienrj->pendaftaran_id."'".')">Diagnosa</button>';
                // $row[] = '<button type="button" class="btn btn-default" title="" disabled>Pulangkan</button>';
            // }else{
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk menambahkan diagnosa" data-toggle="modal" data-target="#modal_diagnosa" data-backdrop="static" data-keyboard="false" onclick="periksaPasienRj('."'".$pasienrj->pendaftaran_id."'".')">Diagnosa</button>';
                // $row[] = '<button type="button" class="btn btn-success" title="Klik untuk menambah status pulang" data-toggle="modal" data-target="#modal_keterangan_keluar" onclick="pulangkanPasienRj('."'".$pasienrj->pendaftaran_id."'".')">Pulangkan</button>';
            // }
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasienrj_model->count_all_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien, $dokter_id, $nama_dokter, $nama_klinik, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "recordsFiltered" => $this->Pasienrj_model->count_all_list_filtered($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien, $dokter_id, $nama_dokter, $nama_klinik, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function periksa_pasienrj($pendaftaran_id){
        $pasienrj['list_pasien'] = $this->Pasienrj_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('periksa_pasienrj', $pasienrj);
    }

    public function periksa_pasienrj_sudahpulang($pendaftaran_id){
        $pasienrj['list_pasien'] = $this->Pasienrj_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('periksa_pasienrj_sudahpulang', $pasienrj);
    }

    public function reseptur_pasienrj($pendaftaran_id){
        $pasienrj['list_pasien'] = $this->Pasienrj_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('reseptur_pasienrj', $pasienrj);
    }

    public function ajax_list_diagnosa_pasien(){
        $table = "t_diagnosapasien";
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrj_model->get_diagnosa_pasien_list($pendaftaran_id,$table);
        $data = array();
        foreach($list as $diagnosa){
            $row = array();
            $row[] = date('d M Y', strtotime($diagnosa->tgl_diagnosa));
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->kode_icd10;
            $row[] = $diagnosa->nama_icd10;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->diagnosa_by;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" id="hapusDiagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."','t_diagnosapasien'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_list_icd9(){
        $table = "t_tindakanpasien_icd9";
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrj_model->get_diagnosa_pasien_list($pendaftaran_id, $table);
        $data = array();
        foreach($list as $diagnosa){
            $row = array();
            $row[] = date('d M Y', strtotime($diagnosa->tanggal_tindakan));
            $row[] = $diagnosa->kode_icd_9_cm;
            $row[] = $diagnosa->nama_tindakan;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" id="hapusDiagnosa" onclick="hapusDiagnosa('."'".$diagnosa->tindakan_icd_id."','t_tindakanpasien_icd9'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_diagnosa_pasien_sudahpulang(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrj_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $row = array();
            $row[] = date('d M Y', strtotime($diagnosa->tgl_diagnosa));
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->diagnosa_by;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_diagnosa_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_diagnosa','Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('nama_diagnosa','Nama Diagnosa', 'required|trim');
        $this->form_validation->set_rules('jenis_diagnosa','Jenis Diagnosa', 'required');
        $this->form_validation->set_rules('kode_diagnosa_icd10','Diagnosa ICD 10', 'required');
        $this->form_validation->set_rules('nama_diagnosa_icd10','Nama Diagnosa ICD 10', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrj_view";
            $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id          = $this->input->post('pasien_id', TRUE);
            $dokter             = $this->input->post('dokter', TRUE);
            $nama_diagnosa      = $this->input->post('nama_diagnosa', TRUE);
            $kode_icd10         = $this->input->post('kode_diagnosa_icd10', TRUE);
            $nama_icd10         = $this->input->post('nama_diagnosa_icd10', TRUE);
            $kd_diagnosa        = $this->input->post('kode_diagnosa', TRUE);
            $jenis_diagnosa     = $this->input->post('jenis_diagnosa', TRUE);
            $data_diagnosapasien = array(
                'nama_diagnosa'  => $nama_diagnosa,
                'tgl_diagnosa'   => date('Y-m-d'),
                'kode_diagnosa'  => $kd_diagnosa,
                'kode_icd10'     => $kode_icd10,
                'nama_icd10'     => $nama_icd10,
                'kode_diagnosa'  => $kd_diagnosa,
                'jenis_diagnosa' => $jenis_diagnosa,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id'      => $pasien_id,
                'dokter_id'      => $dokter,
                'diagnosa_by'    => 'RM'
            );
            // cek diagnosa primer
            $query = $this->db->get_where('t_diagnosapasien', array('pendaftaran_id'=>$pendaftaran_id,'1'=>$jenis_diagnosa,'kode_icd10'=> $kode_icd10));
            if ($query->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Diagnosa Utama Hanya Bisa Diinput Satu Kali');

                    // if permitted, do logit
                    $perms      = "master_data_dokter_ruangan_view";
                    $comments   = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{

                $table = "t_diagnosapasien";

                $ins = $this->Pasienrj_model->insert_diagnosapasien($pendaftaran_id, $data_diagnosapasien, $table);
                //$ins=true;
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Diagnosa berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienrj_view";
                    $comments = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienrj_view";
                    $comments = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }

    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrj_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $row = array();
            $row[] = date('d M Y', strtotime($tindakan->tgl_tindakan));
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            // $row[] = "Rp ".number_format($tindakan->total_harga_tindakan);
            // $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            $row[] = "Rp ".number_format($tindakan->total_harga);
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_delete_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosapasien_id = $this->input->post('diagnosapasien_id', TRUE);
        $table = $this->input->post('table', TRUE);
        // $table = "t_diagnosapasien"
        $delete = $this->Pasienrj_model->delete_diagnosa($diagnosapasien_id, $table);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrj_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrj_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function set_status_pulang(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        $this->form_validation->set_rules('carapulang','Cara Pulang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrj_view";
            $comments = "Gagal set status pulang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $carapulang = $this->input->post('carapulang', TRUE);
            $rs_rujukan = $this->input->post('nama_rs', TRUE);
            $user_id = $this->data['users']->id;

            $data = array(
                'status_periksa' => 'DIPULANGKAN',
                'carapulang' => $carapulang,
                'rs_rujukan' => $rs_rujukan,
                'petugas_id' => $user_id
            );

            $update_pendaftaran = $this->Pasienrj_model->update_pendaftaran($pendaftaran_id, $data);

            if($update_pendaftaran){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Berhasil set status pulang'
                );
                // if permitted, do logit
                $perms = "rekam_medis_pasienrj_view";
                $comments = "Berhasil set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal set status pulang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rekam_medis_pasienrj_view";
                $comments = "Gagal set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function ajax_list_diagnosa($icd){
        $table = "";
        if ($icd == "icd9") {
            $table = "m_icd_9_cm";
        } else if ($icd == "icd10") {
            $table = "m_diagnosa_icd10";
        }

        $list = $this->Pasienrj_model->get_list_diagnosa($table);
        $data = array();
        foreach($list as $diagnosa){
            $row = array();

            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            // $row[] = $diagnosa->kelompokdiagnosa_nama;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Diagnosa" onclick="pilihDiagnosa('."'".$diagnosa->diagnosa_id."','".$table."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Pasienrj_model->count_list_diagnosa($table),
            "recordsFiltered" => $this->Pasienrj_model->count_list_filtered_diagnosa($table),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

     public function ajax_list_diagnosa10(){
		$data_diagnosa = $this->Pasienrj_model->get_list_diagnosa10();
		$data = array();
		foreach($data_diagnosa as $diagnosa){
			$row = array();
			$row[] = $diagnosa->diagnosa_kode;
			$row[] = $diagnosa->diagnosa_nama;
			$row[] = $diagnosa->kode_diagnosa;
			$row[] = $diagnosa->nama_diagnosa;
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihDiagnosa10('."'".$diagnosa->diagnosa2_id."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" => $this->Pasienrj_model->count_list_diagnosa10(),
			"recordsFiltered" => $this->Pasienrj_model->count_list_filtered_diagnosa10(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}

    public function ajax_get_diagnosa_by_id10(){
		$diagnosa2_id = $this->input->get('diagnosa2_id',TRUE);

		$data_diagnosa = $this->Pasienrj_model->get_diagnosa_by_id10($diagnosa2_id);
		if (sizeof($data_diagnosa)>0){
			$diagnosa = $data_diagnosa[0];

			$res = array(
				"success" => true,
				"messages" => "Diagnosa obat ditemukan",
				"data" => $diagnosa
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data Diagnosa tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
	}

    public function ajax_get_diagnosa_by_id($id,$table){
        // $diagnosa_id = $this->input->get('diagnosa_id',TRUE);
        $data_diagnosa = $this->Pasienrj_model->get_diagnosa_by_id($id,$table);
        if (sizeof($data_diagnosa)>0){
            $diagnosa = $data_diagnosa[0];
            $res = array(
                "success" => true,
                "messages" => "Data diagnosa ditemukan",
                "data" => $diagnosa
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data diagnosa tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }

    public function ajax_list_rs_rujukan(){
        $list = $this->Pasienrj_model->get_list_rs_rujukan();

        $data = array();
        foreach($list as $rs_rujukan){
            $row = array();

            $row[] = $rs_rujukan->kode_rs;
            $row[] = $rs_rujukan->nama_rs;
            $row[] = $rs_rujukan->alamat_rs;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih RS Rujukan" onclick="pilihRSRujukan('."'".$rs_rujukan->rs_rujukan_id."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Pasienrj_model->count_list_rs_rujukan(),
            "recordsFiltered" => $this->Pasienrj_model->count_list_filtered_rs_rujukan(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_rs_rujukan_by_id(){
        $rs_rujukan_id = $this->input->get('rs_rujukan_id',TRUE);
        $data_rs_rujukan = $this->Pasienrj_model->get_rs_rujukan_by_id($rs_rujukan_id);
        if (sizeof($data_rs_rujukan)>0){
            $rs_rujukan = $data_rs_rujukan[0];
            $res = array(
                "success" => true,
                "messages" => "Data RS Rujukan ditemukan",
                "data" => $rs_rujukan
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data RS Rujukan tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }


    public function keterangan_keluar($pendaftaran_id){
        $pasienrj['list_pasien'] = $this->Pasienrj_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('v_keterangan_keluar_pasienRJ', $pasienrj);
    }

    public function do_create_icd9(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_icd9','Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('nama_icd9','Nama Diagnosa', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrj_view";
            $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $kode_icd9 = $this->input->post('kode_icd9', TRUE);
            $nama_icd9 = $this->input->post('nama_icd9', TRUE);
            $data_icd9 = array(
                'tanggal_tindakan' => date('Y-m-d H:i:s'),
                'kode_icd_9_cm' => $kode_icd9,
                'nama_tindakan' => $nama_icd9,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
            );

            $table = "t_tindakanpasien_icd9";

            $insert = $this->Pasienrj_model->insert_diagnosapasien($pendaftaran_id, $data_icd9, $table);
                //$ins=true;
                if($insert){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Diagnosa berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienrj_view";
                    $comments = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienrj_view";
                    $comments = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
        }
        echo json_encode($res);
    }
}
