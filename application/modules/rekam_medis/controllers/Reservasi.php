<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservasi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        // Load model
        $this->load->model('Menu_model');
        $this->load->model('Reservasi_model');
        $this->load->model('Pendaftaran_model');
        $this->load->model('Models');
        // load data
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['list_jenis_kelamin']   = array('lk'=>'Laki-laki','pr'=>'Perempuan', 'ambigu'=>'Ambigu/Belum Jelas');
        $this->data['list_type_bayar']  = array('1'=>'Pribadi','2'=>'Asuransi', '3'=>'Bill Hotel', '4'=> 'Tanggungan Hotel');
        $this->data['list_type_identitas']  = array('1'=>'KTP','2'=>'SIM','3'=>'Kartu Pelajar','4'=>'KTA','5'=>'KITAS','6'=>'Passpor','7'=>'Lainnya');
        $this->data['next_no_rm'] = $this->Reservasi_model->get_no_rm();
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "rekam_medis_pendaftaran_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('rekam_medis/v_reservasi', $this->data);
    }

    // Insert data reservasi
    public function do_create_reservasi(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $status_pasien = $this->input->post('no_rm',TRUE);
        $layanan_id = $this->input->post('layanan',TRUE);
        $tglres = $this->input->post('tgl_reservasi',TRUE);
        $tglres2 = $this->Reservasi_model->casting_date_indo($tglres);
        $count_pasien_res = $this->Reservasi_model->get_status_rm($status_pasien, $tglres2, $layanan_id);

        $no_rm = $this->input->post('no_rm',TRUE);

        $data_pendaftaran = @$this->Reservasi_model->get_pasien($no_rm);
        $data_pasien = @$this->Reservasi_model->get_pendaftaran($data_pendaftaran[0]->pasien_id);

//        echo "<pre>";
//        print_r($data_pasien);
//        exit;

        if (!empty($data_pendaftaran)) {
            if ($data_pasien[0]->status_periksa != "SUDAH BAYAR") {
                // Mengecek pasien sudah melakukan reservasi
                if ($count_pasien_res >= 1) {
                    $result = array(
                        'count' => $count_pasien_res,
                        'messages' => "Pasien sudah melakukan reservasi"
                    );

                    echo json_encode($result);
                    exit;
                }
            }
        }

        // Form validation
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pasien_nama', 'Nama Pasien', 'required|trim');
        $this->form_validation->set_rules('no_rm', 'No Rekam Medis', 'required|trim');
        $this->form_validation->set_rules('jk', 'jenis_kelamin', 'required|trim');
        $this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'required|trim');
        $this->form_validation->set_rules('metode_regis', 'metode metode_registrasi', 'required|trim');
        $this->form_validation->set_rules('tgl_reservasi', 'tanggal Reservasi', 'required|trim');
        $this->form_validation->set_rules('layanan', 'Layanan', 'required|trim');
        $this->form_validation->set_rules('jenis_pembayaran', 'Jenis Pembayaran', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pendaftaran_create";
            $comments = "Gagal melakukan pendaftaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE){
            // set local time to indoesia
            date_default_timezone_set('Asia/Jakarta');
            setlocale(LC_ALL, 'IND');
            setlocale(LC_ALL, 'id_ID');
            $no_book = $this->Reservasi_model->get_no_booking();
            $datapasien['kode_booking'] = $no_book;
            $datapasien['nama_pasien'] = $this->input->post('pasien_nama',TRUE);
            $tgl_lahir1 = $this->input->post('tanggal_lahir',TRUE);
            $date_lhr = $this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir1));
            $datapasien['tanggal_lahir'] = $date_lhr;
            $datapasien['jenis_kelamin'] = $this->input->post('jk',TRUE);
            $datapasien['alamat_pasien'] = $this->input->post('alamat',TRUE);
            $metode_regis = $this->input->post('metode_regis',TRUE);
            $datapasien['metode_registrasi'] = $metode_regis;
            $tgl_res1= $this->input->post('tgl_reservasi',TRUE);
            $date_res = $this->Reservasi_model->casting_date_indo(strtolower($tgl_res1));
            $datapasien['tanggal_reservasi'] = $date_res;
            $datapasien['kelas_pelayanan_id'] = $this->input->post('layanan',TRUE);
            $dokter_id = $this->input->post('dokter',TRUE);
            $datapasien['dokter_id'] = $dokter_id;
            // $datapasien['jam'] = $this->input->post('jam',TRUE);
            $datapasien['asuransi1'] = $this->input->post('asuransi1',TRUE);
            $datapasien['asuransi2'] = $this->input->post('asuransi2',TRUE);
            $datapasien['keterangan'] = $this->input->post('ket',TRUE);
            $datapasien['tgl_created'] = date('Y-m-d H:i:s');
            $datapasien['no_pendaftaran'] = $this->Reservasi_model->get_no_pendaftaran_reg();
            $datapasien['no_telp'] = $this->input->post('no_telp',TRUE);
            $is_pasien_baru = $this->input->post('is_checked',TRUE);
            $data_reg['no_reg'] = $this->Reservasi_model->get_no_pendaftaran_reg();
            $datapasien['jenis_pelayanan_id'] = $this->input->post('layanan_baru',TRUE);
            $datapasien['jenis_pasien_id'] = $this->input->post('jenis_pasien',TRUE);
            $datapasien['branch_id'] = $this->session->tempdata('data_session')[0];
            $datapasien['pic_hotel'] = $this->input->post('pic_hotel',TRUE);
            $datapasien["driver_id"] = $this->input->post('driver',TRUE);
            $datapasien['no_asuransi1'] = $this->input->post('no_asuransi1',TRUE);
            $datapasien['no_asuransi2'] = $this->input->post('no_asuransi2',TRUE);
            $jenis_pembayaran = $this->input->post('jenis_pembayaran', TRUE);
            $jenis_pembayaran = $this->data['list_type_bayar'][$jenis_pembayaran];

            $data_pembayaran['type_pembayaran'] = $jenis_pembayaran;
            $data_pembayaran['nama_asuransi'] = $this->input->post('asuransi1',TRUE);
            $data_pembayaran['nama_asuransi2'] = $this->input->post('asuransi2',TRUE);
            $data_pembayaran['no_asuransi'] = $this->input->post('no_asuransi1',TRUE);
            $data_pembayaran['no_asuransi2'] = $this->input->post('no_asuransi2',TRUE);

            $insert_pembayaran = $this->Reservasi_model->insert_pembayaran($data_pembayaran);
            if ($insert_pembayaran > 0) {
                $datapasien['pembayaran_id'] = $insert_pembayaran;
            }

            if ($datapasien["driver_id"] > 0) {
                $datapasien["pemanggilan"] = $this->input->post('cek_malam',TRUE);
            }
            
            if ($this->input->post('cek_hotel',TRUE)) {
                $data_hotel['nama_hotel'] = $this->input->post('hotel_nama',TRUE);
                $hotel_id = $this->Reservasi_model->insert_hotel($data_hotel);
                $datapasien['hotel_id'] = $hotel_id;
            }
            else 
            {
                $datapasien['hotel_id'] = $this->input->post('hotel',TRUE);
            }
            
            $this->Reservasi_model->insert_reg($data_reg);

            $no_rm = "";
            if ($is_pasien_baru == "1") {
                $status_pasien = "PASIEN BARU";
                $no_rm = $this->Reservasi_model->get_no_rm();
                $data_rm['no_rekam_medis'] =$no_rm;
                $this->Reservasi_model->insert_rm($data_rm);
            } else {
                $no_rm = $this->input->post('no_rm',TRUE);
                $status_pasien = "PASIEN LAMA";
            }

            $datapasien['no_rekam_medis'] = $no_rm;
            if ($metode_regis == "LANGSUNG") {
                $status = "HADIR";
            } else {
                $status = "";
            }

            $datapasien['status'] = $status;
            $datapasien['status_pasien'] = $status_pasien;
            $no_urut = $this->Reservasi_model->get_no_urut($date_res,$dokter_id);
            $datapasien['no_urut'] = $no_urut;

            $push = $this->Reservasi_model->insert_pasien($datapasien);

            if($push){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'no_booking' => $no_book,
                    'no_urut' => $no_urut,
                    'messages' => 'Register has been saved to database'
                );
                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Success to Create a new patient register with post data";
                $this->aauth->logit($perms, current_url(), $comments);
            }
            else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Failed insert user group to database, please contact web administrator.');
                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Failed to Create a new patient register when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }
        echo json_encode($res);
    }

    // fungsi batal data reservasi
    function do_batal_reservasi($id){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_reservasi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $datares['status'] = "BATAL";
        $datares['tgl_verif'] = date('Y-m-d H:i:s');
        $update = $this->Reservasi_model->update_reservasi($datares, $id);

        // Jika batal reservasi sukses
        if($update){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'reservasi berhasil dibatalkan'
            );

            // if permitted, do logit
            $perms = "rekam_medis_reservasi_view";
            $comments = "Berhasil mengubah reservasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
            'csrfTokenName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash(),
            'success' => false,
            'messages' => 'Gagal mengubah list_pasien , hubungi web administrator.');

            // if permitted, do logit
            $perms = "rekam_medis_resevasi_view";
            $comments = "Gagal mengubah reservasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    // menampilkan pasien list pasien reservasi
    function get_pasien_reservasi(){
        $tgl_awal       = $this->input->get('tgl_res_awal');
        $poli           = $this->input->get('poli');
        $dokter         = $this->input->get('dokter');
        $nama_pasien    = $this->input->get('nama_pasien');
        $no_rekam_medis = $this->input->get('no_rekam_medis');
        $kode_booking   = $this->input->get('kode_booking');
        $data = $this->Reservasi_model->get_pasien_reservasi($tgl_awal,$poli,$dokter,$nama_pasien,$no_rekam_medis,$kode_booking);

        $output = '';
        foreach ($data as $val) {
            // menentukan ava pasien
            if($val->jenis_kelamin == "Laki-laki"){
                $img_url = "boy.png";
            }elseif ($val->jenis_kelamin == "Perempuan") {
                $img_url = "girl.png";
            }else{
                $img_url = "boy.png";
            }
            // menentukan style hadir dan batal
            if ($val->status == "HADIR") {
                $text = "HADIR";
                $class = "";
                $color = "btn-success";
                $is_batal = "";
            }elseif ($val->status == "BATAL") {
                $text = "BATAL";
                $class = "";
                $color = "btn-danger";
                $is_batal = "hidden";
            }else{
                $color = "";
                $text = "-";
                $is_batal = "";
                $class = "hidden";
            }
            if ($val->status_verif) {
                $is_batal = "hidden";
            }

                $output .= '
                <div class="comment-body" style="padding: 8px; width: 100% ; ">
                    <div class="user-img"> <img style="margin-top: 24px;" class="img-circle" alt="user" src="'.base_url().'assets/plugins/images/'.$img_url.'" ></div>
                    <div  class="mail-contnet" >
                        <div class="col-md-1 pull-right" >
                            <span class="btn "  style="font-size: 10px; background:'.$val->color .'; color: #FFFFFF; border-radius: 20%; padding: 3px; font-weight: bold;" >'.$val->no_urut.'
                            </span>
                        </div>
                        <span class="mytooltip tooltip-effect-5">
                            <span class="btn tooltip-item" style="background:'.$val->color .'; margin-bottom:8px;  font-size: 9px; color: #FFFFFF;  padding: 3px; font-weight: bold;" >'. $val->nama_poliruangan .'</span>
                            <span class="btn tooltip-item" style="background:'.$val->color .'; margin-bottom:8px;  font-size: 9px; color: #FFFFFF;  padding: 3px; font-weight: bold;" >'. $val->NAME_DOKTER .'</span>
                            <span class="tooltip-content clearfix ">
                                <span class="tooltip-text">'. $val->NAME_DOKTER .'</span>
                            </span>
                        </span>
                        <h5 style="font-size: 11px; margin-top:8px; ">'.$val->no_rekam_medis.'</h5><h5 id="list_pasien_nama">'.$val->nama_pasien.'</h5>
                        <span style="font-size: 12px;"> '.$val->alamat_pasien.' </span>
                        <br>
                        <button type="button" class="btn btn-outline btn-rounded btn-danger" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="batalReservasi('.$val->reservasi_id.')" '.$is_batal.' >BATAL</button>
                        <button type="button" class="btn btn-outline btn-rounded btn-success" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="print('.$val->reservasi_id.')" >PRINT</button>
                        <button type="button" class="btn btn-outline btn-rounded btn-primary" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="verify('.$val->reservasi_id.')" '.$is_batal.' >VERIFY</button>
                        <span class="btn '.$color.' pull-right"  style="margin-top: 8px; font-size: 6px; color: #FFFFFF; border-radius: 20%; padding: 4px; font-weight: bold;"'.$class.'>'.$text.'</span>
                    </div>
                </div>
                ';
        }
        echo json_encode($output);
    }

    // menampilkan pasien list pasien batal reservasi
    function get_pasien_reservasi_batal(){
        $tgl_awal       = $this->input->get('tgl_res_awal');
        $poli           = $this->input->get('poli');
        $dokter         = $this->input->get('dokter');
        $nama_pasien    = $this->input->get('nama_pasien');
        $no_rekam_medis = $this->input->get('no_rekam_medis');
        $kode_booking   = $this->input->get('kode_booking');


        $data = $this->Reservasi_model->get_pasien_reservasi_batal($tgl_awal,$poli,$dokter,$nama_pasien,$no_rekam_medis,$kode_booking);
        $output = '';
        foreach ($data as $val) {
            if($val->jenis_kelamin == "Laki-laki"){
                $img_url = "boy.png";
            }elseif ($val->jenis_kelamin == "Perempuan") {
                $img_url = "girl.png";
            }

            if ($val->status == "HADIR") {
                $text = "HADIR";
                $class = "";
                $color = "btn-success";
                $is_batal = "";


            }elseif ($val->status == "BATAL") {
                $text = "BATAL";
                $class = "";
                $color = "btn-danger";
                $is_batal = "hidden";

            }else{
                $color = "";
                $text = "-";
                $is_batal = "";
                $class = "hidden";
            }

                $output .= '
                <div class="comment-body" style="padding: 8px; width: 100%">
                    <div class="user-img"> <img style="margin-top: 16px;" class="img-circle" alt="user" src="'.base_url().'assets/plugins/images/'.$img_url.'" ></div>

                    <div class="mail-contnet">

                        <h5 style="font-size: 10px">'.$val->no_rekam_medis.'</h5><h5>'.$val->nama_pasien.'</h5>
                        <span style="font-size: 12px;"> '.$val->alamat_pasien.' </span>
                        <br>
                        <button type="button" class="btn btn-outline btn-rounded btn-danger" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="batalReservasi('.$val->reservasi_id.')" '.$is_batal.' >BATAL</button>
                        <button type="button" class="btn btn-outline btn-rounded btn-primary" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="verify('.$val->reservasi_id.')" '.$is_batal.' >VERIFY</button>

                        <span class="btn '.$color.' pull-right"  style="margin-top: 8px; font-size: 6px; color: #FFFFFF; border-radius: 20%; padding: 4px; font-weight: bold;"'.$class.'>'.$text.'</span>

                    </div>
                </div>
            ';
        }
        echo json_encode($output);
    }

    // mengirim data ke form pendaftaran
    function verifikasi_data($id){
        $this->data['list_jenis_kelamin']   = array('Laki-laki'=>'Laki-laki','Perempuan'=>'Perempuan', 'Ambigu'=>'Ambigu/Belum Jelas');
        $this->data['list_status_kawin']    = array('1'=>'Belum Kawin','2'=>'Kawin','3'=>'Duda/Janda','4'=>'Di Bawah Umur');
        $this->data['list_warga_negara']    = array('1'=>'Indonesia','2'=>'Asing');
        $this->data['list_golongan_darah']  = array('a'=>'A','b'=>'B','ab'=>'AB','o'=>'O');
        $this->data['list_type_identitas']  = array('1'=>'KTP','2'=>'SIM','3'=>'Kartu Pelajar','4'=>'KTA','5'=>'KITAS','6'=>'Passpor','7'=>'Lainnya');
        $this->data['list_pekerjaan']  = array('1'=>'Swasta','2'=>'Wiraswasta','3'=>'IRT','4'=>'PNS','5'=>'TNI/Polri','6'=>'Lainnya');
        $this->data['list_hubungan']  = array('1'=>'Orang Tua','2'=>'Suami','3'=>'Istri','4'=>'Lainnya');
        $this->data['list_type_bayar']  = array('1'=>'Pribadi','2'=>'Asuransi', '3'=>'Bill Hotel', '4'=> 'Tanggungan Hotel');
        $this->data['js1'] = '<script src="'.base_url().'assets/dist/js/pages/rekam_medis/pendaftaran.js"></script>';
        $this->data['js2'] = '<script src="'.base_url().'assets/dist/js/pages/rekam_medis/reservasi.js"></script>';
        $this->data['reservasi_id'] = $id;

        $pasien = $this->Reservasi_model->get_data_reservasi_by_id($id);
        $norm = $pasien->no_rekam_medis;

        date_default_timezone_set('Asia/Jakarta');
        setlocale(LC_ALL, 'IND');
        setlocale(LC_ALL, 'id_ID');
        $data_res = $this->Reservasi_model->get_data_reservasi_by_id($id);
        $tgl_lahir_pas = $data_res->tanggal_lahir;
        $tanggal_lahir_pas = ($tgl_lahir_pas == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lahir_pas));

        if($pasien->status_pasien == "PASIEN LAMA"){
            $m_pasien = $this->Reservasi_model->get_data_m_pasien($norm);
            $data_reservasi = $this->Reservasi_model->get_data_reservasi_by_id($id);
            $tgl_lhr_ortu =  $m_pasien->tgl_lahir_ortu;
            $tgl_lahir_ortu = ($tgl_lhr_ortu == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_ortu));
            $tgl_lhr_suami = $m_pasien->tgl_lahir_suami_istri;
            $tgl_lahir_suami = ($tgl_lhr_suami == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_suami));
            $tgl_rsvs = date_create($data_reservasi->tanggal_reservasi);
            $tgl_reservasi = date_format($tgl_rsvs, 'd F Y');

            $this->data["data_pasien"] = $m_pasien;
            $this->data["reservasi"] = $data_reservasi;
            $this->data["tgl_lahir"] = $tanggal_lahir_pas;
            $this->data["tgl_reservasi"] = $tgl_reservasi;
            $this->data["tgl_lahir_ortu"] = $tgl_lahir_ortu;
            $this->data["tgl_lahir_suami"] = $tgl_lahir_suami;
            $this->data["umur"] = $this->hitung_umur($tgl_lahir_pas);
        }else{
            $data_pasien = $this->Reservasi_model->get_data_reservasi_by_id($id);
            $data_reservasi = $this->Reservasi_model->get_data_reservasi_by_id($id);
            $tgl_rsvs = date_create($data_pasien->tanggal_reservasi);
            $tgl_reservasi = date_format($tgl_rsvs, 'd F Y');

            $this->data["data_pasien"] = $data_pasien;
            $this->data["reservasi"] = $data_reservasi;
            $this->data["tgl_lahir"] = $tanggal_lahir_pas;
            $this->data["tgl_reservasi"] = $tgl_reservasi;
            $this->data["tgl_lahir_ortu"] = "";
            $this->data["tgl_lahir_suami"] = "";
            $this->data["umur"] = $this->hitung_umur($tgl_lahir_pas);
        }

//        echo "<pre>";
//        print_r($this->data);
//        exit;

        $this->load->view('rekam_medis/v_form_pendaftaran', $this->data);
    }

    public function hitung_umur($tgl_lahir){
        // $tgl_lahir2 = date_create($tgl_lahir);
        // $tgl_lahir3 = $this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir));
        // print_r($tgl_lahir2); die();
        $today = date("Y-m-d");
        $date1 = date_create($tgl_lahir);
        $date2 = new DateTime($today);

        echo $tgl_lahir;

        $interval = $date1->diff($date2);
        $umur = str_pad($interval->y, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($interval->m, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($interval->d, 2, '0', STR_PAD_LEFT).' Hr';

        $res = array(
            "umur" => $umur,
            "success" => true
        );

        return $umur;
    }

    // autocomplete pasien di reservasi
    function autocomplete_pasien(){
        $val_pasien = $this->input->post('keyword');
        if(empty($val_pasien)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }else if ($val_pasien != ""){
            $pasien_list = $this->Reservasi_model->get_pasien_autocomplete($val_pasien);
            if(!empty($pasien_list)) {
                for ($i=0; $i<count($pasien_list); $i++){
                    $status_pasien = $this->Reservasi_model->get_status_pendaftaran($pasien_list[$i]->pasien_id);
                    date_default_timezone_set('Asia/Jakarta');
                    setlocale(LC_ALL, 'IND');
                    setlocale(LC_ALL, 'id_ID');
                    $date_lahir = ($pasien_list[$i]->tanggal_lahir == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($pasien_list[$i]->tanggal_lahir)) ;

                    $list[] = array(
                        "id"                    => $pasien_list[$i]->pasien_id,
                        "value"                 => $pasien_list[$i]->no_rekam_medis." - ".$pasien_list[$i]->pasien_nama,
                        "pasien_nama"           => $pasien_list[$i]->pasien_nama,
                        "alamat"                => $pasien_list[$i]->pasien_alamat,
                        "nama_panggilan"        => $pasien_list[$i]->nama_panggilan,
                        "no_rekam_medis"        => $pasien_list[$i]->no_rekam_medis,
                        "no_bpjs"               => $pasien_list[$i]->no_bpjs,
                        "type_id"               => $pasien_list[$i]->type_identitas,
                        "no_identitas"          => $pasien_list[$i]->no_identitas,
                        "tempat_lahir"          => $pasien_list[$i]->tempat_lahir,
                        "tanggal_lahir"         => $date_lahir,
                        "umur"                  => $pasien_list[$i]->umur,
                        "jenis_kelamin"         => $pasien_list[$i]->jenis_kelamin,
                        "status_kawin"          => $pasien_list[$i]->status_kawin,
                        "tlp_rumah"             => $pasien_list[$i]->tlp_rumah,
                        "tlp_selular"           => $pasien_list[$i]->tlp_selular,
                        "warga_negara"          => $pasien_list[$i]->warga_negara,
                        "propinsi"              => $pasien_list[$i]->propinsi_id,
                        "kabupaten"             => $pasien_list[$i]->kabupaten_id,
                        "kecamatan"             => $pasien_list[$i]->kecamatan_id,
                        "kelurahan"             => $pasien_list[$i]->kelurahan_id,
                        "pekerjaan"             => $pasien_list[$i]->pekerjaan_id,
                        "agama"                 => $pasien_list[$i]->agama_id,
                        "bahasa"                => $pasien_list[$i]->bahasa_id,
                        "pendidikan"            => $pasien_list[$i]->pendidikan_id,
                        "nama_suami_istri"      => $pasien_list[$i]->nama_suami_istri,
                        "tgl_lahir_suami_istri" => $pasien_list[$i]->tgl_lahir_suami_istri,
                        "umur_suami_istri"      => $pasien_list[$i]->umur_suami_istri,
                        "nama_orangtua"         => $pasien_list[$i]->nama_orangtua,
                        "tgl_lahir_ortu"        => $pasien_list[$i]->tgl_lahir_ortu,
                        "umur_ortu"             => $pasien_list[$i]->umur_ortu,
                        "catatan_khusus"        => $pasien_list[$i]->catatan_khusus,
                        "status"                => $status_pasien
                    );
                }
                $res = array(
                    'success' => true,
                    'messages' => "Pasien ditemukan",
                    'data' => $list
                    );
            }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
            }
        }
        echo json_encode($res);
    }

    // Menampilkan list ruangan
    function get_ruangan_list(){
        $instalasi_id = $this->input->get('instalasi_id',TRUE);
        $list_ruangan = $this->Reservasi_model->get_ruangan($instalasi_id);
        $list = "<option value=\"\" disabled selected>Pilih Poli/Ruangan</option>";
        foreach($list_ruangan as $list_ruang){
            $list .= "<option value='".$list_ruang->poliruangan_id."'>".$list_ruang->nama_poliruangan."</option>";
        }
        $res = array(
            "list" => $list,
            "success" => true
        );
        echo json_encode($res);
    }

    // Menampilkan list kelas ruangan
    function get_kelasruangan_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_kelasruangan = $this->Reservasi_model->get_kelas_poliruangan($poliruangan_id);
        if(count($list_kelasruangan) > 1){
           $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        }else{
           $list = "";
        }
        foreach($list_kelasruangan as $list_kelas){
            $list .= "<option value='".$list_kelas->kelaspelayanan_id."'>".$list_kelas->kelaspelayanan_nama."</option>";
        }
        $res = array(
            "list" => $list,
            "success" => true
        );
        echo json_encode($res);
    }

    // Menampilkan list dokter
    function get_dokter_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $tgl = $this->input->get('tgl_reservasi',TRUE);
        date_default_timezone_set('Asia/Jakarta');
        setlocale(LC_ALL, 'IND');
        setlocale(LC_ALL, 'id_ID');
        $tgl_res = $this->Reservasi_model->casting_date_indo(strtolower($tgl));
        $tgl_res_day = $this->Reservasi_model->casting_day_indo($tgl_res);
        $tgl_reservasi = $tgl_res_day;
        $list_dokter = $this->Reservasi_model->get_dokter($poliruangan_id,$tgl_reservasi);
        if(count($list_dokter) > 1){
            $list = "<option value=\"\" disabled selected>PILIH DOKTER</option>";
        }else{
            $list = "<option value=\"\" disabled selected>PILIH DOKTER</option>";
        }
        foreach($list_dokter as $list_dktr){
            $list .= "<option value='".$list_dktr->id_M_DOKTER."'>".$list_dktr->NAME_DOKTER."</option>";
        }
        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }


    function get_hotel_list(){
        $list_hotel = $this->Reservasi_model->get_hotel();
        if(count($list_hotel) > 1){
            $list = "<option value=\"\" disabled selected>PILIH HOTEL</option>";
        }else{
            $list = "<option value=\"\" disabled selected>PILIH HOTEL</option>";
        }
        foreach($list_hotel as $list_dktr){
            $list .= "<option value='".$list_dktr->id_hotel."'>".$list_dktr->nama_hotel."</option>";
        }
        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    // Menampilkan jam dokter
    function get_jam_dokter(){
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $tgl = $this->input->get('tgl_reservasi',TRUE);
        $tgl_cast_indo = $this->Reservasi_model->casting_date_indo($tgl);
        $tgl_res = $this->Reservasi_model->casting_date_indo(strtolower($tgl));
        date_default_timezone_set('Asia/Jakarta');
        setlocale(LC_ALL, 'IND');
        setlocale(LC_ALL, 'id_ID');
        $tgl_reservasi = $this->Reservasi_model->casting_day_indo($tgl_res);

        $list_dokter = $this->Reservasi_model->get_jam_dokter($dokter_id,$tgl_reservasi);
        if(count($list_dokter) > 1){
            $list = "<option value=\"\" disabled selected>Pilih Jam</option>";
        }else{
            $list = "<option value=\"\" disabled selected>Pilih Jam</option>";
        }
        foreach($list_dokter as $list_dktr){
            $list .= "<option value='".$list_dktr->jam_mulai."'>".$list_dktr->jam_mulai."</option>";
        }
        $res = array(
            "list" => $list,
            "success" => true
        );
        echo json_encode($res);
    }

    // menmapilkan kuota dokter yang tersedia
    function get_kuota_dokter(){
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $jam = $this->input->get('jam',TRUE);
        $tgl = $this->input->get('tgl_reservasi',TRUE);
        $tgl_cast_indo = $this->Reservasi_model->casting_date_indo($tgl);
        $tgl_reservasi = $this->Reservasi_model->casting_day_indo($tgl_cast_indo);
        $count_kuota_dokter = $this->Reservasi_model->get_kuota_dokter($dokter_id,$tgl_reservasi,$jam);
        $count_pasien = $this->Reservasi_model->get_count_pasien($dokter_id,$tgl_cast_indo,$jam);
        $int_pas = (int)$count_pasien->jumlah;
        $int_dok = (int)$count_kuota_dokter->kuota;

        $res = array(
            "success" => true,
            "count_pasien" => $int_pas,
            "count_kuota_dokter" => $int_dok
        );
        echo json_encode($res);
    }

    // fungsi print dengan wajib memasukan parameter id
    public function print_rm($id){
        $print['reservasi_id'] = $id;
        $print['data_pasien'] = $this->Reservasi_model->get_data_print($id);
        $this->load->view('rekam_medis/print_reservasi',$print);
    }

    public function ajax_list_pasien(){
        $list = $this->Pendaftaran_model->get_pasien_list();
        $data = array();
        $no = 1;
        foreach($list as $pasien){
            $row = array();

            $row[] = $no++;
            $row[] = $pasien->no_rekam_medis;
            $row[] = $pasien->no_bpjs;
            $row[] = $pasien->pasien_nama;
            $row[] = $pasien->nama_panggilan;
            $row[] = $pasien->tanggal_lahir;
            $row[] = $pasien->pasien_alamat;
            //add html for action
            $row[] = '<button class="btn btn-info btn-circle" onclick="pilihPasien('."'".$pasien->pasien_id."'".')"><i class="fa fa-check"></i></button>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Pendaftaran_model->count_pasien_all(),
            "recordsFiltered" => $this->Pendaftaran_model->count_pasien_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_pasien_by_id(){
        $id_m_pasien = $this->input->get('id_m_pasien',TRUE);
        $old_data = $this->Pendaftaran_model->get_pasien_by_id($id_m_pasien);
        // print_r($old_data);die();

        if(count($old_data) > 0) {
            $status_pasien = $this->Pendaftaran_model->get_status_pendaftaran($old_data->pasien_id);
            // print_r($status_pasien);die();
            date_default_timezone_set('Asia/Jakarta');
            setlocale(LC_ALL, 'IND');
            setlocale(LC_ALL, 'id_ID');
            $tgl_lhr = $old_data->tanggal_lahir;
            $tgl_lahir = ($tgl_lhr == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr));

            $tgl_lhr_ortu =  $old_data->tgl_lahir_ortu;
            $tgl_lahir_ortu = ($tgl_lhr_ortu == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_ortu));

            $tgl_lhr_suami = $old_data->tgl_lahir_suami_istri;
            $tgl_lahir_suami = ($tgl_lhr_suami == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_suami));

            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data,
                'tgl_lahir' => $tgl_lahir,
                'tgl_lahir_ortu' =>$tgl_lahir_ortu,
                'tgl_lahir_suami' =>$tgl_lahir_suami,
                'status_pasien' => $status_pasien
            );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Pasien tidak ditemukan");
        }

        echo json_encode($res);
    }
}
