<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasienri extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pasienri_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienri_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "rekam_medis_pasienri_view";
        $comments = "List Pasien Rawat Inap";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_pasienri', $this->data);
    }

    public function ajax_list_pasienri(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $poliklinik = $this->input->get('poliklinik',TRUE);
        $status = $this->input->get('status',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $nama_dokter = $this->input->get('nama_dokter', TRUE);
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $lokasi = $this->input->get('lokasi',TRUE);

        $list = $this->Pasienri_model->get_pasienri_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien,  $no_rekam_medis, $nama_dokter, $dokter_id,$pasien_alamat,$no_bpjs,$no_pendaftaran,$lokasi);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $pasienri){
            $no++;
            $row = array();
            $row[] = $pasienri->no_pendaftaran;
            $row[] = $pasienri->no_rekam_medis."/ \n".$pasienri->pasien_nama;
            $row[] = $pasienri->no_bpjs;
            $row[] = date('d M Y H:i:s', strtotime($pasienri->tgl_masukadmisi));
            $row[] = $pasienri->pasien_alamat;
            $row[] = $pasienri->jenis_kelamin;
            $row[] = $pasienri->umur;
            $row[] = $pasienri->type_pembayaran;
            $row[] = $pasienri->kelaspelayanan_nama;
            $row[] = $pasienri->NAME_DOKTER;
            $row[] = $pasienri->poli_ruangan;
            $row[] = $pasienri->no_kamar." / ".$pasienri->no_bed;
            $row[] = $pasienri->lokasi;
            $row[] = $pasienri->statusrawat;
            $row[] = !empty($pasienri->carapulang) ? $pasienri->carapulang : 'Not Set';
            $row[] = $pasienri->nama_perujuk;

            if(trim(strtoupper($pasienri->statusrawat)) == "SUDAH PULANG"){
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk melihat diagnosa" data-toggle="modal" data-backdrop="static" data-keyboard="false"  data-target="#modal_diagnosa_sudahpulang" onclick="periksaPasienRiSudahPulang('."'".$pasienri->pendaftaran_id."'".')">Diagnosa</button>';
                // $row[] = '<button type="button" class="btn btn-default" title="" disabled>Pulangkan</button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_diagnosa" data-backdrop="static" data-keyboard="false"  title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRi('."'".$pasienri->pendaftaran_id."'".')">Diagnosa</button>';
                // $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_keterangan_keluar" title="Klik untuk set status keluar" onclick="pulangkanPasienRi('."'".$pasienri->pendaftaran_id."'".')">Pulangkan</button>';
            }

            // $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_diagnosa" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRi('."'".$pasienri->pendaftaran_id."'".')">Diagnosa</button>';
            // $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_keterangan_keluar" title="Klik untuk set status keluar" onclick="pulangkanPasienRi('."'".$pasienri->pendaftaran_id."'".')">Pulangkan</button>';

            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Pasienri_model->count_all_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien,  $no_rekam_medis, $nama_dokter, $dokter_id,$pasien_alamat,$no_bpjs,$no_pendaftaran),
            "recordsFiltered" => $this->Pasienri_model->count_all_list_filtered($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien,  $no_rekam_medis, $nama_dokter, $dokter_id,$pasien_alamat,$no_bpjs,$no_pendaftaran),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);

    }
    public function periksa_pasienri($pendaftaran_id){
        $pasienri['list_pasien'] = $this->Pasienri_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('periksa_pasienri', $pasienri);
    }

    public function periksa_pasienri_sudahpulang($pendaftaran_id){
        $pasienri['list_pasien'] = $this->Pasienri_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('periksa_pasienri_sudahpulang', $pasienri);
    }

    public function ajax_list_diagnosa_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienri_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->diagnosa_by;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" id="hapusDiagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_diagnosa_pasien_sudahpulang(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienri_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->diagnosa_by;
            // $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" id="hapusDiagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_diagnosa_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_diagnosa','Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('nama_diagnosa','Nama Diagnosa', 'required|trim');
        $this->form_validation->set_rules('jenis_diagnosa','Jenis Diagnosa', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienri_view";
            $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $nama_diagnosa = $this->input->post('nama_diagnosa', TRUE);
            $jenis_diagnosa = $this->input->post('jenis_diagnosa', TRUE);
            $kode_diagnosa = $this->input->post('kode_diagnosa', TRUE);

            $data_diagnosapasien = array(
                'nama_diagnosa' => $nama_diagnosa,
                'tgl_diagnosa' => date('Y-m-d'),
                'kode_diagnosa' => $kode_diagnosa,
                'jenis_diagnosa' => $jenis_diagnosa,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'dokter_id' => $dokter,
                'diagnosa_by' => 'RM'
            );
            // cek diagnosa primer
            $query = $this->db->get_where('t_diagnosapasien', array('pendaftaran_id'=>$pendaftaran_id,'1'=>$jenis_diagnosa));
            if ($query->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Diagnosa Utama Hanya Bisa Diinput Satu Kali');

                    // if permitted, do logit
                    $perms = "master_data_dokter_ruangan_view";
                    $comments = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{

                $ins = $this->Pasienri_model->insert_diagnosapasien($pendaftaran_id, $data_diagnosapasien);
                //$ins=true;
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Diagnosa berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienri_view";
                    $comments = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienri_view";
                    $comments = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }

    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienri_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            $row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            $row[] = $tindakan->total_harga;
            //$row[] = '<button type="button" class="btn-floating yellow waves-effect waves-light darken-4" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanpasien_id."'".')"><i class="mdi-content-clear"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_delete_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosapasien_id = $this->input->post('diagnosapasien_id', TRUE);

        $delete = $this->Pasienri_model->delete_diagnosa($diagnosapasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienri_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienri_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function get_dokter_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_dokter = $this->Pasienri_model->get_dokter($poliruangan_id);
        if(count($list_dokter) > 1){
            $list = "<option value=\"\" disabled selected>Pilih Dokter</option>";
        }else{
            $list = "";
        }
        foreach($list_dokter as $list_dktr){
            $list .= "<option value='".$list_dktr->id_M_DOKTER."'>".$list_dktr->NAME_DOKTER."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    public function set_status_pulang(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienri_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        $this->form_validation->set_rules('status_pulang','Status Pulang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienri_view";
            $comments = "Gagal set status pulang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $status_pulang = $this->input->post('status_pulang', TRUE);
            $rs_rujukan = $this->input->post('rs_rujukan', TRUE);
            $user_id = $this->data['users']->id;

            $data = array(
                'no_kamar'   => '',
                'no_bed'    => '',
                'status_periksa' => 'SUDAH PULANG',
                'carapulang' => $status_pulang,
                'rs_rujukan' => $rs_rujukan,
                'petugas_id' => $user_id
            );

            $update_pendaftaran = $this->Pasienri_model->update_pendaftaran($pendaftaran_id, $data);

            if($update_pendaftaran){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Berhasil set status pulang'
                );
                // if permitted, do logit
                $perms = "rekam_medis_pasienri_view";
                $comments = "Berhasil set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal set status pulang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rekam_medis_pasienri_view";
                $comments = "Gagal set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function ajax_list_diagnosa(){
        $list = $this->Pasienri_model->get_list_diagnosa();

        $data = array();
        foreach($list as $diagnosa){
            $row = array();

            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->kelompokdiagnosa_nama;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Diagnosa" onclick="pilihDiagnosa('."'".$diagnosa->diagnosa_id."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Pasienri_model->count_list_diagnosa(),
            "recordsFiltered" => $this->Pasienri_model->count_list_filtered_diagnosa(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_diagnosa_by_id(){
        $diagnosa_id = $this->input->get('diagnosa_id',TRUE);
        $data_diagnosa = $this->Pasienri_model->get_diagnosa_by_id($diagnosa_id);
        if (sizeof($data_diagnosa)>0){
            $diagnosa = $data_diagnosa[0];
            $res = array(
                "success" => true,
                "messages" => "Data diagnosa ditemukan",
                "data" => $diagnosa
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data diagnosa tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }

    public function ajax_list_rs_rujukan(){
        $list = $this->Pasienri_model->get_list_rs_rujukan();

        $data = array();
        foreach($list as $rs_rujukan){
            $row = array();

            $row[] = $rs_rujukan->kode_rs;
            $row[] = $rs_rujukan->nama_rs;
            $row[] = $rs_rujukan->alamat_rs;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih RS Rujukan" onclick="pilihRSRujukan('."'".$rs_rujukan->rs_rujukan_id."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Pasienri_model->count_list_rs_rujukan(),
            "recordsFiltered" => $this->Pasienri_model->count_list_filtered_rs_rujukan(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_rs_rujukan_by_id(){
        $rs_rujukan_id = $this->input->get('rs_rujukan_id',TRUE);
        $data_rs_rujukan = $this->Pasienri_model->get_rs_rujukan_by_id($rs_rujukan_id);
        if (sizeof($data_rs_rujukan)>0){
            $rs_rujukan = $data_rs_rujukan[0];
            $res = array(
                "success" => true,
                "messages" => "Data RS Rujukan ditemukan",
                "data" => $rs_rujukan
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data RS Rujukan tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }


    public function keterangan_keluar($pendaftaran_id){
        $pasienri['list_pasien'] = $this->Pasienri_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('v_keterangan_keluar_pasienRi', $pasienri);
    }
}
