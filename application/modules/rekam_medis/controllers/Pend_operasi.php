<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pend_operasi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pendaftaran_operasi_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['list_jenis_kelamin']   = array('lk'=>'Laki-laki','pr'=>'Perempuan');
        $this->data['list_status_kawin']    = array('1'=>'Belum Kawin','2'=>'Kawin','3'=>'Duda/Janda','4'=>'Di Bawah Umur','5'=>'Lainnya');
        $this->data['list_warga_negara']    = array('1'=>'Indonesia','2'=>'Asing');
        $this->data['list_golongan_darah']  = array('a'=>'A','b'=>'B','ab'=>'AB','o'=>'O');
        $this->data['list_type_identitas']  = array('1'=>'KTP','2'=>'SIM','3'=>'Kartu Pelajar','4'=>'KTA','5'=>'KITAS','6'=>'Passpor','7'=>'Lainnya');
        $this->data['list_pekerjaan']  = array('1'=>'Swasta','2'=>'Wiraswasta','3'=>'IRT','4'=>'PNS','5'=>'TNI/Polri','6'=>'Lainnya');
        $this->data['list_hubungan']  = array('1'=>'Orang Tua','2'=>'Suami','3'=>'Istri','4'=>'Lainnya');
        $this->data['list_type_bayar']  = array('1'=>'Pribadi','2'=>'BPJS','3'=>'Instansi','4'=>'Lainnya');
        $this->data['next_no_rm'] = $this->Pendaftaran_operasi_model->get_no_rm();
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('pendaftaran_menu_paket_operasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "pendaftaran_menu_paket_operasi_view";
        $comments = "Listing Userrrr.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_form_pendaftaran_operasi', $this->data);
    }

    public function do_create_pendaftaran_operasi(){
        $is_permit = $this->aauth->control_no_redirect('pendaftaran_menu_paket_operasi_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $status_pasien = $this->input->post('status_pasien',TRUE);
        if($status_pasien > 0){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => "Pasien masih menjalani perawatan"
                );
            echo json_encode($res);
            exit;
        }


        $this->load->library('form_validation');
        $this->form_validation->set_rules('type_id', 'Type Identitas', 'required|trim');
        $this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|trim');
        // $this->form_validation->set_rules('no_bpjs', 'No BPJS', 'required|trim');
        $this->form_validation->set_rules('no_rm', 'No. Rekam Medis', 'required|trim');
        $this->form_validation->set_rules('nama_pasien', 'Nama Pasien', 'required|trim');
        $this->form_validation->set_rules('nama_panggilan', 'Nama panggilan', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|trim');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('umur', 'Umur', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat Pasien', 'required|trim');
        $this->form_validation->set_rules('alamat_domisili', 'Alamat Domisili', 'required|trim');
        $this->form_validation->set_rules('propinsi', 'Propinsi Pasien', 'required|trim');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten Pasien', 'required|trim');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan Pasien', 'required|trim');
        $this->form_validation->set_rules('kelurahan', 'Kelurahan Pasien', 'required|trim');
        $this->form_validation->set_rules('provinsi_tinggal', 'Propinsi Pasien', 'required|trim');
        $this->form_validation->set_rules('kabupaten_tinggal', 'Kabupaten Pasien', 'required|trim');
        $this->form_validation->set_rules('kecamatan_tinggal', 'Kecamatan Pasien', 'required|trim');
        $this->form_validation->set_rules('kelurahan_tinggal', 'Kelurahan Pasien', 'required|trim');
        $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required|trim');
        // $this->form_validation->set_rules('nama_suami_istri', 'Nama Suami/Istri', 'required|trim');
        // $this->form_validation->set_rules('tgl_suami_istri', 'Tanggal Lahir Suami/Istri', 'required|trim');
        // $this->form_validation->set_rules('umur_suami_istri', 'Umur Suami/Istri', 'required|trim');
        $this->form_validation->set_rules('no_mobile', 'No.Ponsel/Mobile', 'required|trim');
        $this->form_validation->set_rules('nama_orangtua', 'Nama Orang Tua', 'required|trim');
        // $this->form_validation->set_rules('hubunganpj', 'Hubungan Status', 'required|trim');
//        $this->form_validation->set_rules('warga_negara', 'Warga Negara', 'required|trim');
//        $this->form_validation->set_rules('agama', 'Agama', 'trim');
        // $this->form_validation->set_rules('instalasi', 'Instalasi Tujuan', 'required|trim');
        $this->form_validation->set_rules('paket_operasi', 'Paket Operasi', 'required|trim');
        $this->form_validation->set_rules('poli_ruangan', 'Poli/Ruangan', 'required|trim');
        $this->form_validation->set_rules('kamarruangan', 'Kamar Ruangan', 'required|trim');
        $this->form_validation->set_rules('dokter', 'Dokter', 'required|trim');
        $this->form_validation->set_rules('dokter_anastesi', 'Dokter Anastesi', 'required|trim');
        $this->form_validation->set_rules('perawat', 'Perawat', 'required|trim');
        $this->form_validation->set_rules('pj_nama', 'Penanggung Jawab', 'required|trim');
        $this->form_validation->set_rules('byr_type_bayar', 'Cara Bayar', 'required|trim');
        $instalasi = $this->input->post('instalasi',TRUE);
        if($instalasi == '3'){
            $this->form_validation->set_rules('kamarruangan', 'Kamar Ruangan', 'required');
            $this->form_validation->set_rules('dokter_anastesi', 'Dokter Anastesi', 'required');
            $this->form_validation->set_rules('perawat', 'Perawat', 'required');
        }


        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "pendaftaran_menu_paket_operasi_view";
            $comments = "Gagal melakukan pendaftaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $pasien_id = $this->input->post('pasien_id',TRUE);
            $tgl_lahir1 = date_create($this->input->post('tgl_lahir',TRUE));
            $tgl_lahir2 = date_format($tgl_lahir1, 'Y-m-d');
            $datapasien['no_rekam_medis'] = $this->Pendaftaran_operasi_model->get_no_rm();
            $datapasien['pasien_nama'] = $this->input->post('nama_pasien',TRUE);
            $datapasien['pasien_alamat'] = $this->input->post('alamat',TRUE);
            $datapasien['pasien_alamat_domisili'] = $this->input->post('alamat_domisili',TRUE);
            $datapasien['type_identitas'] = $this->input->post('type_id',TRUE);
            $datapasien['no_identitas'] = $this->input->post('no_identitas',TRUE);
            $datapasien['tempat_lahir'] = $this->input->post('tempat_lahir',TRUE);
            $datapasien['tanggal_lahir'] = $tgl_lahir2;
            $datapasien['umur'] = $this->input->post('umur',TRUE);
            $datapasien['jenis_kelamin'] = $this->input->post('jenis_kelamin',TRUE);
            $datapasien['status_kawin'] = $this->input->post('status_kawin',TRUE);
            // $datapasien['gol_darah'] = $this->input->post('golongan_darah',TRUE);
            // $datapasien['rhesus'] = $this->input->post('rhesus',TRUE);
            // $datapasien['anak_ke'] = $this->input->post('anakke',TRUE);
            $datapasien['tlp_rumah'] = $this->input->post('no_tlp_rmh',TRUE);
            $datapasien['tlp_selular'] = $this->input->post('no_mobile',TRUE);
            $datapasien['warga_negara'] = $this->input->post('warga_negara',TRUE);
            $datapasien['propinsi_id'] = $this->input->post('propinsi',TRUE);
            $datapasien['kabupaten_id'] = $this->input->post('kabupaten',TRUE);
            $datapasien['kecamatan_id'] = $this->input->post('kecamatan',TRUE);
            $datapasien['kelurahan_id'] = $this->input->post('kelurahan',TRUE);
            $datapasien['pekerjaan_id'] = $this->input->post('pekerjaan',TRUE);
            $datapasien['agama_id'] = $this->input->post('agama',TRUE);
            $datapasien['bahasa_id'] = $this->input->post('bahasa',TRUE);
            // $datapasien['suku'] = $this->input->post('suku',TRUE);
            // $bahasa = $this->input->post('bahasa[]',TRUE);
            // $printbhs = '';
            // foreach($bahasa as $key => $value){
            //     $printbhs .= $value.',';
            // }
            // $datapasien['bahasa'] = trim($printbhs,',');
            $datapasien['pendidikan_id'] = $this->input->post('pendidikan',TRUE);
            $datapasien['nama_orangtua'] = $this->input->post('nama_orangtua',TRUE);
            $datapasien['tgl_lahir_ortu'] = $this->input->post('tgl_lahir_ortu',TRUE);
            $datapasien['umur_ortu'] = $this->input->post('umur_ortu',TRUE);
            // $datapasien['status_hubungan'] = $this->input->post('hubunganpj',TRUE);
            //$this->input->post('hubungankeluarga',TRUE);
            $datapasien['catatan_khusus'] = $this->input->post('catatan_khusus',TRUE);
            $datapasien['create_date'] = date('Y-m-d H:i:s');

            $datapendaftaran['tgl_pendaftaran'] = date('Y-m-d H:i:s');
            $datapendaftaran['status_periksa'] = "ANTRIAN";
            $datapendaftaran['status_pasien'] = 1;
            $datapendaftaran['pasien_id'] = $pasien_id;
            $datapendaftaran['instalasi_id'] = 3; //rawat inap
            $datapendaftaran['poliruangan_id'] = $this->input->post('poli_ruangan',TRUE);
            $datapendaftaran['dokter_id'] = $this->input->post('dokter',TRUE);
            //instalasi_id = 3 rawat inap
            $datapendaftaran['no_pendaftaran'] = $this->Pendaftaran_operasi_model->get_no_pendaftaran_ri();

//            if($datapendaftaran['instalasi_id'] == '1'){
//                $datapendaftaran['no_pendaftaran'] = $this->Pendaftaran_operasi_model->get_no_pendaftaran_rj();
//            }else if($datapendaftaran['instalasi_id'] == '2'){
//                $datapendaftaran['no_pendaftaran'] = $this->Pendaftaran_operasi_model->get_no_pendaftaran_rd();
//            }else if($datapendaftaran['instalasi_id'] == '3'){
//                $datapendaftaran['no_pendaftaran'] = $this->Pendaftaran_operasi_model->get_no_pendaftaran_ri();
//            }else{
//                if($datapendaftaran['poliruangan_id'] == '6'){
//                    $datapendaftaran['no_pendaftaran'] = $this->Pendaftaran_operasi_model->get_no_pendaftaran_rad();
//                }else if($datapendaftaran['poliruangan_id'] == '7'){
//                    $datapendaftaran['no_pendaftaran'] = $this->Pendaftaran_operasi_model->get_no_pendaftaran_lab();
//                }
//            }
            $datapendaftaran['paketoperasi_id'] = $this->input->post('paket_operasi',TRUE);
            $kelaspelayanan_id = $this->Pendaftaran_operasi_model->get_kelas_paket_operasi($datapendaftaran['paketoperasi_id']);
            $datapendaftaran['kelaspelayanan_id'] = $kelaspelayanan_id;
            $datapendaftaran['nama_perujuk'] = $this->input->post('rm_namaperujuk',TRUE);
            $datapendaftaran['alamat_perujuk'] = $this->input->post('rm_alamatperujuk',TRUE);
            $datapendaftaran['petugas_id'] = $this->data['users']->id;
            $datapendaftaran['dokteranastesi_id'] = $this->input->post('dokter_anastesi',TRUE);
            $datapendaftaran['perawat_id'] = $this->input->post('perawat',TRUE);

            //start transaction pendaftaran
            $this->db->trans_begin();
            //insert penanggungjawab
            $data_pj['nama_pj'] = $this->input->post('pj_nama',TRUE);
            $data_pj['alamat_pj'] = $this->input->post('pj_alamat',TRUE);
            $data_pj['propinsi_id'] = $this->input->post('pj_propinsi',TRUE);
            $data_pj['kabupaten_id'] = $this->input->post('pj_kabupaten',TRUE);
            $data_pj['kecamatan_id'] = $this->input->post('pj_kecamatan',TRUE);
            $data_pj['kelurahan_id'] = $this->input->post('pj_kelurahan',TRUE);
            $data_pj['no_telepon'] = $this->input->post('pj_no_tlp_rmh',TRUE);
            $data_pj['no_ponsel'] = $this->input->post('pj_no_mobile',TRUE);
            $data_pj['status_hubungan_pj'] = $this->input->post('hubunganpj',TRUE);
            $insert_pj = $this->Pendaftaran_operasi_model->insert_penanggungjawab($data_pj);
            $datapendaftaran['penanggungjawab_id'] = $insert_pj;

            //insert Metode Pembayaran
            $data_byr['type_pembayaran'] = $this->input->post('byr_type_bayar',TRUE);
            $data_byr['instansi_pekerjaan'] = $this->input->post('byr_instansi',TRUE);
            $data_byr['nama_asuransi'] = $this->input->post('byr_namaasuransi',TRUE);
            $data_byr['no_asuransi'] = $this->input->post('byr_nomorasuransi',TRUE);
            $insert_byr = $this->Pendaftaran_operasi_model->insert_pembayaran($data_byr);
            $datapendaftaran['pembayaran_id'] = $insert_byr;

            if(!empty($pasien_id)){
                $datapasien['no_rekam_medis'] = $this->input->post('no_rm',TRUE);
                $update_pasien = $this->Pendaftaran_operasi_model->update_pasien($datapasien, $pasien_id);
                if($update_pasien){
                    $datapendaftaran['status_kunjungan'] = "PASIEN LAMA";
                    $insert_pendaftaran = $this->Pendaftaran_operasi_model->insert_pendaftaran($datapendaftaran);
                }
            }else{
                $insert_pasien = $this->Pendaftaran_operasi_model->insert_pasien($datapasien);
                if($insert_pasien){
                    $datapendaftaran['pasien_id'] = $insert_pasien;
                    $datapendaftaran['status_kunjungan'] = "PASIEN BARU";
                    $insert_pendaftaran = $this->Pendaftaran_operasi_model->insert_pendaftaran($datapendaftaran);
                }
            }

            if($insert_pendaftaran){
                $last_pendaftaran = $this->Pendaftaran_operasi_model->get_last_pendaftaran($insert_pendaftaran);
//                if($datapendaftaran['instalasi_id'] == '4'){
//                    $datapenunjang['kelaspelayanan_id'] = $last_pendaftaran->kelaspelayanan_id;
//                    $datapenunjang['pendaftaran_id'] = $last_pendaftaran->pendaftaran_id;
//                    $datapenunjang['pasien_id'] = $last_pendaftaran->pasien_id;
//                    $datapenunjang['no_masukpenunjang'] = $last_pendaftaran->no_pendaftaran;
//                    $datapenunjang['tgl_masukpenunjang'] = date('Y-m-d H:i:s');
//                    $datapenunjang['statusperiksa'] = "ANTRIAN";
//                    $datapenunjang['poli_ruangan_id'] = $last_pendaftaran->poliruangan_id;
//                    $datapenunjang['dokterpengirim_id'] = $this->input->post('dokter',TRUE);
//                    $datapenunjang['create_time'] = date('Y-m-d H:i:s');
//                    $datapenunjang['create_by'] = $this->data['users']->id;
//                    $insert_penunjang = $this->Pendaftaran_operasi_model->insert_penunjang($datapenunjang);
//                }

                if($datapendaftaran['instalasi_id'] == '3'){
                    $dataadmisi['kelaspelayanan_id'] = $last_pendaftaran->kelaspelayanan_id;
                    $dataadmisi['pendaftaran_id'] = $last_pendaftaran->pendaftaran_id;
                    $dataadmisi['pasien_id'] = $last_pendaftaran->pasien_id;
                    $dataadmisi['no_masukadmisi'] = $last_pendaftaran->no_pendaftaran;
                    $dataadmisi['tgl_masukadmisi'] = date('Y-m-d H:i:s');
                    $dataadmisi['statusrawat'] = "SEDANG RAWAT INAP";
                    $dataadmisi['poli_ruangan_id'] = $last_pendaftaran->poliruangan_id;
                    $dataadmisi['kamarruangan_id'] = $this->input->post('kamarruangan',TRUE);
                    $dataadmisi['create_time'] = date('Y-m-d H:i:s');
                    $dataadmisi['dokter_id'] = $last_pendaftaran->dokter_id;
                    $dataadmisi['dokteranastesi_id'] = $this->input->post('dokter_anastesi',TRUE);
                    $dataadmisi['dokterperawat_id'] = $this->input->post('perawat',TRUE);
                    $dataadmisi['create_by'] = $this->data['users']->id;
                    $insert_admisi = $this->Pendaftaran_operasi_model->insert_admisi($dataadmisi);
                    if ($insert_admisi){
                        $masukkamar['poliruangan_id'] =  $last_pendaftaran->poliruangan_id;
                        $masukkamar['kelaspelayanan_id'] =  $last_pendaftaran->kelaspelayanan_id;
                        $masukkamar['kamarruangan_id'] =  $this->input->post('kamarruangan',TRUE);
                        $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                        $masukkamar['pasienadmisi_id'] =  $insert_admisi;
                        $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                        $masukkamar['create_user'] =  $this->data['users']->id;
                        $this->Pendaftaran_operasi_model->insert_kamar($masukkamar);

                        $updatekamar['status_bed'] = '1';
                        $this->Pendaftaran_operasi_model->update_kamar($updatekamar, $dataadmisi['kamarruangan_id']);
                        $updatependaftaran['status_periksa'] = "SEDANG RAWAT INAP";
                        $this->Pendaftaran_operasi_model->update_pendaftaran($updatependaftaran, $insert_pendaftaran);
                    }
                }

                $tariftindakan = $this->Pendaftaran_operasi_model->get_tarif_tindakan('2');
                $data_tindakanpasien = array(
                    'pendaftaran_id' => $insert_pendaftaran,
                    'pasien_id' => $last_pendaftaran->pasien_id,
                    'tariftindakan_id' => TARIF_PENDAFTARAN,
                    'jml_tindakan' => '1',
                    'total_harga_tindakan' => $tariftindakan->harga_tindakan,
                    'is_cyto' => '0',
                    'total_harga' => $tariftindakan->harga_tindakan,
                    'dokter_id' => null,
                    'tgl_tindakan' => date('Y-m-d'),
                    'ruangan_id' => $last_pendaftaran->poliruangan_id,
                    'petugas_id' => $this->data['users']->id
                );
                $tindakanpendafataran = $this->Pendaftaran_operasi_model->insert_tindakanpasien($data_tindakanpasien);
                if($tindakanpendafataran){
                    $this->Pendaftaran_operasi_model->insert_tindakan_paket($last_pendaftaran->paketoperasi_id,$insert_pendaftaran,$last_pendaftaran->pasien_id,$last_pendaftaran->dokter_id,$last_pendaftaran->dokteranastesi_id,$last_pendaftaran->perawat_id);
                    $this->Pendaftaran_operasi_model->insert_obat_paket($last_pendaftaran->paketoperasi_id, $insert_pendaftaran, $last_pendaftaran->pasien_id, $last_pendaftaran->dokter_id);
                }
            }

            if($insert_pendaftaran){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Register has been saved to database'
                );

                // if permitted, do logit
                $perms = "pendaftaran_menu_paket_operasi_view";
                $comments = "Success to Create a new patient register with post data";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Failed insert user group to database, please contact web administrator.');

                // if permitted, do logit
                $perms = "pendaftaran_menu_paket_operasi_view";
                $comments = "Failed to Create a new patient register when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }
        echo json_encode($res);
    }

    function autocomplete_pasien(){
        $val_pasien = $this->input->get('val_pasien');
        if(empty($val_pasien)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }
        $pasien_list = $this->Pendaftaran_operasi_model->get_pasien_autocomplete($val_pasien);
        if(count($pasien_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($pasien_list); $i++){
                $status_pasien = $this->Pendaftaran_operasi_model->get_status_pendaftaran($pasien_list[$i]->pasien_id);
                $tgl_lhr = date_create($pasien_list[$i]->tanggal_lahir);
                $tgl_lahir = date_format($tgl_lhr, 'd F Y');
                $list[] = array(
                    "id"            => $pasien_list[$i]->pasien_id,
                    "value"         => $pasien_list[$i]->no_rekam_medis." - ".$pasien_list[$i]->pasien_nama,
                    "pasien_nama"   => $pasien_list[$i]->pasien_nama,
                    "no_rekam_medis"        => $pasien_list[$i]->no_rekam_medis,
                    "type_id"        => $pasien_list[$i]->type_identitas,
                    "no_identitas"        => $pasien_list[$i]->no_identitas,
                    "tempat_lahir"          => $pasien_list[$i]->tempat_lahir,
                    "tanggal_lahir"         => $tgl_lahir,
                    "umur"         => $pasien_list[$i]->umur,
                    "jenis_kelamin"         => $pasien_list[$i]->jenis_kelamin,
                    "status_kawin"          => $pasien_list[$i]->status_kawin,
                    "gol_darah"        => $pasien_list[$i]->gol_darah,
                    "rhesus"                => $pasien_list[$i]->rhesus,
                    "anak_ke"               => $pasien_list[$i]->anak_ke,
                    "tlp_rumah"          => $pasien_list[$i]->tlp_rumah,
                    "tlp_selular"          => $pasien_list[$i]->tlp_selular,
                    "warga_negara"          => $pasien_list[$i]->warga_negara,
                    "alamat" => $pasien_list[$i]->pasien_alamat,
                    "propinsi"          => $pasien_list[$i]->propinsi_id,
                    "kabupaten"          => $pasien_list[$i]->kabupaten_id,
                    "kecamatan"          => $pasien_list[$i]->kecamatan_id,
                    "kelurahan"          => $pasien_list[$i]->kelurahan_id,
                    "pekerjaan"          => $pasien_list[$i]->pekerjaan,
                    "agama"          => $pasien_list[$i]->agama,
                    "suku"          => $pasien_list[$i]->suku,
                    "bahasa"          => $pasien_list[$i]->bahasa,
                    "pendidikan"          => $pasien_list[$i]->pendidikan,
                    "nama_keluarga"          => $pasien_list[$i]->nama_keluarga,
                    "status_hubungan"          => $pasien_list[$i]->status_hubungan,
                    "alergi"          => $pasien_list[$i]->alergi,
                    "status"          => $status_pasien,
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Pasien ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
        }
        echo json_encode($res);
    }

    function get_kabupaten_list(){
        $propinsi_id = $this->input->get('propinsi_id',TRUE);
        $list_kabupaten = $this->Pendaftaran_operasi_model->get_kabupaten($propinsi_id);
        $list = "<option value=\"\" disabled selected>Pilih Kabupaten</option>";
        foreach($list_kabupaten as $list_kab){
            $list .= "<option value='".$list_kab->kabupaten_id."'>".$list_kab->kabupaten_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kecamatan_list(){
        $kabupaten_id = $this->input->get('kabupaten_id',TRUE);
        $list_kecamatan = $this->Pendaftaran_operasi_model->get_kecamatan($kabupaten_id);
        $list = "<option value=\"\" disabled selected>Pilih Kecamatan</option>";
        foreach($list_kecamatan as $list_kec){
            $list .= "<option value='".$list_kec->kecamatan_id."'>".$list_kec->kecamatan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kelurahan_list(){
        $kecamatan_id = $this->input->get('kecamatan_id',TRUE);
        $list_kelurahan = $this->Pendaftaran_operasi_model->get_kelurahan($kecamatan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelurahan</option>";
        foreach($list_kelurahan as $list_kel){
            $list .= "<option value='".$list_kel->kelurahan_id."'>".$list_kel->kelurahan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_daerah_list(){
        $propinsi_id = $this->input->get('propinsi_id',TRUE);
        $kabupaten_id = $this->input->get('kabupaten_id',TRUE);
        $kecamatan_id = $this->input->get('kecamatan_id',TRUE);
        $kelurahan_id = $this->input->get('kelurahan_id',TRUE);

        $kelurahan = $this->Pendaftaran_operasi_model->get_kelurahan($kecamatan_id);
        $kecamatan = $this->Pendaftaran_operasi_model->get_kecamatan($kabupaten_id);
        $kabupaten = $this->Pendaftaran_operasi_model->get_kabupaten($propinsi_id);

        $list_kabupaten = "<option value=\"\" disabled>Pilih Kabupaten</option>";
        foreach($kabupaten as $list_kab){
            if($kabupaten_id == $list_kab->kabupaten_id){
                $list_kabupaten .= "<option value='".$list_kab->kabupaten_id."' selected>".$list_kab->kabupaten_nama."</option>";
            }else{
                $list_kabupaten .= "<option value='".$list_kab->kabupaten_id."'>".$list_kab->kabupaten_nama."</option>";
            }

        }

        $list_kecamatan = "<option value=\"\" disabled>Pilih Kecamatan</option>";
        foreach($kecamatan as $list_kec){
            if($kecamatan_id == $list_kec->kecamatan_id){
                $list_kecamatan .= "<option value='".$list_kec->kecamatan_id."' selected>".$list_kec->kecamatan_nama."</option>";
            }else{
                $list_kecamatan .= "<option value='".$list_kec->kecamatan_id."'>".$list_kec->kecamatan_nama."</option>";
            }

        }

        $list_kelurahan = "<option value=\"\" disabled>Pilih Kelurahan</option>";
        foreach($kelurahan as $list_kel){
            if($kelurahan_id == $list_kel->kelurahan_id){
                $list_kelurahan .= "<option value='".$list_kel->kelurahan_id."' selected>".$list_kel->kelurahan_nama."</option>";
            }else{
                $list_kelurahan .= "<option value='".$list_kel->kelurahan_id."'>".$list_kel->kelurahan_nama."</option>";
            }
        }

        $res = array(
            "list_kabupaten" => $list_kabupaten,
            "list_kecamatan" => $list_kecamatan,
            "list_kelurahan" => $list_kelurahan,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_ruangan_list(){
        $paket_operasi = $this->input->get('paket_operasi',TRUE);
        $list_ruangan = $this->Pendaftaran_operasi_model->get_ruangan($paket_operasi);
//        if(count($list_ruangan) > 1){
//            $list = "<option value=\"\" disabled selected>Pilih Poli/Ruangan</option>";
//        }else{
//            $list = "";
//        }
        $list = "<option value=\"\" disabled selected>Pilih Poli/Ruangan</option>";
        foreach($list_ruangan as $list_ruang){
            $list .= "<option value='".$list_ruang->poliruangan_id."'>".$list_ruang->nama_poliruangan."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kelasruangan_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_kelasruangan = $this->Pendaftaran_operasi_model->get_kelas_poliruangan($poliruangan_id);
//        if(count($list_kelasruangan) > 1){
//            $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
//        }else{
//            $list = "";
//        }
        $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        foreach($list_kelasruangan as $list_kelas){
            $list .= "<option value='".$list_kelas->kelaspelayanan_id."'>".$list_kelas->kelaspelayanan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_dokter_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_dokter = $this->Pendaftaran_operasi_model->get_dokter($poliruangan_id);
        if(count($list_dokter) > 1){
            $list = "<option value=\"\" disabled selected>Pilih Dokter</option>";
        }else{
            $list = "";
        }
        foreach($list_dokter as $list_dktr){
            $list .= "<option value='".$list_dktr->id_M_DOKTER."'>".$list_dktr->NAME_DOKTER."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kamarruangan(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $paketoperasi_id = $this->input->get('paketoperasi_id',TRUE);
        $kelaspelayanan_id = $this->Pendaftaran_operasi_model->get_kelas_paket_operasi($paketoperasi_id);
        $list_kamarruangan = $this->Pendaftaran_operasi_model->get_kamar_aktif($poliruangan_id, $kelaspelayanan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kamar</option>";
//        $list = "";
        foreach($list_kamarruangan as $list_kamar){
            $status = $list_kamar->status_bed == '1' ? 'IN USE' : 'OPEN';
            if($list_kamar->status_bed == '0'){
                $list .= "<option value='".$list_kamar->kamarruangan_id."'>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }else{
                $list .= "<option value='".$list_kamar->kamarruangan_id."' disabled>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    public function hitung_umur(){
        $tgl_lahir = $this->input->get('tgl_lahir',TRUE);
        $tgl_lahir2 = date_create($tgl_lahir);
        $tgl_lahir3 = date_format($tgl_lahir2, "Y-m-d");
        $today=date("Y-m-d");
        $date1 = new DateTime($tgl_lahir3);
        $date2 = new DateTime($today);
        $interval = $date1->diff($date2);
        $umur = str_pad($interval->y, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($interval->m, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($interval->d, 2, '0', STR_PAD_LEFT).' Hr';

        $res = array(
            "umur" => $umur,
            "success" => true
        );

        echo json_encode($res);
    }
    public function ajax_list_ortu(){
        $list = $this->Pendaftaran_model->get_pasien_list();
        $data = array();
        $no = 1;
        foreach($list as $pasien){
            $row = array();

            $row[] = $no++;
            $row[] = $pasien->no_rekam_medis;
            $row[] = $pasien->pasien_nama;
            $row[] = $pasien->jenis_kelamin;
            $row[] = $pasien->tanggal_lahir;
            $row[] = $pasien->pasien_alamat;
            //add html for action
            $row[] = '<button class="btn btn-info btn-circle" onclick="pilihOrtu('."'".$pasien->pasien_id."'".')"><i class="fa fa-check"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pendaftaran_model->count_pasien_all(),
                    "recordsFiltered" => $this->Pendaftaran_model->count_pasien_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_pasien(){
        $list = $this->Pendaftaran_operasi_model->get_pasien_list();
        $data = array();
        $no = 1;
        foreach($list as $pasien){
            $row = array();
           $row[] = $no++;
            $row[] = $pasien->no_rekam_medis;
            $row[] = $pasien->no_bpjs;
            $row[] = $pasien->pasien_nama;
            $row[] = $pasien->nama_panggilan;
            $row[] = $pasien->jenis_kelamin;
            $row[] = $pasien->tanggal_lahir;
            $row[] = $pasien->tempat_lahir;
            $row[] = $pasien->umur;
            $row[] = $pasien->status_kawin;
            $row[] = $pasien->pasien_alamat ."Kel.".$pasien->kelurahan_nama ." Kec.".$pasien->kecamatan_nama ." , ".$pasien->kabupaten_nama;
            $row[] = $pasien->tlp_selular;
            $row[] = $pasien->agama_nama;
            $row[] = $pasien->pendidikan_nama;
            $row[] = $pasien->nama_pekerjaan;
            $row[] = $pasien->nama_orangtua;
            $row[] = $pasien->nama_suami_istri;
            //add html for action
            $row[] = '<button class="btn btn-info btn-circle" onclick="pilihPasien('."'".$pasien->pasien_id."'".')"><i class="fa fa-check"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pendaftaran_operasi_model->count_pasien_all(),
                    "recordsFiltered" => $this->Pendaftaran_operasi_model->count_pasien_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_get_pasien_by_id(){
        $id_m_pasien = $this->input->get('id_m_pasien',TRUE);
        $old_data = $this->Pendaftaran_operasi_model->get_pasien_by_id($id_m_pasien);

        if(count($old_data) > 0) {
            $status_pasien = $this->Pendaftaran_operasi_model->get_status_pendaftaran($old_data->pasien_id);
            $tgl_lhr = date_create($old_data->tanggal_lahir);
            $tgl_lahir = date_format($tgl_lhr, 'd F Y');
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data,
                'tgl_lahir' => $tgl_lahir,
                'status_pasien' => $status_pasien
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Pasien tidak ditemukan");
        }

        echo json_encode($res);
    }
}
