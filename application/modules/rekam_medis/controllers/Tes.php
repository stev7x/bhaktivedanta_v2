<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tes extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pendaftaran_operasi_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['list_jenis_kelamin']   = array('lk'=>'Laki-laki','pr'=>'Perempuan');
        $this->data['list_status_kawin']    = array('1'=>'Belum Kawin','2'=>'Kawin','3'=>'Duda/Janda','4'=>'Di Bawah Umur','5'=>'Lainnya');
        $this->data['list_warga_negara']    = array('1'=>'Indonesia','2'=>'Asing');
        $this->data['list_golongan_darah']  = array('a'=>'A','b'=>'B','ab'=>'AB','o'=>'O');
        $this->data['list_type_identitas']  = array('1'=>'KTP','2'=>'SIM','3'=>'Kartu Pelajar','4'=>'KTA','5'=>'KITAS','6'=>'Passpor','7'=>'Lainnya');
        $this->data['list_pekerjaan']  = array('1'=>'Swasta','2'=>'Wiraswasta','3'=>'IRT','4'=>'PNS','5'=>'TNI/Polri','6'=>'Lainnya');
        $this->data['list_hubungan']  = array('1'=>'Orang Tua','2'=>'Suami','3'=>'Istri','4'=>'Lainnya');
        $this->data['list_type_bayar']  = array('1'=>'Pribadi','2'=>'BPJS','3'=>'Instansi','4'=>'Lainnya');
        $this->data['next_no_rm'] = $this->Pendaftaran_operasi_model->get_no_rm();
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_paket_operasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "rekam_medis_pendaftaran_paket_operasi_view";
        $comments = "Listing Userrrr.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('tes', $this->data);
    }
}
