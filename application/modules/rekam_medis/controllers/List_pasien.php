<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_pasien extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pendaftaran_model');
        $this->load->model('List_pasien_model');
        $this->load->model('Reservasi_model');
        $this->load->model('masterdata/Karyawan_model');
        $this->load->model('rawatjalan/Pasien_rj_model');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['list_type_identitas']  = array('1'=>'KTP','2'=>'SIM','3'=>'Kartu Pelajar','4'=>'KTA','5'=>'KITAS','6'=>'Passpor','7'=>'Lainnya');
        $this->data['list_jenis_kelamin']   = array('Laki-laki'=>'Male','Perempuan'=>'Female');
        $this->data['list_status_kawin']    = array('1'=>'Belum Kawin','2'=>'Kawin','3'=>'Duda/Janda','4'=>'Di Bawah Umur','5'=>'Lainnya');
        $this->data['list_golongan_darah']  = array('a'=>'A','b'=>'B','ab'=>'AB','o'=>'O');
        $this->data['list_pekerjaan']  = array('1'=>'Swasta','2'=>'Wiraswasta','3'=>'IRT','4'=>'PNS','5'=>'TNI/Polri','6'=>'Lainnya');
        $this->data['list_warga_negara']    = array('1'=>'Indonesia','2'=>'Asing');
        $this->data['list_hubungan']  = array('1'=>'Orang Tua','2'=>'Suami','3'=>'Istri','4'=>'Lainnya');
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['next_no_rm'] = $this->List_pasien_model->get_no_rm();
        $this->data['list_agama'] = $this->Pendaftaran_model->get_agama();
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_list_pasien_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "rekam_medis_list_pasien_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_pasien', $this->data);
    }

    public function get_no_rekam_medis(){
         $res = array(
                    'no_rm' =>  $this->List_pasien_model->get_no_rm()
                );

        echo json_encode($res);

    }

    public function ajax_list_list_pasien(){
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $tlp_selular = $this->input->get('tlp_selular',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $no_identitas = $this->input->get('no_identitas',TRUE);
        $list = $this->List_pasien_model->get_list_pasien_list($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular, $no_bpjs, $no_identitas);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $list_pasien){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list_pasien->no_rekam_medis;
            $row[] = $list_pasien->pasien_nama;
            $row[] = $list_pasien->pasien_alamat;
            $row[] = $list_pasien->tlp_selular;
            $row[] = $list_pasien->no_bpjs;
            $row[] = $list_pasien->type_identitas."/".$list_pasien->no_identitas;
            //add html for action
            $row[] = '
                    <button class="btn btn-success btn-circle" data-toggle="modal" data-target="#modal_riwayat_penyakit_pasien" data-backdrop="static" data-keyboard="false" title="klik untuk Riwayat Penyaakit Pasien"  onclick="riwayatPenyakitPasien('."'".$list_pasien->pasien_id."'".')"><i class="fa fa-book"></i></button>
                    <button class="btn btn-info btn-circle" title="klik untuk Print"  onclick="form_print('."'".$list_pasien->pasien_id."'".')"><i class="fa fa-print"></i></button>
                    <button class="btn btn-warning btn-circle" title="klik untuk memperbarui"  onclick="editlist_pasien('."'".$list_pasien->pasien_id."'".')"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus" onclick="hapusListPasien('."'".$list_pasien->pasien_id."'".')"><i class="fa fa-trash"></i></button>
                    ';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->List_pasien_model->count_list_pasien_all($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas),
                    "recordsFiltered" => $this->List_pasien_model->count_list_pasien_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_list_pasien_by_id(){
        $pasien_id = $this->input->get('pasien_id',TRUE);
        $old_data = $this->List_pasien_model->get_list_pasien_by_id($pasien_id);
        $data_reservasi = $this->Pendaftaran_model->get_reservasi_by_no_rm($old_data->no_rekam_medis);

        date_default_timezone_set('Asia/Jakarta');
        setlocale(LC_ALL, 'IND');
        setlocale(LC_ALL, 'id_ID');

        $tgl_lahir_pas      = $old_data->tanggal_lahir;
        $tanggal_lahir_pas  = ($tgl_lahir_pas == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lahir_pas));

        $tgl_lhr_ortu =  $old_data->tgl_lahir_ortu;
        $tgl_lahir_ortu = ($tgl_lhr_ortu == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_ortu));

        $tgl_lhr_suami = $old_data->tgl_lahir_suami_istri;
        $tgl_lahir_suami = ($tgl_lhr_suami == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_suami));


        // print_r($old_data);die();

        if(count($old_data) > 0) {
            $res = array(
                'success'   => true,
                'messages'  => "Data found",
                'data'      => $old_data,
                'data_reservasi' => $data_reservasi,
                'tgl_lahir' => $tanggal_lahir_pas,
                'tgl_lahir_ortu' => $tgl_lahir_ortu,
                'tgl_lahir_suami' => $tgl_lahir_suami,
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_list_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_list_pasien_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_pasien', 'Nama Pasien', 'required|trim');
        // $this->form_validation->set_rules('upd_type_id', 'Type Identitas', 'required|trim');
        // $this->form_validation->set_rules('upd_no_identitas', 'No Identitas', 'required|trim');
        // $this->form_validation->set_rules('upd_tempat_lahir', 'Tempat Lahir', 'required|trim');
        // $this->form_validation->set_rules('upd_tgl_lahir', 'Tanggal Lahir', 'required|trim');
        // $this->form_validation->set_rules('upd_umur', 'Umur', 'required|trim');
        // $this->form_validation->set_rules('upd_jenis_kelamin', 'Jenis Kelamin', 'required|trim');
        // $this->form_validation->set_rules('upd_alamat', 'Alamat Pasien', 'required|trim');
        // $this->form_validation->set_rules('propinsi', 'Propinsi Pasien', 'required|trim');
        // $this->form_validation->set_rules('kabupaten', 'Kabupaten Pasien', 'required|trim');
        // $this->form_validation->set_rules('kecamatan', 'Kecamatan Pasien', 'required|trim');
        // $this->form_validation->set_rules('kelurahan', 'Kelurahan Pasien', 'required|trim');

        // $this->form_validation->set_rules('upd_propinsi_tinggal', 'Propinsi Pasien', 'required|trim');
        // $this->form_validation->set_rules('upd_kabupaten_tinggal', 'Kabupaten Pasien', 'required|trim');
        // $this->form_validation->set_rules('upd_kecamatan_tinggal', 'Kecamatan Pasien', 'required|trim');
        // $this->form_validation->set_rules('upd_kelurahan_tinggal', 'Kelurahan Pasien', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_list_pasien_view";
            $comments = "Gagal update list_pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){



            $pasien_id = $this->input->post('upd_pasien_id',TRUE);
            $nama_list_pasien = $this->input->post('upd_nama_list_pasien', TRUE);
            $tgl_lahir_pasien = $this->input->post('upd_tgl_lahir',TRUE);
            // print_r($tgl_lahir_pasien); die();

            if (!empty($tgl_lahir_pasien)) {
                $tgl_lahir        = $this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir_pasien));
            }else{
                $tgl_lahir = null;
            }

            $tgl_lahir_ortu = $this->input->post('upd_tgl_lahir_ortu',TRUE);
            if(!empty($tgl_lahir_ortu)) {
                $tgl_lahir_orangtua = $this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir_ortu));
            }else{
                $tgl_lahir_orangtua = null;
            }

            $tgl_lahir_suami_istri = $this->input->post('upd_tgl_suami_istri',TRUE);
            if(!empty($tgl_lahir_suami_istri)) {
                $tgl_lahir_sustri      = $this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir_suami_istri));
            }else{
                $tgl_lahir_sustri = null;
            }

            // var_dump($tgl_lahir_orangtua, $tgl_lahir);
            // var_dump($tgl_lahir_sustri);
            // die();

            // $tgl_lahir1 = date_create($this->input->post('upd_tgl_lahir',TRUE));
            // $tgl_lahir2 = date_format($tgl_lahir1, 'Y-m-d');
            $datapasien['type_identitas'] = $this->input->post('upd_type_id',TRUE);
            $datapasien['no_identitas'] = $this->input->post('upd_no_identitas',TRUE);
            $datapasien['no_rekam_medis'] = $this->input->post('upd_no_rm',TRUE);
            $datapasien['no_bpjs'] = $this->input->post('upd_no_bpjs',TRUE);
            $datapasien['pasien_nama'] = $this->input->post('upd_nama_pasien',TRUE);
            $datapasien['pasien_alamat'] = $this->input->post('upd_alamat',TRUE);
            $datapasien['nama_panggilan'] = $this->input->post('upd_nama_panggilan', TRUE);
            $datapasien['tempat_lahir'] = $this->input->post('upd_tempat_lahir',TRUE);
            $datapasien['tanggal_lahir'] = $tgl_lahir;
            $datapasien['umur'] = $this->input->post('upd_umur',TRUE);
            $datapasien['jenis_kelamin'] = $this->input->post('upd_jenis_kelamin',TRUE);
            $datapasien['status_kawin'] = $this->input->post('upd_status_kawin',TRUE);
            $datapasien['tlp_rumah'] = $this->input->post('upd_no_tlp_rmh',TRUE);
            $datapasien['tlp_selular'] = $this->input->post('upd_no_mobile',TRUE);
            $datapasien['warga_negara'] = $this->input->post('upd_warga_negara',TRUE);
            $datapasien['nama_suami_istri'] = $this->input->post('upd_nama_suami_istri',TRUE);
            $datapasien['tgl_lahir_suami_istri'] = $tgl_lahir_sustri;
            $datapasien['propinsi_id'] = $this->input->post('propinsi',TRUE);
            $datapasien['kabupaten_id'] = $this->input->post('kabupaten',TRUE);
            $datapasien['kecamatan_id'] = $this->input->post('kecamatan',TRUE);
            $datapasien['kelurahan_id'] = $this->input->post('kelurahan',TRUE);
            $datapasien['pekerjaan_id'] = $this->input->post('upd_pekerjaan',TRUE);
            $datapasien['provinsi_tinggal_id'] = $this->input->post('upd_propinsi_tinggal',TRUE);
            $datapasien['kabupaten_tinggal_id'] = $this->input->post('upd_kabupaten_tinggal',TRUE);
            $datapasien['kecamatan_tinggal_id'] = $this->input->post('upd_kecamatan_tinggal',TRUE);
            $datapasien['kelurahan_tinggal_id'] = $this->input->post('upd_kelurahan_tinggal',TRUE);
            $datapasien['pekerjaan_id'] = $this->input->post('upd_pekerjaan',TRUE);
            $datapasien['agama_id'] = $this->input->post('upd_agama',TRUE);
            $datapasien['pendidikan_id'] = $this->input->post('upd_pendidikan',TRUE);
            $datapasien['catatan_khusus'] = $this->input->post('upd_catatan_khusus',TRUE);
            $datapasien['bahasa_id'] = $this->input->post('upd_bahasa',TRUE);
            $datapasien['nama_orangtua'] = $this->input->post('upd_nama_orangtua',TRUE);
            $datapasien['tgl_lahir_ortu'] = $tgl_lahir_orangtua;
            $datapasien['umur_ortu'] = $this->input->post('upd_umur_ortu',TRUE);
            $datapasien['allergy_history'] = $this->input->post('upd_allergy_history',TRUE);
            $datapasien['pregnant'] = $this->input->post('upd_pregnant',TRUE);
            $datapasien['current_medication'] = $this->input->post('upd_current_medication',TRUE);
            $datapasien['weight'] = $this->input->post('upd_weight',TRUE);
            $datapasien['medical_history'] = $this->input->post('upd_medical_history',TRUE);
            $datapasien['medical_event'] = $this->input->post('upd_medical_event',TRUE);
            // $datapasien['layanan_diskon'] = $this->input->post('upd_layanan_diskon',TRUE);

            // var_dump($datapasien);die();
            $update = $this->List_pasien_model->update_list_pasien($datapasien, $pasien_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'list_pasien berhasil diubah'
                );

                // if permitted, do logit
                $perms = "rekam_medis_list_pasien_view";
                $comments = "Berhasil mengubah list_pasien  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah list_pasien , hubungi web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_list_pasien_view";
                $comments = "Gagal mengubah list_pasien  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_list_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_list_pasien_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pasien_id = $this->input->post('pasien_id', TRUE);

        $delete = $this->List_pasien_model->delete_list_pasien($pasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'list_pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rekam_medis_list_pasien_view";
            $comments = "Berhasil menghapus list_pasien dengan id list_pasien = '". $pasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_list_pasien_view";
            $comments = "Gagal menghapus data list_pasien dengan ID = '". $pasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    function get_daerah_list(){
        $propinsi_id = $this->input->get('propinsi_id',TRUE);
        $kabupaten_id = $this->input->get('kabupaten_id',TRUE);
        $kecamatan_id = $this->input->get('kecamatan_id',TRUE);
        $kelurahan_id = $this->input->get('kelurahan_id',TRUE);

        $kelurahan = $this->List_pasien_model->get_kelurahan($kecamatan_id);
        $kecamatan = $this->List_pasien_model->get_kecamatan($kabupaten_id);
        $kabupaten = $this->List_pasien_model->get_kabupaten($propinsi_id);

        $list_kabupaten = "<option value=\"\" disabled>Pilih Kabupaten</option>";
        foreach($kabupaten as $list_kab){
            if($kabupaten_id == $list_kab->kabupaten_id){
                $list_kabupaten .= "<option value='".$list_kab->kabupaten_id."' selected>".$list_kab->kabupaten_nama."</option>";
            }else{
                $list_kabupaten .= "<option value='".$list_kab->kabupaten_id."'>".$list_kab->kabupaten_nama."</option>";
            }

        }

        $list_kecamatan = "<option value=\"\" disabled>Pilih Kecamatan</option>";
        foreach($kecamatan as $list_kec){
            if($kecamatan_id == $list_kec->kecamatan_id){
                $list_kecamatan .= "<option value='".$list_kec->kecamatan_id."' selected>".$list_kec->kecamatan_nama."</option>";
            }else{
                $list_kecamatan .= "<option value='".$list_kec->kecamatan_id."'>".$list_kec->kecamatan_nama."</option>";
            }

        }

        $list_kelurahan = "<option value=\"\" disabled>Pilih Kelurahan</option>";
        foreach($kelurahan as $list_kel){
            if($kelurahan_id == $list_kel->kelurahan_id){
                $list_kelurahan .= "<option value='".$list_kel->kelurahan_id."' selected>".$list_kel->kelurahan_nama."</option>";
            }else{
                $list_kelurahan .= "<option value='".$list_kel->kelurahan_id."'>".$list_kel->kelurahan_nama."</option>";
            }
        }

        $res = array(
            "list_kabupaten" => $list_kabupaten,
            "list_kecamatan" => $list_kecamatan,
            "list_kelurahan" => $list_kelurahan,
            "success" => true
        );

        echo json_encode($res);
    }
    function get_kabupaten_list(){
        $propinsi_id = $this->input->get('propinsi_id',TRUE);
        $list_kabupaten = $this->List_pasien_model->get_kabupaten($propinsi_id);
        $list = "<option value=\"\" disabled selected>Pilih Kabupaten</option>";
        foreach($list_kabupaten as $list_kab){
            $list .= "<option value='".$list_kab->kabupaten_id."'>".$list_kab->kabupaten_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kecamatan_list(){
        $kabupaten_id = $this->input->get('kabupaten_id',TRUE);
        $list_kecamatan = $this->List_pasien_model->get_kecamatan($kabupaten_id);
        $list = "<option value=\"\" disabled selected>Pilih Kecamatan</option>";
        foreach($list_kecamatan as $list_kec){
            $list .= "<option value='".$list_kec->kecamatan_id."'>".$list_kec->kecamatan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kelurahan_list(){
        $kecamatan_id = $this->input->get('kecamatan_id',TRUE);
        $list_kelurahan = $this->List_pasien_model->get_kelurahan($kecamatan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelurahan</option>";
        foreach($list_kelurahan as $list_kel){
            $list .= "<option value='".$list_kel->kelurahan_id."'>".$list_kel->kelurahan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }
    public function hitung_umur(){
        // $tgl_lahir = $this->input->get('tgl_lahir',TRUE);
        // $tgl_lahir2 = date_create($tgl_lahir);
        // $tgl_lahir3 = date_format($tgl_lahir2, "Y-m-d");
        // $today=date("Y-m-d");
        // $date1 = new DateTime($tgl_lahir3);
        // $date2 = new DateTime($today);
        // $interval = $date1->diff($date2);
        // $umur = str_pad($interval->y, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($interval->m, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($interval->d, 2, '0', STR_PAD_LEFT).' Hr';

        $tgl_lahir = $this->input->get('tgl_lahir',TRUE);
        // $tgl_lahir2 = date_create($tgl_lahir);
        $tgl_lahir3 = $this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir));
        // print_r($tgl_lahir3); die();
        $today=date("Y-m-d");
        $date1 = new DateTime($tgl_lahir3);
        $date2 = new DateTime($today);
        $interval = $date1->diff($date2);
        $umur = str_pad($interval->y, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($interval->m, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($interval->d, 2, '0', STR_PAD_LEFT).' Hr';

        $res = array(
            "umur" => $umur,
            "success" => true
        );

        echo json_encode($res);
    }


    public function do_print(){
        $pasien_id = $this->input->post('pasien_id');
        $nama_ibu = $this->input->post('nama_ibu');
        $nama_ayah = $this->input->post('nama_ayah');
        $alamat = $this->input->post('alamat');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $nama_anak = $this->input->post('nama_anak');
        $anak_ke = $this->input->post('anak_ke');
        $panjang_badan = $this->input->post('panjang_badan');
        $berat_badan = $this->input->post('berat_badan');
        $nama_dokter = $this->input->post('nama_dokter');

        $data = array(
            // "data_pasien" => array(
            "pasien_id"     => $pasien_id,
            "nama_ibu"      => $nama_ibu,
            "nama_ayah"     => $nama_ayah,
            "alamat"        => $alamat,
            "tgl_lahir"     => $tgl_lahir,
            "nama_anak"     => $nama_anak,
            "anak_ke"       => $anak_ke,
            "panjang_badan" => $panjang_badan,
            "berat_badan"   => $berat_badan,
            "nama_dokter"   => $nama_dokter
        );

        $check = $query = $this->db->get_where('t_surat_ket_lahir', array('pasien_id' => $pasien_id), 1, 0);

        // print_r($check);die();

        if($check->num_rows() > 0){
            $query = $this->List_pasien_model->update_pasien($data,$pasien_id);
            $res = array(
            "messege" => "Data berhasil di di Update",
            "pasien_id" =>$pasien_id,
            "success" => true
            );
        } else{
            $query = $this->List_pasien_model->insert_pasien($data);
            $res = array(
            "messages" => "data berhasil di Insert ",
            "pasien_id" =>$pasien_id,
            "success"  => true
            );
        }
        echo json_encode($res);
        // $this->load->view("rekam_medis/print_anak",$data_pasien);

        // print_r($data);die();
    }
    public function do_print_surat($data){
        $print['data_pasien'] = $this->List_pasien_model->get_data_ket_lahir($data);
        $this->load->view('rekam_medis/print_anak',$print);

    }


    public function print_data($pasien_id,$view){


        $datapasien = $this->List_pasien_model->get_data_print($pasien_id);
        $print['data_pasien'] = $datapasien;

        $print['barcode'] = "<img alt='barcode' width='500' height='500' src='".base_url()."/rekam_medis/list_pasien/create_barcode/".$datapasien->no_rekam_medis."'>";


        $this->load->view('rekam_medis/'.$view,$print);
    }

    public function create_barcode($no_rm){
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');

        $barcodeOptions = array(
            'text' => $no_rm,
            'barHeight'=> 74,
            'factor'=>3.98,
        );
        //generate barcode
        Zend_Barcode::render('code128', 'image', $barcodeOptions, array());

    }

    public function do_create_pasien(){
        /* Permission setting di auuth perms */
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_list_pasien_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');

        /* Data Pasien */
        $this->form_validation->set_rules('no_rm', 'No. Rekam Medis', 'required|trim');
        // $this->form_validation->set_rules('no_ktp', 'No. KTP', 'required|trim');
        // $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap Karyawan', 'required|trim');
        // $this->form_validation->set_rules('nama_panggilan', 'Nama Panggilan Karyawan', 'required|trim');
        // $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|trim');
        // $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        // $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');
        // $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        // $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error
            );

            // if permitted, do logit
            $perms = "rekam_medis_list_pasien_create";
            $comments = "Gagal melakukan pendaftaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{

            /* Data Pasien */

            $tgl_lahir_pasien = $this->input->post('tgl_lahir',TRUE);
            $tgl_lahir        = $this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir_pasien));

            // var_dump($tgl_lahir);
            // die();
            // $format_tanggal = date_create($this->input->post('tgl_lahir',TRUE));
            // $tgl_lahir = date_format($format_tanggal, 'Y-m-d');

            $data_pasien['no_rekam_medis'] = $this->input->post('no_rm',TRUE);
            $data_pasien['type_identitas'] = "KTP";
            $data_pasien['no_identitas'] = $this->input->post('no_ktp',TRUE);
            $data_pasien['pasien_nama'] = $this->input->post('nama_lengkap',TRUE);
            $data_pasien['nama_panggilan'] = $this->input->post('nama_panggilan',TRUE);
            $data_pasien['tempat_lahir'] = $this->input->post('tempat_lahir',TRUE);
            $data_pasien['pasien_alamat'] = $this->input->post('alamat',TRUE);
            $data_pasien['tanggal_lahir'] = $tgl_lahir;
            $data_pasien['umur'] = $this->input->post('umur',TRUE);
            $data_pasien['jenis_kelamin'] = $this->input->post('jenis_kelamin',TRUE);
            $data_pasien['no_bpjs'] = $this->input->post('no_bpjs',TRUE);
            $data_pasien['agama_id'] = $this->input->post('agama',TRUE);
            $data_pasien['status_kawin'] = $this->input->post('status_kawin',TRUE);
            $data_pasien['status_karyawan'] = 1;
            $data_pasien['layanan_diskon'] = $this->input->post('layanan_diskon', TRUE);

            /* Data Detail No Rekam Medis */
            $data_rm['no_rekam_medis'] = $this->input->post('no_rm',TRUE);

            $this->db->trans_begin();


            $insert_data_pasien = $this->List_pasien_model->insert_pasien_kary($data_pasien);

            if ($insert_data_pasien) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Register has been saved to database'
                );

                // if permitted, do logit
                $perms = "rekam_medis_list_pasien_create";
                $comments = "Success to Create a new patient register with post data";
                $this->aauth->logit($perms, current_url(), $comments);

                $this->Karyawan_model->insert_rm($data_rm);

            }else{

                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Failed insert user group to database, please contact web administrator.');

                // if permitted, do logit
                $perms = "master_data_karyawan_create";
                $comments = "Failed to Create a new patient register when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }


            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }
        echo json_encode($res);
    }



    function exportAllPasien($rm_awal, $rm_akhir){
        $this->load->library("Excel");
        ini_set('memory_limit','-1');
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Data Pasien");

        $data = $this->List_pasien_model->getAllPasien($rm_awal, $rm_akhir);

        $object->getActiveSheet()
                        ->getStyle("A2:AF2")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A2:AF2")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
         $object->getActiveSheet()
                        ->getStyle("L3:N3")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("L3:N3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
         $object->getActiveSheet()
                        ->getStyle("W3:Y3")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("W3:Y3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $object->getActiveSheet()
                        ->getStyle("AB3:AD3")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("AB3:AD3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        // $object->getActiveSheet()
        //                 ->getStyle("D4:D10")
        //                 ->getNumberFormat()
        //                 ->setFormatCode( '0000-000-0000' );

        // $range = "D4:D10";
        // $object->getActiveSheet()
        //     ->getStyle($range)
        //     ->getNumberFormat()
        //     ->setFormatCode('0000-0000-0000-0000');


        $object->getActiveSheet()
                                ->mergeCells('L2:N2')
                                ->mergeCells('W2:Y2')
                                ->mergeCells('AB2:AD2');
        $object->getActiveSheet()
                                ->mergeCells('A2:A3')->mergeCells('B2:B3')->mergeCells('C2:C3')->mergeCells('D2:D3')
                                ->mergeCells('E2:E3')->mergeCells('F2:F3')->mergeCells('G2:G3')->mergeCells('H2:H3')
                                ->mergeCells('I2:I3')->mergeCells('J2:J3')->mergeCells('K2:K3')->mergeCells('O2:O3')
                                ->mergeCells('P2:P3')->mergeCells('Q2:Q3')->mergeCells('V2:V3')->mergeCells('Z2:Z3')
                                ->mergeCells('R2:R3')->mergeCells('S2:S3')->mergeCells('T2:T3')->mergeCells('U2:U3')
                                ->mergeCells('AA2:AA3')->mergeCells('AE2:AE3')->mergeCells('AF2:AF3');
        $table_columns = array("NO RM","NAMA","NAMA PANGGILAN","NO KTP","NO KARTU BPJS","ALAMAT","KELURAHAN","KECAMATAN","KOTA/KABUPATEN","L/P","TEMPAT LAHIR","TGL","BLN","THN","UMUR","AGAMA","PEKERJAAN","PENDIDIKAN TERAKHIR","KEBANGSAAN","TELP","STATUS PERKAWINAN","NAMA SUAMI/ISTRI","TGL","BLN","THN","UMUR","NAMA ORTU","TGL","BLN","THN","UMUR","CATATAN KHUSUS");
        $table_columns2 = array("NO RM","NAMA","NAMA PANGGILAN","NO KTP","NO KARTU BPJS","ALAMAT","KELURAHAN","KECAMATAN","KOTA/KABUPATEN","L/P","TEMPAT LAHIR","TANGGAL LAHIR","","","UMUR","AGAMA","PEKERJAAN","PENDIDIKAN TERAKHIR","KEBANGSAAN","TELP","STATUS PERKAWINAN","NAMA SUAMI/ISTRI","TANGGAL LAHIR","","","UMUR","NAMA ORTU","TANGGAL LAHIR","","","UMUR","CATATAN KHUSUS");
        $column = 0;
        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }
        $column = 0;
        foreach($table_columns2 as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 2, $field);
            $column++;
        }

        // echo "<pre>";
        // echo print_r($data);
        // echo "</pre>";
        // var_dump($data);
        // die();

        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data as $row){


            $no_identitas = strval($row->no_identitas);
            if($no_identitas != ""){
                $no_identitas1 = "`".$no_identitas;
            }else{
                $no_identitas1 = "";
            }



            if($row->jenis_kelamin == "Laki-laki"){
                $jenis_kelamin = "L";
            }else if ($row->jenis_kelamin == "Perempuan") {
                $jenis_kelamin = "P";
            }else{
                $jenis_kelamin = "";
            }

            $today      = date('Y-m-d');
            $tgl_lahir  = $row->tanggal_lahir;
            if($tgl_lahir != "0000-00-00"){
                $tahun       = date_diff(date_create($tgl_lahir), date_create($today))->y;
                $bulan       = date_diff(date_create($tgl_lahir), date_create($today))->m;
                $hari       = date_diff(date_create($tgl_lahir), date_create($today))->d;
                $umur       = $tahun." Th ".$bulan." Bln ".$hari." Hr";
            }else{
                $umur = "";
            }
            $no++;
            $object->getActiveSheet()
                            ->setCellValueByColumnAndRow(0, $excel_row, $row->no_rekam_medis)
                            ->setCellValueByColumnAndRow(1, $excel_row, $row->pasien_nama)
                            ->setCellValueByColumnAndRow(2, $excel_row, $row->nama_panggilan)
                            ->setCellValueByColumnAndRow(3, $excel_row, $row->no_identitas."\t")
                            ->setCellValueByColumnAndRow(4, $excel_row, $row->no_bpjs)
                            ->setCellValueByColumnAndRow(5, $excel_row, $row->pasien_alamat)
                            ->setCellValueByColumnAndRow(6, $excel_row, $row->kelurahan_nama)
                            ->setCellValueByColumnAndRow(7, $excel_row, $row->kecamatan_nama)
                            ->setCellValueByColumnAndRow(8, $excel_row, $row->kabupaten_nama)
                            ->setCellValueByColumnAndRow(9, $excel_row, $jenis_kelamin)
                            ->setCellValueByColumnAndRow(10, $excel_row, $row->tempat_lahir)
                            ->setCellValueByColumnAndRow(11, $excel_row, ($row->tanggal_lahir == "0000-00-00") ? '' : date('d', strtotime($row->tanggal_lahir)))
                            ->setCellValueByColumnAndRow(12, $excel_row, ($row->tanggal_lahir == "0000-00-00") ? '' : date('m', strtotime($row->tanggal_lahir)))
                            ->setCellValueByColumnAndRow(13, $excel_row, ($row->tanggal_lahir == "0000-00-00") ? '' : date('Y', strtotime($row->tanggal_lahir)))
                            ->setCellValueByColumnAndRow(14, $excel_row, $umur)
                            ->setCellValueByColumnAndRow(15, $excel_row, $row->agama_nama)
                            ->setCellValueByColumnAndRow(16, $excel_row, $row->nama_pekerjaan)
                            ->setCellValueByColumnAndRow(17, $excel_row, $row->pendidikan_nama)
                            ->setCellValueByColumnAndRow(18, $excel_row, $row->warga_negara)
                            ->setCellValueByColumnAndRow(19, $excel_row, $row->tlp_selular)
                            ->setCellValueByColumnAndRow(20, $excel_row, $row->status_kawin)
                            ->setCellValueByColumnAndRow(21, $excel_row, $row->nama_suami_istri)
                            ->setCellValueByColumnAndRow(22, $excel_row, ($row->tgl_lahir_suami_istri == "0000-00-00") ? '' : date('d', strtotime($row->tgl_lahir_suami_istri)))
                            ->setCellValueByColumnAndRow(23, $excel_row, ($row->tgl_lahir_suami_istri == "0000-00-00") ? '' : date('m', strtotime($row->tgl_lahir_suami_istri)))
                            ->setCellValueByColumnAndRow(24, $excel_row, ($row->tgl_lahir_suami_istri == "0000-00-00") ? '' : date('Y', strtotime($row->tgl_lahir_suami_istri)))
                            ->setCellValueByColumnAndRow(25, $excel_row, $row->umur_suami_istri)
                            ->setCellValueByColumnAndRow(26, $excel_row, $row->nama_orangtua)
                            ->setCellValueByColumnAndRow(27, $excel_row, ($row->tgl_lahir_ortu == "0000-00-00") ? '' : date('d', strtotime($row->tgl_lahir_ortu)))
                            ->setCellValueByColumnAndRow(28, $excel_row, ($row->tgl_lahir_ortu == "0000-00-00") ? '' : date('m', strtotime($row->tgl_lahir_ortu)))
                            ->setCellValueByColumnAndRow(29, $excel_row, ($row->tgl_lahir_ortu == "0000-00-00") ? '' : date('Y', strtotime($row->tgl_lahir_ortu)))
                            ->setCellValueByColumnAndRow(30, $excel_row, $row->umur_ortu)
                            ->setCellValueByColumnAndRow(31, $excel_row, $row->catatan_khusus);
            $excel_row++;
            // var_dump($row->tanggal_lahir);
        }
        // die();
        foreach(range('A','Z') as $columnID) {
                   $object->getActiveSheet()
                                        ->getColumnDimension($columnID)
                                        ->setAutoSize(true);
            }

            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => '000000'),
                    ),
                ),
            );

            $object->getActiveSheet()->getStyle('A2:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

            // $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
            // $cacheSettings = array( ' memoryCacheSize ' => '14MB');
            // PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
            $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="DATA PASIEN '.$rm_awal.' - '.$rm_akhir.'.xlsx"');
            $object_writer->save('php://output');
            exit();
    }


    function autocomplete_pasien(){
        // $val_pasien = $this->input->get('val_pasien');
        if(empty($val_pasien)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        // }else if ($val_pasien != ""){
            // $request = mysqli_real_escape_string($_POST["query"]);
        $request = $this->input->post('keyword');
            // var_dump($request);
            // die();
            $pasien_list = $this->List_pasien_model->get_pasien_autocomplete($request);


            // echo "<pre>";
            // echo print_r($pasien_list);
            // echo "</pre>";
            // die();
            if(!empty($pasien_list)) {
                // $parent = $qry->row(1);
                for ($i=0; $i<count($pasien_list); $i++){
                    $status_pasien = $this->List_pasien_model->get_status_pendaftaran($pasien_list[$i]->pasien_id);
                    $tgl_lhr = date_create($pasien_list[$i]->tanggal_lahir);
                    $tgl_lahir = date_format($tgl_lhr, 'd F Y');
                    $list[] = array(
                        "id"                    => $pasien_list[$i]->pasien_id,
                        "value"                 => $pasien_list[$i]->no_rekam_medis,
                        "pasien_nama"           => $pasien_list[$i]->pasien_nama,
                        "alamat"                => $pasien_list[$i]->pasien_alamat,
                        "nama_panggilan"        => $pasien_list[$i]->nama_panggilan,
                        "no_rekam_medis"        => $pasien_list[$i]->no_rekam_medis,
                        "no_bpjs"               => $pasien_list[$i]->no_bpjs,
                        "type_id"               => $pasien_list[$i]->type_identitas,
                        "no_identitas"          => $pasien_list[$i]->no_identitas,
                        "tempat_lahir"          => $pasien_list[$i]->tempat_lahir,
                        "tanggal_lahir"         => $tgl_lahir,
                        "umur"                  => $pasien_list[$i]->umur,
                        "jenis_kelamin"         => $pasien_list[$i]->jenis_kelamin,
                        "status_kawin"          => $pasien_list[$i]->status_kawin,
                        "tlp_rumah"             => $pasien_list[$i]->tlp_rumah,
                        "tlp_selular"           => $pasien_list[$i]->tlp_selular,
                        "warga_negara"          => $pasien_list[$i]->warga_negara,
                        "propinsi"              => $pasien_list[$i]->propinsi_id,
                        "kabupaten"             => $pasien_list[$i]->kabupaten_id,
                        "kecamatan"             => $pasien_list[$i]->kecamatan_id,
                        "kelurahan"             => $pasien_list[$i]->kelurahan_id,
                        "pekerjaan"             => $pasien_list[$i]->pekerjaan_id,
                        "agama"                 => $pasien_list[$i]->agama_id,
                        "bahasa"                => $pasien_list[$i]->bahasa_id,
                        "pendidikan"            => $pasien_list[$i]->pendidikan_id,
                        "nama_suami_istri"      => $pasien_list[$i]->nama_suami_istri,
                        "tgl_lahir_suami_istri" => $pasien_list[$i]->tgl_lahir_suami_istri,
                        "umur_suami_istri"      => $pasien_list[$i]->umur_suami_istri,
                        "nama_orangtua"         => $pasien_list[$i]->nama_orangtua,
                        "tgl_lahir_ortu"        => $pasien_list[$i]->tgl_lahir_ortu,
                        "umur_ortu"             => $pasien_list[$i]->umur_ortu,
                        "catatan_khusus"        => $pasien_list[$i]->catatan_khusus,

                        // "nama_keluarga"          => $pasien_list[$i]->nama_keluarga,
                        // "status_hubungan"          => $pasien_list[$i]->status_hubungan,
                        // "alergi"          => $pasien_list[$i]->alergi,
                        "status"                => $status_pasien,
                        // "status"          => $pasien_list[$i]->s,
                    );
                }

                $res = array(
                    'success' => true,
                    'messages' => "Pasien ditemukan",
                    'data' => $list
                    );
            }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
            }
        }
        echo json_encode($res);
    }

    public function riwayatPenyakitPasien($pasien_id){
        // var_dump($pasien_id);
        $riwayat_penyakit_pasien['list_data_pasien'] = $this->List_pasien_model->get_riwayat_penyakit_pasien($pasien_id);
        // $riwayat_penyakit_pasien = $this->List_pasien_model->get_riwayat_penyakit_pasien($pasien_id);
        // echo "<pre>";
        // print_r($riwayat_penyakit_pasien);
        // echo "</pre>";
        // die();
        $this->load->view('v_riwayat_penyakit_pasien', $riwayat_penyakit_pasien);
    }



    public function ajax_list_riwayat_pasien_rj(){
        $pasien_id = $this->input->get('pasien_id',TRUE);
        $list      = $this->List_pasien_model->get_riwayat_pasien_rj($pasien_id);
        $data           = array();
        $no             = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $riwayat){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d/m/Y H:i:s', strtotime($riwayat->tgl_pendaftaran));
            $row[] = $riwayat->no_pendaftaran;
            $row[] = $riwayat->nama_poliruangan;
            $row[] = $riwayat->jam_periksa;
            $row[] = $riwayat->NAME_DOKTER;
            $row[] = '
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail tindakan" data-toggle="modal" data-target="#modal_detail_tindakan"  data-backdrop="static" data-keyboard="false" onclick="detailTindakan('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Tindakan</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_diagnosa"  data-backdrop="static" data-keyboard="false" onclick="detailDiagnosa('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Diagnosa</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_obat" data-backdrop="static" data-keyboard="false"  onclick="detailObat('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Obat</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_alergi"  data-backdrop="static" data-keyboard="false" onclick="detailAlergi('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Alergi</button>
                    ';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_diagnosa_pasienrj(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list           = $this->Pasien_rj_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data           = array();
        $no             = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $diagnosa){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d/m/Y', strtotime($diagnosa->tgl_diagnosa));
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->kode_icd10;
            $row[] = $diagnosa->nama_icd10;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->diagnosa_by;
            // $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_tindakan_pasienrj(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list           = $this->Pasien_rj_model->get_tindakan_pasien_list($pendaftaran_id);
        $data           = array();
        $no             = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $tindakan){
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = date('d/m/Y', strtotime($tindakan->tgl_tindakan));
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            $data[] = $row;
        }
        $output = array(
                    "draw"              => $this->input->get('draw'),
                    "recordsTotal"      => count($list),
                    "recordsFiltered"   => count($list),
                    "data"              => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_obat_pasienrj(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list           = $this->Pasien_rj_model->get_resep_pasien_list($pendaftaran_id);
        $data           = array();
        $no             = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $tindakan){
            $row   = array();
            $row[] = date('d/m/Y', strtotime($tindakan->tgl_reseptur));
            $row[] = $tindakan->nama_barang;
            $row[] = $tindakan->nama_jenis;
            $row[] = $tindakan->nama_sediaan;
            $row[] = $tindakan->qty;
            $row[] = $tindakan->NAME_DOKTER;
            $row[] = $tindakan->harga_jual;
            $row[] = ($tindakan->harga_jual * $tindakan->qty);
            $data[] = $row;
        }
        $output = array(
                    "draw"              => $this->input->get('draw'),
                    "recordsTotal"      => count($list),
                    "recordsFiltered"   => count($list),
                    "data"              => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_alergi_pasienrj(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list           = $this->Pasien_rj_model->get_alergi_pasien_list($pendaftaran_id);
        $data           = array();
        $no             = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $tindakan){
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $tindakan->tanggal;
            $row[] = $tindakan->nama_alergi;
            $data[] = $row;
        }
        $output = array(
                    "draw"              => $this->input->get('draw'),
                    "recordsTotal"      => count($list),
                    "recordsFiltered"   => count($list),
                    "data"              => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_riwayat_pasien_igd(){
        $pasien_id = $this->input->get('pasien_id',TRUE);
        $list      = $this->List_pasien_model->get_riwayat_pasien_igd($pasien_id);
        $data           = array();
        $no             = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $riwayat){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d/m/Y H:i:s', strtotime($riwayat->tgl_pendaftaran));
            $row[] = $riwayat->no_pendaftaran;
            $row[] = $riwayat->nama_poliruangan;
            $row[] = $riwayat->jam_periksa;
            $row[] = $riwayat->NAME_DOKTER;
            $row[] = '
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail tindakan" data-toggle="modal" data-target="#modal_detail_tindakan" data-backdrop="static" data-keyboard="false" onclick="detailTindakan('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Tindakan</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_diagnosa" data-backdrop="static" data-keyboard="false" onclick="detailDiagnosa('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Diagnosa</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_obat" data-backdrop="static" data-keyboard="false" onclick="detailObat('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Obat</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_alergi" data-backdrop="static" data-keyboard="false" onclick="detailAlergi('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Alergi</button>
                    ';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_riwayat_pasien_ri(){
        $pasien_id = $this->input->get('pasien_id',TRUE);
        $list      = $this->List_pasien_model->get_riwayat_pasien_ri($pasien_id);
        $data           = array();
        $no             = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $riwayat){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d/m/Y H:i:s', strtotime($riwayat->tgl_pendaftaran));
            $row[] = $riwayat->no_pendaftaran;
            $row[] = $riwayat->nama_poliruangan;
            $row[] = $riwayat->jam_periksa;
            $row[] = $riwayat->NAME_DOKTER;
            $row[] = '
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail tindakan" data-toggle="modal" data-target="#modal_detail_tindakan"  data-backdrop="static" data-keyboard="false" onclick="detailTindakan('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Tindakan</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_diagnosa"  data-backdrop="static" data-keyboard="false" onclick="detailDiagnosa('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Diagnosa</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_obat" data-backdrop="static" data-keyboard="false"  onclick="detailObat('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Obat</button>
                    <button type="button" style="margin-bottom: 8px" class="btn btn-info btn-md" title="Klik untuk detail diagnosa" data-toggle="modal" data-target="#modal_detail_alergi"  data-backdrop="static" data-keyboard="false" onclick="detailAlergi('."'".$riwayat->pendaftaran_id."'".')"><i class="fa fa-eye"></i> Alergi</button>
                    ';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
}
