<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pasien_penunjang extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pasien_penunjang_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasien_penunjang_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "rekam_medis_pasien_penunjang_view";
        $comments = "List Pasien pasien_penunjang";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_pasien_penunjang', $this->data);
    }

    public function ajax_list_pasien_penunjang(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $no_masukpenunjang = $this->input->get('no_masukpenunjang',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $pasien_nama = $this->input->get('pasien_nama',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $list = $this->Pasien_penunjang_model->get_pasien_penunjang_list($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasien_penunjang){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasien_penunjang->no_masukpenunjang;
            $row[] = $pasien_penunjang->tgl_masukpenunjang;
            $row[] = $pasien_penunjang->no_rekam_medis;
            $row[] = $pasien_penunjang->no_bpjs;
            $row[] = $pasien_penunjang->pasien_nama;
            $row[] = $pasien_penunjang->jenis_kelamin;
            $row[] = $pasien_penunjang->umur;
            $row[] = $pasien_penunjang->pasien_alamat;
            $row[] = $pasien_penunjang->type_pembayaran;
            $row[] = $pasien_penunjang->kelaspelayanan_nama;
            $row[] = $pasien_penunjang->status_keberadaan;
            $row[] = $pasien_penunjang->statusperiksa;
            if($pasien_penunjang->statusperiksa == "Batal Periksa"){
            $row[] = '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_periksa_penunjang" data-backdrop="static" data-keyboard="false" title="Klik untuk pemeriksaan pasien"   disabled>Periksa</button>';
            // $row[] = '<button type="button" class="btn btn-default btn-circle" title="Klik untuk membatalkan pemeriksaan pasien"  disabled><i class="fa fa-times"></i></button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_penunjang" data-backdrop="static" data-keyboard="false" title="Klik untuk pemeriksaan pasien"  onclick="periksapasien_penunjang('."'".$pasien_penunjang->pendaftaran_id."'".')">Periksa</button>';
                // $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasien_penunjang->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>';
            }//add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_penunjang_model->count_pasien_penunjang_all($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat),
                    "recordsFiltered" => $this->Pasien_penunjang_model->count_pasien_penunjang_all($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function periksa_pasien_penunjang($pendaftaran_id){
        $pasien_penunjang['list_pasien'] = $this->Pasien_penunjang_model->get_pendaftaran_pasien($pendaftaran_id);
        // echo "<pre>";
        // print_r ($pasien_penunjang);die();
        // echo "</pre>";
        $this->load->view('v_periksa_pasien_penunjang', $pasien_penunjang);
    }


    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_penunjang_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            $row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            $row[] = $tindakan->total_harga;
            $row[] = '<button type="button" class="btn btn-circle btn-danger" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->rekap_penunjang_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_tindakan_list(){
        $poliruangan = $this->input->get('poliruangan',TRUE);
        if($poliruangan == "Laboratorium"){
            $list_tindakan = $this->Pasien_penunjang_model->get_tindakan_lab();
        }else {
            $list_tindakan = $this->Pasien_penunjang_model->get_tindakan_rad();
        }
        $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_tarif_tindakan(){
        $daftartindakan_id = $this->input->get('daftartindakan_id',TRUE);
        $tarif_tindakan = $this->Pasien_penunjang_model->get_tarif_tindakan($daftartindakan_id);
        // print_r($tarif_tindakan);die();
        $res = array(
            "list" => $tarif_tindakan,
            "success" => true
        );

        echo json_encode($res);
    }

    public function do_create_tindakan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasien_penunjang_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('tindakan','Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jumlah Tindakan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasien_penunjang_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $tgl_tindakan = $this->input->post('tgl_tindakan', TRUE);
            $tgltindakan_format = date_create($tgl_tindakan);
            $tgltindakan_db = date_format($tgltindakan_format, 'Y-m-d');
            $tindakan = $this->input->post('tindakan', TRUE);
            $jml_tindakan = $this->input->post('jml_tindakan', TRUE);
            $subtotal = $this->input->post('subtotal', TRUE);
            $is_cyto = $this->input->post('is_cyto', TRUE);
            $totalharga = $this->input->post('totalharga', TRUE);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'daftartindakan_id' => $tindakan,
                'jml_tindakan' => $jml_tindakan,
                'total_harga_tindakan' => $subtotal,
                'is_cyto' => $is_cyto == '1' ? '1' : '0',
                'total_harga' => $totalharga,
                'dokter_id' => $dokter,
                'tgl_tindakan' => $tgltindakan_db,
            );

            $ins = $this->Pasien_penunjang_model->insert_tindakanpasien($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rekam_medis_pasien_penunjang_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_pasien_penunjang_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasien_penunjang_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakanpasien_penunjang_id = $this->input->post('tindakanpasien_penunjang_id', TRUE);

        $delete = $this->Pasien_penunjang_model->delete_tindakan($tindakanpasien_penunjang_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasien_penunjang_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakanpasien_penunjang_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasien_penunjang_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakanpasien_penunjang_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function do_delete_pasien_penunjang(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasien_penunjang_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $delete = $this->Pasien_penunjang_model->delete_periksa($pendaftaran_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasien_penunjang_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasien_penunjang_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function autocomplete_obat(){
        $val_obat = $this->input->get('val_obat');
        if(empty($val_obat)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }
        $pasien_list = $this->Pasien_penunjang_model->get_obat_autocomplete($val_obat);
        if(count($pasien_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($pasien_list); $i++){
                $list[] = array(
                    "id"                => $pasien_list[$i]->obat_id,
                    "value"             => $pasien_list[$i]->nama_obat,
                    "deskripsi"         => $pasien_list[$i]->jenis_obat,
                    "satuan"            => $pasien_list[$i]->satuan,
                    "harga_netto"       => $pasien_list[$i]->harga_netto,
                    "harga_jual"        => $pasien_list[$i]->harga_jual,
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Pasien ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
        }
        echo json_encode($res);
    }



}
