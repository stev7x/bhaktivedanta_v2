<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasienrd extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pasienrd_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "rekam_medis_pasienrd_view";
        $comments = "List Pasien Rawat Darurat";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_pasienrd', $this->data);
    }

    public function ajax_list_pasienrd(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $poliklinik = $this->input->get('poliklinik',TRUE);
        $status = $this->input->get('status',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis', TRUE);
        $nama_dokter = $this->input->get('nama_dokter', TRUE);
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);

        $list = $this->Pasienrd_model->get_pasienrd_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $nama_dokter, $pasien_alamat,$no_bpjs,$no_pendaftaran);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $tgl_lahir = $pasienrd->tanggal_lahir;
            $umur  = new Datetime($tgl_lahir);
            $today = new Datetime();
            $diff  = $today->diff($umur);

            $no++;
            $row = array();
            $row[] = $pasienrd->no_pendaftaran;
            $row[] = $pasienrd->no_rekam_medis;
            $row[] = $pasienrd->no_bpjs ;
            $row[] = $pasienrd->pasien_nama;
            $row[] = $pasienrd->tgl_pendaftaran;
            $row[] = $pasienrd->jenis_kelamin;
            // $row[] = $pasienrd->umur;
            $row[] = $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
            $row[] = $pasienrd->pasien_alamat;
            $row[] = $pasienrd->type_pembayaran;
            $row[] = $pasienrd->kelaspelayanan_nama;
            $row[] = $pasienrd->status_periksa;
            $row[] = !empty($pasienrd->carapulang) ? $pasienrd->carapulang : 'Not Set';
            // $row[] = $pasienrd->rs_rujukan;

            if(trim(strtoupper($pasienrd->status_periksa)) == "SUDAH PULANG"){
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk melihat diagnosa" data-toggle="modal" data-target="#modal_diagnosa_sudahpulang" data-backdrop="static" data-keyboard="false"  onclick="periksaPasienRdSudahPulang('."'".$pasienrd->pendaftaran_id."'".')">Diagnosa</button>';
                // $row[] = '<button type="button" class="btn btn-default" title="" disabled>Pulangkan</button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_diagnosa"  data-backdrop="static" data-keyboard="false" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRd('."'".$pasienrd->pendaftaran_id."'".')">Diagnosa</button>';
                // $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_keterangan_keluar" title="Klik untuk set status keluar" onclick="pulangkanPasienRi('."'".$pasienri->pendaftaran_id."'".')">Pulangkan</button>';
            }



            // $row[] = '<button type="button" class="btn btn-success" title="Klik untuk memulangkan pasien" onclick="pulangkanPasienRd('."'".$pasienrd->pendaftaran_id."'".')">Pulangkan</button>';

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasienrd_model->count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $nama_dokter, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "recordsFiltered" => $this->Pasienrd_model->count_all_list_filtered($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $nama_dokter, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }
    public function periksa_pasienrd($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasienrd_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('periksa_pasienrd', $pasienrd);
    }

    public function periksa_pasienrd_sudahpulang($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasienrd_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('periksa_pasienrd_sudahpulang', $pasienrd);
    }

    public function ajax_list_diagnosa_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrd_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->diagnosa_by;
            $row[] = '
                <button type="button" class="btn btn-info btn-circle" title="Klik untuk pilih diagnosa" onclick="editDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-check"></i></button>
                <button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>

                ';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_diagnosa_pasien_sudahpulang(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrd_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->diagnosa_by;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrd_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            $row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            $row[] = $tindakan->total_harga;
            // $row[] = '<button type="button" class="btn-floating yellow waves-effect waves-light darken-4" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanpasien_id."'".')"><i class="mdi-content-clear"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    function get_tindakan_list(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $list_tindakan = $this->Pasienrd_model->get_tindakan($kelaspelayanan_id);
        $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->tariftindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }
    function get_tarif_tindakan(){
        $tariftindakan_id = $this->input->get('tariftindakan_id',TRUE);
        $tarif_tindakan = $this->Pasienrd_model->get_tarif_tindakan($tariftindakan_id);

        $res = array(
            "list" => $tarif_tindakan,
            "success" => true
        );

        echo json_encode($res);
    }

    public function do_create_diagnosa_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_diagnosa','Nama Diagnosa', 'required|trim');
        $this->form_validation->set_rules('kode_diagnosa','Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('jenis_diagnosa','Jenis Diagnosa', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $kode_diagnosa = $this->input->post('kode_diagnosa', TRUE);
            $tgldiagnosa_db = date('Y-m-d');
            $nama_diagnosa = $this->input->post('nama_diagnosa', TRUE);
            $jenis_diagnosa = $this->input->post('jenis_diagnosa', TRUE);
            $data_diagnosapasien = array(
                'nama_diagnosa' => $nama_diagnosa,
                'tgl_diagnosa' => $tgldiagnosa_db,
                'kode_diagnosa' => $kode_diagnosa,
                'jenis_diagnosa' => $jenis_diagnosa,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'dokter_id' => $dokter,
                'diagnosa_by' => 'RM'
            );

            $query = $this->db->get_where('t_diagnosapasien', array('pendaftaran_id'=>$pendaftaran_id,'1'=>$jenis_diagnosa));
            if ($query->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Diagnosa Utama Hanya Bisa Diinput Satu Kali');

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienrd_view";
                    $comments = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $ins = $this->Pasienrd_model->insert_diagnosapasien($pendaftaran_id, $data_diagnosapasien);
                //$ins=true;
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Diagnosa berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienrd_view";
                    $comments = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "rekam_medis_pasienrd_view";
                    $comments = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }

    public function do_update_diagnosa_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_diagnosa','Nama Diagnosa', 'required|trim');
        $this->form_validation->set_rules('kode_diagnosa','Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('jenis_diagnosa','Jenis Diagnosa', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id      = $this->input->post('pasien_id', TRUE);
            $dokter         = $this->input->post('dokter', TRUE);
            $diagnosapasien_id    = $this->input->post('diagnosapasien_id', TRUE);
            $kode_diagnosa  = $this->input->post('kode_diagnosa', TRUE);
            $tgldiagnosa_db = date('Y-m-d');
            $nama_diagnosa  = $this->input->post('nama_diagnosa', TRUE);
            $jenis_diagnosa = $this->input->post('jenis_diagnosa', TRUE);
            $data_diagnosapasien = array(
                'nama_diagnosa'     => $nama_diagnosa,
                'tgl_diagnosa'      => $tgldiagnosa_db,
                'kode_diagnosa'     => $kode_diagnosa,
                'jenis_diagnosa'    => $jenis_diagnosa,
                'pendaftaran_id'    => $pendaftaran_id,
                'pasien_id'         => $pasien_id,
                'dokter_id'         => $dokter,
                'diagnosa_by'       => 'RM'
            );

            $ins = $this->Pasienrd_model->update_diagnosapasien($data_diagnosapasien, $diagnosapasien_id);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Diagnosa berhasil diupdate'
                );

                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_create_tindakan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('tindakan','Nama Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jumlah Tindakan', 'required');
        $this->form_validation->set_rules('tindakan','Tarif Tindakan', 'required');
        $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        $this->form_validation->set_rules('is_cyto','Is Cyto', 'required');
        $this->form_validation->set_rules('dokter','Dokter', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            // $tgl_tindakan = $this->input->post('tgl_tindakan', TRUE);
            // $tgltindakan_format = date_create($tgl_tindakan);
            // $tgltindakan_db = date_format($tgltindakan_format, 'Y-m-d');
            $tgl_tindakan = date_create($this->input->post('tgl_tindakan',TRUE));
            $tgltindakan_db = date_format($tgl_tindakan, 'Y-m-d');
            $tindakan = $this->input->post('tindakan', TRUE);
            $jml_tindakan = $this->input->post('jml_tindakan', TRUE);
            $subtotal = $this->input->post('subtotal', TRUE);
            $is_cyto = $this->input->post('is_cyto', TRUE);
            $totalharga = $this->input->post('totalharga', TRUE);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'tariftindakan_id' => $tindakan,
                'jml_tindakan' => $jml_tindakan,
                'total_harga_tindakan' => $subtotal,
                'is_cyto' => $is_cyto == '1' ? '1' : '0',
                'total_harga' => $totalharga,
                'dokter_id' => $dokter,
                'tgl_tindakan' => $tgltindakan_db,
            );

            $ins = $this->Pasienrd_model->insert_tindakanpasien($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
    public function do_delete_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosapasien_id = $this->input->post('diagnosapasien_id', TRUE);

        $delete = $this->Pasienrd_model->delete_diagnosa($diagnosapasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function do_delete_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakanpasien_id = $this->input->post('tindakanpasien_id', TRUE);

        $delete = $this->Pasienrd_model->delete_tindakan($tindakanpasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_icd9(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakan_icd_id = $this->input->post('tindakan_icd_id', TRUE);

        $delete = $this->Pasienrd_model->delete_icd9($tindakan_icd_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'icd 9 Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id icd 9 Pasien Operasi = '". $tindakan_icd_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal menghapus data icd 9 Pasien dengan ID = '". $tindakan_icd_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_list_resep_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrd_model->get_resep_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $resep){
            $tgl_ = date_create($resep->tgl_reseptur);
            $row = array();
            $row[] = date_format($tgl_, 'd-M-Y');
            $row[] = $resep->nama_obat;
            $row[] = $resep->qty;
            $row[] = $resep->harga_jual;
            $row[] = $resep->satuan;
            $row[] = $resep->signa;
            $row[] = '<button type="button" class="btn-floating yellow waves-effect waves-light darken-4" title="Klik untuk menghapus Resep" onclick="hapusResep('."'".$resep->reseptur_id."'".')"><i class="mdi-content-clear"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    function autocomplete_obat(){
        $val_obat = $this->input->get('val_obat');
        if(empty($val_obat)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }
        $pasien_list = $this->Pasienrd_model->get_obat_autocomplete($val_obat);
        if(count($pasien_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($pasien_list); $i++){
                $list[] = array(
                    "id"                => $pasien_list[$i]->obat_id,
                    "value"             => $pasien_list[$i]->nama_obat,
                    "deskripsi"         => $pasien_list[$i]->jenis_obat,
                    "satuan"            => $pasien_list[$i]->satuan,
                    "harga_netto"       => $pasien_list[$i]->harga_netto,
                    "harga_jual"        => $pasien_list[$i]->harga_jual,
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Pasien ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
        }
        echo json_encode($res);
    }
    public function do_create_resep_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('tgl_resep','Tanggal Resep', 'required');
        $this->form_validation->set_rules('nama_obat','Nama Obat', 'required|trim');
        $this->form_validation->set_rules('qty_obat','Jumlah Obat', 'required');
        $this->form_validation->set_rules('harga_jual','Harga Obat', 'required');
        $this->form_validation->set_rules('satuan_obat','Satuan Obat', 'required');
        $this->form_validation->set_rules('signa_obat','Signa Obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal input resep pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $obat_id = $this->input->post('obat_id', TRUE);
            // $tgl_ = $this->input->post('tgl_resep', TRUE);
            // $tglresep_format = date_create($tgl_);
            // $tglresep_db = date_format($tglresep_format, 'Y-m-d');
            $tgl_ = date_create($this->input->post('tgl_resep',TRUE));
            $tglresep_db = date_format($tgl_, 'Y-m-d');
            $nama_obat = $this->input->post('nama_obat', TRUE);
            $qty_obat = $this->input->post('qty_obat', TRUE);
            $harga_jual = $this->input->post('harga_jual', TRUE);
            $satuan_obat = $this->input->post('satuan_obat', TRUE);
            $signa_obat = $this->input->post('signa_obat', TRUE);
            $data_tindakanpasien = array(
                'obat_id' => $obat_id,
                'qty' => $qty_obat,
                'harga_jual' => $harga_jual,
                'satuan' => $satuan_obat,
                'signa' => $signa_obat,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'dokter_id' => $dokter,
                'tgl_reseptur' => $tglresep_db,
            );

            $ins = $this->Pasienrd_model->insert_reseppasien($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Resep berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Berhasil menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Resep, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Gagal menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
    public function do_delete_resep(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $reseptur_id = $this->input->post('reseptur_id', TRUE);

        $delete = $this->Pasienrd_model->delete_resep($reseptur_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Resep Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Berhasil menghapus Resep Pasien dengan id Resep Pasien = '". $reseptur_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal menghapus data Resep Pasien dengan ID = '". $reseptur_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function do_batal_periksa(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $delete = $this->Pasienrd_model->delete_periksa($pendaftaran_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function get_kelasruangan_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_kelasruangan = $this->Pasienrd_model->get_kelas_poliruangan($poliruangan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        foreach($list_kelasruangan as $list_kelas){
            $list .= "<option value='".$list_kelas->kelaspelayanan_id."'>".$list_kelas->kelaspelayanan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kamarruangan(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $list_kamarruangan = $this->Pasienrd_model->get_kamar_aktif($poliruangan_id, $kelaspelayanan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kamar</option>";
        foreach($list_kamarruangan as $list_kamar){
            $status = $list_kamar->status_bed == '1' ? 'IN USE' : 'OPEN';
            if($list_kamar->status_bed == '0'){
                $list .= "<option value='".$list_kamar->kamarruangan_id."'>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }else{
                $list .= "<option value='".$list_kamar->kamarruangan_id."' disabled>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function do_kirimke_rawatinap(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        $this->form_validation->set_rules('pasien_id','Pasien', 'required');
        $this->form_validation->set_rules('no_pendaftaran','Pendaftaran', 'required');
        $this->form_validation->set_rules('poli_ruangan','Ruangan', 'required');
        $this->form_validation->set_rules('kelas_pelayanan','Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('kamarruangan','Kamar', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal mengirim pasien ke rawat inap dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);

            //start transaction kirim ke RI
            $this->db->trans_begin();

            $dataadmisi['kelaspelayanan_id'] = $kelaspelayanan;
            $dataadmisi['pendaftaran_id'] = $pendaftaran_id;
            $dataadmisi['pasien_id'] = $pasien_id;
            $dataadmisi['no_masukadmisi'] = $no_pendaftaran;
            $dataadmisi['tgl_masukadmisi'] = date('Y-m-d H:i:s');
            $dataadmisi['statusrawat'] = "SEDANG RAWAT INAP";
            $dataadmisi['poli_ruangan_id'] = $poliruangan;
            $dataadmisi['kamarruangan_id'] = $kamarruangan;
            $dataadmisi['create_time'] = date('Y-m-d H:i:s');
            $dataadmisi['create_by'] = $this->data['users']->id;
            $insert_admisi = $this->Pasienrd_model->insert_admisi($dataadmisi);
            if ($insert_admisi){
                $masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $insert_admisi;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasienrd_model->insert_kamar($masukkamar);

                $updatekamar['status_bed'] = '1';
                $this->Pasienrd_model->update_kamar($updatekamar, $kamarruangan);
                $updatependaftaran['status_periksa'] = "SEDANG RAWAT INAP";
                $this->Pasienrd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengirim pasien, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Gagal mengirim pasien dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $this->db->trans_commit();
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Pasien berhasil dikirim ke rawat inap'
                );

                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Pasien berhasil dikirim ke rawat inap dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
    public function set_status_pulang(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id_','Pendaftaran', 'required');
        $this->form_validation->set_rules('status_pulang','Status Pulang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pasienrd_view";
            $comments = "Gagal set status pulang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id_', TRUE);
            $status_pulang = $this->input->post('status_pulang', TRUE);
            $rs_rujukan = $this->input->post('rs_rujukan', TRUE);
            $user_id = $this->data['users']->id;

            $data = array(
                'status_periksa' => 'DIPULANGKAN',
                'carapulang' => $status_pulang,
                'rs_rujukan' => $rs_rujukan,
                'petugas_id' => $user_id
            );
            $update_pendaftaran = $this->Pasienrd_model->update_pendaftaran($data, $pendaftaran_id);

            if($update_pendaftaran){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Berhasil set status pulang'
                );
                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Berhasil set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal set status pulang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rekam_medis_pasienrd_view";
                $comments = "Gagal set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    //pengambilan id diagnosa ditable diagnosa pasien
    public function ajax_get_diagnosa_pasien_by_id(){
        $diagnosapasien_id    = $this->input->get('diagnosapasien_id',TRUE);
        $data_diagnosa_pasien = $this->Pasienrd_model->get_diagnosa_pasien_by_id($diagnosapasien_id);
        if (sizeof($data_diagnosa_pasien)>0){
            $diagnosa_pasien  = $data_diagnosa_pasien[0];
            $res = array(
                "success"  => true,
                "messages" => "Data diagnosa pasien ditemukan",
                "data"     => $diagnosa_pasien
            );
        } else {
            $res = array(
                "success"  => false,
                "messages" => "Data diagnosa pasien tidak ditemukan",
                "data"     => null
            );
        }
        echo json_encode($res);
    }

    //list pemeriksaan pasien diagnosa icd 10
    public function ajax_list_diagnosa(){
        $list = $this->Pasienrd_model->get_list_diagnosa();

        $data = array();
        foreach($list as $diagnosa){
            $row = array();

            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->kelompokdiagnosa_nama;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Diagnosa" onclick="pilihDiagnosa('."'".$diagnosa->diagnosa_id."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Pasienrd_model->count_list_diagnosa(),
            "recordsFiltered" => $this->Pasienrd_model->count_list_filtered_diagnosa(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //get diagnosa icd 10
        public function ajax_get_diagnosa_by_id(){
        $diagnosa_id = $this->input->get('diagnosa_id',TRUE);
        $data_diagnosa = $this->Pasienrd_model->get_diagnosa_by_id($diagnosa_id);
        if (sizeof($data_diagnosa)>0){
            $diagnosa = $data_diagnosa[0];
            $res = array(
                "success" => true,
                "messages" => "Data diagnosa ditemukan",
                "data" => $diagnosa
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data diagnosa tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }


    //insert icd 9 ke table

    public function do_create_icd_9_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_icd_9_cm','Kode ICD', 'required|trim');
        $this->form_validation->set_rules('nama_icd_9_cm','Nama ICD', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error  = validation_errors();
            $res    = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => $error
                );

            // if permitted, do logit
            $perms      = "rekam_medis_pasienrd_view";
            $comments   = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id      = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id           = $this->input->post('pasien_id', TRUE);
            $kode_icd_9_cm       = $this->input->post('kode_icd_9_cm', TRUE);
            $nama_icd_9_cm       = $this->input->post('nama_icd_9_cm', TRUE);
            $data_pendaftaran    = $this->Pasienrd_model->get_pendaftaran_pasien($pendaftaran_id);
            $data_tindakanpasien = array(
                'pendaftaran_id'    => $pendaftaran_id,
                'pasien_id'         => $pasien_id,
                'kode_icd_9_cm'     => $kode_icd_9_cm,
                'nama_tindakan'     => $nama_icd_9_cm,
                'tanggal_tindakan'  => date('Y-m-d')
            );

            $ins = $this->Pasienrd_model->insert_icd9($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms      = "rekam_medis_pasienrd_view";
                $comments   = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms      = "rekam_medis_pasienrd_view";
                $comments   = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_update_icd_9_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_icd_9_cm','Kode ICD', 'required|trim');
        $this->form_validation->set_rules('nama_icd_9_cm','Nama ICD', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error  = validation_errors();
            $res    = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => $error
                );

            // if permitted, do logit
            $perms      = "rekam_medis_pasienrd_view";
            $comments   = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id      = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id           = $this->input->post('pasien_id', TRUE);
            $tindakan_icd_id     = $this->input->post('tindakan_icd_id', TRUE);
            $kode_icd_9_cm       = $this->input->post('kode_icd_9_cm', TRUE);
            $nama_icd_9_cm       = $this->input->post('nama_icd_9_cm', TRUE);
            $data_pendaftaran    = $this->Pasienrd_model->get_pendaftaran_pasien($pendaftaran_id);
            $data_tindakanpasien = array(
                'pendaftaran_id'    => $pendaftaran_id,
                'pasien_id'         => $pasien_id,
                'kode_icd_9_cm'     => $kode_icd_9_cm,
                'nama_tindakan'     => $nama_icd_9_cm,
                'tanggal_tindakan'  => date('Y-m-d')
            );

            $ins = $this->Pasienrd_model->update_icd9($data_tindakanpasien, $tindakan_icd_id);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Tindakan berhasil diupdate'
                );

                // if permitted, do logit
                $perms      = "rekam_medis_pasienrd_view";
                $comments   = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms      = "rekam_medis_pasienrd_view";
                $comments   = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    //list icd 9 pemeriksaan pasien rd
    public function ajax_list_icd9(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrd_model->get_icd9_list($pendaftaran_id);
        $data = array();
        foreach($list as $icd9){
            $tanggal_tindakan = date_create($icd9->tanggal_tindakan);
            $row = array();
            $row[] = date_format($tanggal_tindakan, 'd-M-Y');
            $row[] = $icd9->kode_icd_9_cm;
            $row[] = $icd9->nama_tindakan;
            $row[] = '
                <button type="button" class="btn btn-info btn-circle" title="Klik untuk pilih diagnosa" onclick="editIcd9('."'".$icd9->tindakan_icd_id."'".')"><i class="fa fa-check"></i></button>
                <button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusIcd9('."'".$icd9->tindakan_icd_id."'".')"><i class="fa fa-times"></i></button>

                ';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw"              => $this->input->get('draw'),
                    "recordsTotal"      => count($list),
                    "recordsFiltered"   => count($list),
                    "data"              => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    // get buat edit icd 9
    public function ajax_get_tindakan_icd_9_by_id(){ //function get id ketika pilih tindakan icd 9 untuk function edit
        $tindakan_icd_id    = $this->input->get('tindakan_icd_id',TRUE);
        $data_tindakan_icd9 = $this->Pasienrd_model->get_tindakan_icd_9_by_id($tindakan_icd_id);
        if (sizeof($data_tindakan_icd9)>0){
            $tindakan_icd_9 = $data_tindakan_icd9[0];
            $res            = array(
                                "success"   => true,
                                "messages"  => "Data icd 9 ditemukan",
                                "data"      => $tindakan_icd_9
                            );
        }else{
            $res = array(
                "success"   => false,
                "messages"  => "Data icd 9 tidak ditemukan",
                "data"      => null
            );
        }
        echo json_encode($res);
    }


    //list icd 9
    public function ajax_list_daftar_icd_9(){
        // $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasienrd_model->get_list_icd_9();
        $data = array();
        foreach($list as $daftar_icd_9){
            $row    = array();
            $row[]  = $daftar_icd_9->kode_diagnosa;
            $row[]  = $daftar_icd_9->nama_diagnosa;
            $row[]  = '
                <button type="button" class="btn btn-info btn-circle" title="Klik untuk pilih diagnosa" onclick="pilihIcd9('."'".$daftar_icd_9->diagnosa_id."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw"              => $this->input->get('draw'),
            "recordsTotal"      => $this->Pasienrd_model->count_list_icd_9(),
            "recordsFiltered"   => $this->Pasienrd_model->count_list_filtered_icd_9(),
            "data"              => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //get id buat pilih icd 9
    public function ajax_get_icd9_by_id(){
        $diagnosa_id         = $this->input->get('diagnosa_id',TRUE);
        $data_diagnosa  = $this->Pasienrd_model->get_icd9_by_id($diagnosa_id);
        if (sizeof($data_diagnosa)>0){
            $diagnosa   = $data_diagnosa[0];
            $res        = array(
                        "success"   => true,
                        "messages"  => "Data icd 9 ditemukan",
                        "data"      => $diagnosa
                    );
        } else {
            $res = array(
                "success"   => false,
                "messages"  => "Data icd 9 tidak ditemukan",
                "data"      => null
            );
        }
        echo json_encode($res);
    }
}
