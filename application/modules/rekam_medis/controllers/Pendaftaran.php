<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pendaftaran_model');
        $this->load->model('Reservasi_model');
        $this->load->model('rekam_medis/list_pasien_model');
        $this->load->model('Models');

        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['list_jenis_kelamin']   = array('Laki-laki'=>'Male','Perempuan'=>'Female');
        $this->data['list_status_kawin']    = array('1'=>'Belum Kawin','2'=>'Kawin','3'=>'Duda/Janda','4'=>'Di Bawah Umur');
        $this->data['list_warga_negara']    = array('1'=>'Indonesia','2'=>'Asing');
        
        $this->data['list_golongan_darah']  = array('a'=>'A','b'=>'B','ab'=>'AB','o'=>'O');
        $this->data['list_type_identitas']  = array('1'=>'KTP','2'=>'SIM','3'=>'Kartu Pelajar','4'=>'KTA','5'=>'KITAS','6'=>'Passpor','7'=>'Lainnya');
        $this->data['list_pekerjaan']  = array('1'=>'Swasta','2'=>'Wiraswasta','3'=>'IRT','4'=>'PNS','5'=>'TNI/Polri','6'=>'Lainnya');
        $this->data['list_hubungan']  = array('1'=>'Orang Tua','2'=>'Suami','3'=>'Istri','4'=>'Lainnya');
        $this->data['list_type_bayar']  = array('1'=>'Pribadi');
        $this->data['next_no_rm'] = $this->Reservasi_model->get_no_rm();
        $this->data['data_user'] = $this->list_pasien_model->get_data_user($this->data['users']->id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;

        // die("aya");
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "rekam_medis_pendaftaran_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('rekam_medis/v_form_pendaftaran', $this->data);
    }

    public function get_retensi_pendaftaran() {
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "rekam_medis_pendaftaran_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('rekam_medis/v_retensi_pendaftaran', $this->data);
    }

    public function do_create_pendaftaran(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $status_pasien = $this->input->post('status_pasien',TRUE);
        if($status_pasien > 0){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => "Pasien masih menjalani perawatan"
                );
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        // $this->form_validation->set_rules('type_id', 'Type Identitas', 'required|trim');
        // $this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|trim');
        $this->form_validation->set_rules('nama_pasien', 'Nama Pasien', 'required|trim');
        // $this->form_validation->set_rules('nama_panggilan', 'Nama panggilan', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');
        // $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|trim');
//        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
//        $this->form_validation->set_rules('umur', 'Umur', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat Pasien', 'required|trim');
        $this->form_validation->set_rules('warga_negara', 'Warga Negara', 'required|trim');
        // $this->form_validation->set_rules('alamat_domisili', 'Alamat Domisili', 'required|trim');
        $this->form_validation->set_rules('propinsi', 'Propinsi Pasien', 'required|trim');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten Pasien', 'required|trim');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan Pasien', 'required|trim');
        $this->form_validation->set_rules('kelurahan', 'Kelurahan Pasien', 'required|trim');
        
        // $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required|trim');
        // $this->form_validation->set_rules('no_mobile', 'No.Ponsel/Mobile', 'required|trim');
        // $this->form_validation->set_rules('nama_orangtua', 'Nama Orang Tua', 'required|trim');
        $this->form_validation->set_rules('instalasi', 'Instalasi Tujuan', 'required|trim');
//        $this->form_validation->set_rules('poli_ruangan', 'Poli/Ruangan', 'required|trim');
        $this->form_validation->set_rules('kelas_pelayanan', 'Kelas Pelayanan', 'required|trim');
        $this->form_validation->set_rules('dokter', 'Dokter', 'required|trim');
//        $this->form_validation->set_rules('pj_nama', 'Penanggung Jawab', 'required|trim');
//        $this->form_validation->set_rules('byr_type_bayar', 'Cara Bayar', 'required|trim');
        //new

        $this->form_validation->set_rules('jenis_pasien', 'Jenis Pasien', 'required|trim');
        $this->form_validation->set_rules('type_call', 'Tipe Layanan', 'required|trim');
//        $this->form_validation->set_rules('weight', 'Berat', 'required|trim');
//        $this->form_validation->set_rules('present_complain', 'Present Complain', 'required|trim');

//        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $instalasi = $this->input->post('instalasi',TRUE);
        $cek_alamat = $this->input->post('cek_alamat',TRUE);
        $cek_tinggal = $this->input->post('cek_tinggal',TRUE);
        
        if($instalasi == '3'){
            $this->form_validation->set_rules('kamarruangan', 'Kamar Ruangan', 'required');
            $this->form_validation->set_rules('dokter_anastesi', 'Dokter Anastesi', 'required');
            $this->form_validation->set_rules('perawat', 'Perawat', 'required');
        }
        //yang baru hotel
        if ($this->input->post('jenis_pasien',TRUE) != 1) {
            if ($this->input->post('cek_hotel',TRUE)) {
                $this->form_validation->set_rules('hotel_nama', 'Insert Nama Hotel', 'required|trim');
            }
            else{
                $this->form_validation->set_rules('hotel', 'Hotel', 'required|trim');
            }
        }

        if ($this->input->post('jenis_pasien',TRUE) == 3) {
            $this->form_validation->set_rules('email', 'Email', 'required|trim');
        }

        //sampe sini

        //sudah ga digunakan tapi dibiarin
        if($cek_alamat){
            if ($cek_tinggal == 'hotel') {
                // asal
                // if ($this->input->post('cek_hotel',TRUE)) {
                //     $this->form_validation->set_rules('hotel_nama', 'Insert Nama Hotel', 'required|trim');
                // }
                // else{
                //     $this->form_validation->set_rules('hotel', 'Hotel', 'required|trim');
                // }
                
            }
            else{
                $this->form_validation->set_rules('provinsi_tinggal', 'Propinsi Pasien Tinggal', 'required|trim');
                $this->form_validation->set_rules('kabupaten_tinggal', 'Kabupaten Pasien Tinggal', 'required|trim');
                $this->form_validation->set_rules('kecamatan_tinggal', 'Kecamatan Pasien Tinggal', 'required|trim');
                $this->form_validation->set_rules('kelurahan_tinggal', 'Kelurahan Pasien Tinggal', 'required|trim');
            }
            
        }
        //sampe sini
        
        if ($this->form_validation->run() == FALSE)  {

            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pendaftaran_create";
            $comments = "Gagal melakukan pendaftaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
            echo json_encode($res);
            exit;
        }else if ($this->form_validation->run() == TRUE)  {
            $pasien_id = $this->input->post('pasien_id',TRUE);
            // $tgl_lahir1 = date_create($this->input->post('tgl_lahir',TRUE));
            $tgl_lahir1 = $this->input->post('tgl_lahir',TRUE);
            $tgl_lahir2 = $this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir1));
            // var_dump($tgl_lahir2);die;
            $tgl_lhr_suami = $this->input->post('tgl_suami_istri',TRUE);
            if ($tgl_lhr_suami) {

                $tgl_sustri1 = $this->input->post('tgl_suami_istri',TRUE);

                if (!empty($tgl_sustri1)) {
                    $tgl_sustri2 = $this->Reservasi_model->casting_date_indo(strtolower($tgl_sustri1));

                }else {
                    $tgl_sustri2 = "0000-00-00";
                }


                // $tgl_sustri2 = date_format($tgl_sustri1, 'Y-m-d');

            }else{
                $tgl_sustri2 = "0000-00-00";
            }
            $tgl_ortu1  = $this->input->post('tgl_lahir_ortu',TRUE);
            if (!empty($tgl_ortu1)) {
                $tgl_ortu2 = $this->Reservasi_model->casting_date_indo(strtolower($tgl_ortu1));
            }else {
                $tgl_ortu2 = "0000-00-00";
            }
            // $tgl_ortu2  = date_format($tgl_ortu1,'Y-m-d');
            // $datapasien['no_rekam_medis'] = $this->Pendaftaran_model->get_no_rm();
            
            $datapasien['type_identitas'] = $this->input->post('type_id',TRUE);
            $datapasien['no_identitas'] = $this->input->post('no_identitas',TRUE);
            $datapasien['no_bpjs'] = $this->input->post('no_bpjs',TRUE);
            $datapasien['pasien_nama'] = $this->input->post('nama_pasien',TRUE);
            $datapasien['nama_panggilan'] = $this->input->post('nama_panggilan',TRUE);
            $datapasien['jenis_kelamin'] = $this->input->post('jenis_kelamin',TRUE);
            $datapasien['tempat_lahir'] = $this->input->post('tempat_lahir',TRUE);
            $datapasien['tanggal_lahir'] = $tgl_lahir2;
            $datapasien['umur'] = $this->input->post('umur',TRUE);
            $datapasien['pasien_alamat'] = $this->input->post('alamat',TRUE);
            $datapasien['pasien_alamat_domisili'] = $this->input->post('alamat_domisili',TRUE);
            $datapasien['propinsi_id'] = $this->input->post('propinsi',TRUE);
            $datapasien['kabupaten_id'] = $this->input->post('kabupaten',TRUE);
            $datapasien['kecamatan_id'] = $this->input->post('kecamatan',TRUE);
            $datapasien['kelurahan_id'] = $this->input->post('kelurahan',TRUE);
            $datapasien["cek_insurance"] = $this->input->post('name_insurance', TRUE);
            //start transaction pendaftaran
            $this->db->trans_begin();

            if ($this->input->post('cek_hotel',TRUE)) {
                        $data_hotel['nama_hotel'] = $this->input->post('hotel_nama',TRUE);
                        $data_hotel['alamat'] = $this->input->post('alamat_hotel',TRUE);
                        $data_hotel['no_telp'] = $this->input->post('tlp_hotel',TRUE);
                        $hotel_id = $this->Pendaftaran_model->insert_hotel($data_hotel);
                        // var_dump($hotel_id);die;
                        $datapasien['id_hotel'] = $hotel_id;
                        
                    }
                    else{
                        $datapasien['id_hotel'] = $this->input->post('hotel',TRUE);
                    }
            //sudah ga digunakan tapi dibiarin
            if($cek_alamat){
                if ($cek_tinggal == 'hotel') {
                    //asal
                    // if ($this->input->post('cek_hotel',TRUE)) {
                    //     $data_hotel['nama_hotel'] = $this->input->post('hotel_nama',TRUE);
                    //     $hotel_id = $this->Pendaftaran_model->insert_hotel($data_hotel);
                    //     $datapasien['id_hotel'] = $hotel_id;
                        
                    // }
                    // else{
                    //     $datapasien['id_hotel'] = $this->input->post('hotel',TRUE);
                    // }
                    
                }
                else{
                    $datapasien['provinsi_tinggal_id'] = $this->input->post('provinsi_tinggal',TRUE);
                    $datapasien['kabupaten_tinggal_id'] = (int)$this->input->post('kabupaten_tinggal',TRUE);
                    $datapasien['kecamatan_tinggal_id'] = (int)$this->input->post('kecamatan_tinggal',TRUE);
                    $datapasien['kelurahan_tinggal_id'] = (int)$this->input->post('kelurahan_tinggal',TRUE);
                }
                
            }
            //sampe sini
            //masuk else terus
            else{
                $datapasien['provinsi_tinggal_id'] = $this->input->post('propinsi',TRUE);
                $datapasien['kabupaten_tinggal_id'] = (int)$this->input->post('kabupaten',TRUE);
                $datapasien['kecamatan_tinggal_id'] = (int)$this->input->post('kecamatan',TRUE);
                $datapasien['kelurahan_tinggal_id'] = (int)$this->input->post('kelurahan',TRUE);
            }
            //ok
            
            $datapasien['status_kawin'] = $this->input->post('status_kawin',TRUE);
            $datapasien['nama_suami_istri'] = $this->input->post('nama_suami_istri',TRUE);
            $datapasien['tgl_lahir_suami_istri'] = $tgl_sustri2;
            $datapasien['umur_suami_istri'] = $this->input->post('umur_suami_istri',TRUE);
            $datapasien['tlp_rumah'] = $this->input->post('no_tlp_rmh',TRUE);
            $datapasien['tlp_selular'] = $this->input->post('no_mobile',TRUE);
            $datapasien['pekerjaan_id'] = (int)$this->input->post('pekerjaan',TRUE);
            $datapasien['warga_negara'] = (int)$this->input->post('warga_negara',TRUE);
            $datapasien['agama_id'] = (int)$this->input->post('agama',TRUE);
            $datapasien['bahasa_id'] = (int)$this->input->post('bahasa',TRUE);
            $datapasien['pendidikan_id'] = (int)$this->input->post('pendidikan',TRUE);
            $datapasien['nama_orangtua'] = $this->input->post('nama_orangtua',TRUE);
            $datapasien['tgl_lahir_ortu'] = $tgl_ortu2;
            $datapasien['umur_ortu'] = $this->input->post('umur_ortu',TRUE);
            $datapasien['info_medis'] = $this->input->post('info_medis',TRUE);
            $datapasien['create_date'] = date('Y-m-d H:i:s');
            $datapasien['kasus_polisi'] = $this->input->post('cakep',TRUE);
            $datapasien['berat_bayi'] = $this->input->post('berat_bayi',TRUE);
            
            //data tambahan
            $datapasien['room_number'] = $this->input->post('room_number',TRUE);
            // $datapasien['insurance_company'] = $this->input->post('insurance_company',TRUE);
            // $datapasien['travel_insurance'] = $this->input->post('travel_insurance',TRUE);
            // die($this->input->post('present_complain',TRUE));
            $datapasien['present_complain'] = $this->input->post('present_complain',TRUE); 
            if ($this->input->post('date_first_complain',TRUE)) {
                $datapasien['date_first_complain'] = $this->Reservasi_model->casting_date_indo(strtolower($this->input->post('date_first_complain',TRUE)));
            }
            if ($this->input->post('date_first_consultation',TRUE)) {
                $datapasien['date_first_consultation'] = $this->Reservasi_model->casting_date_indo(strtolower($this->input->post('date_first_consultation',TRUE)));
            }
            
            
            $datapasien['allergy_history'] = $this->input->post('allergy_history',TRUE);
            $datapasien['pregnant'] = (int)$this->input->post('pregnant',TRUE);
            // die($this->input->post('pregnant',TRUE));
            $datapasien['current_medication'] = $this->input->post('current_medication',TRUE);
            $datapasien['weight'] = $this->input->post('weight',TRUE);
            $datapasien['medical_history'] = $this->input->post('medical_history',TRUE);
            $datapasien['medical_event'] = $this->input->post('medical_event',TRUE);
            $datapasien['email'] = $this->input->post('email',TRUE);
            $datapasien['jenis_pasien'] = $this->input->post('jenis_pasien',TRUE);
            $datapasien['type_call'] = $this->input->post('type_call',TRUE);

            //sampe sini tambahan

            $datapendaftaran['tgl_pendaftaran'] = date('Y-m-d H:i:s');
            $datapendaftaran['status_periksa'] = "ANTRIAN";
            $datapendaftaran['status_pasien'] = 1;
            $datapendaftaran['pasien_id'] = $pasien_id;
            $datapendaftaran['instalasi_id'] = $this->input->post('instalasi',TRUE);
            $datapendaftaran['poliruangan_id'] = $this->input->post('poli_ruangan',TRUE);
            $datapendaftaran['dokter_id'] = $this->input->post('dokter',TRUE);
            // $datapendaftaran['jam_periksa'] = $this->input->post('jam_periksa',TRUE);
            $datapendaftaran['petugas_id'] = $this->data['users']->id;

            
            if($datapendaftaran['instalasi_id'] == '1'){
//                 $datapendaftaran['no_pendaftaran'] = $this->Pendaftaran_model->get_no_pendaftaran_rj();
                $datapendaftaran['no_pendaftaran'] = $this->input->post('no_pendaftaran',TRUE);

                $datares['status_verif'] = "1";
                $datares['status'] = "HADIR";
                $datares['tgl_verif'] = date('Y-m-d H:i:s');
                $datares['room_number'] = $this->input->post('room_number',TRUE);

                $reservasi_id =  $this->input->post('reservasi_id',TRUE);

                $update_res = $this->Reservasi_model->update_reservasi($datares, $reservasi_id);
               
            }else if($datapendaftaran['instalasi_id'] == '2'){
                $datapendaftaran['no_pendaftaran'] = $this->Reservasi_model->get_no_pendaftaran_reg();
                $data_reg['no_reg'] = $this->Reservasi_model->get_no_pendaftaran_reg();
                $this->Reservasi_model->insert_reg($data_reg);
                print_r("2"); die();

            }else if($datapendaftaran['instalasi_id'] == '3'){
                $datapendaftaran['no_pendaftaran'] = $this->Reservasi_model->get_no_pendaftaran_reg();
                $datapendaftaran['dokteranastesi_id'] = $this->input->post('dokter_anastesi',TRUE);
                $datapendaftaran['perawat_id'] = $this->input->post('perawat',TRUE);
                $data_reg['no_reg'] = $this->Reservasi_model->get_no_pendaftaran_reg();
                $this->Reservasi_model->insert_reg($data_reg);
                print_r("3"); die();
            }else{
                if($datapendaftaran['poliruangan_id'] == '6'){
                    $datapendaftaran['no_pendaftaran'] = $this->Reservasi_model->get_no_pendaftaran_reg();
                }else if($datapendaftaran['poliruangan_id'] == '7'){
                    $datapendaftaran['no_pendaftaran'] = $this->Reservasi_model->get_no_pendaftaran_reg();
                }
                $data_reg['no_reg'] = $this->Reservasi_model->get_no_pendaftaran_reg();
                $this->Reservasi_model->insert_reg($data_reg);
                print_r("6"); die();
            }

            $poliruangan = $this->Pendaftaran_model->get_poliklinik($datapendaftaran['poliruangan_id']);

            // CREATE NO ANTRIAN
            if($datapendaftaran['poliruangan_id'] == $poliruangan[0]->poliruangan_id){
                $datapendaftaran['no_antrian'] = $this->Pendaftaran_model->get_no_antrian($poliruangan[0]->nama_singkatan);
            }


            // else if($datapendaftaran['poliruangan_id'] == '5'){
            //     $datapendaftaran['no_antrian'] == $this->Pendaftaran_model->get_no_antrian_umum();
            // }

            $datapendaftaran['kelaspelayanan_id'] = $this->input->post('kelas_pelayanan',TRUE);
            $datapendaftaran['nama_perujuk'] = $this->input->post('rm_namaperujuk',TRUE);
            $datapendaftaran['alamat_perujuk'] = $this->input->post('rm_alamatperujuk',TRUE);

            
            // $insert_pendaftaran = $this->Pendaftaran_model->insert_pendaftaran($datapasien);


            //insert penanggungjawab
            $data_pj['nama_pj']             = $this->input->post('pj_nama',TRUE);
            $data_pj['no_identitas_pj']     = $this->input->post('no_identitas_pj',TRUE);
            $data_pj['alamat_pj']           = $this->input->post('pj_alamat',TRUE);
            $data_pj['propinsi_id']         = $this->input->post('pj_propinsi',TRUE);
            $data_pj['kabupaten_id']        = $this->input->post('pj_kabupaten',TRUE);
            $data_pj['kecamatan_id']        = $this->input->post('pj_kecamatan',TRUE);
            $data_pj['kelurahan_id']        = $this->input->post('pj_kelurahan',TRUE);
            $data_pj['no_telepon']          = $this->input->post('pj_no_tlp_rmh',TRUE);
            $data_pj['no_ponsel']           = $this->input->post('pj_no_mobile',TRUE);
            $data_pj['status_hubungan_pj']  = $this->input->post('hubunganpj',TRUE);
            $data_pj['pendidikan_pj_id']    = $this->input->post('pendidikan_pj',TRUE);
            $data_pj['pekerjaan_pj_id']    = (int)$this->input->post('pekerjaan_pj',TRUE);
            $insert_pj                      = $this->Pendaftaran_model->insert_penanggungjawab($data_pj);
            $datapendaftaran['penanggungjawab_id'] = $insert_pj;

            //insert Metode Pembayaran
            // $data_byr['type_pembayaran']    = $this->input->post('byr_type_bayar',TRUE);
            // $data_byr['instansi_pekerjaan'] = $this->input->post('byr_instansi',TRUE);
            $pembayaran_id = $this->input->post("pembayaran_id", TRUE);
            $data_byr['type_pembayaran']    = $this->input->post('byr_type_bayar',TRUE);
            $data_byr['nama_asuransi']      = $this->input->post('asuransi1',TRUE);
            $data_byr['no_asuransi']        = $this->input->post('byr_nomorasuransi1',TRUE);
            $data_byr['nama_asuransi2']      = $this->input->post('asuransi2',TRUE);
            $data_byr['no_asuransi2']        = $this->input->post('byr_nomorasuransi2',TRUE);

            if (empty($pembayaran_id))
                $insert_byr = $this->Pendaftaran_model->insert_pembayaran($data_byr);
            else
                $insert_byr = $pembayaran_id;
            // var_dump($insert_byr);die;
            $datapendaftaran['pembayaran_id'] = $insert_byr;

            $get_last_no_rm = "";
            
            if(!empty($pasien_id)){
                $datapasien['no_rekam_medis'] = $this->input->post('no_rm',TRUE);
                 $check_total_dokter = $this->Pendaftaran_model->check_total_dokter($datapendaftaran['dokter_id']);
                    // if( $check_total_dokter[0]->jml_dokter < 15) {
                        // $get_last_no_rm = $this->input->post('no_rm',TRUE);
                        $where = "pasien_id";
                        $update_pasien = $this->Pendaftaran_model->update_pasien($datapasien, $where, $pasien_id);
                        if($update_pasien){
                            $datapendaftaran['status_kunjungan'] = "PASIEN LAMA";
                            $insert_pendaftaran = $this->Pendaftaran_model->insert_pendaftaran($datapendaftaran);
                            $message['success'] = "true";
                        }
                    // }else{
                        // $insert_pendaftaran = "DOKTER_PENUH";
                        // $message['success'] = "false";
                    // }
                    // echo json_encode("lama");
            }else{
                
                $datapasien['no_rekam_medis'] = $this->input->post('no_rm',TRUE);
                $is_no_rm = $this->input->post('is_no_rm', TRUE);
                $is_retension = $this->input->post('is_retension', TRUE);
                $no_rekam_med = $this->input->post('no_rm',TRUE);
                $status_action = "";
                

                if($is_no_rm == 1){
                    $where = "no_rekam_medis";
                    $update_pasien = $this->Pendaftaran_model->update_pasien($datapasien, $where, $no_rekam_med);
                    $status_action = "update";
                    // $message['success'] = "true";

                }else{
                    if ($is_retension != 0 || $is_retension != "0") {
                        $datapasien_retension['no_rekam_medis'] = 0;
                        $datapasien_retension['is_retention'] = 1;
                        $datapasien_retension['no_rekam_medis_lama'] = $no_rekam_med;
                        $this->Pendaftaran_model->update_pasien_retension($datapasien_retension, $no_rekam_med);
                        die('asup kadieu');
                    }
                    $insert_pasien = $this->Pendaftaran_model->insert_pasien($datapasien);
                    $status_action = "insert";
                    // echo json_encode("aya mang");
                }
                if($status_action == "insert"){
                    // $get_last_no_rm = $this->Pendaftaran_model->get_no_rm();
                    // $check_total_dokter = $this->Pendaftaran_model->check_total_dokter($datapendaftaran['dokter_id']);

                    // if($check_total_dokter[0]->jml_dokter < 15){
                        $datapendaftaran['pasien_id'] = $insert_pasien;
                        // echo json_encode("ieu mang");
            
                    // }else{
                        // $message['success'] = "false";
                    // }
                }
                $datapendaftaran['status_kunjungan'] = "PASIEN BARU";
                $insert_pendaftaran = $this->Pendaftaran_model->insert_pendaftaran($datapendaftaran);
                $message['success'] = "true";
                
            }
            $get_last_no_rm = $this->input->post('no_rm',TRUE);



            // print_r($insert_pendaftaran);die();

            if(!empty($insert_pendaftaran)){
                $last_pendaftaran = $this->Pendaftaran_model->get_last_pendaftaran($insert_pendaftaran);
                if($datapendaftaran['instalasi_id'] == '4'){
                    $id_poli                = $this->input->post('poli_ruangan',TRUE); //ambil id poli
                    $id_kelaspelayanan      = $this->input->post('kelas_pelayanan',TRUE); //ambil id kelas pelayanan
                    $poli                   = $this->Pendaftaran_model->get_kelas_poli($id_poli,$id_kelaspelayanan); // ambil id_poli dan kelaspelayanan di m_kelaspelayanan
                    $kelas_poli             = $this->Pendaftaran_model->get_kelas_pelayanan_poli($poli->kelaspelayanan_id); // ambil id kelaspelayanan
                    $poliruangan            = $this->Pendaftaran_model->get_tarif_adm($id_poli);
                    $dokter_poliruangan     = $this->Pendaftaran_model->get_tarif_dokter($id_poli);
                    $tariftindakan          = $this->Pendaftaran_model->get_tarif_tindakan($poliruangan->tariftindakan_admin);
                    $tariftindakan_dokterr  = $this->Pendaftaran_model->get_tarif_tindakan($dokter_poliruangan->tariftindakan_dokter);

                    $datapenunjang['kelaspelayanan_id']  = $last_pendaftaran->kelaspelayanan_id;
                    $datapenunjang['pendaftaran_id']     = $last_pendaftaran->pendaftaran_id;
                    $datapenunjang['pasien_id']          = $last_pendaftaran->pasien_id;
                    $datapenunjang['no_masukpenunjang']  = $last_pendaftaran->no_pendaftaran;
                    $datapenunjang['tgl_masukpenunjang'] = date('Y-m-d H:i:s');
                    $datapenunjang['statusperiksa']      = "ANTRIAN";
                    if($last_pendaftaran->poliruangan_id == 6){
                        $statuspenunjang = "RADIOLOGI";
                    }else {
                        $statuspenunjang = "LABORATORIUM";
                    }
                    $datapenunjang['poli_ruangan_id']    = $last_pendaftaran->poliruangan_id;
                    $datapenunjang['status_keberadaan']  = $statuspenunjang;
                    $datapenunjang['dokterpengirim_id']  = $this->input->post('dokter',TRUE);
                    $datapenunjang['create_time']        = date('Y-m-d H:i:s');
                    $datapenunjang['create_by']          = $this->data['users']->id;
                    $insert_penunjang = $this->Pendaftaran_model->insert_penunjang($datapenunjang);
                }

                if($datapendaftaran['instalasi_id'] == '3'){
                    $dataadmisi['kelaspelayanan_id']    = $last_pendaftaran->kelaspelayanan_id;
                    $dataadmisi['pendaftaran_id']       = $last_pendaftaran->pendaftaran_id;
                    $dataadmisi['pasien_id']            = $last_pendaftaran->pasien_id;
                    $dataadmisi['no_masukadmisi']       = $last_pendaftaran->no_pendaftaran;
                    $dataadmisi['tgl_masukadmisi']      = date('Y-m-d H:i:s');
                    $dataadmisi['statusrawat']          = "SEDANG RAWAT INAP";
                    $dataadmisi['poli_ruangan_id']      = $last_pendaftaran->poliruangan_id;
                    $dataadmisi['kamarruangan_id']      = $this->input->post('kamarruangan',TRUE);
                    $dataadmisi['create_time']          = date('Y-m-d H:i:s');
                    $dataadmisi['dokter_id']            = $last_pendaftaran->dokter_id;
                    $dataadmisi['dokteranastesi_id']    = $this->input->post('dokter_anastesi',TRUE);
                    $dataadmisi['dokterperawat_id']     = $this->input->post('perawat',TRUE);
                    $dataadmisi['create_by']            = $this->data['users']->id;
                    $insert_admisi = $this->Pendaftaran_model->insert_admisi($dataadmisi);
                    if ($insert_admisi){
                        $masukkamar['poliruangan_id'] =  $last_pendaftaran->poliruangan_id;
                        $masukkamar['kelaspelayanan_id'] =  $last_pendaftaran->kelaspelayanan_id;
                        $masukkamar['kamarruangan_id'] =  $this->input->post('kamarruangan',TRUE);
                        $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                        $masukkamar['pasienadmisi_id'] =  $insert_admisi;
                        $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                        $masukkamar['create_user'] =  $this->data['users']->id;
                        $this->Pendaftaran_model->insert_kamar($masukkamar);

                        $updatekamar['status_bed'] = '1';
                        $this->Pendaftaran_model->update_kamar($updatekamar, $dataadmisi['kamarruangan_id']);
                        $updatependaftaran['status_periksa'] = "SEDANG RAWAT INAP";
                        $this->Pendaftaran_model->update_pendaftaran($updatependaftaran, $insert_pendaftaran);
                    }
                }

                // $tariftindakan = $this->Pendaftaran_model->get_tarif_tindakan('2');

                $reservasiId = $this->input->post('reservasi_id', TRUE);
                if ($reservasiId != 0) {
                    $date_reservasi['asuransi1'] = $this->input->post('asuransi1', TRUE);
                    $data_reservasi['asuransi2'] = $this->input->post('asuransi1', TRUE);
                    $data_reservasi['pic_nama'] = $this->input->post('pj_nama', TRUE);
                    $data_reservasi['room_number'] = $this->input->post('room_number',TRUE);

                    $this->Reservasi_model->update_reservasi($date_reservasi, $reservasiId);
                }

                $id_poli                = $this->input->post('poli_ruangan',TRUE); //ambil id poli
                $id_kelaspelayanan      = $this->input->post('kelas_pelayanan',TRUE); //ambil id kelas pelayanan
                $poli                   = $this->Pendaftaran_model->get_kelas_poli($id_poli,$id_kelaspelayanan); // ambil id_poli dan kelaspelayanan di m_kelaspelayanan
                $kelas_poli             = $this->Pendaftaran_model->get_kelas_pelayanan_poli($poli->kelaspelayanan_id); // ambil id kelaspelayanan
                $poliruangan            = $this->Pendaftaran_model->get_tarif_adm($id_poli);
                $dokter_poliruangan     = $this->Pendaftaran_model->get_tarif_dokter($id_poli);
                $tariftindakan          = $this->Pendaftaran_model->get_tarif_tindakan($poliruangan->tariftindakan_admin);
                $tariftindakan_dokterr  = $this->Pendaftaran_model->get_tarif_tindakan($dokter_poliruangan->tariftindakan_dokter);

                    // print_r($tariftindakan);
                    // print_r($kelas_poli);
                    // die();


                $data_tindakanpasien = array(
                    'pendaftaran_id'        => $insert_pendaftaran,
                    'pasien_id'             => $last_pendaftaran->pasien_id,
                    'daftartindakan_id'     => $tariftindakan->daftartindakan_id,
                    'jml_tindakan'          => '1',
                    'total_harga_tindakan'  => $tariftindakan->harga_tindakan,
                    'is_cyto'               => '0',
                    'total_harga'           => $tariftindakan->harga_tindakan,
                    'dokter_id'             => null,
                    'tgl_tindakan'          => date('Y-m-d'),
                    'ruangan_id'            => $last_pendaftaran->poliruangan_id,
                    'petugas_id'            => $this->data['users']->id,
                    'komponentarif_id'      => '4',
                    'kelaspelayanan_id'     => $kelas_poli->kelaspelayanan_id,
                    'harga_pelayanan'       => $kelas_poli->tarif

                );
                // $this->Pendaftaran_model->insert_tindakanpasien($data_tindakanpasien);

                // $data_tindakanpasien_2 = array(
                //     'pendaftaran_id'        => $insert_pendaftaran,
                //     'pasien_id'             => $last_pendaftaran->pasien_id,
                //     'daftartindakan_id'     => $tariftindakan_dokterr->daftartindakan_id,
                //     'jml_tindakan'          => '1',
                //     'total_harga_tindakan'  => $tariftindakan_dokterr->harga_tindakan,
                //     'is_cyto'               => '0',
                //     'total_harga'           => $tariftindakan_dokterr->harga_tindakan,
                //     'dokter_id'             => null,
                //     'tgl_tindakan'          => date('Y-m-d'),
                //     'ruangan_id'            => $last_pendaftaran->poliruangan_id,
                //     'petugas_id'            => $this->data['users']->id,
                //     'komponentarif_id'      => '8'

                // );

                // $this->Pendaftaran_model->insert_tindakanpasien($data_tindakanpasien_2);

                // var_dump($data_tindakanpasien);
                // var_dump($data_tindakanpasien_2);

                // print_r($data_tindakanpasien);
                // print_r($data_tindakanpasien_2);
                // die();



            }

            if($message['success'] =="true"){

                    $receiver = array(
                    '0' => 'Kasir',
                    '1' => 'Rawat Jalan',
                    '2' => 'Rekam Medis'
                );

                for ($i=0; $i <= 2 ; $i++) {
                    // insert notification
                        $notif =array(
                            'title'         => $this->data['data_user']->name ,
                            'description'   => 'Pasien baru telah terdaftar',
                            'sender'        => $this->data['data_user']->name,
                            'receiver'      => $receiver[$i],
                            'date_notif'    =>  date('Y-m-d H:i:s'),
                            'view'          => 0
                        );
                        $this->Pendaftaran_model->insert_notif($notif);
                }
                // $this->Pendaftaran_model->insert_notif($notif);


                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'pendaftaran_id' => $insert_pendaftaran,
                    'no_rm' => $get_last_no_rm,
                    'no_antri' =>  $datapendaftaran['no_antrian'],
                    'messages' => 'Register has been saved to database'
                );

                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Success to Create a new patient register with post data";
                $this->aauth->logit($perms, current_url(), $comments);
            }else if($message['success'] =="false"){
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'display' => true,
                'messages' => 'Kuota pasien dokter ini sudah penuh.'
                );

                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Success to Create a new patient register with post data";
                $this->aauth->logit($perms, current_url(), $comments);

            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Failed insert user group to database, please contact web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Failed to Create a new patient register when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            // $data_rm['no_rekam_medis'] = $this->Pendaftaran_model->get_no_rm();
            $data_rm['no_rekam_medis'] = $get_last_no_rm;
            $this->Pendaftaran_model->insert_rm($data_rm);

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            // var_dump($this->db->trans_status()); die;

            if ($this->db->trans_status() === FALSE){
                $error = $this->db->error();
                // print_r($error);die;
                $this->db->trans_rollback();
                // $error = ['messages' => 'error found'];
                echo json_encode($error);
            }else{
                $this->db->trans_commit();
                echo json_encode($res);
            }
        }
        
    }

    function autocomplete_pasien(){
        $val_pasien = $this->input->get('val_pasien');
        if(empty($val_pasien)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }
        $pasien_list = $this->Pendaftaran_model->get_pasien_autocomplete($val_pasien);
        if(count($pasien_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($pasien_list); $i++){
                $status_pasien = $this->Pendaftaran_model->get_status_pendaftaran($pasien_list[$i]->pasien_id);
                $tgl_lhr = date_create($pasien_list[$i]->tanggal_lahir);
                $tgl_lahir = date_format($tgl_lhr, 'd F Y');
                $list[] = array(
                    "id"                    => $pasien_list[$i]->pasien_id,
                    "value"                 => $pasien_list[$i]->no_rekam_medis." - ".$pasien_list[$i]->pasien_nama,
                    "pasien_nama"           => $pasien_list[$i]->pasien_nama,
                    "nama_panggilan"        => $pasien_list[$i]->nama_panggilan,
                    "no_rekam_medis"        => $pasien_list[$i]->no_rekam_medis,
                    "no_bpjs"               => $pasien_list[$i]->no_bpjs,
                    "type_id"               => $pasien_list[$i]->type_identitas,
                    "no_identitas"          => $pasien_list[$i]->no_identitas,
                    "tempat_lahir"          => $pasien_list[$i]->tempat_lahir,
                    "tanggal_lahir"         => $tgl_lahir,
                    "umur"                  => $pasien_list[$i]->umur,
                    "jenis_kelamin"         => $pasien_list[$i]->jenis_kelamin,
                    "status_kawin"          => $pasien_list[$i]->status_kawin,
                    "tlp_rumah"             => $pasien_list[$i]->tlp_rumah,
                    "tlp_selular"           => $pasien_list[$i]->tlp_selular,
                    "warga_negara"          => $pasien_list[$i]->warga_negara,
                    "alamat"                => $pasien_list[$i]->pasien_alamat,
                    "propinsi"              => $pasien_list[$i]->propinsi_id,
                    "kabupaten"             => $pasien_list[$i]->kabupaten_id,
                    "kecamatan"             => $pasien_list[$i]->kecamatan_id,
                    "kelurahan"             => $pasien_list[$i]->kelurahan_id,
                    "pekerjaan"             => $pasien_list[$i]->pekerjaan_id,
                    "agama"                 => $pasien_list[$i]->agama_id,
                    "bahasa"                => $pasien_list[$i]->bahasa_id,
                    "pendidikan"            => $pasien_list[$i]->pendidikan_id,
                    "nama_suami_istri"      => $pasien_list[$i]->nama_suami_istri,
                    "tgl_lahir_suami_istri" => $pasien_list[$i]->tgl_lahir_suami_istri,
                    "umur_suami_istri"      => $pasien_list[$i]->umur_suami_istri,
                    "nama_orangtua"         => $pasien_list[$i]->nama_orangtua,
                    "tgl_lahir_ortu"        => $pasien_list[$i]->tgl_lahir_ortu,
                    "umur_ortu"             => $pasien_list[$i]->umur_ortu,
                    "info_medis"            => $pasien_list[$i]->info_medis,

                    // "nama_keluarga"          => $pasien_list[$i]->nama_keluarga,
                    // "status_hubungan"          => $pasien_list[$i]->status_hubungan,
                    // "alergi"          => $pasien_list[$i]->alergi,
                    "status"                => $status_pasien,
                    // "status"          => $pasien_list[$i]->s,
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Pasien ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
        }
        echo json_encode($res);
    }

    function get_kabupaten_list(){
        $propinsi_id = $this->input->get('propinsi_id',TRUE);
        $list_kabupaten = $this->Pendaftaran_model->get_kabupaten($propinsi_id);
        $list = "<option value=\"\" disabled selected>Pilih Kabupaten</option>";
        foreach($list_kabupaten as $list_kab){
            $list .= "<option value='".$list_kab->kabupaten_id."'>".$list_kab->kabupaten_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kecamatan_list(){
        $kabupaten_id = $this->input->get('kabupaten_id',TRUE);
        $list_kecamatan = $this->Pendaftaran_model->get_kecamatan($kabupaten_id);

        $list = "<option value=\"\" disabled selected>Pilih Kecamatan</option>";
        foreach($list_kecamatan as $list_kec){
            $list .= "<option value='".$list_kec->kecamatan_id."'>".$list_kec->kecamatan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kelurahan_list(){
        $kecamatan_id = $this->input->get('kecamatan_id',TRUE);
        $list_kelurahan = $this->Pendaftaran_model->get_kelurahan($kecamatan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelurahan</option>";
        foreach($list_kelurahan as $list_kel){
            $list .= "<option value='".$list_kel->kelurahan_id."'>".$list_kel->kelurahan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function validasi_ktp(){
        $data_ktp = $this->input->get('ktp',true);
        $check_data = $this->Pendaftaran_model->check_ktp($data_ktp);
        if(!empty($check_data)){
            $data = $check_data[0]->no_identitas;
        }

        if(!empty($data) > 0 ){
            $res = array(
                'message' => 'NO.IDENTITAS SUDAH DIGUNAKAN',
                'display' => true,
            );
        }else {
            $res = array(
                'message' => 'NO.IDENTITAS DAPAT DIGUNAKAN',
                'display' => false,
            );
        }

        echo json_encode($res);
    }

    function validasi_no_rm(){
        $data_no_rm = $this->input->get('no_rm',true);
        $check_data = $this->Pendaftaran_model->check_no_rm($data_no_rm);

        if($check_data == 0 ){
            $res = array(
                'message' => 'NO RM DAPAT DIGUNAKAN',
                'display' => true,
            );
        }else {
            $res = array(
                'message' => 'NO RM SUDAH DIGUNAKAN',
                'display' => false,
            );
        }

        echo json_encode($res);
    }

    function validasi_bpjs(){
        $data_bpjs = $this->input->get('bpjs',true);
        $check_data = $this->Pendaftaran_model->check_bpjs($data_bpjs);
        if(!empty($check_data)){
            $data = $check_data[0]->no_bpjs;
        }

        if(!empty($data) > 0 ){
            $res = array(
                'message' => 'NO.BPJS SUDAH DIGUNAKAN',
                'display' => true,
            );
        }else {
            $res = array(
                'message' => 'NO.BPJS DAPAT DIGUNAKAN',
                'display' => false,
            );
        }

        echo json_encode($res);
    }

    function get_ringkasan_masuk(){
        $instalasi_id = $this->input->get('instalasi',true);
        $poliruangan_id = $this->input->get('poli_ruangan',true);
        $dokter_id = $this->input->get('dokter_id',true);
        $tgl = $this->input->get('tgl_reservasi',TRUE);
        // $tgl_reservasi = date('l', strtotime($tgl));

        date_default_timezone_set('Asia/Jakarta');
        setlocale(LC_ALL, 'IND');
        setlocale(LC_ALL, 'id_ID');

        $tgl_reservasi = strftime( "%A", strtotime($tgl));

        $data_ruangan = $this->Pendaftaran_model->get_ruangan($instalasi_id);
        $list_ruangan = '';
        foreach ($data_ruangan as $list_ruang) {
           $list_ruangan .= "<option value='".$list_ruang->poliruangan_id."'>".$list_ruang->nama_poliruangan."</option>";
        }


        $data_kelaspoliruangan = $this->Pendaftaran_model->get_kelas_poliruangan($poliruangan_id);
        $list_kelaspelayanan = '';
        foreach ($data_kelaspoliruangan as $list_kelas) {
           $list_kelaspelayanan .= "<option value='".$list_kelas->kelaspelayanan_id."'>".$list_kelas->kelaspelayanan_nama."</option>";
        }


        $data_dokter = $this->Pendaftaran_model->get_dokter($poliruangan_id,$instalasi_id);
        $list_dokter = '';
        foreach ($data_dokter as $list_dktr) {
            $list_dokter .= "<option value='".$list_dktr->id_M_DOKTER."'>".$list_dktr->NAME_DOKTER."</option>";
        }

        $jam_dokter = $this->Pendaftaran_model->get_jam_dokter($dokter_id,$tgl_reservasi);
        $list_jamDokter = '';
        foreach($jam_dokter as $list_dktr){
            $list_jamDokter .= "<option value='".$list_dktr->jam_mulai."'>".$list_dktr->jam_mulai."</option>";
        }


        $res = array(
            'ruangan'=> $list_ruangan,
            'kelas_pelayanan'=> $list_kelaspelayanan,
            'dokter'=> $list_dokter,
            'jam'=> $list_jamDokter
        );
        echo json_encode($res);


    }


    function get_daerah_list(){
        $propinsi_id = $this->input->get('propinsi_id',TRUE);
        $kabupaten_id = $this->input->get('kabupaten_id',TRUE);
        $kecamatan_id = $this->input->get('kecamatan_id',TRUE);
        $kelurahan_id = $this->input->get('kelurahan_id',TRUE);

        $kelurahan = $this->Pendaftaran_model->get_kelurahan($kecamatan_id);
        $kecamatan = $this->Pendaftaran_model->get_kecamatan($kabupaten_id);
        $kabupaten = $this->Pendaftaran_model->get_kabupaten($propinsi_id);

        $list_kabupaten = "";
        foreach($kabupaten as $list_kab){
            if($kabupaten_id == $list_kab->kabupaten_id){
                $list_kabupaten .= "<option value='".$list_kab->kabupaten_id."' selected>".$list_kab->kabupaten_nama."</option>";
            }else{
                $list_kabupaten .= "<option value='".$list_kab->kabupaten_id."'>".$list_kab->kabupaten_nama."</option>";
            }

        }

        $list_kecamatan = "";
        foreach($kecamatan as $list_kec){
            if($kecamatan_id == $list_kec->kecamatan_id){
                $list_kecamatan .= "<option value='".$list_kec->kecamatan_id."' selected>".$list_kec->kecamatan_nama."</option>";
            }else{
                $list_kecamatan .= "<option value='".$list_kec->kecamatan_id."'>".$list_kec->kecamatan_nama."</option>";
            }

        }

        $list_kelurahan = "";
        foreach($kelurahan as $list_kel){
            if($kelurahan_id == $list_kel->kelurahan_id){
                $list_kelurahan .= "<option value='".$list_kel->kelurahan_id."' selected>".$list_kel->kelurahan_nama."</option>";
            }else{
                $list_kelurahan .= "<option value='".$list_kel->kelurahan_id."'>".$list_kel->kelurahan_nama."</option>";
            }
        }

        $res = array(
            "list_kabupaten" => $list_kabupaten,
            "list_kecamatan" => $list_kecamatan,
            "list_kelurahan" => $list_kelurahan,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_ruangan_list(){
        $instalasi_id = $this->input->get('instalasi_id',TRUE);
        $list_ruangan = $this->Pendaftaran_model->get_ruangan($instalasi_id);
//        if(count($list_ruangan) > 1){
//            $list = "<option value=\"\" disabled selected>Pilih Poli/Ruangan</option>";
//        }else{
//            $list = "";
//        }
        $list = "<option value=\"\" disabled>Pilih Poli/Ruangan</option>";
        if ($instalasi_id == 4) {
            foreach($list_ruangan as $list_ruang){

                     $list .= "<option value='".$list_ruang->poliruangan_id."'>".$list_ruang->nama_poliruangan."</option>";
                     break;
            }
        }
        else {
            foreach($list_ruangan as $list_ruang){
                    $list .= "<option value='".$list_ruang->poliruangan_id."' selected>".$list_ruang->nama_poliruangan."</option>";
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kelasruangan_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_kelasruangan = $this->Pendaftaran_model->get_kelas_poliruangan($poliruangan_id);
       if(count($list_kelasruangan) > 1){
           $list = "<option value=\"\" disabled>Pilih Kelas Pelayanan</option>";
       }else{
           $list = "";
       }
        // $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        foreach($list_kelasruangan as $list_kelas){
            $list .= "<option value='".$list_kelas->kelaspelayanan_id."' selected>".$list_kelas->kelaspelayanan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_dokter_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $instalasi_id = $this->input->get('instalasi_id',TRUE);
        $list_dokter = $this->Pendaftaran_model->get_dokter($poliruangan_id,$instalasi_id);
        if(count($list_dokter) > 1){
            $list = "<option value=\"\" disabled selected>Pilih Dokter</option>";
        }else{
            $list = "";
        }
        foreach($list_dokter as $list_dktr){
            $list .= "<option value='".$list_dktr->id_M_DOKTER."'>".$list_dktr->NAME_DOKTER."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kamarruangan(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $list_kamarruangan = $this->Pendaftaran_model->get_kamar_aktif($poliruangan_id, $kelaspelayanan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kamar</option>";
//        $list = "";
        foreach($list_kamarruangan as $list_kamar){
            $status = $list_kamar->status_bed == '1' ? 'IN USE' : 'OPEN';
            if($list_kamar->status_bed == '0'){
                $list .= "<option value='".$list_kamar->kamarruangan_id."'>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }else{
                $list .= "<option value='".$list_kamar->kamarruangan_id."' disabled>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    public function hitung_umur(){
        $tgl_lahir = $this->input->get('tgl_lahir',TRUE);
        $tgl_lahir2 = date_create($tgl_lahir);
        $tgl_lahir3 = date_create($this->Reservasi_model->casting_date_indo(strtolower($tgl_lahir)));
//         print_r($tgl_lahir3); die();
        $today = date("Y-m-d");
        $date1 = $tgl_lahir3;
        $date2 = new DateTime($today);
        $interval = $date1->diff($date2);
        $umur = str_pad($interval->y, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($interval->m, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($interval->d, 2, '0', STR_PAD_LEFT).' Hr';

        $res = array(
            "umur" => $umur,
            "success" => true
        );

        echo json_encode($res);
    }

    public function ajax_list_pasien(){
        $list = $this->Pendaftaran_model->get_pasien_list();
        $data = array();
        $no = 1;
        foreach($list as $pasien){
            $row = array();

            $row[] = $no++;
            $row[] = $pasien->no_rekam_medis;
            $row[] = $pasien->no_bpjs;
            $row[] = $pasien->pasien_nama;
            $row[] = $pasien->nama_panggilan;
            // $row[] = $pasien->jenis_kelamin;
            $row[] = $pasien->tanggal_lahir;
            $row[] = $pasien->pasien_alamat ."Kel.".$pasien->kelurahan_nama ." Kec.".$pasien->kecamatan_nama ." , ".$pasien->kabupaten_nama;
            $row[] = $pasien->nama_orangtua;
            // $row[] = $pasien->tempat_lahir;
            // $row[] = $pasien->umur;
            // $row[] = $pasien->status_kawin;
            // $row[] = $pasien->tlp_selular;
            // $row[] = $pasien->agama_nama;
            // $row[] = $pasien->pendidikan_nama;
            // $row[] = $pasien->nama_pekerjaan;
            // $row[] = $pasien->nama_suami_istri;
            //add html for action
            $row[] = '<button class="btn btn-info btn-circle" onclick="pilihPasien('."'".$pasien->pasien_id."'".')"><i class="fa fa-check"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pendaftaran_model->count_pasien_all(),
                    "recordsFiltered" => $this->Pendaftaran_model->count_pasien_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_ortu(){
        $list = $this->Pendaftaran_model->get_pasien_list();
        $data = array();
        $no = 1;
        foreach($list as $pasien){
            $row = array();

            $row[] = $no++;
            $row[] = $pasien->no_rekam_medis;
            $row[] = $pasien->pasien_nama;
            $row[] = $pasien->jenis_kelamin;
            $row[] = $pasien->tanggal_lahir;
            $row[] = $pasien->pasien_alamat;
            //add html for action
            $row[] = '<button class="btn btn-info btn-circle" onclick="pilihOrtu('."'".$pasien->pasien_id."'".')"><i class="fa fa-check"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pendaftaran_model->count_pasien_all(),
                    "recordsFiltered" => $this->Pendaftaran_model->count_pasien_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_pasien_lama() {
        $list = $this->Pendaftaran_model->get_pasien_list();
        $data = array();
        $no = 1;
        foreach($list as $pasien){
            $row = array();

            $row[] = $no++;
            $row[] = $pasien->no_rekam_medis;
            $row[] = $pasien->pasien_nama;
            $row[] = $pasien->jenis_kelamin;
            $row[] = $pasien->tanggal_lahir;
            $row[] = $pasien->pasien_alamat;
            //add html for action
            $row[] = '<button class="btn btn-info btn-circle" data-toggle="modal" data-target="#modal_retensi_pasien" data-backdrop="static" data-keyboard="false"  onclick="pilihPasienLama('."'".$pasien->pasien_id."'".')"><i class="fa fa-check"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pendaftaran_model->count_pasien_all(),
                    "recordsFiltered" => $this->Pendaftaran_model->count_pasien_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_perujuk(){
        $list = $this->Pendaftaran_model->get_pasien_perujuk();
        $data = array();
        $no = 1;
        foreach($list as $data_perujuk){
            $row = array();

            $row[] = $no++;
            $row[] = $data_perujuk->kode_perujuk;
            $row[] = $data_perujuk->nama_perujuk;
            $row[] = $data_perujuk->alamat_kantor;
            $row[] = $data_perujuk->telp_hp;
            $row[] = $data_perujuk->nama_marketing;
            //add html for action
            $row[] = '<button class="btn btn-info btn-circle" onclick="pilihRujuk('."'".$data_perujuk->id_perujuk."'".')"><i class="fa fa-check"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pendaftaran_model->count_perujuk_all(),
                    "recordsFiltered" => $this->Pendaftaran_model->count_perujuk_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_skor_kspr(){
        $pasien_kspr_id = $this->input->get('pasien_kspr_id',TRUE);
        $list = $this->Pendaftaran_model->get_skor_kspr_list($pasien_kspr_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kspr){
            $no++;
            $row = array();
            $row[] = $no;
            // $row[] = date('d M Y', strtotime($pem_fisik->tgl_periksa));
            $row[] = $kspr->kspr_nama;
            $row[] = $kspr->skor_kspr;
            $row[] = $kspr->jml;
            $row[] = $kspr->total;
            //add html for action
            // $row[] = '
            //           <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus PemFisik" id="hapusPemFisik" onclick="hapusKSPR('."'".$kspr->id_skor_kspr."'".')"><i class="fa fa-times"></i></button>
            //           ';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pendaftaran_model->count_skor_kspr_all(),
                    "recordsFiltered" => $this->Pendaftaran_model->count_skor_kspr_all(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_list_faktor_resiko(){
        $pasien_kspr_id = $this->input->get('pasien_kspr_id',TRUE);
        $list = $this->Pendaftaran_model->get_faktor_resiko($pasien_kspr_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kspr){
            $no++;
            $row = array();
            $row[] = $kspr->faktor;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_perujuk_by_id(){
        $id_perujuk = $this->input->get('id_perujuk',TRUE);
        $old_data = $this->Pendaftaran_model->get_perujuk_by_id($id_perujuk);

        if(count($old_data) > 0) {
            $status_pasien = $this->Pendaftaran_model->get_status_pendaftaran($old_data->id_perujuk);

            $res = array(
                'success' => true,
                'data' => $old_data,
                'messages' => "Data found",
                'status_pasien' => $status_pasien
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Perujuk tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function ajax_get_pasien_by_id(){
        $id_m_pasien = $this->input->get('id_m_pasien',TRUE);
        $old_data = $this->Pendaftaran_model->get_pasien_by_id($id_m_pasien);
        $data_reservasi = $this->Pendaftaran_model->get_reservasi_by_no_rm($old_data->pasien_id);
        // print_r($old_data);die();

        if(count($old_data) > 0) {
            $status_pasien = $this->Pendaftaran_model->get_status_pendaftaran($old_data->pasien_id);
            // print_r($status_pasien);die();
            date_default_timezone_set('Asia/Jakarta');
            setlocale(LC_ALL, 'IND');
            setlocale(LC_ALL, 'id_ID');
            $tgl_lhr = $old_data->tanggal_lahir;
            $tgl_lahir = ($tgl_lhr == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr));

            $tgl_lhr_ortu =  $old_data->tgl_lahir_ortu;
            $tgl_lahir_ortu = ($tgl_lhr_ortu == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_ortu));

            $tgl_lhr_suami = $old_data->tgl_lahir_suami_istri;
            $tgl_lahir_suami = ($tgl_lhr_suami == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_suami));

            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data,
                'data_reservasi' => $data_reservasi,
                'tgl_lahir' => $tgl_lahir,
                'tgl_lahir_ortu' =>$tgl_lahir_ortu,
                'tgl_lahir_suami' =>$tgl_lahir_suami,
                'status_pasien' => $status_pasien
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Pasien tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function print_data($pendaftaran_id,$view){

        $datapasien = $this->Pendaftaran_model->get_data_print($pendaftaran_id);
        $print['data_pasien'] = $datapasien;
        $print['data_reservasi'] = $this->Pendaftaran_model->get_reservasi_by_no_rm($datapasien->pasien_id);

        $print['barcode'] = "<img alt='barcode' width='500' height='500' src='".base_url()."/rawatjalan/pasien_rj/create_barcode/".$datapasien->no_rekam_medis."'>";

        $this->load->view('rekam_medis/'.$view,$print);
    }

    public function create_barcode($no_rm){
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');

        $barcodeOptions = array(
            'text' => $no_rm,
            'barHeight'=> 74,
            'factor'=>3.98,
        );
        //generate barcode
        Zend_Barcode::render('code128', 'image', $barcodeOptions, array());

    }

    public function print_pendaftaran($pendaftaran_id){
        $print['pendaftaran_id'] = $pendaftaran_id;
        $print['data_pasien'] = $this->Pendaftaran_model->get_data_print($pendaftaran_id);
        $print['hotel'] = $this->Pendaftaran_model->get_hotel_by_id($print['data_pasien']->id_hotel);
        $print['list_jenis_kelamin']   = array('Laki-laki'=>'Male','Perempuan'=>'Female');
        $print['list_status_kawin']    = array('1'=>'Belum Kawin','2'=>'Kawin','3'=>'Duda/Janda','4'=>'Di Bawah Umur');
        $print['list_warga_negara']    = array('1'=>'Indonesia','2'=>'Asing');
        $print['list_golongan_darah']  = array('a'=>'A','b'=>'B','ab'=>'AB','o'=>'O');
        $print['list_type_identitas']  = array('1'=>'KTP','2'=>'SIM','3'=>'Kartu Pelajar','4'=>'KTA','5'=>'KITAS','6'=>'Passpor','7'=>'Lainnya');
        $print['list_pekerjaan']  = array('1'=>'Swasta','2'=>'Wiraswasta','3'=>'IRT','4'=>'PNS','5'=>'TNI/Polri','6'=>'Lainnya');
        $print['list_hubungan']  = array('1'=>'Orang Tua','2'=>'Suami','3'=>'Istri','4'=>'Lainnya');
        $print['list_type_bayar']  = array('1'=>'Pribadi');
    
        $list_instalasi = $this->Pendaftaran_model->get_warganegara();
        foreach($list_instalasi as $list){
            if ($list->id == $print["data_pasien"]->warga_negara)
                $print["data_pasien"]->warga_negara = $list->nama_negara;
        }
        $this->load->view('rekam_medis/print_data_pasien',$print);
    }

    public function print_kartu($pendaftaran_id){
        $print['pendaftaran_id'] = $pendaftaran_id;
        $print['data_pasien'] = $this->Pendaftaran_model->get_data_print($pendaftaran_id);

        $this->load->view('rekam_medis/print_kartu',$print);
    }

    public function print_resiko($pendaftaran_id){
        $print['pendaftaran_id'] = $pendaftaran_id;
        $print['data_pasien']    = $this->Pendaftaran_model->get_data_print($pendaftaran_id);
        $this->load->view('rekam_medis/print_resiko', $print);
    }
    function get_jam_dokter(){
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $tgl = $this->input->get('tgl_reservasi',TRUE);
        // $tgl_reservasi = date('l');

        date_default_timezone_set('Asia/Jakarta');
        setlocale(LC_ALL, 'IND');
        setlocale(LC_ALL, 'id_ID');

        $tgl_reservasi = strftime( "%A", strtotime($tgl));

        $list_dokter = $this->Pendaftaran_model->get_jam_dokter($dokter_id,$tgl_reservasi);
        if(count($list_dokter) > 1){
            $list = "<option value=\"\" disabled selected>Pilih Jam</option>";
        }else{
            $list = "<option value=\"\" disabled selected>Pilih Jam</option>";
        }
        foreach($list_dokter as $list_dktr){

            $list .= "<option value='".$list_dktr->jam_mulai."'>".$list_dktr->jam_mulai."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }
    function get_hotel_list(){
        $list_hotel = $this->Pendaftaran_model->get_hotel();
        if(count($list_hotel) > 1){
            $list = "<option value=\"\" disabled selected>Choose Hotel</option>";
        }else{
            $list = "<option value=\"\" disabled selected>Choose Hotel</option>";
        }
        foreach($list_hotel as $list_dktr){
            $list .= "<option value='".$list_dktr->id_hotel."'>".$list_dktr->nama_hotel."</option>";
        }
        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }
    function get_asuransi_list(){
        $type = $this->input->get('type',TRUE);
        
        if ($type == 'company') {
            $type = null;
        }
        // var_dump($type);
        // die;
        $list_asuransi = $this->Reservasi_model->get_asuransi($type);

        if(count($list_asuransi) > 1){
            $list = "<option value=\"\" disabled selected>Choose Asuransi</option>";
        }else{
            $list = "<option value=\"\" disabled selected>Choose Asuransi</option>";
        }
        foreach($list_asuransi as $lists){
            $list .= "<option value='".$lists->nama."'>".$lists->nama."</option>";
        }
        // var_dump($list);die;
        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    public function migrasi() {
        $reservasi = $this->Pendaftaran_model->reservasi();

        foreach ($reservasi as $key => $value) {
            @$pasien_id = $this->Pendaftaran_model->pasien($value->nama_pasien)->pasien_id;
            $this->Pendaftaran_model->update_reservasi($pasien_id, $value->reservasi_id);
        }
    }
}
