<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_penunjang_model extends CI_Model {
    var $column = array('no_masukpenunjang','tgl_masukpenunjang','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','kelaspelayanan_nama','statusperiksa');
    var $order = array('tgl_masukpenunjang' => 'DSC');

    public function __construct(){
        parent::__construct();
    }

    public function get_pasien_penunjang_list($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat){
        $this->db->from('v_rekap_penunjang');
         $this->db->where('tgl_masukpenunjang BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        
        if(!empty($no_masukpenunjang)){    
            $this->db->like('no_masukpenunjang', $no_masukpenunjang);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_nama)){
            $this->db->like('pasien_nama', $pasien_nama);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }

    // public function get_pasien_penunjang_list(){
    //     $this->db->from('v_pasien_rad');
    //     $i = 0;
    //     $search_value = $this->input->get('search');
    //     if($search_value){
    //         foreach ($this->column as $item){
    //             ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
    //             $i++;
    //         }
    //     }
    //     $order_column = $this->input->get('order');
    //     if($order_column !== false){
    //         $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
    //     }
    //     else if(isset($this->order)){
    //         $order = $this->order;
    //         $this->db->order_by(key($order), $order[key($order)]);
    //     }

    //     $length = $this->input->get('length');
    //     if($length !== false){
    //         if($length != -1) {
    //             $this->db->limit($this->input->get('length'), $this->input->get('start'));
    //         }
    //     }

    //     $query = $this->db->get();

    //     return $query->result();
    // }

    public function count_pasien_penunjang_all($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat){   
        $this->db->from('v_rekap_penunjang');
        $this->db->where('tgl_masukpenunjang BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
         
        if(!empty($no_masukpenunjang)){
            $this->db->like('no_masukpenunjang', $no_masukpenunjang);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_nama)){
            $this->db->like('pasien_nama', $pasien_nama);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }

        return $this->db->count_all_results();
    }

    // public function count_pasien_penunjang_filtered(){
    //     $this->db->from('v_pasien_rad');
    //     $i = 0;
    //     $search_value = $this->input->get('search');
    //     if($search_value){
    //         foreach ($this->column as $item){
    //             ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
    //             $i++;
    //         }
    //     }
    //     $order_column = $this->input->get('order');
    //     if($order_column !== false){
    //         $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
    //     }
    //     else if(isset($this->order)){
    //         $order = $this->order;
    //         $this->db->order_by(key($order), $order[key($order)]);
    //     }
    //     $query = $this->db->get();
    //     return $query->num_rows();
    // }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_rekap_penunjang', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
 
        return $query->row();  
    }

    public function get_dokter_list(){
        $this->db->from('m_dokter');
        $query = $this->db->get();

        return $query->result();
    }

    public function update_to_sudahperiksa($id){
        $data = array(
            'status_periksa' => 'SUDAH PERIKSA'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));
        $datas = array(
            'statusperiksa' => 'SUDAH PERIKSA'
        );
        $update = $this->db->update('t_pasienmasukpenunjang', $datas, array('pendaftaran_id' => $id));
        return $update;
    }
 
    public function get_tindakan_pasien_list($pendaftaran_id){
        $this->db->select('t_rekap_penunjang.*, m_tindakan.*');
        $this->db->from('t_rekap_penunjang');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_rekap_penunjang.daftartindakan_id');
        $this->db->where('t_rekap_penunjang.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');       
        if($length !== false){    
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }
 
    public function get_tindakan_rad(){
        $this->db->from('m_tindakan');   
        $this->db->where('m_tindakan.kelompoktindakan_id','6');   
        $query = $this->db->get();   

        return $query->result(); 
    }

    public function get_tindakan_lab(){ 
        $this->db->from('m_tindakan');   
        $this->db->where('m_tindakan.kelompoktindakan_id','5');   
        $query = $this->db->get();     

        return $query->result(); 
    }    

    public function get_tarif_tindakan($daftartindakan_id){
        $query = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $daftartindakan_id));
            
        return $query->row();       
    } 

    public function insert_tindakanpasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_rekap_penunjang',$data);   

        return $insert;
    }
    public function delete_periksa($pendaftaran_id){
      $datas = array(
          'statusperiksa' => 'Batal Periksa'
      );
      $update = $this->db->update('t_pasienmasukpenunjang', $datas, array('pendaftaran_id' => $pendaftaran_id));
      return $update;
    }

    public function delete_tindakan($id){
        $delete = $this->db->delete('t_rekap_penunjang', array('rekap_penunjang_id' => $id));

        return $delete;
    }     
    
    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_obat');
        $this->db->like('nama_obat', $val);
        $this->db->or_like('jenis_obat', $val);
        $query = $this->db->get();

        return $query->result();
    }
}
