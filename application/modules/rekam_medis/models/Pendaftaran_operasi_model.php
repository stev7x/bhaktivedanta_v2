<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran_operasi_model extends CI_Model {
    var $column = array('no_rekam_medis','pasien_nama','jenis_kelamin','tanggal_lahir','pasien_alamat');
    var $order = array('no_rekam_medis' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_pasien_list(){
        $this->db->from('m_pasien');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id');
        $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id');
        $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
        $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
        $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');
        $i = 0; 
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_pasien_all(){
        $this->db->from('m_pasien');

        return $this->db->count_all_results();
    }
    
    public function count_pasien_filtered(){
        $this->db->from('m_pasien');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    function get_pasien_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_pasien');
        $this->db->like('no_rekam_medis', $val);
        $this->db->or_like('pasien_nama', $val);
        $query = $this->db->get();
        
        return $query->result(); 
    }
    
    public function insert_pasien($data){
        $this->db->insert('m_pasien',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function update_pasien($data, $id){
        $update = $this->db->update('m_pasien', $data, array('pasien_id' => $id));

        return $update;
    }
    
    public function insert_pendaftaran($data){
        $this->db->insert('t_pendaftaran',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }
    
    public function insert_penunjang($data){
        $insert = $this->db->insert('t_pasienmasukpenunjang',$data);
        return $insert;
    }
    
    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }
    
    public function insert_penanggungjawab($data){
        $this->db->insert('m_penanggungjawab',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function insert_pembayaran($data){
        $this->db->insert('t_pembayaran',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    
    function get_no_pendaftaran_rj(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RJ".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran 
                        WHERE no_pendaftaran LIKE ('RJ".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_pendaftaran;
    }
    
    function get_no_pendaftaran_ri(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RI".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran 
                        WHERE no_pendaftaran LIKE ('RI".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_pendaftaran;
    }
    
    function get_no_pendaftaran_rd(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RD".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran 
                        WHERE no_pendaftaran LIKE ('RD".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_pendaftaran;
    }
    
    function get_no_pendaftaran_rad(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RAD".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran 
                        WHERE no_pendaftaran LIKE ('RAD".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_pendaftaran;
    }
    
    function get_no_pendaftaran_lab(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "LAB".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran 
                        WHERE no_pendaftaran LIKE ('LAB".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_pendaftaran;
    }
    
    function get_no_rm(){
        $default = "000001";
        $prefix = "RM";
        
        $sql = "SELECT CAST(MAX(SUBSTR(no_rekam_medis,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM m_pasien
                        WHERE no_rekam_medis LIKE ('RM%')";
        $no_current =  $this->db->query($sql)->result();
        
        
        $next_rm = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_rm;
    }
    
    function get_propinsi(){
        $this->db->from('m_propinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_kabupaten($propinsi_id){
        $this->db->order_by('kabupaten_nama', 'ASC');
        $query = $this->db->get_where('m_kabupaten', array('propinsi_id' => $propinsi_id));
        
        return $query->result();
    }
    
    function get_kecamatan($kabupaten_id){
        $this->db->order_by('kecamatan_nama', 'ASC');
        $query = $this->db->get_where('m_kecamatan', array('kabupaten_id' => $kabupaten_id));
        
        return $query->result();
    }
    
    function get_kelurahan($kecamatan_id){
        $this->db->order_by('kelurahan_nama', 'ASC');
        $query = $this->db->get_where('m_kelurahan', array('kecamatan_id' => $kecamatan_id));
        
        return $query->result();
    }
    
    function get_agama(){
        $this->db->from('m_agama');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_suku(){
        $this->db->from('m_suku');
        $this->db->order_by('suku_nama', 'ASC');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_bahasa(){
        $this->db->from('m_bahasa');
        $this->db->order_by('bahasa', 'ASC');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_pendidikan(){
        $this->db->from('m_pendidikan');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_instalasi(){
        $this->db->from('m_instalasi');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_paketoperasi(){
        $this->db->from('m_paketoperasi');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_kelas_paket_operasi($paketoperasi){
        $query = $this->db->get_where('m_paketoperasi', array('paketoperasi_id' => $paketoperasi), 1, 0);
        $listpaket = $query->result();
      
        $kelaspelayanan = $listpaket[0]->kelaspelayanan_id;
        
        return $kelaspelayanan;
    }
    
    function get_ruangan($paketoperasi){
        $kelaspelayanan_id = $this->get_kelas_paket_operasi($paketoperasi);
        
        $this->db->select('m_kelaspoliruangan.poliruangan_id, m_poli_ruangan.nama_poliruangan');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id = m_kelaspoliruangan.poliruangan_id');
        $this->db->where('m_kelaspoliruangan.kelaspelayanan_id', $kelaspelayanan_id);
        $this->db->where('m_poli_ruangan.instalasi_id', 3);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function get_pasien_by_id($id){
        $query = $this->db->get_where('m_pasien', array('pasien_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function insert_tindakanpasien($data=array()){
        $insert = $this->db->insert('t_tindakanpasien',$data);
        return $insert;
    }
    
    public function get_tarif_tindakan($id){
        $q = $this->db->get_where('m_tariftindakan', array('tariftindakan_id' => $id));
        return $q->row();
    }
    
    public function insert_admisi($data){
        $this->db->insert('t_pasienadmisi',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }
    
    public function insert_kamar($data){
        $insert = $this->db->insert('t_masukkamar',$data);
        return $insert;
    }
    
    public function get_kamar_aktif($ruangan_id, $kelasruangan){
        $this->db->order_by('no_kamar', 'ASC');
        $query = $this->db->get_where('m_kamarruangan', array('poliruangan_id' => $ruangan_id, 'kelaspelayanan_id' => $kelasruangan, 'status_aktif' => '1'));
        
        return $query->result();
    }
    
    public function update_kamar($data, $id){
        $update = $this->db->update('m_kamarruangan', $data, array('kamarruangan_id' => $id));

        return $update;
    }
    
    public function update_pendaftaran($data, $id){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    
    public function get_status_pendaftaran($pasien_id){
        $query = $this->db->get_where('t_pendaftaran', array('pasien_id' => $pasien_id, 'status_pasien' => 1));
        
        return $query->num_rows();
    }
    
    function get_dokter($poliruangan_id){
        $this->db->select('m_dokter.id_M_DOKTER, m_dokter.NAME_DOKTER');
        $this->db->from('m_dokter');
        $this->db->join('m_dokterruangan','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
        $this->db->where('m_dokterruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        
        return $query->result(); 
    }
    
    public function get_dokter_anastesi(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_perawat(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_PERAWAT); //Perawat
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tindakan_paket($paketoperasi_id){
        $query = $this->db->get_where('m_paketoperasidetail', array('paketoperasi_id' => $paketoperasi_id));
        
        return $query->result();
    }
    
    public function insert_tindakan_paket($paketoperasi_id, $pendaftaran_id, $pasien_id, $dokter_id, $dokteranastesi_id, $perawat_id){
        $tindakan = $this->get_tindakan_paket($paketoperasi_id);
        $statusinsert = false;
        
        foreach($tindakan as $list){
            $tariftindakan = $this->get_tarif_tindakan($list->tariftindakan_id);
            $hargatindakan = intval($tariftindakan->harga_tindakan) - intval($list->subsidi);
            $total_harga = intval($hargatindakan) * intval($list->jumlah);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'tariftindakan_id' => $list->tariftindakan_id,
                'jml_tindakan' => $list->jumlah,
                'total_harga_tindakan' => $total_harga,
                'is_cyto' => '0',
                'total_harga' => $total_harga,
                'dokter_id' => $dokter_id,
                'tgl_tindakan' => date('Y-m-d'),
                'dokteranastesi_id' => $dokteranastesi_id,
                'perawat_id' => $perawat_id,
                'petugas_id' =>$this->data['users']->id,
                'is_paket' => '1'
            );
            $inserttindakan = $this->insert_tindakanpasien($data_tindakanpasien);
            
            if($inserttindakan){
                $statusinsert = true;
            }
        }
        
        return $statusinsert;
    }
    
    public function get_obat_paket($paketoperasi_id){
        $query = $this->db->get_where('m_paketoperasiobat', array('paketoperasi_id' => $paketoperasi_id));
        
        return $query->result();
    }
    
    public function get_obat_stok($id){
        $q = $this->db->get_where('v_stokobatalkes', array('obat_id' => $id));
        return $q->row();
    }
    
    public function insert_obat_paket($paketoperasi_id, $pendaftaran_id, $pasien_id, $dokter_id){
        $obat = $this->get_obat_paket($paketoperasi_id);
        $statusinsert = false;
        
        foreach($obat as $list){
            $obatstok = $this->get_obat_stok($list->obat_id);
            if(count($obatstok) > 0){
                $data_reseppasien = array(
                    'obat_id' => $list->obat_id,
                    'qty' => $list->jumlah_obat,
                    'harga_netto' => $obatstok->harga_modal,
                    'harga_jual' => $obatstok->harga_jual - $list->subsidi_obat,
                    'satuan' => $obatstok->satuan,
                    'signa' => null,
                    'pendaftaran_id' => $pendaftaran_id,
                    'pasien_id' => $pasien_id,
                    'dokter_id' => $dokter_id,
                    'tgl_reseptur' => date('Y-m-d'),
                );
                $ins = $this->insert_reseppasien($data_reseppasien);
                if($ins){
                    $updatestok = array(
                        'penjualandetail_id' => $ins,
                        'obat_id' => $list->obat_id,
                        'tglstok_out' => date('Y-m-d'),
                        'qtystok_out' => $list->jumlah_obat,
                        'create_time' => date('Y-m-d H:i:s'),
                        'create_user' => $this->data['users']->id
                    );

                    $inst_update = $this->insert_stokobatalkes_keluar($updatestok);

                    if($inst_update){
                        $statusinsert = true;
                    }
                }
            }else{
                $statusinsert = true;
            }
        }
        
        return $statusinsert;
    }
    
    public function insert_reseppasien($data=array()){
        $this->db->insert('t_resepturpasien',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function insert_stokobatalkes_keluar($data=array()){
        $stok = $this->db->insert('t_stokobat',$data);
        
        return $stok;
    }
}