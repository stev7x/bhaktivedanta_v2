<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasienrj_model extends CI_Model {
    var $column = array('pendaftaran_id','nama_poliruangan','no_pendaftaran','tgl_pendaftaran','no_rekam_medis','no_bpjs','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_   periksa');
    var $order = array('tgl_pendaftaran' => 'ASC');

    var $column1 = array('kode_diagnosa','nama_diagnosa');
    var $order1  = array('kode_diagnosa' => 'ASC'); 

    var $column2 = array('kode_rs','nama_rs','alamat_rs');
    var $order2  = array('rs_rujukan_id' => 'ASC'); 

    var $column3    =   array('diagnosa_kode', 'diagnosa_nama');
    var $order3    =   array('diagnosa_nama','ASC');

    public function __construct(){
        parent::__construct(); 
    }

    public function delete_periksa($pendaftaran_id){
      $datas = array(
          'status_periksa' => 'BATAL PERIKSA',
          'status_pasien' => 0
      );
      $update = $this->db->update('t_pendaftaran', $datas, array('pendaftaran_id' => $pendaftaran_id));
      return $update;
    }

    public function get_pasienrj_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien, $dokter_id, $nama_dokter, $nama_klinik,$no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran){
        $this->db->from('v_rm_pasien_rj');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poli_ruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($nama_klinik)){
            $this->db->like('nama_poliruangan', $nama_klinik);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_all_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien, $dokter_id, $no_rekam_medis, $pasien_alamat,$no_bpjs, $no_pendaftaran){
    // public function count_all_list(){
        $this->db->from('v_rm_pasien_rj');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poli_ruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        return $this->db->count_all_results();
    }

    public function count_all_list_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $no_bpjs, $no_pendaftaran){
        $this->db->from('v_rm_pasien_rj');
        
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }


    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_rm_pasien_rj', array('pendaftaran_id' => $pendaftaran_id), 1, 0);

        return $query->row();
    }

    public function get_dokter_list(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('instalasi_id', 1); //rawat jalan
        $this->db->group_by('id_M_DOKTER');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_diagnosa_pasien_list($pendaftaran_id, $table){
        $this->db->from($table);
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function insert_diagnosapasien($pendaftaran_id, $data=array(), $table){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert($table,$data);

        return $insert;
    }

    public function update_to_sudahperiksa($id){
        $data = array(
            'status_periksa' => 'SUDAH PERIKSA'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

    public function update_to_penunjang($id){
        $data = array(
            'status_periksa' => 'SEDANG DI PENUNJANG'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

    public function get_tindakan_pasien_list($pendaftaran_id){ 
        $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama');
        $this->db->from('t_tindakanpasien'); 
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);   
        $length = $this->input->get('length');   
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    } 

    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan($kelaspelayanan_id, $is_bpjs){
        $this->db->select('m_tariftindakan.*, m_daftartindakan.daftartindakan_nama');
        $this->db->from('m_tariftindakan');
        $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('m_tariftindakan.kelaspelayanan_id',$kelaspelayanan_id);
        $this->db->where('m_daftartindakan.kelompoktindakan_id','2');
        $this->db->where('m_tariftindakan.komponentarif_id','1');
        if(strtoupper($is_bpjs) == strtoupper("BPJS")){
            $this->db->where('m_tariftindakan.bpjs_non_bpjs','BPJS');
        }else{
            $this->db->where('m_tariftindakan.bpjs_non_bpjs','Non BPJS');
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tarif_tindakan($tariftindakan_id){
        $query = $this->db->get_where('m_tariftindakan', array('tariftindakan_id' => $tariftindakan_id), 1, 0);

        return $query->row();
    }

    public function insert_tindakanpasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_tindakanpasien',$data);

        return $insert;
    }
    public function delete_diagnosa($id,$table){
        if ($table == "t_tindakanpasien_icd9") {
            $pk = 'tindakan_icd_id';
        }elseif ($table == "t_diagnosapasien") {
            $pk = 'diagnosapasien_id';
        }
        $delete = $this->db->delete($table, array($pk => $id));

        return $delete;
    }
    public function delete_tindakan($id){
        $delete = $this->db->delete('t_tindakanpasien', array('tindakanpasien_id' => $id));

        return $delete;
    }
     public function get_resep_pasien_list($pendaftaran_id){
        $this->db->select('t_resepturpasien.*, m_obat.nama_obat');
        $this->db->from('t_resepturpasien');
        $this->db->join('m_obat','m_obat.obat_id = t_resepturpasien.obat_id');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('v_stokobatalkes');
        $this->db->like('nama_obat', $val);
        $this->db->where('qtystok !=', 0);
        $query = $this->db->get();

        return $query->result();
    }


    function get_list_diagnosa($icd){
        $this->db->from($icd);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }



    public function count_list_diagnosa($icd){
        $this->db->from($icd);
        return $this->db->count_all_results();
    }


    public function count_list_filtered_diagnosa($icd){
        $this->db->from($icd);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_diagnosa_by_id($id, $table){
            $this->db->from($table);
            $this->db->where('diagnosa_id =', $id);
            $query = $this->db->get();

            return $query->result();
    }



    public function insert_reseppasien($data=array()){
        $this->db->insert('t_resepturpasien',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function insert_stokobatalkes_keluar($data=array()){
        $stok = $this->db->insert('t_stokobat',$data);

        return $stok;
    }

    public function delete_resep($id){
        $delete = $this->db->delete('t_resepturpasien', array('reseptur_id' => $id));

        return $delete;
    }

    public function delete_stok_resep($id){
        $stok = $this->db->delete('t_stokobat', array('penjualandetail_id' => $id));

        return $stok;
    }

    public function get_list_penunjang(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => 4));

        return $query->result();
    }

    public function insert_penunjang($data){
        $insert = $this->db->insert('t_pasienmasukpenunjang',$data);
        return $insert;
    }

    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }

    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_JALAN));

        return $query->result();
    }

    function update_pendaftaran($pendaftaran_id, $data){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $pendaftaran_id));
        return $update;
    }



    function get_list_rs_rujukan(){
        $this->db->select('*');
        $this->db->from('m_rs_rujukan');
        $i = 0;
          
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column2 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column2[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order2)){
            $order2 = $this->order2;
            $this->db->order_by(key($order2), $order2[key($order2)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }



    public function count_list_rs_rujukan(){
        $this->db->select('*');
        $this->db->from('m_rs_rujukan');
        return $this->db->count_all_results();
    }


    public function count_list_filtered_rs_rujukan(){
        $this->db->select('*');
        $this->db->from('m_rs_rujukan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column2 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column2[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order2)){
            $order2 = $this->order2;
            $this->db->order_by(key($order2), $order2[key($order2)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_rs_rujukan_by_id($id){
        $this->db->select('*');
        $this->db->from('m_rs_rujukan');
        $this->db->where('rs_rujukan_id =', $id);
        $query = $this->db->get();

        return $query->result();
    }


    function get_list_diagnosa10(){
         $this->db->select('*, m_diagnosa_icd10.kode_diagnosa, m_diagnosa_icd10.nama_diagnosa');
        $this->db->join('m_diagnosa_icd10','m_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
        $this->db->from('m_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column3 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column3[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order3)){
            $order3 = $this->order3;
            $this->db->order_by(key($order3), $order3[key($order3)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }



    public function count_list_filtered_diagnosa10(){
        $this->db->select('*, m_diagnosa_icd10.kode_diagnosa, m_diagnosa_icd10.nama_diagnosa');
        $this->db->join('m_diagnosa_icd10','m_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
        $this->db->from('m_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column3 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column3[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order3)){
            $order3 = $this->order3;
            $this->db->order_by(key($order3), $order3[key($order3)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_list_diagnosa10(){
            $this->db->from('m_diagnosa');
        
            return $this->db->count_all_results();
    }
	
	function get_diagnosa_by_id10($id){
            // $this->db->select('*');
            $this->db->select('*, m_diagnosa_icd10.kode_diagnosa, m_diagnosa_icd10.nama_diagnosa');
            $this->db->join('m_diagnosa_icd10','m_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
            $this->db->from('m_diagnosa');
            $this->db->where('diagnosa2_id =', $id);            
            $query = $this->db->get();

            return $query->result();
    }



}
