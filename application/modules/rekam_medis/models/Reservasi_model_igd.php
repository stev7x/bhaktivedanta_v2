<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// edit by kfsl
// Reservasi_model_igd copy dari Reservasi_model
class Reservasi_model_igd extends CI_Model {
    var $column = array('no_rekam_medis','pasien_nama','jenis_kelamin','tanggal_lahir','pasien_alamat');
    var $order = array('no_rekam_medis' => 'ASC');
    var $column_rujuk = array('kode_perujuk','nama_perujuk','alamat_kantor','telp_hp','nama_marketing');
    var $order_rujuk = array('kode_perujuk' => 'ASC');

    public function __construct(){
        parent::__construct();
    }

    // insert data pasien
    public function insert_reservasi_igd($id){
        // get new reservasi id biar ga bentrok
        $this->db->select_max('reservasi_id');
        $query = $this->db->get('t_reservasi');  // Produces: SELECT MAX(reservasi_id) as t_reservasi FROM t_reservasi
        $get_id = $query->result();
        $new_id = $get_id[0]->reservasi_id+1;
        // $get_id['reservasi_id']reservasi_id
        // $new_id = $get_id+1;
        // print_r($new_id);die;
        $reservasi_igd_id = $id;
        $sql = "INSERT INTO t_reservasi
        (`reservasi_id`,`kode_booking`,`no_urut`,`no_rekam_medis`,`nama_pasien`,`alamat_pasien`,`jenis_kelamin`,`tanggal_lahir`,`asuransi1`,`asuransi2`,
          `kelas_pelayanan_id`,`dokter_id`,`jam`,`tanggal_reservasi`,`metode_registrasi`,`keterangan`,`status`,`status_verif`,`tgl_verif`,
          `tgl_created`,`no_pendaftaran`,`status_pasien`,`no_telp`,`is_reschedule`,`alasan_id`,`catatan`, `sumber_reservasi`)
          SELECT '".$new_id."',`kode_booking`,`no_urut`,`no_rekam_medis`,`nama_pasien`,`alamat_pasien`,`jenis_kelamin`,`tanggal_lahir`,`asuransi1`,`asuransi2`,
          `kelas_pelayanan_id`,`dokter_id`,`jam`,`tanggal_reservasi`,`metode_registrasi`,`keterangan`,`status`,`status_verif`,`tgl_verif`,
          `tgl_created`,`no_pendaftaran`,`status_pasien`,`no_telp`,`is_reschedule`,`alasan_id`,`catatan`,'IGD' AS  `sumber_reservasi`
          FROM t_reservasi_IGD WHERE reservasi_igd_id = ". $reservasi_igd_id;
        $mantap = $this->db->query($sql);
        if($this->db->affected_rows() > 0){
            return $new_id;
        }else{
            return false;
        }

        //
        // $default = "001";
        // $date = date('ymd');
        // $prefix = "BOK-".$date;
        // $sql = "SELECT CAST(MAX(SUBSTR(kode_booking,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
        //                 FROM t_reservasi
        //                 WHERE kode_booking LIKE ('BOK-".$date."%')";
        // $no_current =  $this->db->query($sql)->result();
        // $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        // return $next_pendaftaran;
    }

    // menampilkan list pasien
    public function get_pasien_list(){
        $this->db->from('m_pasien');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id','left');
        $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id','left');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id','left');
        $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
        $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
        $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');
        $i = 0;
        $search_value = $this->input->get('searc');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan list pasien reservasi berdasarkan parameter #jos
    public function get_pasien_reservasi_igd($tgl_awal,$nama_pasien,$no_rm,$kd_booking){
        $this->db->select('t_reservasi_igd.*, m_perujuk.*');
        $this->db->from('t_reservasi_igd');
        $this->db->join('m_perujuk','m_perujuk.id_perujuk = t_reservasi_igd.perujuk_id', 'left'); // left join cos ada perujuk yang null
        $this->db->where('status is null');
        $this->db->where('tanggal_reservasi = "'. date('Y-m-d', strtotime($tgl_awal)).'"' );

        if(!empty($nama_pasien)){
            $this->db->where('nama_pasien LIKE "%'.$nama_pasien.'%" ');
        }
        if(!empty($no_rm)){
            $this->db->where('no_rekam_medis LIKE "%'.$no_rm.'%" ');
        }
        if(!empty ($kd_booking)){
            $this->db->where('kode_booking LIKE "%'.$kd_booking.'%" ');
        }
        // if(!empty($poli)){
        //     $this->db->where('kelas_pelayanan_id',$poli);
        // }
        // if(!empty($dokter)){
        //     $this->db->where('dokter_id',$dokter);
        // }
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan list pasien batal reservasi berdasarkan parameter
    public function get_pasien_reservasi_igd_batal($tgl_awal,$nama_pasien,$no_rm,$kd_booking){
        $this->db->from('t_reservasi_igd');
        $this->db->where('status = "BATAL"');
        $this->db->where('tanggal_reservasi = "'. date('Y-m-d', strtotime($tgl_awal)). '"');
        if(!empty($nama_pasien)){
            $this->db->where('nama_pasien LIKE "%'.$nama_pasien.'%" ');
        }
        if(!empty($no_rm)){
            $this->db->where('no_rekam_medis LIKE "%'.$no_rm.'%" ');
        }
        if(!empty ($kd_booking)){
            $this->db->where('kode_booking LIKE "%'.$kd_booking.'%" ');
        }
        // if(!empty($poli)){
        //     $this->db->where('kelas_pelayanan_id',$poli);
        // }
        // if(!empty($dokter)){
        //     $this->db->where('dokter_id',$dokter);
        // }
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan list pasien batal reservasi berdasarkan parameter jos
    public function get_pasien_reservasi_igd_tolak($tgl_awal,$nama_pasien,$no_rm,$kd_booking){
        $this->db->select('t_reservasi_igd.*, m_lookup.*');
        $this->db->from('t_reservasi_igd');
        $this->db->join('m_lookup','m_lookup.code = t_reservasi_igd.alasan_id');
        $this->db->where('status = "TOLAK"');
        $this->db->where('tanggal_reservasi = "'. date('Y-m-d', strtotime($tgl_awal)). '"');
        if(!empty($nama_pasien)){
            $this->db->where('nama_pasien LIKE "%'.$nama_pasien.'%" ');
        }
        if(!empty($no_rm)){
            $this->db->where('no_rekam_medis LIKE "%'.$no_rm.'%" ');
        }
        if(!empty ($kd_booking)){
            $this->db->where('kode_booking LIKE "%'.$kd_booking.'%" ');
        }
        // if(!empty($poli)){
        //     $this->db->where('kelas_pelayanan_id',$poli);
        // }
        // if(!empty($dokter)){
        //     $this->db->where('dokter_id',$dokter);
        // }
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan list pasien reservasi berdasarkan id
    public function get_data_reservasi_by_id($id){
        $this->db->from('t_reservasi_igd');
        $this->db->where('reservasi_igd_id',$id);
        $query = $this->db->get();
        return $query->row();
    }

    // menampilkan list pasien reservasi berdasarkan no_rekam_medis
    public function get_data_reservasi_by_norm($id){
        $this->db->from('t_reservasi_igd');
        $this->db->where('no_rekam_medis',$id);
        $query = $this->db->get();
        return $query->row();
    }

    // menampilkan data pasien berdasarkan no_rekam_medis
    public function get_data_m_pasien($data){
        $query = $this->db->get_where('m_pasien', array('no_rekam_medis' =>$data));
        return $query->row();
    }

    // menampilkan perujuk
    public function get_pasien_perujuk(){
        $this->db->from('m_perujuk');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column_rujuk as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column_rujuk[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order_rujuk)){
            $order = $this->order_rujuk;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan data autocomplete
    function get_pasien_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_pasien');
        $this->db->like('no_rekam_medis', $val);
        $this->db->or_like('pasien_nama', $val);
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result();
    }

	// insert alasan m lookup
    public function insert_alasan_m_lookup($data){
        $this->db->insert('m_lookup',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    // insert data pasien
    public function insert_pasien($data){
        $this->db->insert('t_reservasi_igd',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    // update data pasien
    public function update_pasien($data, $id){
        $update = $this->db->update('m_pasien', $data, array('pasien_id' => $id));
        return $update;
    }

    // update nomor perujuk
    public function update_telepon_perujuk($data, $id){
        $update = $this->db->update('m_perujuk', $data, array('id_perujuk' => $id));
        return $update;
    }

    // insert no rm
    public function insert_rm($data){
        $this->db->insert('t_detail_rm', $data);
    }

    // insert no reg
    public function insert_reg($data){
        $this->db->insert('t_detail_reg', $data);
    }

    // menampilkan status pendaftaran
    public function get_status_pendaftaran($pasien_id){
        $query = $this->db->get_where('t_pendaftaran', array('pasien_id' => $pasien_id, 'status_pasien' => 1));
        return $query->num_rows();
    }

    // menampilkan no booking auto
    function get_no_booking(){
        $default = "001";
        $date = date('ymd');
        $prefix = "BOK-".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(kode_booking,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_reservasi_igd
                        WHERE kode_booking LIKE ('BOK-".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $next_pendaftaran;
    }

    // menampilkan no urut auto
    function get_no_urut($tgl_reservasi){
        $date_now = date('Y-m-d');
        $default = "1";
        $sql = "SELECT MAX(no_urut) AS  nomaksimal
                        FROM t_reservasi_igd
                        WHERE tanggal_reservasi = '". $tgl_reservasi ."' AND STATUS IS NULL";
                        $no_current =  $this->db->query($sql)->result();
        $next_no_urut = (isset($no_current[0]->nomaksimal) ? $no_current[0]->nomaksimal+1 : $default);
        return $next_no_urut;
    }

    // menampilkan no_antrian auto
    function get_no_antrian($nama){
        $default = "00001";
        $date = date('Ymd-');
        $prefix = $nama."-".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_antrian,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran
                        WHERE no_antrian LIKE ('".$nama."-".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $next_pendaftaran;
    }

    // menampilkan no pendaftaran rj auto
    function get_no_pendaftaran_rj(){
        $default = "0001";
        $date = date('ymd');
        $prefix = "RJ".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_reservasi_igd
                        WHERE no_pendaftaran LIKE ('RJ".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $next_pendaftaran;
    }

    // menampilkan no pendaftaran reg auto
    function get_no_pendaftaran_reg(){
        $default = "0001";
        $date = date('ymd');
        $prefix = "REG".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_reg,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_detail_reg
                        WHERE no_reg LIKE ('REG".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $next_pendaftaran;
    }

    // mengecek jumlah total kuota dokter
    function check_total_dokter($dokter_id){
        $date = date('Y-m-d');
        $sql = "SELECT COUNT(dokter_id) AS jml_dokter FROM t_pendaftaran
            WHERE tgl_pendaftaran LIKE '%".$date."%' AND dokter_id ='".$dokter_id."'";
        $check = $this->db->query($sql)->result();
        return $check;
    }

    // menampilkan no rekam medis auto
    function get_no_rm(){
        $default = "000001";
        $prefix = "RM";

        $sql = "SELECT CAST(MAX(SUBSTR(no_rekam_medis,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_detail_rm
                        WHERE no_rekam_medis LIKE ('RM%')";
        $no_current =  $this->db->query($sql)->result();


        $next_rm = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_rm;
    }

    // menampilkan asuransi
    function get_asuransi(){
        $this->db->from('m_asuransi');
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan instalasi
    function get_instalasi(){
        $this->db->from('m_instalasi');
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan ruangan
    function get_ruangan($instalasi_id){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => $instalasi_id));
        return $query->result();
    }

    // menampilkan poliklinik
    function get_poliklinik($poliruangan_id){
        $query = $this->db->get_where('m_poli_ruangan', array('poliruangan_id' => $poliruangan_id),1,0);
        return $query->result();
    }

    // menampilkan kelas poli ruangan
    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan ruangan poli
    function get_ruangan_poli($tgl_reservasi){
        $this->db->select('DISTINCT(nama_poliruangan),  poliruangan_id ');
        $query = $this->db->get_where('v_jadwal_dokter', array('hari' => $tgl_reservasi));
        return $query->result();
    }

    // menampilkan dokter sesuai tanggal
    function get_dokter($poliruangan_id,$tgl_reservasi){
        $this->db->select('DISTINCT (NAME_DOKTER), id_M_DOKTER');
        $query = $this->db->get_where('v_jadwal_dokter', array('poliruangan_id' => $poliruangan_id, 'hari' => $tgl_reservasi));
        return $query->result();
    }

    // menampilkan semua dokter
    function get_all_dokter(){
        $this->db->select('DISTINCT (NAME_DOKTER), id_M_DOKTER');
        $this->db->from('v_jadwal_dokter');
        $query = $this->db->get();
        return $query->result();
    }

    // menampilkan data jam dokter
    function get_jam_dokter($dokter_id,$tgl_reservasi){
        $this->db->select('NAME_DOKTER, hari, jam_mulai');
        $query = $this->db->get_where('v_jadwal_dokter', array('id_M_DOKTER' => $dokter_id, 'hari' => $tgl_reservasi));
        return $query->result();
    }

    // update reservasi
    function update_reservasi_igd($data, $id){
        $update = $this->db->update('t_reservasi_IGD', $data, array('reservasi_igd_id' => $id));
        return $update;
    }

    // mengambil data untuk print
    function get_data_print($id){
        $this->db->select('*');
        $this->db->from('t_reservasi_igd');
        $this->db->where('t_reservasi_igd.reservasi_igd_id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    // menghitung jumlah pasien yg telah daftar
    // function get_count_pasien($id,$tgl_reservasi, $jam){
    //     $this->db->select('count(reservasi_igd_id) as jumlah');
    //     $query = $this->db->get_where('t_reservasi_igd', array('dokter_id' => $id, 'tanggal_reservasi' => $tgl_reservasi, 'jam' => $jam));
    //     return $query->row();
    // }

    // cirian
    function html_dokter($id){
        $this->db->select('jumlah_pasien');
        $query = $this->db->get_where('m_dokter', array('id_M_DOKTER' => $id));

        return $query->row();
    }

    // menghitung jumlah kuota dokter
    function get_kuota_dokter($id,$tgl,$jam){
        $this->db->select('kuota');
        $this->db->from('m_jadwal_dokter');
        $this->db->join('m_detail_jadwal', 'm_jadwal_dokter.detail_jadwal_id = m_detail_jadwal.detail_jadwal_id');
        $array = array('dokter_id' => $id, 'hari' => $tgl, 'jam_mulai' => $jam);
        $this->db->where($array);
        $query = $this->db->get();
        return $query->row();
    }

    // menampilkan status rm
    function get_status_rm($no_rm,$tgl_res,$pelayanan){
        $this->db->select('reservasi_igd_id');
        $query = $this->db->get_where('t_reservasi_igd', array('no_rekam_medis' => $no_rm, 'tanggal_reservasi' => $tgl_res, 'status' => "HADIR"));
        return $query->num_rows();
    }

    // mengubah hari pada tanggal ke indo
    function casting_day_indo($date){
        $day1 = date('l', strtotime($date));
        $day = strtolower($day1);
        $day_indo = "";
        if ($day == "sunday") {
            $day_indo = "MINGGU";
        } elseif ($day == "monday") {
            $day_indo = "SENIN";
        } elseif ($day == "tuesday") {
            $day_indo = "SELASA";
        } elseif ($day == "wednesday") {
            $day_indo = "RABU";
        } elseif ($day == "thursday") {
            $day_indo = "KAMIS";
        } elseif ($day == "friday") {
            $day_indo = "JUMAT";
        } elseif ($day == "saturday") {
            $day_indo = "SABTU";
        } else {
            $day_indo = "eweuhan";
        }
        return $day_indo;
    }

    // mengubah format tanggal ke indo
    function casting_date_indo($date){
        $dates = strtolower($date);
        $month = "";
        list($tgl,$bln,$thn) = explode(" ", $dates) ;
        if ($bln == "januari") {
            $month = "01" ;
        } elseif ($bln == "februari" || $bln == "pebruari") {
            $month = "02" ;
        } elseif ($bln == "maret") {
            $month = "03" ;
        } elseif ($bln == "april") {
            $month = "04" ;
        } elseif ($bln == "mei") {
            $month = "05" ;
        } elseif ($bln == "juni") {
            $month = "06" ;
        } elseif ($bln == "juli") {
            $month = "07" ;
        } elseif ($bln == "agustus") {
            $month = "08" ;
        } elseif ($bln == "september") {
            $month = "09" ;
        } elseif ($bln == "oktober") {
            $month = "10" ;
        } elseif ($bln == "november") {
            $month = "11" ;
        } elseif ($bln == "desember") {
            $month = "12" ;
        }
        $format_date_indo = $thn."-".$month."-".$tgl ;
        return $format_date_indo ;
    }

}
