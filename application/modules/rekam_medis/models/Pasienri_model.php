<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasienri_model extends CI_Model {
    var $column = array('no_pendaftaran','tgl_masukadmisi','poliruangan_id','no_rekam_medis','no_bpjs','pasien_nama', 'pasien_alamat','jenis_kelamin','umur','type_pembayaran','kelaspelayanan_nama','poli_ruangan','no_kamar','statusrawat');
    var $order = array('tgl_masukadmisi' => 'DESC');

    var $column1 = array('kode_diagnosa','nama_diagnosa','kelompokdiagnosa_id','kelompokdiagnosa_nama');
    var $order1  = array('kelompokdiagnosa_id' => 'ASC');

    var $column2 = array('kode_rs','nama_rs','alamat_rs');
    var $order2  = array('rs_rujukan_id' => 'ASC');
    public function __construct(){
        parent::__construct();
    }
 
    public function get_pasienri_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien,  $no_rekam_medis, $nama_dokter, $dokter_id,$pasien_alamat,$no_bpjs,$no_pendaftaran, $lokasi){
        $this->db->from('v_rm_pasien_ri');
        $this->db->where('tgl_masukadmisi BETWEEN "'.  date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');     
        if(!empty($poliklinik)){
            $this->db->where('poliruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('statusrawat', $status);
        }

        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($lokasi)){
            $this->db->like('lokasi', $lokasi);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
       
        
       
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        } 

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs, $no_pendaftaran, $lokasi, $nama_dokter){
        $this->db->from('v_rm_pasien_ri');
        $this->db->where('tgl_masukadmisi BETWEEN "'.  date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');     
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($lokasi)){
            $this->db->like('lokasi', $lokasi);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
        
        return $this->db->count_all_results();
    }

    public function count_all_list_filtered($nama_pasien, $no_rekam_medis, $pasien_alamat, $no_bpjs, $no_pendaftaran, $lokasi, $nama_dokter){
        $this->db->from('v_rm_pasien_ri');
        
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->where('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->where('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->where('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($lokasi)){
            $this->db->like('lokasi', $lokasi);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_rm_pasien_ri', array('pendaftaran_id' => $pendaftaran_id), 1, 0);

        return $query->row(); 
    }

    public function get_dokter_list(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('instalasi_id', 3); //rawat inap
        $this->db->group_by('id_M_DOKTER');
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_diagnosa_pasien_list($pendaftaran_id){
        $this->db->from('t_diagnosapasien');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function insert_diagnosapasien($pendaftaran_id, $data=array()){
        $insert = $this->db->insert('t_diagnosapasien',$data);

        return $insert;
    }

    public function get_tindakan_pasien_list($pendaftaran_id){
        $this->db->select('t_tindakanpasien.*, m_daftartindakan.daftartindakan_nama');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        // $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function delete_diagnosa($id){
        $delete = $this->db->delete('t_diagnosapasien', array('diagnosapasien_id' => $id));

        return $delete;
    }
    
    public function get_pasien_admisi($pendaftaran_id){
        $query = $this->db->get_where('t_pasienadmisi', array('pendaftaran_id' => $pendaftaran_id), 1, 0);

        return $query->row();
    }
    
    public function update_pasienadmisi($pendaftaran_id, $data){
        $update2 = $this->db->update('t_pasienadmisi', $data, array('pendaftaran_id' => $pendaftaran_id));
        
        return $update2;
    }
    
    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_INAP));
        
        return $query->result();
    }
    
    function get_dokter($poliruangan_id){
        $this->db->select('m_dokter.id_M_DOKTER, m_dokter.NAME_DOKTER');
        $this->db->from('m_dokter');
        $this->db->join('m_dokterruangan','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
        $this->db->where('m_dokterruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }
    
    function update_pendaftaran($pendaftaran_id, $data){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $pendaftaran_id));
        return $update;
    }

    function get_list_rs_rujukan(){
        $this->db->select('*');
        $this->db->from('m_rs_rujukan');
        $i = 0;
          
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column2 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column2[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order2)){
            $order2 = $this->order2;
            $this->db->order_by(key($order2), $order2[key($order2)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }



    public function count_list_rs_rujukan(){
        $this->db->select('*');
        $this->db->from('m_rs_rujukan');
        return $this->db->count_all_results();
    }


    public function count_list_filtered_rs_rujukan(){
        $this->db->select('*');
        $this->db->from('m_rs_rujukan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column2 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column2[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order2)){
            $order2 = $this->order2;
            $this->db->order_by(key($order2), $order2[key($order2)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_rs_rujukan_by_id($id){
        $this->db->select('*');
        $this->db->from('m_rs_rujukan');
        $this->db->where('rs_rujukan_id =', $id);
        $query = $this->db->get();

        return $query->result();
    }

    function get_list_diagnosa(){
        $this->db->from('v_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }



    public function count_list_diagnosa(){
        $this->db->from('v_diagnosa');
        return $this->db->count_all_results();
    }


    public function count_list_filtered_diagnosa(){
        $this->db->from('v_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_diagnosa_by_id($id){
            $this->db->from('v_diagnosa');
            $this->db->where('diagnosa_id =', $id);
            $query = $this->db->get();

            return $query->result();
    }
}
