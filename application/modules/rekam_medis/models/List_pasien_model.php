<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_pasien_model extends CI_Model {
    var $column = array('pasien_id','pasien_nama','pasien_alamat','tlp_selular', 'no_bpjs','type_identias','no_identitas' );
    var $order = array('pasien_nama' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }


    function get_no_rm(){   
        $default = "000001";
        $prefix = "RM";
        
        $sql = "SELECT CAST(MAX(SUBSTR(no_rekam_medis,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_detail_rm
                        WHERE no_rekam_medis LIKE ('RM%')";
        $no_current =  $this->db->query($sql)->result();
        
        
        $next_rm = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_rm;
    }
    
    public function get_list_pasien_list($nama_pasien, $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas){
        
        // $this->db->join('t_pendaftaran','t_pendaftaran.pasien_id = m_pasien.pasien_id','left');
        $this->db->from('m_pasien');
        $this->db->order_by('no_rekam_medis', 'asc');

        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($tlp_selular)){
            $this->db->like('tlp_selular', $tlp_selular);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_identitas)){
            $this->db->like('no_identitas', $no_identitas);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_list_pasien_all($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas){
        $this->db->from('m_pasien');

        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($tlp_selular)){
            $this->db->like('tlp_selular', $tlp_selular);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_identitas)){
            $this->db->like('no_identitas', $no_identitas);
        }

        return $this->db->count_all_results();
    }
    
    public function count_list_pasien_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $tlp_selular,$no_bpjs, $no_identitas){
        $this->db->from('m_pasien');
        
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($tlp_selular)){
            $this->db->like('tlp_selular', $tlp_selular);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_identitas)){
            $this->db->like('no_identitas', $no_identitas);
        }
        // $i = 0;
        // $search_value = $this->input->get('search');
        // if($search_value){
        //     foreach ($this->column as $item){
        //         ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
        //         $i++;
        //     }
        // }
        // $order_column = $this->input->get('order');
        // if($order_column !== false){
        //     $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        // } 
        // else if(isset($this->order)){
        //     $order = $this->order;
        //     $this->db->order_by(key($order), $order[key($order)]);
        // }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function get_list_pasien_by_id($id){ 
        $query = $this->db->get_where('m_pasien', array('pasien_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_list_pasien($data, $id){
        $update = $this->db->update('m_pasien', $data, array('pasien_id' => $id));
        
        return $update;
    }
    
    public function delete_list_pasien($id){
        $delete = $this->db->delete('m_pasien', array('pasien_id' => $id));
        
        return $delete;
    }
    
    function get_propinsi(){
        $this->db->from('m_propinsi');
        $this->db->order_by('propinsi_nama','ASC');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_kabupaten($propinsi_id){
        $this->db->order_by('kabupaten_nama', 'ASC');
        $query = $this->db->get_where('m_kabupaten', array('propinsi_id' => $propinsi_id));
        
        return $query->result();
    }
    
    function get_kecamatan($kabupaten_id){
        $this->db->order_by('kecamatan_nama', 'ASC');
        $query = $this->db->get_where('m_kecamatan', array('kabupaten_id' => $kabupaten_id));
        
        return $query->result();
    }
    
    function get_kelurahan($kecamatan_id){
        $this->db->order_by('kelurahan_nama', 'ASC');
        $query = $this->db->get_where('m_kelurahan', array('kecamatan_id' => $kecamatan_id));
        
        return $query->result();
    }
    function get_agama(){
        $this->db->from('m_agama');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_suku(){
        $this->db->from('m_suku');
        $this->db->order_by('suku_nama', 'ASC');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    function get_pekerjaan(){
        $this->db->from('m_pekerjaan');
        $query = $this->db->get();
        return $query->result();
    }

    function get_bahasa(){
        $this->db->from('m_bahasa');
        $query = $this->db->get();
        return $query->result();
    }
    function get_pendidikan(){
        $this->db->from('m_pendidikan');
        $query = $this->db->get();
        
        return $query->result();
    }

   function get_data_print($pasien_id){
  
    $this->db->select('*');    
    $this->db->from('m_pasien');
    $this->db->join('m_propinsi','m_propinsi.propinsi_id = m_pasien.propinsi_id','left');
    $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id','left');
    $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id','left');
    $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id','left');
    $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
    $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
    $this->db->join('m_bahasa','m_bahasa.bahasa_id = m_pasien.bahasa_id','left');

    $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');
    $this->db->where('m_pasien.pasien_id', $pasien_id,1,0);
    $query = $this->db->get();   

    
    return $query->row();
    } 

    public function get_data_ket_lahir($id){
        $query =  $this->db->get_where('t_surat_ket_lahir',array('pasien_id' => $id));
  
        return $query->row();   
    }

     public function insert_pasien($data){
        $this->db->insert('t_surat_ket_lahir',$data);
        $id = $this->db->insert_id();
        return $id; 
    }
     public function update_pasien($data, $id){
        $update = $this->db->update('t_surat_ket_lahir', $data, array('pasien_id' => $id));

        return $update;
    }

    public function insert_pasien_kary($data){
        return $this->db->insert('m_pasien',$data);
    }

    public function get_data_user($id){
        $this->db->from('aauth_user_to_group'); 
        $this->db->join('aauth_groups','aauth_user_to_group.group_id = aauth_groups.id','left');
        $this->db->where('aauth_user_to_group.user_id',$id); 
        $query = $this->db->get();
        return $query->row();   
    }

     public function getNoRM(){
         $this->db->select('no_rekam_medis');
        $this->db->from('m_pasien');
        $this->db->order_by('no_rekam_medis','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllPasien($rm_awal, $rm_akhir){
        $rm_awal    = strtoupper($rm_awal);
        $rm_akhir   = strtoupper($rm_akhir);

        $this->db  
                ->select('m_pasien.*, m_pekerjaan.nama_pekerjaan, m_pendidikan.pendidikan_nama, m_kelurahan.kelurahan_nama, m_kecamatan.kecamatan_nama, m_kabupaten.kabupaten_nama, m_agama.agama_nama')
                ->order_by('no_rekam_medis', 'asc')
                ->from('m_pasien')
                ->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left')
                ->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left')
                ->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id','left')
                ->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id','left')
                ->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id','left')
                ->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left')
                ->where('no_rekam_medis BETWEEN "'.$rm_awal.'" AND "'.$rm_akhir.'"');
        $query = $this->db->get();
        return $query->result();
    }

    function get_pasien_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_pasien');  
        $this->db->like('no_rekam_medis', $val);
        $this->db->or_like('pasien_nama', $val);
        $this->db->limit(20);
        $query = $this->db->get();
        
        return $query->result(); 
    }

    public function get_status_pendaftaran($pasien_id){
        $query = $this->db->get_where('t_pendaftaran', array('pasien_id' => $pasien_id, 'status_pasien' => 1));
            
        return $query->num_rows();  
    }



    public function get_riwayat_penyakit_pasien($pasien_id){
        $this->db
                ->select('*')
                ->from('v_pasien_rj, m_pasien')
                ->where('m_pasien.pasien_id',$pasien_id);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_riwayat_pasien_rj($pasien_id){
        $this->db->from('v_riwayat_pasien_rj');
        $this->db->where('pasien_id',$pasien_id);
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function get_riwayat_pasien_igd($pasien_id){
        $this->db->from('v_riwayat_pasien_igd');
        $this->db->where('pasien_id',$pasien_id);
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function get_riwayat_pasien_ri($pasien_id){
        $this->db->from('v_riwayat_pasien_ri');
        $this->db->where('pasien_id',$pasien_id);
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        return $query->result();
    }
 
}