<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_pasienri_model extends CI_Model {
    var $column = array('kamarruangan_id','nama_poliruangan','kelaspelayanan_nama','no_kamar','no_bed','status_bed','no_rekam_medis','pasien_nama');
    var $order = array('kamarruangan_id' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_ruanganpasienri_list(){
        $this->db->from('v_pasien_ruangan_ri');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_ruangan_all(){
        $this->db->from('v_pasien_ruangan_ri');

        return $this->db->count_all_results();
    }
    
    public function count_ruangan_filtered(){
        $this->db->from('v_pasien_ruangan_ri');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
}