<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservasi_model extends CI_Model {
    var $column = array('no_rekam_medis','pasien_nama','jenis_kelamin','tanggal_lahir','pasien_alamat');
    var $order = array('no_rekam_medis' => 'ASC');
    var $column_rujuk = array('kode_perujuk','nama_perujuk','alamat_kantor','telp_hp','nama_marketing');
    var $order_rujuk = array('kode_perujuk' => 'ASC');
    
    public function __construct(){
        parent::__construct();
    }
    
    // menampilkan list pasien
    public function get_pasien_list(){
        $this->db->from('m_pasien');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id','left');
        $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id','left');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id','left');
        $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
        $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
        $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){ 
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
        return $query->result(); 
    }

    // menampilkan list pasien reservasi berdasarkan parameter
    public function get_pasien_reservasi($tgl_awal,$poli,$dokter,$nama_pasien,$no_rm,$kd_booking){
        $this->db->from('t_reservasi');
        $this->db->join('m_poli_ruangan', 'm_poli_ruangan.poliruangan_id = t_reservasi.kelas_pelayanan_id');
        $this->db->join('m_dokter', 'm_dokter.id_M_DOKTER = t_reservasi.dokter_id');                    
        $this->db->where('status != "BATAL"'); 
        $this->db->where('tanggal_reservasi = "'. date('Y-m-d', strtotime($tgl_awal)).'"' );
        $this->db->where('branch_id = "'. $this->session->tempdata('data_session')[0].'"' );         
                     
        if(!empty($nama_pasien)){
            $this->db->where('nama_pasien LIKE "%'.$nama_pasien.'%" ');
        }
        if(!empty($no_rm)){
            $this->db->where('no_rekam_medis LIKE "%'.$no_rm.'%" ');
        }
        if(!empty ($kd_booking)){
            $this->db->where('kode_booking LIKE "%'.$kd_booking.'%" ');
        }   
        if(!empty($poli)){    
            $this->db->where('kelas_pelayanan_id',$poli);
        }
        if(!empty($dokter)){ 
            $this->db->where('dokter_id',$dokter);
        } 
        $query = $this->db->get();   
        return $query->result();            
    }   

    // menampilkan list pasien batal reservasi berdasarkan parameter
    public function get_pasien_reservasi_batal($tgl_awal,$poli,$dokter,$nama_pasien,$no_rm,$kd_booking){        
        $this->db->from('t_reservasi');           
        $this->db->where('status = "BATAL"');
        $this->db->where('tanggal_reservasi = "'. date('Y-m-d', strtotime($tgl_awal)). '"');   
        if(!empty($nama_pasien)){
            $this->db->where('nama_pasien LIKE "%'.$nama_pasien.'%" ');
        } 
        if(!empty($no_rm)){
            $this->db->where('no_rekam_medis LIKE "%'.$no_rm.'%" ');
        } 
        if(!empty ($kd_booking)){
            $this->db->where('kode_booking LIKE "%'.$kd_booking.'%" ');
        }
        if(!empty($poli)){
            $this->db->where('kelas_pelayanan_id',$poli); 
        }
        if(!empty($dokter)){
            $this->db->where('dokter_id',$dokter); 
        }  
        $query = $this->db->get();
        return $query->result();      
    }

    // menampilkan list pasien reservasi berdasarkan id    
    public function get_data_reservasi_by_id($id){
        $this->db->from('t_reservasi');
        $this->db->where('reservasi_id',$id);       
        $query = $this->db->get();    
        return $query->row();    
    }

    // menampilkan list pasien reservasi berdasarkan no_rekam_medis    
    public function get_data_reservasi_by_norm($id){
        $this->db->from('t_reservasi');     
        $this->db->where('no_rekam_medis',$id);       
        $query = $this->db->get();    
        return $query->row();    
    }

    // menampilkan data pasien berdasarkan no_rekam_medis        
    public function get_data_m_pasien($data){
        $query = $this->db->get_where('m_pasien', array('no_rekam_medis' =>$data));
        return $query->row();
    }  

    // menampilkan perujuk
    public function get_pasien_perujuk(){
        $this->db->from('m_perujuk');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column_rujuk as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column_rujuk[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order_rujuk)){
            $order = $this->order_rujuk;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
        return $query->result();
    }
   
    // menampilkan data autocomplete
    function get_pasien_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_pasien');  
        $this->db->like('no_rekam_medis', $val);
        $this->db->or_like('pasien_nama', $val);
        $this->db->limit(10);
        $query = $this->db->get();     
        return $query->result(); 
    }
    
    // insert data pasien
    public function insert_pasien($data){
        $this->db->insert('t_reservasi', $data);
        $id = $this->db->insert_id();

//        echo "<pre>";
//        print_r($data);
//        print_r($this->db->error());
//        die;

        return $id;
    }

    public function insert_hotel($data){  
        $this->db->insert('m_hotel',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    
    // update data pasien
    public function update_pasien($data, $id){
        $update = $this->db->update('m_pasien', $data, array('pasien_id' => $id));
        return $update;
    }

    // insert no rm
    public function insert_rm($data){
        $this->db->insert('t_detail_rm', $data);
    }

    // insert no reg
    public function insert_reg($data){
        $this->db->insert('t_detail_reg', $data);
    }

    // menampilkan status pendaftaran
    public function get_status_pendaftaran($pasien_id){
        $query = $this->db->get_where('t_pendaftaran', array('pasien_id' => $pasien_id, 'status_pasien' => 1));
        return $query->num_rows();  
    }

    public function get_pendaftaran($pasien_id){
        $this->db->select('*');
        $this->db->from('t_pendaftaran');
        $this->db->where('pasien_id', $pasien_id);
        $this->db->order_by('pendaftaran_id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_pasien($no_rm){
        $this->db->select('*');
        $this->db->from('m_pasien');
        $this->db->where('no_rekam_medis', $no_rm);
        $this->db->order_by('pasien_id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->result();
    }
 
    // menampilkan no booking auto
    function get_no_booking(){
        $default = "001";
        $date = date('ymd');
        // $prefix = "BOK-".$date;
        $prefix = "BOK-".$this->session->tempdata('data_session')[2]."-".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(kode_booking,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_reservasi 
                        WHERE kode_booking LIKE ('$prefix%')";
        $no_current =  $this->db->query($sql)->result();
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $next_pendaftaran;
    }
       // NEW FOR KLINIK
    function get_layanan(){
        $this->db->select('*');
        $this->db->from('m_layanan');  
        $query = $this->db->get();
        return $query->result(); 
    }

    function get_jenis_pasien(){
        $this->db->select('*');
        $this->db->from('m_jenis_pasien');  
        $query = $this->db->get();
        return $query->result(); 
    }

    function get_hotel(){   
        $this->db->select('*');   
        $this->db->from('m_hotel');  
        $query = $this->db->get();
        return $query->result(); 
    }
 // ==============================================================================

    // menampilkan no urut auto
    function get_no_urut($tgl_reservasi, $dokter_id){
        $date_now = date('Y-m-d');
        $default = "1";
        $sql = "SELECT MAX(no_urut) AS  nomaksimal
                        FROM t_reservasi 
                        WHERE tanggal_reservasi = '". $tgl_reservasi ."' AND dokter_id = '". $dokter_id ."' AND status != 'BATAL' ";
                        $no_current =  $this->db->query($sql)->result();
        $next_no_urut = (isset($no_current[0]->nomaksimal) ? $no_current[0]->nomaksimal+1 : $default);
        return $next_no_urut;
    }
    
    // menampilkan no_antrian auto
    function get_no_antrian($nama){
        $default = "00001";   
        $date = date('Ymd-');       
        $prefix = $nama."-".$date;     
        $sql = "SELECT CAST(MAX(SUBSTR(no_antrian,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran           
                        WHERE no_antrian LIKE ('".$nama."-".$date."%')";      
        $no_current =  $this->db->query($sql)->result();
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);  
        return $next_pendaftaran;      
    }

    // menampilkan no pendaftaran rj auto
    function get_no_pendaftaran_rj(){
        $default = "0001";
        $date = date('ymd');
        $prefix = "RJ".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_reservasi 
                        WHERE no_pendaftaran LIKE ('RJ".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $next_pendaftaran;
    }

    // menampilkan no pendaftaran reg auto
    function get_no_pendaftaran_reg(){
        $default = "0001";
        $date = date('ymd');
        $prefix = "REG-".$this->session->tempdata('data_session')[2]."-".$date;
        // $prefix = "REG".$date;
        
        $sql = "SELECT CAST(MAX(SUBSTR(no_reg,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_detail_reg 
                        WHERE no_reg LIKE ('$prefix%')";
        $no_current =  $this->db->query($sql)->result();
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $next_pendaftaran;
    }
     
    // mengecek jumlah total kuota dokter
    function check_total_dokter($dokter_id){   
        $date = date('Y-m-d');
        $sql = "SELECT COUNT(dokter_id) AS jml_dokter FROM t_pendaftaran
            WHERE tgl_pendaftaran LIKE '%".$date."%' AND dokter_id ='".$dokter_id."'";    
        $check = $this->db->query($sql)->result();    
        return $check; 
    }

    // menampilkan no rekam medis auto
    function get_no_rm(){
        $default = "000001";
        $prefix = "RM".$this->session->tempdata('data_session')[2];
        $sql = "SELECT CAST(MAX(SUBSTR(no_rekam_medis,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_detail_rm
                        WHERE no_rekam_medis LIKE ('$prefix%')"; 
        $no_current =  $this->db->query($sql)->result();
        
        
        $next_rm = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_rm;
    }

    // menampilkan asuransi
    function get_asuransi($type){
        $this->db->from('m_asuransi');
        if (!empty($type)) {
            $this->db->where('tipe_asuransi',$type);
        }
        $query = $this->db->get();
        return $query->result();
    }
    
    // menampilkan instalasi
    function get_instalasi(){
        $this->db->from('m_instalasi');
        $query = $this->db->get();
        return $query->result();
    }
    
    // menampilkan ruangan
    function get_ruangan($instalasi_id){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => $instalasi_id));
        return $query->result(); 
    }

    
    
    // menampilkan poliklinik
    function get_poliklinik($poliruangan_id){
        $query = $this->db->get_where('m_poli_ruangan', array('poliruangan_id' => $poliruangan_id),1,0);
        return $query->result();  
    }

    // menampilkan kelas poli ruangan
    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    // menampilkan ruangan poli
    function get_ruangan_poli($tgl_reservasi){
        $this->db->select('DISTINCT(nama_poliruangan),  poliruangan_id ');
        $query = $this->db->get_where('v_jadwal_dokter', array('hari' => $tgl_reservasi));
        return $query->result(); 
    } 

    // menampilkan dokter sesuai tanggal
    function get_dokter($poliruangan_id,$tgl_reservasi){   
        $this->db->select('DISTINCT (NAME_DOKTER), id_M_DOKTER');   
        $query = $this->db->get_where('v_jadwal_dokter', array('poliruangan_id' => $poliruangan_id, 'hari' => $tgl_reservasi, 'nama_branch' => $this->session->tempdata('data_session')[1]));
        // $query = $this->db->get_where('v_jadwal_dokter', array('nama_branch' => $this->session->tempdata('data_session')[1], 'hari' => $tgl_reservasi));

        return $query->result(); 
    }

    // menampilkan semua dokter
    function get_all_dokter(){
        $this->db->select('DISTINCT (NAME_DOKTER), id_M_DOKTER');  
        $this->db->from('v_jadwal_dokter');
        $query = $this->db->get(); 
        return $query->result();   
    }

    // menampilkan data jam dokter
    function get_jam_dokter($dokter_id,$tgl_reservasi){
        $this->db->select('NAME_DOKTER, hari, jam_mulai');
        $this->db->from('v_jadwal_dokter');
        $this->db->where('id_M_DOKTER', $dokter_id);
        $this->db->where('hari', $tgl_reservasi);
        $this->db->where('nama_branch = "'. $this->session->tempdata('data_session')[1].'"' ); 
        $query = $this->db->get(); 
        return $query->result(); 
    }

    // update reservasi
    function update_reservasi($data, $id){
        $update = $this->db->update('t_reservasi', $data, array('reservasi_id' => $id));
        // print_r($this->db->error());die;
        return $update;
    }

    // mengambil data untuk print
    function get_data_print($id){
        $this->db->select('*');
        $this->db->from('t_reservasi');
        $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id = t_reservasi.kelas_pelayanan_id');
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_reservasi.dokter_id');
        $this->db->where('t_reservasi.reservasi_id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    // menghitung jumlah pasien yg telah daftar
    function get_count_pasien($id,$tgl_reservasi, $jam){
        $this->db->select('count(reservasi_id) as jumlah');
        $query = $this->db->get_where('t_reservasi', array('dokter_id' => $id, 'tanggal_reservasi' => $tgl_reservasi, 'jam' => $jam));
        return $query->row();
    }      

    // cirian
    function html_dokter($id){
        $this->db->select('jumlah_pasien');
        $query = $this->db->get_where('m_dokter', array('id_M_DOKTER' => $id));

        return $query->row();
    }

    // menghitung jumlah kuota dokter
    function get_kuota_dokter($id,$tgl,$jam){
        $this->db->select('kuota');
        $this->db->from('m_jadwal_dokter');
        $this->db->join('m_detail_jadwal', 'm_jadwal_dokter.detail_jadwal_id = m_detail_jadwal.detail_jadwal_id');
        $array = array('dokter_id' => $id, 'hari' => $tgl, 'jam_mulai' => $jam);
        $this->db->where($array);
        $query = $this->db->get();
        return $query->row();
    }

    // menampilkan status rm
    function get_status_rm($no_rm,$tgl_res,$pelayanan){
        $this->db->select('reservasi_id');
        $query = $this->db->get_where('t_reservasi', array('no_rekam_medis' => $no_rm, 'tanggal_reservasi' => $tgl_res, 'status' => "HADIR"));
        return $query->num_rows();
    }

    // mengubah hari pada tanggal ke indo
    function casting_day_indo($date){
        $day1 = date('l', strtotime($date));
        $day = strtolower($day1);
        $day_indo = "";
        if ($day == "sunday") {
            $day_indo = "MINGGU";
        } elseif ($day == "monday") {
            $day_indo = "SENIN";
        } elseif ($day == "tuesday") {
            $day_indo = "SELASA";
        } elseif ($day == "wednesday") {
            $day_indo = "RABU";
        } elseif ($day == "thursday") {
            $day_indo = "KAMIS";
        } elseif ($day == "friday") {
            $day_indo = "JUMAT";
        } elseif ($day == "saturday") {
            $day_indo = "SABTU";
        } else {
            $day_indo = "eweuhan";
        }
        return $day_indo;
    }

    // mengubah format tanggal ke indo
    function casting_date_indo($date){
//         var_dump($date); die;
        $dates = strtolower($date);
        $month = "";
        list($tgl,$bln,$thn) = explode(" ", $dates);
        if ($bln == "januari" || $bln == "january" || $bln == "jan") {
            $month = "01" ;
        } elseif ($bln == "februari" || $bln == "pebruari" || $bln == "february" || $bln == "feb") {
            $month = "02" ;          
        } elseif ($bln == "maret" || $bln == "march" || $bln == "mar") {
            $month = "03" ;          
        } elseif ($bln == "april" || $bln == "apr") {
            $month = "04" ;          
        } elseif ($bln == "mei" || $bln == "may") {
            $month = "05" ;          
        } elseif ($bln == "juni" || $bln == "june" || $bln == "jun") {
            $month = "06" ;          
        } elseif ($bln == "juli" || $bln == "july" || $bln == "jul") {
            $month = "07" ;          
        } elseif ($bln == "agustus" || $bln == "august" || $bln == "aug" || $bln == "agu") {
            $month = "08" ;          
        } elseif ($bln == "september"  || $bln == "sep") {
            $month = "09" ;          
        } elseif ($bln == "oktober" || $bln == "october" || $bln == "oct" || $bln == "jan") {
            $month = "10" ;          
        } elseif ($bln == "november" || $bln == "nov") {
            $month = "11" ;          
        } elseif ($bln == "desember" || $bln == "december" || $bln == "dec" || $bln == "des") {
            $month = "12" ;          
        }
        
        $format_date_indo = $thn."-".$month."-".$tgl ;
        // var_dump($format_date_indo);
        return $format_date_indo ;
    }

    public function get_driver() {
        return $this->db->get("m_driver")->result();
    }

    public function insert_pembayaran($data) {
        $insert = $this->db->insert("t_pembayaran", $data);
        if ($insert) {
            $this->db->select("pembayaran_id");
            $this->db->order_by("pembayaran_id", "desc");
            $this->db->limit(1);
            return $this->db->get("t_pembayaran")->row()->pembayaran_id;
        }
        return 0;
    }
}