<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran_model extends CI_Model {
    var $column = array('no_rekam_medis','pasien_nama','jenis_kelamin','tanggal_lahir','pasien_alamat');
    var $order = array('no_rekam_medis' => 'ASC');
    var $column_rujuk = array('kode_perujuk','nama_perujuk','alamat_kantor','telp_hp','nama_marketing');
    var $order_rujuk = array('kode_perujuk' => 'ASC');

    public function __construct() {
        parent::__construct();
    }

    public function get_pasien_list(){
        $this->db->from('m_pasien');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id', 'left');
        $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id', 'left');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id', 'left');
        $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
        $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
        $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_pasien_all(){
        $this->db->from('m_pasien');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id', 'left');
        $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id', 'left');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id', 'left');
        $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
        $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
        $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');

        return $this->db->count_all_results();
    }

    public function count_pasien_filtered(){
        $this->db->from('m_pasien');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id', 'left');
        $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id', 'left');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id', 'left');
        $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
        $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
        $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_pasien_perujuk(){
        $this->db->from('m_perujuk');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column_rujuk as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column_rujuk[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order_rujuk)){
            $order = $this->order_rujuk;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    function get_skor_kspr_list($pasien_kspr_id){
        $this->db->select('t_skor_kspr.*, m_kspr.kspr_nama');
        $this->db->from('t_skor_kspr');
        $this->db->join('m_kspr','m_kspr.kspr_id = t_skor_kspr.kspr_id','left');
        $this->db->where('pasien_id', $pasien_kspr_id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_faktor_resiko($pasien_kspr_id){
        $sql = 'SELECT CONCAT(m_kspr.`kspr_nama`," ", t_skor_kspr.`jml`,"x") AS faktor
            FROM t_skor_kspr
            LEFT JOIN m_kspr ON m_kspr.`kspr_id` = t_skor_kspr.`kspr_id`
            WHERE pasien_id = "'.$pasien_kspr_id.'"';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function count_skor_kspr_all(){
            $this->db->from('t_skor_kspr');
            return $this->db->count_all_results();
    }

    public function count_perujuk_all(){
        $this->db->from('m_perujuk');

        return $this->db->count_all_results();
    }

    public function count_perujuk_filtered(){
        $this->db->from('m_perujuk');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column_rujuk as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column_rujuk[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order_rujuk)){
            $order = $this->order_rujuk;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_pasien_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_pasien');
        $this->db->like('no_rekam_medis', $val);
        $this->db->or_like('pasien_nama', $val);
        $query = $this->db->get();

        return $query->result();
    }

    public function insert_pasien($data){
        // var_dump($data);die;
        // $this->db->truncate('m_pasien'); 
        $this->db->insert('m_pasien',$data);
        // print_r($this->db->error());die;
        $id = $this->db->insert_id();
        return $id;
        
        // $sql = "insert into m_pasien (no_rekam_medis, pasien_nama)
        // values ('35, 'Group 1')";
        // $this->db->query($sql);
        // $insert = $this->db->insert_id();
        // return $insert;

    }

    public function update_pasien($data, $where, $id){
        $this->db->where($where, $id);
        $update = $this->db->update('m_pasien', $data);
        return $update;
    }

    public function update_pasien_retension($data, $id) {
        $this->db->where('no_rekam_medis', $id);
        return $this->db->update('m_pasien', $data);
    }

    public function insert_rm($data){
        $this->db->insert('t_detail_rm', $data);
    }

    public function insert_pendaftaran($data){
        $this->db->insert('t_pendaftaran',$data);
        // print_r($this->db->error());die;
        $insert = $this->db->insert_id();
        return $insert;
    }

    public function insert_penunjang($data){
        $insert = $this->db->insert('t_pasienmasukpenunjang',$data);
        return $insert;
    }

    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }

    public function insert_penanggungjawab($data){
        $this->db->insert('m_penanggungjawab',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function insert_pembayaran($data){
        $this->db->insert('t_pembayaran',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    function get_no_pendaftaran_rj(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RJ".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran
                        WHERE no_pendaftaran LIKE ('RJ".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pendaftaran;
    }

    function get_no_pendaftaran_ri(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RI".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran
                        WHERE no_pendaftaran LIKE ('RI".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pendaftaran;
    }

    function get_no_pendaftaran_rd(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RD".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran
                        WHERE no_pendaftaran LIKE ('RD".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pendaftaran;
    }

    function get_no_pendaftaran_rad(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RAD".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran
                        WHERE no_pendaftaran LIKE ('RAD".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pendaftaran;
    }

    function get_no_pendaftaran_lab(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "LAB".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran
                        WHERE no_pendaftaran LIKE ('LAB".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pendaftaran;
    }

    function get_no_antrian($nama){
        $default = "00001";
        $date = date('Ymd-');
        $prefix = $nama."-".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_antrian,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran
                        WHERE no_antrian LIKE ('".$nama."-".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pendaftaran;
    }

    function check_ktp($data){
        $sql = "SELECT no_identitas FROM m_pasien
                WHERE no_identitas = '".$data."'";
        $hasil = $this->db->query($sql)->result();
        return $hasil;
    }

    function check_no_rm($data){
        $query = $this->db->get_where('m_pasien', array('no_rekam_medis' => $data), 1, 0);
        return $query->num_rows();
    }

    function check_bpjs($data){
        $sql = "SELECT no_bpjs FROM m_pasien
                WHERE no_bpjs = '".$data."'";
        $hasil = $this->db->query($sql)->result();
        return $hasil;
    }

    function check_total_dokter($dokter_id){
        $date = date('Y-m-d');
        $sql = "SELECT COUNT(dokter_id) AS jml_dokter FROM t_pendaftaran
            WHERE tgl_pendaftaran LIKE '%".$date."%' AND dokter_id ='".$dokter_id."'";

        $check = $this->db->query($sql)->result();

        return $check;
    }

    function get_no_rm(){
        $default = "000001";
        $prefix = "RM";

        $sql = "SELECT CAST(MAX(SUBSTR(no_rekam_medis,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_detail_rm
                        WHERE no_rekam_medis LIKE ('RM%')";
        $no_current =  $this->db->query($sql)->result();


        $next_rm = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_rm;
    }

    function get_propinsi(){
        $this->db->order_by("propinsi_nama","ASC");
        $this->db->from('m_propinsi');
        $query = $this->db->get();

        return $query->result();
    }

    function get_kabupaten($propinsi_id){
        $this->db->order_by('kabupaten_nama', 'ASC');
        $query = $this->db->get_where('m_kabupaten', array('propinsi_id' => $propinsi_id));

        return $query->result();
    }

    function get_kecamatan($kabupaten_id){
        $this->db->order_by('kecamatan_nama', 'ASC');
        $query = $this->db->get_where('m_kecamatan', array('kabupaten_id' => $kabupaten_id));

        return $query->result();
    }

    function get_kelurahan($kecamatan_id){
        $this->db->order_by('kelurahan_nama', 'ASC');
        $query = $this->db->get_where('m_kelurahan', array('kecamatan_id' => $kecamatan_id));

        return $query->result();
    }

    function get_pekerjaan(){
        $this->db->from('m_pekerjaan');
        $query = $this->db->get();

        return $query->result();
    }

    function get_agama(){
        $this->db->from('m_agama');
        $query = $this->db->get();

        return $query->result();
    }

    function get_suku(){
        $this->db->from('m_suku');
        $this->db->order_by('suku_nama', 'ASC');
        $query = $this->db->get();

        return $query->result();
    }

    function get_bahasa(){
        $this->db->from('m_bahasa');
        $this->db->order_by('bahasa', 'ASC');
        $query = $this->db->get();

        return $query->result();
    }

    function get_pendidikan(){
        $this->db->from('m_pendidikan');
        $query = $this->db->get();

        return $query->result();
    }

    function get_instalasi(){
        $this->db->from('m_instalasi');
        $query = $this->db->get();

        return $query->result();
    }

    function get_ruangan($instalasi_id){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => $instalasi_id));

        return $query->result();
    }

    function get_poliklinik($poliruangan_id){
        // $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('poliruangan_id' => $poliruangan_id),1,0);

        return $query->result();
    }
    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_pasien_by_id($id){
        $query = $this->db->get_where('m_pasien', array('pasien_id' => $id), 1, 0);

        return $query->row();
    }

    public function get_perujuk_by_id($id){
        $query = $this->db->get_where('m_perujuk', array('id_perujuk' => $id), 1, 0);

        return $query->row();
    }

    public function insert_tindakanpasien($data=array()){
        $insert = $this->db->insert('t_tindakanpasien',$data);
        return $insert;
    }

    public function get_tarif_tindakan($id){
        $q = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $id));
        return $q->row();
    }

    public function get_kelas_poli($poli, $kelas_poli){
        $q = $this->db->get_where('m_kelaspoliruangan', array('poliruangan_id' => $poli,'kelaspelayanan_id' => $kelas_poli));
        return $q->row();
    }

    public function get_kelas_pelayanan_poli($kelas_poli){
        $q = $this->db->get_where('m_kelaspelayanan', array('kelaspelayanan_id' => $kelas_poli));
        return $q->row();
    }

    public function get_tarif_adm($poli){
        $sql = "SELECT tariftindakan_admin FROM `m_poli_ruangan` WHERE `poliruangan_id` = '$poli'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_tarif_pelayanan($poli){
        $sql = "SELECT tarif_pelayanan FROM `m_poli_ruangan` WHERE `poliruangan_id` = '$poli'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_tarif_dokter($poli){
        $sql = "SELECT tariftindakan_dokter FROM `m_poli_ruangan` WHERE `poliruangan_id` = '$poli'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function insert_admisi($data){
        $this->db->insert('t_pasienadmisi',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }

    public function insert_kamar($data){
        $insert = $this->db->insert('t_masukkamar',$data);
        return $insert;
    }

    public function get_kamar_aktif($ruangan_id, $kelasruangan){
        $this->db->order_by('no_kamar', 'ASC');
        $query = $this->db->get_where('m_kamarruangan', array('poliruangan_id' => $ruangan_id, 'kelaspelayanan_id' => $kelasruangan, 'status_aktif' => '1'));

        return $query->result();
    }

    public function update_kamar($data, $id){
        $update = $this->db->update('m_kamarruangan', $data, array('kamarruangan_id' => $id));

        return $update;
    }

    public function update_pendaftaran($data, $id){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

    public function get_status_pendaftaran($pasien_id){
        $query = $this->db->get_where('t_pendaftaran', array('pasien_id' => $pasien_id, 'status_pasien' => 1));

        return $query->num_rows();
    }

    function get_dokter($poliruangan_id,$instalasi_id){
        $this->db->select('m_dokter.id_M_DOKTER, m_dokter.NAME_DOKTER');
        $this->db->from('m_dokter');
//        if($instalasi_id == INSTALASI_RAWAT_JALAN){
//            $this->db->join('m_dokterruangan','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
//            $this->db->where('m_dokterruangan.poliruangan_id', $poliruangan_id);
//        }
//        if($poliruangan_id == POLIRUANGAN_LABORATORIUM){
//            // $this->db->where('m_dokter.kelompokdokter_id', KELOMPOK_PATOLOGI_KLINIS);
//            $this->db->join('m_dokterruangan','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
//            $this->db->where('m_dokterruangan.poliruangan_id', $poliruangan_id);
//        }
//        if($poliruangan_id == POLIRUANGAN_RADIOLOGI){
//            // $this->db->where('m_dokter.kelompokdokter_id', KELOMPOK_RADIOLOGI);
//            $this->db->join('m_dokterruangan','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
//            $this->db->where('m_dokterruangan.poliruangan_id', $poliruangan_id);
//        }
        $this->db->where('m_dokter.kelompokdokter_id <>', KELOMPOK_PERAWAT);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_dokter_anastesi(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    public function get_perawat(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_PERAWAT); //Perawat
        $query = $this->db->get();

        return $query->result();
    }

    function get_data_print($pendaftaran_id){

        $this->db->select('*, CONCAT(nama_asuransi , IF(nama_asuransi2="" && nama_asuransi ="" || nama_asuransi2=""," " , ",") , nama_asuransi2 ) AS asuransi');
        $this->db->from('t_pendaftaran');
        $this->db->join('m_pasien','m_pasien.pasien_id = t_pendaftaran.pasien_id','left');
        $this->db->join('m_propinsi','m_propinsi.propinsi_id = m_pasien.propinsi_id','left');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id','left');
        $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id','left');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id','left');
        $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
        $this->db->join('m_penanggungjawab','m_penanggungjawab.penanggungjawab_id = t_pendaftaran.penanggungjawab_id');
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_pendaftaran.dokter_id');
        $this->db->join('t_pembayaran','t_pembayaran.pembayaran_id = t_pendaftaran.pembayaran_id');
        $this->db->join('m_instalasi','m_instalasi.instalasi_id = t_pendaftaran.instalasi_id');
        $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
        $this->db->join('m_bahasa','m_bahasa.bahasa_id = m_pasien.bahasa_id','left');
        $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');
        $this->db->where('t_pendaftaran.pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();


        return $query->row();
    }

    function get_jam_dokter($dokter_id,$tgl_reservasi){
        $this->db->select('NAME_DOKTER, hari, jam_mulai, jumlah_pasien');
        $this->db->where('nama_branch = "'. $this->session->tempdata('data_session')[1].'"' ); 
        $query = $this->db->get_where('v_jadwal_dokter', array('id_M_DOKTER' => $dokter_id, 'hari' => $tgl_reservasi));
        
        return $query->result();
    }

    public function insert_notif($data){
        $this->db->insert('t_notif',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    function get_warganegara() {   
        $this->db->select('*');   
        $this->db->from('m_kewarganegaraan');  
        $query = $this->db->get();
        return $query->result(); 
    }
    function get_hotel(){   
        $this->db->select('*');   
        $this->db->from('m_hotel');  
        $query = $this->db->get();
        return $query->result(); 
    }
    function insert_hotel($data){  
        $this->db->insert('m_hotel',$data);
        // print_r($this->db->error());die;
        $id = $this->db->insert_id();
        return $id;
    }
    function get_hotel_by_id($id) {
        $this->db->select('*');   
        $this->db->from('m_hotel');  
        $this->db->where("id_hotel", $id);
        $query = $this->db->get();
        return $query->row(); 
    }

    function get_pembayaran($id) {
        $this->db->where("pembayaran_id", $id);
        return $this->db->get("t_pembayaran")->row();
    }

    function get_reservasi_by_no_rm($no_rm) {
        $this->db->select('*');
        $this->db->from('t_reservasi');
        $this->db->where("no_rekam_medis", $no_rm);
        $query = $this->db->get();

        return $query->row();
    }

    function reservasi() {
        $this->db->select('*');
        $this->db->from('t_reservasi');
        $query = $this->db->get();

        return $query->result();
    }

    function pasien($name) {
        $this->db->select('*');
        $this->db->from('m_pasien');
        $this->db->where("pasien_nama", $name);
        $query = $this->db->get();

        return $query->row();
    }

    public function update_reservasi($pasien_id, $id) {
        $data['pasien_id'] = $pasien_id;
        $this->db->where("reservasi_id", $id);
        $update = $this->db->update('t_reservasi', $data);
        return $update;
    }
}
