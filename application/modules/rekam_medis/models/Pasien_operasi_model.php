<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_operasi_model extends CI_Model {
    var $column = array('no_pendaftaran','tgl_masukadmisi','poliruangan_id','no_rekam_medis','pasien_nama','jenis_kelamin','umur','type_pembayaran','kelaspelayanan_nama','poli_ruangan','no_kamar','statusrawat');
    var $order = array('tgl_masukadmisi' => 'DESC');
     var $column1 = array('kode_diagnosa','nama_diagnosa','kelompokdiagnosa_id','kelompokdiagnosa_nama');
    var $order1  = array('kelompokdiagnosa_id' => 'ASC');    
    public function __construct(){
        parent::__construct();
    }

   
    public function get_pasienri_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien, $dokter_id, $nama_dokter, $nama_klinik,$no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran, $lokasi){
    // public function get_pasienrj_list(){  
        $this->db->from('v_pasien_operasi');
        $this->db->where('tgl_masukadmisi BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poli_ruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($nama_klinik)){
            $this->db->like('nama_poliruangan', $nama_klinik);
        }
        if(!empty($no_rekam_medis)){
            $this->db->where('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
        if(!empty($no_bpjs)){
            $this->db->where('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->where('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($lokasi)){
            $this->db->like('lokasi', $lokasi);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    function count_all($tgl_awal, $tgl_akhir, $poliklinik, $status, $nama_pasien, $dokter_id, $nama_dokter, $nama_klinik,$no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran, $lokasi){
        $this->db->from('v_pasien_operasi');
        $this->db->where('tgl_masukadmisi BETWEEN "'.  date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');     
        if(!empty($poliklinik)){
            $this->db->where('poli_ruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($nama_klinik)){
            $this->db->like('nama_poliruangan', $nama_klinik);
        }
        if(!empty($no_rekam_medis)){
            $this->db->where('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
        if(!empty($no_bpjs)){
            $this->db->where('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->where('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($lokasi)){
            $this->db->like('lokasi', $lokasi);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }
        
        return $this->db->count_all_results();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_pasien_operasi', array('pendaftaran_id' => $pendaftaran_id), 1, 0);

        return $query->row();
    }

    public function get_dokter_list(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('instalasi_id', 3); //rawat inap
        $this->db->group_by('id_M_DOKTER');
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_dokter_anastesi(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_perawat(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_PERAWAT); //Perawat
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_diagnosa_pasien_list($pendaftaran_id){
        $this->db->from('t_diagnosapasien');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function insert_diagnosapasien($pendaftaran_id, $data=array()){
        $insert = $this->db->insert('t_diagnosapasien',$data);

        return $insert;
    }

    public function update_to_sudahperiksa($id){
        $data = array(
            'status_periksa' => 'SUDAH PERIKSA'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    public function delete_periksa($pendaftaran_id){
      $datas = array(
          'status_periksa' => 'BATAL RAWAT'
      );
      $dataadmi = array(
          'statusrawat' => 'BATAL RAWAT'
      );
      $update = $this->db->update('t_pendaftaran', $datas, array('pendaftaran_id' => $pendaftaran_id));
      $update = $this->db->update('t_pasienadmisi', $dataadmi, array('pendaftaran_id' => $pendaftaran_id));
      return $update;
    }
    public function get_tindakan_pasien_list($pendaftaran_id){
        $this->db->select('t_tindakanpasien.*, m_daftartindakan.daftartindakan_nama');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_daftartindakan','m_tariftindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        // $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    public function get_tindakan($kelaspelayanan_id,$is_bpjs){
        $this->db->select('m_tariftindakan.*, m_daftartindakan.daftartindakan_nama');
        $this->db->from('m_tariftindakan');
        $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('m_tariftindakan.kelaspelayanan_id',$kelaspelayanan_id);
        //$this->db->where('m_daftartindakan.kelompoktindakan_id',PELAYANAN_RAWAT_INAP);
        $this->db->where('m_tariftindakan.komponentarif_id','1');
        if(strtoupper($is_bpjs) == strtoupper("BPJS")){
            $this->db->where('m_tariftindakan.bpjs_non_bpjs','BPJS');
        }else{
            $this->db->where('m_tariftindakan.bpjs_non_bpjs','Non BPJS');
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tarif_tindakan($tariftindakan_id){
        $query = $this->db->get_where('m_tariftindakan', array('tariftindakan_id' => $tariftindakan_id), 1, 0);

        return $query->row();
    }

    public function insert_tindakanpasien($pendaftaran_id, $data=array()){
        $insert = $this->db->insert('t_tindakanpasien',$data);

        return $insert;
    }
    public function delete_diagnosa($id){
        $delete = $this->db->delete('t_diagnosapasien', array('diagnosapasien_id' => $id));

        return $delete;
    }
    public function delete_tindakan($id){
        $delete = $this->db->delete('t_tindakanpasien', array('tindakanpasien_id' => $id));

        return $delete;
    }
     public function get_resep_pasien_list($pendaftaran_id){
        $this->db->select('t_resepturpasien.*, m_obat.nama_obat');
        $this->db->from('t_resepturpasien');
        $this->db->join('m_obat','m_obat.obat_id = t_resepturpasien.obat_id');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('v_stokobatalkes');
        $this->db->like('nama_obat', $val);
        $this->db->where('qtystok !=', 0);
        $query = $this->db->get();

        return $query->result();
    }
    public function insert_reseppasien($data=array()){
        $this->db->insert('t_resepturpasien',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function insert_stokobatalkes_keluar($data=array()){
        $stok = $this->db->insert('t_stokobat',$data);
        
        return $stok;
    }
    
    public function delete_resep($id){
        $delete = $this->db->delete('t_resepturpasien', array('reseptur_id' => $id));

        return $delete;
    }
    
    public function delete_stok_resep($id){
        $stok = $this->db->delete('t_stokobat', array('penjualandetail_id' => $id));
        
        return $stok;
    }
    
    public function pulangkan_pasien($pendaftaran_id){
        $lama_rawat = $this->hitung_lama_rawat($pendaftaran_id);
        $datas = array(
            'status_periksa' => 'DIPULANGKAN'
        );
        
        $dataadmi = array(
            'statusrawat' => 'DIPULANGKAN',
            'tgl_pulang' => date('Y-m-d H:i:s'),
            'lama_rawat_hari' => $lama_rawat
        );
        
        $datakamar = array(
            'status_bed' => '0'
        );
        
        $pasien_admisi = $this->get_pasien_admisi($pendaftaran_id);
        $daftartindakan = JASA_KAMAR;
        $kelaspelayanan = $pasien_admisi->kelaspelayanan_id;
        $row_tind = $this->get_tarif_kamar($daftartindakan, $kelaspelayanan);
        $lama_kamar = $this->hitung_lama_rawat_kamar($pasien_admisi->pasienadmisi_id, $pasien_admisi->poli_ruangan_id, $pasien_admisi->kamarruangan_id);
        $totalharga = intval($lama_kamar)* intval($row_tind->harga_tindakan);
        
        $datamasukkmr = array(
            'tgl_keluarkamar' => date('Y-m-d H:i:s'),
            'lama_rawat_kamar' => $lama_kamar
        );
        
        $datatindakan = array(
            'pendaftaran_id' => $pendaftaran_id,
            'pasien_id' => $pasien_admisi->pasien_id,
            'tariftindakan_id' => $row_tind->tariftindakan_id,
            'jml_tindakan' => $lama_kamar,
            'total_harga_tindakan' => $totalharga,
            'is_cyto' => 0,
            'total_harga' => $totalharga,
            'dokter_id' => null,
            'tgl_tindakan' => date('Y-m-d'),
        );
        $this->insert_tindakanpasien($pendaftaran_id, $datatindakan);
        
        $res = false;
        $update1 = $this->db->update('t_pendaftaran', $datas, array('pendaftaran_id' => $pendaftaran_id));
        $update2 = $this->db->update('t_pasienadmisi', $dataadmi, array('pendaftaran_id' => $pendaftaran_id));
        $update3 = $this->update_kamar($datakamar, $pendaftaran_id);
        $update4 = $this->db->update('t_masukkamar', 
                                        $datamasukkmr, 
                                        array('pasienadmisi_id' => $pasien_admisi->pasienadmisi_id,
                                                'poliruangan_id' => $pasien_admisi->poli_ruangan_id,
                                                'kamarruangan_id' => $pasien_admisi->kamarruangan_id,
                                                'tgl_keluarkamar' => null,
                                                'lama_rawat_kamar' => null
                                        ));

        if($update1 && $update2 && $update3 && $update4){
            $res = true;
        }

        return $res;
    }
    
    public function hitung_lama_rawat($pendaftaran_id){
        $this->db->select('tgl_masukadmisi');
        $this->db->from('t_pasienadmisi');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        $date_from = $query->row();
        
        $dateFrom = $date_from->tgl_masukadmisi;
        $dateTo = time(); // or your date as well
        $dateFrom2 = strtotime($dateFrom);
        $datefrom21 = date('Y-m-d', $dateFrom2)."21:00:00";
        $datediff = $dateTo - $dateFrom2;

        if($dateFrom2 < strtotime($datefrom21)){
            $hari = floor($datediff/(60*60*24)) + 1;
        }else{
            $hari = floor($datediff/(60*60*24));
        }
//        $dateFrom = strtotime($dateFrom);
//        $datediff = $dateTo - $dateFrom;
//        $hari = floor($datediff/(60*60*24)) + 1;
        
        return $hari;
    }
    
    public function hitung_lama_rawat_kamar($pasienadmisi_id, $poliruangan_id, $kamarruangan_id){
        $this->db->select('tgl_masukkamar');
        $this->db->from('t_masukkamar');
        $this->db->where('pasienadmisi_id', $pasienadmisi_id);
        $this->db->where('poliruangan_id', $poliruangan_id);
        $this->db->where('kamarruangan_id', $kamarruangan_id);
        $this->db->where('tgl_keluarkamar', null);
        $this->db->where('lama_rawat_kamar', null);
        $query = $this->db->get();
        $date_from = $query->row();
        
        $dateFrom = $date_from->tgl_masukkamar;
        $dateTo = time(); // or your date as well
        $dateFrom2 = strtotime($dateFrom);
        $datefrom21 = date('Y-m-d', $dateFrom2)."21:00:00";
        $datediff = $dateTo - $dateFrom2;

        if($dateFrom2 < strtotime($datefrom21)){
            $hari = floor($datediff/(60*60*24)) + 1;
        }else{
            $hari = floor($datediff/(60*60*24));
        }
        
        return $hari;
    }
    
    public function update_kamar($data, $pendaftaran_id){
        $this->db->select('kamarruangan_id');
        $this->db->from('t_pasienadmisi');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        $kamar = $query->row();
        
        $kamarruangan_id = $kamar->kamarruangan_id;
        
        $update = $this->db->update('m_kamarruangan', $data, array('kamarruangan_id' => $kamarruangan_id));

        return $update;
    }
    
    public function get_tarif_kamar($daftartindakan, $kelaspelayanan){
        $query = $this->db->get_where('m_tariftindakan', array('kelaspelayanan_id' => $kelaspelayanan, 'daftartindakan_id' => $daftartindakan), 1, 0);

        return $query->row();
    }
    
    public function get_pasien_admisi($pendaftaran_id){
        $query = $this->db->get_where('t_pasienadmisi', array('pendaftaran_id' => $pendaftaran_id), 1, 0);

        return $query->row();
    }
    
    public function update_masukkamar($datamasukkmr, $pasienadmisi_id, $poliruangan_id, $kamarruangan_id){
        $update = $this->db->update('t_masukkamar', 
                                        $datamasukkmr, 
                                        array('pasienadmisi_id' => $pasienadmisi_id,
                                                'poliruangan_id' => $poliruangan_id,
                                                'kamarruangan_id' => $kamarruangan_id,
                                                'tgl_keluarkamar' => null,
                                                'lama_rawat_kamar' => null
                                        ));
        return $update;
    }
    
    public function insert_masukkamar($data){
        $insert = $this->db->insert('t_masukkamar',$data);
        return $insert;
    }
    
    public function update_pasienadmisi($pendaftaran_id, $data){
        $update2 = $this->db->update('t_pasienadmisi', $data, array('pendaftaran_id' => $pendaftaran_id));
        
        return $update2;
    }
    
    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_INAP));
        
        return $query->result();
    }
    
    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function get_kamar_aktif($ruangan_id, $kelasruangan){
        $this->db->order_by('no_kamar', 'ASC');
        $query = $this->db->get_where('m_kamarruangan', array('poliruangan_id' => $ruangan_id, 'kelaspelayanan_id' => $kelasruangan, 'status_aktif' => '1'));
        
        return $query->result();
    }
    
    function get_dokter($poliruangan_id){
        $this->db->select('m_dokter.id_M_DOKTER, m_dokter.NAME_DOKTER');
        $this->db->from('m_dokter');
        $this->db->join('m_dokterruangan','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
        $this->db->where('m_dokterruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function get_list_penunjang(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => 4));

        return $query->result();
    } 
   function get_list_diagnosa(){
        $this->db->from('v_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            } 
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    } 
    
     public function count_list_diagnosa(){
        $this->db->from('v_diagnosa'); 
        return $this->db->count_all_results();
    }


    public function count_list_filtered_diagnosa(){
        $this->db->from('v_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            } 
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    } 
    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }
    
    public function insert_penunjang($data){
        $insert = $this->db->insert('t_pasienmasukpenunjang',$data);
        return $insert;
    }
    
    public function update_to_penunjang($id){
        $status = false;
        $data = array(
            'status_periksa' => 'SEDANG DI PENUNJANG'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));
        
        $data2 = array(
            'statusrawat' => 'SEDANG DI PENUNJANG'
        );
        $update2 = $this->db->update('t_pasienadmisi', $data2, array('pendaftaran_id' => $id));
        
        if($update && $update2){
            $status = true;
        }else{
            $status = false;
        }
        
        return $status;
    }
}
