    <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasienrd_model extends CI_Model {
    var $column = array('no_pendaftaran','tgl_pendaftaran','poliruangan_id','no_rekam_medis','no_bpjs','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa','id_M_DOKTER');
    var $order = array('tgl_pendaftaran' => 'ASC');
    
    var $column1 = array('kode_diagnosa','nama_diagnosa','kelompokdiagnosa_id','kelompokdiagnosa_nama');
    var $order1  = array('kelompokdiagnosa_id' => 'ASC');

    var $column4 = array('kode_diagnosa');
    var $order4  = array('kode_diagnosa' => 'ASC');

    public function __construct(){
        parent::__construct();
    }
    public function delete_periksa($pendaftaran_id){
      $datas = array(
          'statusperiksa' => 'Batal Periksa',
          'status_pasien' => 0
      );
      $update = $this->db->update('t_pendaftaran', $datas, array('pendaftaran_id' => $pendaftaran_id));
      return $update;
    }
    public function get_pasienrd_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $nama_dokter, $pasien_alamat,$no_bpjs,$no_pendaftaran){
        $this->db->from('v_rm_pasien_rd');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');


        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($nama_dokter)){
            $this->db->like('NAME_DOKTER', $nama_dokter);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    public function count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs, $no_pendaftaran){
        $this->db->from('v_rm_pasien_rd');
         $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        
        return $this->db->count_all_results();
    }


    public function count_all_list_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $no_bpjs, $no_pendaftaran){
        $this->db->from('v_rm_pasien_rd');
        
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
   
    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_rm_pasien_rd', array('pendaftaran_id' => $pendaftaran_id), 1, 0);

        return $query->row();
    }
    public function get_dokter_list(){
        $this->db->from('m_dokter');
        $query = $this->db->get();
    
        return $query->result();
    }
    public function get_dokter_list_ruangan(){
        $this->db->where('instalasi_id',INSTALASI_RAWAT_DARURAT);
        $this->db->from('v_dokterruangan');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }
    public function get_diagnosa_pasien_list($pendaftaran_id){
        $this->db->from('t_diagnosapasien');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    public function get_tindakan_pasien_list($pendaftaran_id){
        $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama'); 
        $this->db->from('t_tindakanpasien'); 
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);   
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    public function get_tindakan($kelaspelayanan_id){
        $this->db->from('m_tindakan');  
        $this->db->where('kelaspelayanan_id',$kelaspelayanan_id);    
        $this->db->where('kelompoktindakan_id',3);    
        
        $query = $this->db->get();

        return $query->result();
    }
    public function get_tarif_tindakan($tariftindakan_id){
             $query = $this->db->get_where('m_tariftindakan', array('tariftindakan_id' => $tariftindakan_id), 1, 0);

        return $query->row();
    }
    public function insert_diagnosapasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_diagnosapasien',$data);

        return $insert;
    }

    public function update_diagnosapasien($data, $diagnosapasien_id){
            $update = $this->db->update('t_diagnosapasien', $data, array('diagnosapasien_id' => $diagnosapasien_id));
            return $update;
        }

    public function update_to_sudahperiksa($id){
        $data = array(
            'status_periksa' => 'Sudah Periksa'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    public function insert_tindakanpasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_tindakanpasien',$data);

        return $insert;
    }
    public function delete_diagnosa($id){
        $delete = $this->db->delete('t_diagnosapasien', array('diagnosapasien_id' => $id));

        return $delete;
    }
    public function delete_tindakan($id){
        $delete = $this->db->delete('t_tindakanpasien', array('tindakanpasien_id' => $id));

        return $delete;
    }

    public function delete_icd9($id){
        $delete = $this->db->delete('t_tindakanpasien_icd9', array('tindakan_icd_id' => $id));

        return $delete;
    }

     public function get_resep_pasien_list($pendaftaran_id){
        $this->db->select('t_resepturpasien.*, m_obat.nama_obat');
        $this->db->from('t_resepturpasien');
        $this->db->join('m_obat','m_obat.obat_id = t_resepturpasien.obat_id');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_obat');
        $this->db->like('nama_obat', $val);
        $this->db->or_like('jenis_obat', $val);
        $query = $this->db->get();

        return $query->result();
    }
    public function insert_reseppasien($pendaftaran_id, $data=array()){
        $insert = $this->db->insert('t_resepturpasien',$data);

        return $insert;
    }
    public function delete_resep($id){
        $delete = $this->db->delete('t_resepturpasien', array('reseptur_id' => $id));

        return $delete;
    }
    
    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_DARURAT));
        
        return $query->result();
    }
    
    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function get_kamar_aktif($ruangan_id, $kelasruangan){
        $this->db->order_by('no_kamar', 'ASC');
        $query = $this->db->get_where('m_kamarruangan', array('poliruangan_id' => $ruangan_id, 'kelaspelayanan_id' => $kelasruangan, 'status_aktif' => '1'));
        
        return $query->result();
    }
    
    public function insert_admisi($data){
        $this->db->insert('t_pasienadmisi',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }
    
    public function insert_kamar($data){
        $insert = $this->db->insert('t_masukkamar',$data);
        return $insert;
    }
    
    public function update_kamar($data, $id){
        $update = $this->db->update('m_kamarruangan', $data, array('kamarruangan_id' => $id));

        return $update;
    }
    
    public function update_pendaftaran($data, $id){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

    function get_diagnosa_pasien_by_id($id){
            $this->db->from('t_diagnosapasien');
            $this->db->where('diagnosapasien_id =', $id);
            $query = $this->db->get();

            return $query->result();
    }


    // start diagnosa icd 10
    function get_list_diagnosa(){
        $this->db->from('v_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }



    public function count_list_diagnosa(){
        $this->db->from('v_diagnosa');
        return $this->db->count_all_results();
    }


    public function count_list_filtered_diagnosa(){
        $this->db->from('v_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_diagnosa_by_id($id){
            $this->db->from('v_diagnosa');
            $this->db->where('diagnosa_id =', $id);
            $query = $this->db->get();

            return $query->result();
    }

    //end diagnosa icd 10



    // start icd 9
    function get_list_icd_9(){
        $this->db->from('m_icd_9_cm');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column4 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column4[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order4)){
            $order4 = $this->order4;
            $this->db->order_by(key($order4), $order4[key($order4)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function count_list_icd_9(){
        $this->db->from('m_icd_9_cm');
        return $this->db->count_all_results();
    }

    public function count_list_filtered_icd_9(){
        $this->db->from('m_icd_9_cm');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column4 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column4[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order4)){
            $order4 = $this->order4;
            $this->db->order_by(key($order4), $order4[key($order4)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_icd9_by_id($id){
            $this->db->from('m_icd_9_cm');
            $this->db->where('diagnosa_id =', $id);
            $query = $this->db->get();

            return $query->result();
    }

    //end icd 9 


    // insert icd 9 pasien
    public function insert_icd9($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_tindakanpasien_icd9',$data);
        return $insert;
    }


    //update icd 9 pasien
    public function update_icd9($data, $tindakan_icd_id){
            $update = $this->db->update('t_tindakanpasien_icd9', $data, array('tindakan_icd_id' => $tindakan_icd_id));
            return $update;
        }

    // list icd 9 pemeriksaan pasien
    public function get_icd9_list($pendaftaran_id){
        $this->db->from('t_tindakanpasien_icd9');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    //get tindakan icd 9  
    function get_tindakan_icd_9_by_id($id){
        $this->db->from('t_tindakanpasien_icd9');
        $this->db->where('tindakan_icd_id =', $id);
        $query = $this->db->get();
        return $query->result();
    }

}
