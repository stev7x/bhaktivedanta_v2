<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Pendaftaran Paket Operasi</h4> </div>     
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <!-- <li><a href="#">Dashboard</a></li> -->
                            <li><a href="#">Rekam Medis</a></li>
                            <li class="active">Pendaftaran</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div> 
                <!-- .row -->
                <?php echo form_open('#',array('id' => 'fmCreatePendaftaran', 'data-toggle' => 'validator'))?>
                    <div class="col-sm-12"> 
                        <div class="white-box">
                         <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <h3 class="box-title m-b-0">Data Pasien</h3>
                                        <p class="text-muted m-b-30"> <!-- Bootstrap Form Validation --></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-5">
                                                     <button hidden class="btn btn-success " id="printKartu" ><i class="fa fa-print"></i> PRINT KARTU BEROBAT</button> 
                                                 </div> 
                                                <div class="col-sm-5">
                                                     <button hidden class="btn btn-info " id="printPendaftaran" ><i class="fa fa-print"></i> PRINT DATA PASIEN</button> 
                                                 </div> 
                                                 <div class="col-sm-2">
                                                      <a hidden href="<?php echo base_url() ?>rekam_medis/pendaftaran" class="btn btn-danger" type="button" id="reset">  
                                                     <i class="fa fa-repeat"></i> RESET
                                                     </a>
                                                 </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="pull-right col-sm-4 "> 
                                        <div class="row">
                                            
                                            <div class="form-group col-sm-12">
                                                <div class="input-group custom-search-form">
                                                      <input readonly type="text" class="form-control" placeholder="Cari Berdasarkan No.Rekam Medis ..."> <span class="input-group-btn"> 
                                                        <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_pasien" data-backdrop="static" data-keyboard="false"  type="button" onclick="dialogPasien()"> <i class="fa fa-bars"></i> </button>   
                                                    </span> 
                                                 </div>   
                                            </div>
                                        </div>
                                    </div>

                                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">      
                                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                                        <div >
                                            <p id="card_message"></p>
                                        </div>
                                    </div>

                            </div>
                                    <hr style="margin-top: -10px;padding-bottom: 18px;">
                        </div>                                    
                    </div>
                            <div class="row">
                            
                            <div class="col-sm-6"> 
                                <!-- <form data-toggle="validator"> -->
                                     
                                    <div class="form-group" style="margin-top: -29px" >
                                        <label for="inputName1" class="control-label">No.Referensi Rekam Medis </label>
                                        <div class="input-group custom-search-form">
                                              <input readonly type="text" id="no_rm_ref" name="no_rm_ref" class="form-control" placeholder="Cari No Referensi Rekam Medis ..."> <span class="input-group-btn">  
                                                <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_ortu" data-backdrop="static" data-keyboard="false"  type="button" onclick="dialogOrtu()"> <i class="fa fa-bars"></i> </button>    
                                            </span> 
                                         </div>
                                    </div>
                                    <div class="form-group" >
                                        <label for="inputName1" class="control-label">No.Rekam Medis <span style="color: red;">*</span></label>
                                        <input id="no_rm" type="text" name="no_rm" class="validate form-control" value="<?php echo $next_no_rm; ?>" placeholder="No.Rekam Medis" readonly>
                                    </div>
                                    <div> 
                                        <div class="row"> 
                                            <div class="form-group col-sm-6">
                                                <label for="inputTypeID" class="control-label">Type Identitas<span style="color:red">*</span></label>
                                                <select id="type_id" name="type_id" class="form-control">
                                                    <option value="" disabled selected>Type ID</option>
                                                    <?php
                                                        foreach($list_type_identitas as $list){
                                                            echo "<option value='".$list."'>".$list."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                            <label for="inputPassword" class="control-label">No.Identitas<span style="color:red">*</span></label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "16" class="form-control no" placeholder="No.Identitas" name="no_identitas" id="no_identitas" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-top: -10px">
                                        <label for="no_bpjs" class="control-label">No.BPJS</label>
                                        <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "13" class="form-control no" id="no_bpjs" name="no_bpjs" placeholder="No.BPJS">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    


                                    <div class="form-group">
                                        <label for="nama_pasien" class="control-label">Nama Lengkap Pasien<span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" id="nama_pasien" name="nama_pasien" placeholder="Nama Lengkap Pasien" data-error="Please fill out this field." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group"> 
                                        <label for="nama_pasien" class="control-label">Nama Panggilan<span style="color: red;">*</span></label> 
                                        <input type="text" class="form-control" id="nama_panggilan" name="nama_panggilan" placeholder="Nama Panggilan" data-error="Please fill out this field." required>
                                        <div class="help-block with-errors"></div> 
                                    </div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Jenis Kelamin<span style="color: red;">*</span></label>
                                         <select class="form-control selectpicker" id="jenis_kelamin" name="jenis_kelamin">
                                            <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                            <?php
                                                foreach($list_jenis_kelamin as $list){
                                                    echo "<option value='".$list."'>".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tempat_lahir" class="control-label">Tempat Lahir<span style="color: red;">*</span></label>
                                        <input id="tempat_lahir" type="text" name="tempat_lahir" class="form-control" placeholder="Kota/Kabupaten Kelahiran" data-error="Please fill out this field." required>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="tanggal_lahir" class="control-label">Tanggal Lahir<span style="color:red">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_lahir" id="tgl_lahir" type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy" onchange="setUmur()"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div> 
                                            <div class="form-group col-sm-6">
                                                <label for="inputPassword" class="control-label">Umur<span style="color:red">*</span></label>
                                                <input type="text" name="umur" id="umur" class="form-control" placeholder="Umur" readonly required> 
                                            </div>
                                        </div>
                                    </div>
                                    

                                <div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Alamat Jalan<span style="color: red;">*</span></label>
                                        <textarea id="alamat" name="alamat" data-error="Please fill out this field." class="form-control" required></textarea> 
                                        <div class="help-block with-errors"></div>

                                    </div>  
                                </div>

                                <div class="row" style="margin-top: -12px">
                                    <div class="form-group col-sm-6" >
                                        <label for="inputPassword" class="control-label">Provinsi<span style="color: red;">*</span></label>          
                                        <select class="form-control select2" id="propinsi" name="propinsi" onchange="getKabupaten()">
                                            <!-- <option>Select</option> -->
                                            <option value="" disabled selected>Pilih Provinsi</option>
                                            <?php
                                            $list_propinsi = $this->Pendaftaran_operasi_model->get_propinsi();

                                            foreach($list_propinsi as $list){
                                                
                                                if ($list->propinsi_id == 15) {
                                                    echo "<option value='15' selected >JAWA TIMUR</option>";
                                                    
                                                } else{
                                                    echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";

                                                }
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group col-sm-6">
                                    <label for="inputPassword" class="control-label">Kabupaten<span style="color: red;">*</span></label>
                                        <select class="form-control select2" id="kabupaten" name="kabupaten" onchange="getKecamatan()">
                                            <option value=""  selected>Pilih Kota/Kabupaten</option>
                                            <?php
                                            $list_kabupaten = $this->Pendaftaran_operasi_model->get_kabupaten(15);

                                            foreach($list_kabupaten as $list){

                                                // if ($list->kabupaten_id == 229) {
                                                //    echo "<option value='229' selected id='bangka'>BANGKALAN</option>";
                                                    
                                                // }else{
                                                //     echo "<option value='".$list->kabupaten_id."'>".$list->kabupaten_nama."</option>";
                                                // }
                                                echo "<option value='".$list->kabupaten_id."'>".$list->kabupaten_nama."</option>";
                                            }
                                            ?>
                                            
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="inputPassword" class="control-label">Kecamatan<span style="color: red;">*</span></label>   
                                            <select class="form-control select2" id="kecamatan" name="kecamatan" onchange="getKelurahan()">
                                                <option value="" selected>Pilih Kecamatan</option>
                                            </select> 
                                        </div>
                                        <div class="form-group col-sm-6">
                                        <label for="inputPassword" class="control-label">Kelurahan<span style="color: red;">*</span></label>
                                            <select id="kelurahan" name="kelurahan" class="form-control select2">
                                                <option value="" selected>Pilih Kelurahan</option>
                                                
                                                
                                            </select> 
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: -30px">
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Alamat Tinggal<span style="color: red;">*</span></label>
                                        <textarea id="alamat_domisili" name="alamat_domisili" id="alamat_domisili" data-error="Please fill out this field." class="form-control" required readonly></textarea> 
                                        <input type="checkbox" id="cek_alamat" ><span style="font-size:10px; color:red;"> * Jika alamat berbeda dengan tempat tinggal</span>                                        
                                        <div class="help-block with-errors"></div>

                                    </div>  
                                </div>

                                <div class="row" style="margin-top: -12px">
                                    <div class="form-group col-sm-6" >
                                        <label for="inputPassword" class="control-label">Provinsi<span style="color: red;">*</span></label>          
                                        <select class="form-control select2" id="provinsi_tinggal" name="provinsi_tinggal" onchange="getKabupatenTinggal()">
                                            <!-- <option>Select</option> -->
                                            <option value="" disabled selected>Pilih Provinsi</option>
                                            <?php
                                            $list_propinsi = $this->Pendaftaran_operasi_model->get_propinsi();

                                            foreach($list_propinsi as $list){
                                                
                                                if ($list->propinsi_id == 15) {
                                                    echo "<option value='15' selected >JAWA TIMUR</option>";
                                                    
                                                } else{
                                                    echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";

                                                }
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group col-sm-6">
                                    <label for="inputPassword" class="control-label">Kota/Kabupaten<span style="color: red;">*</span></label>
                                        <select class="form-control select2" id="kabupaten_tinggal" name="kabupaten_tinggal" onchange="getKecamatanTinggal()">
                                            <option value=""  selected>Pilih Kota/Kabupaten</option>
                                            <?php
                                            $list_kabupaten = $this->Pendaftaran_operasi_model->get_kabupaten(15);

                                            foreach($list_kabupaten as $list){

                                                // if ($list->kabupaten_id == 229) {
                                                //    echo "<option value='229' selected id='bangka'>BANGKALAN</option>";
                                                    
                                                // }else{
                                                //     echo "<option value='".$list->kabupaten_id."'>".$list->kabupaten_nama."</option>";
                                                // }
                                                echo "<option value='".$list->kabupaten_id."'>".$list->kabupaten_nama."</option>";
                                            }
                                            ?>
                                            
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="inputPassword" class="control-label">Kecamatan<span style="color: red;">*</span></label>   
                                            <select class="form-control select2" id="kecamatan_tinggal" name="kecamatan_tinggal" onchange="getKelurahanTinggal()">
                                                <option value="" selected>Pilih Kecamatan</option>
                                            </select> 
                                        </div>
                                        <div class="form-group col-sm-6">
                                        <label for="inputPassword" class="control-label">Kelurahan<span style="color: red;">*</span></label>
                                            <select id="kelurahan_tinggal" name="kelurahan_tinggal" class="form-control select2">
                                                <option value="" selected>Pilih Kelurahan</option>
                                                
                                                
                                            </select> 
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6">  
                                <!-- <form data-toggle="validator"> -->
                                <div class="form-group" style="margin-top: -30px" > 
                                    <!-- <br><br><h3 class="box-title m-b-0">Data Keluarga</h3><br> -->
                                        <label for="textarea" class="control-label">Status Kawin<span style="color: red;">*</span></label>   
                                         <select class="form-control selectpicker status_kawin" onclick="cekStatusKawin()"  name="status_kawin">           
                                            <option value="" disabled selected>Pilih Status Kawin</option>
                                            <?php
                                                foreach($list_status_kawin as $list){
                                                    echo "<option value='".$list."'>".$list."</option>";
                                                }
                                            ?>
                                        </select>   
                                        <!-- <button type="button" onclick="cekStatusKawin()" id="statuskawin">tes</button> -->
                                    </div>    
  
                                    <div class="Suami_istri" style="display: none;">     
                                        <div class="form-group">  
                                            <label for="nama" class="control-label">Nama Suami<span style="color:red">*</span></label>
                                            <input type="email" class="form-control" name="nama_suami_istri" id="nama_suami_istri" placeholder="Nama Suami" data-error="Please fill out this field." >  
                                            <!-- <div class="help-block with-errors"></div> -->
                                        </div>
                                        <div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                <label for="tgl_lahir" class="control-label">Tanggal Lahir</label>
                                                <div class="input-group">
                                                    <input name="tgl_suami_istri" id="tgl_suami_istri" type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy" onchange="setUmurSustri()"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                                    
                                                </div>
                                                <div class="form-group col-sm-6">
                                                
                                                <label for="umur" class="control-label">Umur</label>
                                                    <input id="umur_suami_istri" name="umur_suami_istri" type="text" class="form-control" readonly placeholder="Umur">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">No.Telp rumah</label>
                                        <input type="number" class="form-control no" id="no_tlp_rmh" name="no_tlp_rmh" placeholder="No.Telp rumah"> 
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label">No.Ponsel/Mobile<span style="color:red">*</span></label>
                                        <input type="number" class="form-control no" id="no_mobile" name="no_mobile" placeholder="No.Ponsel/Mobile" data-error="Please fill out this field." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Pekerjaan Pasien</label>
                                        <select id="pekerjaan" name="pekerjaan" class="form-control select2">
                                            <option value="" selected>Pilih Pekerjaan</option>
                                            <!-- <?php
                                                // $list_pekerjaan = $this->Pendaftaran_operasi_model->get_pekerjaan();

                                                // foreach($list_pekerjaan as $list){
                                                //     echo "<option value='".$list->pekerjaan_id."'>".$list->nama_pekerjaan."</option>";
                                                // }
                                            ?> -->  
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Warga Negara</label>
                                        <select id="warga_negara" name="warga_negara" class="form-control selectpicker">
                                            <option value="" disabled selected>Pilih Warga Negara</option>
                                            <?php
                                                foreach($list_warga_negara as $list){
                                                    echo "<option value='".$list."'>".$list."</option>";
                                                }
                                            ?>            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Agama</label>
                                        <select id="agama" name="agama" class="form-control selectpicker">
                                            <option value="" disabled selected>Pilih Agama</option>
                                            <?php
                                            $list_agama = $this->Pendaftaran_operasi_model->get_agama();
                                            foreach($list_agama as $list){
                                                echo "<option value='".$list->agama_id."'>".$list->agama_nama."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
 
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Bahasa</label>
                                        <select id="bahasa" name="bahasa" class="form-control select2">
                                            <option value="" selected>Pilih Bahasa</option>
                                            <?php
                                            $list_bahasa = $this->Pendaftaran_operasi_model->get_bahasa();
                                            foreach($list_bahasa as $list){
                                                echo "<option value='".$list->bahasa_id."'>".$list->bahasa."</option>";
                                            }
                                            ?>  
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Pendidikan</label>
                                        <select id="pendidikan" name="pendidikan" class="form-control selectpicker">
                                        <option value="" disabled selected>Pilih Pendidikan</option>
                                            <?php
                                            $list_pendidikan = $this->Pendaftaran_operasi_model->get_pendidikan();
                                            foreach($list_pendidikan as $list){
                                                echo "<option value='".$list->pendidikan_id."'>".$list->pendidikan_nama."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nama Orang Tua<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" id="nama_orangtua" name="nama_orangtua" placeholder="Nama Orang Tua" data-error="Please fill out this field." required>  
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                    <div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="control-label">Tanggal Lahir</label>
                                                <div class="input-group"> 
                                                    <input name="tgl_lahir_ortu" id="tgl_lahir_ortu" type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy" onchange="setUmurOrtu()"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6"> 
                                                <label class="control-label">Umur</label>    
                                                <input type="text" id="umur_ortu" name="umur_ortu" class="form-control" placeholder="Umur" readonly required>    
                                            </div>
                                        </div>
                                    </div>  

                                     <div class="form-group">
                                        <label class="control-label">Catatan Khusus</label>
                                        <textarea id="textarea" name="catatan_khusus" class="form-control" ></textarea> <span class="help-block with-errors" style="color: red; font-size: 10px;">*Optional(jika diperlukan)</span>
                                     </div>                                   

                                    
                                <!-- </form> -->
                            </div>

                          </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Ringkasan Masuk</h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Pilih Paket Operasi<span style="color: red;"> *</span></label>
                                    <select id="paket_operasi" name="paket_operasi" class="form-control" onchange="getRuangan()"> 
                                        <option value="" disabled selected>Pilih Paket Operasi</option>
                                        <?php
                                        $list_paketoperasi = $this->Pendaftaran_operasi_model->get_paketoperasi();
                                        foreach($list_paketoperasi as $list){
                                            echo "<option value='".$list->paketoperasi_id."'>".$list->nama_paket."</option>";
                                        }
                                        ?>
                                    </select>      
                                    </div>
                                    <div class="form-group">
                                        <label>Poliklinik/Ruangan<span style="color: red;"> *</span></label>
                                        <select id="poli_ruangan" name="poli_ruangan" class="form-control" onchange="getKamar()">
                                            <option value="" disabled selected>Pilih Poli/Ruangan</option>

                                        </select>       
                                    </div>
                                    <div class="form-group">
                                        <label>Kamar<span style="color: red;"> *</span></label>
                                        <select id="kamarruangan" name="kamarruangan" class="form-control">
                                            <option value="" disabled selected>Pilih Kamar</option>

                                        </select>      
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Dokter Penanggung Jawab<span style="color: red;">*</span></label>
                                        <select id="dokter" name="dokter" class="form-control selectpicker">
                                            <option value="" disabled selected>Pilih Dokter</option>
                                        </select>        
                                    </div>
                                    <div class="form-group">
                                       <label>Dokter Anastesi<span style="color: red;"> *</span></label>
                                        <select id="dokter_anastesi" name="dokter_anastesi" class="form-control">
                                            <option value="" disabled selected>Pilih Dokter</option>
                                            <?php
                                            $list_anastesi = $this->Pendaftaran_operasi_model->get_dokter_anastesi();
                                            foreach($list_anastesi as $list){
                                                echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                            }
                                            ?>
                                        </select>       
                                    </div>
                                    <div class="form-group">
                                       <label>Perawat<span style="color: red;"> *</span></label>
                                        <select id="perawat" name="perawat" class="form-control">
                                            <option value="" disabled selected>Pilih Perawat</option>
                                            <?php
                                            $list_perawat = $this->Pendaftaran_operasi_model->get_perawat();
                                            foreach($list_perawat as $list){
                                                echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                            }
                                            ?>
                                        </select>       
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nama Perujuk</label>
                                        <div class="input-group custom-search-form">
                                            <input type="text" id="rm_namaperujuk" name="rm_namaperujuk" class="form-control" placeholder="Nama Perujuk" readonly>
                                            <span class="input-group-btn">  
                                                <button class="btn btn-info" title="Lihat Data Perujuk" data-toggle="modal" data-target="#modal_list_rujuk" data-backdrop="static" data-keyboard="false"  type="button" onclick="dialogPerujuk()"> <i class="fa fa-bars"></i> </button>   
                                            </span>   
                                        </div> 

                                        <!-- <input type="text" id="rm_namaperujuk" name="rm_namaperujuk" class="form-control" placeholder="Nama Perujuk" />         -->
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Alamat Perujuk</label>
                                        <textarea id="rm_alamatperujuk" name="rm_alamatperujuk" class="form-control" placeholder="Alamat Perujuk" readonly></textarea>         
                                    </div>
                                </div>


                            </div>
                        </div> 
                    </div>
                    

                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Data Penanggung Jawab</h3><br><br>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Nama Penanggung Jawab<span style="color: red;">*</span></label>   
                                        <input type="text" id="pj_nama" name="pj_nama" class="form-control" placeholder="Nama Penanggung Jawab" >       
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">No. Telp Rumah</label>   
                                        <input type="text" id="pj_no_tlp_rmh" name="pj_no_tlp_rmh" class="form-control" placeholder="No. Telp Rumah" >   
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">No. Ponsel/Mobile</label>
                                        <input type="text" id="pj_no_mobile" name="pj_no_mobile" class="form-control" placeholder="No. Ponsel Mobile" >
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Alamat</label>     
                                        <textarea id="pj_alamat" name="pj_alamat" class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Provinsi</label>   
                                        <select class="form-control select2" id="pj_propinsi" name="pj_propinsi" onchange="getKabupatenPj()">
                                            <option value="" disabled selected>Pilih Propinsi</option>
                                            <?php
                                            $list_propinsi_pj = $this->Pendaftaran_operasi_model->get_propinsi();
                                            foreach($list_propinsi_pj as $list){
                                                echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";
                                            }
                                            ?>
                                        </select>        
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Kota/Kabupaten</label>   
                                        <select id="pj_kabupaten" name="pj_kabupaten" class="form-control select2" onchange="getKecamatanPj()">
                                            <option value="" disabled selected>Pilih Kota/Kabupaten</option>
                                        </select>        
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Kecamatan</label>   
                                        <select id="pj_kecamatan" name="pj_kecamatan" class="form-control select2" onchange="getKelurahanPj()">
                                            <option value="" disabled selected>Pilih Kecamatan</option>
                                        </select>        
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Kelurahan/Desa</label>   
                                        <select class="form-control select2" id="pj_kelurahan" name="pj_kelurahan">
                                            <option value="" disabled selected>Pilih Kelurahan</option>
                                        </select>        
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Status Hubungan</label>   
                                        <select class="form-control" id="hubunganpj" name="hubunganpj">
                                            <option value="" disabled selected>Pilih Hubungan</option>
                                            <?php
                                                foreach($list_hubungan as $list){
                                                    echo "<option value='$list'>".$list."</option>";
                                                }
                                            ?>
                                        </select>        
                                    </div>
                                </div>


                            </div>
                        </div> 
                    </div>

                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">PEMBAYARAN</h3><br><br>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                                <label for="inputPassword" class="control-label">Type Pembayaran<span style="color: red;">*</span></label>   
                                                <select id="byr_type_bayar" name="byr_type_bayar" onclick="cekTypeBayar()" class="form-control" >             
                                                    <option value="" selected disabled>Pilih Type Bayar</option>   
                                                    <?php
                                                        foreach($list_type_bayar as $list){
                                                            echo "<option value=".$list.">".$list."</option>";
                                                        }
                                                    ?>
                                                </select>         
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Instansi Pekerjaan</label>   
                                        <input type="text" id="byr_instansi" name="byr_instansi" class="form-control" placeholder="Instansi Pekerjaan" readonly>   
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label">Nama Asuransi</label>   
                                        <input type="text" id="byr_namaasuransi" name="byr_namaasuransi" class="form-control" placeholder="Nama Asuransi" readonly>   
                                    </div>
                                    <div class="form-group" >
                                        <label for="inputPassword" class="control-label">Nomor Asuransi</label>   
                                        <input type="text"  id="byr_nomorasuransi" name="byr_nomorasuransi" class="form-control" placeholder="Nomor Asuransi" readonly>   
                                    </div>
                                </div>


                            </div>
                        </div>  
                    </div>
                    <div class="col-sm-12">
                        <div class="white-box">
                            <button type="button" class="btn btn-success col-sm-2" id="savePendaftaran"><i class="fa fa-floppy-o"></i> &nbsp;DAFTAR</button>
                        </div>
                    </div>
                    
                <!-- /.row -->
                
            </div>
            <!-- /.container-fluid -->
            <?php echo form_close(); ?>

 
<div id="modal_list_pasien" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large"> 
        <div class="modal-content">   
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Pasien</h2> </div>
            <div class="modal-body" style="height:500px;overflow: auto;"> 
                <table id="table_list_pasien" class="table table-striped dataTable no-footer table-responsive" cellspacing="0">
                    <thead>  
                        <tr>
                            <th>No</th>
                            <th>Rekam Medis</th>
                            <th>No. BPJS</th>
                            <th>Nama Pasien</th>
                            <th>Nama Panggilan</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Tempat Lahir</th>
                            <th>Umur</th>
                            <th>Status Kawin</th>
                            <th>Alamat Jalan</th>
                            <th>No Telp/HP</th>
                            <th>Agama</th>
                            <th>Pendidikan</th>
                            <th>Pekerjaan</th>
                            <th>Nama Orang Tua</th>
                            <th>Nama Suami/Istri</th>
                            <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Rekam Medis</th>
                            <th>No. BPJS</th>
                            <th>Nama Pasien</th>
                            <th>Nama Panggilan</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Tempat Lahir</th>
                            <th>Umur</th>
                            <th>Status Kawin</th>
                            <th>Alamat Jalan</th>
                            <th>No Telp/HP</th>
                            <th>Agama</th>
                            <th>Pendidikan</th>
                            <th>Pekerjaan</th>
                            <th>Nama Orang Tua</th>
                            <th>Nama Suami/Istri</th>
                            <th>Tindakan</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_list_ortu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large"> 
        <div class="modal-content">   
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Referensi Rekam Medis</h2> </div>   
            <div class="modal-body" style="height:500px;overflow: auto;"> 
                    <div class="table-responsive">      
                        <table id="table_list_ortu" class="table table-striped dataTable no-footer" cellspacing="0">
                            <thead> 
                                <tr>
                                    <th>No</th>
                                    <th>Rekam Medis</th>
                                    <th>Nama Pasien</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Rekam Medis</th>
                                    <th>Nama Pasien</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Tindakan</th>
                                </tr>
                            </tfoot>
                        </table>
                  </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_list_rujuk" class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large"> 
        <div class="modal-content">   
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Data Perujuk</h2> </div>
            <div class="modal-body" style="height:500px;overflow: auto;"> 
                    <div class="table-responsive">   
                        <table id="table_list_rujuk" class="table table-striped dataTable no-footer" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Perujuk</th>
                                    <th>Nama Perujuk</th>
                                    <th>Alamat Kantor</th>
                                    <th>No Telp/HP</th>
                                    <th>Nama Marketing</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                  </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="modal_print" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">     
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Print Pendaftaran</b></h2> </div>
                <div class="modal-body" style="background: #fafafa">

                    <div class="form-group col-md-push-3"> 
                        <button class="btn btn-info"><i class="fa fa-print"></i> Print Data Pasien</button>
                        <button class="btn btn-success"><i class="fa fa-print"></i> Print Kartu Berobat</button>
                    </div>

            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php $this->load->view('footer');?>  
 