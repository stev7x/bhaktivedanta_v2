<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print Identitas Pasien</title>

<style type="text/css">
    body{
      padding:0;
      margin:0;
    }
    table {
        border-collapse: collapse;
        /*text-align: center center;*/
      
      }
    body{
      margin: auto;
    }

    p{
      font-size: 8pt;
    }
    td{
      font-size: 12pt;

    }
    @page {
      size: portrait;
    }

    ol {
      padding: 4px;
      margin: 2px;
      margin-left: 14pt;
    }
    
  </style>

</head>

<body onload="window.print()">
    <div align="center">
      <img  src="<?= base_url() ?>/assets/plugins/images/header.png" alt="puri_bunda" style="max-width: 200px">  
    </div>
  
    <center>
      <p>
        Patitenget: Address. Jl. Patitenget IX Kerobokan-Kuta, Bali - Indonesia. Phone: +62 361 9343-811, +62 81 236212 - 023, Email: bvmedicalbali@gmail.com<br>
        Seminyak: Address. Jl. Camplung Tanduk 108, Seminyak-Kuta, Bali - Indonesia. Phone: +62 361 9343-911, +62 811 389 - 1600, Email: bvmedicalseminyak@gmail.com<br>
        Canggu: Address. Jl. Batu Bolong No. 11B, Canggu, Bali - Indonesia. Phone: +62 811 392 6699, +62 811 398 198, Email: bvmedicalcanggu@gmail.com<br>
        WhatsApp: +62 87 860 303 105<br>
        <a href="#">Website: www.bhaktivedantamedical.com</a>
      </p>
      <h3>PATIENT REGISTRATION FORM</h3>
    </center>

    <table width="100%" border="1" cellpadding="4">
      <tr>
        <th colspan="2" align="left">
          <b style="font-size: 14pt">PERSONAL INFORMATION</b>
        </th>
      </tr>
      <tr>
        <td>Name</td>
        <td><?= @$data_pasien->pasien_nama; ?></td>
      </tr>
      <tr>
        <td>Sex</td>
        <td>
          <?php 
            if (@$data_pasien->jenis_kelamin == "Laki-laki") {
              echo "
                <b>Male</b>
              ";
            }
            else {
              echo "
                <b>Female</b>
              ";
            }
          ?>
        </td>
      </tr>
      <tr>
        <td>Date of Birth</td>
        <td><?= @$data_pasien->tempat_lahir .", ". @$data_pasien->tanggal_lahir; ?> </td>
      </tr>
      <tr>
        <td>Nationality</td>
        <td><?= @$data_pasien->warga_negara; ?></td>
      </tr>
      <tr>
        <td>Are you holding Insurance</td>
        <td>
          <?php
            echo "Insurance Company : " . @$data_reservasi->asuransi1 . " (" . @$data_reservasi->no_asuransi1 . ")";
            echo "</br>";
            echo "Travel Insurance : " . @$data_reservasi->asuransi2 . " (" . @$data_reservasi->no_asuransi2 . ")";
//            if (@$data_pasien->cek_insurance != "") {
//              echo "
//                <b>Yes</b>, @$data_pasien->cek_insurance
//              ";
//            }
//            else {
//              echo "
//                <b>No</b>
//              ";
//            }
          ?>
        </td>
      </tr>
      <tr>
        <td>Email address</td>
        <td><?= @$data_pasien->email; ?></td>
      </tr>

      <tr>
        <th colspan="2" align="left">
          <b style="font-size: 12pt">Resident / Hotel in Bali</b>
        </th>
      </tr>
      <tr>
        <td>Hotel name</td>
        <td><?= @$hotel->nama_hotel; ?></td>
      </tr>
      <tr>
        <td>Room number</td>
        <td><?= @$data_pasien->room_number; ?></td>
      </tr>
      <tr>
        <td>Telephone</td>
        <td><?= @$hotel->no_telp==""?"-" : @$hotel->no_telp; ?></td>
      </tr>
      <tr>
        <td>Address</td>
        <td><?= @$hotel->alamat==""?"-" : @$hotel->alamat; ?></td>
      </tr>

      <tr>
        <th colspan="2" align="left">
          <b style="font-size: 12pt">Permanent address</b>
        </th>
      </tr>
      <tr>
        <td>Address</td>
        <td><?= @$data_pasien->pasien_alamat; ?></td>
      </tr>
      <tr>
        <td>Telephone</td>
        <td><?= @$data_pasien->tlp_selular; ?></td>
      </tr>

      <tr>
        <th colspan="2" align="left">
          <b style="font-size: 14pt">MEDICAL INFORMATION</b>
        </th>
      </tr>
      <tr>
        <td>Present complain</td>
        <td><?= @$data_pasien->present_complain; ?></td>
      </tr>
      <tr>
        <td>Date first complain</td>
        <td><?= @$data_pasien->date_first_complain; ?></td>
      </tr>
      <tr>
        <td>Date first consultation</td>
        <td><?= @$data_pasien->date_first_consultation; ?></td>
      </tr>
      <tr>
        <td>Allergy history</td>
        <td>
          <?php
            if (!empty(@$data_pasien->allergy_history)) {
              echo "
                <b>Yes</b>, for @$data_pasien->allergy_history
              ";
            }
            else {
              echo "
                <b>No</b>
              ";
            }
          ?>
        </td>
      </tr>
      <tr>
        <td>Pregnant</td>
        <td>
          <?php
            if (!empty(@$data_pasien->pregnant)) {
              echo "
                <b>Yes</b>, month @$data_pasien->pregnant
              ";
            }
            else {
              echo "
                <b>No</b>
              ";
            }
          ?>
        </td>
      </tr>
      <tr>
        <td>Current medication</td>
        <td>
          <?= @$data_pasien->current_medication; ?>
        </td>
      </tr>
      <tr>
        <td>Weight (kg)</td>
        <td><?= @$data_pasien->weight; ?></td>
      </tr>
      <tr>
        <td>Do you have any past medical history?</td>
        <td>Detail: <?= @$data_pasien->medical_history; ?></td>
      </tr>
      <tr>
        <td>Please note all important medical events:</td>
        <td>Detail: <?= @$data_pasien->medical_event; ?></td>
      </tr>
    </table>

</body>
</html>
  