<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row">    
        <div class="col-md-12">  
            <div class="panel panel-info1">       
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">   
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr> 
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->poli_ruangan; ?></td>
                                    </tr>
                                    <tr> 
                                        <td><b>Dokter Penanggung Jawab</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->umur; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr>
                                  
                                </table>
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <!-- <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly> --> 
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly> 
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->poli_ruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">  
                                <!-- <input type="hidden" id="pasienmasukpenunjang_id" name="pasienmasukpenunjang_id" value="<?php echo $list_pasien->pasienmasukpenunjang_id; ?>"> -->

                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  
    </div>


<!-- modal save diagnosa -->

    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
    <div class="col-md-12"> 
        <div class="row"> 
       
        <h3 style="width:100%;border-bottom: 1px solid #eeeeee;margin-bottom: 30px;margin-left: 10px;margin-right: 10px;padding-left: 5px">Pilih Dokter</h3> 
            
        </div> 
   </div> 

   <div class="col-md-12" >
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#tabs-diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Diagnosa</span></a></li> 
                <li role="presentation" class=" nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Tindakan</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#tabs-resep" class="nav-link" aria-controls="test" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Pembelian Resep</span></a></li>    
            </ul> 
            <!-- Tab panes -->  
            <div class="tab-content">  
                <div role="tabpanel" class="tab-pane active" id="tabs-diagnosa"> 
                    <table id="table_diagnosa_pasienri" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl. Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>
                                <th>Keterangan</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl. Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>
                                <th>Keterangan</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </tfoot>
                    </table>

                    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                     <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                        <div class ="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="card_message"></p>
                            </div>
                        </div>
                     <div class="form-group col-md-6">
                            <label>Dokter Anastesi</label>
                            <select id="dokter_anastesi" name="dokter_anastesi" class="form-control select2" > <span style="color: red;">*</span>
                                <option value=""  selected>Pilih Dokter</option>
                                <?php
                                $list_anastesi = $this->Pasien_operasi_model->get_dokter_anastesi();
                                foreach($list_anastesi as $list){
                                    if($list_pasien->dokteranastesi_id == $list->id_M_DOKTER){
                                        echo "<option value='".$list->id_M_DOKTER."' >".$list->NAME_DOKTER."</option>";
                                    }else{
                                        echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6"> 
                            <label>Perawat / Bidan</label> 
                            <select id="perawat" name="perawat" class="form-control select2" > <span style="color: red;">*</span>
                                <option value=""  selected>Pilih Perawat</option>
                                <?php
                                $list_perawat = $this->Pasien_operasi_model->get_perawat();
                                foreach($list_perawat as $list){
                                    if($list_pasien->dokterperawat_id == $list->id_M_DOKTER){
                                        echo "<option value='".$list->id_M_DOKTER."' >".$list->NAME_DOKTER."</option>";
                                    }else{
                                        echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                    }
                                }
                                ?>
                            </select> 
                        </div>
                        <div class="form-group col-md-4">   
                            <label class="control-label">
                            <span style="color: red;">*</span>Input Diagnosa</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_diagnosa" name="kode_diagnosa" placeholder="Kode Diagnosa"> 
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" title="Lihat List Diagnosa" data-toggle="modal" data-target="#modal_diagnosa" data-backdrop="static" data-keyboard="false"  onclick="dialogDiagnosa()"><i class="fa fa-list"></i></button>
                              </span>    
                            </div>
                        </div>     
                        <div class="form-group col-md-4">   
                            <label class="control-label">
                            <span style="color: red;">*</span>Nama Diagnosa</label>
                            <input id="nama_diagnosa" type="text" name="nama_diagnosa" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                        <div class="form-group col-md-4">   
                            <label class="control-label">
                            <span style="color: red;">*</span>Jenis Diagnosa</label>
                            <select id="jenis_diagnosa" name="jenis_diagnosa" class="form-control" >
                                <option value="" disabled selected>Pilih Jenis Diagnosa</option>
                                <option value="1">Diagnosa Utama</option>
                                <option value="2">Diagnosa Tambahan</option>
                            </select> 
                        </div>  
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-success" id="saveDiagnosa">
                                <i class="fa fa-floppy-o"></i> SIMPAN
                            </button>  
                        </div>

                        <?php echo form_close();?>  
                </div> 
<!-- end modal diagnosa -->

<!-- modal tindakan -->
                <div role="tabpanel" class="tab-pane" id="tabs-tindakan"> 

                    <table id="table_tindakan_pasien_operasi" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Cyto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Cyto</th>
                            </tr> 
                        </tfoot>
                    </table><br> 
                    
                   
                    <div class="clearfix"></div>
                </div>   
<!-- end modal tindakan -->

<!-- modal obat -->
                <div role="tabpanel" class="tab-pane" id="tabs-resep">
                    <table id="table_resep_pasien_operasi" class="table table-striped dataTable" cellspacing="0">
                        <thead>  
                            <tr> 
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <th>Satuan</th>
                                <th>Signa</th>
                            </tr>  
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="6">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <th>Satuan</th>
                                <th>Signa</th>
                            </tr> 
                        </tfoot>  
                    </table><br>

                </div>
                 
            </div>  
        </div>


   

</div>
<!-- end whitebox --> 


<div id="modal_reseptur" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Obat</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                        <div class="table">
                            <table id="table_list_obat" class="table table-striped dataTable" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Nama Obat</th>
                                        <th>satuan</th>
                                        <th>Kelompok Diagnosa</th>
                                        <th>Pilih</th>
                                        <th>Pilih</th>
                                        <th>Pilih</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div id="modal_diagnosa" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">  
                <div class="modal-content" >
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Diagnosa</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                        <div class="table">
                            <table id="table_diagnosa" class="table table-striped dataTable" cellspacing="0">
                                <thead> 
                                    <tr>
                                        <th>Kode Diagnosa</th>
                                        <th>Nama Diagnosa</th>
                                        <th>Kelompok Diagnosa</th>
                                        <th>Pilih</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_iframe');?>            
<script src="<?php echo base_url()?>assets/dist/js/pages/rekam_medis/pasien_operasi/periksa_pasienoperasi.js"></script>


</body>
 
</html>  



