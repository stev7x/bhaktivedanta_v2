<html>
<head>
	<title>Print</title>
</head>
	<style type="text/css">
		body{
			padding:0;
			margin:0;
		}
		table {
				border-collapse: collapse;
				/*text-align: center center;*/
			}
		
	</style>
<body onload="window.print()">
	<br><br><br>
	<table align="center" border="1" width="1000px">
		<tr>
			<td> 
				<table align="center" style="text-align: center">
					<tr>
						<td><h1>RSU MITRA DELIMA</h1></td>
					</tr>
					<tr>
						<td>Jl.Raya Bulupayung No.1B Krebet,</td>
					</tr>
					<tr>
						<td>Kec.Bululawang Kab.Malang 65171</td>
					</tr>
					<tr>
						<td>Telp. (0341) 805 183, Fax: 0341 - 805 159</td>
					</tr>
				</table>
			</td>

			<td colspan="2">
				<table align="center"  style="text-align: center">
					<tr>
						<td><h1>FORMULIR IDENTITAS PASIEN</h1></td>
					</tr>	
					<tr>
						<td>
							<table align="center" style="text-align: center; ">
								<tr>
									<td><b>No.Rekam Medis</b></td>
									<td>:</td>
									<td>
										<table border="1" width="200px">
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<p align="right">RM001</p>
			</td>
		</tr>

		<tr>
			<td>
				<table  align="center">
					<tr>
						<td colspan="3"><b>Identitas sesuai dengan Tanda Pengenal (KTP,SIM,dll)</b></td>
					</tr>
					<tr>
						<td colspan="3"><b><u>Data Pasien</u></b></td>
					</tr>
					<tr>
						<td>Nama Lengkap</td>
						<td>:</td>
						<td><?php echo $data_pasien->pasien_nama; ?></td>
					</tr>
					<tr>
						<td>Tmpt & Tgl Lahir</td>
						<td>:</td>
						<td><?php echo $data_pasien->tanggal_lahir; ?></td>
					</tr>
					<tr>
						<td>Umur</td>
						<td>:</td>
						<td><?php echo $data_pasien->umur; ?></td>
					</tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td><?php echo $data_pasien->jenis_kelamin; ?></td>
                    </tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td><?php echo $data_pasien->pasien_alamat; ?></td>
					</tr>
					<!-- <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<table>
								<tr>
									<td>RT:</td>
									<td>.........</td>
									<td style="padding-left: 5px">RW:</td>
									<td>..............</td>
									<td style="padding-left: 5px">Desa:</td>
									<td>...................</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<table>
								<tr>
									<td>Kec:</td>
									<td>..................</td>
									<td style="padding-left: 5px">Kab:</td>
									<td>.......................</td>
								</tr>
							</table>
						</td>
					</tr> -->
					<tr>
						<td>Telepon Rumah</td>
						<td>:</td>
						<td>
							<Table>
								<tr>
									<td><?php echo $data_pasien->tlp_rumah; ?></td>
									<td style="padding-left: 5px">HP</td>
									<td>:</td>
									<td><?php echo $data_pasien->tlp_selular; ?></td>
								</tr>
							</Table>
						</td>
					</tr>
					<tr>
						<td>Agama</td>
						<td>:</td>
						<td><?php echo $data_pasien->agama; ?>
							<!-- <table>
								<tr>
									<td><input type="checkbox" name="agama" />Islam </td>
									<td><input type="checkbox" name="agama" />Khatolik </td>
									<td><input type="checkbox" name="agama" />Kristen </td>
									<td><input type="checkbox" name="agama" />Hindu </td>
									<td><input type="checkbox" name="agama" />Budha </td>
								</tr>
							</table> -->
						</td>
					</tr>
					<!-- <tr>
						<td></td>
						<td></td>
						<td>.........................................................</td>
					</tr> -->
					<tr>
						<td>Suku</td>
						<td>:</td>
						<td><?php echo $data_pasien->suku; ?>
							<!-- <table>
								<tr>
									<td><input type="checkbox" name="suku" />Jawa </td>
									<td><input type="checkbox" name="suku" />Madura </td>
									<td><input type="checkbox" name="suku" />......................... </td>
								</tr>
							</table> -->
						</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>:</td>
						<td><?php echo $data_pasien->status_kawin; ?>
							<!-- <table>
								<tr>
									<td><input type="checkbox" name="status" />Kawin </td>
									<td><input type="checkbox" name="status" />Duda/Janda </td>
									<td><input type="checkbox" name="status" />Blm Kwn </td>
									<td><input type="checkbox" name="status" />Dbwh Umur </td>
								</tr>
							</table> -->
						</td>
					</tr>
					<tr>
						<td>Pendidikan</td>
						<td>:</td>
						<td><?php echo $data_pasien->pendidikan; ?>
							<!-- <table>
								<tr>
									<td><input type="checkbox" name="Pendidikan" />Blum/Tdk Sklh </td>
									<td><input type="checkbox" name="Pendidikan" />SD </td>
									<td><input type="checkbox" name="Pendidikan" />SMP </td>
									<td><input type="checkbox" name="Pendidikan" />SMA </td>
								</tr>
							</table> -->
						</td>
					</tr>
					<!-- <tr>
						<td></td>
						<td></td>
						<td>
							<table>
								<tr>
									<td><input type="checkbox" name="pendidikan" />Diploma</td>
									<td><input type="checkbox" name="pendidikan" />Sarjana</td>
									<td><input type="checkbox" name="pendidikan" />.....................</td>
								</tr>
							</table>
						</td>
					</tr> -->
					<tr>
						<td>Pekerjaan</td>
						<td>:</td>
						<td><?php echo $data_pasien->pekerjaan; ?></td>
					</tr>
					<tr>
						<td>Nama Keluarga</td>
						<td>:</td>
						<td><?php echo $data_pasien->status_hubungan; ?>
							<!-- <table>
								<tr>
									<td><input type="checkbox" name="keluarga">Orang Tua</td>
									<td><input type="checkbox" name="keluarga">Istri</td>
									<td><input type="checkbox" name="keluarga">Suami</td>
								</tr>
							</table> -->
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><?php echo $data_pasien->nama_keluarga; ?></td>
					</tr>

					<tr>
						<td>Gol.Darah</td>
						<td>:</td>
						<td><?php echo $data_pasien->gol_darah; ?>
							<table>
							<tr>
								<!-- <td><input type="checkbox" name="goldarah">A</td>
								<td><input type="checkbox" name="goldarah">B</td>
								<td><input type="checkbox" name="goldarah">AB</td>
								<td><input type="checkbox" name="goldarah">O</td> -->
								<td style="padding-left: 5px">Alergi:</td>
								<td style="padding-left: 5px"><?php echo $data_pasien->alergi; ?></td>
							</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>

			<td colspan="2">
				<table border="0" align="center" width="90%"  >
					<tr>
						<td></td>
					</tr>
					<tr>
						<td><b><u>Penanggung Jawab</u></b></td>
					</tr>
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td>..............................................................</td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td>..............................................................</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>..............................................................</td>
					</tr>
					<tr>
						<td>Telepon</td>
						<td>:</td>
						<td>
							<table>
								<tr>
									<td>.........................</td>
									<td>HP:</td>
									<td>.........................</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3"><b><u>Pembayaran</u></b></td>
					</tr>
					<tr>
						<td>Pembayaran</td>
						<td>:</td>
						<td>
							<table>
								<tr>
									<td><input type="checkbox" name="" />Sendiri</td>
									<td><input type="checkbox" name="" />Instansi</td>
									<td><input type="checkbox" name="" />................</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>Instansi Kerja</td>
						<td>:</td>
						<td>...........................................................</td>
					</tr>
					<tr>
						<td>Nama Asuransi</td>
						<td>:</td>
						<td>...........................................................</td>
					</tr>
					<tr>
						<td>No Asuransi</td>
						<td>:</td>
						<td>...........................................................</td>
					</tr>

					<tr>
						<td colspan="3"><b><u>Ringkasan Masuk</u></b></td>
					</tr>
					<tr>
						<td>Tanggal Datang</td>
						<td>:</td>
						<td>
							<table>
								<tr>
									<td>...............................</td>
									<td style="padding-left: 5px">Jam:</td>
									<td>...............................</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>Nama Perujuk</td>
						<td>:</td>
						<td>.........................................</td>
					</tr>
					<tr>
						<td>Alamat Perujuk</td>
						<td>:</td>
						<td>.........................................</td>
					</tr>
					<tr>
						<td>Unit yg dituju</td>
						<td>:</td>
						<td>
							<table>
								<tr>
									<td><input type="checkbox" name="" />UGD</td>
									<td><input type="checkbox" name="" />Ka.Ber</td>
									<td><input type="checkbox" name="" />Perinatologi</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<table>
								<tr>
									<td><input type="checkbox" name="" />Laboraturium</td>
									<td><input type="checkbox" name="" />Radiologi</td>
									<td><input type="checkbox" name="" />Poli</td>
								</tr>
							</table> 
						</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td>
				<p style="padding: 10px;font-size: 18px">Dengan ini saya menyatakan setuju untuk dilakukan pemeriksaan &  <br> 
				tindakan yang diperlukan dalam upaya kesembuhan / keselamatan <br>
				Jiwa Saya / pasien tersebut diatas.</p>
			</td>
			<td><p style="text-align: center"> Yang Menyatakan <br><br><br><br> ............................. </p></td>
			<td><p style="text-align: center"> Petugas Pendaftar <br><br><br><br> ............................. </p></td>
			 

		</tr>

	</table>
	<br><br><br>

</body>
</html>