<?php $this->load->view('header');?>
<title>SIMRS | Pasien RI</title>
<?php $this->load->view('sidebar');?>

      <!-- START CONTENT -->
<section id="content">
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Pasien Rawat Inap</h5>
            <ol class="breadcrumbs">
                <li><a href="#">Rekam Medis</a></li> 
                <li class="active">Pasien Rawat Inap</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->


    <!--start container-->
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header">List Pasien Rawat Inap</h4>
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div id="card-alert" class="card green notif" style="display:none;">
                            <div class="card-content white-text">
                                <p id="card_message">SUCCESS : The page has been added.</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col s4">
                                    <i class="mdi-action-event prefix"></i>
                                    <input type="text" class="datepicker" name="tgl_awal" id="tgl_awal" placeholder="Tanggal Awal" value="<?php echo date('d F Y'); ?>">
                                    <label for="tgl_awal">Tanggal Admisi, Dari</label>
                                </div>
                                <div class="input-field col s4">
                                    <select id="status" name="status" class="validate">
                                        <option value="" disabled selected>Pilih Status</option>
                                        <option value="ANTRIAN">ANTRIAN</option>
                                        <option value="SEDANG RAWAT INAP">SEDANG RAWAT INAP</option>
                                        <option value="DIPULANGKAN">DIPULANGKAN</option>
                                        <option value="SUDAH PULANG">SUDAH PULANG</option>
                                        <option value="BATAL RAWAT">BATAL RAWAT</option>
                                        <option value="SEDANG DI PENUNJANG">SEDANG DI PENUNJANG</option>
                                    </select>
                                    <label for="status">Status Rawat</label>
                                </div>
                                <div class="input-field col s4">
                                    <select id="poliklinik" name="poliklinik" class="validate">
                                        <option value="" disabled selected>Pilih Ruangan</option>
                                        <?php
                                        $list_poli = $this->Pasienri_model->get_ruangan();
                                        foreach($list_poli as $list){
                                            echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                        }
                                        ?>
                                    </select>
                                    <label for="poliklinik">Ruangan</label>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col s4">
                                    <i class="mdi-action-event prefix"></i>
                                    <input type="text" class="datepicker" name="tgl_akhir" id="tgl_akhir" placeholder="Tanggal Akhir" value="<?php echo date('d F Y'); ?>">
                                    <label for="tgl_akhir">Sampai</label>
                                </div>
                                <div class="input-field col s4">
                                    <input type="text" name="nama_pasien" id="nama_pasien" placeholder="Ketik Nama Pasien" value="">
                                    <label for="nama_pasien">Nama Pasien</label>
                                </div>
                                <div class="input-field col s4">
                                    <select id="dokter_pj" name="dokter_pj" class="validate">
                                        <option value="" disabled selected>Pilih Dokter</option>
                                        <?php
                                        $list_dokter = $this->Pasienri_model->get_dokter_list();
                                        foreach($list_dokter as $list){
                                            echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                        }
                                        ?>
                                    </select>
                                    <label for="dokter_pj">Dokter Penanggung Jawab</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col s12">
                                    <button type="button" class="btn light-green waves-effect waves-light darken-4" title="Cari Pasien" onclick="cariPasien()">Search</button>
                                </div>
                            </div>
                        </div>
                        <div class="col s12"> 
                            &nbsp;
                        </div>
                        <div id="table-datatables">
                            <div class="row">
                                <div class="col s12 m4 l12">
                                    <table id="table_list_pasienri" class="responsive-table display" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No. Pendaftaran</th>
                                                <th>No. RM / Nama Pasien</th>
                                                <th>Tanggal Admisi</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur Pasien</th>
                                                <th>Pembayaran</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Dokter PJ</th>
                                                <th>Ruangan</th>
                                                <th>No. Kamar/ No. Bad</th>
                                                <th>Status Rawat</th>
                                                <th>Status Keluar</th>
                                                <th>RS Rujukan</th>
                                                <th>Diagnosa</th>
                                                <th>Set Keluar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="14">No data to display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end container-->
</section>
    <!-- END CONTENT -->
    <!-- Start Modal Add Paket -->
    <div id="modal_periksa_pasienri" class="modal" style="width: 80% !important ; max-height: 100% !important ;">
        <!--<div class="modal-dialog" role="document">-->
        <div class="modal-content">
            <h1>Pemeriksaan Pasien</h1>
            <div class="divider"></div>
            <div class="col s12 formValidate">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!--</div>-->
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close" onclick="reloadTablePasien()">Close<i class="mdi-navigation-close left"></i></button>
        </div>
    </div>
    <div id="modal_status_pulang" class="modal">
        <?php echo form_open('#',array('id' => 'fmStatusPulang'))?>
        <div class="modal-content">
            <h1>Keterangan Keluar</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green modal2_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="modal2_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="col s12">
                            <label>Keterangan Keluar<span style="color: red;"> *</span></label>
                            <select id="status_pulang" name="status_pulang" class="validate browser-default" onchange="is_rujuk()">
                                <option value="" disabled selected>Pilih Ket. Keluar</option>
                                <option value="Diizinkan">Diizinkan</option>
                                <option value="PAPS">PAPS</option>
                                <option value="Rujuk">Rujuk</option>
                                <option value="Meninggal">Meninggal</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="col s12 input-field" id="rujukan_rs" style="display:none;">
                            <input type="text" name="rs_rujukan" id="rs_rujukan" placeholder="Ketik Nama RS Rujukan" value="">
                            <label for="rs_rujukan">RS Rujukan</label>
                        </div>
                    </div>
                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="" readonly>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="saveStatusPulang">Save<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php $this->load->view('footer');?>
