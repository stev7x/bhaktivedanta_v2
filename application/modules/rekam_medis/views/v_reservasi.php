<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
 <style type="text/css" media="screen">
        .hide{
            display: none;
        }
        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: #686868 !important;
            text-decoration: bold !important;
        }


        option, option:selected {
          color:#686868;
        }

        input,
        input::-webkit-input-placeholder {
            font-size: 14px;
            line-height: 3;
        }
        .mt10 { margin-top: -10px; }
        .mt5  { margin-top: -5px; }

        .panel-info .panel-heading{
            background-color: #42bec8 !important;
            border-color: #42bec8 !important;
        }

        .full{
            height: 100%;
        }

        .tooltip-content{
            width: 300px !important;
            /* z-index : 999999 !important !important ; */
        }
    </style>
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Reservasi Pasien</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <!-- <li><a href="#">Dashboard</a></li> -->
                            <li><a href="#">Pendaftaran</a></li>
                            <li class="active">Reservasi</li>
                        </ol>
                    </div>
                </div>
                <?php echo form_open('#',array('id' => 'fmCreateReservasi', 'data-toggle' => 'validator'))?>
                <div class="row">
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div >
                            <p id="card_message"></p>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="col-md-12">
                            <div class="panel panel-info bg">
                                <div class="panel-heading"> List Data Reservasi
                                </div>
                                <div class="panel-wrapper collapse in " aria-expanded="true"  >
                                    <div class="panel-body" style=" padding:8px !important;height: 622px !important;"  >
                                        <div class="row">
                                            <div class="inline-box">
                                                <div class="form-group col-md-12">
                                                    <div class="input-group">
                                                        <input onkeydown="return false" name="tgl_res_awal" id="tgl_res_awal" type="text" class="form-control mydatepicker2" value="<?php echo date('d M Y'); ?>" placeholder="mm/dd/yyyy"> <span  class="input-group-addon"><i class="icon-calender"></i></span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <select class="form-control" id="poli" name="poli" style="margin-bottom: 7px" onchange="getDokter2()">
                                                        <option value="" disabled selected>PILIH POLI</option>
                                                        <?php
                                                            $list_instalasi = $this->Reservasi_model->get_ruangan(1);
                                                            foreach($list_instalasi as $list){
                                                                    echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <select class="form-control" id="dokter_list" name="dokter_list" style="margin-bottom: 7px" >
                                                        <option value="" disabled selected>PILIH DOKTER</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>
                                                </div>
                                            </div>
                                        </div><hr style="margin-top: -6px">
                                        <div class="row" style="padding: 18px">
                                            <div class="col-md-12">
                                            <ul class="nav customtab nav-tabs" role="tablist" >
                                                    <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#HADIR" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> HADIR</span></a></li>
                                                    <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#BATAL" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">BATAL</span></a></li>
                                                </ul>
                                             <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="HADIR" >
                                                    <select class="form-control" id="change_option" name="change_option" style="margin-bottom: 19px" >
                                                        <option value="" disabled selected>Cari Berdasarkan</option>
                                                        <option value="nama_pasien">Nama Pasien</option>
                                                        <option value="no_rekam_medis">No.Rekam Medis</option>
                                                        <option value="kode_booking">Kode Booking</option>
                                                    </select>
                                                    <input type="text" class="form-control pilih" placeholder="Cari berdasarkan yang anda pilih ..." style="width: 100%;margin-top: -19px;margin-bottom: 16px;" onkeyup="cariPasien()">
                                                    <div class="comment-center hadir-reservasi" style="max-height: 236px;overflow: auto;">
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="BATAL">
                                                    <select class="form-control" id="change_option_batal" name="change_option_batal" style="margin-bottom: 19px" >
                                                        <option value="" disabled selected>Cari Berdasarkan</option>
                                                        <option value="nama_pasien_batal">Nama Pasien</option>
                                                        <option value="no_rekam_medis_batal">No.Rekam Medis</option>
                                                        <option value="kode_booking_batal">Kode Booking</option>
                                                    </select>
                                                    <input type="text" class="form-control pilihbatal" placeholder="Cari berdasarkan yang anda pilih ..." style="width: 100%;margin-top: -19px;margin-bottom: 16px;" onkeyup="cariPasien()">
                                                    <div class="comment-center batal-reservasi" style="max-height: 236px;overflow: auto;">
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-3" style="background-color: #42bec8" >
                            <div style="height: 88px">
                                <i class="fa fa-child fa-4x " style="margin-left: 8px; margin-top: 10px; padding: 8px; color: #ffffff;"></i>
                            </div>
                        </div>
                        <div class="col-md-9" style="background-color: #ffffff">
                            <div style="height: 88px;">
                                <div style="padding-left: 16px; padding-top: 14px;">
                                    <span style="font-size: 13px">NOMOR ANTRIAN</span><br><br>
                                    <span id="jumlah_pasien" style=" font-size: 19px; font-weight: bold;">-</span>
                                    <input type="hidden" name="next_no_urut" id="next_no_urut" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-3" style="background-color: #42bec8" >
                            <div style="height: 88px">
                                <i class="fa fa-ticket fa-4x " style="margin-top: 10px; padding: 8px; color: #ffffff;"></i>
                            </div>
                        </div>
                        <div class="col-md-9" style="background-color: #ffffff; ">
                            <div style="height: 88px;">
                                <div style="padding-left: 16px; padding-top: 14px;">
                                    <span style="font-size: 13px">KODE BOOKING</span><br><br>
                                    <span id="kode_booking" style=" font-size: 19px; font-weight: bold;">-</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-8 pull-right" style="margin-top: -596px;">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Data Pasien</h3><hr>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword" class="control-label">Jenis Pasien<span style="color: red;">*</span></label>
                                        <select class="form-control" id="jenis_pasien" name="jenis_pasien">
                                            <option value="" disabled selected>PILIH</option>
                                                <?php
                                                // die($this->Reservasi_model->get_layanan());
                                                $list_instalasi = $this->Reservasi_model->get_jenis_pasien();
                                                foreach($list_instalasi as $list){

                                                        echo "<option value='".$list->id_jenis_pasien."'>".$list->nama_jenis_pasien."</option>";

                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="no_rm" class="control-label">Nomor Rekam Medis<span style="color: red;">*</span></label>
                                        <div class="input-group custom-search-form">
                                            <input readonly type="text" class="form-control autocomplete" id="no_rekam_medis" name="no_rekam_medis" placeholder="Cari Berdasarkan No.Rekam Medis ...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_pasien" data-backdrop="static" data-keyboard="false"  type="button" onclick="dialogPasien()"> <i class="fa fa-bars"></i> </button>
                                            </span>
                                        </div>
                                        <input type="checkbox" name="cek_pasien" id="cek_pasien" style=""><span for="cek_pasien" style="color: red"> * centang jika pasien baru</span>
                                        <input type="hidden" name="no_rm" id="no_rm" class="form-control">
                                        <input type="hidden" name="is_checked" id="is_checked" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Nama Lengkap<span style="color: red;">*</span></label>
                                        <input type="text" id="pasien_nama" name="pasien_nama" class="form-control" placeholder="Nama Lengkap" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="tgl_lahir" class="control-label">Tanggal Lahir<span style="color: red;" id="span_tgl" >*</span></label>
                                        <div class="input-group">
                                            <input name="tanggal_lahir" id="tanggal_lahir" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Jenis Kelamin<span style="color: red;">*</span></label>
                                        <select class="form-control" id="jk" name="jk">
                                            <option value="" disabled selected>PILIH</option>
                                            <?php
                                                    foreach($list_jenis_kelamin as $list){
                                                        echo "<option value='".$list."'>".$list."</option>";
                                                    }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="form-group col-md-9">
                                        <label class="control-label">Alamat<span hidden style="color: red;" id="span_alamat" >*</span></label>
                                        <textarea class="form-control" rows="2" id="alamat" name="alamat" placeholder="Alamat"> </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Metode Registrasi<span style="color: red;">*</span></label>
                                        <select class="form-control" id="metode_regis" name="metode_regis" onchange="getMetode()">
                                            <option value="" disabled>PILIH</option>
                                            <option value="LANGSUNG" selected>LANGSUNG</option>
                                            <option value="VIA WHATSAPP/SMS">VIA WHATSAPP/SMS</option>
                                            <option value="APLIKASI">APLIKASI</option>
                                            <option value="TELEPON">TELEPON</option>
                                            <option value="RUJUKAN">RUJUKAN</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="tgl_lahir" class="control-label">Tanggal Reservasi<span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <input name="tgl_reservasi" id="tgl_reservasi" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" value="" > <span class="input-group-addon"><i class="icon-calender"></i></span>
                                            <input type="hidden" name="is_tgl_res" id="is_tgl_res">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4" id="div_telp" hidden>
                                        <label for="no_telp" class="control-label">No.Telepon<span style="color: red;">*</span></label>
                                        <input type="text" name="no_telp" id="no_telp" class="form-control">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Layanan <span style="color: red;">*</span></label>
                                        <select class="form-control" id="layanan_baru" name="layanan_baru" onchange="getHotel()">
                                            <option value="" disabled selected>PILIH</option>
                                            <?php
                                                // die($this->Reservasi_model->get_layanan());
                                                $list_instalasi = $this->Reservasi_model->get_layanan();
                                                foreach($list_instalasi as $list){
                                                        echo "<option value='".$list->id_layanan."'>".$list->nama_layanan."</option>";
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group col-md-4 hidden" id="form-hotel">
                                        <label for="inputPassword" class="control-label">Hotel <span style="color: red;">*</span></label>
                                        <select class="form-control" id="hotel" name="hotel">
                                            <option value="" disabled selected>PILIH</option>
                                        </select>
                                        <div id="lainnya">
                                            <input type="checkbox" name="cek_hotel" id="cek_hotel" style=""><span for="cek_hotel" style="color: red"> * Lainya</span>
                                            <div class="input-group">
                                                <input type="text" id="hotel_nama" name="hotel_nama" class="form-control" placeholder="Nama Hotel" disabled >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-8 hidden" id="form-hotel-pic">
                                        <label for="inputPassword" class="control-label">PIC Hotel<span style="color: red;">*</span></label>
                                        <input type="text" id="pic_hotel" name="pic_hotel" class="form-control" placeholder="Nama PIC Hotel">
                                    </div>

                                    <div class="form-group col-md-4 hidden" id="form-driver">
                                        <label for="inputPassword" class="control-label">Driver<span style="color: red;">*</span></label>
                                        <select class="form-control" id="driver" name="driver">
                                            <option value="0" disabled selected>PILIH</option>
                                            <?php
                                                $list = $this->Models->where('masterdata_perawat', array('driver' => 1));
                                                foreach($list as $key => $value) {
                                                        echo "<option value='".$value['id']."'>".$value['nama']."</option>";
                                                }
                                            ?>
                                        </select>
                                        <label for="inputPassword" class="control-label">Pemanggilan<span style="color: red;">*</span></label>
                                        <select class="form-control" id="cek_malam" name="cek_malam">
                                            <option value="PAGI" selected>PAGI (08.00 - 22.00)</option>
                                            <option value="MALAM">MALAM (22.00 - 08.00)</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4 hidden">
                                    <label for="layanan" class="control-label">POLI<span style="color: red;">*</span></label>
                                        <select class="form-control" id="layanan" name="layanan" style="margin-bottom: 7px" onchange="getDokter()">
                                            <option value="">PILIH POLI</option>
                                            <?php
                                                $list_instalasi = $this->Reservasi_model->get_ruangan(1);
                                                foreach($list_instalasi as $list){
                                                        echo "<option value='".$list->poliruangan_id."' selected>".$list->nama_poliruangan."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Dokter<span style="color: red;">*</span></label>
                                        <select class="form-control" id="dokter" name="dokter">
                                            <option value="" disabled selected>PILIH</option>
                                            <?php
                                                $list = $this->Models->allCustom('m_dokter', 'id_M_DOKTER');
                                                foreach($list as $key => $value) {
                                                    echo "<option value='".$value['id_M_DOKTER']."'>".$value['NAME_DOKTER']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>

                                    <!-- <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Jam<span style="color: red;">*</span></label>

                                        <select class="form-control" id="jam" name="jam" onchange="getKuota()">
                                            <option value="" disabled selected>PILIH</option>
                                        </select>
                                    </div> -->

                                    <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Jenis Pembayaran<span style="color: red;">*</span></label>

                                        <select class="form-control" id="jenis_pembayaran" name="jenis_pembayaran" onchange="showJenisPembayaran(this.value)">
                                            <?php
                                                foreach ($list_type_bayar as $item => $value) {
                                                    echo '<option value="'.$item.'">'.$value.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="row hidden" id="container-asuransi">
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Insurance company</label>
                                        <select class="form-control" id="asuransi1" name="asuransi1">
                                            <option value="" selected>PILIH</option>
                                                <?php
                                                $list = $this->Models->all('masterdata_asuransi');
                                                foreach($list as $key => $value) {
                                                    echo "<option value='".$value['id']."'>".$value['nama']."</option>";
                                                }
                                                ?>
                                        </select>
                                        <label for="inputPassword" class="control-label">Nomor Asuransi</label>
                                        <input type="text" class="form-control" id="no_asuransi1" name="no_asuransi1">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword" class="control-label">Travel Insurance</label>
                                        <select class="form-control" id="asuransi2" name="asuransi2">
                                            <option value="" selected>PILIH</option>
                                            <?php
                                                $list = $this->Models->all('masterdata_asuransi');
                                                foreach($list as $key => $value) {
                                                    echo "<option value='".$value['id']."'>".$value['nama']."</option>";
                                                }
                                                ?>
                                        </select>
                                        <label for="inputPassword" class="control-label">Nomor Asuransi</label>
                                        <input type="text" class="form-control" id="no_asuransi2" name="no_asuransi2">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-9">
                                        <label class="control-label">Keterangan</label>
                                        <textarea class="form-control" id="ket" name="ket" placeholder="Keterangan"> </textarea>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 42px">
                                    <div class="col-md-12">
                                    <button type="button" class="btn btn-info pull-right" id="btnSaveReservasi"><i class="fa fa-floppy-o"></i> &nbsp;SIMPAN</button>
                                    <button style="margin-right: 8px" type="button" class="btn btn-danger pull-right" onclick="reload()"><i class="fa fa-refresh"></i> &nbsp;RESET</button>


                                    </div>
                                </div>





                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

<div id="modal_list_pasien" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Pasien</h2> </div>
            <div class="modal-body" style="height:500px;overflow: auto;">
                <table id="table_list_pasien" class="table table-striped dataTable no-footer table-responsive" cellspacing="0">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Rekam Medis</th>
                        <th>No. BPJS</th>
                        <th>Nama Pasien</th>
                        <th>Nama Panggilan</th>
                        <th>Tanggal Lahir</th>
                        <th>Alamat</th>
                        <th>Pilih</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Rekam Medis</th>
                        <th>No. BPJS</th>
                        <th>Nama Pasien</th>
                        <th>Nama Panggilan</th>
                        <th>Tanggal Lahir</th>
                        <th>Alamat</th>
                        <th>Pilih</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php $this->load->view('footer');?>
