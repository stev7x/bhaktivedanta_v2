<html>
<head>
	<title>SEP KOSONG</title>
</head>
<style type="text/css">
	table {
		border-collapse: collapse;
	}
	.mx-10 { 
		max-width: 10%;
	}
	.mx-20 {
		max-width: 20%;
	}
	.mx-30 { 
		max-width: 30%;
	}
	.w80{
		width: 80%; 
	}  
</style>    
<body onload="window.print()">
	<table align="center" width="100%" >
		<tr><td>  
			<!-- Header -->
			<table  width="100%" >     
				<tr>
					<td class="mx-30"><img src="<?= base_url() ?>/assets/plugins/images/bpjs-logo.png" width="100%" height="62px"></td>    
					<td width="70%"><h4 align="center">SURAT ELEGIBILITAS PESERTA</h4></td> 
				</tr>
			</table>
			<!-- end header -->

			<!-- body -->
			<table width="100%" >
				<tr>
					<!-- body left -->
					<td max-width="50%" valign="top" >
						<table width="100%" >
							<tr>
								<td class="mx-20" 	>No.SEP</td>
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td class="mx-20" valign="top" >Tgl.SEP</td>
								<td class="w80" valign="top"  >:</td> 
							</tr>
							<tr>
								<td class="mx-20" valign="top" >No.Kartu</td>
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td class="mx-20" valign="top" >Nama Peserta</td>
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td class="mx-20" valign="top" >Tgl.Lahir</td>
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td class="mx-20" valign="top" >Jns.Kelamin</td>
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td class="mx-20" valign="top" >Poli Tujuan</td>
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td class="mx-20" valign="top" >Asal Faskes Tk. I</td>
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td class="mx-20" valign="top" >Diagnosa Awal</td>
								<td class="w80" valign="top"  >:</td>
							</tr> 
							<tr>
								<td class="mx-20" valign="top" >Catatan</td>
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td colspan="2"> 
									<p style="font-size: 13px"><i>*Saya Menyetujui BPJS Kesehatan menggunakan informasi Medis Pasien jika diperlukan.<br>*SEP bukan sebagai bukti penjaminan peserta</i></p>
								</td>
							</tr>
						</table>
					</td>
					<!-- end body left -->

					<!-- body right -->
					<td width="50%" >
						<table width="100%" >
							<tr>
								<td class="mx-10" valign="top">Peserta</td>
								<td class="w80" valign="top" style="padding-bottom : 38px;width: 90px"   >:</td>
							</tr>
							<tr>
								<td class="mx-10" valign="top" >COB</td> 
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr>
								<td class="mx-10" valign="top" >Jns.Rawat</td> 
								<td class="w80" valign="top"  >:</td>
							</tr>
							<tr> 
								<td class="mx-10" valign="top" >Kls.Rawat</td>
								<td class="w80" valign="top"  style="padding-bottom: 28px">:</td>
							</tr>
							<tr><td colspan="2">
								<table width="60%" align="center" >
									<tr>
										<td>Pasien/<br>Keluarga Pasien<br><br><br>______________</td>
										<td>Petugas<Br> BPJS Kesehatan<br><br><br>______________</td>
									</tr>
								</table>
							</td></tr>


						</table>
					</td>
					<!-- end body right -->
				</tr>
			</table>

			<!-- end body -->

		</td></tr>
	</table>
		

</body>
</html>