<?php $this->load->view('header');?>
<title>SIMRS | Rekam Medis List Pasien</title>
<?php $this->load->view('sidebar');?>
     <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Hospital Dashboard</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <a href="" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
                        <ol class="breadcrumb">
                            <li><a href="index.html">Hospital</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
        <!--start container-->
        <div class="container">
            <div class="section">  
                <div class="row"> 
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="divider"></div>
                            <h4 class="header">Rekam Medis List Pasien</h4>
                            <div id="card-alert" class="card green notif" style="display:none;">
                                <div class="card-content white-text">
                                    <p id="card_message">SUCCESS : The page has been added.</p>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div id="table-datatables">
                                <div class="row">
                                    <div class="col s12 m4 l12">
                                        <table id="table_list_pasien_list" class="responsive-table display" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Rekam Medis</th>
                                                    <th>Nama Pasien</th>
                                                    <th>Alamat</th>
                                                    <th>Telepon</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Rekam Medis</th>
                                                    <th>Nama Pasien</th>
                                                    <th>Alamat</th>
                                                    <th>Telepon</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                            <br>
                        </div>
                    </div>
                </div>
           
        <!--end container-->
    </section>
    <!-- END CONTENT --> 
    
    <!-- Start Modal Add list_pasien -->
    <!-- End Modal Add list_pasien -->
     <!-- Modal Update list_pasien-->
     <div id="modal_update_list_pasien" class="modal fadeIn" tabindex="-1" role="dialog" style="width: 80% !important ; max-height: 90% !important ;">
        <?php echo form_open('#',array('id' => 'fmUpdatelist_pasien'))?>
        <div class="modal-content">
            <h1>Update List Pasien</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green upd_modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="upd_modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div class="card-panel">
                            <span style="color: red;"> *) wajib diisi</span>
                            <br>
                                <div class="row">
                                    <div id="jqueryvalidation" class="section">
                                        <div class="col s6 formValidate">
                                            <div class="row">
                                                <div class="input-field col s4">
                                                    <select id="upd_type_id" name="upd_type_id">
                                                        <option value="">Type ID</option>
                                                        <?php
                                                            foreach($list_type_identitas as $list){
                                                                echo "<option value='".$list."'>".$list."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                    <label>Type Identitas</label>
                                                </div>
                                                <div class="input-field col s7">
                                                    <input id="upd_no_identitas" type="text" name="upd_no_identitas" class="validate" placeholder="No. Identitas Pasien">
                                                    <label for="upd_no_identitas">No. Identitas<span style="color: red;"> *</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <input id="upd_no_rm" type="text" name="upd_no_rm" class="validate" value="" readonly>
                                                    <label for="upd_no_rm" id='label_upd_no_rm' class="active">No. Rekam Medis<span style="color: red;"> *</span></label>
                                                </div> 
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <input id="upd_nama_pasien" type="text" name="upd_nama_pasien" class="validate" placeholder="Nama Lengkap Pasien">
                                                    <label for="upd_nama_pasien" id='label_upd_nama_pasien'>Nama Pasien<span style="color: red;"> *</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <input id="upd_tempat_lahir" type="text" name="upd_tempat_lahir" placeholder="Kota/Kabupaten Kelahiran">
                                                    <label for="upd_tempat_lahir" id="label_upd_tempat_lahir">Tempat Lahir</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <input type="text" class="datepicker" name="upd_tgl_lahir" id="upd_tgl_lahir" placeholder="Tanggal Lahir" onchange="setUmur()">
                                                    <label for="upd_tgl_lahir" id="label_upd_tgl_lahir">Tanggal Lahir<span style="color: red;"> *</span></label>
                                                </div>
                                                <div class="input-field col s5">
                                                    <input id="upd_umur" type="text" name="upd_umur" placeholder="00 thn 00 bln 00 hr" class="validate" readonly>
                                                    <label for="upd_umur" id="label_upd_umur">Umur<span style="color: red;"> *</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s11">
                                                    <label>Jenis Kelamin<span style="color: red;"> *</span></label>
                                                    <select id="upd_jenis_kelamin" name="upd_jenis_kelamin" class="validate browser-default">
                                                        <option value="">Pilih Jenis Kelamin</option>
                                                        <?php
                                                            foreach($list_jenis_kelamin as $list){
                                                                echo "<option value='".$list."'>".$list."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s11">
                                                    <label>Status Perkawinan</label>
                                                    <select id="upd_status_kawin" name="upd_status_kawin" class="browser-default">
                                                        <option value="">Pilih Status Kawin</option>
                                                        <?php
                                                            foreach($list_status_kawin as $list){
                                                                echo "<option value='".$list."'>".$list."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s8">
                                                    <label>Golongan Darah</label>
                                                    <select  id="upd_golongan_darah" name="upd_golongan_darah" class="browser-default">
                                                        <option value="">Gol. Darah</option>
                                                        <?php
                                                            foreach($list_golongan_darah as $list){
                                                                echo "<option value=".$list.">".$list."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col s3">
                                                    <p>
                                                        <input id="rhesus0" name="rhesus" type="radio" value="0">
                                                        <label for="rhesus0">Rh-</label>
                                                    </p>
                                                    <p>
                                                        <input id="rhesus1" name="rhesus" type="radio" value="1">
                                                        <label for="rhesus1">Rh+</label>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <textarea id="upd_alamat" name="upd_alamat" class="materialize-textarea validate"></textarea>
                                                    <label id="label_alamat" for="upd_alamat">Alamat<span style="color: red;"> *</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s5">
                                                    <label>Propinsi<span style="color: red;"> *</span></label>
                                                    <select id="upd_propinsi" name="upd_propinsi" class="validate browser-default" onchange="getKabupaten()">
                                                        <option value="" disabled selected>Pilih Propinsi</option>
                                                        <?php
                                                        $list_propinsi = $this->List_pasien_model->get_propinsi();
                                                        foreach($list_propinsi as $list){
                                                            echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col s6">
                                                    <label>Kabupaten<span style="color: red;"> *</span></label>
                                                    <select id="kabupaten" name="kabupaten" class="validate browser-default" onchange="getKecamatan()">
                                                        <option value="" disabled selected>Pilih Kabupaten</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s5">
                                                    <label>Kecamatan<span style="color: red;"> *</span></label>
                                                    <select  id="kecamatan" name="kecamatan" class="validate browser-default" onchange="getKelurahan()">
                                                        <option value="" disabled selected>Pilih Kecamatan</option>

                                                    </select>
                                                </div>
                                                <div class="col s6">
                                                    <label>Kelurahan/Desa<span style="color: red;"> *</span></label>
                                                    <select  id="kelurahan" name="kelurahan" class="validate browser-default">
                                                        <option value="" disabled selected>Pilih Kelurahan</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s6 formValidate">
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <input type="text" id="upd_no_tlp_rmh" name="upd_no_tlp_rmh" placeholder="No. Telepon yang bisa dihubungi">
                                                    <label for="upd_no_tlp_rmh" id="label_upd_no_tlp_rmh">No. Telepon Rumah</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <input type="text" id="upd_no_mobile" name="upd_no_mobile" placeholder="No. Ponsel yang bisa dihubungi">
                                                    <label for="upd_no_mobile" id="label_upd_no_mobile">No. Ponsel/Mobile</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s11">
                                                    <!--<input type="text" id="pekerjaan" name="pekerjaan" placeholder="Pekerjaan Pasien">-->
                                                    <label for="upd_pekerjaan">Pekerjaan Pasien</label>
                                                    <select id="upd_pekerjaan" name="upd_pekerjaan" class="browser-default">
                                                        <option value="">Pilih Pekerjaan</option>
                                                        <?php
                                                            foreach($list_pekerjaan as $list){
                                                                echo "<option value='".$list."'>".$list."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s11">
                                                    <label>Warga Negara</label>
                                                    <select id="upd_warga_negara" name="upd_warga_negara" class="validate browser-default">
                                                        <option value="">Pilih Warga Negara</option>
                                                        <?php
                                                            foreach($list_warga_negara as $list){
                                                                echo "<option value='".$list."'>".$list."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s11">
                                                    <label>Agama</label>
                                                    <select id="upd_agama" name="upd_agama" class="validate browser-default">
                                                        <option value="">Pilih Agama</option>
                                                        <?php
                                                        $list_agama = $this->List_pasien_model->get_agama();
                                                        foreach($list_agama as $list){
                                                            echo "<option value='".$list->agama_id."'>".$list->agama_nama."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s11">
                                                    <label>Suku</label>
                                                    <select  id="upd_suku" name="upd_suku" class="browser-default">
                                                        <option value="">Pilih Suku</option>
                                                        <?php
                                                        $list_suku = $this->List_pasien_model->get_suku();
                                                        foreach($list_suku as $list){
                                                            echo "<option value='".$list->suku_id."'>".$list->suku_nama."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s11">
                                                    <label>Pendidikan</label>
                                                    <select  id="upd_pendidikan" name="upd_pendidikan" class="browser-default">
                                                        <option value="">Pilih Pendidikan</option>
                                                        <?php
                                                        $list_pendidikan = $this->List_pasien_model->get_pendidikan();
                                                        foreach($list_pendidikan as $list){
                                                            echo "<option value='".$list->pendidikan_id."'>".$list->pendidikan_nama."</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <input id="upd_anakke" type="text" name="upd_anakke">
                                                    <label class="active" for="upd_anakke" id="label_upd_anakke">Anak Ke</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <input id="upd_namakeluarga" type="text" name="upd_namakeluarga" placeholder="Nama Keluarga">
                                                    <label for="upd_namakeluarga" id="label_upd_namakeluarga">Nama Keluarga</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s11">
                                                    <label>Status Hubungan</label>
                                                    <select  id="upd_hubungankeluarga" name="upd_hubungankeluarga" class="browser-default">
                                                        <option value="" disabled selected>Pilih Hubungan</option>
                                                        <?php
                                                            foreach($list_hubungan as $list){
                                                                echo "<option value=".$list.">".$list."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s11">
                                                    <textarea id="upd_alergi" name="upd_alergi" class="materialize-textarea"></textarea>
                                                    <label id="label_upd_alergi" for="upd_alergi">Alergi</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <input id="upd_pasien_id" type="hidden" name="upd_pasien_id" readonly>
                                                <input id="upd_status_pasien" type="hidden" name="upd_status_pasien" readonly>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="changelist_pasienOperasi">Save Change<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php $this->load->view('footer');?>
      