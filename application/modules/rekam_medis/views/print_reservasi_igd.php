
<!DOCTYPE html>
<head>
<title>Sticker Pasien</title>
</head>

<style type="text/css">
  body{
    margin: 0;
  }
	td {
		font-weight: bold;
	}

</style>

<body onload="window.print()"><font size="2">
<table align="center">
          <tr>
            <td>No. RM</td>
            <td>:</td>
            <td><b style="font-size: 20px;"><?php echo $data_pasien->no_rekam_medis; ?><b></td>
          </tr>
          <tr>
            <td> Nama</td>
            <td>:</td>
            <td><?php echo $data_pasien->nama_pasien; ?></td>
          </tr>
          <tr>
            <td>Tgl Lahir</td>
            <td>:</td>

            <td><?php echo date("d-m-Y", strtotime($data_pasien->tanggal_lahir)) ?></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?php echo $data_pasien->alamat_pasien; ?></td>
          </tr>
          <tr>
            <td>Tanggal Reservasi</td>
            <td>:</td>
            <?php
              $res = "";
              if ($data_pasien->is_reschedule == 1) {
                $res = "(RESCHEDULE)";
              }
            ?>
            <td><?php echo $data_pasien->tanggal_reservasi ." ". $res; ?></td>
          </tr>
          <tr>
            <td>Jam Periksa</td>
            <td>:</td>
            <td><?php echo $data_pasien->jam; ?></td>
          </tr>
          <tr>
            <td>Status</td>
            <td>:</td>
            <td><?php echo $data_pasien->status_pasien; ?></td>
          </tr>



</table>
</font>
</body>
</html>
