<!DOCTYPE html>
<html>
<head>
	<title>Kartu Berobat</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<style type="text/css">
		.ukuran{
			width: 9cm ;
			height: 6cm;
		}
		.row{
			margin-left: 16px;
			margin-top: 24px;
		}
		.jarak{
			margin-top: -14px;
		}
		img{
			width: 80px;
			height:35px;
		}

		.image{
			float: right;
			margin-right: 82px;
			margin-top: -32px;
		}
	</style>
</head>
<body onload="window.print()">
<div class="ukuran">
	<div class="row">
		<?php
			$jk = "";
			if ($data_pasien->jenis_kelamin == "Laki-laki") {
				$jk = "L";
			}else if($data_pasien->jenis_kelamin == "Perempuan"	){
				$jk = "P";
			}
		?>
		<input hidden type="text" name="temp_nama" id="temp_nama" value="<?php echo $data_pasien->pasien_nama ?>">
		<input hidden type="text" name="temp_jk" id="temp_jk" value="<?php echo $jk ?>">
		<div ><b style="text-transform: uppercase; width: 5px;"><span id="nama_pasien"></span></b></div><br>
		<div class="jarak">
			<?php

	      setlocale(LC_ALL, 'IND');
	      setlocale(LC_ALL, 'id_ID');
			$date = strftime( "%d %B %Y ", strtotime($data_pasien->tanggal_lahir));
			 echo $date; ?>
			
		</div><br>
		<div class="jarak"><?php echo $data_pasien->no_identitas; ?></div><br>
		<div class="jarak"><b><?php echo $data_pasien->no_rekam_medis; ?></div>
		<div class="image"><?= $barcode ?></div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){
		$temp_nama = $('#temp_nama').val().substring(0,20)
		$temp_jk = $('#temp_jk').val();
		
		$('#nama_pasien').text($temp_nama + " ("+$temp_jk+")" );


	});
</script>