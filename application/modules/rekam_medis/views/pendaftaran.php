
<?php $this->load->view('header');?>
<title>SIMRS | Pendaftaran</title>
<?php $this->load->view('sidebar');?>

      <!-- START CONTENT -->
      <section id="content">

        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Pendaftaran Pasien</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Rekam Medis</a></li>
                    <li class="active">Pendaftaran</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->


        <!--start container-->
    <?php echo form_open('#',array('id' => 'fmCreatePendaftaran'))?>
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card-panel">
                    <h4 class="header2">Data Pasien</h4>
                    <div class="row">
                        <!--<form class="col s12">-->
                          <!--jqueryvalidation-->
                        <div id="jqueryvalidation" class="section">
                            <div class="col s12">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <div id="card-alert" class="card green modal_notif" style="display:none;">
                                            <div class="card-content white-text">
                                                <p id="card_message"></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s6 formValidate">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="cari_no_rekam_medis" class="autocomplete" type="text" name="cari_no_rekam_medis" placeholder="Ketik no.rekam medis">
                                        <label for="cari_no_rekam_medis">Cari no. Rekam Medis</label>
                                    </div>
                                    <div class="input-field col s3">
                                        <div class="input-field col s12">
                                            <button type="button" class="btn waves-effect waves-light yellow darken-4" onclick="dialogPasien()"><i class="mdi-action-list"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s4">
                                        <select id="type_id" name="type_id">
                                            <option value="" disabled selected>Type ID</option>
                                            <?php
                                                foreach($list_type_identitas as $list){
                                                    echo "<option value='".$list."'>".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                        <label>Type Identitas</label>
                                    </div>
                                    <div class="input-field col s7">
                                        <input id="no_identitas" type="text" name="no_identitas" class="validate" placeholder="No. Identitas Pasien">
                                        <label for="no_identitas">No. Identitas<span style="color: red;"> *</span></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <input id="no_rm" type="text" name="no_rm" class="validate" value="<?php echo $next_no_rm; ?>" readonly>
                                        <label for="no_rm" class="active">No. Rekam Medis<span style="color: red;"> *</span></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <input id="nama_pasien" type="text" name="nama_pasien" class="validate" placeholder="Nama Lengkap Pasien">
                                        <label for="nama_pasien">Nama Pasien<span style="color: red;"> *</span></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <input id="tempat_lahir" type="text" name="tempat_lahir" placeholder="Kota/Kabupaten Kelahiran">
                                        <label for="tempat_lahir">Tempat Lahir</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input type="text" class="datepicker" name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir" onchange="setUmur()">
                                        <label for="tgl_lahir">Tanggal Lahir<span style="color: red;"> *</span></label>
                                    </div>
                                    <div class="input-field col s5">
                                        <input id="umur" type="text" name="umur" placeholder="00 thn 00 bln 00 hr" class="validate" readonly>
                                        <label for="umur">Umur<span style="color: red;"> *</span></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s11">
                                        <label>Jenis Kelamin<span style="color: red;"> *</span></label>
                                        <select id="jenis_kelamin" name="jenis_kelamin" class="validate browser-default">
                                            <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                            <?php
                                                foreach($list_jenis_kelamin as $list){
                                                    echo "<option value='".$list."'>".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s11">
                                        <label>Status Perkawinan</label>
                                        <select id="status_kawin" name="status_kawin" class="browser-default">
                                            <option value="" disabled selected>Pilih Status Kawin</option>
                                            <?php
                                                foreach($list_status_kawin as $list){
                                                    echo "<option value='".$list."'>".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s8">
                                        <label>Golongan Darah</label>
                                        <select  id="golongan_darah" name="golongan_darah" class="browser-default">
                                            <option value="" disabled selected>Gol. Darah</option>
                                            <?php
                                                foreach($list_golongan_darah as $list){
                                                    echo "<option value=".$list.">".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col s3">
                                        <p>
                                            <input id="rhesus0" name="rhesus" type="radio" value="0">
                                            <label for="rhesus0">Rh-</label>
                                        </p>
                                        <p>
                                            <input id="rhesus1" name="rhesus" type="radio" value="1">
                                            <label for="rhesus1">Rh+</label>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <textarea id="alamat" name="alamat" class="materialize-textarea validate"></textarea>
                                        <label id="label_alamat" for="alamat">Alamat<span style="color: red;"> *</span></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s5">
                                        <label>Propinsi<span style="color: red;"> *</span></label>
                                        <select id="propinsi" name="propinsi" class="validate browser-default" onchange="getKabupaten()">
                                            <option value="" disabled selected>Pilih Propinsi</option>
                                            <?php
                                            $list_propinsi = $this->Pendaftaran_model->get_propinsi();
                                            foreach($list_propinsi as $list){
                                                echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col s6">
                                        <label>Kabupaten<span style="color: red;"> *</span></label>
                                        <select id="kabupaten" name="kabupaten" class="validate browser-default" onchange="getKecamatan()">
                                            <option value="" disabled selected>Pilih Kabupaten</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s5">
                                        <label>Kecamatan<span style="color: red;"> *</span></label>
                                        <select  id="kecamatan" name="kecamatan" class="validate browser-default" onchange="getKelurahan()">
                                            <option value="" disabled selected>Pilih Kecamatan</option>

                                        </select>
                                    </div>
                                    <div class="col s6">
                                        <label>Kelurahan/Desa<span style="color: red;"> *</span></label>
                                        <select  id="kelurahan" name="kelurahan" class="validate browser-default">
                                            <option value="" disabled selected>Pilih Kelurahan</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col s6 formValidate">
                                <div class="row">
                                    <div class="input-field col s11">
                                        <input type="text" id="no_tlp_rmh" name="no_tlp_rmh" placeholder="No. Telepon yang bisa dihubungi">
                                        <label for="no_tlp_rmh">No. Telepon Rumah</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <input type="text" id="no_mobile" name="no_mobile" placeholder="No. Ponsel yang bisa dihubungi">
                                        <label for="no_mobile">No. Ponsel/Mobile</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s11">
                                        <!--<input type="text" id="pekerjaan" name="pekerjaan" placeholder="Pekerjaan Pasien">-->
                                        <label for="pekerjaan">Pekerjaan Pasien</label>
                                        <select id="pekerjaan" name="pekerjaan" class="browser-default">
                                            <option value="" disabled selected>Pilih Pekerjaan</option>
                                            <?php
                                                foreach($list_pekerjaan as $list){
                                                    echo "<option value='".$list."'>".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s11">
                                        <label>Warga Negara</label>
                                        <select id="warga_negara" name="warga_negara" class="validate browser-default">
                                            <option value="" disabled selected>Pilih Warga Negara</option>
                                            <?php
                                                foreach($list_warga_negara as $list){
                                                    echo "<option value='".$list."'>".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s11">
                                        <label>Agama</label>
                                        <select id="agama" name="agama" class="validate browser-default">
                                            <option value="" disabled selected>Pilih Agama</option>
                                            <?php
                                            $list_agama = $this->Pendaftaran_model->get_agama();
                                            foreach($list_agama as $list){
                                                echo "<option value='".$list->agama_id."'>".$list->agama_nama."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s11">
                                        <label>Suku</label>
                                        <select  id="suku" name="suku" class="browser-default">
                                            <option value="" disabled selected>Pilih Suku</option>
                                            <?php
                                            $list_suku = $this->Pendaftaran_model->get_suku();
                                            foreach($list_suku as $list){
                                                echo "<option value='".$list->suku_id."'>".$list->suku_nama."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s11">
                                        <label>Bahasa</label>
                                       <select  id="bahasa" name="bahasa" class="browser-default">
                                            <option value="" disabled selected>Pilih Bahasa</option>
                                            <?php
                                            $list_bahasa = $this->Pendaftaran_model->get_bahasa();
                                            foreach($list_bahasa as $list){
                                                echo "<option value='".$list->bahasa_id."'>".$list->bahasa."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s11">
                                        <label>Pendidikan</label>
                                        <select  id="pendidikan" name="pendidikan" class="browser-default">
                                            <option value="" disabled selected>Pilih Pendidikan</option>
                                            <?php
                                            $list_pendidikan = $this->Pendaftaran_model->get_pendidikan();
                                            foreach($list_pendidikan as $list){
                                                echo "<option value='".$list->pendidikan_id."'>".$list->pendidikan_nama."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <input id="anakke" type="text" name="anakke">
                                        <label class="active" for="anakke">Anak Ke</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <input id="namakeluarga" type="text" name="namakeluarga" placeholder="Nama Keluarga">
                                        <label for="namakeluarga">Nama Keluarga</label>
                                    </div>
                                </div>
<!--                                <div class="row">
                                    <div class="col s11">
                                        <label>Status Hubungan</label>
                                        <select  id="hubungankeluarga" name="hubungankeluarga" class="browser-default">
                                            <option value="" disabled selected>Pilih Hubungan</option>
                                            <?php
//                                                foreach($list_hubungan as $list){
//                                                    echo "<option value='".$list."'>".$list."</option>";
//                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>-->
                                <div class="row">
                                    <div class="input-field col s11">
                                        <textarea id="alergi" name="alergi" class="materialize-textarea"></textarea>
                                        <label id="label_alergi" for="alergi">Alergi</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <input id="pasien_id" type="hidden" name="pasien_id" readonly>
                                    <input id="status_pasien" type="hidden" name="status_pasien" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-panel">
                    <h4 class="header2">Ringkasan Masuk</h4>
                    <div class="row">
                        <div class="col s6">
                            <div class="row">
                                <div class="col s11">
                                    <label>Instalasi Tujuan<span style="color: red;"> *</span></label>
                                    <select id="instalasi" name="instalasi" class="validate browser-default" onchange="getRuangan()">
                                        <option value="" disabled selected>Pilih Instalasi</option>
                                        <?php
                                        $list_instalasi = $this->Pendaftaran_model->get_instalasi();
                                        foreach($list_instalasi as $list){
                                            if($list->instalasi_id != INSTALASI_RAWAT_INAP){
                                                echo "<option value='".$list->instalasi_id."'>".$list->instalasi_nama."</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s11">
                                    <label>Poliklinik/Ruangan<span style="color: red;"> *</span></label>
                                    <select id="poli_ruangan" name="poli_ruangan" class="validate browser-default" onchange="getKelasPoliruangan()">
                                        <option value="" disabled selected>Pilih Poli/Ruangan</option>

                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s11">
                                    <label>Kelas Pelayanan<span style="color: red;"> *</span></label>
                                    <select id="kelas_pelayanan" name="kelas_pelayanan" class="validate browser-default" onchange="getKamar()">
                                        <option value="" disabled selected>Pilih Kelas Pelayanan</option>

                                    </select>
                                </div>
                            </div>
                            <div class="row" id="rowKamar" style="display: none;">
                                <div class="col s11">
                                    <label>Kamar<span style="color: red;"> *</span></label>
                                    <select id="kamarruangan" name="kamarruangan" class="validate browser-default">
                                        <option value="" disabled selected>Pilih Kamar</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col s6">
                            <div class="row">
                                <div class="col s11">
                                    <label>Dokter Penanggung jawab<span style="color: red;"> *</span></label>
                                    <select id="dokter" name="dokter" class="validate browser-default">
                                        <option value="" disabled selected>Pilih Dokter</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" id="rowDokterAnastesi" style="display: none;">
                                <div class="col s11">
                                    <label>Dokter Anastesi<span style="color: red;"> *</span></label>
                                    <select id="dokter_anastesi" name="dokter_anastesi" class="validate browser-default">
                                        <option value="" disabled selected>Pilih Dokter</option>
                                        <?php
                                        $list_anastesi = $this->Pendaftaran_model->get_dokter_anastesi();
                                        foreach($list_anastesi as $list){
                                            echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" id="rowPerawat" style="display: none;">
                                <div class="col s11">
                                    <label>Perawat<span style="color: red;"> *</span></label>
                                    <select id="perawat" name="perawat" class="validate browser-default">
                                        <option value="" disabled selected>Pilih Perawat</option>
                                        <?php
                                        $list_perawat = $this->Pendaftaran_model->get_perawat();
                                        foreach($list_perawat as $list){
                                            echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s11">
                                    <input type="text" id="rm_namaperujuk" name="rm_namaperujuk" placeholder="Nama Perujuk">
                                    <label for="rm_namaperujuk">Nama Perujuk</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s11">
                                    <textarea id="rm_alamatperujuk" name="rm_alamatperujuk" class="materialize-textarea"></textarea>
                                    <label for="rm_alamatperujuk">Alamat Perujuk</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-panel">
                    <h4 class="header2">Penanggung Jawab</h4>
                    <div class="row">
                        <div class="col s6">
                            <div class="row">
                                <div class="input-field col s11">
                                    <input type="text" id="pj_nama" name="pj_nama" placeholder="Nama Penanggung Jawab Pasien">
                                    <label for="pj_nama">Nama Penanggung Jawab<span style="color: red;"> *</span></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s11">
                                    <input type="text" id="pj_no_tlp_rmh" name="pj_no_tlp_rmh" placeholder="No. Telepon yang bisa dihubungi">
                                    <label for="pj_no_tlp_rmh">No. Telepon Rumah</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s11">
                                    <input type="text" id="pj_no_mobile" name="pj_no_mobile" placeholder="No. Ponsel yang bisa dihubungi">
                                    <label for="pj_no_mobile">No. Ponsel/Mobile</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s11">
                                    <textarea id="pj_alamat" name="pj_alamat" class="materialize-textarea"></textarea>
                                    <label for="pj_alamat">Alamat</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s6">
                            <div class="row">
                               <div class="col s11">
                                    <label>Propinsi</label>
                                    <select id="pj_propinsi" name="pj_propinsi" class="browser-default" onchange="getKabupatenPj()">
                                        <option value="" disabled selected>Pilih Propinsi</option>
                                        <?php
                                        $list_propinsi_pj = $this->Pendaftaran_model->get_propinsi();
                                        foreach($list_propinsi_pj as $list){
                                            echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s11">
                                    <label>Kabupaten</label>
                                    <select id="pj_kabupaten" name="pj_kabupaten" class="browser-default" onchange="getKecamatanPj()">
                                        <option value="" disabled selected>Pilih Kabupaten</option>
                                        <?php
//                                            foreach($list_status_kawin as $list){
//                                                echo "<option value='".$list."'>".$list."</option>";
//                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s11">
                                    <label>Kecamatan</label>
                                    <select  id="pj_kecamatan" name="pj_kecamatan" class="browser-default" onchange="getKelurahanPj()">
                                        <option value="" disabled selected>Pilih Kecamatan</option>
                                        <?php
//                                            foreach($list_golongan_darah as $list){
//                                                echo "<option value=".$list.">".$list."</option>";
//                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s11">
                                    <label>Kelurahan/Desa</label>
                                    <select  id="pj_kelurahan" name="pj_kelurahan" class="browser-default">
                                        <option value="" disabled selected>Pilih Kelurahan</option>
                                        <?php
//                                            foreach($list_golongan_darah as $list){
//                                                echo "<option value=".$list.">".$list."</option>";
//                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s11">
                                    <label>Status Hubungan<span style="color: red;"> *</span></label>
                                        <select  id="hubunganpj" name="hubunganpj" class="browser-default">
                                            <option value="" disabled selected>Pilih Hubungan</option>
                                            <?php
                                                foreach($list_hubungan as $list){
                                                    echo "<option value='alis'>".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-panel">
                    <h4 class="header2">Pembayaran</h4>
                    <div class="row">
                        <div class="col s6">
                            <div class="row">
                                <div class="col s11">
                                    <label>Type Pembayaran<span style="color: red;"> *</span></label>
                                    <select  id="byr_type_bayar" name="byr_type_bayar" class="browser-default" onclick="cekTypeBayar()">
                                        <option value="" disabled selected>Pilih Type Pembayaran</option>
                                        <?php
                                            foreach($list_type_bayar as $list){
                                                echo "<option value=".$list.">".$list."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s11">
                                    <input type="text" id="byr_instansi" name="byr_instansi" placeholder="Instansi Pekerjaan">
                                    <label for="byr_instansi">Instansi Pekerjaan</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s6">
                            <div class="row">
                                <div class="input-field col s11">
                                    <input type="text" id="byr_namaasuransi" name="byr_namaasuransi" placeholder="Nama Asuransi">
                                    <label for="byr_namaasuransi">Nama Asuransi</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s11">
                                    <input type="text" id="byr_nomorasuransi" name="byr_nomorasuransi" placeholder="No. Asuransi">
                                    <label for="byr_nomorasuransi">Nomor Asuransi</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-panel">
                    <div class="row">
                        <div class="col s12 m8 l9">
                            <button class="btn light-green waves-effect waves-light darken-4" type="button" id="savePendaftaran">
                                <i class="mdi-navigation-check left"></i>Simpan
                            </button>
                            <button class="btn light-blue waves-effect waves-light darken-4" type="button" id="printPendaftaran" disabled>
                                <i class="mdi-action-print left"></i>Print
                            </button>
                            <a href="<?php echo base_url() ?>rekam_medis/pendaftaran" class="btn yellow waves-effect waves-light darken-4" type="button">
                                <i class="mdi-navigation-refresh left"></i>Ulang
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>
</section>
<!-- END CONTENT -->
<div id="modal_list_pasien" class="modal" style="width: 80% !important ;">
    <!--<div class="modal-dialog" role="document">-->
    <div class="modal-content">
        <h1>List Pasien</h1>
        <div class="divider"></div>
        <div id="card-alert" class="card green modal_notif" style="display:none;">
            <div class="card-content white-text">
                <p id="modal_card_message"></p>
            </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="col s12">
            <div id="table-datatables">
                <div class="row">
                    <div class="col s12 m4 l12">
                        <table id="table_list_pasien" class="responsive-table display" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No. Rekam Medik</th>
                                    <th>Nama Pasien</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat Pasien</th>
                                    <th>Pilih</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6">No Data to Display</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--</div>-->
    <div class="modal-footer">
        <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
    </div>
</div>
<?php $this->load->view('footer');?>
