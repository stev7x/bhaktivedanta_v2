<?php $this->load->view('header_iframe');?>
<body>
  
  
<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div> 
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Instalasi</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->instalasi_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>  
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">  
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                              $tgl_lahir = $list_pasien->tanggal_lahir;
                                            $umur  = new Datetime($tgl_lahir);
                                            $today = new Datetime();
                                            $diff  = $today->diff($umur);
                                            echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="instalasi_nama" type="hidden" name="instalasi_nama" class="validate" value="<?php echo $list_pasien->instalasi_nama; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Diagnosa</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Tindakan</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-icd9" class="nav-link" aria-controls="icd9" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">ICD 9</span></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content"> 
                <!-- diagnosa -->
                <div role="tabpanel" class="tab-pane active" id="diagnosa">
                    <table id="table_diagnosa_pasienrd" class="table table-striped dataTable" cellspacing="0">
                        <thead>  
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>   
                                <th>Keterangan</th>
                                <th>Diagnosa By</th>
                                <th>Aksi</th>
                            </tr> 
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="6">No Data to Display</td>
                             </tr>
                        </tbody> 
                        <tfoot>
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>
                                <th>Keterangan</th>
                                <th>Diagnosa By</th>
                                <th>Aksi</th> 
                            </tr> 
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="diagnosapasien_id" id="diagnosapasien_id">
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                        <div >
                            <p id="card_message"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="control-label">
                            <span style="color: red;">*</span>Input Diagnosa</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_diagnosa" name="kode_diagnosa" placeholder="Kode Diagnosa">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" title="Lihat List Diagnosa" data-toggle="modal" data-target="#modal_diagnosa" onclick="dialogDiagnosa()"><i class="fa fa-list"></i></button>
                            </span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label">
                            <span style="color: red;">*</span>Nama Diagnosa</label>
                            <input type="text" class="form-control" id="nama_diagnosa" name="nama_diagnosa" placeholder="Nama Diagnosa">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label">&nbsp;
                            <span style="color: red;">*</span>Jumlah Diagnosa</label>
                            <select name="jenis_diagnosa" id="jenis_diagnosa" class="form-control" >
                                <option value="" disabled selected>Pilih Jenis Diagnosa</option>
                                <option value="1">Diagnosa Utama</option>
                                <option value="2">Diagnosa Tambahan</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="form-group col-md-12" id="simpan_diagnosa" style="margin-top:26px;">
                                <button type="button" class="btn btn-success btn-md" id="saveDiagnosa"><i class="fa fa-floppy-o"></i> Simpan</button>
                            </div>
                            <div class="form-group col-md-12" id="edit_diagnosa" style="display:none;margin-top:26px;">
                                <button type="button" class="btn btn-success btn-md" id="editDiagnosa"><i class="fa fa-floppy-o"></i> Edit</button>
                            </div>
                        </div>
                    </div>
                 <?php echo form_close()?>  
                    <div class="clearfix"></div>
                </div>
                <!-- end diagnosa -->

                <!-- tindakan -->
                <div role="tabpanel" class="tab-pane" id="tabs-tindakan"> 
                    <table id="table_tindakan_pasienrd" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Harga</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Harga</th>
                                <th>Aksi</th>
                            </tr> 
                        </tfoot>
                    </table><br>
                    <div class="clearfix"></div>
                </div>
                <!-- end tindakan -->

                <!-- icd 9 -->
                <div role="tabpanel" class="tab-pane" id="tabs-icd9"> 
                    <table id="table_icd9" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Kode ICD 9</th>
                                <th>Nama ICD 9</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                 <th>Kode ICD 9</th>
                                <th>Nama ICD 9</th>
                                <th>Aksi</th>
                            </tr> 
                        </tfoot>
                    </table><br>
                    <?php echo form_open('#',array('id' => 'fmCreateIcd9'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="tindakan_icd_id" id="tindakan_icd_id">
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                        <div >
                            <p id="card_message"></p>
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <label class="control-label">
                        <span style="color: red;">*</span>Kode ICD 9</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="kode_icd_9_cm" name="kode_icd_9_cm" placeholder="Kode ICD 9">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button" title="Lihat List Diagnosa" data-toggle="modal" data-target="#modal_icd9" onclick="dialogIcd9()"><i class="fa fa-list"></i></button>
                          </span>
                        </div>
                    </div>
                    <div class="form-group col-md-5" >
                        <label class="control-label">
                        <span style="color: red;">*</span>Nama ICD 9</label>
                        <input type="text" class="form-control" id="nama_icd_9_cm" name="nama_icd_9_cm" placeholder="Nama ICD 9">
                    </div>
                    <div class="col-md-2" >
                        <div class="form-group col-md-12" id="simpan_icd9" style="margin-top:26px;">
                            <button type="button" class="btn btn-success btn-md" id="saveIcd9"><i class="fa fa-floppy-o"></i> Simpan</button>
                        </div>


                        <div class="form-group col-md-12" id="edit_icd9" style="display:none;margin-top:26px;" >
                            <button type="button" class="btn btn-success btn-md" id="editIcd9"><i class="fa fa-floppy-o"></i> Edit</button>
                        </div>
                    </div>
                 <?php echo form_close()?>  
                    <div class="clearfix"></div>
                </div>
                <!-- end icd 9 -->

            </div>   
        </div>
    </div>
</div>
 
  
 
 


<div id="modal_diagnosa" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Diagnosa</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                        <div class="table">
                            <table id="table_diagnosa_rd" class="table table-striped dataTable" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Kode Diagnosa</th>
                                        <th>Nama Diagnosa</th>
                                        <th>Kelompok Diagnosa</th>
                                        <th>Pilih</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="modal_icd9" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Diagnosa</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                        <div class="table">
                            <table id="table_list_icd_9" class="table table-striped dataTable" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Kode ICD 9</th>
                                        <th>Nama ICD 9</th>
                                        <th>Pilih</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



 
<?php $this->load->view('footer_iframe');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/rekam_medis/pasienrd/periksa_pasienrd.js"></script>
</body>
 
</html>