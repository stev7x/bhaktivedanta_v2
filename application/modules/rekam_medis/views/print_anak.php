<!DOCTYPE html>
<head>

<title>Untitled Document</title>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> -->     
<style type="text/css">
body,td,th {
	font-size: 16px;
	font-family: "Times New Roman", Times, serif;
}
.image-border {    
     border: 30px solid transparent;  
     border-left:  24px solid transparent;  
     border-right:  24px solid transparent;  
    padding: 15px;            
    border-image: url(../../../assets/plugins/images/border-collapse.png) 31% round;   
    background-image: url(../../../assets/plugins/images/g.png);
    max-width: 900px;    
    background-size: cover;
    /*background-position: center;*/
     /*background-repeat: no-repeat;*/
    /*opacity: 0.2;    */
} 
@media print{        
  .image-border{display:inline;}
} 
</style>         
</head>
 
<body>     
<table  align="center"  cellpadding="1" class="image-border" style="display: block;">   
  <tr>
    <!-- <td valign="top" width="13%">&nbsp;</td> -->    
    <td valign="top" colspan="6" align="center"><img src="<?php echo base_url() ?>/assets/plugins/images/header.png"
     ></td>
    <!-- <td valign="top" valign="top" width="12%">&nbsp;</td> -->
  </tr>
  <tr>
    <td valign="top" colspan="6" align="center"><h1>SURAT KETERANGAN LAHIR</h1></td>
  </tr>
  <tr>
    <td valign="top" colspan="6" align="center">Yang bertanda tangan di bawah ini : Dokter / Bidan <span style="margin-left: 10px;border-bottom: 1px dashed black"><?= $data_pasien->nama_dokter ?></span></td>
  </tr>  
  <tr>
    <td width="13%" valign="top">&nbsp;</td>
    <td valign="top" colspan="5">Menerangkan Bahwa :</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top" width="4%">&nbsp;</td>
    <td valign="top" width="21%">Nyonya </td>
    <td valign="top" width="1%">:</td>
    <td valign="top" width="31%"><?= $data_pasien->nama_ibu ?></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>  
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">Istri dari Tuan</td>
    <td valign="top">:</td>
    <td valign="top"><?= $data_pasien->nama_ayah ?></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">Alamat Rumah</td>
    <td valign="top">:</td>
    <td valign="top"><?= $data_pasien->alamat ?></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">:</td> 
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">Hari / Tanggal </td>
    <td valign="top">:</td>
    <td valign="top"><?= $data_pasien->tgl_lahir ?></td>
    <td valign="top">Pagi / Siang / Sore / Malam</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">Pukul</td>
    <td valign="top">:</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top" colspan="3">Telah melahirkan seorang anak laki - laki / perempuan</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">Dengan diberi nama</td>
    <td valign="top">:</td>
    <td valign="top"><?= $data_pasien->nama_anak ?></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">Anak ke</td>
    <td valign="top">:</td>
    <td valign="top"><?= $data_pasien->anak_ke ?></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top" rowspan="5">&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">Panjang Badan</td>
    <td valign="top">:</td>
    <td valign="top"><?= $data_pasien->panjang_badan ?></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">Berat Badan</td>
    <td valign="top">:</td>
    <td valign="top"><?= $data_pasien->berat_badan ?></td>
  </tr> 
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" valign="top">&nbsp;</td>
  </tr>
</table>  
<!-- <script> 
 $(document).ready(function(){         
        $('.image-border').css('background-image','url("<?php echo base_url()?>assets/plugins/images/ibu_anak.jpg")'); 
        // $("#notif").fadeTo(1100, 500).slideUp(1000, function(){
          console.log("test");
            // $("#notif").hide(); 
        // });       
    });          
    </script> -->
    
</body> 
</html>
