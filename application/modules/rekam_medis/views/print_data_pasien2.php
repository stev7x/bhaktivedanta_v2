<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print Identitas Pasien</title>

<style type="text/css">
    body{
      padding:0;
      margin:0;
    }
    table {
        border-collapse: collapse;
        /*text-align: center center;*/
      }
    
  </style>

</head>

<body onload="window.print()">
<table width="800" border="0">
  <tr>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><h2>IDENTITAS PASIEN</h2>
    <p>&nbsp;</p></td>
  </tr>
</table>

<table width="800" height="678" border="1" bordercolor="#000000">
  <tr>
    <td width="393" valign="middle">Nama Lengkap Pasien : <b><?php echo $data_pasien->pasien_nama; ?></b></td>
    <td colspan="2"><p>No. RM : <b>RM<?php echo $data_pasien->no_rekam_medis; ?></b></p>
    <p>No. Reg : <b><?php echo $data_pasien->no_pendaftaran; ?></b></p></td>
  </tr>
  <tr>
    <td height="31" valign="middle">No. KTP / SIM : <b><?php echo $data_pasien->no_identitas; ?></b></td>
    <td colspan="2" rowspan="2" valign="top"><p>Ruang :</p>
    <p>Kelas :</p></td>
  </tr>
  <tr>
    <td height="28">Tempat , Tanggal Lahir : <b><?php echo $data_pasien->tempat_lahir;?>, <?php echo date('d F Y', strtotime($data_pasien->tanggal_lahir)); ?></b></td>
  </tr>
  <tr>
    <td><p>Jenis Kelamin : <b><?php echo $data_pasien->jenis_kelamin; ?></b></p></td>
    <td colspan="2">Kasus Polisi :</td>
  </tr>
  <tr>
    <td valign="top">
    <p>Alamat : <b><?php echo $data_pasien->pasien_alamat; ?></b></p>
    <p>Desa/Kelurahan : <b><?php echo $data_pasien->kelurahan_nama; ?></b></p>
    <p>Kecamatan : <b><?php echo $data_pasien->kecamatan_nama; ?></b></p>
    <p>Kota/Kabupaten : <b><?php echo $data_pasien->kabupaten_nama; ?></b></p>
    </td>
    <td colspan="2" valign="top">
    <p>Nama Penanggung Jawab Pembayaran : <b><?php echo $data_pasien->nama_pj; ?></b></p>
   	    <p>Hubungan dengan pasien : <b><?php echo $data_pasien->status_hubungan_pj; ?></b></p>
       	<p>Alamat : <b><?php echo $data_pasien->alamat_pj; ?></b></p>
    <p>Telp/HP : <b><?php echo $data_pasien->no_ponsel; ?></b></p></td>
  </tr>
  <tr>
    <td><p>Pendidikan : <b><?php echo $data_pasien->pendidikan_nama; ?></b></p></td>
    <td colspan="2">Nama Asuransi : </td>
  </tr>
  <tr>
    <td><p>Agama : <b><?php echo $data_pasien->agama_nama; ?></b></p></td>
    <td colspan="2">Asal Masuk : </td>
  </tr>
  <tr>
    <td><p>Kebangsaan : <b><?php echo $data_pasien->warga_negara; ?></b></p></td>
    <td colspan="2">Nama Petugas Admission :</td>
  </tr>
  <tr>
    <td><p>Pekerjaan : <b><?php echo $data_pasien->nama_pekerjaan; ?></b></p></td>
    <td colspan="2">Nama Perawat Penerima Pasien :</td>
  </tr>
  <tr>
    <td height="224" valign="top"><p>Status : <b><?php echo $data_pasien->status_kawin; ?></b></p>
    <p>Dokter Penanggung Jawab : <b><?php echo $data_pasien->NAME_DOKTER; ?></b></p></td>
    <td colspan="2" valign="top"><p>Orang Yang Dapat Dihubungi Dalam keadaan darurat :
    </p>
      <p>Nama : <b><?php echo $data_pasien->nama_pj; ?></b></p>
    <p>Hubungan dengan pasien : <b><?php echo $data_pasien->status_hubungan_pj; ?></b></p>
    <p>Alamat : <b><?php echo $data_pasien->alamat_pj; ?></b></p>
    <p>No. HP : <b><?php echo $data_pasien->no_ponsel; ?></b></p></td>
  </tr>
  <tr>
    <!-- <td height="30" colspan="2">Tanggal Masuk :	Bulan : Tahun : Jam : </td> -->
    <td height="30" colspan="2">Tanggal Masuk :	<b><?php echo date('d/m/Y H:i:s', strtotime($data_pasien->tgl_pendaftaran)); ?></b> </td>
    <td width="265" rowspan="2" valign="top"><p>Lama Di Rawat : hari</p></td>
  </tr>
  <tr>
    <td height="30" colspan="2">Tanggal Keluar :&nbsp;&nbsp;&nbsp;&nbsp;  Bulan :&nbsp;&nbsp;&nbsp;&nbsp; Tahun :&nbsp;&nbsp;&nbsp; Jam :&nbsp;&nbsp;&nbsp; </td>
  </tr>
</table>
</body>
</html>
