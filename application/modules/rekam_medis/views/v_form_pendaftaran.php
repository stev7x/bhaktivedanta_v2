<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
 <style type="text/css" media="screen">
        .hide{
            display: none;
        }
        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: #686868 !important;
            text-decoration: bold !important;
        }


        option, option:selected {
          color:#686868;
        }

        input,
        input::-webkit-input-placeholder {
            font-size: 14px;
            line-height: 3;
        }
        .mt10 { margin-top: -10px; }
        .mt5  { margin-top: -5px; }
    </style>

<?php

//    echo "<pre>";
//    print_r($data_pasien);
//    exit;

    if (isset($data_pasien)) {
        $reservasi_id =        $reservasi->reservasi_id;
        $kode_booking =         $reservasi->kode_booking;
        $no_urut =              $reservasi->no_urut;
        $no_rekam_medis =       $data_pasien->no_rekam_medis;
        $nama_pasien =          $reservasi->nama_pasien;
        $alamat_pasien =        @$reservasi->alamat_pasien != '' ? $reservasi->alamat_pasien : $data_pasien->pasien_alamat;
        $jenis_kelamin =        $data_pasien->jenis_kelamin;
        $tanggal_lahir =        date('d M Y', strtotime($data_pasien->tanggal_lahir));
        $asuransi1 =            $reservasi->asuransi1;
        $asuransi2 =            $reservasi->asuransi2;
        $dokter_id =            $reservasi->dokter_id;
        $tanggal_reservasi =    $reservasi->tanggal_reservasi;
        $metode_registrasi =    $reservasi->metode_registrasi;
        $keterangan =           $reservasi->keterangan;
        $status =               $data_pasien->status;
        $tgl_created =          $reservasi->tgl_created;
        $no_pendaftaran =       $reservasi->no_pendaftaran;
        $status_pasien =        $reservasi->status_pasien;
        $no_telp =              $reservasi->no_telp;
        $is_reschedule =        $reservasi->is_reschedule;
        $alasan_id =            $reservasi->alasan_id;
        $catatan =              $reservasi->catatan;
        $sumber_reservasi =     $reservasi->sumber_reservasi;
        $jenis_pelayanan_id =   $reservasi->jenis_pelayanan_id;
        $jenis_pasien_id =      $reservasi->jenis_pasien_id;
        $hotel_id =             $reservasi->hotel_id;
        $room_number =         $reservasi->room_number;
        $pic_hotel =            $reservasi->pic_hotel;
        $driver_id =            $reservasi->driver_id;
        $pemanggilan =          $reservasi->pemanggilan;
        $branch_id =            $reservasi->branch_id;
        $tgl_lahir_ortu =       @$data_pasien->tgl_lahir_ortu;
        $tgl_lahir_suami =      @$data_pasien->tgl_lahir_suami_istri;
        $next_no_rm =           $data_pasien->no_rekam_medis;
        $pembayaran_id =        $reservasi->pembayaran_id;
        $tempat_lahir =         @$data_pasien->tempat_lahir;
        $weight =               @$data_pasien->weight;
        $warga_negara =         @$data_pasien->warga_negara == "" ? 0 : $data_pasien->warga_negara;
        $present_complain =     @$data_pasien->present_complain;
        $email =                @$data_pasien->email;
        $propinsi_id =          @$data_pasien->propinsi_id;
        $kota_id =              @$data_pasien->kabupaten_id;
        $kecamatan_id =         @$data_pasien->kecamatan_id;
        $keluranan_id =         @$data_pasien->kelurahan_id;
    } else {
        $reservasi_id =         "";
        $kode_booking =         "";
        $no_urut =              "";
        $no_rekam_medis =       "";
        $nama_pasien =          "";
        $alamat_pasien =        "";
        $jenis_kelamin =        "";
        $tanggal_lahir =        "";
        $tempat_lahir =         "";
        $asuransi1 =            "";
        $asuransi2 =            "";
        $kelas_pelayanan_id =   "";
        $dokter_id =            "";
        $jam =                  "";
        $tanggal_reservasi =    "";
        $metode_registrasi =    "";
        $keterangan =           "";
        $status =               "";
        $status_verif =         "";
        $tgl_verif =            "";
        $tgl_created =          "";
        $no_pendaftaran =       "";
        $status_pasien =        "";
        $no_telp =              "";
        $is_reschedule =        "";
        $alasan_id =            "";
        $catatan =              "";
        $sumber_reservasi =     "";
        $jenis_pelayanan_id =   "";
        $jenis_pasien_id =      "";
        $hotel_id =             "";
        $pic_hotel =            "";
        $driver_id =            "";
        $pemanggilan =          "";
        $branch_id =            "";
        $tgl_lahir_ortu =       "";
        $tgl_lahir_suami =      "";
        $umur =                 "";
        $pembayaran_id =        "";
        $weight =               "";
        $present_complain =     "";
        $email =                "";
        $propinsi_id =          "";
        $kota_id =              "";
        $kecamatan_id =         "";
        $keluranan_id =         "";
        $room_number =          "";
    }
?>

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Pendaftaran Pasien</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <!-- <li><a href="#">Dashboard</a></li> -->
                            <li><a href="#">Rekam Medis</a></li>
                            <li class="active">Pendaftaran</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <?php echo form_open('#',array('id' => 'fmCreatePendaftaran', 'data-toggle' => 'validator'))?>
                <input type="hidden" name="is_retension" id="is_retension" value="0">
                <input type="hidden" name="reservasi_id" id="reservasi_id" value="0">
            <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                         <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <h3 class="box-title m-b-0">Data Pasien</h3>
                                        <p class="text-muted m-b-30"> <!-- Bootstrap Form Validation --></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-5">
                                                     <button class="btn btn-success" type="button" id="printDataPasien" hidden><i class="fa fa-print"></i> PRINT DATA PASIEN</button>
                                                 </div>
                                                <!-- <div class="col-sm-5">
                                                     <button hidden class="btn btn-success " id="printKartu" ><i class="fa fa-print"></i> PRINT KARTU BEROBAT</button>
                                                 </div>
                                                <div class="col-sm-5">
                                                     <button hidden class="btn btn-info " id="printPendaftaran" ><i class="fa fa-print"></i> PRINT DATA PASIEN</button>
                                                 </div>  -->
                                                 <div class="col-sm-2">
                                                    <a hidden href="<?php echo base_url() ?>rekam_medis/pendaftaran" class="btn btn-danger" type="button" id="reset">
                                                     <i class="fa fa-repeat"></i> RESET
                                                     </a>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right col-sm-4 ">
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                                                <div class="input-group custom-search-form">
                                                      <input readonly type="text" class="form-control autocomplete" id="cari_no_rekam_medis" placeholder="Cari Berdasarkan No.Rekam Medis ..."> <span class="input-group-btn">
                                                        <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_pasien" data-backdrop="static" data-keyboard="false"  type="button" onclick="dialogPasien()"> <i class="fa fa-bars"></i> </button>
                                                    </span>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                        <div >
                                            <p id="card_message"></p>
                                        </div>
                                    </div>
                                </div>
                               <hr style="margin-top: -10px;padding-bottom: 18px;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- <form data-toggle="validator"> -->
                                <h3 class="mt10" style="margin-bottom: 40px">Personal Information</h3>
                                <div class="row">
                                        <div class="form-group col-sm-6" style="margin-top: -29px" >
                                            <input id="pasien_id" type="hidden" name="pasien_id" readonly>
                                            <input id="status_pasien" type="hidden" name="status_pasien" readonly>
                                            <label for="inputName1" class="control-label">No.Referensi RM Orang Tua</label>
                                            <div class="input-group custom-search-form">
                                                  <input readonly type="text" id="no_rm_ref" name="no_rm_ref" class="form-control" placeholder="Cari No Referensi RM"> <span class="input-group-btn">
                                                    <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_ortu" data-backdrop="static" data-keyboard="false"  type="button" onclick="dialogOrtu()"> <i class="fa fa-bars"></i> </button>
                                                </span>
                                             </div>
                                        </div>
                                    <div id="berat" class="form-group col-sm-6" style="margin-top: -29px" hidden>
                                        <label for="inputPassword" class="control-label">Berat Bayi <span style="color:red">*</span></label>
                                        <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "3" class="form-control no" placeholder="Berat Bayi" name="berat_bayi" id="berat_bayi"  required>
                                    </div>
                                </div>

                                    <div style=" margin-bottom: 4px">
                                        <input type="checkbox" name="is_bayi" id="is_bayi" ><label for="is_bayi" style="color: red; font-size: 11px; margin-top-2px; ">*Jika Pasien bayi</label>

                                    </div>

                                    <div class="form-group" style="display: none">
                                        <label for="inputName1" class="control-label">No.Rekam Medis <span style="color: red;">*</span></label>
                                        <!-- <input type="hidden" id="reservasi_id" value="<?php //@$reservasi_id ?>"> -->
                                        <!-- <input id="no_rm" type="text" name="no_rm" value="<?php //$pasien->no_rekam_medis ?>" class="validate form-control" value="<?php //echo $next_no_rm; ?>" placeholder="No.Rekam Medis" readonly> -->
                                        <input type="hidden" name="no_pendaftaran" id="no_pendaftaran" value="<?= $no_pendaftaran ?>" >
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="inputTypeID" class="control-label">Type Identitas</label>
                                                <select id="type_id" name="type_id" class="form-control" onchange="getType(this);">
                                                    <option value="" disabled selected>Type ID</option>
                                                    <?php
                                                        foreach($list_type_identitas as $list){
                                                            echo "<option value='".$list."'>".$list."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                            <label for="inputPassword" class="control-label">No.Identitas</label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "16" class="form-control no" placeholder="No.Identitas" name="no_identitas" id="no_identitas" onchange="validasi_ktp()" >
                                                <span class="message_ktp" style="color: red;font-size: 14px;display: none;"></span>
                                                <!-- <div class="help-block with-errors" id="val_ktp"></div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="no_bpjs" class="control-label">No Rekam Medis <span style="color:red">*</span></label>
                                            <div class="input-group custom-search-form">
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "12" type="text" class="form-control no" id="no_rm" name="no_rm" placeholder="No Rekam Medis" value="<?php echo $next_no_rm; ?>" onchange="validasi_no_rm()">
                                                <button class="btn btn-info" title="Lihat Retensi Pasien" data-toggle="modal" data-target="#modal_retensi_pasien" data-backdrop="static" data-keyboard="false"  type="button"> Retensi </button>
                                            </div>
                                            <span class="message_no_rm" style="color: red;font-size: 14px;display: none;"></span>
                                            <div class="help-block with-errors" id="val_no_rm"></div>
                                            <input type="hidden" name="is_no_rm" id="is_no_rm">
                                            <input type="hidden" name="reservasi_id" id="reservasi_id" value="<?= $reservasi_id ?>" >
                                        </div>
<!--                                        <div class="form-group col-sm-6">-->
<!--                                            <label for="no_bpjs" class="control-label">No.BPJS</label>-->
<!--                                            <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "13" class="form-control no" id="no_bpjs" name="no_bpjs" onchange="validasi_bpjs()" placeholder="No.BPJS">-->
<!--                                            <span class="message_bpjs" style="color: red; font-size: 14px;display: none;"></span>-->
<!--                                            <div class="help-block with-errors"></div>-->
<!--                                        </div>-->
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="nama_pasien" class="control-label">Patient Name <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" id="nama_pasien" name="nama_pasien" placeholder="Patient Name"  data-error="Data tidak boleh kosong" required value="<?= $nama_pasien; ?>">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="textarea" class="control-label">Sex <span style="color: red;">*</span></label>
                                             <select class="form-control selectpicker" id="jenis_kelamin" name="jenis_kelamin">
                                                <option value="" disabled>Sex</option>
                                                <?php
                                                    foreach($list_jenis_kelamin as $key => $value){
                                                        if ($key == $jenis_kelamin)
                                                            echo "<option value='".$key."' selected>".$value."</option>";
                                                        else 
                                                            echo "<option value='".$key."'>".$value."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <!-- <div class="form-group col-sm-6">
                                            <label for="nama_pasien" class="control-label">Nama Panggilan</label>
                                            <input type="text" class="form-control" id="nama_panggilan" name="nama_panggilan" placeholder="Nama Panggilan" data-error="Data tidak boleh kosong" required>
                                            <div class="help-block with-errors"></div>
                                        </div> -->
                                    </div>
                                    <div class="row mt10">
                                        
                                        <div class="form-group col-sm-6">
                                            <label for="tempat_lahir" class="control-label">Place of Birth </label>
                                            <input id="tempat_lahir" type="text" name="tempat_lahir" class="form-control" placeholder="Place of Birth" data-error="Data tidak boleh kosong" value="<?= $tempat_lahir ?>" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="textarea" class="control-label">Nationality <span style="color: red;">*</span></label>
                                            <select id="warga_negara" name="warga_negara" class="form-control selectpicker">
                                                <option value="" disabled selected>Choose one</option>
                                                <?php
                                                // die($this->Reservasi_model->get_layanan());
                                                    $list_warganegara = $this->Pendaftaran_model->get_warganegara();
                                                    foreach($list_warganegara as $list){
                                                        if ($list->id == $warga_negara) {
                                                            echo "<option value='".$list->id."' selected>".$list->nama_negara."</option>";
                                                        } else if ($jenis_pasien_id != 3) {
                                                            if ($list->id == 64) {
                                                                echo "<option value='".$list->id."' selected>".$list->nama_negara."</option>";
                                                            }
                                                        } else {
                                                            echo "<option value='".$list->id."'>".$list->nama_negara."</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            
                                        </div>
                                    </div>


                                    <div>
                                        <div class="row mt10">
                                            <div class="form-group col-sm-6">
                                                <label for="tanggal_lahir" class="control-label">Date of Birth</label>
                                                <div class="input-group">
                                                    <input name="tgl_lahir" id="tgl_lahir" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" onchange="setUmur()" value="<?= $tanggal_lahir; ?>"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="inputPassword" class="control-label">Age</label>
                                                <input type="text" name="umur" id="umur" class="form-control" placeholder="Age" readonly required value="<?= $umur; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="jenis_pasien" class="control-label">Patient Type <span style="color: red;">*</span></label>
                                            <select class="form-control" id="jenis_pasien" name="jenis_pasien" onchange="patientType(this);">
                                                <option disabled>Choose</option>
                                                    <?php
                                                    // die($this->Reservasi_model->get_layanan());
                                                    $list_instalasi = $this->Reservasi_model->get_jenis_pasien();
                                                    foreach($list_instalasi as $list){
                                                        if ($list->id_jenis_pasien == $jenis_pasien_id)
                                                            echo "<option value='".$list->id_jenis_pasien."' selected>".$list->nama_jenis_pasien."</option>";
                                                        else
                                                            echo "<option value='".$list->id_jenis_pasien."'>".$list->nama_jenis_pasien."</option>";

                                                    }
                                                    ?>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label  class="control-label">Service Type <span style="color:red">*</span></label>
                                            <select class="form-control" id="type_call" name="type_call">
                                                <option disabled>Choose</option>
                                                <?php
                                                    // die($this->Reservasi_model->get_layanan());
                                                    $list_instalasi = $this->Reservasi_model->get_layanan();
                                                    foreach($list_instalasi as $list){
                                                        if ($list->id_layanan == $jenis_pelayanan_id)
                                                            echo "<option value='".$list->id_layanan."' selected>".$list->nama_layanan."</option>";
                                                        else 
                                                            echo "<option value='".$list->id_layanan."'>".$list->nama_layanan."</option>";

                                                    }
                                                    ?>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <input type="checkbox" name="cek_insurance" id="cek_insurance" ><label for="cek_insurance" style=" margin-top-2px; ">Are you holding insurance?</label>
                                        </div>
                                        
                                    </div>
                                    <div id="div_insurance" class="row" style="display: none; margin-bottom: 20px;">
<!--                                        <div class="form-group col-sm-6" >-->
<!--                                            <label for="" class="control-label">Insurance</label>-->
<!--                                            <select class="form-control" id="name_insurance" name="name_insurance">-->
<!--                                                <option value="" selected disabled>Choose</option>-->
<!--                                                <option value="company">Insurance Company</option>-->
<!--                                                <option value="travel">Travel Insurance</option>-->
<!--                                            </select>-->
<!--                                        </div>-->
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword" class="control-label">Insurance company</label>
                                            <select class="form-control" id="asuransi1" name="asuransi1">
                                                <option value="" selected>PILIH</option>
                                                <?php
                                                $list_asuransi = $this->Reservasi_model->get_asuransi();
                                                foreach($list_asuransi as $list){
                                                    echo "<option value='".$list->nama."'>".$list->nama."</option>";
                                                }
                                                ?>
                                            </select>
                                            <label for="no_asuransi1" class="control-label">Nomor Asuransi</label>
                                            <input type="text" class="form-control" id="no_asuransi1">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword" class="control-label">Travel Insurance</label>
                                            <select class="form-control" id="asuransi2" name="asuransi2">
                                                <option value="" selected>PILIH</option>
                                                <?php
                                                $list_asuransi = $this->Reservasi_model->get_asuransi();
                                                foreach($list_asuransi as $list){
                                                    echo "<option value='".$list->nama."'>".$list->nama."</option>";
                                                }
                                                ?>
                                            </select>
                                            <label for="no_asuransi2" class="control-label">Nomor Asuransi</label>
                                            <input type="text" class="form-control" id="no_asuransi2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="nama_pasien" class="control-label">Email <span id="mandatory-email" style="color:red; display: none;">*</span></label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Email"  data-error="Data tidak boleh kosong" value="<?= $email ?>" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label  class="control-label">No. Tlp/HP</label>
                                            <input type="number" class="form-control no" id="no_mobile" name="no_mobile" placeholder="No.HP" value="<?= $no_telp ?>">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>


                                <div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Address <span style="color: red;">*</span></label>
                                        <textarea id="alamat" name="alamat" data-error="Data tidak boleh kosong" class="form-control" required><?= $alamat_pasien; ?></textarea>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                </div>

                                <div class="row mt5">
                                    <div class="form-group col-sm-6" >
                                        <label for="inputPassword" class="control-label">State <span style="color:red">*</span></label>
                                        <select class="form-control select2" id="propinsi" name="propinsi" onchange="getKabupaten()">
                                            <!-- <option>Select</option> -->
                                            <!-- <option value="" disabled selected>Pilih Provinsi</option> -->
                                            <?php
                                            $list_propinsi = $this->Pendaftaran_model->get_propinsi();

                                            foreach($list_propinsi as $list){
                                                if ($list->propinsi_id == 17) {
                                                    echo "<option value='".$list->propinsi_id."' selected>".$list->propinsi_nama."</option>";
                                                } else{
                                                    echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                    <label for="inputPassword" class="control-label">City <span style="color:red">*</span></label>
                                        <select class="form-control select2" id="kabupaten" name="kabupaten" onchange="getKecamatan()">
                                            <option value="" selected>Choose State <span style="color:red">*</span></option>
                                            <?php
                                            $list_kabupaten = $this->Pendaftaran_model->get_kabupaten(17);

                                            foreach($list_kabupaten as $list){
                                                if ($list->kabupaten_id == 250) {
                                                    echo "<option value='".$list->kabupaten_id."' selected>".$list->kabupaten_nama."</option>";
                                                } else{
                                                    echo "<option value='".$list->kabupaten_id."'>".$list->kabupaten_nama."</option>";
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="inputPassword" class="control-label">Sub District <span style="color:red">*</span></label>
                                            <select class="form-control select2" id="kecamatan" name="kecamatan" onchange="getKelurahan()">
                                                <option value="" selected>Choose one</option>
                                                <?php
                                                $list_kecamatan = $this->Pendaftaran_model->get_kecamatan(250);

                                                foreach($list_kecamatan as $list){
													if ($list->kecamatan_id == 3361) {
														echo "<option value='".$list->kecamatan_id."' selected>".$list->kecamatan_nama."</option>";
													} else{
														echo "<option value='".$list->kecamatan_id."'>".$list->kecamatan_nama."</option>";
													}
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-6">
                                        <label for="inputPassword" class="control-label">Village <span style="color:red">*</span></label>
                                            <select id="kelurahan" name="kelurahan" class="form-control select2" onchange="getKel()">
                                                <option value="" selected>Choose one</option>
												<?php
                                                $list_kelurahan = $this->Pendaftaran_model->get_kelurahan(3361);

                                                foreach($list_kelurahan as $list){
													if ($list->kelurahan_id == 46265) {
														echo "<option value='".$list->kelurahan_id."' selected>".$list->kelurahan_nama."</option>";
													} else{
														echo "<option value='".$list->kelurahan_id."'>".$list->kelurahan_nama."</option>";
													}
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div  style=" width: 100%;">
                                                <div class="form-group col-md-6" id="form-hotel">
                                                    <label for="inputPassword" class="control-label">Hotel</label>
                                                    <input type="hidden" name="id_hotel" id="id_hotel" value="<?= $hotel_id; ?>">
                                                    <select id="hotel" class="form-control"  name="hotel">
                                                        <option value="" disabled selected>Choose</option>
                                                        <?php
                                                            $lists = $this->Pendaftaran_model->get_hotel();
                                                            foreach ($lists as $item) {
                                                                if ($item->id_hotel == $hotel_id) 
                                                                    echo "<option value='".$item->id_hotel."' selected>".$item->nama_hotel."</option>";
                                                                else
                                                                    echo "<option value='".$item->id_hotel."'>".$item->nama_hotel."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6" >
                                                    <input type="checkbox" name="cek_hotel" id="cek_hotel" style=""><span for="cek_hotel" id="others" style="color: red"> * Others</span>
                                                    <div class="input-group" style="margin-top: 5px;">
                                                        <input type="text" id="hotel_nama" name="hotel_nama" class="form-control" placeholder="Nama Hotel" disabled >
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="alamat_hotel" class="control-label">Hotel Address : </label>
                                            <input type="text" class="form-control" id="alamat_hotel" name="alamat_hotel" placeholder="Hotel Address"  data-error="Data tidak boleh kosong" disabled >
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="tlp_hotel" class="control-label">Hotel Tlp : </label>
                                            <input type="text" class="form-control" id="tlp_hotel" name="tlp_hotel" placeholder="Hotel Tlp"  data-error="Data tidak boleh kosong" disabled>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label  class="control-label">Room Number</label>
                                            <input type="number" class="form-control no" id="room_number" name="room_number" placeholder="Room Number" data-error="Data tidak boleh kosong" value="<?= $room_number ?>" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div>
                                    <div class="form-group">

                                        <input type="checkbox" id="cek_alamat" name="cek_alamat" ><span style="font-size:10px; color:red;"> * Jika alamat KTP berbeda dengan tempat tinggal di Bali</span>

                                        <div class="help-block with-errors"></div>

                                    </div>
                                </div>

                                <div class="form-group" style="margin-top: -12px">
                                    <div id="alamat2" style="display: none;">
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <select class="form-control" name="cek_tinggal">
                                                    <option value="domisili">Domisili</option>
                                                    <option value="hotel">Hotel</option>
                                                    
                                                </select>
                                            </div>
                                            <div  style="display: none; width: 100%;">
                                                <div class="form-group col-md-6" id="form-hotel">
                                                    <label for="inputPassword" class="control-label">Hotel<span style="color: red;">*</span></label>
                                                    <select class="form-control"  name="hotel">
                                                        <option value="" disabled selected>PILIH</option>
                                                    </select>
                                                
                                                </div>
                                                <div class="form-group col-md-6" >
                                                    
                                                    <input type="checkbox" name="cek_hotel" id="" style=""><span for="cek_hotel" style="color: red"> * Lainya</span>
                                                    <div class="input-group" style="margin-top: 5px;">
                                                        <input type="text" id="hotel_nama" name="hotel_nama" class="form-control" placeholder="Nama Hotel" disabled >
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="domisili" style=" width: 100%;">
                                                
                            
                                            <div class="form-group col-sm-12">
                                                <label for="textarea" class="control-label">Alamat tinggal di Bali<span style="color: red;">*</span></label>
                                                <textarea id="alamat_domisili" name="alamat_domisili" id="alamat_domisili" data-error="Data tidak boleh kosong" class="form-control" required></textarea>
                                            </div>
                                            <div class="form-group col-md-6 col-sm-6" >
                                                <label for="inputPassword" class="control-label">Provinsi<span style="color: red;">*</span></label>
                                                <select class="form-control select2" id="provinsi_tinggal" name="provinsi_tinggal" onchange="getKabupatenTinggal()">
                                                    <option value="" disabled selected>Pilih Provinsi</option>
                                                    <?php
                                                    $list_propinsi = $this->Pendaftaran_model->get_propinsi();

                                                    foreach($list_propinsi as $list){

                                                        if ($list->propinsi_id == 15) {
                                                            echo "<option value='15' selected >JAWA TIMUR</option>";

                                                        } else{
                                                            echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";

                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 col-sm-6">
                                                <label for="inputPassword" class="control-label">Kota/Kabupaten<span style="color: red;">*</span></label>
                                                    <select class="form-control select2" id="kabupaten_tinggal" name="kabupaten_tinggal" onchange="getKecamatanTinggal()">
                                                        <option value=""  selected>Pilih Kota/Kabupaten</option>
                                                        <?php
                                                        $list_kabupaten = $this->Pendaftaran_model->get_kabupaten(15);

                                                        foreach($list_kabupaten as $list){

                                                            // if ($list->kabupaten_id == 229) {
                                                            //    echo "<option value='229' selected id='bangka'>BANGKALAN</option>";

                                                            // }else{
                                                            //     echo "<option value='".$list->kabupaten_id."'>".$list->kabupaten_nama."</option>";
                                                            // }
                                                            echo "<option value='".$list->kabupaten_id."'>".$list->kabupaten_nama."</option>";
                                                        }
                                                        ?>

                                                    </select>
                                            </div>
                                            <div class="form-group col-md-6 col-sm-6">
                                                <label for="inputPassword" class="control-label">Kecamatan<span style="color: red;">*</span></label>
                                                <select class="form-control select2" id="kecamatan_tinggal" name="kecamatan_tinggal" onchange="getKelurahanTinggal()">
                                                    <option value="" selected>Pilih Kecamatan</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 col-sm-6">
                                            <label for="inputPassword" class="control-label">Kelurahan/Desa<span style="color: red;">*</span></label>
                                                <select id="kelurahan_tinggal" name="kelurahan_tinggal" class="form-control select2">
                                                    <option value="" selected>Pilih Kelurahan</option>


                                                </select>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                            </div>
                            <div class="col-sm-6">
                                <h3 class="mt10" style="margin-bottom: 40px">Medical Information</h3>
                                
                                <div class="row" style="margin-top: -29px">
                                    <div class="form-group col-sm-6" >
                                        <label for="present_complain" class="control-label">Present Complain</label>
                                        <input type="text" class="form-control" name="present_complain" id="present_complain" placeholder="I have a headache" value="<?= $present_complain ?>" >
                                    </div>
                                    
                                </div>
                                    <!-- <div class="Suami_istri" style="display: none;">
                                        <div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                <label for="tgl_lahir" class="control-label">Tanggal Lahir</label>
                                                <div class="input-group">
                                                    <input name="tgl_suami_istri" id="tgl_suami_istri" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" onchange="setUmurSustri()"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>

                                                </div>
                                                <div class="form-group col-sm-6">

                                                <label for="umur" class="control-label">Umur</label>
                                                    <input id="umur_suami_istri" name="umur_suami_istri" type="text" class="form-control" readonly placeholder="Umur">

                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                   
                                            <label class="control-label">Date First Complain</label>
                                                <div class="input-group">
                                                    <input name="date_first_complain" id="date_first_complain" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" > <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                   
                                            <label class="control-label">Date First Consultation</label>
                                                <div class="input-group">
                                                    <input name="date_first_consultation" id="date_first_consultation" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" > <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 " >
                                            <label for="allergy_history" class="control-label">Allergy History</label>
                                            <input type="text" class="form-control" name="allergy_history" id="allergy_history" placeholder="Yes, For...."  >
                                        </div>
                                        <div class="form-group col-sm-6 " >
                                            <label for="pregnant" class="control-label">Pregnant</label>
                                            <input type="text" class="form-control" name="pregnant" id="pregnant" placeholder="Month "  >
                                        </div>
                                    </div>
                                    <!-- <div class="row"> -->
                                        <div class="form-group">
                                            <label for="textarea" class="control-label">Current Medication</label>
                                            <textarea id="current_medication" name="current_medication"  class="form-control" ></textarea>
                                            <div class="help-block with-errors"></div>

                                        </div>
                                    <!-- </div> -->
                                    <div class="row">
                                        <div class="form-group col-sm-6 " >
                                            <label for="weight" class="control-label">Weight</label>
                                            <input type="text" class="form-control" name="weight" id="weight" placeholder="Kg.."  value="<?= $weight ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Do you have any past medical history</label>
                                        <textarea id="medical_history" name="medical_history"  class="form-control"></textarea>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Please note all important medical event</label>
                                        <textarea id="medical_event" name="medical_event" class="form-control"></textarea>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                    


                                <!-- </form> -->
                            </div>

                        </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                            <div class="white-box ">
                                <h3 class="box-title m-b-0">Ringkasan Masuk</h3><hr>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Instalasi Tujuan <span style="color: red;">*</span></label>
                                            <select id="instalasi" name="instalasi" class="form-control selectpicker" onchange="getRuangan()">
                                                <option value="" disabled>Pilih Instalasi</option>
                                                <?php
                                                $list_instalasi = $this->Pendaftaran_model->get_instalasi();
                                                foreach($list_instalasi as $list){
                                                    if($list->instalasi_id == 1){
                                                        echo "<option value='".$list->instalasi_id."' selected>".$list->instalasi_nama."</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group hidden">
                                            <label class="control-label">Poliklinik/Ruangan<span style="color: red;">*</span></label>
                                            <select id="poli_ruangan" name="poli_ruangan" class="form-control selectpicker" onchange="getKelasPoliruangan()">
                                                <option value="" disabled>Pilih Poli/Ruangan</option>
                                            </select>
                                        </div>
                                        <div class="form-group hidden">
                                            <label class="control-label">Kelas Pelayanan<span style="color: red;">*</span></label>
                                            <select id="kelas_pelayanan" name="kelas_pelayanan" class="form-control selectpicker" onchange="getKamar()">
                                                <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Nama Perujuk</label>
                                            <div class="input-group custom-search-form">
                                                <input type="text" id="rm_namaperujuk" name="rm_namaperujuk" class="form-control" placeholder="Nama Perujuk" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-info" title="Lihat Data Perujuk" data-toggle="modal" data-target="#modal_list_rujuk"  data-backdrop="static" data-keyboard="false"  type="button" onclick="dialogPerujuk()"> <i class="fa fa-bars"></i> </button>
                                                </span>
                                            </div>
                                            <label class="control-label">Alamat Perujuk</label>
                                            <textarea id="rm_alamatperujuk" name="rm_alamatperujuk" class="form-control" placeholder="Alamat Perujuk" readonly style="width: 99%"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div id="toggle_hide_dokter" class="form-group">
                                            <input type="hidden" name="dokter_id" id="dokter_id" value="<?= $dokter_id; ?>">
                                            <label class="control-label">Dokter Penanggung Jawab <span style="color: red;">*</span></label>
                                            <select id="dokter" name="dokter" class="form-control selectpicker">
                                                <option value="" disabled selected>Pilih Dokter</option>
                                            </select>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="control-label">Jam Periksa</label>
                                            <input type="hidden" name="tgl_reservasi" id="tgl_reservasi" value="<?= $tanggal_reservasi; ?>">
                                            <input type="hidden" name="jam" id="jam" value="<?= $jam; ?>">
                                            <div class="input-group custom-search-form">
                                                <select class="form-control" id="jam_periksa" name="jam_periksa">
                                                    <option value="" disabled>Pilih</option>
                                                </select>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>

                            <div class="white-box ">
                                <h3 class="box-title m-b-0">PEMBAYARAN</h3><hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword" class="control-label">Type Pembayaran <span style="color: red;">*</span></label>
                                            <select id="byr_type_bayar" name="byr_type_bayar" onchange="cekTypeBayar()" class="form-control" >
                                                <?php
                                                    $pembayaran = "";
                                                    $is_asuransi = "style='display: none;'";
                                                    $nama_asuransi1 = "";
                                                    $nama_asuransi2 = "";
                                                    $no_asuransi1 = "";
                                                    $no_asuransi2 = "";
                                                    if (!empty($pembayaran_id)) {
                                                        $pembayaran_data = $this->Pendaftaran_model->get_pembayaran($pembayaran_id);
                                                        $pembayaran = $pembayaran_data->type_pembayaran;
                                                        if (strtolower($pembayaran) == "asuransi") {
                                                            $is_asuransi = "";
                                                            $nama_asuransi1 = $pembayaran_data->nama_asuransi;
                                                            $nama_asuransi2 = $pembayaran_data->nama_asuransi2;
                                                            $no_asuransi1 = $pembayaran_data->no_asuransi;
                                                            $no_asuransi2 = $pembayaran_data->no_asuransi2;
                                                        }
                                                    }
                                                    foreach($list_type_bayar as $list){
                                                        if (strtolower($list) == strtolower($pembayaran)) {
                                                            echo "<option value=".$list." selected>".$list."</option>";
                                                        }
                                                        else 
                                                            echo "<option value=".$list.">".$list."</option>";
                                                    }
                                                ?>
                                            </select>
                                            <input type="hidden" name="pembayaran_id" id="pembayaran_id" value="<?= !empty($pembayaran_id) ? $pembayaran_id : '' ; ?>">
                                            <input type="hidden" class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                                        </div>
                                        <label class="control-label">&nbsp;</label>
                                        <div class="form-group col-md-6">
                                            <button type="button" class="btn btn-info" style="width: 50%" disabled>CETAK SEP</button>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="inputPassword" class="control-label">Instansi Pekerjaan</label>
                                            <input type="text" id="byr_instansi" name="byr_instansi" class="form-control" placeholder="Instansi Pekerjaan" readonly>
                                        </div> -->

                                    </div>
                                    <div id="is_asuransi" <?= $is_asuransi; ?>>
                                        <div class="col-md-12" >
                                            <div class="form-group col-md-6">
                                                <label for="inputPassword" class="control-label">Insurance Company</label>
                                                <select class="form-control select2" id="asuransi1" name="asuransi1" onchange="cekBpjs()">
                                                    <option value="" selected disabled >PILIH</option>
                                                        <?php
                                                        $list_asuransi = $this->Reservasi_model->get_asuransi();
                                                        foreach($list_asuransi as $list){
                                                            if (strtolower($nama_asuransi1) == strtolower($list->nama)) {
                                                                echo "<option value='".$list->nama."' selected>".$list->nama."</option>";
                                                            }
                                                            else 
                                                                echo "<option value='".$list->nama."'>".$list->nama."</option>";
                                                        }
                                                        ?>
                                                </select>
                                                <!-- <input type="text" id="byr_namaasuransi" name="byr_namaasuransi" class="form-control" placeholder="Nama Asuransi 1" >    -->
                                            </div>
                                            <div class="form-group col-md-6" >
                                                <label for="inputPassword" class="control-label">Nomor Asuransi</label>
                                                <input type="text"  id="byr_nomorasuransi1" name="byr_nomorasuransi1" class="form-control" placeholder="Nomor Asuransi" value="<?= $no_asuransi1; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12" >
                                            <div class="form-group col-md-6">
                                                <label for="inputPassword" class="control-label">Travel Insurance</label>
                                                <!-- <input type="text" id="byr_namaasuransi" name="byr_namaasuransi" class="form-control" placeholder="Nama Asuransi 2" >    -->
                                                <select class="form-control select2" id="asuransi2" name="asuransi2" onchange="cekBpjs()">
                                                    <option value="" selected disabled>PILIH</option>
                                                        <?php
                                                        $list_asuransi = $this->Reservasi_model->get_asuransi();
                                                        foreach($list_asuransi as $list){
                                                            if (strtolower($nama_asuransi2) == strtolower($list->nama)) {
                                                                echo "<option value='".$list->nama."' selected>".$list->nama."</option>";
                                                            }
                                                            else 
                                                                echo "<option value='".$list->nama."'>".$list->nama."</option>";
                                                        }
                                                        ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6" >
                                                <label for="inputPassword" class="control-label">Nomor Asuransi</label>
                                                <input type="text"  id="byr_nomorasuransi2" name="byr_nomorasuransi2" class="form-control" placeholder="Nomor Asuransi"  value="<?= $no_asuransi2; ?>">
                                            </div>
                                        </div>

                                    </div>



                                </div>

                            </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Data Penanggung Jawab</h3><hr>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword" class="control-label">Nama Penanggung Jawab</label>
                                        <input type="text" id="pj_nama" name="pj_nama" class="form-control" placeholder="Nama Penanggung Jawab" value="<?= $pic_hotel ?>">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword" class="control-label">No Identitas (KTP)</label>
                                        <input type="text" id="no_identitas_pj" name="no_identitas_pj" class="form-control" placeholder="No Identitas (KTP)" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword" class="control-label">No. Ponsel/Mobile</label>
                                        <input type="text" id="pj_no_mobile" name="pj_no_mobile" class="form-control" placeholder="No. Ponsel Mobile" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword" class="control-label">No. Telp Rumah</label>
                                        <input type="text" id="pj_no_tlp_rmh" name="pj_no_tlp_rmh" class="form-control" placeholder="No. Telp Rumah" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword" class="control-label">Status Hubungan</label>
                                        <select class="form-control" id="hubunganpj" name="hubunganpj">
                                            <option value="" disabled selected>Pilih Hubungan</option>
                                            <?php
                                                foreach($list_hubungan as $list){
                                                    echo "<option value='$list'>".$list."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-group">
                                        <label for="inputPassword" class="control-label">Alamat</label>
                                        <textarea id="pj_alamat" name="pj_alamat" class="form-control" style="max-width: 99%"></textarea>
                                    </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label">Provinsi</label>
                                            <select class="form-control select2" id="pj_propinsi" name="pj_propinsi" onchange="getKabupatenPj()">
                                                <option value=""  selected>Pilih Propinsi</option>
                                                <?php
                                                    $list_propinsi = $this->Pendaftaran_model->get_propinsi();

                                                    foreach($list_propinsi as $list){

                                                        if ($list->propinsi_id == 15) {
                                                            echo "<option value='15' selected >JAWA TIMUR</option>";

                                                        } else{
                                                            echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";

                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label">Kecamatan</label>
                                            <select id="pj_kecamatan" name="pj_kecamatan" class="form-control select2" onchange="getKelurahanPj()">
                                                <option value=""  selected>Pilih Kecamatan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">

                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label">Kota/Kabupaten</label>
                                            <select id="pj_kabupaten" name="pj_kabupaten" class="form-control select2" onchange="getKecamatanPj()">
                                                <option value="" selected>Pilih Kota/Kabupaten</option>
                                                <?php
                                                $list_kabupaten = $this->Pendaftaran_model->get_kabupaten(15);

                                                foreach($list_kabupaten as $list){
                                                    echo "<option value='".$list->kabupaten_id."'>".$list->kabupaten_nama."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label">Kelurahan/Desa</label>
                                            <select class="form-control select2" id="pj_kelurahan" name="pj_kelurahan">
                                                <option value="" selected>Pilih Kelurahan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword" class="control-label">Pendidikan</label>
                                        <select class="form-control" id="pendidikan_pj" name="pendidikan_pj">
                                            <option value="" disabled selected>Pilih Pendidikan</option>
                                            <?php
                                                $list_pendidikan = $this->Pendaftaran_model->get_pendidikan();
                                                foreach($list_pendidikan as $list){
                                                    echo "<option value='".$list->pendidikan_id."'>".$list->pendidikan_nama."</option>";
                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword" class="control-label">Pekerjaan</label>
                                        <select class="form-control" id="pekerjaan_pj" name="pekerjaan_pj">
                                            <option value="" selected>Pilih Pekerjaan</option>
                                                <?php
                                                    $list_pekerjaan = $this->Pendaftaran_model->get_pekerjaan();
                                                    foreach($list_pekerjaan as $list){
                                                        echo "<option value='".$list->pekerjaan_id."'>".$list->nama_pekerjaan."</option>";
                                                    }
                                                ?>
                                        </select>
                                    </div>
                                </div>

                        </div>
                    </div>


                 </div>

                 <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <button type="button" class="btn btn-info col-sm-2" id="savePendaftaran"><i class="fa fa-floppy-o"></i> &nbsp;DAFTAR</button>
                            <button type="button" class="btn btn-info col-sm-2 pull-right" id="viewKSPR" title="Klik untuk melihat KSPR"><i class="fa fa-eye"></i> &nbsp;VIEW KSPR</button>
                        </div>
                    </div>
                </div>

                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
            <?php echo form_close(); ?>


<div id="modal_list_pasien" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Pasien</h2> </div>
            <div class="modal-body" style="height:500px;overflow: auto;">
                <table id="table_list_pasien" class="table table-striped dataTable no-footer table-responsive" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Rekam Medis</th>
                            <th>No. BPJS</th>
                            <th>Nama Pasien</th>
                            <th>Nama Panggilan</th>
                            <!-- <th>Jenis Kelamin</th> -->
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Nama Orang Tua</th>
                            <!-- <th>Tempat Lahir</th>
                            <th>Umur</th>
                            <th>Status Kawin</th>
                            <th>No Telp/HP</th>
                            <th>Agama</th>
                            <th>Pendidikan</th>
                            <th>Pekerjaan</th>
                            <th>Nama Suami/Istri</th> -->
                            <th>Pilih</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Rekam Medis</th>
                            <th>No. BPJS</th>
                            <th>Nama Pasien</th>
                            <th>Nama Panggilan</th>
                            <!-- <th>Jenis Kelamin</th> -->
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Nama Orang Tua</th>
                            <!-- <th>Tempat Lahir</th>
                            <th>Umur</th>
                            <th>Status Kawin</th>
                            <th>No Telp/HP</th>
                            <th>Agama</th>
                            <th>Pendidikan</th>
                            <th>Pekerjaan</th>
                            <th>Nama Suami/Istri</th> -->
                            <th>Pilih</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_list_ortu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Referensi Rekam Medis</h2> </div>
            <div class="modal-body" style="height:500px;overflow: auto;">
                    <div class="table-responsive">
                        <table id="table_list_ortu" class="table table-striped dataTable no-footer" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Rekam Medis</th>
                                    <th>Nama Pasien</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Rekam Medis</th>
                                    <th>Nama Pasien</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Tindakan</th>
                                </tr>
                            </tfoot>
                        </table>
                  </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_list_rujuk" class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Data Perujuk</h2> </div>
            <div class="modal-body" style="height:500px;overflow: auto;">
                    <div class="table-responsive">
                        <table id="table_list_rujuk" class="table table-striped dataTable no-footer" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Perujuk</th>
                                    <th>Nama Perujuk</th>
                                    <th>Alamat Kantor</th>
                                    <th>No Telp/HP</th>
                                    <th>Nama Marketing</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                  </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="modal_printA" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Print Pendaftaran</b></h2> </div>
                <div class="modal-body" style="background: #fafafa">

                    <div class="form-group col-md-push-3">
                        <button class="btn btn-info"><i class="fa fa-print"></i> Print Data Pasien</button>
                        <button class="btn btn-success"><i class="fa fa-print"></i> Print Kartu Berobat</button>
                    </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_print" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="margin-top: 150px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Form Print Data Pasien</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="col-sm-12">
                    <!-- <div class="form-group col-md-4">
                        <button type="button" class="btn btn-primary col-md-12 " id="printSurat"  ><i class="fa fa-print p-r-10"></i>PRINT SURAT KET.LAHIR</button>
                    </div> -->
                    <div class="form-group col-md-4">
                        <button type="button" class="btn btn-info col-md-12 " id="printKartu"  ><i class="fa fa-print p-r-10"></i>PRINT KARTU BEROBAT</button>
                    </div>
                    <div class="form-group col-md-4">
                        <button type="button" class="btn btn-warning col-md-12 " id="printCasemix" ><i class="fa fa-print p-r-10"></i>PRINT CASEMIX </button>
                    </div>
                    <!-- <div class="form-group col-md-4">
                        <button type="button" class="btn btn-success col-md-12 " id="printDetail" ><i class="fa fa-print p-r-10"></i>PRINT SEP BPJS</button>
                    </div> -->

                    <div class="form-group col-md-4">
                        <button type="button" class="btn btn-primary col-md-12 " id="printDetail" ><i class="fa fa-print p-r-10"></i>PRINT DETAIL PASIEN</button>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-4">
                        <button type="button" class="btn btn-danger col-md-12 " id="printResiko2" ><i class="fa fa-print p-r-10"></i>PRINT OBGYN</button>
                    </div>
                    <div class="form-group col-md-4">
                        <button type="button" class="btn btn-success col-md-12 " id="printResiko" ><i class="fa fa-print p-r-10"></i>PRINT NON OBGYN</button>
                    </div>
                    <!-- <div class="form-group col-md-4">
                        <button type="button" class="btn btn-danger col-md-12 " id="printKartuHilang" ><i class="fa fa-print p-r-10"></i>PRINT HILANG KARTU</button>
                    </div> -->
                    <!-- <div class="form-group col-md-4">
                        <button type="button" class="btn btn-warning col-md-12 " id="printSticker" ><i class="fa fa-print p-r-10"></i>PRINT STICKER </button>
                    </div> -->
                    <!-- <div class="form-group col-md-4">
                        <button type="button" class="btn btn-warning col-md-12 " style="background: #ea5f0d !important; border-color: #ea5f0d !important;" id="printGelang" ><i class="fa fa-print p-r-10"></i>PRINT GELANG </button>
                    </div>  -->
                        <!-- <select class="form-control" name="Kategori_print" id="Kategori_print">
                            <option disabled selected> Pilih Kategori </option>
                            <option value="ket.lahir">Print Surat Ket.Lahir</option>
                            <option value="kartu_berobat">Kartu Berobat</option>
                            <option value="detail_pasien">Detail Pasien</option>
                        </select>  -->

                </div>
              <!--   <div class="col-sm-12">
                    <button type="button" class="btn btn-success col-md-4 pull-right" onclick="do_print()" ><i class="fa fa-print"></i>Print</button>
                </div> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_retensi_pasien" class="modal fade bs-example-modal-lg" tabindex="-2" role="dialog" aria-labelledby="myLargeModalLabelw" aria-hidden="true" style="display: none; overflow: auto">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Retensi Pasien</b></h2> 
            </div>
            <div class="modal-body" style="background: #fafafa" style="max-height: 500px">
                <style>
                    #judul{
                        width:250px;
                        font-size:12pt;
                    }
                    #isian{
                        width:400px;
                        font-size:12pt;
                        font-weight:600;
                    }
                    #separator{
                        font-size:12pt;

                    }
                </style>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="input-group custom-search-form">
                            <input readonly type="text" id="find_no_rm" name="find_no_rm" class="form-control" placeholder="Cari No Referensi RM"> 
                            <span class="input-group-btn">
                                <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_pasien_lama" type="button" onclick="getDataRMPasienLamaList()"> <i class="fa fa-bars"></i> </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h3 style="margin: 8px 12px">Data Pasien Lama</h3>
                        <form class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="pasien_lama_nama" class="control-label">Nama</label>
                                    <input type="text" id="pasien_lama_nama" name="pasien_lama_nama" class="form-control" placeholder="Nama Pasien" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="pasien_lama_jk" class="control-label">Jenis Kelamin</label>
                                    <select name="pasien_lama_jk" id="pasien_lama_jk" class="form-control" readonly>
                                        <option value="" selected>Pilih Jenis Kelamin</option>
                                        <option value="Laki-laki">Laki - laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="pasien_lama_umur" class="control-label">Umur</label>
                                    <input type="text" id="pasien_lama_umur" name="pasien_lama_umur" class="form-control" placeholder="Umur Pasien" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="pasien_lama_tanggal_kedatangan_terakhir" class="control-label">Tanggal Kedatangan Terakhir</label>
                                    <input type="text" id="pasien_lama_tanggal_kedatangan_terakhir" name="pasien_lama_tanggal_kedatangan_terakhir" class="form-control" placeholder="Tanggal Kedatangan Pasien" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="pasien_lama_alamat" class="control-label">Alamat</label>
                                    <input type="text" id="pasien_lama_alamat" name="pasien_lama_alamat" class="form-control" placeholder="Alamat Pasien" readonly>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h3 style="margin: 8px 12px">Data Pasien Baru</h3>
                        <form class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="pasien_baru_nama" class="control-label">Nama</label>
                                    <input type="text" id="pasien_baru_nama" name="pasien_baru_nama" class="form-control" placeholder="Nama Pasien" required>
                                </div>
                                <div class="form-group">
                                    <label for="pasien_baru_jk" class="control-label" required>Jenis Kelamin</label>
                                    <select name="pasien_baru_jk" id="pasien_baru_jk" class="form-control">
                                        <option value="" selected>Pilih Jenis Kelamin</option>
                                        <option value="Laki-laki">Laki - laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="pasien_baru_umur" class="control-label">Tanggal Lahir</label>
                                    <div class="input-group">
                                        <input name="pasien_baru_umur" id="pasien_baru_umur" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" value=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pasien_baru_alamat" class="control-label">Alamat</label>
                                    <input type="text" id="pasien_baru_alamat" name="pasien_baru_alamat" class="form-control" placeholder="Alamat Pasien">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <button class="btn btn-info" style="float:right; margin-right:8px" id="pasien_lama_pasien_id" onclick="saveRMPasienBaru(this.value)">Pilih No RM</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_list_pasien_lama" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Referensi Rekam Medis</h2> </div>
            <div class="modal-body" style="height:500px;overflow: auto;">
                    <div class="table-responsive">
                        <table id="table_list_pasien_lama" class="table table-striped dataTable no-footer" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Rekam Medis</th>
                                    <th>Nama Pasien</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Rekam Medis</th>
                                    <th>Nama Pasien</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Tindakan</th>
                                </tr>
                            </tfoot>
                        </table>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_view_kspr" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-lg" style="min-width:70% !important;">
    <div class="modal-content" style="overflow: auto;height: 600px;margin-top:200px;">
        <div class="modal-header" style="background: #fafafa !important">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel"><b>KSPR</b></h2> </div>
        <div class="modal-body" style="background: #fafafa">

          <!-- ================================= form kspr baru ==========================================  -->
                      <?php echo form_open('#',array('id' => 'fmCreateSkorKSPR'))?>
                                          <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                                          <input type="hidden" name="pasien_kspr_id" id="pasien_kspr_id" value="<?php //echo $list_pasien->pasien_id; ?>">
                                          <!-- <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<?php //echo $list_pasien->pendaftaran_id; ?>"> -->
                                          <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_bidan" style="display:none;">
                                              <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                              <div >
                                                  <p id="card_message_bidan"></p>
                                              </div>
                                          </div>
                                          <!-- <hr style="margin-top:-9px;margin-bottom:10px;"> -->
                                          <div class="form-group col-md-8">
                                              <label for=""><span style="color:red;">*</span> Jika skor KSPR tidak di isi maka otomatis skor KSPR : 2</label>
                                          </div>
                                          <div class="form-group col-md-6">
                                              <button onclick="cekSkorKSPR()" class="btn btn-info col-md-12" type="button"><i class="fa fa-eye" ></i> CEK TOTAL KESELURUHAN SKOR KSPR</button>
                                          </div>
                          <?php echo form_close();?>
                                          <div class="form-group col-md-12">
                                              <table id="table_skor_kspr" class="table table-striped dataTable" cellspacing="0">
                                                  <thead>
                                                      <tr>
                                                          <th>No</th>
                                                          <th>Nama KSPR</th>
                                                          <th>Skor KSPR</th>
                                                          <th>Jumlah</th>
                                                          <th>Total</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                      <tr>
                                                          <td colspan="6">No Data to Display</td>
                                                      </tr>
                                                  </tbody>
                                                  <tfoot>
                                                      <tr>
                                                          <th>No</th>
                                                          <th>Nama KSPR</th>
                                                          <th>Skor KSPR</th>
                                                          <th>Jumlah</th>
                                                          <th>Total</th>
                                                      </tr>
                                                  </tfoot>
                                              </table>
                                          </div>
                                          <div class="form-group col-md-12">
                                          <!-- table resiko baru -->
                                              <div class="form-group col-md-6">
                                                  <table id="table_faktor_resiko" class="table table-striped dataTable" cellspacing="0">
                                                      <thead>
                                                          <tr>
                                                              <th>Faktor Resiko Yang Ditemukan Oleh Sistem</th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <tr>
                                                              <td colspan="1">No Data to Display</td>
                                                          </tr>
                                                      </tbody>
                                                      <!-- <tfoot>
                                                          <tr>
                                                              <th>Faktor Resiko</th>
                                                          </tr>
                                                      </tfoot> -->
                                                  </table>
                                              </div>
                                          </div>
        </div>
          <!-- ================================= form kspr baru ==========================================  -->


        </div>

    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<?php $this->load->view('footer');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rekam_medis/retensi_pendaftaran.js"></script>