<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">List Pasien</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Rekam Medis</a></li>
                    <li class="active">List Pasien</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Data Seluruh Pasien
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="inputName1" class="control-label"></label>
                                    <dl class="text-right">
                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                        <dd>Ketik / pilih untuk mencari atau filter data </dd>
                                    </dl>
                                </div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="form-group col-md-5">
                                            <label for="">RM AWAL</label>

                                            <input type="text" id="no_rm_awal" maxlength="8" name="no_rm_awal" onkeyup="changeRM()" class="form-control autocomplete" placeholder=" Cari Nomor Rekam Medis....." >
                                            <input type="hidden" id="rm_awal" name="rm_awal">
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label for="">RM AKHIR</label>
                                            <input type="text" id="no_rm_akhir" name="no_rm_akhir" maxlength="8" onkeyup="changeRM()" class="form-control autocomplete" placeholder=" Cari Nomor Rekam Medis....." >
                                            <input type="hidden" name="rm_akhir" id="rm_akhir" class="form-control">

                                        </div>
                                        <div class="form-group col-md-2" style="margin-top:24px">
                                            <button class="btn btn-info" onclick="exportAllPasien()">EXPORT</button>
                                        </div>
                                        <div class="form-group col-md-3" style="margin-top: 7px">

                                            <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                            <b>
                                                <select name="#" class="form-control select" style="margin-bottom: 7px" id="cari_pasien" >
                                                    <option value=""  selected>Pilih Berdasarkan</option>
                                                    <!-- <option value="no_pendaftaran">No. Pendaftaran</option> -->
                                                    <option value="no_rekam_medis">No. Rekam Medis</option>
                                                    <option value="nama_pasien">Nama Pasien</option>
                                                    <option value="pasien_alamat">Alamat Pasien</option>
                                                    <option value="tlp_selular">No HP</option>
                                                    <option value="no_bpjs">No BPJS</option>
                                                    <option value="no_identitas">No Identitas</option>
                                                </select></b>

                                        </div>
                                        <div class="form-group col-md-9" style="margin-top: 7px">
                                            <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                            <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasien()" >
                                        </div>
                                    </div>
                                </div>


                            </div><br><hr style="margin-top: -27px">
                            <!-- <button data-toggle="modal" id="tambah_pasien"  onclick="openModalKary()" type="button" class="btn btn-info"><i class="fa fa-plus"></i> TAMBAH PASIEN</button><br><br> -->
                            <div class="table-responsive">
                                <table id="table_list_pasien_list" class="table table-striped dataTable " cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Rekam Medis</th>
                                        <th>Nama Pasien</th>
                                        <th>Alamat</th>
                                        <th>Telepon</th>
                                        <th>No BPJS</th>
                                        <th>Type indentitas/No Identitas</th>
                                        <th>Tindakan</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Rekam Medis</th>
                                        <th>Nama Pasien</th>
                                        <th>Alamat</th>
                                        <th>Telepon</th>
                                        <th>BPJS</th>
                                        <th>Type indentitas/No Identitas</th>
                                        <th>Tindakan</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->
    </div>
    <!-- /.container-fluid -->


    <!-- modal print -->
    <div id="modal_print" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="margin-top: 150px;">
                <div class="modal-header" style="background: #fafafa">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b>Form Print Data Pasien</b></h2>
                </div>
                <div class="modal-body" style="background: #fafafa">
                    <div class="col-sm-12">
                        <div class="form-group col-md-4" hidden>
                            <!-- <label>Print Surat Ket.Lahir</label>  -->
                            <button type="button" class="btn btn-primary col-md-12 " id="printSurat"  ><i class="fa fa-print p-r-10"></i>PRINT SURAT KET.LAHIR</button>
                        </div>
                        <div class="form-group col-md-4">
                            <!-- <label>Print Kartu Berobat</label>  -->
                            <button type="button" class="btn btn-info col-md-12 " id="printKartu"  ><i class="fa fa-print p-r-10"></i>PRINT KARTU BEROBAT</button>
                        </div>
                        <div class="form-group col-md-4">
                            <!-- <label>Print Detail Pasien</label>  -->
                            <button type="button" class="btn btn-success col-md-12 " id="printDetail" ><i class="fa fa-print p-r-10"></i>PRINT DETAIL PASIEN</button>
                        </div>
                        <div class="form-group col-md-4" >
                            <!-- <label>Print Detail Pasien</label>  -->
                            <button type="button" class="btn btn-danger col-md-12 " id="printCasemix" ><i class="fa fa-print p-r-10"></i>PRINT CASEMIX</button>
                        </div>
                        <div class="form-group col-md-4" hidden>
                            <!-- <label>Print Detail Pasien</label>  -->
                            <button type="button" class="btn btn-danger col-md-12 " id="printKartuHilang" ><i class="fa fa-print p-r-10"></i>PRINT HILANG KARTU</button>
                        </div>
                        <div class="form-group col-md-4" hidden>
                            <!-- <label>Print Detail Pasien</label>  -->
                            <button type="button" class="btn btn-warning col-md-12 " id="printSticker" ><i class="fa fa-print p-r-10"></i>PRINT STICKER </button>
                        </div>
                        <div class="form-group col-md-4" hidden>
                            <!-- <label>Print Detail Pasien</label>  -->
                            <button type="button" class="btn btn-warning col-md-12 " style="background: #ea5f0d !important; border-color: #ea5f0d !important;" id="printGelang" ><i class="fa fa-print p-r-10"></i>PRINT GELANG </button>
                        </div>
                        <div class="form-group col-md-4">
                            <!-- <label>Print Detail Pasien</label>  -->
                            <button type="button" class="btn btn-warning col-md-12 " style="background: #1b5e20 !important; border-color: #1b5e20 !important;" id="printResiko2" ><i class="fa fa-print p-r-10"></i>PRINT OBGYN </button>
                        </div>
                        <div class="form-group col-md-4">
                            <!-- <label>Print Detail Pasien</label>  -->
                            <button type="button" class="btn btn-warning col-md-12 " style="background: #ea5f0d !important; border-color: #ea5f0d !important;" id="printResiko" ><i class="fa fa-print p-r-10"></i>PRINT NON OBGYN </button>
                        </div>
                        <!-- <select class="form-control" name="Kategori_print" id="Kategori_print">
                            <option  selected>-- Pilih Kategori --</option>
                            <option value="ket.lahir">Print Surat Ket.Lahir</option>
                            <option value="kartu_berobat">Kartu Berobat</option>
                            <option value="detail_pasien">Detail Pasien</option>
                        </select>  -->

                    </div>
                    <!--   <div class="col-sm-12">
                          <button type="button" class="btn btn-success col-md-4 pull-right" onclick="do_print()" ><i class="fa fa-print"></i>Print</button>
                      </div> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal  print -->

    <!-- modal update pasien -->
    <div id="modal_update_list_pasien" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
        <div class="modal-dialog modal-large">
            <div class="modal-content" style="margin-top: 160px">
                <?php echo form_open('#',array('id' => 'fmUpdatelist_pasien'))?>

                <div class="modal-header" style="background: #fafafa">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b>Edit Pasien</b></h2>
                </div>
                <div class="modal-body" style="background: #fafafa;height: 500px;overflow: auto;">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">
                                <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif2" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                    <div >
                                        <p id="card_message2"></p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <!-- <form data-toggle="validator"> -->
                                    <div class="form-group" >
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                                        <input id="upd_pasien_id" type="hidden" name="upd_pasien_id" readonly>
                                        <input id="upd_status_pasien" type="hidden" name="upd_status_pasien" readonly>
                                        <label for="inputName1" class="control-label">No.Rekam Medis</label>
                                        <input id="upd_no_rm" type="text" name="upd_no_rm" class="form-control" value="" >
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="inputTypeID" class="control-label">Type Identitas</label>
                                                <select id="upd_type_id" name="upd_type_id" class="form-control" >
                                                    <option value=""  selected>Type ID</option>
                                                    <?php
                                                    foreach($list_type_identitas as $list){
                                                        echo "<option value='".$list."'>".$list."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="inputPassword" class="control-label">No.Identitas</label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  id="upd_no_identitas" name="upd_no_identitas" type = "number" maxlength = "16" class="form-control no" placeholder="No.Identitas" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-top: -10px">
                                        <label for="no_bpjs" class="control-label">No.BPJS</label>
                                        <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "13" class="form-control no" id="upd_no_bpjs" name="upd_no_bpjs" placeholder="No.BPJS" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label for="type_diskon" class="control-label">Layanan Diskon</label>
                                        <select name="upd_layanan_diskon" id="upd_layanan_diskon" class="form-control">
                                            <option value=""  selected>Pilih Layanan Diskon</option>
                                            <option value="1">Diskon 0</option>
                                            <option value="2">Diskon Relasi</option>
                                            <option value="3">Diskon Staf 1</option>
                                            <option value="4">Diskon Staf 2</option>
                                            <option value="5">Diskon Staf 3</option>
                                        </select>
                                    </div> -->
                                    <div class="form-group">
                                        <label for="nama_pasien" class="control-label">Nama Lengkap Pasien</label>
                                        <input type="text" class="form-control" id="upd_nama_pasien" name="upd_nama_pasien" placeholder="Nama Lengkap Pasien" data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_pasien" class="control-label">Nama Panggilan
                                            <!--  -->
                                        </label>
                                        <input type="text" class="form-control" id="upd_nama_panggilan" name="upd_nama_panggilan" placeholder="Nama Panggilan" data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_kelamin" class="control-label">Jenis Kelamin
                                            <!--  -->
                                        </label>
                                        <select class="form-control selectpicker" id="upd_jenis_kelamin" name="upd_jenis_kelamin" >
                                            <option value=""  selected>Pilih Jenis Kelamin</option>
                                            <?php
                                            foreach($list_jenis_kelamin as $key => $value){
                                                echo "<option value='".$key."'>".$value."</option>";
                                            }
                                            ?>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label for="tempat_lahir" class="control-label">Tempat Lahir
                                            <!--  -->
                                        </label>
                                        <input id="upd_tempat_lahir" type="text" name="upd_tempat_lahir" class="form-control" placeholder="Kota/Kabupaten Kelahiran" data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="tanggal_lahir" class="control-label">Tanggal Lahir
                                                    <!-- <span style="color:red">*</span> -->
                                                </label>
                                                <div class="input-group">
                                                    <input name="upd_tgl_lahir" id="upd_tgl_lahir" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" onchange="setUmur()" > <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="inputPassword" class="control-label">Umur
                                                    <!-- <span style="color:red">*</span> -->
                                                </label>
                                                <input type="text" name="upd_umur" id="upd_umur" class="form-control" placeholder="Umur" >
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div class="form-group">
                                            <label for="textarea" class="control-label">Alamat Jalan
                                                <!--  -->
                                            </label>
                                            <textarea id="upd_alamat" name="upd_alamat" data-error="Please fill out this field." class="form-control" ></textarea>
                                            <div class="help-block with-errors"></div>

                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 0px">
                                        <div class="form-group col-sm-6" >
                                            <label for="inputPassword" class="control-label">Provinsi</label>
                                            <select class="form-control select2" id="propinsi" name="propinsi" onchange="getKabupaten()" >
                                                <!-- <option>Select</option> -->
                                                <option value=""  selected>Pilih Provinsi</option>
                                                <?php
                                                $list_provinsi = $this->List_pasien_model->get_propinsi();
                                                foreach($list_provinsi as $list){
                                                    echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="inputPassword" class="control-label">Kabupaten
                                                <!--  -->
                                            </label>
                                            <select class="form-control select2" id="kabupaten" name="kabupaten" onchange="getKecamatan()" >
                                                <option value=""  selected>Pilih Kota/Kabupaten</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="inputPassword" class="control-label">Kecamatan
                                                    <!--  -->
                                                </label>
                                                <select class="form-control select2" id="kecamatan" name="kecamatan" onchange="getKelurahan()" >
                                                    <option value="" selected>Pilih Kecamatan</option>

                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="inputPassword" class="control-label">Kelurahan
                                                    <!--  -->
                                                </label>
                                                <select id="kelurahan" name="kelurahan" class="form-control select2" >
                                                    <option value="" selected>Pilih Kelurahan</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 " >
                                            <label for="upd_allergy_history" class="control-label">Allergy History</label>
                                            <input type="text" class="form-control" name="upd_allergy_history" id="upd_allergy_history" placeholder="Yes, For...." >
                                        </div>
                                        <div class="form-group col-sm-6 " >
                                            <label for="upd_pregnant" class="control-label">Pregnant</label>
                                            <input type="text" class="form-control" name="upd_pregnant" id="upd_pregnant" placeholder="Month " >
                                        </div>
                                    </div>
                                    <!-- <div class="row"> -->
                                    <div class="form-group">
                                        <label for="upd_current_medication" class="control-label">Current Medication</label>
                                        <textarea id="upd_current_medication" name="upd_current_medication"  class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                    <!-- </div> -->
                                    <div class="row">
                                        <div class="form-group col-sm-6 " >
                                            <label for="upd_weight" class="control-label">Weight </label>
                                            <input type="text" class="form-control" name="upd_weight" id="upd_weight" placeholder="Kg.." >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="upd_medical_history" class="control-label">Do you have any past medical history</label>
                                        <textarea id="upd_medical_history" name="upd_medical_history"  class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                    <div class="form-group">
                                        <label for="upd_medical_event" class="control-label">Please note all important medical event</label>
                                        <textarea id="upd_medical_event" name="upd_medical_event" class="form-control" ></textarea>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                    <!-- <div style="margin-top: -32px">
                                        <div class="form-group">
                                            <label for="textarea" class="control-label">Alamat Tinggal

                                            </label>
                                            <textarea id="upd_alamat_domisili" name="upd_alamat_domisili" data-error="Please fill out this field." class="form-control" required readonly></textarea>
                                            <input type="checkbox" id="cek_alamat" ><span style="font-size:10px; color:red;"> * Jika alamat berbeda dengan tempat tinggal</span>
                                            <div class="help-block with-errors"></div>

                                        </div>
                                    </div> -->

                                    <!-- <div class="row" style="margin-top: -12px">
                                    <div class="form-group col-sm-6" >
                                        <label for="inputPassword" class="control-label">Provinsi

                                        </label>
                                        <select class="form-control select2" id="upd_propinsi_tinggal" name="upd_propinsi_tinggal" onchange="getKabupatenTinggal()">
                                            <option value=""  selected>Pilih Provinsi</option>
                                             <?php
                                    $list_provinsi = $this->List_pasien_model->get_propinsi();
                                    foreach($list_provinsi as $list){
                                        echo "<option value='".$list->propinsi_id."'>".$list->propinsi_nama."</option>";
                                    }
                                    ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                    <label for="inputPassword" class="control-label">Kabupaten
                                    </label>
                                        <select class="form-control select2" id="upd_kabupaten_tinggal" name="upd_kabupaten_tinggal" onchange="getKecamatanTinggal()">
                                            <option value=""  selected>Pilih Kota/Kabupaten</option>


                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="inputPassword" class="control-label">Kecamatan
                                                </label>
                                                <select class="form-control select2" id="upd_kecamatan_tinggal" name="upd_kecamatan_tinggal" onchange="getKelurahanTinggal()">
                                                    <option value="" selected>Pilih Kecamatan</option>

                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                            <label for="inputPassword" class="control-label">Kelurahan
                                            </label>
                                                <select id="upd_kelurahan_tinggal" name="upd_kelurahan_tinggal" class="form-control select2">
                                                    <option value="" selected>Pilih Kelurahan</option>

                                                </select>
                                            </div>
                                        </div>
                                </div> -->
                                </div>

                                <div class="col-sm-6">
                                    <!-- <form data-toggle="validator"> -->
                                    <div class="form-group" >
                                        <!-- <br><br><h3 class="box-title m-b-0">Data Keluarga</h3><br> -->
                                        <label for="textarea" class="control-label">Status Kawin
                                            <!--  -->
                                        </label>
                                        <select class="form-control select" onclick="cekStatusKawin()" id="upd_status_kawin" name="upd_status_kawin" >
                                            <option value=""  selected>Pilih Status Kawin</option>
                                            <?php
                                            foreach($list_status_kawin as $list){
                                                echo "<option value='".$list."'>".$list."</option>";
                                            }
                                            ?>
                                        </select>
                                        <!-- <button type="button" onclick="cekStatusKawin()" id="statuskawin">tes</button> -->
                                    </div>

                                    <div id="Suami_istri" class="hide">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nama Suami/Istri
                                                <!-- <span style="color:red">*</span> -->
                                            </label>
                                            <input type="email" class="form-control" name="upd_nama_suami_istri" id="upd_nama_suami_istri" placeholder="Nama Suami/Istri" data-error="Please fill out this field." >
                                            <!-- <div class="help-block with-errors"></div> -->
                                        </div>
                                        <div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="tgl_lahir" class="control-label">Tanggal Lahir</label>
                                                    <div class="input-group">
                                                        <input name="upd_tgl_suami_istri" id="upd_tgl_suami_istri" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" onchange="setUmurSustri()" > <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                    </div>

                                                </div>
                                                <div class="form-group col-sm-6">

                                                    <label for="umur" class="control-label">Umur</label>
                                                    <input id="upd_umur_suami_istri" name="upd_umur_suami_istri" type="text" class="form-control" readonly placeholder="Umur" >

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">No.Telp rumah</label>
                                        <input type="text" class="form-control no" id="upd_no_tlp_rmh" name="upd_no_tlp_rmh" placeholder="No.Telp rumah" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">No.Ponsel/Mobile<span style="color:red">*</span></label>
                                        <input type="number" class="form-control no" id="upd_no_mobile" name="upd_no_mobile" placeholder="No.Ponsel/Mobile" data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="upd_pekerjaan" class="control-label">Pekerjaan Pasien</label>
                                        <select class="form-control" id="upd_pekerjaan" name="upd_pekerjaan" >
                                            <option value="" selected>Pilih Pekerjaan</option>
                                            <?php
                                            $list_pekerjaan = $this->List_pasien_model->get_pekerjaan();
                                            foreach($list_pekerjaan as $list){
                                                echo "<option value='".$list->pekerjaan_id."'>".$list->nama_pekerjaan."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="upd_warga_negara" class="control-label">Warga Negara</label>
                                        <select class="form-control" id="upd_warga_negara" name="upd_warga_negara" >
                                            <option value="" selected>Pilih Warga Negara</option>
                                            <?php
                                            $list_warganegara = $this->Pendaftaran_model->get_warganegara();
                                            foreach($list_warganegara as $list){
                                                echo "<option value='".$list->id."'>".$list->nama_negara."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Agama</label>
                                        <select id="upd_agama" name="upd_agama" class="form-control" >
                                            <option value=""  selected>Pilih Agama</option>
                                            <?php
                                            $list_agama = $this->List_pasien_model->get_agama();
                                            foreach($list_agama as $list){
                                                echo "<option value='".$list->agama_id."'>".$list->agama_nama."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Bahasa</label>
                                        <select id="upd_bahasa" name="upd_bahasa" class="form-control" >
                                            <option value="" selected >Pilih Bahasa</option>
                                            <?php
                                            $list_bahasa = $this->List_pasien_model->get_bahasa();
                                            foreach($list_bahasa as $list){
                                                echo "<option value='".$list->bahasa_id."'>".$list->bahasa."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="textarea" class="control-label">Pendidikan</label>
                                        <select id="upd_pendidikan" name="upd_pendidikan" class="form-control" >
                                            <option value=""  selected>Pilih Pendidikan</option>
                                            <?php
                                            $list_pendidikan = $this->List_pasien_model->get_pendidikan();
                                            foreach($list_pendidikan as $list){
                                                echo "<option value='".$list->pendidikan_id."'>".$list->pendidikan_nama."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nama Orang Tua
                                            <!-- <span style="color:red">*</span> -->
                                        </label>
                                        <input type="text" class="form-control" id="upd_nama_orangtua" name="upd_nama_orangtua" placeholder="Nama Orang Tua" data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="control-label">Tanggal Lahir</label>
                                                <div class="input-group">
                                                    <input name="upd_tgl_lahir_ortu" id="upd_tgl_lahir_ortu" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" onchange="setUmurOrtu()" > <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="control-label">Umur</label>
                                                <input type="text" id="upd_umur_ortu" name="upd_umur_ortu" class="form-control" placeholder="Umur" >
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="upd_asuransi1" class="control-label">Insurance company
                                                    <!-- <span style="color:red">*</span> -->
                                                </label>
                                                <select id="upd_asuransi1" name="upd_asuransi1" class="form-control" >
                                                    <option value="" selected>Choose Insurance Company</option>
                                                    <?php
                                                    $list_asuransi = $this->Reservasi_model->get_asuransi();
                                                    foreach($list_asuransi as $list){
                                                        echo "<option value='".$list->nama."'>".$list->nama."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="upd_asuransi2" class="control-label">Travel Insurance
                                                    <!-- <span style="color:red">*</span> -->
                                                </label>
                                                <select id="upd_asuransi2" name="upd_asuransi2" class="form-control" >
                                                    <option value="" selected>Choose Travel Insurance</option>
                                                    <?php
                                                    $list_asuransi = $this->Reservasi_model->get_asuransi();
                                                    foreach($list_asuransi as $list){
                                                        echo "<option value='".$list->nama."'>".$list->nama."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="upd_no_asuransi1" class="control-label">Nomor Asuransi
                                                    <!-- <span style="color:red">*</span> -->
                                                </label>
                                                <input type="text" name="upd_no_asuransi1" id="upd_no_asuransi1" class="form-control" placeholder="No Asuransi" >
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="upd_no_asuransi2" class="control-label">Nomor Asuransi
                                                    <!-- <span style="color:red">*</span> -->
                                                </label>
                                                <input type="text" name="upd_no_asuransi2" id="upd_no_asuransi2" class="form-control" placeholder="No Asuransi" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Catatan Khusus</label>
                                        <textarea id="upd_catatan_khusus" name="upd_catatan_khusus" class="form-control" ></textarea> <span class="help-block with-errors" style="color: red; font-size: 10px;">*Optional(jika diperlukan)</span>
                                    </div>


                                    <!--                                 </form>-->
                                </div>

                            </div>

                            <div class="col-md-12">
                                <button type="button" class="btn btn-success pull-right" id="changeListPasien"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                            </div>

                        </div>

                    </div>

                </div>


                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- modal add karyawan -->
    <div id="modal_karyawan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
        <div class="modal-dialog modal-large">
            <div class="modal-content" style="margin-top: 165px">
                <div class="modal-header" style="background: #fafafa;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b id="form_judul">Tambah Data Pasien</b></h2>
                </div>
                <div style="margin: 8px">
                    <div id="card-alert" class="card green modal_notif" style="display:none;">
                        <div class="card-content white-text">
                            <p id="modal_card_message"></p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                </div>
                <?php echo form_open('#',array('id' => 'fmCreatePasien', 'data-toggle' => 'validator'))?>

                <div class="modal-body" style="background: #fafafa;max-height: 500px;overflow: auto;">

                    <div class="panel panel-info1">
                        <div class="panel-heading"> Data Pasien
                            <div class="pull-right"><a href="" data-perform="#"><i class="ti-minu"></i></a> </div>
                        </div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">

                            <div class="panel-body">
                                <!-- start white-box -->
                                <div class="white-box">

                                    <div class="row">



                                        <div class="col-md-6">
                                            <input type="text" name="karyawan_id" id="karyawan_id" hidden>
                                            <div class="form-group">
                                                <label>No.Rekam Medis</label>
                                                <input id="no_rm" type="text" name="no_rm" class="validate form-control" value="<?php echo @$next_no_rm; ?>" placeholder="No.Rekam Medis" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label>No.KTP
                                                    <!--  -->
                                                </label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "16" class="form-control no" placeholder="No.KTP" name="no_ktp" id="no_ktp" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Lengkap
                                                    <!--  -->
                                                </label>
                                                <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" placeholder="Nama Lengkap">
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Panggilan
                                                    <!--  -->
                                                </label>
                                                <input id="nama_panggilan" type="text" name="nama_panggilan" class="form-control" placeholder="Nama Panggilan">
                                            </div>
                                            <div class="form-group">
                                                <label>Tempat Lahir
                                                    <!--  -->
                                                </label>
                                                <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                                            </div>

                                            <div class="form-group">
                                                <label>Alamat
                                                    <!--  -->
                                                </label>
                                                <textarea id="alamat" name="alamat" class="form-control" placeholder="Alamat"></textarea>
                                            </div>


                                        </div>
                                        <div class="col-md-6">
                                            <div>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="tanggal_lahir" class="control-label">Tanggal Lahir
                                                            <!-- <span style="color:red">*</span> -->
                                                        </label>
                                                        <div class="input-group">
                                                            <input name="tgl_lahir" id="tgl_lahir" type="text" class="form-control mydatepicker" placeholder="Tanggal Lahir" onchange="setUmurKary()"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="inputPassword" class="control-label">Umur<span style="color:red">*</span></label>
                                                        <input type="text" name="umur" id="umur" class="form-control" placeholder="Umur" readonly required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Jenis Kelamin
                                                    <!-- <span style="color: red;">*</span -->
                                                    ></label>
                                                <select id="jenis_kelamin" name="jenis_kelamin" class="form-control">
                                                    <option  selected value="">Pilih Jenis Kelamin</option>
                                                    <option value="Laki-Laki">Laki-Laki</option>
                                                    <option value="Perempuan">Perempuan</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>No.BPJS</label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "13" class="form-control no" placeholder="No. BPJS" name="no_bpjs" id="no_bpjs" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="type_diskon" class="control-label">Layanan Diskon</label>
                                                <select name="layanan_diskon" id="layanan_diskon" class="form-control">
                                                    <option value=""  selected>Pilih Layanan Diskon</option>
                                                    <option value="1">Diskon 0</option>
                                                    <option value="2">Diskon Relasi</option>
                                                    <option value="3">Diskon Staf 1</option>
                                                    <option value="4">Diskon Staf 2</option>
                                                    <option value="5">Diskon Staf 3</option>
                                                </select>
                                            </div> -->
                                            <div class="form-group">
                                                <label>Agama</label>

                                                <select id="agama" name="agama" class="form-control selectpicker">
                                                    <option value=""  selected>Pilih Agama</option>
                                                    <?php
                                                    foreach($list_agama as $list){
                                                        echo "<option value='".$list->agama_id."'>".$list->agama_nama."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Status Kawin
                                                    <!--  -->
                                                </label>
                                                <select class="form-control status_kawin" id="status_kawin" name="status_kawin">
                                                    <option value=""  selected>Pilih Status Kawin</option>
                                                    <?php
                                                    foreach($list_status_kawin as $list){
                                                        echo "<option value='".$list."'>".$list."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end white-box -->
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                    <div class="white-box">
                        <div class="row">
                            <a class="btn btn-success" onclick="savePasien()"><i class="fa fa-floppy-o p-r-10 "></i>SIMPAN</a>
                        </div>
                    </div>


                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <div id="modal_riwayat_penyakit_pasien" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-large">
            <div class="modal-content">
                <div class="modal-header" style="background: #fafafa">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b>Riwayat Penyakit Pasien</b></h2>
                </div>
                <div class="modal-body" style="background: #fafafa">
                    <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                        <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                    </div>
                </div>
            </div>        <!-- /.modal-content -->
        </div>    <!-- /.modal-dialog -->
    </div>

    <?php $this->load->view('footer');?>
