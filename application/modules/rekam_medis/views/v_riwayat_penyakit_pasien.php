<?php $this->load->view('header_iframe');?>
<body id="body">


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->

    <div class="row">
        <div class="col-md-12" style="padding-bottom:15px">
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?php echo $no_rekam_medis = $list_data_pasien->no_rekam_medis ;?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Nama Pasien</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?php echo $list_data_pasien->pasien_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Tanggal Lahir</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tglLhr = date('d-m-Y', strtotime($list_data_pasien->tanggal_lahir));
                                                if($tglLhr != "01-01-1970"){
                                                   echo date('d-m-Y', strtotime($list_data_pasien->tanggal_lahir)) ; 
                                                }else{
                                                    echo "";
                                                }
                                            ?>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php
                                                $hariIni = date('Y-m-d');
                                                $tglLhr  = $list_data_pasien->tanggal_lahir;
                                                if($tglLhr != "0000-00-00"){
                                                    $today = new DateTime($hariIni);
                                                    $bday = new DateTime($tglLhr);
                                                    $diff = $today->diff($bday);
                                                    echo $diff->y." Th ".$diff->m." Bln ".$diff->d." Hr";    
                                                }else{
                                                    echo "";
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?php echo $list_data_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Alamat</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?php echo $list_data_pasien->pasien_alamat; ?></td>
                                    </tr>
                                </table>
                                <!-- <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_data_pasien->pendaftaran_id; ?>"> -->
                                <input type="hidden" id="pasien_id" name="pasien_id" value='<?php echo $list_data_pasien->pasien_id; ?>'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <ul class="nav customtab nav-tabs" role="tablist">
                <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#a" class="nav-link active" aria-controls="rawat_jalan" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> RAWAT JALAN</span></a></li>
                <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#b" class="nav-link" aria-controls="rawat_inap" role="tab" data-toggle="tab" aria-expanded="false"  onclick="getRiwayatRI()"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">RAWAT INAP</span></a></li>  
                <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#c" class="nav-link" aria-controls="rawat_igd" role="tab" data-toggle="tab" aria-expanded="false" onclick="getRiwayatIGD()"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">RAWAT IGD</span></a></li>
            </ul>
            <div class="tab-content"> 
                <div role="tabpanel" class="tab-pane fade active in" id="a">
                    <div class="table-responsive">
                        <table id="table_riwayat_rawatjalan" class="table table-stripped table-hover" cellspacing="0"> 
                            <thead>
                                    <tr>  
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>No Pendaftaran</th>
                                        <!-- <th>No Registraasi</th> -->
                                        <th>Poli Klinik</th>
                                        <th>Jam Periksa</th>
                                        <th>Dokter</th> 
                                        <th>Detail</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr>   
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                        </table>
                    </div>
                </div> 
                
                <div role="tabpanel" class="tab-pane fade" id="b">
                    <div class="table-responsive">
                        <table id="table_riwayat_ri" class="table table-stripped table-hover" cellspacing="0"> 
                            <thead>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>No Pendaftaran</th>
                                <th>Poli Klinik</th>
                                <th>Jam Periksa</th>
                                <th>Dokter</th> 
                                <th>Detail</th>
                            </thead>
                            <tbody> 
                                <tr>   
                                    <td colspan="6">No Data to Display</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> 

                <div role="tabpanel" class="tab-pane fade" id="c">
                    <div class="table-responsive">
                        <table id="table_riwayat_igd" class="table table-stripped table-hover" cellspacing="0"> 
                            <thead>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>No Pendaftaran</th>
                                <th>Poli Klinik</th>
                                <th>Jam Periksa</th>
                                <th>Dokter</th> 
                                <th>Detail</th>
                            </thead>
                            <tbody> 
                                <tr>   
                                    <td colspan="6">No Data to Display</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> 
            </div>
        </div>


        <div id="modal_detail_diagnosa" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Riwayat Diagnosa</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                        <div class="table">
                            <table id="table_detail_diagnosa_rj" class="table table-striped dataTable" cellspacing="0">
                                 <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Diagnosa Perawat</th>
                                        <th>Nama Diagnosa Perawat</th>
                                        <th>Kode ICD 10</th>
                                        <th>Nama ICD 10</th>
                                        <th>Jenis Diagnosa</th>
                                        <th>Diagnosa By</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Diagnosa Perawat</th>
                                        <th>Nama Diagnosa Perawat</th>
                                        <th>Kode ICD 10</th>
                                        <th>Nama ICD 10</th>
                                        <th>Jenis Diagnosa</th>
                                        <th>Diagnosa By</th>

                                        <!-- <th>Pilih</th> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_detail_tindakan" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Riwayat Tindakan</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                        <div class="table" >
                            <table id="table_detail_tindakan_rj" class="table table-striped dataTable" cellspacing="0">
                                 <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Periksa</th>
                                        <th>Nama Tindakan</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Periksa</th>
                                        <th>Nama Tindakan</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_detail_obat" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Riwayat Obat</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                        <div class="table" >
                            <table id="table_detail_obat_rj" class="table table-striped dataTable" cellspacing="0">
                                 <thead>
                                    <tr>
                                        <th>Tanggal Periksa</th>
                                        <th>Nama Obat</th>
                                        <th>Jenis</th>
                                        <th>Satuan</th>
                                        <th>Jumlah</th>
                                        <th>Tercover Oleh</th>
                                        <th>Harga</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td colspan="8">No Data to Display</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Tanggal Periksa</th>
                                        <th>Nama Obat</th>
                                        <th>Jenis</th>
                                        <th>Satuan</th>
                                        <th>Jumlah</th>
                                        <th>Tercover Oleh</th>
                                        <th>Harga</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_detail_alergi" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Riwayat Alergi</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                        <div class="table" >
                            <table id="table_detail_alergi_rj" class="table table-striped dataTable" cellspacing="0">
                                 <thead>
                                    <tr>
                                        <th>No</th>        
                                        <th>Tanggal Periksa</th>
                                        <th>Nama Alergi</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td colspan="3">No Data to Display</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>        
                                        <th>Tanggal Periksa</th>
                                        <th>Nama Alergi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rekam_medis/list_pasien/riwayat_penyakit_pasien.js"></script>
</body>

</html>
