<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_masukpenunjang; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>      
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->dokterpengirim_nama; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->umur; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->dokterpengirim_nama; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_masukpenunjang; ?>" readonly>  
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->poli_ruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Tindakan</span></a></li>  

                <!-- <li role="presentation" class="active nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Tindakan</span></a></li>  --> 
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">  
                <div role="tabpanel" class="tab-pane active" id="tabs-tindakan"> 

                    <table id="table_tindakan_pasien_penunjang" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl. Tindakan</th>  
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Sub Total</th>
                                <th>Cyto</th>
                                <th>Total Harga</th>
                                <th>Batal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl. Tindakan</th>  
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Sub Total</th>
                                <th>Cyto</th>
                                <th>Total Harga</th>
                                <th>Batal</th>
                            </tr>
                        </tfoot>
                    </table>
                    <br>
                    
                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />     
                    <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                        <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="card_message"></p>
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-md-4">
                                 <label for="tgl_tindakan" class="control-label">
                                 <span style="color: red;">*</span>Tanggal Tindakan</label>
                                <input id="tgl_tindakan" class="form-control mydatepicker" type="text" name="tgl_tindakan" value=""> 
                            </div>
                            <div class="form-group col-md-4">
                                <label>Tindakan</label>  
                                <select id="tindakan" name="tindakan" class="form-control select2" onchange="getTarifTindakan()">
                                    <option value="" selected>Pilih Tindakan</option> 
                                </select> 
                                <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                                <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly> 
                            </div>
                           
                        </div>   
 
                        <div class="row">
                            <div class="form-group col-md-2">
                                <label for="jml_tindakan" class="control-label">
                                <span style="color: red;">*</span>Jumlah Tindakan</label>  
                                <input id="jml_tindakan" type="text" name="jml_tindakan" class="form-control" value=""  onkeyup="hitungHarga()">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="subtotal" class="active">Sub Total</label>
                                <input id="subtotal" type="text" name="subtotal" class="form-control" value="" placeholder="Sub Total" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Cyto</label> 
                                <select id="is_cyto" name="is_cyto" class="form-control" onchange="hitungHarga()">
                                    <option value="" disabled selected>Pilih Cyto</option>
                                    <option value="1">Ya</option>
                                    <option value="0">Tidak</option>
                                </select>
                            </div>  
                            <div class="form-group col-md-4"> 
                                <label for="totalharga" class="active">Total Harga</label>
                                <input id="totalharga" type="text" name="totalharga" class="form-control" value="" placeholder="Sub Total" readonly>   
                            </div> 
                        </div> 
                    </div> 
                    
                    <div class="col-md-12">  <br><br><br>
                        <button type="button" class="btn btn-success col-md-2" id="saveTindakan"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>



                    <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div>
            </div>  
        </div>
    </div>
</div>

  

 
<div id="modal_reseptur" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">  
        <div class="modal-content" style="overflow: auto;height: 365px;">     
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Obat</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">    
                <div class="table-responsive">
                   <table id="table_list_obat" class="table table-striped dataTable" cellspacing="0">   
                        <thead>
                            <tr>
                                <th>Nama Obat</th>
                                <th>Satuan</th>
                                <th>Jenis Obat</th>
                                <th>Harga Jual</th>
                                <th>Jumlah Stok</th> 
                                <th>Pilih</th>
                            </tr>  
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="6">No Data to Display</td>
                            </tr>
                        </tbody>
                    </table> 
                </div>  
            </div>  
            
        </div> 
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

 
<?php $this->load->view('footer_iframe');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/rekam_medis/pasien_penunjang/periksa_pasien_penunjang.js"></script>
</body>
 
</html>