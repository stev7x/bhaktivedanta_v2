<?php $this->load->view('header_iframe');?>
<body id="body">


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->

    <div class="row">
        <div class="col-md-12" style="padding-bottom:15px">
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">
                                    <tr>
                                        <td><b>Tgl.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Poliklinik</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Nama Asuransi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_asuransi; ?></td>
                                    </tr> 
                                    <tr>
                                        <td><b>No Asuransi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->no_asuransi; ?></td>
                                    </tr>   
                                    <tr>
                                        <td><b>Instansi Pekerjaan</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->instansi_pekerjaan; ?></td>
                                    </tr>   
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">
                                    <tr>
                                        <td><b>Nama Pasien</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                        <?php 
                                            // $list_pasien->umur; 
                                            $tgl_lahir = $list_pasien->tanggal_lahir;
                                            $umur  = new Datetime($tgl_lahir);
                                            $today = new Datetime();
                                            $diff  = $today->diff($umur);
                                            echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                        ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Alamat</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_alamat_domisili; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kasus Polisi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->kasus_polisi; ?></td>
                                    </tr> 
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr>
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Diagnosa</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Tindakan</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-icd9" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">ICD 9</span></a></li>

            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="diagnosa">
                    <table id="table_diagnosa_pasienrj" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>   
                                <th>Kode ICD 10</th>   
                                <th>Nama ICD 10</th>   
                                <th>Keterangan</th>
                                <th>Diagnosa By</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="6">No Data to Display</td>
                             </tr>
                        </tbody>
                         <tfoot>
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>   
                                <th>Kode ICD 10</th>   
                                <th>Nama ICD 10</th>   
                                <th>Keterangan</th>
                                <th>Diagnosa By</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>
                    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="pasien_id" id="pasien_id">
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                        <div >
                            <p id="card_message"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="control-label">
                            Kode Diagnosa<span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_diagnosa" name="kode_diagnosa" placeholder="Kode Diagnosa">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" title="Lihat List Diagnosa" data-toggle="modal" data-target="#modal_diagnosa10" onclick="dialogDiagnosa10()"><i class="fa fa-list"></i></button>
                              </span>
                            </div>
                            
                        </div> 
                        <div class="form-group col-md-3">   
                            <label class="control-label">Nama Diagnosa<span style="color: red;"> *</span></label>
                            <input id="nama_diagnosa" type="text" name="nama_diagnosa" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="control-label">
                            Kode Diagnosa ICD 10<span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_diagnosa_icd10" name="kode_diagnosa_icd10" placeholder="Kode Diagnosa">
                            </div>
                        </div> 
                        <div class="form-group col-md-3">   
                            <label class="control-label">Nama Diagnosa ICD 10<span style="color: red;"> *</span></label>
                            <input id="nama_diagnosa_icd10" type="text" name="nama_diagnosa_icd10" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                        <div class="form-group col-md-3">   
                            <label class="control-label">Jenis Diagnosa<span style="color: red;"> *</span></label>
                            <select id="jenis_diagnosa" name="jenis_diagnosa" class="form-control" >
                                <option value="" disabled selected>Pilih Jenis Diagnosa</option>
                                <option value="1">Diagnosa Utama</option>
                                <option value="2">Diagnosa Tambahan</option>
                            </select> 
                        </div>  
                        <div class="form-group col-md-2">
                            <button type="button" style="width: 100%; margin-top: 26px;" class="btn btn-success" id="saveDiagnosa">
                                <i class="fa fa-plus"></i> TAMBAH
                            </button>  
                        </div>
                    </div>                    
                        <?php echo form_close();?>
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tabs-tindakan">

                    <table id="table_tindakan_pasienrj" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <!-- <th>Sub Total</th>
                                <th>Cyto</th> -->
                                <th>Total Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <!-- <th>Sub Total</th>
                                <th>Cyto</th> -->
                                <th>Total Harga</th>
                            </tr>
                        </tfoot>
                    </table><br>

                    <!-- <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-6">
                            <label class="control-label">Input Tindakan</label>
                            <select  id="tindakan" name="tindakan" class="form-control" onchange="getTarifTindakan()" >
                                <option value="" disabled selected>Tindakan1</option>
                            </select>
                            <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">&nbsp;</label>
                             <input id="jml_tindakan" type="text" name="jml_tindakan" class="form-control" value="" onkeypress="return numbersOnly(event);" onkeyup="hitungHarga()">
                            <input type="hidden" id="subtotal" name="subtotal" value="0" readonly>
                            <input type="hidden" id="totalharga" name="totalharga" value="0" readonly>
                            <input type="hidden" id="is_cyto" name="is_cyto" value="0" readonly>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-success" id="saveTindakan"><i class="fa fa-floppy-o"></i> Simpan</button>
                        </div>

                    <?php echo form_close();?>  -->
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tabs-icd9">
                    <table id="table_icd9" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl. Tindakan</th>
                                <th>Kode ICD9</th>
                                <th>Deskripsi</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="4">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl. Tindakan</th>
                                <th>Kode ICD9</th>
                                <th>Deskripsi</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>
                    <?php echo form_open('#',array('id' => 'fmCreateICD9'))?>
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delICD9" name="<?php echo $this->security->get_csrf_token_name()?>_delICD9" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="card_message"></p>
                            </div>
                        </div>
                        
                        <div class="form-group col-md-4">
                            <label class="control-label">
                            Kode ICD9<span style="color: red;">* </span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_icd9" name="kode_icd9" placeholder="Kode ICD9">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" title="Lihat List Diagnosa" data-toggle="modal" data-target="#modal_diagnosa" onclick="dialogDiagnosa('icd9')"><i class="fa fa-list"></i></button>
                              </span>
                            </div>
                            
                        </div>


                        <div class="form-group col-md-6">
                            <label class="control-label">
                           Nama ICD9<span style="color: red;">* </span></label>
                            <input type="text" class="form-control" id="nama_icd9" name="nama_icd9" placeholder="Nama ICD9">
                        </div>
                        
                        <div class="form-group col-md-2">
                            
                            <button style="width: 100%; margin-top: 26px;" type="button" class="btn btn-success" onclick="saveICD9()"><i class="fa fa-plus"></i> TAMBAH</button>
                        </div>
                     <?php echo form_close()?>
                    <div class="clearfix"></div>
                </div>

            </div>

        </div>

    </div>

</div>


        <div id="modal_diagnosa10" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Diagnosa</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                    
                        <div class="table">
                            <table id="table_diagnosa10" class="table table-striped dataTable" cellspacing="0">
                                 <thead>
                                    <tr>
                                        <th>Kode Diagnosa Perawat</th>
                                        <th>Nama Diagnosa Perawat</th>
                                        <th>Kode ICD 10</th>
                                        <th>Nama ICD 10</th>
                                        <th>Pilih</th>
                                    </tr>
                                </thead> 

                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Kode Diagnosa Perawat</th>
                                        <th>Nama Diagnosa Perawat</th>
                                        <th>Kode ICD 10</th>
                                        <th>Nama ICD 10</th>
                                        <th>Pilih</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_diagnosa" class="modal fade bs-example-modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Diagnosa</b></h2>
                    </div>
                    <div class="modal-body" style="overflow: auto; height: 330px;">
                    
                        <div class="table">
                            <table id="table_diagnosa" class="table table-striped dataTable" cellspacing="0">
                                 <thead>
                                    <tr>
                                        <th>Kode ICD 9</th>
                                        <th>Nama ICD 9</th>
                                        <th>Pilih</th>
                                    </tr>
                                </thead> 

                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Kode ICD 9</th>
                                        <th>Nama ICD 9</th>
                                        <th>Pilih</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>




<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rekam_medis/pasienrj/periksa_pasienrj1.js"></script>
</body>

</html>
