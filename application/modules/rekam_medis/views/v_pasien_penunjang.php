<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Pasien Penunjang </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Rekam Medis</a></li>
                            <li class="active">Pasien Penunjang</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Data Pasien Penunjang  
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                        <label for="inputName1" class="control-label"></label>
                                            <dl class="text-right">  
                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                            </dl> 
                                        </div> 
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label>Tanggal Kunjungan,Dari</label>
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                     </div>
                                                </div>
                                                <div class="col-md-5">  
                                                    <label>Sampai</label>    
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                     </div>
                                                </div>
                                                <div class="col-md-2"> 
                                                    <label>&nbsp;</label><br>
                                                    <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                                </div> 
                                                <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="no_masukpenunjang">No. Pendaftaran</option>
                                                        <option value="no_rekam_medis">No. Rekam Medis</option>
                                                        <option value="no_bpjs">No. BPJS</option>
                                                        <option value="pasien_nama">Nama Pasien</option>   
                                                        <option value="pasien_alamat">Alamat Pasien</option>   
                                                    </select></b>
                                                         
                                                </div>    
                                                <div class="form-group col-md-9" style="margin-top: 7px">   
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                    <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasien()" >       
                                                </div> 
                                            </div>
                                        </div> 
                                    </div><br><hr style="margin-top: -27px"> 
                                     
                                   <table id="table_list_pasien_penunjang" class="table table-striped table-responsive" cellspacing="0">  
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>No. Pendaftaran</th>
                                                <th>Tgl. Pendaftaran</th>
                                                <th>No. Rekam Medis</th>
                                                <th>No. BPJS</th>
                                                <th>Nama Pasien</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur</th>
                                                <th>Alamat</th> 
                                                <th>Pembayaran</th> 
                                                <th>Kelas Pelayanan</th>
                                                <th>Status Penunjang</th> 
                                                <th>Status Pasien</th>
                                                <th>Periksa Pasien</th>
                                                <!-- <th>Batal Periksa</th>    -->
                                            </tr>  
                                        </thead> 
                                        <tbody>
                                            <tr>
                                                <td colspan="13">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>     
                        </div>
                        
                    </div>
                    
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid --> 


<div id="modal_periksa_penunjang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" onclick="reloadTablePasien()" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Pasien</b></h2>
             </div>  
            <div class="modal-body" style="background: #fafafa">        
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>    
                </div> 
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div id="modal_diagnosa_sudahpulang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">  
                <div class="modal-dialog modal-large">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #fafafa">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Pasien</b></h2>
                         </div>
                        <div class="modal-body" style="background: #fafafa">
                            <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<div id="modal_keterangan_keluar" onclick="reloadTablePasien()" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-large">
    <div class="modal-content">
        <div class="modal-header" style="background: #fafafa">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel"><b>Keterangan Keluar</b></h2>
         </div>
        <div class="modal-body" style="background: #fafafa">

        <div class="col-md-12">

            <div class="embed-responsive embed-responsive-16by9" style="height:500px;!important;padding: 0px !important">
            
                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
           
            </div>
            
           </div>
        
        </div>
    </div>
</div>
</div>

           

<?php $this->load->view('footer');?>
      