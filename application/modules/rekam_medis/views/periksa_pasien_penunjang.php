<?php $this->load->view('header_iframe');?>
<body>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <div class="row">
                <h5>Data Pasien</h5>
                <div class="divider"></div>
                <div id="card-alert" class="card green modal_notif" style="display:none;">
                    <div class="card-content white-text">
                        <p id="modal_card_message">SUCCESS : The page has been added.</p>
                    </div>
                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div>&nbsp;</div>
                <div class="col s6">
                    <div class="row">
                        <div class="input-field col s11">
                            <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                            <input id="tgl_pendaftaran" type="text" name="tgl_pendaftaran" value="<?php echo $list_pasien->tgl_masukpenunjang; ?>" readonly>
                            <label for="tgl_pendaftaran" class="active">Tgl. Pendaftaran</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="no_pendaftaran" type="text" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_masukpenunjang; ?>" readonly>
                            <label for="tgl_pendaftaran" class="active">No. Pendaftaran</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="no_rm" type="text" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                            <label for="no_rm" class="active">No. Rekam Medis</label>
                        </div>
                    </div>
                </div>
                <div class="col s6">
                    <div class="row">
                        <div class="input-field col s11">
                            <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>" readonly>
                            <input id="nama_pasien" type="text" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                            <label for="nama_pasien" class="active">Nama Pasien</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="umur" type="text" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                            <label for="umur" class="active">Umur</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s11">
                            <input id="jenis_kelamin" type="text" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                            <label for="jenis_kelamin" class="active">Jenis Kelamin</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s6">
                    <div class="row">
                        <div class="col s11">
                            <label>Dokter Pemeriksa<span style="color: red;"> *</span></label>
                            <select id="dokter_periksa" name="dokter_periksa" class="validate browser-default"><!-- onchange="getKabupaten()"-->
                                <option value="" disabled selected>Pilih Dokter</option>
                                <?php
                                $list_dokter = $this->Pasien_penunjang_model->get_dokter_list();
                                foreach($list_dokter as $list){
                                    echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <ul class="tabs tab-demo z-depth-1">
                        <li class="tab col s4"><a class="active" href="#tindakan_tab">Tindakan</a>
                        </li>
                    </ul>
                </div>
                <div class="col s12">
                    <div id="tindakan_tab" class="col s12">
                        <div>&nbsp;</div>
                        <div id="table-datatables">
                            <div class="row">
                                <div class="col s12 m4 l12">
                                    <table id="table_tindakan_pasien_penunjang" class="responsive-table display" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Tgl. Tindakan</th>
                                                <th>Nama Tindakan</th>
                                                <th>Jumlah Tindakan</th>
                                                <th>Sub Total</th>
                                                <th>Cyto</th>
                                                <th>Total Harga</th>
                                                <th>Batal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="7">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col s12">
                            <div>&nbsp;</div>
                            <div class="row">
                                <h5>Input Tindakan</h5>
                                <div class="divider"></div>
                                <div>&nbsp;</div>
                                <div class="col s12">
                                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                                    <div class="row">
                                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                                        <div >
                                            <p id="card_message"></p>
                                        </div>
                                    </div>
                                        <div class="input-field col s4">
                                            <input id="tgl_tindakan" class="datepicker_tindakan" type="text" name="tgl_tindakan" value="">
                                            <label for="tgl_tindakan" class="active">Tgl. Tindakan</label>
                                        </div>
                                        <div class="col s4">
                                            <label>Kelas Pelayanan</label>
                                            <select id="kelas_pelayanan" name="kelas_pelayanan" class="validate browser-default" onchange="getTindakan()">
                                                <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                                                <?php
                                                $list_kelaspelayanan = $this->Pasien_penunjang_model->get_kelas_pelayanan();
                                                foreach($list_kelaspelayanan as $list){
                                                    echo "<option value='".$list->kelaspelayanan_id."'>".$list->kelaspelayanan_nama."</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col s4">
                                            <label>Tindakan</label>
                                            <select id="tindakan" name="tindakan" class="validate browser-default" onchange="getTarifTindakan()">
                                                <option value="" disabled selected>Pilih Tindakan</option>
                                            </select>
                                            <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s2">
                                            <input id="jml_tindakan" type="text" name="jml_tindakan" class="validate" value="" onkeypress="return numbersOnly(event);" onkeyup="hitungHarga()">
                                            <label for="jml_tindakan" class="active">Jumlah Tindakan</label>
                                        </div>
                                        <div class="input-field col s4">
                                            <input id="subtotal" type="text" name="subtotal" class="validate" value="" placeholder="Sub Total" readonly>
                                            <label for="subtotal" class="active">Sub Total</label>
                                        </div>
                                        <div class="col s2">
                                            <label>Cyto</label>
                                            <select id="is_cyto" name="is_cyto" class="validate browser-default" onchange="hitungHarga()">
                                                <option value="" disabled selected>Pilih Cyto</option>
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        <div class="input-field col s4">
                                            <input id="totalharga" type="text" name="totalharga" class="validate" value="" placeholder="Sub Total" readonly>
                                            <label for="totalharga" class="active">Total Harga</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 m8 l9">
                                            <button class="btn light-green waves-effect waves-light darken-4" type="button" id="saveTindakan">
                                                <i class="mdi-navigation-check left"></i>Simpan
                                            </button>
                                        </div>
                                    </div>
                                    <?php echo form_close()?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rekam_medis/pasien_penunjang/periksa_pasien_penunjang.js"></script>
</body>

</html>
