<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Print Casemix</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <style>
            body{
                margin-top: 0;
            }
            #table_header{
                width: 100%;
                text-align: center;
            }
            .table_utama{
                width: 100%;
                /*overflow-x: auto;*/
                border-collapse: collapse;
                /*border-spacing: 0;*/
                
            }

            img{
                /*width: 90px;*/
                width: 100%;
            }
            .img{
                width: 100px;
                height: 100px;
            }
            p{
                line-height: 5%;
                font-size: 14px;
            }

            .norm {
                text-align: center;
                font-weight: bold;
            }

            .bold {
                font-weight: bold;
            }
        </style>
    </head>

    <body>
        <table id="table_header" style="margin-top: -16px"> 
            <tr>
                <td rowspan="6" width="25%"><img class="img"  src="<?= base_url() ?>/assets/plugins/images/header.png" alt="puri_bunda"></td>
                <td rowspan="6" width="50%"> 
                <?php if ($this->session->tempdata('data_session')[1] == 'Bhaktivedanta Medical Seminyak') { ?>
                   <p>CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p>BHAKTIVEDANTA MEDICAL SEMINYAK</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>Jl. Camplung Tanduk, Seminyak, Kuta, Kabupaten Badung, Bali 80361</small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>TELP (0361) 9343911</small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>Website:www.bvmedicalbali.com E-mail:bvmedicalbali@gmail.com</small></p> 
                   <p>BALI</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                <?php } ?>
                <?php if ($this->session->tempdata('data_session')[1] == 'Bhaktivedanta Medical Petitenget') { ?>
                   <p>CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p>BHAKTIVEDANTA MEDICAL BALI</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>Jl. Raya Petitenget No.1X-seminyak, kerobokan, badung, Kerobokan Kelod, Kec. Kuta Utara, Kabupaten Badung, Bali 80361 </small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>TELP (0361) 9343811</small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>Website:www.bvmedicalbali.com E-mail:bvmedicalbali@gmail.com</small></p>
                   <!-- <p><small>Website://puribundamalang.com E-mail:puribunda.malang@yahoo.com</small></p> CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p>BALI</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                <?php } ?> 
                
                <?php if ($this->session->tempdata('data_session')[1] == 'Bhaktivedanta Medical Canggu') { ?>
                   <p>CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p>BHAKTIVEDANTA MEDICAL CANGGU</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>Jl. Pantai Batu Bolong No.11 B, Canggu, Kec. Kuta Utara, Kabupaten Badung, Bali 80351 </small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>TELP 0811-3926-699</small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p><small>Website:www.bvmedicalbali.com E-mail:bvmedicalbali@gmail.com</small></p>
                   <!-- <p><small>Website://puribundamalang.com E-mail:puribunda.malang@yahoo.com</small></p> CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                   <p>BALI</p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
                <?php } ?>
                </td>
                <td rowspan="6" width="25%"><img src="<?= base_url() ?>/assets/plugins/images/logo-BPJS-Kesehatan.png" alt="bpjs"></td>
            </tr>
           
        </table>
        <table border="0px" width="100%" id="table_utama">   
            <tr>
                <td>KODE RS : 3573250</td>
                <td>KELAS RS : C</td>
            </tr>
        </table>
        <table border="1" width="100%" class="table_utama"> 
            <tr>
                <td>1</td>
                <td width="20%">NO.REKAM MEDIS</td>
                <?php $no_rm = str_split($data_pasien->no_rekam_medis); 

                ?>
                <td width="50" class="norm"><?= $no_rm[2]; ?></td>
                <td width="50" class="norm"><?= $no_rm[3]; ?></td>
                <td width="50" class="norm"><?= $no_rm[4]; ?></td>
                <td width="50" class="norm"><?= $no_rm[5]; ?></td>
                <td width="50" class="norm"><?= $no_rm[6]; ?></td> 
                <td width="50" class="norm"><?= $no_rm[7]; ?></td>

                <td width="20%" style="text-align: center">NO.ID</td>
                <td width="50">&nbsp;</td> 
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="50">&nbsp;</td> 
            </tr>
            <tr>
                <td>2</td>
                <td width="20%">NO KARTU</td>
                <td colspan="13" class="bold"><?= $data_pasien->no_bpjs ?></td>
            </tr>
            <tr>
                <td>3</td>
                <td width="20%">NAMA PASIEN</td>
                <td colspan="13" class="bold" style="text-transform: uppercase;"><?= $data_pasien->pasien_nama ?></td>
            </tr>
            <tr>
                <td>4</td>
                <td width="20%">TANGGAL LAHIR</td>
                <?php 
                    $tgl_lahir_pasien = date("d/m/Y", strtotime($data_pasien->tanggal_lahir));
                ?>
                <td colspan="13" class="bold"><?= $tgl_lahir_pasien ?></td>
            </tr>
            <tr>
                <td>5</td>
                <td width="20%">JENIS KELAMIN</td>
                <?php
                    $jk = $data_pasien->jenis_kelamin;
                ?>
                <td colspan="5" class="bold">
                    <input type="checkbox" name="tes" <?php echo $jk == "Laki-laki" ? "checked" : "disabled"  ?> />LAKI-LAKI
                </td>
                <td colspan="8" class="bold">
                    <input type="checkbox" name="tes" <?php echo $jk == "Perempuan" ? "checked" : "disabled"  ?> />PEREMPUAN
                </td>
            </tr>
            <tr>
                <td>6</td>
                <td width="20%">UMUR</td>
                <td colspan="13" class="bold">
                    <?php 
                       $lahir = $data_pasien->tanggal_lahir;
                        $born  = new Datetime($lahir);
                        $today = new Datetime(); 
                        $diff  = $today->diff($born); 
                        // if ($diff->y >= 1) {
                        //     echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
                            
                        // }

                        // if ($diff->y >= 1 ) {
                        //     echo $diff->y . " TAHUN" ;
                        // }elseif($diff->m >= 1){
                        //     echo $diff->m . " BULAN";

                        // }elseif ($diff->d >= 1) {
                        //     echo $diff->d . " HARI";
                            
                        // }else{
                        //     echo "string";
                        // }
                            echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
                            

                    ?>
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td width="20%">UMUR(BAYI)</td>
                <td colspan="5" align="right" > <span style="margin-right: 8px">HARI</span></td>
                <?php 
                    if ($diff->y < 1) { ?>
                        <!-- <td colspan="8" class="bold"> <?= $diff->d ?></td> -->
                                                
                    <?php }
                ?>
                <td colspan="8"> <b style="margin-left: 4px">Khusus untuk bayi <= 7 hari</b></td>
            </tr>
            <tr>
                <td>8</td>
                <td width="20%">BERAT LAHIR</td>
                <td colspan="5" align="right"><span style="margin-right: 8px"> GRAM</span></td>
                <!-- <td colspan="8"><b><?= @$data_pasien->berat_bayi ?></b></td> -->
                <td colspan="8"><b style="margin-left: 4px"> Khusus untuk bayi <= 7 hari</b></td>
            </tr>
            <tr>
                <td>9</td>
                <td width="20%">JENIS PERAWATAN</td> 
                <?php 
                    // $instalasi =  $data_pasien->instalasi_nama;
                ?>
                <td colspan="5">
                    <input type="checkbox" disabled <?php //echo $instalasi == "Instalasi Rawat Inap" ? "checked" : "disabled" ?> >RAWAT INAP
                </td>
                <td colspan="8" style="border-left: 0px">
                    <input type="checkbox" checked <?php //echo $instalasi == "Instalasi Rawat Jalan" ? "checked" : "disabled" ?>>RAWAT JALAN
                </td>
            </tr>
            <tr>
                <td>10</td>
                <td width="20%">KELAS PERAWATAN</td> 
                <td colspan="5"><input type="checkbox" name="" disabled>KELAS 1</td>
                <td colspan="2" ><input type="checkbox" name="" disabled>KELAS 2</td>
                <td colspan="6" ><input type="checkbox" name="" checked>KELAS 3</td>
            </tr>
            <tr>
                <td>11</td>
                <td width="20%">TANGGAL MASUK</td> 
                <td colspan="5" class="bold">
                    <?php
                    $masuk = date('d/m/Y', strtotime(@$data_pasien->tgl_pendaftaran));
                    $tgl_mas = @$data_pasien->tgl_pendaftaran;  
                    
                    if (empty($tgl_mas)) {
                        echo date('d/m/Y');
                    }else{
                        echo $masuk;
                    }

                     ?>
                         
                </td>
                <td colspan="8" >TGL/BL/TH</td>
            </tr>
            <tr>
                <td>12</td>
                <td width="20%">TANGGAL KELUAR</td> 
                <td colspan="5" class="bold">
                    <?php
                    // $masuk = date('d/m/Y', strtotime(@$data_pasien->tgl_pendaftaran)); 
                    if (empty($tgl_mas)) {
                        echo date('d/m/Y');
                    }else{
                        echo $masuk;
                    }

                    ?>
                     
                 </td>
                <td colspan="8" >TGL/BL/TH</td>
            </tr>
            <tr>
                <td>13</td>
                <td width="20%">LAMA DIRAWAT</td> 
                <td colspan="2" align="center"><b>1</b></td>
                <td colspan="3" >HARI</td>
                <td colspan="8" >Jumlah hari perawatan = tgl keluar - tgl masuk + 1</td>
            </tr>
            <tr>
                <td>14</td>
                <td width="20%">TOTAL BIAYA CBG</td> 
                <td colspan="13">&nbsp;</td>
            </tr>
            <tr>
                <td>15</td>
                <td width="20%">KODE CBG-JKN</td> 
                <td colspan="5">&nbsp;</td>
                <td colspan="8">Diisi Oleh Rekam Medis;</td>
            </tr>
            <tr>
                <td rowspan="3">16</td>
                <td width="20%" rowspan="3">CARA PULANG</td>  
                <td colspan="5"><input type="checkbox" name="" disabled>1.SEMBUH</td>
                <td colspan="8"><input type="checkbox" name="" disabled>4.MENINGGAL DUNIA</td>
            </tr>
            <tr>
                <td colspan="5"><input type="checkbox" name="" disabled>2.DIRUJUK</td>
                <td colspan="8"><input type="checkbox" name="" disabled>5.TIDAK TAHU</td>
            </tr>
            <tr>
                <td colspan="13"><input type="checkbox" name="" disabled>3.PULANG PAKSA</td>
            </tr>
            <tr>
                <td rowspan="2">17</td>
                <td rowspan="2" width="20%" style="text-align: center"><B>DIAGNOSA UTAMA</B></td>
                <td colspan="9" ></td> 
                <td colspan="5" style="text-align: center;"><B>CODE ICD-10</B></td>
            </tr>
            <tr>
                <td colspan="9">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>



            <tr>
                <td rowspan="5">18</td>
                <td rowspan="5" width="20%" style="text-align: center;" ><b>DIAGNOSA SEKUNDER</b></td>
                <td>1</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td>2</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td>3</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td>4</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td>5</td>
                <td colspan="8"></td>
                <td colspan="5"></td>
            </tr>

            <tr>
                <td>19</td>
                <td colspan="7" style="text-align: center;"><b>PROCEDUR / TINDAKAN</b></td>
                <td colspan="3" style="text-align: center;">TANGGAL</td>
                <td colspan="5" style="text-align: center;"><b>CODE ICD-9-CM</b></td>
            </tr>
            <tr>
                <td>20</td>
                <td colspan="7"></td>
                <td colspan="3"></td>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">&nbsp;</td> 
                <td colspan="3">&nbsp;</td>
                <td colspan="5">&nbsp;</td>
            </tr>


        </table>     

        <table width="100%" >
            
            <tr>
                <td width="10%" valign="top" style="text-align: right;">Catatan: </td>
                <td valign="top">   
                    <p >Diagnosa diisi oleh dokter yang merawat</p> 
                    <p>Untuk pasien yang dilakukan tindakan disertakan tanggal dilakukan tindakan tersebut</p> 
                    <p>  Beri tanda  ( √ )  pilihan </p> 
                </td>
            </tr>
        </table>
        <table width="100%" >
            <tr> 
                <td width="50%" style="text-align: center;"><h3> <input type="checkbox" name=""> <b>SEVERITY LEVEL 3</b></h3></td> 
                <td style="text-align: center;">
                    <p>DOKTER</p>
                    <p>PENANGGUNG JAWAB PASIEN (DPJP)</p><br><br><br><br>
                    <p>______________________________</p>
                </td>
            </tr>

        </table>

       
    </body>
</html>