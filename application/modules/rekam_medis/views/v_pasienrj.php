<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Rekam Medis - Pasien Rawat Jalan </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Rekam Medis</a></li>
                            <li class="active">Pasien Rawat Jalan</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Data Pasien Rawat Jalan
                                <!-- <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div> -->
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                  <div class="row">
                                      <div class="col-md-2">
                                      <label for="inputName1" class="control-label"></label>
                                          <dl class="text-right">
                                              <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                              <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>
                                          </dl>
                                      </div>
                                      <div class="col-md-10">
                                          <div class="row">
                                              <div class="col-md-5">
                                                  <label>Tanggal Kunjungan,Dari</label>
                                                  <div class="input-group">
                                                      <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                   </div>
                                              </div>
                                              <div class="col-md-5">
                                                  <label>Sampai</label>
                                                  <div class="input-group">
                                                      <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                   </div>
                                              </div>
                                              <div class="col-md-2">
                                                  <label>&nbsp;</label><br>
                                                  <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>
                                              </div>

                                              <div class="form-group col-md-3" style="margin-top: 7px">
                                                  <label for="inputName1" class="control-label"><b>Cari Berdasarkan :</b></label>
                                                  <b>
                                                  <select name="#" class="form-control select" style="margin-bottom: 7px" id="cari_pasien" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="no_pendaftaran">No. Pendaftaran</option>
                                                        <option value="no_rekam_medis">No. Rekam Medis</option>
                                                        <option value="no_bpjs">No. BPJS</option>   
                                                        <option value="nama_pasien">Nama Pasien</option>     
                                                        <option value="pasien_alamat">Alamat Pasien</option>

                                                    </select></b>

                                              </div>

                                              <div class="form-group col-md-9" >
                                                  <label for="inputName1" class="control-label"<b>&nbsp;</b></label>
                                                  <input type="Text" class="form-control pilih" style="margin-top: 7px" placeholder="Cari informasi yang anda butuhkan ....." onkeyup="cariPasien()" required>
                                              </div>
                

                                          </div>
                                      </div>
                                  </div>
                                  <br>
                                  <hr style="margin-top: -27px">

                                    <div class="">
                                           <table id="table_list_pasienrj" class="table table-striped table-responsive" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th>No. Pendaftaran</th>
                                                            <th>No. RM / Nama Pasien</th>
                                                            <th>Tanggal Pendaftaran</th>
                                                            <th>Poliklinik</th>
                                                            <th>Kelas Pelayanan</th>
                                                            <th>Dokter Penanggung Jawab</th>
                                                            <th>Jenis Kelamin</th>
                                                            <th>Umur Pasien</th>
                                                            <th>Alamat Pasien</th>
                                                            <th>Pembayaran</th>
                                                            <th>Status Periksa</th>
                                                            <th>Status Keluar</th>
                                                            <th>RS Rujukan</th>
                                                            <th>Diagnosa</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="14">No Data to Display</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                      </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--/row -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class="checkbox checkbox-info">
                                        <input id="checkbox1" type="checkbox" class="fxhdr">
                                        <label for="checkbox1"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox2" type="checkbox" class="fxsdr">
                                        <label for="checkbox2"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme working">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                                                </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <div id="modal_diagnosa" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-large">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #fafafa">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Pasien</b></h2>
                         </div>
                        <div class="modal-body" style="background: #fafafa">
                            <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal_diagnosa_sudahpulang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-large">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #fafafa">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Pasien</b></h2>
                         </div>
                        <div class="modal-body" style="background: #fafafa">
                            <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="modal_keterangan_keluar" onclick="reloadTablePasien()" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-large">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #fafafa">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h2 class="modal-title" id="myLargeModalLabel"><b>Keterangan Keluar</b></h2>
                         </div>
                        <div class="modal-body" style="background: #fafafa">

                        <div class="col-md-12">
                
                            <div class="embed-responsive embed-responsive-16by9" style="height:500px;!important;padding: 0px !important">
                            
                                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                           
                            </div>
                            
                           </div>
                        
                        </div>
                    </div>
                </div>
            </div>

           
            


<?php $this->load->view('footer');?>
