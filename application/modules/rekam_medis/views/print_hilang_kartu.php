<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table {
			border-collapse: collapse;

		}
		th {
			text-align: left;
			width: 175px;
		}
		
		.rows{
			width: 250px;
		}
		
		.center{
			text-align: center;
			font-weight: bold;
		}
	</style>
</head>
<body onload="window.print()">
	<table border="2" align="center">
		<tr>
			<td style="text-align: center; font-weight: bold;" colspan="3" > FORM DATA PASIEN</td>
		</tr>
		<tr>
			<th>NAMA</th>
			<td class="center">:</td>
			<td class="rows"><?php echo $data_pasien->pasien_nama; ?></td>
		</tr>
		<tr>
			<th>TANGGAL LAHIR</th>
			<td class="center">:</td>
			<td class="rows"><?php echo $data_pasien->tanggal_lahir; ?></td>
		</tr>
		<tr>
			<th>ALAMAT</th>
			<td class="center">:</td>
			<td class="rows"><?php echo $data_pasien->pasien_alamat; ?></td>
		</tr>
		<tr>
			<th>NO.TELP</th>
			<td class="center">:</td>
			<td class="rows"><?php echo $data_pasien->tlp_selular; ?></td>
		</tr>
		<tr>
			<th>NAMA SUAMI</th>
			<td class="center">:</td>
			<td class="rows"><?php echo $data_pasien->nama_suami_istri; ?></td>
		</tr>
		<tr>
			<th>NAMA AYAH/IBU</th>
			<td class="center">:</td>
			<td class="rows"><?php echo $data_pasien->nama_orangtua; ?></td>
		</tr>
	</table>
</body>
</html>