<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Print Casemix</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            body,td,th {
                font-size: 16px;
                font-family: "Times New Roman", Times, serif;
            }
            #table_header{
                width: 100%;
                text-align: center;
            }
            #table_utama{
                width: 100%;
                overflow-x: auto;
                border-collapse: collapse;
                border-spacing: 0;
                border: 1px solid black;
                
            }

            img{
                width: 90px;
                height: 37;
            }
            p{
                line-height: 5%;
            }

            #footer{
                position:relative;
                left:15%;
                text-align:center;
            }
        </style>
            <link href="<?php echo base_url()?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">  

    </head>
    <body onload="window.print()">
        <div style="position: absolute; display: inline-block; width: 100%;">
            
            <div style="position: relative; float: left;">
                <img src="<?php echo base_url()?>assets/plugins/images/logo2.png">
            </div>
            <div style="position: relative; float: right;" >
                <img style="width: 200px;" src="<?php echo base_url()?>assets/plugins/images/logo-BPJS-Kesehatan.png">
            </div>
        </div>
        <table id="table_header" border="0px">
            <tr border="1px">
                <!-- <td rowspan="6" width="25%"><img src="logo-puri" alt="puri_bunda"></td> -->
                <td colspan="2" >CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL</td>
                <!-- <td rowspan="6" width="25%"><img src="logo-bpjs1.png" alt="bpjs"></td> -->
            </tr>
            <tr>
                <td colspan="2">RUMAH SAKIT IBU DAN ANAK PURI BUNDA</td>
            </tr>
            <tr>
                <td colspan="2"><p><small>SIMPANG SULFAT UTARA 60A MALANG - 65124</small></p></td>
            </tr>
            <tr>
                <td colspan="2"><p><small>(0341) 480047, 477511 Fax. (0341) 485990</small></p></td>
            </tr>
            <tr>
                <td colspan="2">Website</td>
            </tr>
            <tr>
                <td colspan="2">MALANG - JAWA TIMUR</td>
            </tr>
           
        </table>
        <br><br>
        <table border="0px" width="100%">
            <tr>
                <td style="text-align:right;" width="25%"><b>KODE RS : </b></td>
                <td width="25%"><b>3573250</b></td>
                <td width="25%" style="text-align: right;"><b>KELAS RS : </b></td>
                <td width="25%" style="text-align:left;"><b>C</b></td>
            </tr>
        </table>
        <table id="table_utama" border="1px">
            <tbody>
                <tr>
                    <td>1</td>
                    <td>NO REKAM MEDIS</td>
                    <td><?php echo $data_pasien->no_rekam_medis; ?></td>
                    <td style="text-align:center;">NO ID : </td>
                    <td></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td   >NO KARTU</td>
                    <td colspan="3"><?php //echo $data_pasien->no_pendaftaran; ?></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td   >NAMA PASIEN</td>
                    <td colspan="3"><?= $data_pasien->pasien_nama ?></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td   >TANGGAL LAHIR</td>
                    <td colspan="3"><?php echo date('d/m/Y', strtotime($data_pasien->tanggal_lahir)); ?></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td   >JENIS KELAMIN</td>
                    <td colspan="3"><?php echo $data_pasien->jenis_kelamin; ?></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td   >UMUR</td>
                    <td colspan="3">
                    <?php 
                       $lahir = $data_pasien->tanggal_lahir;
                        $born  = new Datetime($lahir);
                        $today = new Datetime(); 
                        $diff  = $today->diff($born); 
                        echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>7</td>
                    <td   >UMUR (BAYI)</td>
                    <td   >HARI : </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>8</td>
                    <td   >BERAT LAHIR (BAYI)</td>
                    <td   >GRAM : </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>9</td>
                    <td   >JENIS PERAWATAN</td>
                    <td colspan="3"><?php echo $data_pasien->instalasi_nama;?></td>
                </tr>
                <tr>
                    <td>10</td>
                    <td   >KELAS PERAWATAN</td>
                    <td colspan="3"><?php //$data_pasien->kelaspelayanan_nama ?></td>
                </tr>
                <tr>
                    <td>11</td>
                    <td   >TANGGAL MASUK</td>
                    <td colspan="2"><?php $masuk = date('d m Y', strtotime($data_pasien->tgl_pendaftaran)); echo $masuk;?></td>
                    <td   >TGL/BLN/THN</td>
                </tr>
                <tr>
                    <td>12</td>
                    <td   >TANGGAL KELUAR</td>
                    <td colspan="2"><?php //$pulang = date('d m Y', strtotime($data_pasien->tgl_pendaftaran)); echo $pulang;?></td>
                    <td   >TGL/BLN/THN</td>
                </tr>
                <tr>
                    <td>13</td>
                    <td   >LAMA DIRAWAT</td>
                    <td colspan="2"><?php //$lama_rawat = ($pulang-$masuk)+1; echo $lama_rawat;?></td>
                    <td   >JUMLAH HARI</td>
                </tr>
                <tr>
                    <td>13</td>
                    <td   >TOTAL BIAYA CBG</td>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td>15</td>
                    <td   >KODE CBG-JKN</td>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td>16</td>
                    <td   >CARA PULANG</td>
                    <td colspan="3"><?php echo $data_pasien->carapulang;?></td>
                </tr>                
                <tr>
                    <td rowspan="2">17</td>
                    <td rowspan="2"   >DIAGNOSA UTAMA</td>
                    <td colspan="2" style="text-align:center;"   ><b>NAMA DIAGNOSA</b></td>
                    <td style="text-align: center;"><b>ICD-10</b></td>
                </tr>           

            <!-- menggunakan foreach -->

                <?php
                        // $no = 0;
                    // for($i = 1;$i <= 5; $i++){
                        // $no++;
                    // $diagnosaa = $this->Pasien_rj_model->get_data_diagnosa(1,$data_pasien->pendaftaran_id);
                    // foreach ($diagnosaa as $list) { 
                        ?>
                <!-- <tr> -->
                    <!-- <td colspan="2"><?php  echo (!$no) ? ' ' : $no .'. '; ?></td>
                    <td colspan="0"><?php  echo $no.'. '; ?></td>
                    <td colspan="2"><?php  echo (!$no) ? ' ' : $no .'. '.$list->nama_diagnosa; ?></td>
                    <td colspan="0"><?php  echo $no.'. '.$list->kode_diagnosa; ?></td> -->
                <!-- </tr> -->
                    <?php  ?>

            <!-- end foreach     -->
            
            <!-- tanpa for -->
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="0">&nbsp;</td>
                </tr>
            <!-- end tanpa for -->
               
                <tr>
                    <td rowspan="6">18</td>
                    <td rowspan="6">DIAGNOSA SEKUNDER</td>
                    <!-- <td colspan="2"></td>
                    <td></td> -->
                </tr>               
                <!-- menggunakan for -->
                <?php
                for($i = 1; $i <= 5; $i++){?>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="0">&nbsp;</td>
                </tr> 
                <?php } ?>
                <tr>
                    <td>19</td>
                    <td colspan="2">PROCEDUR / TINDAKAN</td>
                    <td>TANGGAL</td>
                    <td>CODE ICD-9-CM</td>
                </tr>
                <tr>
                    <td>20</td>
                    <td colspan="2"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <?php 
                for ($i=1; $i <=9 ; $i++) { ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                <?php } ?>
                <!-- end for -->


 <!-- foreach   -->
<!--               
                <tr>
                    <td colspan="3">
                        <table border="0px" width="100%" style="border-collapse:collapse;">
                            
                            
                            <?php 
                                $no = 0;
                            
                                for ($i = 1; $i <=5; $i++){
                                $diagnosa = $this->Pasien_rj_model->get_data_diagnosa(2,$data_pasien->pendaftaran_id);
                                // print_r($diagnosa);die();
                                 $no++;
                                foreach ($diagnosa as $list) { 
                                   
                                ?>
                            <tr>
                            

                                <td><?php echo $no.'. '. $list->nama_diagnosa; ?></td>
                                <td style="border-left:1px solid black;" width="22.8%"><?php echo $no.'. '. $list->kode_diagnosa; ?></td>
                                
                            </tr>
                                <?php }} ?>

                        </table>
                     </td>
                </tr>  -->
                <!-- end foreach -->

                <tr>
                    <td rowspan="10">19</td>
                    <td colspan="2">PROCEDUR / TINDAKAN</td>
                    <td style="text-align: center;">TANGGAL</td>
                    <td style="text-align: center;">ICD-9</td>
                </tr>

                <tr>
                    <td colspan="5">
                                    <table width="100%" border="0px" style="border-collapse:collapse;">
                                        <!-- <?php
                                            $tindakan = $this->Pasien_rj_model->get_data_tindakan($data_pasien->pendaftaran_id);
                                            $no = 0;
                                            foreach ($tindakan as $list) {
                                            $no++;?>
                                            <tr>
                                                <td><?php echo $no.'. '.$list->nama_tindakan ; ?></td>
                                                <td width="19.1%" style="border-left:1px solid black;"><?php echo date('d/m/Y', strtotime($list->tanggal_tindakan));?></td>
                                                <td width="13.4%" style="border-left:1px solid black;"><?php echo $list->kode_icd_9_cm; ?></td>
                                            </tr>
                                            <?php } ?> -->

                                            <?php for($i = 1; $i <=5;$i++){?>
                                            <tr>
                                                <td width="450px">&nbsp;</td>
                                                <td width="128px" style="border-left:1px solid black;">&nbsp;</td>
                                                <td width="85px" style="border-left:1px solid black;">&nbsp;</td>
                                            </tr>
                                            <?php } ?>
                                    </table>
                    </td>
                
                </tr>                
            </tbody>
        </table>
        <table>
            <tr>
                 <td rowspan="0"><p>Catatan :</p></td>                               
                 <td rowspan="0"><br>Diagnosa diisi oleh dokter yang merawat<br>Untuk pasien yang dilakukan tindakan disertakan tanggal dilakukan tindakan tersebut</td>                               
            </tr>
            
        </table>
        <br>
        <table width="100%">
            <tr>
                <td width="50%" valign="top">
                    <table> 
                        <tr>
                            <td valign="top" style="font-size:12pt;"><i class="fa fa-square-o fa-2x"></i>  SECURITY LEVEL 3</td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table id="footer">
                        <tr>
                            <td>DOKTER</td>
                        </tr>
                        <tr>
                            <td>PENANGGUNG JAWAB PASIEN (DPJP)</td>
                        </tr>
                        <tr>
                            <td bgcolor="white" height="80px"></td>
                        </tr>
                        <tr>
                            <td><b>_______________________</b></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
        
        
        
        
       
    </body>
</html>