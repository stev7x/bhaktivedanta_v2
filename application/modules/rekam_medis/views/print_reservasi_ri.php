
<!DOCTYPE html>
<head>
<title>Sticker Pasien</title>
</head>

<style type="text/css">
  body{
    margin: 0;
  }
	td {
		font-weight: bold;
	}
  
</style>

<body onload="window.print()"><font size="2">
<table align="center">
          <tr>
            <td>No. RM</td>
            <td>:</td>
            <td><b style="font-size: 20px;"><?php echo $data_pasien->no_rekam_medis; ?><b></td>
          </tr>
          <tr>
            <td> Nama</td>
            <td>:</td>
            <td><?php echo $data_pasien->pasien_nama; ?></td>
          </tr>
          <tr>
            <td>Tgl Lahir</td>
            <td>:</td>

            <td><?php echo date("d-m-Y", strtotime($data_pasien->tanggal_lahir)) ?></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?php echo $data_pasien->pasien_alamat; ?></td>
          </tr>
          <tr>
            <td>Instalasi</td>
            <td>:</td>
            <td><?php echo $data_pasien->instalasi_nama; ?></td>
          </tr>
          <tr>
            <td>Dokter</td>
            <td>:</td>
            <td><?php echo $data_pasien->NAME_DOKTER; ?></td>
          </tr>
          <tr>
            <td>Tanggal Reservasi</td>
            <td>:</td>
            <td><?php echo $data_pasien->tanggal_reservasi; ?></td>
          </tr>
                
    
  
</table>
</font>
</body>
</html>