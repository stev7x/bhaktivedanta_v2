<?php $this->load->view("header_iframe") ?> 
<style type="text/css" rel="stylesheet">
    body{background:#f5f5f5 !important;} 
</style>      
<div class="col-sm-12" style="background: #f5f5f5;padding: 20px">   
    <div class="panel panel-info1"> 
        <div class="panel-heading"> Form Input Surat Ket.Lahir 
        </div>  
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div class="panel-body">
            <?php echo form_open('#',array('id' => 'formSurat'))?>
                <div class="row">           
                    <div class="form-group col-md-12">
                        <label>Nama Ibu<span style="color: red">*</span></label> 
                        <input type="text" name="nama_ibu" value="<?=$data_pasien->nama_orangtua?>" class="form-control" placeholder="Nama Ibu">
                        <input type="hidden" name="pasien_id" value="<?=$data_pasien->pasien_id?>" class="form-control">
                        <input type="hidden" name="nama_dokter" value="<?=$data_pasien->NAME_DOKTER?>" class="form-control">
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Nama Ayah<span style="color: red">*</span></label> 
                        <input type="text" name="nama_ayah" value="<?=$data_pasien->nama_suami_istri?>" class="form-control" placeholder="Nama Ayah">
                    </div>
                    <div class="form-group col-md-12"> 
                        <label>Alamat Rumah<span style="color: red">*</span></label>  
                        <textarea placeholder="alamat_rumah" class="form-control" name="alamat"><?=$data_pasien->pasien_alamat?></textarea>
                        <!-- <input type="text" name="nama_obat" class="form-control" placeholder="Alamat Rumah"> -->
                    </div>
                    <div class="form-group col-md-12">
                        <label for="tanggal_lahir" class="control-label">Hari / Tanggal<span style="color:red">*</span></label>
                        <div class="input-group">           
                            <input name="tgl_lahir" id="tgl_lahir" value="<?= date("l, d F Y", strtotime($data_pasien->tanggal_lahir)); ?>" type="text" class="form-control mydatepicker1" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                        </div>
                    </div>   
                    <div class="form-group col-md-12">
                        <label>Nama Anak<span style="color: red">*</span></label> 
                        <input type="text" name="nama_anak" value="<?=$data_pasien->pasien_nama?>" class="form-control" placeholder="Nama Anak"> 
                    </div>   
                    <div class="form-group col-md-12">
                        <label>Anak Ke <span style="color: red">*</span></label> 
                        <input type="text" name="anak_ke" class="form-control" placeholder="Anak Ke">
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Panjang Badan <span style="color: red">*</span></label> 
                        <input type="text" name="panjang_badan" class="form-control" placeholder="Panjang Badan">
                    </div>   
                    <div class="form-group col-md-12">
                        <label>Berat Badan <span style="color: red">*</span></label>   
                        <input type="text" name="berat_badan" class="form-control" placeholder="Berat Badan">
                    </div>       

                </div>
                 <div class="row">  
                    <div class="col-md-12">    
                        <button id="simpanSurat"  type="button" class="btn btn-success pull-right" onclick="do_print()"><i class="fa fa-floppy-o p-r-10"></i>
                        SIMPAN</button>            
                        <button hidden id="printSurat" type="button" class="btn btn-info pull-right"><i class="fa fa-print p-r-10"></i>
                        PRINT</button> 
                    </div>  
                </div>  
             <?php echo form_close() ?>
            </div>
        </div>     
    </div>
</div>
 
<?php $this->load->view("footer_iframe") ?>        
<script src="<?php echo base_url()?>assets/dist/js/pages/rekam_medis/input_surat.js"></script>