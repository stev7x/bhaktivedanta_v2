<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>

      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Info Pasien Rawat Inap</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Rekam Medis</a></li>
                            <li class="active">Info Pasien Rawat Inap</li> 
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div> 
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">     
                            <div class="panel-heading"> Data Kamar Pasien Rawat Inap 
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                <div class="table-responsive"> 
                                     <table id="table_daftar_kamar_list" class="table table-striped dataTable" cellspacing="0">       
                                        <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Ruangan</th>
                                                    <th>Kelas Pelayanan</th>
                                                    <th>No. Kamar</th>
                                                    <th>No. Bad</th>
                                                    <th>Status</th>
                                                    <th>no. RM / Nama Pasien</th>
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>  
                                    </div>
                                </div>
                            </div>     
                        </div>
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->

           

<?php $this->load->view('footer');?>
      