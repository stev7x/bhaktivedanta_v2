<?php $this->load->view('header_iframe');?>
<body id="body">


<div class="white-box" style="height:500px; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->

    <div class="row">
      
        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            
            <!-- Tab panes -->
                                <?php echo form_open('#',array('id' => 'fmCreateStatusPulang'))?>
                                        <input type="text" id="pendaftaran_id_pulang" name="pendaftaran_id_pulang" value="<?php echo $list_pasien->pendaftaran_id; ?>" hidden>
                                        <div class="form-group">
                                        <label class="control-label">Keterangan Keluar</label>
                                        <select class="form-control" onchange="is_rujuk()" id="status_pulang" name="status_pulang">
                                            <option value="" disabled selected>Pilih Ket. Keluar</option>
                                            <option value="KRS">KRS</option>
                                            <option value="MRS">MRS</option>
                                            <option value="PAPS">PAPS</option>
                                            <option value="Meninggal">Meninggal</option>
                                            <option value="Rujuk">Rujuk</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="rujukan_rs" style="display:none;">
                                        <div class="form-group">
                                            <label class="control-label">RS Rujukan</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="nama_rs" name="nama_rs" placeholder="Nama RS Rujukan" readonly>
                                                <span class="input-group-btn">
                                                <button class="btn btn-info" type="button" title="Lihat Daftar RS Rujukan" data-toggle="modal" data-target="#modal_rs_rujukan"><i class="fa fa-list"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                     <button type="button" class="btn btn-success pull-right" id="saveStatusPulang"><i class="fa fa-floppy-o"></i> Simpan</button>
                                    </div>
                                <?php echo form_close()?>
                              
                        </div>
                    </div>


        <div id="modal_rs_rujukan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Rumah Sakit Rujukan</b></h2>
                    </div>
                    <div class="modal-body">
                        <div class="table">
                            <table id="table_rs_rujukan" class="table table-striped dataTable" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Kode Rumah Sakit</th>
                                        <th>Nama Rumah Sakit</th>
                                        <th>Alamat</th>
                                        <th>Pilih</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6">No Data to Display</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>




<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/pasien_rd/periksa_pasienrd.js"></script>
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/pasien_rd.js"></script>

</body>

</html>
