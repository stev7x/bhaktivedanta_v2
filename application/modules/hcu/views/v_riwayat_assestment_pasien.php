<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row" style="padding-bottom:15px">
        <div class="panel panel-info" style="width: 100%">
            <div class="panel-heading" align="center"> Data Pasien 
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body" style="border:1px solid #f5f5f5">
                    <div class="row">
                        <div class="col-md-6">
                            <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                <tr>   
                                    <td><b>Tgl.Pendaftaran</b></td> 
                                    <td>:</td> 
                                    <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                </tr>
                                <tr>
                                    <td><b>No.Pendaftaran</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                </tr>
                                <tr>
                                    <td><b>No.Rekam Medis</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                </tr>     
                                <tr>
                                    <td><b>Ruangan</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Dokter Pemeriksa</b></td>
                                    <td>:</td>       
                                    <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                </tr>  
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table  width="400px" style="font-size:16px;text-align: left;">       
                                <tr>
                                    <td><b>Nama Pasien</b></td> 
                                    <td>:</td> 
                                    <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                </tr>    
                                <tr>
                                    <td><b>Umur</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px">
                                        <?php 
                                            $tgl_lahir = $list_pasien->tanggal_lahir;
                                            $umur  = new Datetime($tgl_lahir);
                                            $today = new Datetime();
                                            $diff  = $today->diff($umur);
                                            echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Jenis Kelamin</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Kelas Pelayanan</b></td>
                                    <td>:</td> 
                                    <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Type Pembayaran</b></td>
                                    <td>:</td>  
                                    <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                </tr> 
                            </table>
                            <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                            <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                            <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                            <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                            <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                            <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                            <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                            <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                            <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                            <input type="hidden" id="dokter_id" name="dokter_id" value="<?php echo $list_pasien->id_M_DOKTER;?>">
                            <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id;?>">
                        </div>
                    </div>
                </div>  
            </div>
        </div>  
    </div>

    <div class="row"> 
        <div class="container-fluid" style="width: 100%">
            <table id="table_list_assestment_pasienhcu" class="table table-striped table-responsive" cellspacing="0">  
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tensi</th>
                        <th>Tensi Nadi</th>
                        <th>Suhu</th>
                        <th>Suhu Nadi</th>
                        <th>Penggunaan O2</th>
                        <th>Saturasi</th>
                        <th>Nyeri</th>
                        <th>Wong Baker</th>
                        <th>Resiko Jatuh</th>
                        <th>Program Therapy</th>
                        <th>Aksi</th>
                    </tr>
                </thead> 
                <tbody>
                    <tr>
                        <td colspan="12">No data to display</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th> 
                        <th>Tensi</th>
                        <th>Tensi Nadi</th>
                        <th>Suhu</th>
                        <th>Suhu Nadi</th>
                        <th>Penggunaan O2</th>
                        <th>Saturasi</th>
                        <th>Nyeri</th>
                        <th>Wong Baker</th>
                        <th>Resiko Jatuh</th>
                        <th>Program Therapy</th>
                        <th>Aksi</th>
                    </tr>
                </tfoot>
            </table> 
        </div>
    </div>
</div>

<?php $this->load->view('footer_iframe');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/hcu/riwayat_assestment.js"></script>
</body>
 
</html>