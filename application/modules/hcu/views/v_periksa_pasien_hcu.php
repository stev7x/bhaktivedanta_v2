<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row" style="padding-bottom:15px">
        <div class="panel panel-info" style="width: 100%">
            <div class="panel-heading" align="center"> Data Pasien 
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body" style="border:1px solid #f5f5f5">
                    <div class="row">
                        <div class="col-md-6">
                            <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                <tr>   
                                    <td><b>Tgl.Pendaftaran</b></td> 
                                    <td>:</td> 
                                    <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                </tr>
                                <tr>
                                    <td><b>No.Pendaftaran</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                </tr>
                                <tr>
                                    <td><b>No.Rekam Medis</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                </tr>     
                                <tr>
                                    <td><b>Ruangan</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Dokter Pemeriksa</b></td>
                                    <td>:</td>       
                                    <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                </tr>  
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table  width="400px" style="font-size:16px;text-align: left;">       
                                <tr>
                                    <td><b>Nama Pasien</b></td> 
                                    <td>:</td> 
                                    <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                </tr>    
                                <tr>
                                    <td><b>Umur</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px">
                                        <?php 
                                            $tgl_lahir = $list_pasien->tanggal_lahir;
                                            $umur  = new Datetime($tgl_lahir);
                                            $today = new Datetime();
                                            $diff  = $today->diff($umur);
                                            echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Jenis Kelamin</b></td>
                                    <td>:</td>
                                    <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Kelas Pelayanan</b></td>
                                    <td>:</td> 
                                    <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Type Pembayaran</b></td>
                                    <td>:</td>  
                                    <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                </tr> 
                            </table>
                            <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                            <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                            <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                            <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                            <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                            <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                            <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                            <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                            <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                            <input type="hidden" id="dokter_id" name="dokter_id" value="<?php echo $list_pasien->id_M_DOKTER;?>">
                            <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id;?>">
                        </div>
                    </div>
                </div>  
            </div>
        </div>  
    </div>

    <div class="row"> 
        <div class="container-fluid" style="width: 100%">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#assestment" class="nav-link" aria-controls="assestment" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Assestment</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#background" class="nav-link" aria-controls="background" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Background</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Diagnosa</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#tindakan_hcu" class="nav-link" aria-controls="tindakan_hcu" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Tindakan</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#reseptur_obat" class="nav-link" aria-controls="reseptur_obat" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Reseptur Obat</span></a></li> 
            </ul>
        </div>
        
        <div class="container-fluid" style="width: 100%">
            <!-- Tab panes -->
            <div class="tab-content" style="width: 100%"> 
                <div role="tabpanel" class="tab-pane active" id="assestment">
                    <div class="panel panel-default" style="margin:0">
                        <div class="panel-body">
                            <h3>Tanda - tanda vital</h3>
                            <form class="row" id="fmCreateAssestment">
                                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Tensi</label>
                                        </div>
                                        <div class="row">
                                            <input type="number" class="form-control col-sm-10" id="tensi" name="tensi">
                                            <label for="tensi" class="label-control col-sm-2">mmHG</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Suhu</label>
                                        </div>
                                        <div class="row">
                                            <input type="number" class="form-control col-sm-10 col-sm-offset-2" id="suhu" name="suhu">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Penggunaan O<sub>2</sub></label>
                                        </div>
                                        <div class="row">
                                            <input type="number" class="form-control col-sm-10" id="penggunaan_o2" name="penggunaan_o2">
                                            <label for="penggunaan_o2" class="label-control col-sm-2">lt/menit</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Nyeri</label>
                                        </div>
                                        <select class="form-control row" id="nyeri" name="nyeri">
                                            <option value="">Pilih Nyeri</option>
                                            <?php
                                                foreach($nyeri as $item) {
                                                    echo ("
                                                        <option value='$item->lookup_id'>$item->name</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Resiko Jatuh</label>
                                        </div>
                                        <div class="row">
                                            <input type="text" class="form-control col-sm-10 col-sm-offset-2" id="resiko_jatuh" name="resiko_jatuh">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Nadi</label>
                                        </div>
                                        <div class="row">
                                            <input type="number" class="form-control col-sm-10" id="tensi_nadi" name="tensi_nadi">
                                            <label for="tensi_nadi" class="label-control col-sm-2">x/menit</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Nadi</label>
                                        </div>
                                        <div class="row">
                                            <input type="number" class="form-control col-sm-10" id="suhu_nadi" name="suhu_nadi">
                                            <label for="suhu_nadi" class="label-control col-sm-2">x/menit</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Saturasi O<sub>2</sub></label>
                                        </div>
                                        <div class="row">
                                            <input type="number" class="form-control col-sm-10" id="saturasi_o2" name="saturasi_o2">
                                            <label for="saturasi_o2" class="label-control col-sm-2">%</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Numeric / Wong Baker</label>
                                        </div>
                                        <select class="form-control row" id="numeric_wong_baker" name="numeric_wong_baker">
                                            <option value="">Pilih Wong Baker</option>
                                            <?php
                                                foreach($wong_baker as $item) {
                                                    echo ("
                                                        <option value='$item->lookup_id'>$item->name</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div class="row table-responsive" style="width: 100%">
                                <table id="table_list_program_theraphy_hcu" class="table table-striped" cellspacing="0" style="width: 100%">  
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Program Theraphy</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Program Theraphy</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                </table> 
                            </div>
                            <h3>Program Theraphy</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <label class="label-control">Program Therapy</label>
                                    </div>
                                    <select class="form-control row" id="program_therapy" name="program_therapy">
                                        <option value="">Pilih Program Therapy</option>
                                        <?php
                                            foreach($therapy as $item) {
                                                echo ("
                                                    <option value='$item->program_therapy_id'>$item->nama</option>
                                                ");
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <label class="label-control">&nbsp;</label>
                                    </div>
                                    <button class="btn btn-info" onclick="addProgramTherapy()">Tambah</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-success pull-left" data-toggle="modal" data-target="#modal_riwayat_assesment_pasienhcu" data-backdrop="static" data-keyboard="false" onclick="viewAssestmentPasienHCU(<?php echo $list_pasien->pendaftaran_id; ?>)">
                                <span class="fa fa-eye"></span>
                                Riwayat Assestment</button>
                            <button class="btn btn-info pull-right" onclick="insertAssestment()">
                                <span class="fa fa-save"></span>
                                Simpan</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="background">
                    <div class="panel panel-default" style="margin:0">
                        <div class="panel-body">
                            <h3>Keluhan Saat Masuk</h3>
                            <div class="form-group">
                                <textarea name="keluhan" id="keluhan" rows="10" class="form-control" placeholder="Silahkan isi keluhan"><?php echo isset($keluhan_masuk) ? $keluhan_masuk : ""; ?></textarea>
                            </div>
                            <div class="row table-responsive" style="width: 100%">
                                <table id="table_list_alergi_hcu" class="table table-striped" cellspacing="0" style="width: 100%">  
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Alergi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Alergi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                </table> 
                            </div>
                            <h3>Paket Operasi</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="label-control">Alergi</label>
                                        </div>
                                        <select class="form-control row" id="bg_alergi" name="bg_alergi">
                                            <option value="">Pilih Alergi</option>
                                            <?php
                                                foreach ($list_alergi as $item) {
                                                    echo ("
                                                        <option value='$item->alergi_id'>$item->nama_alergi</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <label class="label-control">&nbsp;</label>
                                    </div>
                                    <button class="btn btn-info" onclick="insertAlergiPasien()">Tambah Alergi</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-info pull-right" onclick="saveBackground()">Simpan</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="diagnosa">
                    <div class="panel panel-default" style="margin:0">
                        <div class="panel-body">
                            <div class="row table-responsive" style="width: 100%">
                                <table id="table_list_diagnosa_hcu" class="table table-striped" cellspacing="0" style="width: 100%">  
                                    <thead>
                                        <tr>
                                            <th>Tgl. Diagnosa</th>
                                            <th>Kode Diagnosa</th>
                                            <th>Nama Diagnosa</th>
                                            <th>Kode ICD 10</th>
                                            <th>Nama ICD 10</th>
                                            <th>Jenis Diagnosa</th>
                                            <th>Nama Dokter</th>
                                            <th>Diagnosa Medis</th>
                                            <th>Botol / Hapus</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Tgl. Diagnosa</th>
                                            <th>Kode Diagnosa</th>
                                            <th>Nama Diagnosa</th>
                                            <th>Kode ICD 10</th>
                                            <th>Nama ICD 10</th>
                                            <th>Jenis Diagnosa</th>
                                            <th>Nama Dokter</th>
                                            <th>Diagnosa Medis</th>
                                            <th>Botol / Hapus</th>
                                        </tr>
                                    </tfoot>
                                </table> 
                            </div>
                            <br>
                            <hr>
                            <form id="frmDiagnosa">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <label class="label-control">Nama Dokter 1</label>
                                        </div>
                                        <div class="input-group custom-search-form">
                                            <input readonly type="text" class="form-control autocomplete" id="nama_dokter_1" name="nama_dokter_1" placeholder="Nama Dokter"> 
                                            <input type="hidden" id="nama_dokter_1_id" name="nama_dokter_1_id">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_dokter_pasienhcu" data-backdrop="static" data-keyboard="false" onclick="getDataDokter(1, 'nama_dokter_1_id', 'nama_dokter_1')"> <i class="fa fa-bars"></i> </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <label class="label-control">Nama Dokter 2</label>
                                        </div>
                                        <div class="input-group custom-search-form">
                                            <input readonly type="text" class="form-control autocomplete" id="nama_dokter_2" name="nama_dokter_2" placeholder="Nama Dokter"> 
                                            <input type="hidden" id="nama_dokter_2_id" name="nama_dokter_2_id">
                                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_dokter_pasienhcu" data-backdrop="static" data-keyboard="false" onclick="getDataDokter(1, 'nama_dokter_2_id', 'nama_dokter_2')"> <i class="fa fa-bars"></i> </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <label class="label-control">Nama Dokter 3</label>
                                        </div>
                                        <div class="input-group custom-search-form">
                                            <input readonly type="text" class="form-control autocomplete" id="nama_dokter_3" name="nama_dokter_3" placeholder="Nama Dokter"> 
                                            <input type="hidden" id="nama_dokter_3_id" name="nama_dokter_3_id">
                                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_dokter_pasienhcu" data-backdrop="static" data-keyboard="false" onclick="getDataDokter(1, 'nama_dokter_3_id', 'nama_dokter_3')"> <i class="fa fa-bars"></i> </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <hr>
                                <h3>Diagnosa</h3>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="label-control">Kode Diagnosa</label>
                                        </div>
                                        <div class="input-group custom-search-form">
                                            <input type="text" class="form-control autocomplete" id="kode_diagnosa" name="kode_diagnosa" placeholder="Cari diagnosa"> 
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_diagnosa_pasienhcu" type="button" onclick="getDataDiagnosa()"> Cari </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="label-control">Nama Diagnosa</label>
                                        </div>
                                        <input type="text" class="form-control" id="nama_diagnosa" name="nama_diagnosa" placeholder="Nama diagnosa">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" disabled class="form-control" id="map_icd_10" name="map_icd_10" placeholder="Map ICD ke Diagnosa">
                                    </div>
                                    <div class="form-group col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="label-control">Kode Diagnosa ICD 10</label>
                                        </div>
                                        <div class="input-group custom-search-form">
                                            <input type="text" class="form-control autocomplete" id="kode_diagnosa_icd_10" name="kode_diagnosa_icd_10" placeholder="Cari diagnosa"> 
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_diagnosa_icd10_pasienhcu" type="button" onclick="getDataDiagnosaICD10()"> Cari </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="label-control">Nama Diagnosa ICD 10</label>
                                        </div>
                                        <input type="text" class="form-control" id="nama_diagnosa_icd_10" name="nama_diagnosa_icd_10" placeholder="Nama diagnosa icd 10">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="label-control">Jenis Diagnosa</label>
                                        </div>
                                        <select class="form-control row" id="jenis_diagnosa" name="jenis_diagnosa">
                                            <option value="1">Jenis Diagnosa</option>
                                            <option value="1">Utama</option>
                                            <option value="2">Tambahan</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label class="label-control">Catatan</label>
                                        </div>
                                        <textarea name="catatan_diagnosa" id="catatan_diagnosa" rows="10" class="form-control" placeholder="Catatan Diagnosa Medis"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <button type="button" class="btn btn-info pull-right" onclick="saveDiagnosa()">Simpan</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tindakan_hcu">
                    <div class="panel panel-default" style="margin:0">
                        <div class="panel-body">
                            <div class="row table-responsive" style="width: 100%">
                                <table id="table_list_tindakan_hcu" class="table table-striped" cellspacing="0" style="width: 100%">  
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl. Tindakan</th>
                                            <th>Nama Tindakan</th>
                                            <th>Jumlah Tindakan</th>
                                            <th>Jumlah Jenis Obat</th>
                                            <th>Jumlah Jenis Alkes</th>
                                            <th>Dokter</th>
                                            <th>Botol / Hapus</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl. Tindakan</th>
                                            <th>Nama Tindakan</th>
                                            <th>Jumlah Tindakan</th>
                                            <th>Jumlah Jenis Obat</th>
                                            <th>Jumlah Jenis Alkes</th>
                                            <th>Dokter</th>
                                            <th>Botol / Hapus</th>
                                        </tr>
                                    </tfoot>
                                </table> 
                            </div>
                            <br>
                            <form id="frmTindakanPasienHCU">
                                <hr>
                                <h3>Tindakan Pasien</h3>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <label class="label-control">Input Tindakan</label>
                                        </div>
                                        <div class="input-group custom-search-form">
                                            <input readonly type="text" class="form-control autocomplete" id="kode_tindakan" name="kode_tindakan" placeholder="Cari tindakan"> 
                                            <input readonly type="hidden" class="form-control autocomplete" id="id_tindakan" name="id_tindakan" placeholder="Cari tindakan"> 
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_tindakan_pasienhcu" type="button" onclick="getDataTindakan()"> Cari </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <label class="label-control">Nama Tindakan</label>
                                        </div>
                                        <input type="text" class="form-control" id="nama_tindakan" name="nama_tindakan" placeholder="Nama tindakan">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <label class="label-control">Jumlah Tindakan</label>
                                        </div>
                                        <input type="number" class="form-control" id="jumlah_tindakan" name="jumlah_tindakan" placeholder="Jumlah tindakan">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" disabled class="form-control" id="map_icd_9_tindakan" name="map_icd_9_tindakan" placeholder="Map ICD 9 To Tindakan">
                                    </div>
                                    <div class="col-md-4">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-4">
                                        &nbsp;
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="row">
                                            <label class="label-control">Kode ICD 9</label>
                                        </div>
                                        <div class="input-group custom-search-form">
                                            <input readonly type="text" class="form-control autocomplete" id="kode_icd_9" name="kode_icd_9" placeholder="Kode ICD 9"> 
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_tindakan_icd9_pasienhcu" type="button" onclick="getDataTindakanICD9()"> Cari </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <label class="label-control">Nama ICD 9</label>
                                        </div>
                                        <input type="text" class="form-control" id="nama_icd_9" name="nama_icd_9" placeholder="Nama tindakan">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <label class="label-control">Jumlah ICD 9</label>
                                        </div>
                                        <input type="number" class="form-control" id="jumlah_icd_9" name="jumlah_icd_9" placeholder="Jumlah tindakan">
                                    </div>
                                </div>
                                <br>
                                <hr>
                                <div class="row table-responsive" style="width: 100%">
                                    <table id="table_list_obat_hcu" class="table table-striped" cellspacing="0" style="width: 100%">  
                                        <thead>
                                            <tr>
                                                <th>Obat</th>
                                                <th>Jumlah</th>
                                                <th>Satuan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead> 
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Obat</th>
                                                <th>Jumlah</th>
                                                <th>Satuan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </tfoot>
                                    </table> 
                                </div>
                                <br>
                                <div class="row container-fluid" style="width: 100%">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="label-control">Obat</label>
                                        </div>
                                        <select class="form-control row select2" id="tindakan_obat" name="tindakan_obat">
                                            <option value="">Pilih Obat</option>
                                            <?php
                                                foreach($list_obat as $item) {
                                                    echo ("
                                                        <option value='$item->id_barang'>$item->nama_barang</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="label-control">Jumlah</label>
                                        </div>
                                        <input type="number" class="form-control" name="tindakan_obat_jumlah" id="tindakan_obat_jumlah">
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="label-control">Satuan</label>
                                        </div>
                                        <select class="form-control row select2" id="tindakan_obat_satuan" name="tindakan_obat_satuan">
                                            <option value="">Pilih Satuan</option>
                                            <?php
                                                foreach($list_satuan_obat_alkes as $item) {
                                                    echo ("
                                                        <option value='$item->id_sediaan'>$item->nama_sediaan</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="label-control">&nbsp;</label>
                                        </div>
                                        <button type="button" class="btn btn-info" onclick="addObatTindakan()">Tambah Obat</button>
                                    </div>
                                </div>
                                <br>
                                <hr>
                                <div class="row table-responsive" style="width: 100%">
                                    <table id="table_list_alat_kesehatan_hcu" class="table table-striped" cellspacing="0" style="width: 100%">  
                                        <thead>
                                            <tr>
                                                <th>Alat Kesehatan</th>
                                                <th>Jumlah</th>
                                                <th>Satuan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead> 
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Alat Kesehatan</th>
                                                <th>Jumlah</th>
                                                <th>Satuan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </tfoot>
                                    </table> 
                                </div>
                                <br>
                                <div class="row container-fluid" style="width: 100%">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="label-control">Alat Kesehatan/BHP</label>
                                        </div>
                                        <select class="form-control row select2" id="tindakan_bhp" name="tindakan_bhp">
                                            <option value="">Pilih Alkes / BHP</option>
                                            <?php
                                                foreach($list_alkes as $item) {
                                                    echo ("
                                                        <option value='$item->id_barang'>$item->nama_barang</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="label-control">Jumlah</label>
                                        </div>
                                        <input type="number" class="form-control" name="tindakan_bhp_jumlah" id="tindakan_bhp_jumlah">
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="label-control">Satuan</label>
                                        </div>
                                        <select class="form-control row select2" id="tindakan_bhp_satuan" name="tindakan_bhp_satuan">
                                            <option value="">Pilih Satuan</option>
                                            <?php
                                                foreach($list_satuan_obat_alkes as $item) {
                                                    echo ("
                                                        <option value='$item->id_sediaan'>$item->nama_sediaan</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="label-control">&nbsp;</label>
                                        </div>
                                        <button type="button" class="btn btn-info" onclick="addBHPTindakan()">Tambah Alat Kesehatan</button>
                                    </div>
                                </div>
                                <br>
                                <hr>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="label-control">Dokter</label>
                                        </div>
                                        <select class="form-control row select2" id="tindakan_dokter" name="tindakan_dokter">
                                            <option value="">Pilih Dokter</option>
                                            <?php
                                                foreach($list_dokter as $item) {
                                                    echo ("
                                                        <option value='$item->id_dokter'>$item->nama_dokter</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <label class="label-control">User</label>
                                        </div>
                                        <select class="form-control row select2" id="tindakan_user" name="tindakan_user">
                                            <option value="">Pilih User</option>
                                            <?php
                                                foreach($list_user as $item) {
                                                    echo ("
                                                        <option value='$item->id'>$item->nama_lengkap</option>
                                                    ");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-info pull-right" onclick="saveTindakan()">Simpan</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div><div role="tabpanel" class="tab-pane" id="reseptur_obat">
                    <table id="table_resep_pasienhcu" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <th>Total Harga</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="6">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <th>Total Harga</th>
                                <th>Batal / Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateResepPasien'))?>
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delres" name="<?php echo $this->security->get_csrf_token_name()?>_delres" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="row">
                            <div class="form-group col-sm-3">
                                <label class="control-label">Nama Obat<span style="color: red;"> *</span></label>
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Nama Obat" name="nama_obat" id="nama_obat" class="autocomplete" value="">
                                    <input type="hidden" name="id_jenis_barang" id="id_jenis_barang">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_reseptur_obat_pasienhcu" onclick="getDataObat()" type="button">  <i class="fa fa-bars"></i> </button >
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Jumlah Obat<span style="color: red;"> *</span></label>
                                <input type="number" min="1" class="form-control" id="qty_obat" name="qty_obat" placeholder="Jumlah Obat">
                                <input id="obat_id" type="hidden" name="obat_id" value="" readonly>
                                <input id="current_stok" type="hidden" name="current_stok" value="" readonly>
                           </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Harga Jual<span style="color: red;"> *</span></label>
                                <input type="number" class="form-control" id="harga_jual" name="harga_jual" placeholder="Harga Jual">
                                <input id="harga_netto" type="hidden" name="harga_netto" value="">
                            </div>
                           <div class="form-group col-sm-3">
                                <label class="control-label">Satuan Obat<span style="color: red;"> *</span></label>
                                <select required class="form-control select" id="satuan_obat" name="satuan_obat">
                                    <option value="" disabled selected>Satuan Obat</option>
                                    <?php
                                        foreach($list_satuan_obat_alkes as $item) {
                                            echo ("
                                                <option value='$item->id_sediaan'>$item->nama_sediaan</option>
                                            ");
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-control">
                                <input type="checkbox" name="checkbox_racikan" id="checkbox_racikan" onchange="$('#panel_racikan_obat').toggle();">
                                <label for="checkbox_racikan">Racikan</label>
                            </div>

                            <div class="panel panel-info" id="panel_racikan_obat" style="display: none">
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <table class="table table-striped" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Obat</th>
                                                    <th>Jumlah</th>
                                                    <th>Sediaan</th>
                                                    <th>Hapus</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table_racikan_pasienrd">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Obat</th>
                                                    <th>Jumlah</th>
                                                    <th>Sediaan</th>
                                                    <th>Hapus</th>
                                                </tr>
                                            </tfoot>
                                        </table>

                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Obat<span style="color: red;"> *</span></label>
                                                <select required class="form-control select2" id="obat_racikan" name="obat_racikan">
                                                    <option value="" disabled selected>Obat</option>
                                                    <?php
                                                        foreach($list_obat as $item) {
                                                            echo ("
                                                                <option value='$item->id_barang'>$item->nama_barang</option>
                                                            ");
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="control-label">Jumlah<span style="color: red;"> *</span></label>
                                                <input type="number" class="form-control" id="jumlah_racikan" name="jumlah_racikan" placeholder="Jumlah" value="0">
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <label class="control-label">Sediaan<span style="color: red;"> *</span></label>
                                                <select required class="form-control select" id="sediaan_racikan" name="sediaan_racikan">
                                                    <option value="" disabled selected>Sediaan</option>
                                                    <?php
                                                        foreach($list_satuan_obat_alkes as $item) {
                                                            echo ("
                                                                <option value='$item->id_sediaan'>$item->nama_sediaan</option>
                                                            ");
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="control-label">Keterangan</label>
                                                <textarea class="form-control" placeholder="Keterangan" id="keterangan_racikan" name="keterangan_racikan"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button style="background-color: #01c0c8;" type="button" class="btn btn-success pull-right" onclick="addRacikanObat()"><i class="fa fa-floppy-o"></i> TAMBAH OBAT RACIKAN</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group col-sm-6" style="margin-top: 20px">
                                <label class="control-label">Signa<span style="color: red;"> *</span></label>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <input type="number" class="form-control" id="signa_1" name="signa_1" value="0">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <input type="number" class="form-control" id="signa_2" name="signa_2" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-sm-2">
                            <button style="width: 150px; background-color: #01c0c8;" type="button" class="btn btn-success" onclick="saveResepturObat()"><i class="fa fa-floppy-o"></i> TAMBAH</button>
                        </div>
                        <?php echo form_close();?>
                    <div class="clearfix"></div>
                </div>
            </div>  
        </div>
    </div>
</div>
<!-- /.modal -->
<div id="modal_riwayat_assesment_pasienhcu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Riwayat Assesment Pasien</b></h2>
            </div>  

            <div class="modal-body" style="background: #fafafa">        
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>    
                </div>  
            </div>

        </div>
    </div>
</div>

<div id="modal_dokter_pasienhcu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Dokter</b></h2>
            </div>  

            <div class="modal-body" style="background: #fafafa">     
                <input type="hidden" id="position_dokter">
                <input type="hidden" id="targetID_dokter">
                <input type="hidden" id="targetNama_dokter">
                <div class="row"> 
                    <div class="container-fluid" style="width: 100%">
                        <table class="table table-striped table-responsive" cellspacing="0">  
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokter</th>
                                    <th>Kelompok Dokter</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td colspan="4">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokter</th>
                                    <th>Kelompok Dokter</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="modal_diagnosa_pasienhcu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Tindakan</b></h2>
            </div>  

            <div class="modal-body" style="background: #fafafa">
                <div class="row"> 
                    <div class="container-fluid" style="width: 100%">
                        <table class="table table-striped table-responsive" cellspacing="0">  
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td colspan="3">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="modal_diagnosa_icd10_pasienhcu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Tindakan</b></h2>
            </div>  

            <div class="modal-body" style="background: #fafafa">
                <div class="row"> 
                    <div class="container-fluid" style="width: 100%">
                        <table class="table table-striped table-responsive" cellspacing="0">  
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Diagnosa</th>
                                    <th>Nama Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td colspan="4">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Diagnosa</th>
                                    <th>Nama Diagnosa</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="modal_tindakan_pasienhcu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Tindakan</b></h2>
            </div>  

            <div class="modal-body" style="background: #fafafa">
                <div class="row"> 
                    <div class="container-fluid" style="width: 100%">
                        <table class="table table-striped table-responsive" cellspacing="0">  
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td colspan="3">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="modal_tindakan_icd9_pasienhcu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Tindakan</b></h2>
            </div>  

            <div class="modal-body" style="background: #fafafa">
                <div class="row"> 
                    <div class="container-fluid" style="width: 100%">
                        <table class="table table-striped table-responsive" cellspacing="0">  
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Diagnosa</th>
                                    <th>Nama Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td colspan="4">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Diagnosa</th>
                                    <th>Nama Diagnosa</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="modal_reseptur_obat_pasienhcu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Obat</b></h2>
            </div>  

            <div class="modal-body" style="background: #fafafa">
                <div class="row"> 
                    <div class="container-fluid" style="width: 100%">
                        <table class="table table-striped table-responsive" cellspacing="0">  
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td colspan="3">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tindakan</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php $this->load->view('footer_iframe');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/hcu/periksa_pasien_hcu.js"></script>
</body>
 
</html>