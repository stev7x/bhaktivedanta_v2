<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Print Resep</title>
	<link rel="stylesheet" href="">
    <style>
        .barang{
            align:left;
        }

        .signa{
            align: center;
        }
    </style>
</head>
<body onload="window.print()">
    <table border="1px" width="100%" id="table_print_resep_pasienrd" class="table table-striped dataTable" cellspacing="0">
        <thead>  
            <tr>
                <th>Nama Obat</th>
                <th>Satuan</th>
                <th>Signa</th>
                <th>Jumlah</th>
            </tr> 
        </thead>
        <tbody>
            <?php
                foreach($resep_list as $list){?>
                <tr>
                    <td id="barang"><?php echo ($list->nama_barang); ?></td>
                    <td align="center" id=""><?php echo ($list->kode_sediaan); ?></td>
                    <td align="center" id="signa"><?php echo ($list->signa); ?></td>
                    <td align="center" id="qty"><?php echo ($list->qty); ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table><br>
</body>
</html>