<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_hcu_model extends CI_Model {
    var $column = array('no_pendaftaran','tgl_pendaftaran','poliruangan_id','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa');
    var $order = array('tgl_pendaftaran' => 'ASC');

    var $column1 = array('kode_barang');
    var $order1 = array('nama_barang' => 'ASC');

    public function __construct(){
        parent::__construct();
    }

    function count_all($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran, $dokter_id){
        $this->db->from('v_pasien_hcu');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poliruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }
            
        return $this->db->count_all_results();
    }

    // Get Data Pendaftaran Pasien
    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_pasien_hcu', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();   
    }

    // Get Data M Lookup By Type M Lookup
    public function get_mlookup($type){
        $this->db->select("*");
        $this->db->from("m_lookup");
        $this->db->where("type", $type);
        return $this->db->get()->result();
    }

    // Get Program Therapy
    public function get_program_therapy() {
        $this->db->select("*");
        $this->db->from("m_program_therapy");
        return $this->db->get()->result();
    }

    // Get All Pasien List
    public function get_pasienhcu_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran, $dokter_id){
        $this->db->from('v_pasien_hcu');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poliruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    // Insert Assestment Pasien
    public function insert_assestment_pasienhcu($data=array()) {
        $status = $this->db->insert("t_assestment_pasien", $data);
        if ($status) {
            $this->db->select("assestment_pasien_id");
            $this->db->from("t_assestment_pasien");
            $this->db->order_by("assestment_pasien_id", "desc");
            $this->db->limit(1);
            return $this->db->get()->row()->assestment_pasien_id;
        }
        return 0;
    }

    // Insert Program Therapy For Assestment Pasien
    public function insert_program_therapy_assestment($data=array()) {
        return $this->db->insert("t_assestment_therapy_pasien", $data);
    }

    // Set Status Periksa Pasien
    public function set_status_periksa_pasien($pendaftaran_id, $status) {
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        return $this->db->update("t_pendaftaran", array(
            "status_periksa" => $status
        ));
    }

    // Get Riwayat Assetsment Pasien
    public function get_riwayat_assestment_pasien($pendaftaran_id) {
        $this->db->select("b.assestment_pasien_id, b.instalasi_id, b.pendaftaran_id, b.pasien_id, b.tensi, b.nadi_1, b.suhu, b.nadi_2, b.penggunaan, b.saturasi, b.nyeri, b.numeric_wong_baker, b.resiko_jatuh, b.respitory");
        $this->db->from("t_assestment_pasien b");
        $this->db->where("b.pendaftaran_id", $pendaftaran_id);

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Get Description For Riwayat Assestment Pasien
    public function get_therapy_riwayat_assestment_pasien($assestment_pasien_id) {
        $this->db->select("a.assestment_therapy_id, a.program_therapy_id, b.kode, b.nama");
        $this->db->from("t_assestment_therapy_pasien a");
        $this->db->join("m_program_therapy b", "a.program_therapy_id=b.program_therapy_id", "left");
        $this->db->where("a.assestment_pasien_id", $assestment_pasien_id);

        return $this->db->get()->result();
    }

    // Remove Assestment Pasien And Description From It
    public function remove_riwayat_assestment_pasien($assestment_pasien_id) {
        $this->db->where("assestment_pasien_id", $assestment_pasien_id);
        $this->db->delete("t_assestment_therapy_pasien");

        $this->db->where("assestment_pasien_id", $assestment_pasien_id);
        return $this->db->delete("t_assestment_pasien");
    }

    // Remove Tindakan Pasien And Description From It
    public function remove_tindakan_pasien($pasien_tindakan_id) {
        $this->db->where("tindakan_pasien_id", $pasien_tindakan_id);
        $this->db->delete("t_tindakan_pasien_obat_detail");

        $this->db->where("tindakan_pasien_id", $pasien_tindakan_id);
        $this->db->delete("t_tindakan_pasien_bhp_detail");

        $this->db->where("tindakanpasien_id", $pasien_tindakan_id);
        return $this->db->delete("t_tindakanpasien");
    }

    // Get Keluhan Masuk
    public function get_keluhan_masuk($pendaftaran_id) {
        $this->db->select("catatan_keluhan");
        $this->db->from("t_pendaftaran");
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        return $this->db->get()->row()->catatan_keluhan;
    }

    // Get Alergi
    public function get_list_alergi() {
        return $this->db->get("m_alergi")->result();
    }

    // Get Alergi pasien
    public function get_alergi_pasien($pendaftaran_id) {
        $this->db->select("a.alergi_pasien_id, a.alergi_id, a.tanggal, b.nama_alergi, b.kode_alergi");
        $this->db->from("t_alergi_pasien a");
        $this->db->join("m_alergi b", "a.alergi_id=b.alergi_id", "left");
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        return $this->db->get()->result();
    }

    // Insert Alergi Pasien
    public function insert_alergi_pasien($data=array()) {
        return $this->db->insert("t_alergi_pasien", $data);
    }

    // Remove Alergi Pasie
    public function remove_alergi_pasien($alergi_pasien_id) {
        $this->db->where("alergi_pasien_id", $alergi_pasien_id);
        return $this->db->delete("t_alergi_pasien");
    }

    // Update Background
    public function update_background($keluhan_masuk, $pendaftaran_id) {
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        return $this->db->update("t_pendaftaran", array(
            "catatan_keluhan" => $keluhan_masuk
        ));
    }

    // Get Diagnosa Pasien
    public function get_list_diagnosa_pasien($pendaftaran_id) {
        $this->db->select("a.diagnosapasien_id, a.diagnosa_id, a.tgl_diagnosa, a.jenis_diagnosa, a.diagnosa_by, a.kode_diagnosa, a.nama_diagnosa, a.kode_icd10, a.nama_icd10, a.catatan, b.NAME_DOKTER as dokter_1, c.NAME_DOKTER as dokter_2, d.NAME_DOKTER as dokter_3");
        $this->db->from("t_diagnosapasien a");
        $this->db->join("m_dokter b", "a.dokter_id = b.id_M_DOKTER", "left");
        $this->db->join("m_dokter c", "a.dokter_id = c.id_M_DOKTER", "left");
        $this->db->join("m_dokter d", "a.dokter_id = d.id_M_DOKTER", "left");
        $this->db->where("pendaftaran_id", $pendaftaran_id);

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Get Diagnosa Pasien
    public function get_list_reseptur_pasien($pendaftaran_id) {
        $this->db->select("a.reseptur_id, a.kode_barang, a.nama_barang, a.qty, a.harga_jual, a.total_harga, a.tgl_reseptur");
        $this->db->from("t_resepturpasien a");
        $this->db->where("a.pendaftaran_id", $pendaftaran_id);

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Get Dokter
    public function get_list_dokter() {
        $this->db->select("a.id_M_DOKTER as id_dokter, a.NAME_DOKTER as nama_dokter, a.ALAMAT as alamat, b.kelompokdokter_nama as kelompok_dokter");
        $this->db->from("m_dokter a");
        $this->db->join("m_kelompokdokter b", "a.kelompokdokter_id=b.kelompokdokter_id", "left");

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Get Dokter
    public function get_list_dokter_all() {
        $this->db->select("a.id_M_DOKTER as id_dokter, a.NAME_DOKTER as nama_dokter");
        $this->db->from("m_dokter a");
        return $this->db->get()->result();
    }

    // Get User
    public function get_list_user_all() {
        $this->db->select("id, nama_lengkap");
        $this->db->from("aauth_users");
        return $this->db->get()->result();
    }

    // Get Obat
    public function get_list_obat_all() {
        $this->db->select("id_barang, nama_barang");
        $this->db->from("m_barang_farmasi");
        $this->db->where("id_jenis_barang", 1);
        return $this->db->get()->result();
    }

    // Get Obat
    public function get_list_obat_all2() {
        $this->db->select("id_barang, nama_barang");
        $this->db->from("m_barang_farmasi");
        $this->db->where("id_jenis_barang", 1);

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Get Alkes / BHP
    public function get_list_alkes_all() {
        $this->db->select("id_barang, nama_barang");
        $this->db->from("m_barang_farmasi");
        $this->db->where("id_jenis_barang", 2);
        return $this->db->get()->result();
    }

    // Get Sediaan Alkes / BHP
    public function get_list_satuan_obat_alkes_all() {
        $this->db->select("id_sediaan, nama_sediaan");
        $this->db->from("m_sediaan_obat");
        return $this->db->get()->result();
    }

    // Get Diagnosa
    public function get_tindakan() {
        $this->db->select("a.daftartindakan_id, a.daftartindakan_nama, a.icd9_cm_id as icd");
        $this->db->from("m_tindakan a");

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Get Diagnosa ICD 9
    public function get_diagnosa_icd_9($diagnosa_id) {
        $this->db->select("*");
        $this->db->from("m_icd_9_cm");
        $this->db->where("diagnosa_id", $diagnosa_id);
        return $this->db->get()->row();
    }

    // Get Diagnosa ICD 10
    public function get_diagnosa_icd_10() {
        $this->db->select("a.kode_diagnosa, a.nama_diagnosa");
        $this->db->from("m_diagnosa_icd10 a");

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Get Diagnosa ICD 9
    public function get_tindakan_icd_9() {
        $this->db->select("a.kode_diagnosa, a.nama_diagnosa");
        $this->db->from("m_icd_9_cm a");

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Insert Diagnosa
    public function insert_diagnosa_pasienhcu($data=array()) {
        return $this->db->insert("t_diagnosapasien", $data);
    }

    // Insert Tindakan
    public function insert_tindakan_pasienhcu($data=array()) {
        $status = $this->db->insert("t_tindakanpasien", $data);
        if ($status) {
            $this->db->select("tindakanpasien_id");
            $this->db->from("t_tindakanpasien");
            $this->db->order_by("tindakanpasien_id", "desc");
            $this->db->limit(1);
            return $this->db->get()->row()->tindakanpasien_id;
        }
        return 0;
    }

    // Insert Tindakan Obat Detail
    public function insert_tindakan_obat_pasienhcu($data=array()) {
        return $this->db->insert("t_tindakan_pasien_obat_detail", $data);
    }

    // Insert Tindakan BHP Detail
    public function insert_tindakan_bhp_pasienhcu($data=array()) {
        return $this->db->insert("t_tindakan_pasien_bhp_detail", $data);
    }

    // Insert Reseptur Obat
    public function insert_reseptur_obat_pasienhcu($data=array()) {
        $status = $this->db->insert("t_resepturpasien", $data);
        if ($status) {
            $this->db->select("reseptur_id");
            $this->db->from("t_resepturpasien");
            $this->db->order_by("reseptur_id", "desc");
            $this->db->limit(1);
            return $this->db->get()->row()->reseptur_id;
        }
        return 0;
    }

    // Insert Reseptur Obat Detail
    public function insert_reseptur_obat_detail_pasienhcu($data=array()) {
        return $this->db->insert("t_resepturpasien_detail_racikan", $data);
    }

    // Get Tindakan Pasien 
    public function get_tindakan_pasien($pendaftaran_id) {
        $this->db->select("a.tindakanpasien_id, b.daftartindakan_nama as nama_tindakan, a.jml_tindakan, a.tgl_tindakan, c.NAME_DOKTER as nama_dokter");
        $this->db->from("t_tindakanpasien a");
        $this->db->join("m_tindakan b", "a.daftartindakan_id=b.daftartindakan_id", "left");
        $this->db->join("m_dokter c", "a.dokter_id=c.id_M_DOKTER", "left");
        $this->db->where("a.pendaftaran_id", $pendaftaran_id);

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        return $this->db->get()->result();
    }

    // Get Jumlah Jenis Obat
    public function get_jumlah_jenis_obat($tindakanpasien_id) {
        $this->db->select("sum(d.jumlah) as jml_obat");
        $this->db->from("t_tindakan_pasien_obat_detail d");
        $this->db->where("d.tindakan_pasien_id", $tindakanpasien_id);
        return $this->db->get()->row()->jml_obat;
    }

    // Get Jumlah Jenis BHP
    public function get_jumlah_jenis_bhp($tindakanpasien_id) {
        $this->db->select("sum(e.jumlah) as jml_bhp");
        $this->db->from("t_tindakan_pasien_obat_detail e");
        $this->db->where("e.tindakan_pasien_id", $tindakanpasien_id);
        return $this->db->get()->row()->jml_bhp;
    }
}

?>