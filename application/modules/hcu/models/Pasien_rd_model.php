<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_rd_model extends CI_Model {
    var $column = array('no_pendaftaran','tgl_pendaftaran','poliruangan_id','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa');
    var $order = array('tgl_pendaftaran' => 'ASC');

//    var $column1 = array('obat_id','nama_obat', 'satuan', 'jenisoa_nama','harga_jual','qtystok');
  //  var $order1 = array('nama_obat' => 'ASC');

     var $column1 = array('kode_barang');
    var $order1 = array('nama_barang' => 'ASC');

    public function __construct(){
        parent::__construct();
    }
    
    public function delete_periksa($pendaftaran_id){
      $datas = array(
          'status_periksa' => 'Batal Periksa',
          'status_pasien' => 0
      );
      $update = $this->db->update('t_pendaftaran', $datas, array('pendaftaran_id' => $pendaftaran_id));
      return $update;
    }
    
    
    public function get_pasienrd_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran, $dokter_id){
        $this->db->from('v_pasien_rd');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poliruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    function count_all($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran, $dokter_id){
        $this->db->from('v_pasien_rd');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poliruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }
            
        return $this->db->count_all_results();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_pasien_rd', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();   
    }
    
    public function get_dokter_list(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('instalasi_id', 2); //IGD
        $this->db->group_by('id_M_DOKTER');
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_diagnosa_pasien_list($pendaftaran_id){
        $this->db->from('t_diagnosapasien');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tindakan_pasien_list($pendaftaran_id){
       $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama'); 
        $this->db->from('t_tindakanpasien'); 
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);   
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }


    public function get_tindakan_pasien_list_permintaan($pendaftaran_id){
       $this->db->select('t_det_reservasi_ri.*, m_tindakan.daftartindakan_nama'); 
        $this->db->from('t_det_reservasi_ri'); 
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_det_reservasi_ri.tindakan_id');
        $this->db->where('t_det_reservasi_ri.pendaftaran_id',$pendaftaran_id);   
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

      public function get_permintaan_tindakan($poliruangan_id){
        $this->db->select('m_tindakan.*');
        $this->db->from('m_tindakan');
        $this->db->join('m_kelaspelayanan', 'm_kelaspelayanan.kelaspelayanan_id = m_tindakan.kelaspelayanan_id');
        $this->db->join('m_kelaspoliruangan', 'm_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id',$poliruangan_id);        
        
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tindakan($kelaspelayanan_id){
        $this->db->from('m_tindakan');  
        $this->db->where('kelaspelayanan_id',$kelaspelayanan_id);    
        $this->db->where('kelompoktindakan_id',3);    
        
        $query = $this->db->get();

        return $query->result();
    }
    
    
    public function get_tarif_tindakan($daftartindakan_id){
        $query = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $daftartindakan_id), 1, 0);

        return $query->row();
    }
    
    public function insert_diagnosapasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_diagnosapasien',$data);

        return $insert;
    }
    
    public function update_to_sudahperiksa($id){
        $data = array(
            'status_periksa' => 'Sudah Periksa'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    
    public function insert_tindakanpasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_tindakanpasien',$data);

        return $insert;
    }

        public function insert_ajukan_permintaan($data=array()){
        $insert = $this->db->insert('t_reservasi_ri',$data);
        if ($insert) {
            $this->db->select("reservasi_ri_id");
            $this->db->from("t_reservasi_ri");
            $this->db->order_by("reservasi_ri_id", "desc");
            $this->db->limit(1);
            $data = $this->db->get()->row();
            return $data->reservasi_ri_id;
        }
        return 0;
    }

    
    public function insert_tindakanpasienpermintaan($data=array()){
       // $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_det_reservasi_ri', $data);

        return $insert;
    }
    
    public function delete_diagnosa($id){
        $delete = $this->db->delete('t_diagnosapasien', array('diagnosapasien_id' => $id));

        return $delete;
    }
    
    public function delete_tindakan($id){
        $delete = $this->db->delete('t_tindakanpasien', array('tindakanpasien_id' => $id));

        return $delete;
    }

    public function get_sediaan_list(){
        return $this->db->get_where('m_sediaan_obat')->result();
    }


    //insert to apotek keluar
    public function insert_resep_to_apotek($data=array()){
        $this->db->insert('t_apotek_keluar',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    
     public function get_resep_pasien_list($pendaftaran_id){
        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_resepturpasien.id_sediaan','left');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_resepturpasien.id_jenis_barang','left');
        $this->db->from('t_resepturpasien');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('v_stokobatalkes');
        $this->db->like('nama_obat', $val);
        $this->db->where('qtystok !=', 0);
        $query = $this->db->get();

        return $query->result();
    }
    
    public function insert_reseppasien($data=array()){
        $this->db->insert('t_resepturpasien',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function insert_reseppasien_racikan($data=array()) {
        $this->db->insert("t_resepturpasien_detail_racikan", $data);
    }
    
    public function insert_stokobatalkes_keluar($data=array()){
        $stok = $this->db->insert('t_stokobat',$data);
        
        return $stok;
    }
    
    public function delete_resep($id){
        $delete = $this->db->delete('t_resepturpasien', array('reseptur_id' => $id));

        return $delete;
    }
    
    public function delete_stok_resep($id){
        $stok = $this->db->delete('t_stokobat', array('penjualandetail_id' => $id));
        
        return $stok;
    }
    
    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_INAP));
        
        return $query->result();
    }
    
    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();
        
        return $query->result();
    }
     
    public function get_kamar_aktif($ruangan_id, $kelasruangan){
        $this->db->order_by('no_kamar', 'ASC');
        $query = $this->db->get_where('m_kamarruangan', array('poliruangan_id' => $ruangan_id, 'kelaspelayanan_id' => $kelasruangan, 'status_aktif' => '1'));
        
        return $query->result();
    }


    
    public function insert_admisi($data){
        $this->db->insert('t_pasienadmisi',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }
    
    public function insert_kamar($data){
        $insert = $this->db->insert('t_masukkamar',$data);
        return $insert;
    }
    
    public function update_kamar($data, $id){
        $update = $this->db->update('m_kamarruangan', $data, array('kamarruangan_id' => $id));

        return $update;
    }
    
    public function update_pendaftaran($data, $id){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    public function update_to_penunjang($id){
        $data = array(
            'status_periksa' => 'SEDANG DI PENUNJANG'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }
    public function insert_penunjang($data){
        $insert = $this->db->insert('t_pasienmasukpenunjang',$data);
        return $insert;
    }

    public function insert_permintaan_rawat($data){
        $insert = $this->db->insert('t_reservasi_ri',$data);
        return $insert;
    }

   public function update_to_permintaan_rawat($id){
        $data = array(
            'status_periksa' => 'SEDANG PERMOHONAN RAWAT INAP'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

    public function get_list_penunjang(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => 4));

        return $query->result();
    }
    
    function get_paketoperasi(){
        $this->db->from('m_paketoperasi');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function get_dokter_anastesi(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_dokter_umum(){
        $this->db->from('m_dokter');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }


     public function get_rencana_tindakan(){
        $this->db->from('m_rencana_tindakan');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }
    

    public function get_kamar_semua(){

        $this->db->from('m_poli_ruangan');
        // $this->db->select("a.*, b.*");
        // $this->db->from('m_kamarruangan a');
        // $this->db->join("m_poli_ruangan b", "a.poliruangan_id = b.poliruangan_id");
        // //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
         $query = $this->db->get();

        return $query->result();
    }

    public function get_perawat(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_PERAWAT); //Perawat
        $query = $this->db->get();

        return $query->result();
    }
    
    function get_no_pendaftaran_ri(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RI".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran 
                        WHERE no_pendaftaran LIKE ('RI".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        
        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_pendaftaran;
    }
    
    function get_kelas_paket_operasi($paketoperasi){
        $query = $this->db->get_where('m_paketoperasi', array('paketoperasi_id' => $paketoperasi), 1, 0);
        $listpaket = $query->result();
      
        $kelaspelayanan = $listpaket[0]->kelaspelayanan_id;
        
        return $kelaspelayanan;
    }
    
    public function insert_pendaftaran($data){
        $this->db->insert('t_pendaftaran',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }
    
    public function insert_tindakanpasien_po($data=array()){
        $insert = $this->db->insert('t_tindakanpasien',$data);
        return $insert;
    }
    
    public function get_tindakan_paket($paketoperasi_id){
        $query = $this->db->get_where('m_paketoperasidetail', array('paketoperasi_id' => $paketoperasi_id));
        
        return $query->result();
    }
    
    public function insert_tindakan_paket($paketoperasi_id, $pendaftaran_id, $pasien_id, $dokter_id, $dokteranastesi_id, $perawat_id){
        $tindakan = $this->get_tindakan_paket($paketoperasi_id);
        $statusinsert = false;
        
        foreach($tindakan as $list){
            $tariftindakan = $this->get_tarif_tindakan($list->tariftindakan_id);
            $hargatindakan = intval($tariftindakan->harga_tindakan) - intval($list->subsidi);
            $total_harga = intval($hargatindakan) * intval($list->jumlah);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'tariftindakan_id' => $list->tariftindakan_id,
                'jml_tindakan' => $list->jumlah,
                'total_harga_tindakan' => $total_harga,
                'is_cyto' => '0',
                'total_harga' => $total_harga,
                'dokter_id' => $dokter_id,
                'tgl_tindakan' => date('Y-m-d'),
                'dokteranastesi_id' => $dokteranastesi_id,
                'perawat_id' => $perawat_id,
                'petugas_id' =>$this->data['users']->id,
                'is_paket' => '1'
            );
            $inserttindakan = $this->insert_tindakanpasien_po($data_tindakanpasien);
            
            if($inserttindakan){
                $statusinsert = true;
            }
        }
        
        return $statusinsert;
    }
    
    public function get_obat_paket($paketoperasi_id){
        $query = $this->db->get_where('m_paketoperasiobat', array('paketoperasi_id' => $paketoperasi_id));
        
        return $query->result();
    }
    
    public function get_obat_stok($id){
        $q = $this->db->get_where('v_stokobatalkes', array('obat_id' => $id));
        return $q->row();
    }
    
    public function insert_obat_paket($paketoperasi_id, $pendaftaran_id, $pasien_id, $dokter_id){
        $obat = $this->get_obat_paket($paketoperasi_id);
        $statusinsert = false;
        
        foreach($obat as $list){
            $obatstok = $this->get_obat_stok($list->obat_id);
            if(count($obatstok) > 0){
                $data_reseppasien = array(
                    'obat_id' => $list->obat_id,
                    'qty' => $list->jumlah_obat,
                    'harga_netto' => $obatstok->harga_modal,
                    'harga_jual' => $obatstok->harga_jual - $list->subsidi_obat,
                    'satuan' => $obatstok->satuan,
                    'signa' => null,
                    'pendaftaran_id' => $pendaftaran_id,
                    'pasien_id' => $pasien_id,
                    'dokter_id' => $dokter_id,
                    'tgl_reseptur' => date('Y-m-d'),
                );
                $ins = $this->insert_reseppasien($data_reseppasien);
                if($ins){
                    $updatestok = array(
                        'penjualandetail_id' => $ins,
                        'obat_id' => $list->obat_id,
                        'tglstok_out' => date('Y-m-d'),
                        'qtystok_out' => $list->jumlah_obat,
                        'create_time' => date('Y-m-d H:i:s'),
                        'create_user' => $this->data['users']->id
                    );

                    $inst_update = $this->insert_stokobatalkes_keluar($updatestok);

                    if($inst_update){
                        $statusinsert = true;
                    }
                }
            }else{
                $statusinsert = true;
            }
        }
        
        return $statusinsert;
    }
	
	function get_list_obat(){
		// $this->db->select('*');
        //$this->db->from('v_stokobatalkes');
        // $this->db->where('qtystok !=', 0);

        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_apotek_stok.id_sediaan');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_apotek_stok.id_jenis_barang');
        $this->db->from('t_apotek_stok');
		
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
	}
	
		function get_obat_by_id($id){
            $this->db->select('*');
            $this->db->from('t_apotek_stok');
            $this->db->where('kode_barang =', $id);
            $query = $this->db->get();

            return $query->result();
    }
    
    
    public function count_list_obat(){
        $this->db->from('t_apotek_stok');
        // $this->db->from('v_stokobatalkes');
		// $this->db->where('qtystok !=', 0);
        return $this->db->count_all_results();
    }

	function count_list_filtered_obat(){
        $this->db->from('t_apotek_stok');
        // $this->db->from('v_stokobatalkes');
		// $this->db->where('qtystok !=', 0);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
	}
	
	public function get_barang_stok_list(){
        $this->db->select("*");
        $this->db->from("m_barang_farmasi");
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $query = $this->db->get();
        return $query->result();
    }
}
