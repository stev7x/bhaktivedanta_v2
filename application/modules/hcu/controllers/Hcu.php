<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hcu extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pasien_hcu_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    // Index Page
    public function index(){
        $is_permit = $this->aauth->control_no_redirect('hcu_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "hcu_view";
        $comments = "List Pasien High Care Unit";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_pasien', $this->data);
    }

    // Get Data Pasien HCU (All)
    public function ajax_list_pasienhcu() {
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $poliklinik = $this->input->get('poliklinik',TRUE);
        $status = $this->input->get('status',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $list = $this->Pasien_hcu_model->get_pasienhcu_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran, $dokter_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $tgl_lahir = $pasienrd->tanggal_lahir;
            $umur  = new Datetime($tgl_lahir);
            $today = new Datetime();
            $diff  = $today->diff($umur);

            $no++;
            $row = array();
            $row[] = $pasienrd->no_pendaftaran;
            $row[] = $pasienrd->tgl_pendaftaran;
            $row[] = '<span data-toggle="modal" data-target="#modal_riwayat_penyakit_pasien" data-backdrop="static" data-keyboard="false" title="klik untuk Riwayat Penyaakit Pasien"  onclick="riwayatPenyakitPasien('."'".$pasienrd->pasien_id."'".')">'.$pasienrd->no_rekam_medis.'</span>';
            $row[] = $pasienrd->no_bpjs;
            $row[] = $pasienrd->pasien_nama;
            $row[] = $pasienrd->jenis_kelamin;
            $row[] = $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
            $row[] = $pasienrd->pasien_alamat;
            $row[] = $pasienrd->type_pembayaran;
            $row[] = $pasienrd->kelaspelayanan_nama;
            $row[] = $pasienrd->status_periksa;
            if(trim(strtoupper($pasienrd->status_periksa)) == "DIPULANGKAN" || trim(strtoupper($pasienrd->status_periksa)) == "SUDAH PULANG" || trim(strtoupper($pasienrd->status_periksa)) == "BATAL RAWAT" || trim(strtoupper($pasienrd->status_periksa)) == "BATAL PERIKSA" || trim(strtoupper($pasienrd->status_periksa)) == "SEDANG RAWAT INAP" || trim(strtoupper($pasienrd->status_periksa)) == "SEDANG DI PENUNJANG" || trim(strtoupper($pasienrd->status_periksa)) == "SEDANG DI OPERASI"){
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRd('."'".$pasienrd->pendaftaran_id."'".')" disabled>PERIKSA</button>';  
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')" disabled>Kirim ke OK</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')" disabled>Kirim ke VK</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')" disabled>Rujuk RS</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')" disabled>Penunjang</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')" disabled>Kirim ke Isolasi</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')" disabled>Kirim ke Pathologi Anak</button>';
                $row[] = '<button type="button" id="batalPeriksa" class="btn btn-default btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasienrd->pendaftaran_id."'".')" disabled><i class="fa fa-times"></i></button>';
            }else if (trim(strtoupper($pasienrd->status_periksa)) == "SEDANG PERMOHONAN RAWAT INAP"){
                $row[] = '<button type="button" data-toggle="modal" data-target="#modal_periksa_pasienhcu" data-backdrop="static" data-keyboard="false" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienHCU('."'".$pasienrd->pendaftaran_id."'".')">PERIKSA</button>';  
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Kirim ke OK</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Kirim ke VK</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Rujuk RS</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Penunjang</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Kirim ke Isolasi</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Kirim ke Pathologi Anak</button>';
                $row[] = '<button type="button" id="batalPeriksa" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasienrd->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>';
            }else{   
                $row[] = '<button type="button" data-toggle="modal" data-target="#modal_periksa_pasienhcu" data-backdrop="static" data-keyboard="false" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienHCU('."'".$pasienrd->pendaftaran_id."'".')">PERIKSA</button>';  
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Kirim ke OK</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Kirim ke VK</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Rujuk RS</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Penunjang</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Kirim ke Isolasi</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">Kirim ke Pathologi Anak</button>';
                $row[] = '<button type="button" id="batalPeriksa" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasienrd->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>';
            }

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_hcu_model->count_all($tgl_awal, $tgl_akhir, $poliklinik, $status,  $no_rekam_medis, $no_bpjs,  $pasien_alamat, $nama_pasien, $no_pendaftaran,$dokter_id),
                    "recordsFiltered" => $this->Pasien_hcu_model->count_all($tgl_awal, $tgl_akhir, $poliklinik, $status,  $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran,$dokter_id),
                    "data" => $data
                    );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Assestment Pasien HCU (BY PENDAFTARAN ID)
    public function ajax_list_assestment_pasienhcu() {
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_hcu_model->get_riwayat_assestment_pasien($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $getProgramTherapy = $this->Pasien_hcu_model->get_therapy_riwayat_assestment_pasien($pasienrd->assestment_pasien_id);
            $program_therapy = "";
            foreach($getProgramTherapy as $itemProgramTherapy) {
                $program_therapy .= $itemProgramTherapy->nama . ", ";
            }

            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->tensi;
            $row[] = $pasienrd->nadi_1;
            $row[] = $pasienrd->suhu;
            $row[] = $pasienrd->nadi_2;
            $row[] = $pasienrd->penggunaan;
            $row[] = $pasienrd->saturasi;
            $row[] = $pasienrd->nyeri;
            $row[] = $pasienrd->numeric_wong_baker;
            $row[] = $pasienrd->resiko_jatuh;
            $row[] = $program_therapy;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus assestment pasien" onclick="removeAssestment('."'".$pasienrd->assestment_pasien_id."'".')"><i class="fa fa-times"></i></button>';

            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Alergi Pasien HCU (BY PENDAFTARAN ID)
    public function ajax_list_alergi_pasienhcu() {
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_hcu_model->get_alergi_pasien($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->nama_alergi;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus alergi pasien" onclick="removeAlergi('."'".$pasienrd->alergi_pasien_id."'".')"><i class="fa fa-times"></i></button>';

            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Diagnosa Pasien HCU (BY PENDAFTARAN ID)
    public function ajax_list_diagnosa_pasienhcu() {
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_hcu_model->get_list_diagnosa_pasien($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->tgl_diagnosa;
            $row[] = $pasienrd->kode_diagnosa;
            $row[] = $pasienrd->nama_diagnosa;
            $row[] = $pasienrd->kode_icd10;
            $row[] = $pasienrd->nama_icd10;
            $row[] = $pasienrd->jenis_diagnosa;
            $row[] = "" . isset($pasienrd->dokter_1) ? "$pasienrd->dokter_1" : "".
                    ", " . isset($pasienrd->dokter_2) ? "$pasienrd->dokter_2" : "".
                    ", " . isset($pasienrd->dokter_3) ? "$pasienrd->dokter_3" : "";
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus alergi pasien" onclick="removeAlergi('."'".$pasienrd->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>';

            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Dokter (ALL)
    public function ajax_list_dokter() {
        $list = $this->Pasien_hcu_model->get_list_dokter();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->nama_dokter;
            $row[] = $pasienrd->kelompok_dokter;
            $row[] = '<button type="button" class="btn btn-success" title="Klik untuk memilih dokter pasien" onclick="setDokterPasien('."'".$pasienrd->id_dokter."'".', '."'".$pasienrd->nama_dokter."'".')">Pilih</button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Diagnosa Tindakan (ALL)
    public function ajax_list_tindakan() {
        $list = $this->Pasien_hcu_model->get_tindakan();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->daftartindakan_nama;
            $row[] = '<button type="button" class="btn btn-success" title="Klik untuk memilih dokter pasien" onclick="setDiagnosa('."'".$pasienrd->icd."'".')">Pilih</button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Diagnosa Tindakan (ALL)
    public function ajax_list_tindakan2() {
        $list = $this->Pasien_hcu_model->get_tindakan();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->daftartindakan_nama;
            $row[] = '<button type="button" class="btn btn-success" title="Klik untuk memilih dokter pasien" onclick="setTindakan('."'".$pasienrd->daftartindakan_id."'".', '."'".$pasienrd->icd."'".')">Pilih</button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Diagnosa ICD 10 (ALL)
    public function ajax_list_diagnosa_icd_10() {
        $list = $this->Pasien_hcu_model->get_diagnosa_icd_10();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->kode_diagnosa;
            $row[] = $pasienrd->nama_diagnosa;
            $row[] = '<button type="button" class="btn btn-success" title="Klik untuk memilih dokter pasien" onclick="setDiagnosaICD10('."'".$pasienrd->kode_diagnosa."'".', '."'".$pasienrd->nama_diagnosa."'".')">Pilih</button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Diagnosa ICD 10 (ALL)
    public function ajax_list_diagnosa_icd_9() {
        $list = $this->Pasien_hcu_model->get_tindakan_icd_9();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->kode_diagnosa;
            $row[] = $pasienrd->nama_diagnosa;
            $row[] = '<button type="button" class="btn btn-success" title="Klik untuk memilih dokter pasien" onclick="setDiagnosaICD9('."'".$pasienrd->kode_diagnosa."'".', '."'".$pasienrd->nama_diagnosa."'".')">Pilih</button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Tindakan
    public function ajax_list_tindakan_pasienhcu() {
        $pendaftaran_id = $this->input->get("pendaftaran_id", TRUE);
        $list = $this->Pasien_hcu_model->get_tindakan_pasien($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $jml_jenis_obat = $this->Pasien_hcu_model->get_jumlah_jenis_obat($pasienrd->tindakanpasien_id);
            $jml_jenis_bhp = $this->Pasien_hcu_model->get_jumlah_jenis_bhp($pasienrd->tindakanpasien_id);
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->tgl_tindakan;
            $row[] = $pasienrd->nama_tindakan;
            $row[] = $pasienrd->jml_tindakan;
            $row[] = isset($jml_jenis_obat) ? $jml_jenis_obat : 0;
            $row[] = isset($jml_jenis_bhp) ? $jml_jenis_bhp : 0;
            $row[] = isset($pasienrd->nama_dokter) ? $pasienrd->nama_dokter : "-";
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk hapus tindakan pasien" onclick="removeTindakanPasienHCU('."'".$pasienrd->tindakanpasien_id."'".')"><span class="fa fa-times"></span></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Get Data Diagnosa ICD 9 (BY DIAGNOSA ID)
    public function ajax_get_diagnosa_icd_9() {
        $icd = $this->input->post('icd',TRUE);
        $list = $this->Pasien_hcu_model->get_diagnosa_icd_9($icd);
        $data = isset($list) ? array(
            "kode_diagnosa" => $list->kode_diagnosa,
            "nama_diagnosa" => $list->nama_diagnosa
        ): array(
            "kode_diagnosa" => "",
            "nama_diagnosa" => ""
        );
        
        $output = array(
            'csrfTokenName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash(),
            'success' => true,
            'data'  => $data
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_reseptur_pasien() {
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_hcu_model->get_list_reseptur_pasien($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->tgl_reseptur;
            $row[] = $pasienrd->nama_barang;
            $row[] = $pasienrd->qty;
            $row[] = $pasienrd->harga_jual;
            $row[] = $pasienrd->total_harga;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus alergi pasien" onclick="removeAlergi('."'".$pasienrd->reseptur_id."'".')"><i class="fa fa-times"></i></button>';

            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Remove Data Alergi Pasien HCU (BY ALERGI PASIEN ID)
    public function remove_alergi_pasienhcu() {
        $alergi_pasien_id = $this->input->post('alergi_pasien_id', TRUE);
        $status = $this->Pasien_hcu_model->remove_alergi_pasien($alergi_pasien_id);
        if ($status) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Alergi berhasil dihapus'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus Alergi, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }

    // Update Background
    public function update_background() {
        $keluhan = $this->input->post("keluhan_masuk", TRUE);
        $pendaftaran_id = $this->input->post("pendaftaran_id", TRUE);
        $status = $this->Pasien_hcu_model->update_background($keluhan, $pendaftaran_id);
        if ($status) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Alergi berhasil dihapus'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus Alergi, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }

    // Insert Data Alergi Pasien
    public function insert_alergi_pasienhcu() {
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
        $pasien_id = $this->input->post('pasien_id', TRUE);
        $alergi_id = $this->input->post('alergi_id', TRUE);
        $tanggal = date("Y-m-d");

        $data = array(
            "alergi_id" => $alergi_id,
            "tanggal" => $tanggal,
            "pendaftaran_id" => $pendaftaran_id,
            "pasien_id" => $pasien_id,
            "instalasi_id" => 8
        );

        $status = $this->Pasien_hcu_model->insert_alergi_pasien($data);
        if ($status) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Alergi berhasil ditambahkan'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Alergi, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }

    // Show View Periksa Pasien HCU (BY PENDAFTARAN ID)
    public function periksa_pasienhcu($pendaftaran_id) {
        $pasienHCU['list_pasien'] = $this->Pasien_hcu_model->get_pendaftaran_pasien($pendaftaran_id);

        // For Assestment
        $pasienHCU['wong_baker'] = $this->Pasien_hcu_model->get_mlookup("assestment.numeric.wong");
        $pasienHCU['nyeri'] = $this->Pasien_hcu_model->get_mlookup("assestment.nyeri");
        $pasienHCU['therapy'] = $this->Pasien_hcu_model->get_program_therapy();

        // For Background
        $pasienHCU['keluhan_masuk'] = $this->Pasien_hcu_model->get_keluhan_masuk($pendaftaran_id);
        $pasienHCU['list_alergi'] = $this->Pasien_hcu_model->get_list_alergi();

        // For Tindakan
        $pasienHCU['list_dokter'] = $this->Pasien_hcu_model->get_list_dokter_all();
        $pasienHCU['list_user'] = $this->Pasien_hcu_model->get_list_user_all();
        $pasienHCU['list_obat'] = $this->Pasien_hcu_model->get_list_obat_all();
        $pasienHCU['list_alkes'] = $this->Pasien_hcu_model->get_list_alkes_all();
        $pasienHCU['list_satuan_obat_alkes'] = $this->Pasien_hcu_model->get_list_satuan_obat_alkes_all();

        $this->load->view('v_periksa_pasien_hcu', $pasienHCU);
    }

    // Show View Assestment Pasien HCU (BY PENDAFTARAN ID)
    public function view_assestment_pasienhcu($pendaftaran_id) {
        $pasienHCU['list_pasien'] = $this->Pasien_hcu_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('v_riwayat_assestment_pasien', $pasienHCU);
    }

    // Insert Data Assestment Pasien
    public function insert_assestment(){
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
        $pasien_id = $this->input->post('pasien_id', TRUE);
        $tensi = $this->input->post('tensi', TRUE);
        $suhu = $this->input->post('suhu', TRUE);
        $penggunaan_o2 = $this->input->post('penggunaan_o2', TRUE);
        $nyeri = $this->input->post('nyeri', TRUE);
        $resiko_jatuh = $this->input->post('resiko_jatuh', TRUE);
        $tensi_nadi = $this->input->post('tensi_nadi', TRUE);
        $suhu_nadi = $this->input->post('suhu_nadi', TRUE);
        $saturasi_o2 = $this->input->post('saturasi_o2', TRUE);
        $numeric_wong_baker = $this->input->post('numeric_wong_baker', TRUE);
        $program_therapy = $this->input->post('program_therapy', TRUE);

        $dataAssesment = array(
            "instalasi_id" => 8,
            "pendaftaran_id" => $pendaftaran_id,
            "pasien_id" => $pasien_id,
            "tensi" => $tensi,
            "nadi_1" => $tensi_nadi,
            "suhu" => $suhu,
            "nadi_2" => $suhu_nadi,
            "penggunaan" => $penggunaan_o2,
            "saturasi" => $saturasi_o2,
            "nyeri" => $nyeri,
            "numeric_wong_baker" => $numeric_wong_baker,
            "resiko_jatuh" => $resiko_jatuh
        );

        $insertAssesment = $this->Pasien_hcu_model->insert_assestment_pasienhcu($dataAssesment);
        if ($insertAssesment > 0) {

            $program_therapy = json_decode($program_therapy);
            foreach ($program_therapy as $item) {
                $dataProgramTherapy = array(
                    "program_therapy_id" => $item->id,
                    "assestment_pasien_id" => $insertAssesment
                );

                $this->Pasien_hcu_model->insert_program_therapy_assestment($dataProgramTherapy);
            }

            $this->setStatusPasien("Sudah Periksa", $pendaftaran_id);

            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Assestment berhasil ditambahkan'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Assestment, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }

    // Insert Data Diagnosa
    public function insert_diagnosa() {
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
        $pasien_id = $this->input->post('pasien_id', TRUE);
        $dokter_1 = $this->input->post('nama_dokter_1_id', TRUE);
        $dokter_2 = $this->input->post('nama_dokter_2_id', TRUE);
        $dokter_3 = $this->input->post('nama_dokter_3_id', TRUE);
        $kode_diagnosa = $this->input->post('kode_diagnosa', TRUE);
        $nama_diagnosa = $this->input->post('nama_diagnosa', TRUE);
        $kode_diagnosa_icd_10 = $this->input->post('kode_diagnosa_icd_10', TRUE);
        $nama_diagnosa_icd_10 = $this->input->post('nama_diagnosa_icd_10', TRUE);
        $jenis_diagnosa = $this->input->post('jenis_diagnosa', TRUE);
        $catatan_diagnosa = $this->input->post('catatan_diagnosa', TRUE);

        $dataDiagnosa = array(
            "instalasi_id" => 8,
            "pendaftaran_id" => $pendaftaran_id,
            "pasien_id" => $pasien_id,
            "dokter_id" => $dokter_1,
            "dokter_id_2" => $dokter_2,
            "dokter_id_3" => $dokter_3,
            "kode_diagnosa" => $kode_diagnosa,
            "nama_diagnosa" => $nama_diagnosa,
            "kode_icd10" => $kode_diagnosa_icd_10,
            "nama_icd10" => $nama_diagnosa_icd_10,
            "jenis_diagnosa" => $jenis_diagnosa,
            "tgl_diagnosa" => date("Y-m-d")
        );

        $insertDiagnosa = $this->Pasien_hcu_model->insert_diagnosa_pasienhcu($dataDiagnosa);
        if ($insertDiagnosa) {

            $this->setStatusPasien("Sudah Periksa", $pendaftaran_id);

            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa berhasil ditambahkan'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Assestment, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }

    // Insert Data Tindakan
    public function insert_tindakan() {
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
        $pasien_id = $this->input->post('pasien_id', TRUE);
        $id_tindakan = $this->input->post('id_tindakan', TRUE);
        $jumlah_tindakan = $this->input->post('jumlah_tindakan', TRUE);
        $kode_icd_9 = $this->input->post('kode_icd_9', TRUE);
        $jumlah_icd_9 = $this->input->post('jumlah_icd_9', TRUE);
        $user_id = $this->input->post('tindakan_user', TRUE);
        $dokter_id = $this->input->post('tindakan_dokter', TRUE);
        $tindakan_obat = $this->input->post('tindakan_obat', TRUE);
        $tindakan_bhp = $this->input->post('tindakan_bhp', TRUE);

        $dataTindakan = array(
            "instalasi_id" => 8,
            "pendaftaran_id" => $pendaftaran_id,
            "pasien_id" => $pasien_id,
            "dokter_id" => $dokter_id,
            "user_id" => $user_id,
            "daftartindakan_id" => $id_tindakan,
            "jml_tindakan" => $jumlah_tindakan,
            "tgl_tindakan" => date("Y-m-d")
        );

        $insertTindakan = $this->Pasien_hcu_model->insert_tindakan_pasienhcu($dataTindakan);
        if ($insertTindakan > 0) {

            $tindakan_obat = json_decode($tindakan_obat);
            foreach($tindakan_obat as $item) {
                $data = array(
                    "tindakan_pasien_id" => $insertTindakan,
                    "barang_farmasi_id" => $item->id,
                    "jumlah" => $item->jumlah,
                    "sediaan_id" => $item->id_satuan
                );

                $this->Pasien_hcu_model->insert_tindakan_obat_pasienhcu($data);
            }

            $tindakan_bhp = json_decode($tindakan_bhp);
            foreach($tindakan_bhp as $item) {
                $data = array(
                    "tindakan_pasien_id" => $insertTindakan,
                    "barang_farmasi_id" => $item->id,
                    "jumlah" => $item->jumlah,
                    "sediaan_id" => $item->id_satuan
                );

                $this->Pasien_hcu_model->insert_tindakan_bhp_pasienhcu($data);
            }

            $this->setStatusPasien("Sudah Periksa", $pendaftaran_id);

            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan berhasil ditambahkan'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }

    // Delete Data Asssestment Pasien
    public function remove_assestment() {
        $assestment_id = $this->input->post('assestment_id', TRUE);
        $status = $this->Pasien_hcu_model->remove_riwayat_assestment_pasien($assestment_id);
        if ($status) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Assestment berhasil dihapus'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus Assestment, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }

    // Delete Data Tindakan Pasien
    public function remove_tindakan_pasien() {
        $pasien_tindakan_id = $this->input->post('pasien_tindakan_id', TRUE);
        $status = $this->Pasien_hcu_model->remove_tindakan_pasien($pasien_tindakan_id);
        if ($status) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan berhasil dihapus'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus Tindakan, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }

    // Set Status Pasien
    private function setStatusPasien($status, $pendaftaran_id) {
        $this->Pasien_hcu_model->set_status_periksa_pasien($pendaftaran_id, $status);
    }

    // Get All Obat List
    public function get_obat_list() {
        $list = $this->Pasien_hcu_model->get_list_obat_all2();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrd->nama_barang;
            $row[] = '<button type="button" class="btn btn-success" title="Klik untuk hapus tindakan pasien" onclick="setResepturObat('."'".$pasienrd->id_barang."'".', '."'".$pasienrd->nama_barang."'".')">Pilih</button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    // Insert Reseptu Obat
    public function insert_reseptur_obat() {
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
        $pasien_id = $this->input->post('pasien_id', TRUE);
        $dokter_id = $this->input->post('dokter_id', TRUE);
        $nama_obat = $this->input->post('nama_obat', TRUE);
        $id_jenis_barang = $this->input->post('id_jenis_barang', TRUE);
        $qty_obat = $this->input->post('qty_obat', TRUE);
        $harga_jual = $this->input->post('harga_jual', TRUE);
        $satuan_obat = $this->input->post('satuan_obat', TRUE);
        $signa_1 = $this->input->post('signa_1', TRUE);
        $signa_2 = $this->input->post('signa_2', TRUE);
        $racikan_obat = $this->input->post('racikan_obat', TRUE);

        $dataObat = array(
            "pendaftaran_id" => $pendaftaran_id,
            "pasien_id" => $pasien_id,
            "dokter_id" => $dokter_id,
            "nama_barang" => $nama_obat,
            "qty" => $qty_obat,
            "harga_jual" => $harga_jual,
            "total_harga" => ($qty_obat * $harga_jual),
            "tgl_reseptur" => date("Y-m-d"),
            "id_jenis_barang" => $id_jenis_barang,
            "id_sediaan" => $satuan_obat
        );

        $insertObat = $this->Pasien_hcu_model->insert_reseptur_obat_pasienhcu($dataObat);
        if ($insertObat > 0) {

            $racikan_obat = json_decode($racikan_obat);
            foreach($racikan_obat as $item) {
                $data = array(
                    "barang_farmasi_id" => $item->id,
                    "sediaan_obat_id" => $item->id_satuan,
                    "jumlah" => $item->jumlah,
                    "jumlah_obat" => $item->jumlah,
                    "keterangan" => $item->keterangan,
                    "reseptur_id" => $insertObat
                );

                $this->Pasien_hcu_model->insert_reseptur_obat_detail_pasienhcu($data);
            }

            $this->setStatusPasien("Sudah Periksa", $pendaftaran_id);

            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Reseptur Obat berhasil ditambahkan'
            );
            echo json_encode($res);
        }
        else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Reseptur Obat, hubungi web administrator.'
            );
            echo json_encode($res);
        }
    }
}
