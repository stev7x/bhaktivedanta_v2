<?php $this->load->view('header_iframe');?>
<body id="body">

<div class="white-box" style="min-height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    

    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                                <input type="hidden" id="dokter_id" name="dokter_id" value="<?php echo $list_pasien->id_M_DOKTER;?>">
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#informasi" class="nav-link" aria-controls="informasi" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Informasi</span></a></li>
                <li role="presentation" class="nav-item"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Diagnosa</span></a></li> 
            </ul>
            <!-- Tab panes -->
            <div class="tab-content"> 
                <div role="tabpanel" class="tab-pane active" id="informasi">
                    <?php echo form_open('#',array('id' => 'fmInformasiPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                    <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id;?>">

                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>

                    <div class="col-md-6">   

                        <div class="form-group">   
                            <label class="control-label">Kondisi Saat Keluar<span style="color: red;"> *</span></label>
                            <select id="kondisi_pasien" name="kondisi_pasien" class="form-control" >
                                <option value="" disabled selected>Pilih Kondisi Pasien</option>
                            </select> 
                        </div>
                        <h3>TANDA VITAL</h3>
                        <div class="form-group">   
                            <label class="control-label">Tekanan Darah<span style="color: red;"> *</span></label>
                            <input type="number" name="tekanan_darah" id="tekanan_darah" class="form-control">
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Nadi<span style="color: red;"> *</span></label>
                            <input type="number" name="nadi" id="nadi" class="form-control">
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Respitory<span style="color: red;"> *</span></label>
                            <input type="number" name="respitory" id="respitory" class="form-control">
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Suhu<span style="color: red;"> *</span></label>
                            <input type="number" name="suhu" id="suhu" class="form-control">
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Catatan<span style="color: red;"> *</span></label>
                            <textarea name="catatan" id="catatan" class="form-control"></textarea>
                        </div>

                    </div>
                    <div class="col-md-6">  

                        <div class="form-group">   
                            <input type="checkbox" name="rujuk" id="rujuk">
                            <label class="control-label">Rujuk</label>
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Instansi<span style="color: red;"> *</span></label>
                            <select id="instansi" name="instansi" class="form-control" >
                                <option value="" disabled selected>Pilih Instansi Rujukan</option>
                            </select> 
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Catatan Rujukan<span style="color: red;"> *</span></label>
                            <textarea name="catatan_rujukan" id="catatan_rujukan" class="form-control"></textarea>
                        </div>

                    </div>

                    <?php echo form_close();?>  
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="diagnosa">
                    <div class="panel panel-default" style="margin:0">
                        <div class="panel-body">
                            <div class="row table-responsive" style="width: 100%">
                                <table id="table_list_diagnosa_hcu" class="table table-striped" cellspacing="0" style="width: 100%">  
                                    <thead>
                                        <tr>
                                            <th>Tgl. Diagnosa</th>
                                            <th>Kode Diagnosa</th>
                                            <th>Nama Diagnosa</th>
                                            <th>Kode ICD 10</th>
                                            <th>Nama ICD 10</th>
                                            <th>Jenis Diagnosa</th>
                                            <th>Nama Dokter</th>
                                            <th>Diagnosa Medis</th>
                                            <th>Botol / Hapus</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        <tr>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td>Example</td>
                                            <td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus Resep" onclick=""><i class="fa fa-times"></i></button></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Tgl. Diagnosa</th>
                                            <th>Kode Diagnosa</th>
                                            <th>Nama Diagnosa</th>
                                            <th>Kode ICD 10</th>
                                            <th>Nama ICD 10</th>
                                            <th>Jenis Diagnosa</th>
                                            <th>Nama Dokter</th>
                                            <th>Diagnosa Medis</th>
                                            <th>Botol / Hapus</th>
                                        </tr>
                                    </tfoot>
                                </table> 
                            </div>
                            <br>
                            <hr>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="row">
                                        <label class="label-control">Nama Dokter 1</label>
                                    </div>
                                    <div class="input-group custom-search-form">
                                        <input readonly type="text" class="form-control autocomplete" id="nama_dokter_1" placeholder="Nama Dokter"> 
                                        <span class="input-group-btn">
                                            <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_pasien" data-backdrop="static" data-keyboard="false" type="button" onclick="dialogPasien()"> <i class="fa fa-bars"></i> </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="row">
                                        <label class="label-control">Nama Dokter 2</label>
                                    </div>
                                    <div class="input-group custom-search-form">
                                        <input readonly type="text" class="form-control autocomplete" id="nama_dokter_2" placeholder="Nama Dokter"> 
                                        <span class="input-group-btn">
                                            <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_pasien" data-backdrop="static" data-keyboard="false" type="button" onclick="dialogPasien()"> <i class="fa fa-bars"></i> </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="row">
                                        <label class="label-control">Nama Dokter 3</label>
                                    </div>
                                    <div class="input-group custom-search-form">
                                        <input readonly type="text" class="form-control autocomplete" id="nama_dokter_3" placeholder="Nama Dokter"> 
                                        <span class="input-group-btn">
                                            <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_pasien" data-backdrop="static" data-keyboard="false" type="button" onclick="dialogPasien()"> <i class="fa fa-bars"></i> </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <h3>Diagnosa</h3>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div class="row">
                                        <label class="label-control">Kode Diagnosa</label>
                                    </div>
                                    <div class="input-group custom-search-form">
                                        <input type="text" class="form-control autocomplete" id="kode_diagnosa" placeholder="Cari diagnosa"> 
                                        <span class="input-group-btn">
                                            <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_list_pasien" data-backdrop="static" data-keyboard="false" type="button" onclick="dialogPasien()"> Cari </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="row">
                                        <label class="label-control">Nama Diagnosa</label>
                                    </div>
                                    <input type="text" class="form-control" id="resiko_jatuh" name="resiko_jatuh" placeholder="Nama diagnosa">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" disabled class="form-control" id="resiko_jatuh" name="resiko_jatuh" placeholder="Map ICD ke Diagnosa">
                                </div>
                                <div class="form-group col-md-6">
                                    &nbsp;
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="row">
                                        <label class="label-control">Jenis Diagnosa</label>
                                    </div>
                                    <select class="form-control row" id="nyeri" name="nyeri">
                                        <option value="">Jenis Diagnosa</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    &nbsp;
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="row">
                                        <label class="label-control">Catatan</label>
                                    </div>
                                    <textarea name="keluhan" id="keluhan" rows="10" class="form-control" placeholder="Catatan Diagnosa Medis"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-info pull-right">Tambahkan</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>

</div>

<div class="col-md-12">
    <button class="btn btn-info" style="float: right;margin-right: 4px;margin-bottom: 16px;" onclick="doPulangkanPasien()">Pulangkan</button>
    <div class="clearfix"></div>
</div>

<?php $this->load->view('footer_iframe');?>
</body>

</html>
