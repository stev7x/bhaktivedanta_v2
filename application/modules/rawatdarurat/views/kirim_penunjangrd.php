<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                                <input type="hidden" id="instalasi_id" name="instalasi_id" value="<?php echo $list_pasien->instalasi_id; ?>"> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>


  
            <div class="modal-body" style="background: #fafafa"> 
                        <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div>
                                <p id="card_message1" class=""></p>
                            </div>
                        </div>
                         <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
            <div class="col-md-12">
                    <span style="color:red" >*) Wajib diisi</span>
                    <div class="form-group">
                        <label class="control-label">Pilih Penunjang<span style="color: red">*</span></label>
                        <select id="penunjang_id" name="penunjang_id" class="form-control select2" onchange="getPermintaanTindakanPenunjang()">
                            <option value=""  selected>Pilih Penunjang</option> 
                            <?php
                            $list_penunjang = $this->Pasien_rd_model->get_list_penunjang();
                                foreach($list_penunjang as $list){
                                    echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                }
                            ?>
                        </select>
                        <input type="hidden" id="pendaftaran_id_" name="pendaftaran_id_" value="<?php echo($pendaftaran_id_); ?>">
                    </div>
                 </div>
                 <div class="form-group">
                     <div class="form-group">
                            <label>Asal Ruangan :   </label>
                        </div>
                        <strong>
                            <?php
                            if ($list_pasien->instalasi_id == 1) {
                                echo "Instalasi Rawat Jalan";
                            }elseif ($list_pasien->instalasi_id == 2) {
                                echo "Instalasi Gawat Darurat";
                            }elseif($list_pasien->instalasi_id == 3){
                                 echo "Instalasi Rawat Inap";
                            }else{
                                 echo "Instalasi Penunjang";
                            }

                            ?> 
                        </strong>
                    </div> 
                 </div>
             </div>
                       

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#background" class="nav-link" aria-controls="background" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Background</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#tabs-diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Diagnosa</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-assestment" class="nav-link" aria-controls="assestement" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Asesstement</span></a></li>
                 <li role="presentation" class="nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Tindakan</span></a></li>
            </ul>
            <!-- Tab panes -->

             <div class="tab-content"> 


                <div role="tabpanel" class="tab-pane active" id="background"> 
                    <table id="table_background_pasienrd_penunjangrd" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Alergi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Alergi</th>
                            </tr> 
                        </tfoot>
                    </table><br>
                    
                <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div>


            <div role="tabpanel" class="tab-pane" id="tabs-diagnosa">
                    <table id="table_diagnosa_pasienrd_penunjangrd" class="table table-striped dataTable" cellspacing="0">
                        <thead>  
                            <tr>
                            <th>Tgl.Diagnosa</th>
                            <th>Kode Diagnosa</th>
                            <th>Nama Diagnosa</th>
                            <th>Keterangan</th>
                            </tr> 
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="4">No Data to Display</td>
                             </tr>
                        </tbody> 
                        <tfoot>
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>
                                <th>Keterangan</th> 
                            </tr> 
                        </tfoot>
                    </table><br>

                        <?php echo form_close();?>  
                    <div class="clearfix"></div>
            </div>

            <div role="tabpanel" class="tab-pane" id="tabs-assestment"> 

                 <table id="table_assestment_penunjangrd" class="table table-striped dataTable" cellspacing="0">                        
                    <thead>
                            <tr>
                                <th>No</th>
                                <th>Program Theraphy</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Program Theraphy</th>
                            </tr> 
                        </tfoot>
                    </table><br>
                        </div>
                <?php echo form_close();?> 
                    <div class="clearfix"></div>
          
                        <div role="tabpanel" class="tab-pane" id="tabs-tindakan"> 

                    <table id="table_tindakan_pasienrd_penunjangrd" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Cyto</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Cyto</th>
                               
                            </tr> 
                        </tfoot>
                    </table><br>
                    
                <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div> 
             </div>


              <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
            <div class="form-group col-sm-12">
                <label class="control-label">Catatan<span style="color: red;"> *</span></label>
                <textarea class="form-control" rows="5" id="catatan"></textarea>
            </div>
            </div> 

<!-- 
// Rencana Tindakan -->
 <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
         <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span> <span class="hidden-xs">Rencana Tindakan</span></a></li>
            </ul>
            <!-- Tab panes -->
                <div role="tabpanel" class="tab-pane" id="tabs-tindakan"> 

                    <table  class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Tindakan</th>
                                <th>Batal</th>
                            </tr>
                        </thead>
                        <tbody id="table_penununjang_pasienrd">
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama Tindakan</th>
                                <th>Batal</th>
                            </tr> 
                        </tfoot>
                    </table><br>

                    <div class="alert alert-success alert-dismissable row" id="modal_notif4" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div> 
                            <p id="card_message4" class=""></p>
                            </div>
                            </div>  
                    
                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />    
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif2" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div>
                                <p id="card_message2" class=""></p> 
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-4">   
                            <label class="control-label">Input Tindakan<span style="color: red;"> *</span></label>
                            <select  id="pilihtindakanpenunjang" name="pilihtindakanpenunjang" class="form-control select2" onchange="$('#tindakan').val($('#pilihtindakanpenunjang option:selected').text())" >
                                <option value="" selected>Pilih Tindakan</option>
                            </select> 
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label">Nama Tindakan</label> 
                            <input type="text" id="tindakan" name="tindakan" class="form-control">
                        </div>
                        <div class="form-group col-md-2 " style="margin-top: 26px;">
                        <a type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" onclick="changeDataRencanaTindakanPenunjangRd()"><i class="fa fa-floppy-o"></i> TAMBAH</a> 
                        </div>
                    </div>
                <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div>
        <div class="clearfix"></div>
                </div>
            </div>

            </div>  
          


           <!--  <div class="row"> -->
                <div class="col-md-3 pull-right">
                 <button type="button" class="btn btn-success col-md-12" onclick="savePenunjangRd()"><i class="fa fa-floppy-o p-r-9"></i >KIRIM KE PENUNJANG</button>
                </div>
                    <!-- </div>   -->
                
            
        
<!-- ClockPicker Stylesheet -->


</div>
</div>

<?php $this->load->view('footer_iframe');?>    
<!-- <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/pasien_rd/kirim_penunjangrd.js"></script>
 --></body>
 
</html>  