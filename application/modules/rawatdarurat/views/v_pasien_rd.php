
<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Pasien Rawat Darurat </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Rawat Darurat</a></li>
                            <li class="active">Pasien Rawat Darurat</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Data Pasien Rawat Darurat
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="row">
                                    <div class="col-md-2">
                                    <label for="inputName1" class="control-label"></label>
                                        <dl class="text-right">
                                            <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                            <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>
                                        </dl>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Tanggal Kunjungan,Dari</label>
                                                <div class="input-group">
                                                    <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <label>Sampai</label>
                                                <div class="input-group">
                                                    <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>&nbsp;</label><br>
                                                <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>
                                            </div>
                                            <div class="form-group col-md-3" style="margin-top: 7px">
                                                <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                <b>
                                                <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                    <option value="" disabled selected>Pilih Berdasarkan</option>
                                                    <option value="no_pendaftaran">No. Pendaftaran</option>
                                                    <option value="no_rekam_medis">No. Rekam Medis</option>
                                                    <option value="no_bpjs">No. BPJS</option>
                                                    <option value="nama_pasien">Nama Pasien</option>
                                                    <option value="pasien_alamat">Alamat Pasien</option>
                                                    <option value="status">Status</option>
                                                </select></b>

                                            </div>
                                            <div class="form-group col-md-9" style="margin-top: 7px">
                                                <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasien()" >
                                            </div>
                                        </div>
                                    </div>
                                </div><br><hr style="margin-top: -27px">

                                   <table id="table_list_pasienrd" class="table table-striped table-responsive" cellspacing="0">
                                       <thead>
                                            <tr>
                                                <th>No. Pendaftaran</th>
                                                <th>Tanggal Pendaftaran</th>
                                                <th>No. Rekam Medis</th>
                                                <th>No. BPJS</th>
                                                <th>Nama Pasien</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur Pasien</th>
                                                <th>Alamat Pasien</th>
                                                <th>Pembayaran</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Status Pasien</th>
                                                <th>Periksa Pasien</th>
                                                <th>Proses Lain</th>
                                                <!--
                                                Kirim Ke Rawat INAP
                                                Kirim Ke OK
                                                Kirim Ke VK
                                                Kirim Ke Penunjang
                                                Pulangkan Pasien
                                                View History
                                                Batal-Tolak
                                                -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="11">No data to display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--/row -->

            </div>
            <!-- /.container-fluid -->

<div id="modal_periksa_pasienrd" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Pasien</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:650px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->

<div id="modal_paket_operasi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: auto;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Pendaftaran Paket Operasi</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                    <?php echo form_open('#',array('id' => 'fmPaketOperasi'))?>
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif2" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button>
                            <div>
                                <p id="card_message2" class=""></p>
                            </div>
                        </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pilih Paket Operasi<span style="color:red">*</span></label>
                            <input type="hidden" id="pendaftaran_id_po" name="pendaftaran_id_po" value="" readonly>
                            <input type="hidden" id="pasien_id_po" name="pasien_id_po" value="" readonly>
                            <select id="paket_operasi_po" name="paket_operasi_po" class="form-control select2" onchange="getRuangan()">
                                <option value=""  selected>Pilih Paket Operasi</option>
                                <?php
                                $list_paketoperasi = $this->Pasien_rd_model->get_paketoperasi();
                                foreach($list_paketoperasi as $list){
                                    echo "<option value='".$list->paketoperasi_id."'>".$list->nama_paket."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Poliklinik/Ruangan<span style="color: red;"> *</span></label>
                            <select id="poli_ruangan_po" name="poli_ruangan_po" class="form-control select2" onchange="getKamarPo()">
                                <option value=""  selected>Pilih Poli/Ruangan</option>

                            </select>
                        </div>
                        <div class="form-group">
                           <label>Kamar<span style="color: red;"> *</span></label>
                            <select id="kamarruangan_po" name="kamarruangan_po" class="form-control select2">
                                <option value=""  selected>Pilih Kamar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Dokter Penanggung jawab<span style="color: red;"> *</span></label>
                            <select id="dokter_po" name="dokter_po" class="form-control select2">
                                <option value=""  selected>Pilih Dokter</option>
                                <?php
                                $list_umum = $this->Pasien_rd_model->get_dokter_umum();
                                foreach($list_umum as $list){
                                    echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Dokter Anastesi<span style="color: red;"> *</span></label>
                            <select id="dokter_anastesi_po" name="dokter_anastesi_po" class="form-control select2">
                                <option value=""  selected>Pilih Dokter</option>
                                <?php
                                $list_anastesi = $this->Pasien_rd_model->get_dokter_anastesi();
                                foreach($list_anastesi as $list){
                                    echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Perawat<span style="color: red;"> *</span></label>
                            <select id="perawat_po" name="perawat_po" class="form-control select2">
                                <option value=""  selected>Pilih Perawat</option>
                                <?php
                                $list_perawat = $this->Pasien_rd_model->get_perawat();
                                foreach($list_perawat as $list){
                                    echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-success pull-right" onclick="savePO()"><i class="fa fa-floppy-o p-r-10"></i> SIMPAN</button>
                    </div>
                    <?php echo form_close(); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal_status_pulang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content" style="overflow: auto;height: auto;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Pulangkan Pasien</b></h2>
            </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_kirim_pasienrd" class="modal" style="width: 80% !important ; max-height: 90% !important ;">
    <div class="modal-content">
        <h1>Pemeriksaan Pasien</h1>
        <div class="divider"></div>
        <div id="card-alert" class="card green modal_notif" style="display:none;">
            <div class="card-content white-text">
                <p id="modal_card_message"></p>
            </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="col s12 formValidate">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close" onclick="reloadTablePasien()">Close<i class="mdi-navigation-close left"></i></button>
    </div>
</div>
<!-- /.modal -->


<div id="modal_kirim_penunjang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim Penunjang</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_kirim_hcurd" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim HCU</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_rujuk_rslain" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Rujuk Rumah Sakit Lain</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_kirim_pathologird" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim PATHOLOGI ANAK</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_kirim_isolasird" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim ISOLASI</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_kirim_vkrd" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim VK</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_kirim_okrd" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim OK</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_kirim_permintaanrawat" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Permintaan Rawat</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_riwayat_penyakit_pasien" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Riwayat Penyakit Pasien</b></h2>
            </div>
        </div>
    </div>        <!-- /.modal-content -->
</div>    <!-- /.modal-dialog -->

<div id="modal_kirim_rawatinap" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                onclick="reloadTablePasien()">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim Rawat Inap</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>

                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

</div>


<?php $this->load->view('footer');?>
