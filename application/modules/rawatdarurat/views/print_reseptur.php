<!DOCTYPE html >
<head>
<title>Detail Pemeriksaan Pasien</title>
<style type="text/css" rel="stylesheet">
      body{
      padding:1;
      }
      table {
  border-collapse: collapse;
  white-space: normal;        /*text-align: center center;*/
      }

  </style>
</head>
<body onLoad="window.print()">
<table width="100%" border="0" cellpadding="1">
  <tr>      
    <td width="18%" rowspan="5" align="center">       
    <img src="<?= base_url() ?>assets/plugins/images/logo-puribunda.png" >
    </td> 
    <td width="65%" align="center">RUMAH SAKIT IBU DAN ANAK</td>
    <td width="17%" align="center">&nbsp;</td>
  </tr> 
  <tr>
    <td align="center">&quot; PURI BUNDA &quot;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">Jl. Simpang Sulfat Utara No. 60 A Malang</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">Phone ( 0341) 480047</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
</table>
<hr width="100%" color="#000000">
<table width="100%" border="0" cellpadding="1" cellspacing="5">
   <tr>
    <td align="center"><strong>RESEPTUR OBAT</strong></td>
    <td align="center">&nbsp;</td> 
  </tr>
</table>
<table width="100%" border="0" cellpadding="1" cellspacing="5">
  <tr> 
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td width="13%">Dokter</td>
    <td width="1%">:</td>
    <td colspan="2"><?php 
    $list_resep =$this->Pasien_rd_model->get_pendaftran_resep_pasien($pendaftaran_id);
    foreach ($list_resep as $list) {
        echo $list->NAME_DOKTER;
    }
  
     ?></td>
    <td align="right" width="14%">Tanggal </td>
    <td width="1%">:</td> 
    <td width="31%"><?php echo date("Y-M-d") ?></td>
  </tr>

  <tr>
    <td>Instalasi</td>
    <td>:</td>
    <td><?php 
    $list_resep =$this->Pasien_rd_model->get_pendaftran_resep_pasien($pendaftaran_id);
    foreach ($list_resep as $list) {
        echo $list->instalasi_nama;
    }
  
     ?>
       
     </td>
  </tr>
  
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="1">

  <tr style="border-bottom: 2px solid black;">
    <th width="" align="left" valign="middle"><strong>Nama Obat</strong></th>
    <th width="" align="left"  valign="middle"><strong>Jumlah</strong></th>
    <th width="" align="left"   valign="middle"><strong>Keterangan</strong></th>
    <th width="" align="left"  valign="middle"><strong>Harga</strong></th>
  </tr> 

          <?php 
            $resep =$this->Pasien_rd_model->get_detail_resep_pasien($pendaftaran_id);
          foreach ($resep as $list) {
            echo '<tr>';
            echo '<td>'.$list->nama_barang.'</td>';
            echo '<td>'.$list->qty.'</td>';
            echo '<td>'.$list->keterangan.'</td>';
            echo '<td>'.$list->total_harga.'</td>';
            echo '</tr>';
           }
          ?>

      </table>


       <table width="100%" border="0" cellpadding="1" cellspacing="5">
       <tr  style="border-top: 2px solid black;">
          <td width="80%" align="right" valign="middle">Total </td>
          <td width="">:</td> 
          <td width=""><?php echo $tot_rad->total_harga;?></td>
       </tr>
     </table>

<table width="100%" border="0" cellpadding="1" cellspacing="5">
  <tr> 
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td width="13%">Pasien</td>
    <td width="1%">:</td>
    <td colspan="2">
      <?php 
    $list_resep =$this->Pasien_rd_model->get_pendaftran_resep_pasien($pendaftaran_id);
    foreach ($list_resep as $list) {
        echo $list->pasien_nama;
    }
  
     ?>

    </td>
  </tr>

  <tr>
    <td>Umur</td>
    <td>:</td>
    <td>
      <?php 
    $list_resep =$this->Pasien_rd_model->get_pendaftran_resep_pasien($pendaftaran_id);
    foreach ($list_resep as $list) {
        echo $list->umur;
    }
     ?>
    </td>
  </tr>
  
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>

<table width="100%" border="0" cellpadding="1">
<tr>
  <td colspan="4" rowspan="5">&nbsp;</td>
  <td align="center">&nbsp;</td>
</tr>
<tr>
   <td width="1%" align="center">TTD</td>
    <td width="10%" align="center">TTD</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>(.....................................................)</td>
    <td>(.....................................................)</td>
  </tr>
</table>


     
          

</body>
</html>
