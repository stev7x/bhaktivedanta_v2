<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:640px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->

    <div class="row">
        <div class="col-md-12" style="padding-bottom:15px">
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">
                                    <tr>
                                        <td><b>Tgl.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">
                                    <tr>
                                        <td><b>Nama Pasien</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr>
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">
                                <input type="hidden" id="dokter_id" name="dokter_id" value="<?php echo $list_pasien->id_M_DOKTER;?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="nav-item active"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Diagnosa</span></a></li>
                <li role="presentation" class="nav-item"><a href="#assestment" class="nav-link" aria-controls="assestment" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Assestment</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tindakans" class="nav-link" aria-controls="tindakans" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-user"></i></span><span class="hidden-xs">Tindakan</span></a></li>
                <li role="presentation" class="nav-item"><a href="#background" class="nav-link" aria-controls="background" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Background</span></a></li>
                <li role="presentation" class="nav-item"><a href="#obat" class="nav-link" aria-controls="obat" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Reseptur Obat</span></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="diagnosa">
                    <table id="table_diagnosa_pasienrd_periksa" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                              <th>Tgl.Diagnosa</th>
                              <th>Kode Diagnosa</th>
                              <th>Nama Diagnosa</th>
                              <th>Kode ICD 10</th>
                              <th>Nama ICD 10</th>
                              <th>Jenis Diagnosa</th>
                              <th>Nama Dokter</th>
                              <th>Diagnosa Medis</th>
                              <th>Batal / Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="9" align="center">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>Tgl.Diagnosa</th>
                              <th>Kode Diagnosa</th>
                              <th>Nama Diagnosa</th>
                              <th>Kode ICD 10</th>
                              <th>Nama ICD 10</th>
                              <th>Jenis Diagnosa</th>
                              <th>Nama Dokter</th>
                              <th>Diagnosa Medis</th>
                              <th>Batal / Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                    <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id;?>">

                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                      <div class="col-md-12">
                        <h4>Diagnosa</h4><br>
                      </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-4">
                            <label class="control-label">Nama Dokter <span style="color: red;"> *</span></label>
                            <div class="input-group">
                                <input type="hidden" name="dokter_id_1" id="dokter_id_1" value="">
                                <input type="text" class="form-control" id="nama_dokter1" name="nama_dokter1" placeholder="Pilih Dokter">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button" title="Lihat List Dokter" data-toggle="modal" data-target="#modal_dokter" onclick="dialogListDokter1()"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Nama Dokter</label>
                            <div class="input-group">
                                <input type="hidden" name="dokter_id_2" id="dokter_id_2" value="">
                                <input type="text" class="form-control" id="nama_dokter2" name="nama_dokter2" placeholder="Pilih Dokter">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button" title="Lihat List Dokter" data-toggle="modal" data-target="#modal_dokter" onclick="dialogListDokter2()"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Nama Dokter</label>
                            <div class="input-group">
                                <input type="hidden" name="dokter_id_3" id="dokter_id_3" value="">
                                <input type="text" class="form-control" id="nama_dokter3" name="nama_dokter3" placeholder="Pilih Dokter">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button" title="Lihat List Dokter" data-toggle="modal" data-target="#modal_dokter" onclick="dialogListDokter3()"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-6">
                            <label class="control-label">Kode Diagnosa<span style="color: red;"> *</span></label>
                            <div class="input-group">
                                <input type="hidden" name="diagnosa_id" id="diagnosa_id" value="">
                                <input type="text" class="form-control" id="kode_diagnosa" name="kode_diagnosa" placeholder="Cari Kode Diagnosa">
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button" title="Lihat List Dokter" data-toggle="modal" data-target="#modal_list_diagnosa" onclick="dialogPilihKodeDiagnosa()"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">Nama Diagnosa<span style="color: red;"> *</span></label>
                            <input id="nama_diagnosa" type="text" name="nama_diagnosa" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                        <div class="form-group col-md-6" style="margin-top:-25px;">
                            <label>&nbsp;</label>
                            <button style="width: 100%" type="button" class="btn btn-success" id="mapICD10keDiagnosa">
                                <!-- <i class="fa fa-floppy-o"></i> -->
                                Map ICD 10 Ke Diagnosa
                            </button>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group col-md-6">
                              <label class="control-label">Kode Diagnosa ICD 10<span style="color: red;"> *</span></label>
                              <div class="input-group">
                                  <input type="text" class="form-control" id="kode_diagnosa_icd10" name="kode_diagnosa_icd10" placeholder="Cari Kode Diagnosa ICD 10">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" title="Lihat List Dokter" data-toggle="modal" data-target="#modal_list_diagnosa_icd10" onclick="dialogPilihKodeDiagnosaICD10()"><i class="fa fa-search"></i></button>
                                  </span>
                              </div>
                          </div>
                          <div class="form-group col-md-6">
                              <label class="control-label">Nama Diagnosa ICD 10<span style="color: red;"> *</span></label>
                              <input id="nama_diagnosa_icd10" type="text" name="nama_diagnosa_icd10" class="form-control" placeholder="Nama Diagnosa" value="">
                          </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label">Jenis Diagnosa<span style="color: red;"> *</span></label>
                            <select id="jenis_diagnosa" name="jenis_diagnosa" class="form-control" >
                                <option value="" disabled selected>Pilih Jenis Diagnosa</option>
                                <option value="1">Diagnosa Utama</option>
                                <option value="2">Diagnosa Tambahan</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label">Catatan<span style="color: red;"> *</span></label>
                            <textarea id="catatan_diagnosa" name="catatan_diagnosa" class="form-control" style="min-height:175px !important;" placeholder="Tuliskan catatan bila diperlukan" ></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group col-md-3" style="margin-top:-30px;">
                          <label>&nbsp;</label>
                          <button style="width: 100%" type="button" class="btn btn-success" id="saveDiagnosa">
                              <i class="fa fa-plus"></i> TAMBAH
                          </button>
                      </div>
                    </div>
                  </div>

                        <?php echo form_close();?>
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="assestment">
                    <input id="assessment_pasien_id" type="hidden" name="assessment_pasien_id" value="" class="form-control">
                    <div class="form-group col-md-12">
                        <label class="control-label">Tanda - Tanda Fital<span style="color: red;"> *</span></label>
                    </div>
                    <div class="form-group col-md-5">
                        <input id="tensi" type="text" name="tensi" value="" class="form-control" placeholder="Tensi">
                    </div>
                    <div class="form-group col-md-1">
                        <span>mmHG</span>
                    </div>
                    <div class="form-group col-md-5">
                        <input id="nadi_1" type="text" name="nadi_1" value="" class="form-control" placeholder="Nadi">
                    </div>
                    <div class="form-group col-md-1">
                        <span>x/menit</span>
                    </div>
                    <div class="form-group col-md-5">
                        <input id="suhu" type="text" name="suhu" value="" class="form-control" placeholder="Suhu">
                    </div>
                    <div class="form-group col-md-1">
                    </div>

                    <div class="form-group col-md-5">
                        <input id="nadi_2" type="text" name="nadi_2" value="" class="form-control" placeholder="Nadi">
                    </div>
                    <div class="form-group col-md-1">
                        <span>x/menit</span>
                    </div>

                    <div class="form-group col-md-5">
                        <input id="penggunaan" type="text" name="penggunaan" value="" class="form-control" placeholder="Penggunaan 02">
                    </div>
                    <div class="form-group col-md-1">
                        <span>It/menit</span>
                    </div>

                    <div class="form-group col-md-5">
                        <input id="saturasi" type="text" name="saturasi" value="" class="form-control" placeholder="Saturasi 02">
                    </div>
                    <div class="form-group col-md-1">
                        <span>%</span>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="nyeri" name="nyeri" class="form-control select2">
                                <option value="" selected>Pilih Nyeri</option>
                                <?php
                                $list_nyeri = $this->Pasien_rd_model->get_reference_where('m_lookup','type','assesstment.nyeri');
                                foreach($list_nyeri as $list){
                                    echo "<option value='".$list->code."'>".$list->name."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="numeric_wong_baker" name="numeric_wong_baker" class="form-control select2">
                                <option value="" selected>Numeric/wong baker</option>
                                <?php
                                $list_nyeri = $this->Pasien_rd_model->get_reference_where('m_lookup','type','assesstment.numeric.wong');
                                foreach($list_nyeri as $list){
                                    echo "<option value='".$list->code."'>".$list->name."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        <input id="resiko" type="text" name="resiko_jatuh" value="" class="form-control" placeholder="Resiko Jatuh">
                    </div>

                    <table id="table_asessment_therapy" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Program Therapy</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Program Therapy</th>
                                <th>Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label">Pilih  Program Therapy<span style="color: red;"> *</span></label>
                            <select id="nama_program" name="nama_program" class="form-control select2" onchange="$('#nama_therapy').val($('#nama_program option:selected').text())" placeholder="Pilih Therapy">
                                <option value="#" selected>Pilih Therapy</option>
                                <?php
                                // $list_therapy = $this->list_phatologi_model->get_theraphy_pasien();
                                $list_therapy = $this->Pasien_rd_model->get_reference('m_program_therapy');
                                foreach($list_therapy as $list){
                                    echo "<option value='".$list->program_therapy_id."'>".$list->nama."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-2" style="margin-top:27px;">
                            <button type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" id="addTherapy"><i class="fa fa-plus"></i> TAMBAH</button>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <button type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" id="saveAssessment"><i class="fa fa-floppy-o"></i> SIMPAN ASSESSMENT</button>
                    </div>
                    <div class="form-group col-md-6">
                        <button style="width: 100%" type="button" class="btn btn-success" id="RiwayatAssestment">
                            RIWAYAT ASSESSMENT
                        </button>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tindakans">

                    <table id="table_tindakan_pasienrj" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Jumlah Jenis Obat</th>
                                <th>Jumlah Jenis Alkes</th>
                                <th>Dokter</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>Tgl.Tindakan</th>
                              <th>Nama Tindakan</th>
                              <th>Jumlah Tindakan</th>
                              <th>Jumlah Jenis Obat</th>
                              <th>Jumlah Jenis Alkes</th>
                              <th>Dokter</th>
                              <th>Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_tindakan" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message_tindakan"></p>
                        </div>
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="id_pasien" id="id_pasien" value="<?php echo $list_pasien->pasien_id; ?>">
                    <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php //echo $list_pasien->kelaspelayanan_id; ?>">
                    <input type="hidden" id="dokter_id_tindakan" name="dokter_id_tindakan" value="<?php // echo $list_pasien->id_M_DOKTER; ?>">

                    <?php
                        //if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                        //else{
                      ?>
                      <div class="col-md-12">
                        <div class="form-group col-md-4">
                            <label class="control-label">Kode Tindakan<span style="color: red;"> *</span></label>
                            <input id="kode_tindakan" name="kode_tindakan" class="form-control" placeholder="Kode Tindakan" required>
                            <!-- <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly> -->
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Nama Tindakan<span style="color: red;"> *</span></label>
                            <input id="nama_tindakan" name="nama_tindakan" class="form-control" placeholder="Nama Tindakan" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Jumlah Tindakan<span style="color: red;"> *</span></label>
                            <input id="jml_tindakan" name="jml_tindakan" class="form-control" min="1" value="" type="number" onkeyup="hitungHarga()" min="1" placeholder="Jumlah Tindakan" required>
                            <!-- <input type="hidden" id="harga_tindakan_satuan" name="harga_tindakan_satuan" value="0" readonly>
                            <input type="hidden" id="subtotal" name="subtotal" value="0" readonly>
                            <input type="hidden" id="totalharga" name="totalharga" value="0" readonly>
                            <input type="hidden" id="is_cyto" name="is_cyto" value="0" readonly> -->
                        </div>
                      </div>
                      <div class="col-md-12">
                          <div class="form-group col-md-4" style="margin-top:-25px;">
                              <label>&nbsp;</label>
                              <button style="width: 100%" type="button" class="btn btn-success" id="mapICD9keTindakan">
                                  <!-- <i class="fa fa-floppy-o"></i> -->
                                  Map ICD 9 Ke Tindakan
                              </button>
                          </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group col-md-4">
                            <label class="control-label">Kode Tindakan ICD 9<span style="color: red;"> *</span></label>
                            <input id="kode_tindakan_icd9" name="kode_tindakan_icd9" class="form-control" placeholder="Kode Tindakan ICD 9" required>
                            <!-- <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly> -->
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Nama Tindakan ICD 9<span style="color: red;"> *</span></label>
                            <input id="nama_tindakan_icd9" name="nama_tindakan_icd9" class="form-control" placeholder="Nama Tindakan ICD 9" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Jumlah Tindakan ICD 9<span style="color: red;"> *</span></label>
                            <input id="jml_tindakan_icd9" name="jml_tindakan_icd9" class="form-control" min="1" value="" type="number" onkeyup="hitungHarga()" min="1" placeholder="Jumlah Tindakan ICD 9" required>
                            <!-- <input type="hidden" id="harga_tindakan_satuan" name="harga_tindakan_satuan" value="0" readonly>
                            <input type="hidden" id="subtotal" name="subtotal" value="0" readonly>
                            <input type="hidden" id="totalharga" name="totalharga" value="0" readonly>
                            <input type="hidden" id="is_cyto" name="is_cyto" value="0" readonly> -->
                        </div>
                      </div>
                      <style>
                      .tbody-input{
                        margin: 0px;
                        padding: 3px 4px;
                        text-align: center;
                        width: 100%;
                      }

                      .tbody-input-left{
                        margin: 0px;
                        padding: 3px 4px;
                        text-align: left;
                        width: 100%;
                      }
                      </style>
                      <table id="table_tindakan_obat" class="table table-striped dataTable" cellspacing="0">
                          <thead>
                              <tr>
                                  <th width="70%">Obat</th>
                                  <th width="10%">Jumlah</th>
                                  <th width="10%">Satuan</th>
                                  <th width="10%">Hapus</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <td><input class="tbody-input-left" type="text" value=""></td>
                                <td><input class="tbody-input" type="number" value=""></td>
                                <td><input class="tbody-input" type="text" value=""></td>
                                <td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus obat" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td>
                              </tr>
                          </tbody>
                      </table>
                      <div class="form-group col-md-1" style="margin-top: -20px;padding: 0px; float:right;">
                          <button type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" id="addTindakanObat"><i class="fa fa-plus"></i> OBAT</button>
                      </div>
                      <br>
                      <table id="table_tindakan_bhp" class="table table-striped dataTable" cellspacing="0">
                          <thead>
                              <tr>
                                  <th width="70%">BHP</th>
                                  <th width="10%">Jumlah</th>
                                  <th width="10%">Satuan</th>
                                  <th width="10%">Hapus</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><input class="tbody-input-left" type="text" value=""></td>
                                  <td><input class="tbody-input" type="number" value=""></td>
                                  <td><input class="tbody-input"type="text" value=""></td>
                                  <td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus BHP" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td>
                              </tr>
                          </tbody>
                      </table>
                      <div class="form-group col-md-1" style="margin-top: -20px;padding: 0px; float:right;">
                          <button type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" id="addTindakanBHP"><i class="fa fa-plus"></i> BHP</button>
                      </div>
                      <br>
                      <div class="col-md-12">
                          <div class="form-group col-md-6">
                              <label class="control-label">Dokter <span style="color: red;"> *</span></label>
                              <div class="input-group">
                                  <input type="hidden" name="tindakan_dokter_id" id="tindakan_dokter_id" value="">
                                  <input type="text" class="form-control" id="tindakan_dokter_id" name="tindakan_dokter_id" placeholder="Pilih Dokter">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" title="Lihat List Dokter" data-toggle="modal" data-target="#modal_dokter_tindakan" onclick="dialogTindakanCariDokter()"><i class="fa fa-list"></i></button>
                                  </span>
                              </div>
                          </div>
                          <div class="form-group col-md-6">
                              <label class="control-label">User</label>
                              <div class="input-group">
                                  <input type="hidden" name="tindakan_user_id" id="tindakan_user_id" value="">
                                  <input type="text" class="form-control" id="tindakan_user_id" name="tindakan_user_id" placeholder="Pilih User">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" title="Lihat List User" data-toggle="modal" data-target="#modal_user_tindakan" onclick="dialogTindakanCariUser()"><i class="fa fa-list"></i></button>
                                  </span>
                              </div>
                          </div>
                      </div>


                      <div class="form-group col-md-3">
                          <button type="button" class="btn btn-success" style="width: 100%; margin-top: 26px;" id="saveTindakan"><i class="fa fa-plus"></i> TAMBAH</button>
                      </div>
                    <?php// } ?>

                    <?php echo form_close();?>
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="background">
                    <?php echo form_open('#',array('id' => 'fmCreateBackgroundPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                    <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="control-label">Keluhan saat masuk<span style="color: red;"> *</span></label>
                        <textarea id="keluhan" name="keluhan" class="form-control" style="min-height:75px !important;" placeholder="Silahkan isi Keluhan">
                          <?php
                          $t_pendaftaran = $this->Pasien_rd_model->get_t_pendaftaran($list_pasien->pendaftaran_id);
                          if(!empty($t_pendaftaran->catatan_keluhan)){
                            echo $t_pendaftaran->catatan_keluhan;
                          }
                          ?>
                        </textarea>
                    </div>
                    <table id="table_background" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Alergi</th>
                                <th>Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Alergi</th>
                                <th>Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>
                    <div class="form-group col-md-6">
                        <label class="control-label">Alergi<span style="color: red;"> *</span></label>
                        <select id="paket_operasi" name="paket_operasi" class="form-control" >
                            <option value="" disabled selected>Pilih Alergi</option>
                            <?php
                            // $list_therapy = $this->list_phatologi_model->get_theraphy_pasien();
                            $list_therapy = $this->Pasien_rd_model->get_reference('m_alergi');
                            foreach($list_therapy as $list){
                                echo "<option value='".$list->alergi_id."'>".$list->nama_alergi."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                            <label>&nbsp;</label>
                            <button style="width: 100%" type="button" class="btn btn-success" id="saveBackground">
                                <i class="fa fa-plus"></i> TAMBAH ALERGI
                            </button>
                    </div>
                    <?php echo form_close();?>
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="obat">
                    <table id="table_resep_pasienrd" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <!-- <th>Satuan</th>
                                <th>Signa</th> -->
                                <th>Batal / Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="6">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <!-- <th>Satuan</th> -->
                                <!-- <th>Signa</th> -->
                                <th>Batal / Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateResepPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delres" name="<?php echo $this->security->get_csrf_token_name()?>_delres" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="reseptur_id" name="reseptur_id">
                        <div class="row">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif3" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                <div>
                                    <p id="card_message3" class=""></p>
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Nama Obat<span style="color: red;"> *</span></label>
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Nama Obat" name="nama_obat" id="nama_obat" class="autocomplete" value="">
                                    <input type="hidden" name="id_jenis_barang" id="id_jenis_barang">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_reseptur" onclick="dialogObat()" type="button">  <i class="fa fa-bars"></i> </button >
                                        </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Jumlah Obat<span style="color: red;"> *</span></label>
                                <input type="number" min="1" class="form-control" id="qty_obat" name="qty_obat" placeholder="Jumlah Obat">
                                <input id="obat_id" type="hidden" name="obat_id" value="" readonly>
                                <input id="current_stok" type="hidden" name="current_stok" value="" readonly>
                           </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">Harga Jual<span style="color: red;"> *</span></label>
                                <input type="number" class="form-control" id="harga_jual" name="harga_jual" placeholder="Harga Jual">
                                <input id="harga_netto" type="hidden" name="harga_netto" value="">
                            </div>
                           <div class="form-group col-sm-3">
                                <label class="control-label">Satuan Obat<span style="color: red;"> *</span></label>
                                <select required class="form-control select" id="satuan_obat" name="satuan_obat">
                                    <option value="" disabled selected>Satuan Obat</option>
                                    <?php
                                        $list_sediaan = $this->Pasien_rd_model->get_sediaan_list();
                                        foreach ($list_sediaan as $list) {
                                            echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-control">
                                <input type="checkbox" name="checkbox_racikan" id="checkbox_racikan" onchange="$('#panel_racikan_obat').toggle();">
                                <label for="checkbox_racikan">Racikan</label>
                            </div>

                            <div class="panel panel-info" id="panel_racikan_obat" style="display: none">
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        <table class="table table-striped" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Obat</th>
                                                    <th>Jumlah</th>
                                                    <th>Sediaan</th>
                                                    <!-- <th>Satuan</th>
                                                    <th>Signa</th> -->
                                                    <th>Hapus</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table_racikan_pasienrd">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Obat</th>
                                                    <th>Jumlah</th>
                                                    <th>Sediaan</th>
                                                    <!-- <th>Satuan</th>
                                                    <th>Signa</th> -->
                                                    <th>Hapus</th>
                                                </tr>
                                            </tfoot>
                                        </table>

                                        <div class="alert alert-success alert-dismissable row" id="modal_notif4" style="display:none;">
                                            <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                            <div>
                                                <p id="card_message4" class=""></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="control-label">Obat<span style="color: red;"> *</span></label>
                                                <select required class="form-control select2" id="obat_racikan" name="obat_racikan">
                                                    <option value="" disabled selected>Obat</option>
                                                    <?php
                                                        $list_obat = $this->Pasien_rd_model->get_barang_stok_list();
                                                        // print_r($list_obat);
                                                        foreach ($list_obat as $list) {
                                                            echo "<option value=".$list->id_barang.">".$list->nama_barang." (".$list->kode_barang.")</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="control-label">Jumlah<span style="color: red;"> *</span></label>
                                                <input type="number" class="form-control" id="jumlah_racikan" name="jumlah_racikan" placeholder="Jumlah" value="0">
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <label class="control-label">Sediaan<span style="color: red;"> *</span></label>
                                                <select required class="form-control select" id="sediaan_racikan" name="sediaan_racikan">
                                                    <option value="" disabled selected>Sediaan</option>
                                                    <?php
                                                        $list_sediaan = $this->Pasien_rd_model->get_sediaan_list();
                                                        foreach ($list_sediaan as $list) {
                                                            echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="control-label">Keterangan</label>
                                                <textarea class="form-control" placeholder="Keterangan"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button style="background-color: #01c0c8;" type="button" class="btn btn-success pull-right" onclick="changeDataResepRacikan()"><i class="fa fa-floppy-o"></i> TAMBAH OBAT RACIKAN</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group col-sm-6" style="margin-top: 20px">
                                <label class="control-label">Signa<span style="color: red;"> *</span></label>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <input type="number" class="form-control" id="signa_1" name="signa_1" value="0">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <input type="number" class="form-control" id="signa_2" name="signa_2" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                      <div class="col-md-12" style="width:100%;">
                        <div class="form-group col-md-6">
                            <button style="width: 100%" type="button" class="btn btn-success" id="saveResep"><i class="fa fa-plus"></i> TAMBAH</button>
                        </div>
                        <div class="form-group col-md-6">
                            <button style="width: 100%" type="button" class="btn btn-primary" id="printreseptur" onclick="printReseptur()"><i class="fa fa-print"></i> PRINT</button>
                        </div>
                      </div>
                        <?php echo form_close();?>

                    <div class="clearfix"></div>
                </div>

            </div><br>
            <div class="col-md-5 float-right">
                <button style="width: 100%" type="button" class="btn btn-success" id="printResep"><i class="fa fa-floppy-o"></i> SIMPAN KESELURUHAN HASIL PEMERIKSAAN</button>
            </div>
        </div>

    </div>

</div>




<div id="modal_reseptur" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: 365px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Obat</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="table-responsive">
                   <table id="table_list_obat" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nama Obat</th>
                                <th>Satuan</th>
                                <th>Jenis Obat</th>
                                <th>Harga Jual</th>
                                <th>Jumlah Stok</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="6">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nama Obat</th>
                                <th>Satuan</th>
                                <th>Jenis Obat</th>
                                <th>Harga Jual</th>
                                <th>Jumlah Stok</th>
                                <th>Pilih</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div id="modal_dokter" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: 525px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Dokter</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="table-responsive">
                   <table id="table_list_dokter_rd" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nama Dokter</th>
                                <th>Kelompok Dokter</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="3">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>Nama Dokter</th>
                              <th>Kelompok Dokter</th>
                              <th>Pilih</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="modal_dokter_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: 525px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Dokter</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="table-responsive">
                   <table id="table_list_dokter_tindakan" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nama Dokter</th>
                                <th>Kelompok Dokter</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="3">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>Nama Dokter</th>
                              <th>Kelompok Dokter</th>
                              <th>Pilih</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="modal_user_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: 525px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Dokter</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="table-responsive">
                   <table id="table_list_user_tindakan" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nama Dokter</th>
                                <th>Kelompok Dokter</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="3">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>Nama Dokter</th>
                              <th>Kelompok Dokter</th>
                              <th>Pilih</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="modal_list_diagnosa" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: 525px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Diagnosa</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="table-responsive">
                   <table id="table_list_diagnosa" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                              <th>Kode Diagnosa</th>
                              <th>Nama Diagnosa</th>
                              <th>Pilih</th>
                          </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div id="modal_list_diagnosa_icd10" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: 525px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Diagnosa ICD 10</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="table-responsive">
                   <table id="table_list_diagnosa_icd10_rd" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                              <th>Kode Diagnosa ICD 10</th>
                              <th>Nama Diagnosa ICD 10</th>
                              <th>Pilih</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="2">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>Kode Diagnosa ICD 10</th>
                              <th>Nama Diagnosa ICD 10</th>
                              <th>Pilih</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/pasien_rd/periksa_pasienrd.js"></script>
</body>

</html>
