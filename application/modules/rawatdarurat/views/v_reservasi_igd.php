<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
<style>
    .panel-info .panel-heading{
        background-color: #42bec8 !important;
        border-color: #42bec8 !important;
    }
    .ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
</style>
<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Reservasi IGD</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <!-- <li><a href="#">Dashboard</a></li> -->
                        <li><a href="#">Pendaftaran</a></li>
                        <li class="active">Reservasi IGD</li>
                    </ol>
                </div>
            </div>
            <div class="row">
              <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                  <button type="button" class="close" data-dismiss="alert" >&times;</button>
                  <div >
                      <p id="card_message"></p>
                  </div>
              </div>
                <div class="col-md-4 ">
                    <div class="panel panel-info bg">
                        <div class="panel-heading"> List Data Reservasi IGD </div>
                        <div class="panel-wrapper collapse in " aria-expanded="true"  >
                            <div class="panel-body" style=" padding:8px !important;height: 490px !important;"  >
                                <div class="row">
                                    <div class="inline-box">
                                        <div class="form-group col-md-9">
                                            <!-- <label align="center">Tanggal</label>     -->
                                            <div class="input-group">
                                                <input onkeydown="return false" name="tgl_res_awal" id="tgl_res_awal" type="text" class="form-control mydatepicker2" value="<?php echo date('d M Y'); ?>" placeholder="mm/dd/yyyy"> <span  class="input-group-addon"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>
                                        </div>
                                    </div>
                                </div><hr style="margin-top: -6px">
                                <div class="row" style="padding: 18px">
                                    <div class="col-md-12">
                                    <ul class="nav customtab nav-tabs" role="tablist" >
                                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#HADIR" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> HADIR</span></a></li>
                                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#BATAL" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">BATAL</span></a></li>
                                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#TOLAK" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">TOLAK</span></a></li>
                                        </ul>
                                        <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="HADIR" >
                                            <select class="form-control" id="change_option" name="change_option" style="margin-bottom: 19px" >
                                                <option value="" disabled selected>Cari Berdasarkan</option>
                                                <option value="nama_pasien">Nama Pasien</option>
                                                <option value="no_rekam_medis">No.Rekam Medis</option>
                                            </select>
                                            <input type="text" class="form-control pilih" placeholder="Cari berdasarkan yang anda pilih ..." style="width: 100%;margin-top: -19px;margin-bottom: 16px;" onkeyup="cariPasien()">

                                            <div class="comment-center hadir-reservasi" style="max-height: 236px;overflow: auto;">

                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="BATAL">
                                            <select class="form-control" id="change_option_batal" name="change_option_batal" style="margin-bottom: 19px" >
                                                <option value="" disabled selected>Cari Berdasarkan</option>
                                                <option value="nama_pasien_batal">Nama Pasien</option>
                                                <option value="no_rekam_medis_batal">No.Rekam Medis</option>
                                            </select>
                                            <input type="text" class="form-control pilihbatal" placeholder="Cari berdasarkan yang anda pilih ..." style="width: 100%;margin-top: -19px;margin-bottom: 16px;" onkeyup="cariPasien()">

                                            <div class="comment-center batal-reservasi" style="max-height: 236px;overflow: auto;">
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="TOLAK">
                                            <select class="form-control" id="change_option_tolak" name="change_option_tolak" style="margin-bottom: 19px" >
                                                <option value="" disabled selected>Cari Berdasarkan</option>
                                                <option value="nama_pasien_tolak">Nama Pasien</option>
                                                <option value="no_rekam_medis_tolak">No.Rekam Medis</option>
                                            </select>
                                            <input type="text" class="form-control pilihtolak" placeholder="Cari berdasarkan yang anda pilih ..." style="width: 100%;margin-top: -19px;margin-bottom: 16px;" onkeyup="cariPasien()">

                                            <div class="comment-center tolak-reservasi" style="max-height: 236px;overflow: auto;">
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <?php echo form_open('#',array('id' => 'fmCreateReservasiIGD', 'data-toggle' => 'validator'))?>
                    <div class="panel panel-info bg">
                        <div class="panel-heading"> Form Data Pasien Reservasi IGD</div>
                        <div class="white-box">
                            <div class="row">
                                <div class="form-group col-md-6" >
                                    <label for="no_rm" class="control-label">Nomor Rekam Medis<span style="color: red;">*</span></label>
                                    <input type="text" id="cari_no_rekam_medis" name="cari_no_rekam_medis" class="form-control autocomplete" placeholder=" Cari Nomor Rekam Medis....." >
                                    <input type="checkbox" name="cek_pasien" id="cek_pasien" style=""><span for="cek_pasien" style="color: red"> * centang jika pasien baru</span>
                                    <input type="hidden" name="no_rm" id="no_rm" class="form-control">
                                    <input type="hidden" name="is_checked" id="is_checked" class="form-control">

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword" class="control-label">Nama Lengkap<span style="color: red;">*</span></label>
                                    <input type="text" id="pasien_nama" name="pasien_nama" class="form-control" placeholder="Nama Lengkap" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="tgl_lahir" class="control-label">Tanggal Reservasi<span style="color: red;">*</span></label>
                                    <div class="input-group">
                                        <input name="tgl_reservasi" id="tgl_reservasi" type="text" class="form-control datepick" placeholder="mm/dd/yyyy" value="<?php
                                        // date_default_timezone_set('Asia/Jakarta');
                                        // setlocale(LC_ALL, 'IND');
                                        // setlocale (LC_TIME, 'id_ID');
                                        // setlocale(LC_ALL, 'id_ID');
                                        setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID');
                                        $date = strftime( "%d %B %Y ", strtotime(date('d F Y')));
                                            echo $date;
                                            ?>" > <span class="input-group-addon"><i class="icon-calender"></i></span>
                                        <input type="hidden" name="is_tgl_res" id="is_tgl_res">
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Jam Periksa<span style="color: red;">*</span></label>
                                    <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                        <input type="text" name="jam_periksa" id="jam_periksa" class="form-control" value="<?= date("H:i") ?>"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nama Perujuk<span style="color: red;">*</span></label>
                                    <div class="input-group custom-search-form">
                                        <input type="text" id="rm_namaperujuk" name="rm_namaperujuk" class="form-control" placeholder="Nama Perujuk" readonly>
                                        <input type="hidden" name="id_perujuk" id="id_perujuk">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info" title="Lihat Data Perujuk" data-toggle="modal" data-target="#modal_list_rujuk"  data-backdrop="static" data-keyboard="false" type="button" onclick="dialogPerujuk()"> <i class="fa fa-bars"></i> </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nomor Telepon Perujuk</label>
                                    <div class="input-group custom-search-form">
                                        <input type="text" id="rm_noteleponperujuk" name="rm_noteleponperujuk" class="form-control" placeholder="Nomor Telepon Perujuk" readonly>
                                        <!-- <input type="hidden" name="is_notelp_edited" id="is_notelp_edited"> -->
                                        <span class="input-group-btn">
                                            <button class="btn btn-info" title="Edit Perujuk" data-toggle="modal" data-target="#modal_edit_no_telp_perujuk" data-backdrop="static" data-keyboard="false" type="button" id="buttonEditTeleponPerujuk" style="display:none;"> <i class="fa fa-edit"></i> </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Alamat Perujuk<span style="color: red;">*</span></label>
                                    <textarea id="rm_alamatperujuk" name="rm_alamatperujuk" class="form-control" placeholder="Alamat Perujuk" readonly></textarea>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 42px">
                                <div class="col-md-12">
                                <button type="button" class="btn btn-info pull-right" id="btnSaveReservasiIGD"><i class="fa fa-floppy-o"></i> &nbsp;DAFTAR</button>
                                <button style="margin-right: 8px" type="button" class="btn btn-danger pull-right" onclick="reload()"><i class="fa fa-refresh"></i> &nbsp;RESET</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_list_rujuk" class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-large">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">List Data Perujuk</h2> </div>
                <div class="modal-body" style="height:500px;overflow: auto;">
                    <div class="table-responsive">
                        <table id="table_list_rujuk" class="table table-striped dataTable no-footer" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Perujuk</th>
                                    <th>Nama Perujuk</th>
                                    <th>Alamat Kantor</th>
                                    <th>No Telp/HP</th>
                                    <th>Nama Marketing</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal edit no telp perujuk reservasi pasien -->
    <div id="modal_edit_no_telp_perujuk" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
          <div class="modal-dialog modal-medium" style="min-width: 700px !important">
              <div class="modal-content" style="margin-top: 100px;">
                  <div class="modal-header" style="background: #fafafa">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h2 class="modal-title" id="myLargeModalLabel"><b>Edit Nomor Telepon Perujuk</b></h2>
                   </div>
                  <div class="modal-body" style="background: #fafafa;overflow: auto;">
                      <div class="col-sm-12">
                              <!-- <div class="white-box"> -->
                                    <?php echo form_open('#',array('id' => 'fmEditTeleponPerujukReservasiIGD', 'class' => 'form-inline'))?>
                                    <div class="col-sm-12">
                                      <div class="row">
                                        <div class="col-sm-7">
                                          <input type="hidden" id="ci_csrf_token_del" name="ci_csrf_token_del" value="">
                                          <input type="hidden" id="id_perujuk" name="id_perujuk">
                                          <input type="text" id="rm_noteleponperujuk_edit" name="rm_noteleponperujuk_edit" class="form-control" placeholder="Nomor Telepon Perujuk" style="width: 100%;">
                                        </div>
                                        <div class="col-sm-5">
                                          <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonSimpanTeleponPerujuk" style="margin-left : 10px;" onclick="simpanTeleponPerujuk()"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                                          <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-left : 10px;"><i class="fa fa-times p-r-10"></i>BATAL</button>
                                        </div>
                                      </div>
                                    </div>
                                    <?php echo form_close(); ?>
                              <!-- </div> -->
                        </div>
                  </div>

              </div>
              <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- modal tolak reservasi pasien -->
    <div id="modal_tolak_reservasi_igd" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
        <div class="modal-dialog modal-medium">
            <div class="modal-content" style="margin-top: 200px;">
            <?php echo form_open('#',array('id' => 'fmTolakReservasiIGD'))?>

                <div class="modal-header" style="background: #fafafa">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel"><b>Tolak Reservasi IGD</b></h2>
                 </div>
                <div class="modal-body" style="background: #fafafa;overflow: auto;">
                    <div class="col-sm-12">
                            <div class="white-box">
                                <div class="row">
                                  <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                                      <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                      <div >
                                          <p id="card_message"></p>
                                      </div>
                                  </div>
                                <div class="col-sm-12">
                                    <!-- <form data-toggle="validator"> -->
                                        <div class="form-group" >
                                          <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                                          <input id="reservasi_igd_id" type="hidden" name="reservasi_igd_id" readonly>
                                          <label for="reservasi_igd_alasan" class="control-label">Alasan<span style="color: red;">*</span></label>
                                          <select class="form-control selectpicker" id="reservasi_igd_alasan" name="reservasi_igd_alasan">
                                            <option value="" disabled selected>Pilih Alasan</option>
                                               <?php
                                                   foreach($list_alasan as $name=>$description){
                                                       echo "<option value='".$name."'>".$description."</option>";
                                                   }
                                               ?>
                                           </select>
                                        </div>
                                        <div class="form-group">
                                           <label class="control-label">Catatan</label>
                                           <textarea id="reservasi_igd_catatan" name="reservasi_igd_catatan" class="form-control" style="min-height:175px !important;"></textarea>
                                           <span class="help-block with-errors" style="color: red; font-size: 10px;">*Optional(jika diperlukan)</span>
                                        </div>
                                </div>
                              </div>
                              <div class="col-md-12">
                                  <!-- <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" aria-hidden="true">BATAL</button>
								   onclick="tolakReservasiIGD()
								  -->
                                  <button type="button" class="btn btn-success pull-right" id="btnTolakReservasiIGD"><i class="fa fa-floppy-o p-r-10"></i>TOLAK</button>
                                  <button style="margin-right: 8px" type="button" class="btn btn-danger pull-right" data-dismiss="modal" ><i class="fa fa-times p-r-10"></i>BATAL</button>
                              </div>
                              <br>
                            </div>
                        </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php $this->load->view('footer');?>
