<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_rd_model extends CI_Model {
    var $column = array('no_pendaftaran','tgl_pendaftaran','poliruangan_id','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa');
    var $order = array('tgl_pendaftaran' => 'ASC');

//    var $column1 = array('obat_id','nama_obat', 'satuan', 'jenisoa_nama','harga_jual','qtystok');
  //  var $order1 = array('nama_obat' => 'ASC');

    var $column1 = array('kode_barang');
    var $order1 = array('nama_barang' => 'ASC');

    var $columnListDokter = array('NAME_DOKTER');
    var $orderListDokter = array('NAME_DOKTER' => 'ASC');
    var $columnListDiagnosa = array('diagnosa_kode');
    var $orderListDiagnosa = array('diagnosa_kode' => 'ASC');
    var $columnListDiagnosaICD10 = array('kode_diagnosa');
    var $orderListDiagnosaICD10 = array('kode_diagnosa' => 'ASC');

    public function __construct(){
        parent::__construct();
    }

    public function delete_periksa($pendaftaran_id){
      $datas = array(
          'status_periksa' => 'Batal Periksa',
          'status_pasien' => 0
      );
      $update = $this->db->update('t_pendaftaran', $datas, array('pendaftaran_id' => $pendaftaran_id));
      return $update;
    }


    public function get_pasienrd_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran, $dokter_id){
        $this->db->from('v_pasien_rd');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poliruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    function count_all($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran, $dokter_id){
        $this->db->from('v_pasien_rd');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($poliklinik)){
            $this->db->where('poliruangan_id', $poliklinik);
        }
        if(!empty($status)){
            $this->db->like('status_periksa', $status);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        return $this->db->count_all_results();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_pasien_rd', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();
    }

    public function get_t_pendaftaran($pendaftaran_id){
        $query = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();
    }

    public function get_reference($table){
        $this->db->from($table);
    }
    
    public function get_pendaftran_resep_pasien($pendaftaran_id){
        $this->db->select('t_pendaftaran.*,  m_pasien.*,  m_instalasi.*,  m_dokter.*');
        $this->db->from('t_pendaftaran');
        $this->db->join('m_pasien',' t_pendaftaran.pasien_id = m_pasien.pasien_id');
        $this->db->join('m_instalasi','m_instalasi.instalasi_id = t_pendaftaran.instalasi_id');
         $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_pendaftaran.dokter_id');
        $this->db->where('t_pendaftaran.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_reference_where($table, $column, $value){
        // $this->db->select('*');
        // $this->db->from($table);
        // $this->db->where($column);
        $query = $this->db->get_where($table, array($column => $value),100,0);
        return $query->result();
    }

    public function get_detail_resep_pasien($reseptur_id){
        $this->db->select('t_resepturpasien.*,  m_jenis_barang_farmasi.*,  m_sediaan_obat.*,  t_resepturpasien_detail_racikan.*,  t_pembayarankasir.*');
        $this->db->from('t_resepturpasien');
        $this->db->join('m_jenis_barang_farmasi',' m_jenis_barang_farmasi.id_jenis_barang = t_resepturpasien.id_jenis_barang');
        $this->db->join('m_sediaan_obat',' m_sediaan_obat.id_sediaan = t_resepturpasien.id_sediaan');
        $this->db->join('t_resepturpasien_detail_racikan','t_resepturpasien_detail_racikan.reseptur_id = t_resepturpasien.reseptur_id');
        $this->db->join('t_pembayarankasir','t_pembayarankasir.pembayarankasir_id = t_resepturpasien.pembayarankasir_id');
        $this->db->where('t_resepturpasien.pendaftaran_id',$reseptur_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }


    public function sum_total_resep($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_resepturpasien');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }




    public function get_dokter_list(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('instalasi_id', 2); //IGD
        $this->db->group_by('id_M_DOKTER');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_diagnosa_pasien_list($pendaftaran_id){
        $sql = "SELECT t_diagnosapasien.*,d1.NAME_DOKTER nama_dokter_1,d2.NAME_DOKTER nama_dokter_2,d3.NAME_DOKTER nama_dokter_3 FROM t_diagnosapasien
                LEFT JOIN m_dokter AS d1 ON t_diagnosapasien.dokter_id  = d1.id_M_DOKTER
                LEFT JOIN m_dokter AS d2 ON t_diagnosapasien.dokter_id_2  = d2.id_M_DOKTER
                LEFT JOIN m_dokter AS d3 ON t_diagnosapasien.dokter_id_3  = d3.id_M_DOKTER
                where pendaftaran_id = ".$pendaftaran_id;
                // where pendaftaran_id = ".$pendaftaran_id;
        $this->db->query($sql)->result();
        // $this->db->select('t_diagnosapasien.*,m_dokter.NAME_DOKTER');
        // $this->db->from('t_diagnosapasien');
        // $this->db->join('m_dokter','t_diagnosapasien.dokter_id = m_dokter.id_M_DOKTER');
        // $this->db->where('pendaftaran_id',$pendaftaran_id);
        // $length = $this->input->get('length');
        // if($length !== false){
        //     if($length != -1) {
        //         $this->db->limit($this->input->get('length'), $this->input->get('start'));
        //     }
        // }

        // $query = $this->db->get();

        // return $query->result();
        return $this->db->query($sql)->result();
    }

    public function get_background_pasien_list($pendaftaran_id){
        $this->db->select('t_alergi_pasien.*, m_alergi.nama_alergi');
        $this->db->from('t_alergi_pasien');
        $this->db->join('m_alergi','m_alergi.alergi_id = t_alergi_pasien.alergi_id');
        $this->db->where('t_alergi_pasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_assestment_pasien_list($pendaftaran_id){
        $this->db->select('t_assestment_pasien.*, t_assestment_therapy_pasien.program_therapy_id');
        $this->db->from('t_assestment_pasien');
        $this->db->join('t_assestment_therapy_pasien','t_assestment_therapy_pasien.assestment_pasien_id = t_assestment_pasien.assestment_pasien_id','left');
        $this->db->join('m_program_therapy','m_program_therapy.program_therapy_id = t_assestment_therapy_pasien.program_therapy_id','left');
        $this->db->where('t_assestment_pasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_reseptur_pasien_list($pendaftaran_id){
        $this->db->select('t_resepturpasien.*, m_det_racikan_barang_farmasi');
        $this->db->from('t_resepturpasien');
        $this->db->join(' m_det_racikan_barang_farmasi',' m_det_racikan_barang_farmasi.sediaan_obat_id = t_resepturpasien.sediaan_obat_id');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_pasien_list($pendaftaran_id){
       $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }


    public function get_tindakan_pasien_list_permintaan($pendaftaran_id){
       $this->db->select('t_det_reservasi_ri.*, m_tindakan.daftartindakan_nama');
        $this->db->from('t_det_reservasi_ri');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_det_reservasi_ri.tindakan_id');
        $this->db->where('t_det_reservasi_ri.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

      public function get_permintaan_tindakan($poliruangan_id){
        $this->db->select('m_tindakan.*');
        $this->db->from('m_tindakan');
        $this->db->join('m_kelaspelayanan', 'm_kelaspelayanan.kelaspelayanan_id = m_tindakan.kelaspelayanan_id');
        $this->db->join('m_kelaspoliruangan', 'm_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id',$poliruangan_id);

        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan($kelaspelayanan_id){
        $this->db->from('m_tindakan');
        $this->db->where('kelaspelayanan_id',$kelaspelayanan_id);
        $this->db->where('kelompoktindakan_id',3);

        $query = $this->db->get();

        return $query->result();
    }


    public function get_tarif_tindakan($daftartindakan_id){
        $query = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $daftartindakan_id), 1, 0);

        return $query->row();
    }

    // Paket Function Periksa
    public function insert_diagnosapasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_diagnosapasien',$data);

        return $insert;
    }

    public function update_to_sudahperiksa($id){
        $data = array(
            'status_periksa' => 'Sudah Periksa'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));
        return $update;
    }

    public function insert_assessment($data=array()){
        $insert = $this->db->insert('t_diagnosapasien',$data);
        return $insert;
    }

    public function insert_tindakanpasien($pendaftaran_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_tindakanpasien', $data);

        return $insert;
    }

    public function insert_ajukan_permintaan($data){
        $insert = $this->db->insert('t_reservasi_ri', $data);
        if ($insert) {
            $this->db->select("reservasi_ri_id");
            $this->db->from("t_reservasi_ri");
            $this->db->order_by("reservasi_ri_id", "desc");
            $this->db->limit(1);
            $data = $this->db->get()->row();
            return $data->reservasi_ri_id;
        }
        return 0;
    }

    public function insert_penunjangrd($data=array()){
        $insert = $this->db->insert('t_pasienmasukpenunjang',$data);
        if ($insert) {
            $this->db->select("pasienmasukpenunjang_id");
            $this->db->from("t_pasienmasukpenunjang");
            $this->db->order_by("pasienmasukpenunjang_id", "desc");
            $this->db->limit(1);
            $data = $this->db->get()->row();
            return $data->pasienmasukpenunjang_id;
        }
        return 0;
    }

    public function insert_assestment($data=array()){
        $insert = $this->db->insert('t_assestment_pasien',$data);
        if ($insert) {
            $this->db->select("assestment_pasien_id");
            $this->db->from("t_assestment_pasien");
            $this->db->order_by("assestment_pasien_id", "desc");
            $this->db->limit(1);
            $data = $this->db->get()->row();
            return $data->assestment_pasien_id;
        }
        return 0;
    }



    public function insert_tindakanpasienpermintaan($data=array()){
       // $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_det_reservasi_ri', $data);
        return $insert;
    }

    public function insert_labpenunjang($data=array()){
       // $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_tindakan_lab', $data);
		if ($insert) {
            $this->db->select("tindakan_lab_id");
            $this->db->from("t_tindakan_lab");
            $this->db->order_by("tindakan_lab_id", "desc");
            $this->db->limit(1);
            $data = $this->db->get()->row();
            return $data->tindakan_lab_id;
        }
        return 0;
    }

    public function insert_labpenunjangdet($data=array()){
       // $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_det_tindakan_lab', $data);

        return $insert;
    }

    public function insert_radpenunjang($data=array()){
       // $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_tindakanradiologi', $data);

        return $insert;
    }

    public function insert_detassestment($data=array()){
       // $this->update_to_sudahperiksa($pendaftaran_id);
        $insert = $this->db->insert('t_assestment_therapy_pasien', $data);

        return $insert;
    }

    // Paket Function Periksa
    public function delete_diagnosa($id){
        $delete = $this->db->delete('t_diagnosapasien', array('diagnosapasien_id' => $id));

        return $delete;
    }

    public function delete_alergi($id){
        $delete = $this->db->delete('t_alergi_pasien', array('alergi_pasien_id' => $id));

        return $delete;
    }

    public function delete_assessment($id){
        $delete = $this->db->delete('t_assestment_pasien', array('assessment_pasien_id' => $id));

        return $delete;
    }

    public function delete_tindakan($id){
        $delete = $this->db->delete('t_tindakanpasien', array('tindakanpasien_id' => $id));

        return $delete;
    }

    public function delete_background($id){
        $delete = $this->db->delete('t_alergi_pasien', array('alergi_pasien_id' => $id));

        return $delete;
    }

    public function delete_reseptur_obat($id){
        $delete = $this->db->delete('t_resepturpasien', array('reseptur_id' => $id));

        return $delete;
    }

    public function get_sediaan_list(){
        return $this->db->get_where('m_sediaan_obat')->result();
    }


    //insert to apotek keluar
    public function insert_resep_to_apotek($data=array()){
        $this->db->insert('t_apotek_keluar',$data);
        $id = $this->db->insert_id();
        return $id;
    }

     public function get_resep_pasien_list($pendaftaran_id){
        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_resepturpasien.id_sediaan','left');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_resepturpasien.id_jenis_barang','left');
        $this->db->from('t_resepturpasien');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('v_stokobatalkes');
        $this->db->like('nama_obat', $val);
        $this->db->where('qtystok !=', 0);
        $query = $this->db->get();

        return $query->result();
    }

    public function insert_reseppasien($data=array()){
        $this->db->insert('t_resepturpasien',$data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function insert_reseppasien_racikan($data=array()) {
        $this->db->insert("t_resepturpasien_detail_racikan", $data);
    }

    public function insert_stokobatalkes_keluar($data=array()){
        $stok = $this->db->insert('t_stokobat',$data);

        return $stok;
    }

    public function delete_resep($id){
        $delete = $this->db->delete('t_resepturpasien', array('reseptur_id' => $id));

        return $delete;
    }

    public function delete_stok_resep($id){
        $stok = $this->db->delete('t_stokobat', array('penjualandetail_id' => $id));

        return $stok;
    }

    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_INAP));

        return $query->result();
    }

     function get_ruangan_byinstalasi($instalasi_id){
        $this->db->select('*');
        $this->db->join('m_instalasi','m_poli_ruangan.instalasi_id = m_instalasi.instalasi_id','left');
        $this->db->from('m_poli_ruangan');
        $this->db->order_by('m_poli_ruangan.nama_poliruangan', 'ASC');
        $this->db->where("m_poli_ruangan.instalasi_id", $instalasi_id);
        $query = $this->db->get();

        return $query->result();
    }

    function get_kelas_poliruangan($poliruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $poliruangan_id);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_kamar_aktif($ruangan_id, $kelasruangan){
        $this->db->order_by('no_kamar', 'ASC');
        $query = $this->db->get_where('m_kamarruangan', array('poliruangan_id' => $ruangan_id, 'kelaspelayanan_id' => $kelasruangan, 'status_aktif' => '1'));

        return $query->result();
    }



       public function insert_admisi($data=array()){
        $insert = $this->db->insert('t_pasienadmisi',$data);
        if ($insert) {
            $this->db->select("pasienadmisi_id");
            $this->db->from("t_pasienadmisi");
            $this->db->order_by("pasienadmisi_id", "desc");
            $this->db->limit(1);
            $data = $this->db->get()->row();
            return $data->pasienadmisi_id;
        }
        return 0;
    }
    public function insert_kamar($data){
        $insert = $this->db->insert('t_masukkamar',$data);
        return $insert;
    }

    public function update_kamar($data, $id){
        $update = $this->db->update('m_kamarruangan', $data, array('kamarruangan_id' => $id));

        return $update;
    }

    public function update_pendaftaran($data, $id){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    public function update_to_penunjang($id){
        $data = array(
            'status_periksa' => 'SEDANG DI PENUNJANG'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }
     public function get_last_pendaftaran_pasien($id){
        $q = $this->db->get_where('m_pasien', array('pasien_id' => $id));
        return $q->row();
    }
    public function insert_permintaan_rawat($data){
        $insert = $this->db->insert('t_reservasi_ri',$data);
        return $insert;
    }

   public function update_to_permintaan_rawat($id){
        $data = array(
            'status_periksa' => 'SEDANG PERMOHONAN RAWAT INAP'
        );
        $this->db->where("pendaftaran_id", $id);
        $update = $this->db->update('t_pendaftaran', $data);
        return $update;
    }

    public function get_list_penunjang(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => 4));

        return $query->result();
    }

    function get_paketoperasi(){
        $this->db->from('m_paketoperasi');
        $query = $this->db->get();

        return $query->result();
    }

        function get_datalab(){
        $this->db->from('m_tindakan_lab');
        //$query = $this->db->where('tindakan_lab_nama');
        $query = $this->db->get();

        return $query->result();
    }

    function get_datarad(){
        $this->db->from('m_tindakan');
        //$query = $this->db->where('tindakan_lab_nama');
        $query = $this->db->get();

        return $query->result();
    }


    public function get_dokter_anastesi(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    public function get_dokter_umum(){
        $this->db->from('m_dokter');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    public function get_rencana_tindakan_pasien(){
        $this->db->from('m_tindakan');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    public function get_look_pasien(){
        $this->db->from('m_lookup');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

     public function get_alasan_rujuk(){
        $this->db->from('m_alasan_rujuk');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    public function get_theraphy_pasien(){
        $this->db->from('m_program_therapy');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

     public function get_rencana_tindakan(){
        $this->db->from('m_rencana_tindakan');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }


    public function get_kamar_semua(){

        $this->db->from('m_poli_ruangan');
        // $this->db->select("a.*, b.*");
        // $this->db->from('m_kamarruangan a');
        // $this->db->join("m_poli_ruangan b", "a.poliruangan_id = b.poliruangan_id");
        // //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
         $query = $this->db->get();

        return $query->result();
    }

    public function get_perawat(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_PERAWAT); //Perawat
        $query = $this->db->get();

        return $query->result();
    }

    function get_no_pendaftaran_ri(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "RI".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pendaftaran
                        WHERE no_pendaftaran LIKE ('RI".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pendaftaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pendaftaran;
    }

    function get_kelas_paket_operasi($paketoperasi){
        $query = $this->db->get_where('m_paketoperasi', array('paketoperasi_id' => $paketoperasi), 1, 0);
        $listpaket = $query->result();

        $kelaspelayanan = $listpaket[0]->kelaspelayanan_id;

        return $kelaspelayanan;
    }

    public function insert_pendaftaran($data){
        $this->db->insert('t_pendaftaran',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }

    public function insert_tindakanpasien_po($data=array()){
        $insert = $this->db->insert('t_tindakanpasien',$data);
        return $insert;
    }

    public function get_tindakan_paket($paketoperasi_id){
        $query = $this->db->get_where('m_paketoperasidetail', array('paketoperasi_id' => $paketoperasi_id));

        return $query->result();
    }

    public function insert_tindakan_paket($paketoperasi_id, $pendaftaran_id, $pasien_id, $dokter_id, $dokteranastesi_id, $perawat_id){
        $tindakan = $this->get_tindakan_paket($paketoperasi_id);
        $statusinsert = false;

        foreach($tindakan as $list){
            $tariftindakan = $this->get_tarif_tindakan($list->tariftindakan_id);
            $hargatindakan = intval($tariftindakan->harga_tindakan) - intval($list->subsidi);
            $total_harga = intval($hargatindakan) * intval($list->jumlah);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'tariftindakan_id' => $list->tariftindakan_id,
                'jml_tindakan' => $list->jumlah,
                'total_harga_tindakan' => $total_harga,
                'is_cyto' => '0',
                'total_harga' => $total_harga,
                'dokter_id' => $dokter_id,
                'tgl_tindakan' => date('Y-m-d'),
                'dokteranastesi_id' => $dokteranastesi_id,
                'perawat_id' => $perawat_id,
                'petugas_id' =>$this->data['users']->id,
                'is_paket' => '1'
            );
            $inserttindakan = $this->insert_tindakanpasien_po($data_tindakanpasien);

            if($inserttindakan){
                $statusinsert = true;
            }
        }

        return $statusinsert;
    }

    public function get_obat_paket($paketoperasi_id){
        $query = $this->db->get_where('m_paketoperasiobat', array('paketoperasi_id' => $paketoperasi_id));

        return $query->result();
    }

    public function get_obat_stok($id){
        $q = $this->db->get_where('v_stokobatalkes', array('obat_id' => $id));
        return $q->row();
    }

    public function insert_obat_paket($paketoperasi_id, $pendaftaran_id, $pasien_id, $dokter_id){
        $obat = $this->get_obat_paket($paketoperasi_id);
        $statusinsert = false;

        foreach($obat as $list){
            $obatstok = $this->get_obat_stok($list->obat_id);
            if(count($obatstok) > 0){
                $data_reseppasien = array(
                    'obat_id' => $list->obat_id,
                    'qty' => $list->jumlah_obat,
                    'harga_netto' => $obatstok->harga_modal,
                    'harga_jual' => $obatstok->harga_jual - $list->subsidi_obat,
                    'satuan' => $obatstok->satuan,
                    'signa' => null,
                    'pendaftaran_id' => $pendaftaran_id,
                    'pasien_id' => $pasien_id,
                    'dokter_id' => $dokter_id,
                    'tgl_reseptur' => date('Y-m-d'),
                );
                $ins = $this->insert_reseppasien($data_reseppasien);
                if($ins){
                    $updatestok = array(
                        'penjualandetail_id' => $ins,
                        'obat_id' => $list->obat_id,
                        'tglstok_out' => date('Y-m-d'),
                        'qtystok_out' => $list->jumlah_obat,
                        'create_time' => date('Y-m-d H:i:s'),
                        'create_user' => $this->data['users']->id
                    );

                    $inst_update = $this->insert_stokobatalkes_keluar($updatestok);

                    if($inst_update){
                        $statusinsert = true;
                    }
                }
            }else{
                $statusinsert = true;
            }
        }

        return $statusinsert;
    }

	function get_list_obat(){
		// $this->db->select('*');
        //$this->db->from('v_stokobatalkes');
        // $this->db->where('qtystok !=', 0);

        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_apotek_stok.id_sediaan');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_apotek_stok.id_jenis_barang');
        $this->db->from('t_apotek_stok');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
	}

  function get_list_dokter(){
    // $this->db->select('*');
        //$this->db->from('v_stokobatalkes');
        // $this->db->where('qtystok !=', 0);
        //SELECT name_dokter, kelompokdokter_nama FROM `m_dokter` JOIN m_kelompokdokter ON m_dokter.kelompokdokter_id = m_kelompokdokter.kelompokdokter_id
        $this->db->select('id_M_DOKTER, NAME_DOKTER, kelompokdokter_nama');
        $this->db->join('m_kelompokdokter','m_dokter.kelompokdokter_id = m_kelompokdokter.kelompokdokter_id');
        $this->db->from('m_dokter');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->columnListDokter as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnListDokter[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->orderListDokter)){
            $orderListDokter = $this->orderListDokter;
            $this->db->order_by(key($orderListDokter), $orderListDokter[key($orderListDokter)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
  }

  function get_list_diagnosa(){
        // $this->db->select('*');
        $this->db->select('diagnosa2_id,diagnosa_kode,diagnosa_nama');
        // $this->db->select('diagnosa2_id,diagnosa_kode,diagnosa_nama,kode_diagnosa,nama_diagnosa');
        // $this->db->join('m_diagnosa_icd10','m_diagnosa.diagnosa2_id = m_diagnosa_icd10.diagnosa_id');
        $this->db->from('m_diagnosa');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->columnListDiagnosa as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnListDiagnosa[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->orderListDokter)){
            $orderListDiagnosa = $this->orderListDokter;
            $this->db->order_by(key($orderListDiagnosa), $orderListDiagnosa[key($orderListDiagnosa)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
  }

  function get_list_diagnosa_icd10(){
        $this->db->select('diagnosa_id,kode_diagnosa,nama_diagnosa');
        $this->db->from('m_diagnosa_icd10');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->columnListDiagnosaICD10 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnListDiagnosaICD10[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->orderListDokter)){
            $orderListDiagnosaICD10 = $this->orderListDokter;
            $this->db->order_by(key($orderListDiagnosaICD10), $orderListDiagnosaICD10[key($orderListDiagnosaICD10)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
  }

		function get_obat_by_id($id){
            $this->db->select('*');
            $this->db->from('t_apotek_stok');
            $this->db->where('kode_barang =', $id);
            $query = $this->db->get();
            return $query->result();
    }

    function get_dokter_by_id($id){
            $this->db->select('*');
            $this->db->from('m_dokter');
            $this->db->where('id_M_DOKTER =', $id);
            $query = $this->db->get();
            return $query->result();
    }

    function get_diagnosa_by_id($id){
            $this->db->select('diagnosa2_id,diagnosa_kode,diagnosa_nama,kode_diagnosa,nama_diagnosa');
            $this->db->join('m_diagnosa_icd10','m_diagnosa.diagnosa2_id = m_diagnosa_icd10.diagnosa_id');
            $this->db->from('m_diagnosa');
            $this->db->where('diagnosa2_id =', $id);
            $query = $this->db->get();
            return $query->result();
    }

    function get_diagnosa_icd10_by_id($id){
            $this->db->select('m_diagnosa_icd10.diagnosa_id,m_diagnosa.diagnosa2_id,diagnosa_kode,diagnosa_nama,kode_diagnosa,nama_diagnosa');
            $this->db->join('m_diagnosa','m_diagnosa.diagnosa2_id = m_diagnosa_icd10.diagnosa_id');
            $this->db->from('m_diagnosa_icd10');
            $this->db->where('m_diagnosa_icd10.diagnosa_id =', $id);
            $query = $this->db->get();
            return $query->result();
    }

    public function count_list_obat(){
        $this->db->from('t_apotek_stok');
        // $this->db->from('v_stokobatalkes');
		// $this->db->where('qtystok !=', 0);
        return $this->db->count_all_results();
    }

    // create more generic datatable count list
    public function count_list_dokter(){
        $this->db->from('m_dokter');
        return $this->db->count_all_results();
    }

    // create more generic datatable list filtered
    function count_list_filtered_dokter(){
          $this->db->from('m_dokter');
          $i = 0;
          $search_value = $this->input->get('search');
          if($search_value){
              foreach ($this->columnListDokter as $item){
                  ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                  $i++;
              }
          }
          $order_column = $this->input->get('order');
          if($order_column !== false){
              $this->db->order_by($this->columnListDokter[$order_column['0']['column']], $order_column['0']['dir']);
          }
          else if(isset($this->orderListDokter)){
              $orderListDokter = $this->orderListDokter;
              $this->db->order_by(key($orderListDokter), $orderListDokter[key($orderListDokter)]);
          }
          $query = $this->db->get();
          return $query->num_rows();
    }

	function count_list_filtered_obat(){
        $this->db->from('t_apotek_stok');
        // $this->db->from('v_stokobatalkes');
		// $this->db->where('qtystok !=', 0);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
	}

	public function get_barang_stok_list(){
        $this->db->select("*");
        $this->db->from("m_barang_farmasi");
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $query = $this->db->get();
        return $query->result();
    }
}
