<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_rd extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pasien_rd_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "rawat_darurat_pasienrd_view";
        $comments = "List Pasien Rawat Darurat";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_pasien_rd', $this->data);
    }

    public function ajax_list_pasienrd(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $poliklinik = $this->input->get('poliklinik',TRUE);
        $status = $this->input->get('status',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $list = $this->Pasien_rd_model->get_pasienrd_list($tgl_awal, $tgl_akhir, $poliklinik, $status, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran, $dokter_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $tgl_lahir = $pasienrd->tanggal_lahir;
            $umur  = new Datetime($tgl_lahir);
            $today = new Datetime();
            $diff  = $today->diff($umur);

            $no++;
            $row = array();
            $row[] = $pasienrd->no_pendaftaran;
            $row[] = $pasienrd->tgl_pendaftaran;
            $row[] = '<span data-toggle="modal" data-target="#modal_riwayat_penyakit_pasien" data-backdrop="static" data-keyboard="false" title="klik untuk Riwayat Penyaakit Pasien"  onclick="riwayatPenyakitPasien('."'".$pasienrd->pasien_id."'".')">'.$pasienrd->no_rekam_medis.'</span>';
            $row[] = $pasienrd->no_bpjs;
            $row[] = $pasienrd->pasien_nama;
            $row[] = $pasienrd->jenis_kelamin;
            $row[] = $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
            $row[] = $pasienrd->pasien_alamat;
            $row[] = $pasienrd->type_pembayaran;
            $row[] = $pasienrd->kelaspelayanan_nama;
            $row[] = $pasienrd->status_periksa;
            if(trim(strtoupper($pasienrd->status_periksa)) == "DIPULANGKAN" || trim(strtoupper($pasienrd->status_periksa)) == "SUDAH PULANG" || trim(strtoupper($pasienrd->status_periksa)) == "BATAL RAWAT" || trim(strtoupper($pasienrd->status_periksa)) == "BATAL PERIKSA" || trim(strtoupper($pasienrd->status_periksa)) == "SEDANG RAWAT INAP" || trim(strtoupper($pasienrd->status_periksa)) == "SEDANG DI PENUNJANG" || trim(strtoupper($pasienrd->status_periksa)) == "SEDANG DI OPERASI"){
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk permintaan rawat inap" onclick="permintaanRawatInap('."'".$pasienrd->pendaftaran_id."'".')" disabled>PERMINTAAN RAWAT INAP</button>';  
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRd('."'".$pasienrd->pendaftaran_id."'".')" disabled>PERIKSA</button>';  
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')" disabled>RAWAT INAP</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukPaketOperasi('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."'".')" disabled>PAKET OPERASI</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk memulangkan pasien" onclick="pulangkanPasienRd('."'".$pasienrd->pendaftaran_id."'".')" disabled>PULANGKAN</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke penunjang" onclick="kirimPasienPenunjang('."'".$pasienrd->pendaftaran_id."'".')" disabled>PENUNJANG</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke vk" onclick="kirimPasienVk('."'".$pasienrd->pendaftaran_id."'".')" disabled>VK</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke ok" onclick="kirimPasienVk('."'".$pasienrd->pendaftaran_id."'".')" disabled>OK</button>';
                 $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke hcu" onclick="kirimPasienHCU('."'".$pasienrd->pendaftaran_id."'".')" disabled>HCU</button>';
                  $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke isolasi" onclick="kirimPasienISOLASI('."'".$pasienrd->pendaftaran_id."'".')" disabled>ISOLASI</button>';
                $row[] = '<button type="button" id="batalPeriksa" class="btn btn-default btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasienrd->pendaftaran_id."'".')" disabled><i class="fa fa-times"></i></button>';
            }else if (trim(strtoupper($pasienrd->status_periksa)) == "SEDANG PERMOHONAN RAWAT INAP"){
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_permintaanrawat" data-backdrop="static" data-keyboard="false" title="Klik untuk permintaan rawat inap" onclick="permintaanRawatInap('."'".$pasienrd->pendaftaran_id."'".')">PERMINTAAN RAWAT INAP</button>';
                 $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_pasienrd" data-backdrop="static" data-keyboard="false" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRd('."'".$pasienrd->pendaftaran_id."'".')">PERIKSA</button>';  
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')" disabled>RAWAT INAP</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_paket_operasi" data-backdrop="static" data-keyboard="false" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukPaketOperasi('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."'".')">PAKET OPERASI</button>'; 
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_status_pulang" data-backdrop="static" data-keyboard="false" title="Klik untuk memulangkan pasien" onclick="pulangkanPasienRd('."'".$pasienrd->pendaftaran_id."'".')">PULANGKAN</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_penunjang" data-backdrop="static" data-keyboard="false" title="Klik untuk kirim ke penunjang" onclick="kirimPasienPenunjang('."'".$pasienrd->pendaftaran_id."'".')">PENUNJANG</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke vk" onclick="kirimPasienVk('."'".$pasienrd->pendaftaran_id."'".')" disabled>VK</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke ok" onclick="kirimPasienVk('."'".$pasienrd->pendaftaran_id."'".')" disabled>OK</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke hcu" onclick="kirimPasienHCU('."'".$pasienrd->pendaftaran_id."'".')" disabled>HCU</button>';
                $row[] = '<button type="button" class="btn light-green waves-effect waves-light darken-4" title="Klik untuk kirim ke isolasi" onclick="kirimPasienISOLASI('."'".$pasienrd->pendaftaran_id."'".')" disabled>ISOLASI</button>';
                 $row[] = '<button type="button" id="batalPeriksa" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasienrd->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>'; 
                

            }else{   
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_permintaanrawat" data-backdrop="static" data-keyboard="false" title="Klik untuk permintaan rawat inap" onclick="permintaanRawatInap('."'".$pasienrd->pendaftaran_id."'".')">PERMINTAAN RAWAT INAP</button>'; 
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_pasienrd" data-backdrop="static" data-keyboard="false" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRd('."'".$pasienrd->pendaftaran_id."'".')">PERIKSA</button>'; 
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_rawatinap" data-backdrop="static" data-keyboard="false" title="Klik untuk mengirim pasien ke rawat inap" onclick="rujukKeRI('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'".$pasienrd->no_pendaftaran."'".')">RAWAT INAP</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_ok" data-backdrop="static" data-keyboard="false" title="Klik untuk mengirim pasien ke ok" onclick="getDataPasien('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'ok'".')">KIRIM KE OK</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_vk" data-backdrop="static" data-keyboard="false" title="Klik untuk mengirim pasien ke vk" onclick="getDataPasien('."'".$pasienrd->pendaftaran_id."',"."'".$pasienrd->pasien_id."',"."'vk'".')">KIRIM KE VK</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_penunjang" data-backdrop="static" data-keyboard="false" title="Klik untuk kirim ke penunjang" onclick="kirimKePenunjang('."'".$pasienrd->pendaftaran_id."'".')">PENUNJANG</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_status_pulang" data-backdrop="static" data-keyboard="false" title="Klik untuk memulangkan pasien" onclick="pulangkanPasienRd('."'".$pasienrd->pendaftaran_id."'".')">PULANGKAN</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_penunjang" data-backdrop="static" data-keyboard="false" title="Klik untuk kirim ke penunjang" onclick="kirimPasienPenunjang('."'".$pasienrd->pendaftaran_id."'".')">PENUNJANG</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_vkrd" data-backdrop="static" data-keyboard="false" title="Klik untuk kirim ke VK" onclick="kirimPasienVk('."'".$pasienrd->pendaftaran_id."'".')">OK</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_vkrd" data-backdrop="static" data-keyboard="false" title="Klik untuk kirim ke OK" onclick="kirimPasienVk('."'".$pasienrd->pendaftaran_id."'".')">VK</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_hcurd" data-backdrop="static" data-keyboard="false" title="Klik untuk kirim ke HCU" onclick="kirimPasienHCu('."'".$pasienrd->pendaftaran_id."'".')">HCU</button>';  
                 $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_isolasird" data-backdrop="static" data-keyboard="false" title="Klik untuk kirim ke ISOLASI" onclick="kirimPasienISOLASI('."'".$pasienrd->pendaftaran_id."'".')">ISOLASI</button>';  
                $row[] = '<button type="button" id="batalPeriksa" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasienrd->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>'; 
            }

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_rd_model->count_all($tgl_awal, $tgl_akhir, $poliklinik, $status,  $no_rekam_medis, $no_bpjs,  $pasien_alamat, $nama_pasien, $no_pendaftaran,$dokter_id),
                    "recordsFiltered" => $this->Pasien_rd_model->count_all($tgl_awal, $tgl_akhir, $poliklinik, $status,  $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran,$dokter_id),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function periksa_pasienrd($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);
        // $pasienrd['list_alergi'] = $this->Pasien_rd_model->get_reference('m_alergi');
        // $pasienrd['list_therapy'] = $this->Pasien_rd_model->get_reference('m_therapy');
        // echo json_encode($pasienrd);
        $this->load->view('periksa_pasienrd', $pasienrd);
    }

    public function jes(){
        // echo 'a';die;
        // $table = $this->input->get('table', TRUE);
        // // $table = $this->input->get('table', TRUE);
        // $data = $this->Pasien_rd_model->get_reference($table);
        // print_r($data);
        // echo json_encode($data);
        echo current_url();
        echo json_encode($_GET);
    }




    public function kirim_vkrd($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);


        $this->load->view('kirim_vkrd', $pasienrd);
    }


    public function kirim_okrd($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);


        $this->load->view('kirim_okrd', $pasienrd);
    }

    public function kirim_hcurd($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);


        $this->load->view('kirim_hcurd', $pasienrd);
    }

    public function rujuk_rslain($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);


        $this->load->view('rujuk_rslain', $pasienrd);
    }


    public function kirim_pathologi($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);


        $this->load->view('kirim_pathologird', $pasienrd);
    }



    public function kirim_isolasird($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);


        $this->load->view('kirim_isolasird', $pasienrd);
    }

      public function kirim_permintaanrawat($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);
        $this->load->view('permintaan_rawatinap', $pasienrd);
    }


      public function kirim_rawatinap($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);


        $this->load->view('kirim_rawatinap', $pasienrd);
    }

    public function kirim_pasienrdpenunjang($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienrd['pendaftaran_id_'] =$pendaftaran_id;
        //$pasienrd['pasien_id'] =$pasien_id;
        // $pasienrd['harga_cyto'] =$tarif_cyto;
        // $pasienrd['harga_tindakan'] =$tarif_tindakan;
        // $pasienrd['is_cyto'] =$is_cyto;
        // $pasienrd['jml_tindakan'] =$jml_tindakan;
        // $pasienrd['subtotal'] =$subtotal;
        // $pasienrd['totalharga'] =$total;
        // $pasienrd['tindakanpasien_id'] =$tindakanpasien_id;
        // $pasienrd['kelaspelayanan_id'] =$kelaspelayanan_id;

        $this->load->view('kirim_penunjangrd', $pasienrd);
    }

    public function ajax_list_diagnosa_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_diagnosa_pasien_list($pendaftaran_id);
        // echo json_encode($list);die;
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->kode_icd10;
            $row[] = $diagnosa->nama_icd10;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            // tricky comma
            $comma1 = ',';
            $comma2 = ',';
            if($diagnosa->nama_dokter_1 == null or $diagnosa->nama_dokter_2 == null){
              $comma1 = '';
            }elseif($diagnosa->nama_dokter_2 == null or $diagnosa->nama_dokter_3 == null){
              $comma2 = '';
            }
            $row[] = $diagnosa->nama_dokter_1.$comma1.$diagnosa->nama_dokter_2.$comma2.$diagnosa->nama_dokter_3;
            $row[] = $diagnosa->catatan;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_diagnosa(){
  		$list = $this->Pasien_rd_model->get_list_diagnosa();
      // echo json_encode($list);die;
  		$data = array();
  		foreach($list as $item){
  			$row = array();
  			$row[] = $item->diagnosa_kode;
  			$row[] = $item->diagnosa_nama;
  			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihListDiagnosa('."'".$item->diagnosa2_id."'".')"><i class="fa fa-check"></i></button>';
  			$data[] = $row;
  		}

  		$output = array(
  			"draw" => $this->input->get('draw'),
        "recordsTotal" => count($list),
        "recordsFiltered" => count($list),
  			"data" => $data,
  		);
          //output to json format
          echo json_encode($output);
  	}

    public function ajax_list_diagnosa_icd10(){
  		$list = $this->Pasien_rd_model->get_list_diagnosa_icd10();
      // echo json_encode($list);die;
  		$data = array();
  		foreach($list as $item){
  			$row = array();
  			$row[] = $item->kode_diagnosa;
  			$row[] = $item->nama_diagnosa;
  			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihListDiagnosaICD10('."'".$item->diagnosa_id."'".')"><i class="fa fa-check"></i></button>';
  			$data[] = $row;
  		}

  		$output = array(
  			"draw" => $this->input->get('draw'),
        "recordsTotal" => count($list),
        "recordsFiltered" => count($list),
  			"data" => $data,
  		);
          //output to json format
          echo json_encode($output);
  	}

    public function ajax_list_diagnosa_pasien_rawatinap(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_permintaan_diagnosa_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $diagnosa){
            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date_format($tgl_diagnosa, 'd-M-Y');
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->kode_icd10;
            $row[] = $diagnosa->nama_icd10;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->dokter_id;
            $row[] = 'ngga ada field di db';
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_assessment_therapy(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_background_pasien_list($pendaftaran_id);
        $data = array();
        $count = 0;
        foreach($list as $background){
            $row = array();
            $row[] = ++$count;
            $row[] = $background->nama_alergi;
            //$row[] = $tindakan->total_harga;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            //$row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanpasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_tindakan_pasien_rawatinap(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            //$row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    #jos
    public function ajax_list_background_pasien_rawatinap(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_background_pasien_list($pendaftaran_id);
        $data = array();
        $count = 0;
        foreach($list as $background){
            $row = array();
            $row[] = ++$count;
            $row[] = $background->nama_alergi;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus alergi" onclick="hapusAlergi('."'".$background->alergi_pasien_id."'".')"><i class="fa fa-times"></i></button>';
            //$row[] = $tindakan->total_harga;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_resep_pasien_rawatinap(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_resep_pasien_list($pendaftaran_id);
        $data = array();
        $count =0;
        foreach($list as $resepp){
            $row = array();
            $row[]=++$count;
            $row[] = $resepp->nama_sediaan;
            $row[] = $resepp->qty;
            $row[] = $resepp->nama_jenis;

            //$row[] = $tindakan->total_harga;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


  public function ajax_list_assestment_pasien_list(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_assestment_pasien_list($pendaftaran_id);
        $data = array();
        $count =0;
        foreach($list as $resepp){
            $row = array();
            $row[]=++$count;
            $row[] = $resepp->nama;
            // $row[] = $resepp->qty;
            // $row[] = $resepp->nama_jenis;

            //$row[] = $tindakan->total_harga;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }



    public function ajax_list_permintaan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_tindakan_pasien_list_permintaan($pendaftaran_id);
        $data = array();
        $count = 0;
        foreach($list as $tindakan){
            //$tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
         //   $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = ++$count;
            $row[] = $tindakan->daftartindakan_nama;
            //$row[] = $tindakan->jml_tindakan;
            //$row[] = $tindakan->total_harga_tindakan;
           // $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('.$tindakan->tindakan_id.')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
       function get_permintaan_tindakan_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_tindakan = $this->Pasien_rd_model->get_permintaan_tindakan($poliruangan_id);
        // $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        // foreach($list_tindakan as $list_tindak){
        //     $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        // }


        if(count($list_tindakan) >= 1){
            $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        }else{
            $list = "";
        }
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";

        }


        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

      function get_permintaan_tindakan_list_penunjang(){
        $poliruangan_id = $this->input->get('penunjang_id',TRUE);
        $list_tindakan = $this->Pasien_rd_model->get_list_penunjang($poliruangan_id);
        $list_lab = $this->Pasien_rd_model->get_datalab();
        $list_rad = $this->Pasien_rd_model->get_datarad();
        // $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        // foreach($list_tindakan as $list_tindak){
        //     $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        // }

        $list = "<option value=\"\" disabled selected>Pilih Poli/Ruangan</option>";
        if ($poliruangan_id == 7) {
            foreach($list_lab as $list_ruang){
                     $list .= "<option value='".$list_ruang->tindakan_lab_id."'>".$list_ruang->tindakan_lab_nama."</option>";
            }
        }elseif ($poliruangan_id == 8) {
            foreach ($list_rad as $list_ruang) {
                  $list .= "<option value='".$list_ruang->daftartindakan_id."'>".$list_ruang->daftartindakan_nama."</option>";
            }
            # code...
        }
        // else {
        //     foreach($list_ruangan as $list_ruang){
        //             $list .= "<option value='".$list_ruang->poliruangan_id."'>".$list_ruang->nama_poliruangan."</option>";
        //     }
        // }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_tindakan_list(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $list_tindakan = $this->Pasien_rd_model->get_tindakan($kelaspelayanan_id);
        // $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        // foreach($list_tindakan as $list_tindak){
        //     $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        // }


        if(count($list_tindakan) >= 1){
            $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        }else{
            $list = "";
        }
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";

        }


        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }
    function get_tarif_tindakan(){
        $daftartindakan_id = $this->input->get('daftartindakan_id',TRUE);
        $daftar_tindakan = $this->Pasien_rd_model->get_tarif_tindakan($daftartindakan_id);

        $res = array(
            "list" => $daftar_tindakan,
            "success" => true
        );

        echo json_encode($res);
    }

    // jos
    public function do_create_diagnosa_pasien(){
        // $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        // if(!$is_permit) {
        //     $res = array(
        //         'csrfTokenName' => $this->security->get_csrf_token_name(),
        //         'csrfHash' => $this->security->get_csrf_hash(),
        //         'success' => false,
        //         'messages' => $this->lang->line('aauth_error_no_access'));
        //     echo json_encode($res);
        //     exit;
        // }
        //
        // $this->load->library('form_validation');
        // $this->form_validation->set_rules('nama_diagnosa','Nama Diagnosa', 'required|trim');
        // $this->form_validation->set_rules('kode_diagnosa','Kode Diagnosa', 'required|trim');
        // $this->form_validation->set_rules('jenis_diagnosa','Jenis Diagnosa', 'required');
        //
        // if ($this->form_validation->run() == FALSE)  {
        //     $error = validation_errors();
        //     $res = array(
        //         'csrfTokenName' => $this->security->get_csrf_token_name(),
        //         'csrfHash' => $this->security->get_csrf_hash(),
        //         'success' => false,
        //         'messages' => $error);
        //
        //     // if permitted, do logit
        //     $perms = "rawat_darurat_pasienrd_view";
        //     $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
        //     $this->aauth->logit($perms, current_url(), $comments);
        //
        // }else if ($this->form_validation->run() == TRUE)  {

            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter_id = $this->input->post('dokter_id_1', TRUE);
            $dokter_id_2 = $this->input->post('dokter_id_2', TRUE);
            $dokter_id_3 = $this->input->post('dokter_id_3', TRUE);
            $kode_diagnosa = $this->input->post('kode_diagnosa', TRUE);
            $nama_diagnosa = $this->input->post('nama_diagnosa', TRUE);
            $kode_diagnosa_icd10 = $this->input->post('kode_diagnosa_icd10', TRUE);
            $nama_diagnosa_icd10 = $this->input->post('nama_diagnosa_icd10', TRUE);
            $jenis_diagnosa = $this->input->post('jenis_diagnosa', TRUE);
            $catatan_diagnosa = $this->input->post('catatan_diagnosa', TRUE);
            // insert all data to array
            $data_diagnosapasien = array(
                // hardcode
                // ini sesuai nama kolom database
                'diagnosa_by' => 'IGD',
                'instalasi_id' => 2, // kode untuk IGD
                'tgl_diagnosa' => date('Y-m-d'),
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'dokter_id' => $dokter_id,
                'dokter_id_2' => $dokter_id_2,
                'dokter_id_3' => $dokter_id_3,
                'kode_diagnosa' => $kode_diagnosa,
                'nama_diagnosa' => $nama_diagnosa,
                'kode_icd10' => $kode_diagnosa_icd10,
                'nama_icd10' => $nama_diagnosa_icd10,
                'jenis_diagnosa' => $jenis_diagnosa,
                'catatan' => $catatan_diagnosa

            );
            // $res = $data_diagnosapasien;
            if($jenis_diagnosa == 2){
              $getz = 0;
            }else{
              $getz = 1;
            }
            $query = $this->db->get_where('t_diagnosapasien', array('pendaftaran_id' => $pendaftaran_id,'jenis_diagnosa'=>$getz));
            if ($query->num_rows() > 0) {
                $res = array(
            //         'csrfTokenName' => $this->security->get_csrf_token_name(),
            //         'csrfHash' => $this->security->get_csrf_hash(),
                      'success' => false,
                      'messages' => 'Diagnosa Utama Hanya Bisa Diinput Satu Kali'
                  );
            //
            //         // if permitted, do logit
            //         $perms = "rawat_darurat_pasienrd_view";
            //         $comments = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
            //         $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $ins = $this->Pasien_rd_model->insert_diagnosapasien($pendaftaran_id, $data_diagnosapasien);
                //$ins=true;
                if($ins){
                    $res = array(
            //             'csrfTokenName' => $this->security->get_csrf_token_name(),
            //             'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Diagnosa berhasil ditambahkan'
                    );
            //
            //         // if permitted, do logit
            //         $perms = "rawat_darurat_pasienrd_view";
            //         $comments = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
            //         $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
            //         'csrfTokenName' => $this->security->get_csrf_token_name(),
            //         'csrfHash' => $this->security->get_csrf_hash(),
                          'success' => false,
                          'messages' => 'Gagal menambahkan Diagnosa, hubungi web administrator.'
                      );
            //
            //         // if permitted, do logit
            //         $perms = "rawat_darurat_pasienrd_view";
            //         $comments = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
            //         $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        // }
        echo json_encode($res);
    }

    // insert tindakan
    public function do_create_tindakan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('tindakan','Nama Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jumlah Tindakan', 'required');
        // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        $this->form_validation->set_rules('is_cyto','Cyto', 'required');
        // $this->form_validation->set_rules('dokter','Dokter', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $tindakan = $this->input->post('tindakan', TRUE);
            $jml_tindakan = $this->input->post('jml_tindakan', TRUE);
            $subtotal = $this->input->post('subtotal', TRUE);
            $is_cyto = $this->input->post('is_cyto', TRUE);
            $totalharga = $this->input->post('totalharga', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'daftartindakan_id' => $tindakan,
                'jml_tindakan' => $jml_tindakan,
                'total_harga_tindakan' => $subtotal,
                'is_cyto' => $is_cyto == '1' ? '1' : '0',
                'total_harga' => $totalharga,
                'dokter_id' => $dokter,
                'tgl_tindakan' => date('Y-m-d'),
                'ruangan_id' => $data_pendaftaran->poliruangan_id,
                'komponentarif_id' => 1,
                'kelaspelayanan_id' => 1
            );

            $ins = $this->Pasien_rd_model->insert_tindakanpasien($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
            // // count dokter
            // $query  = $this->Pasien_rd_model->checkDokter($pendaftaran_id);

            // // // check if dokter available, do insert
            // $query1 = $this->Pasien_rd_model->checkAvailableDok($pendaftaran_id, $dokter);

            // if($query >= 3 && $query1 == NULL ){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

        public function do_create_tindakan_pasien_penunjang(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('tindakan','Nama Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jumlah Tindakan', 'required');
        // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        $this->form_validation->set_rules('is_cyto','Cyto', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $tindakan = $this->input->post('tindakan', TRUE);
            $jml_tindakan = $this->input->post('jml_tindakan', TRUE);
            $subtotal = $this->input->post('subtotal', TRUE);
            $is_cyto = $this->input->post('is_cyto', TRUE);
            $totalharga = $this->input->post('totalharga', TRUE);
        //    $penunjang_id =$this->input->post('penunjang_id', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'daftartindakan_id' => $tindakan,
                'jml_tindakan' => $jml_tindakan,
                'total_harga_tindakan' => $subtotal,
                'is_cyto' => $is_cyto == '1' ? '1' : '0',
                'total_harga' => $totalharga,
                'dokter_id' => $dokter,
                'tgl_tindakan' => date('Y-m-d'),
                'ruangan_id' => $data_pendaftaran->poliruangan_id,
                'komponentarif_id' => 1,
                'kelaspelayanan_id' => 1
            );

            $ins = $this->Pasien_rd_model->insert_tindakanpasien($pendaftaran_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_create_ajukan_permintaan(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('dokter_id','Nama Dokter', 'required|trim');
        $this->form_validation->set_rules('kamar_id','Nama Kamar', 'required');
        // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        //$this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
           // $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter_id', TRUE);
            $kamar = $this->input->post('kamar_id', TRUE);
            $rencanaTindakanArr = $this->input->post('rencanaTindakanArr', TRUE);
        //    $penunjang_id =$this->input->post('penunjang_id', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_tindakanpasien = array(
                'no_pendaftaran' => $pendaftaran_id,
                'dokter_id' => $dokter,
                'kamarruangan_id' => $kamar,
                'nama_pasien' => $data_pendaftaran_pasien->pasien_nama,
                'alamat_pasien' => $data_pendaftaran_pasien->pasien_alamat,
                'jenis_kelamin' => $data_pendaftaran_pasien->jenis_kelamin,
                'tanggal_lahir' => $data_pendaftaran_pasien->tanggal_lahir,
                'no_telp' => $data_pendaftaran_pasien->tlp_selular,


                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->Pasien_rd_model->insert_ajukan_permintaan($data_tindakanpasien);
            //$ins=true;
            if($ins != 0){
                $this->Pasien_rd_model->update_to_permintaan_rawat($pendaftaran_id);
                $rencanaTindakanArr = json_decode($rencanaTindakanArr);
                if (count($rencanaTindakanArr) > 0 ) {
                    foreach ($rencanaTindakanArr as $item) {
                        $data = array(
                            'tindakan_id' =>$item->id_rencana_tindakan,
                            'det_reservasi_ri_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_tindakanpasienpermintaan($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
            else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    // ada 2 fungsi kirim ke penunjang ?
    // public function kirim_ke_penunjang(){
    //     $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
    //     if(!$is_permit) {
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash' => $this->security->get_csrf_hash(),
    //             'success' => false,
    //             'messages' => $this->lang->line('aauth_error_no_access'));
    //         echo json_encode($res);
    //         exit;
    //     }

    //     $this->load->library('form_validation');
    //     //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
    //     $this->form_validation->set_rules('pendaftaran_id_','Pendaftaran', 'required');
    //     $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

    //     if ($this->form_validation->run() == FALSE)  {
    //         $error = validation_errors();
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash' => $this->security->get_csrf_hash(),
    //             'success' => false,
    //             'messages' => $error);

    //         // if permitted, do logit
    //         $perms = "rawat_darurat_pasienrd_view";
    //         $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
    //         $this->aauth->logit($perms, current_url(), $comments);

    //     }else if ($this->form_validation->run() == TRUE)  {
    //         $pendaftaran_id = $this->input->post('pendaftaran_id_', TRUE);
    //         $penunjang_id = $this->input->post('penunjang_id', TRUE);
    //         $catatan = $this->input->post('catatan', TRUE);
    //         $instalasi_id = $this->input->post('instalasi_id', TRUE);
    //         $rencanaTindakanArr = $this->input->post('rencanaTindakanArr', TRUE);
    //         $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
    //         $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
    //             if($penunjang_id == 7){
    //                 $keberadaan = "LABORATORIUM";
    //             }else {
    //                 $keberadaan = "RADIOLOGI";
    //             }
    //         $data_penunjang = array(
    //              'pendaftaran_id' => $pendaftaran_id,
    //              'status_keberadaan' => $keberadaan,
    //              'pasien_id' => $data_pendaftaran_pasien->pasien_id,
    //              'no_masukpenunjang' => $data_pendaftaran->no_pendaftaran,
    //              'kelaspelayanan_id' => $data_pendaftaran->kelaspelayanan_id,
    //              'tgl_masukpenunjang' => date('Y-m-d-H-i-s'),
    //              'catatan' => $catatan,
    //              'statusperiksa' => 'ANTRIAN',
    //              'dokterpengirim_id' => $data_pendaftaran->dokter_id,
    //              'create_time' => date('Y-m-d-H-i-s'),
    //              'poli_ruangan_asal' => $instalasi_id,
    //              'poli_ruangan_id' => $penunjang_id,


    //         );

    //         $ins = $this->Pasien_rd_model->insert_penunjangrd($data_penunjang);
    //         //$ins=true;
    //         if($ins != 0){
    //             $this->Pasien_rd_model->update_to_penunjang($pendaftaran_id);
    //             if ($penunjang_id == 7) {
    //                 $data_tindakanlab = array(
    //                     'pendaftaran_id' => $pendaftaran_id ,
    //                     'pasien_id' => $data_pendaftaran_pasien->pasien_id,
    //                     'dokter_id' => $data_pendaftaran->dokter_id,
    //                     'dokterpengirim_id' => $data_pendaftaran->dokter_id,
    //                     'tgl_tindakan' => date('Y-m-d-H-i-s'),
    //                 );

    //                 $masuk= $this->Pasien_rd_model->insert_labpenunjang($data_tindakanlab);

    //                 $rencanaTindakanArr = json_decode($rencanaTindakanArr);
    //                 if (count($rencanaTindakanArr) > 0 ) {
    //                     foreach ($rencanaTindakanArr as $item) {
    //                         $data = array(
    //                             'nama_pemeriksaan' =>$item->nama_rencana_tindakan,
    //                             'tindakan_lab_id' => $masuk
    //                         );

    //                         $rencana = $this->Pasien_rd_model->insert_labpenunjangdet($data);
    //                     }
    //                 }
    //             }else{
    //                 $rencanaTindakanArr = json_decode($rencanaTindakanArr);
    //                 if (count($rencanaTindakanArr) > 0 ) {
    //                     foreach ($rencanaTindakanArr as $item) {
    //                         $data = array(
    //                             'pendaftaran_id' => $pendaftaran_id ,
    //                             'pasien_id' => $data_pendaftaran_pasien->pasien_id,
    //                             'dokter_id' => $data_pendaftaran->dokter_id,
    //                             'dokterpengirim_id' => $data_pendaftaran->dokter_id,
    //                             'dokteranastesi_id' => $data_pendaftaran->dokter_id,
    //                             'tgl_tindakan' => date('Y-m-d-H-i-s'),
    //                             'daftartindakan_id' =>$item->id_rencana_tindakan,
    //                             'jml_tindakan' =>count($rencanaTindakanArr),
    //                         );

    //                         $rencana = $this->Pasien_rd_model->insert_radpenunjang($data);
    //                     }
    //                 }
    //             }
    //             // $this->Pasien_rd_model->update_to_permintaan_rawat($pendaftaran_id);


    //             $res = array(
    //                 'csrfTokenName' => $this->security->get_csrf_token_name(),
    //                 'csrfHash' => $this->security->get_csrf_hash(),
    //                 'success' => true,
    //                 'messages' => 'Ajukan Berhasil Ditambahkan'
    //             );

    //             // if permitted, do logit
    //             $perms = "rawat_darurat_pasienrd_view";
    //             $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
    //             $this->aauth->logit($perms, current_url(), $comments);
    //         }
    //         else{
    //             $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash' => $this->security->get_csrf_hash(),
    //             'success' => false,
    //             'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

    //             // if permitted, do logit
    //             $perms = "rawat_darurat_pasienrd_view";
    //             $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
    //             $this->aauth->logit($perms, current_url(), $comments);
    //         }
    //     }
    //     echo json_encode($res);
    // }





    public function do_create_assestment(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('tensi','Tensi', 'required|trim');
        $this->form_validation->set_rules('suhu','Suhu', 'required');
        // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        //$this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('tensi', TRUE);
            $suhu = $this->input->post('suhu', TRUE);
            $nadi = $this->input->post('nadi', TRUE);
            $nadi_2 = $this->input->post('nadi_1', TRUE);
            $penggunaan = $this->input->post('penggunaan', TRUE);
            $saturasi = $this->input->post('saturasi', TRUE);
            $nyeri = $this->input->post('nama_nyeri', TRUE);
            $numeric = $this->input->post('numeric', TRUE);
            $resiko = $this->input->post('resiko', TRUE);
            $theraphyArr = $this->input->post('theraphyArr', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'pendaftaran_id' => $pendaftaran_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'nadi_2' => $nadi_2,
                'suhu' => $suhu,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko,
                'pasien_id' => $data_pendaftaran->pasien_id,
                'instalasi_id' => '3',

                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->Pasien_rd_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_detassestment($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }



        public function do_create_rawatinap(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
      $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);
            $instalasi_id = $this->input->post('instalasi_id', TRUE);
            $catatan = $this->input->post('catatan', TRUE);
            $dokter_ri = $this->input->post('dokter_id', TRUE);
            $paketOperasiArr = $this->input->post('paketOperasiArr', TRUE);

            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $dataadmisi = array(
                 'pendaftaran_id' => $pendaftaran_id,
                 'kelaspelayanan_id' => $kelaspelayanan,
                 'no_masukadmisi' => $data_pendaftaran->no_pendaftaran,
                 'tgl_masukadmisi' => date('Y-m-d-H-i-s'),
                 'statusrawat' => 'SEDANG RAWAT INAP',
                 'poli_ruangan_id' => $poliruangan,
                 'kamarruangan_id' => $kamarruangan,
                 'create_time' => date('Y-m-d-H-i-s'),
                // 'create_by' => $this->data['users']->id,
                 'pasien_id' => $data_pendaftaran->pasien_id,
                 'instalasi_asal_id' => $instalasi_id,
                 'instalasi_tujuan_id' => '4',
                 'catatan' => $catatan,
                 'dokter_id' => $dokter_ri,
            );
            $ins = $this->Pasien_rd_model->insert_admisi($dataadmisi);
            if ($ins) {
            	$masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $ins;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
               // $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasien_rd_model->insert_kamar($masukkamar);

                $updatekamar['status_bed'] = '1';
                $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
                $updatependaftaran['status_periksa'] = "SEDANG RAWAT INAP";
                $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            }
            // $ins = array(
            // 	'poli_ruangan_id' => $poliruangan,
            // 	'kelaspelayanan_id' => $kelaspelayanan,
            // 	'kamarruangan_id' => $kamarruangan,
            // 	'tgl_masukkamar' => date('Y-m-d-H-i-s'),
            // 	'pasienadmisi_id' => $ins,
            // 	'create_time' => date('Y-m-d-H-i-s'),
            //     'create_user' => $this->data['users']->id,
            //     'pasienadmisi_id' => '1'
            //     );
            //  $this->Pasien_rd_model->insert_kamar($datakamar);
            //  $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
            //  $updatependaftaran['status_periksa'] = "SEDANG RAWAT INAP";
            //  $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);


            //$ins=true;
            if($ins != 0){
                $paketOperasiArr = json_decode($paketOperasiArr);
                if (count($paketOperasiArr) > 0 ) {
                    foreach ($paketOperasiArr as $item) {
                        $data = array(
                            'daftartindakan_id' =>$item->id_nama_paket,
                            'instalasi_id' => '3',
                            'pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_tindakanpasien_po($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }



    public function do_create_vkrd(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
      $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);
            $instalasi_id = $this->input->post('instalasi_id', TRUE);
            $catatan = $this->input->post('catatan', TRUE);
            $jamperiksa = $this->input->post('jam_periksa', TRUE);
            $paketOperasiArr = $this->input->post('paketOperasiArr', TRUE);

            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $dataadmisi = array(
                 'pendaftaran_id' => $pendaftaran_id,
                 'kelaspelayanan_id' => $kelaspelayanan,
                 'no_masukadmisi' => $data_pendaftaran->no_pendaftaran,
                 'tgl_masukadmisi' => date('Y-m-d-H-i-s'),
                 'statusrawat' => 'SEDANG DI VK',
                 'poli_ruangan_id' => $poliruangan,
                 'kamarruangan_id' => $kamarruangan,
                 'create_time' => date('Y-m-d-H-i-s'),
                // 'create_by' => $this->data['users']->id,
                 'pasien_id' => $data_pendaftaran->pasien_id,
                 'instalasi_asal_id' => $instalasi_id,
                 'instalasi_tujuan_id' => '5',
                 'catatan' => $catatan,
                 // 'dokter_id' => $dokter_ri,
            );
            $ins = $this->Pasien_rd_model->insert_admisi($dataadmisi);
            if ($ins) {
                $masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $ins;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                $masukkamar['tanggal_rencana_tindakan'] =$jamperiksa;
               // $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasien_rd_model->insert_kamar($masukkamar);

                $updatekamar['status_bed'] = '1';
               $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
                $updatependaftaran['status_periksa'] = "SEDANG DI VK";
                $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            }
            //$ins=true;
            if($ins != 0){
                $paketOperasiArr = json_decode($paketOperasiArr);
                if (count($paketOperasiArr) > 0 ) {
                    foreach ($paketOperasiArr as $item) {
                        $data = array(
                            'daftartindakan_id' =>$item->id_nama_paket,
                            'instalasi_id' => '3',
                            'pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_tindakanpasien_po($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_create_hcurd(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
      $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);
            $instalasi_id = $this->input->post('instalasi_id', TRUE);
            $catatan = $this->input->post('catatan', TRUE);
            $jamperiksa = $this->input->post('jam_periksa', TRUE);
            $paketOperasiArr = $this->input->post('paketOperasiArr', TRUE);

            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $dataadmisi = array(
                 'pendaftaran_id' => $pendaftaran_id,
                 'kelaspelayanan_id' => $kelaspelayanan,
                 'no_masukadmisi' => $data_pendaftaran->no_pendaftaran,
                 'tgl_masukadmisi' => date('Y-m-d-H-i-s'),
                 'statusrawat' => 'SEDANG DI HCU',
                 'poli_ruangan_id' => $poliruangan,
                 'kamarruangan_id' => $kamarruangan,
                 'create_time' => date('Y-m-d-H-i-s'),
                // 'create_by' => $this->data['users']->id,
                 'pasien_id' => $data_pendaftaran->pasien_id,
                 'instalasi_asal_id' => $instalasi_id,
                 'instalasi_tujuan_id' => '8',
                 'catatan' => $catatan,
                 // 'dokter_id' => $dokter_ri,
            );
            $ins = $this->Pasien_rd_model->insert_admisi($dataadmisi);
            if ($ins) {
                $masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $ins;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                $masukkamar['tanggal_rencana_tindakan'] =$jamperiksa;
               // $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasien_rd_model->insert_kamar($masukkamar);

                $updatekamar['status_bed'] = '1';
               $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
                $updatependaftaran['status_periksa'] = "SEDANG DI HCU";
                $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            }
            //$ins=true;
            if($ins != 0){
                $paketOperasiArr = json_decode($paketOperasiArr);
                if (count($paketOperasiArr) > 0 ) {
                    foreach ($paketOperasiArr as $item) {
                        $data = array(
                            'daftartindakan_id' =>$item->id_nama_paket,
                            'instalasi_id' => '3',
                            'pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_tindakanpasien_po($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_create_pathologird(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
      $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);
            $instalasi_id = $this->input->post('instalasi_id', TRUE);
            $catatan = $this->input->post('catatan', TRUE);
            $jamperiksa = $this->input->post('jam_periksa', TRUE);
            $paketOperasiArr = $this->input->post('paketOperasiArr', TRUE);

            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $dataadmisi = array(
                 'pendaftaran_id' => $pendaftaran_id,
                 'kelaspelayanan_id' => $kelaspelayanan,
                 'no_masukadmisi' => $data_pendaftaran->no_pendaftaran,
                 'tgl_masukadmisi' => date('Y-m-d-H-i-s'),
                 'statusrawat' => 'SEDANG DI PATHOLOGI ANAK',
                 'poli_ruangan_id' => $poliruangan,
                 'kamarruangan_id' => $kamarruangan,
                 'create_time' => date('Y-m-d-H-i-s'),
                // 'create_by' => $this->data['users']->id,
                 'pasien_id' => $data_pendaftaran->pasien_id,
                 'instalasi_asal_id' => $instalasi_id,
                 'instalasi_tujuan_id' => '9',
                 'catatan' => $catatan,
                 // 'dokter_id' => $dokter_ri,
            );
            $ins = $this->Pasien_rd_model->insert_admisi($dataadmisi);
            if ($ins) {
                $masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $ins;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                $masukkamar['tanggal_rencana_tindakan'] =$jamperiksa;
               // $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasien_rd_model->insert_kamar($masukkamar);

                $updatekamar['status_bed'] = '1';
               $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
                $updatependaftaran['status_periksa'] = "SEDANG DI PATHOLOGI ANAK";
                $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            }
            //$ins=true;
            if($ins != 0){
                $paketOperasiArr = json_decode($paketOperasiArr);
                if (count($paketOperasiArr) > 0 ) {
                    foreach ($paketOperasiArr as $item) {
                        $data = array(
                            'daftartindakan_id' =>$item->id_nama_paket,
                            'instalasi_id' => '3',
                            'pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_tindakanpasien_po($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }



    public function do_create_isolasird(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
      $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);
            $instalasi_id = $this->input->post('instalasi_id', TRUE);
            $catatan = $this->input->post('catatan', TRUE);
            $jamperiksa = $this->input->post('jam_periksa', TRUE);
            $paketOperasiArr = $this->input->post('paketOperasiArr', TRUE);

            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $dataadmisi = array(
                 'pendaftaran_id' => $pendaftaran_id,
                 'kelaspelayanan_id' => $kelaspelayanan,
                 'no_masukadmisi' => $data_pendaftaran->no_pendaftaran,
                 'tgl_masukadmisi' => date('Y-m-d-H-i-s'),
                 'statusrawat' => 'SEDANG DI ISOLASI',
                 'poli_ruangan_id' => $poliruangan,
                 'kamarruangan_id' => $kamarruangan,
                 'create_time' => date('Y-m-d-H-i-s'),
                // 'create_by' => $this->data['users']->id,
                 'pasien_id' => $data_pendaftaran->pasien_id,
                 'instalasi_asal_id' => $instalasi_id,
                 'instalasi_tujuan_id' => '7',
                 'catatan' => $catatan,
                 // 'dokter_id' => $dokter_ri,
            );
            $ins = $this->Pasien_rd_model->insert_admisi($dataadmisi);
            if ($ins) {
                $masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $ins;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                $masukkamar['tanggal_rencana_tindakan'] =$jamperiksa;
               // $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasien_rd_model->insert_kamar($masukkamar);

                $updatekamar['status_bed'] = '1';
               $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
                $updatependaftaran['status_periksa'] = "SEDANG DI ISOLASI";
                $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            }
            //$ins=true;
            if($ins != 0){
                $paketOperasiArr = json_decode($paketOperasiArr);
                if (count($paketOperasiArr) > 0 ) {
                    foreach ($paketOperasiArr as $item) {
                        $data = array(
                            'daftartindakan_id' =>$item->id_nama_paket,
                            'instalasi_id' => '3',
                            'pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_tindakanpasien_po($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


      public function do_create_rujukrslain(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
      $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
      $this->form_validation->set_rules('alasanrujuk','Alasan Rujuk', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $halperlu = $this->input->post('halperlu', TRUE);
            $catatan = $this->input->post('catatan', TRUE);
            $alasanrujuk = $this->input->post('alasanrujuk', TRUE);
            $instalasi_id = $this->input->post('instalasi_id', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $dataadmisi = array(
                 'pendaftaran_id' => $pendaftaran_id,
                 'catatan_rujukan' => $catatan,
                 'alasan_rujuk_id' => $alasanrujuk,
                 'catatan_keluhan' => $halperlu,
                 'status_periksa' => 'RUJUK RS LAIN',
                 'is_rujuk_pulang' => $instalasi_id,
                 // 'pasien_id' => $data_pendaftaran->pasien_id,
            );
            $ins = $this->Pasien_rd_model->update_pendaftaran($dataadmisi, $pendaftaran_id);
            //$ins=true;
            if($ins != 0){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_create_okrd(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
      $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);
            $instalasi_id = $this->input->post('instalasi_id', TRUE);
            $catatan = $this->input->post('catatan', TRUE);
            $jamperiksa = $this->input->post('jam_periksa', TRUE);
            $paketOperasiArr = $this->input->post('paketOperasiArr', TRUE);

            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $dataadmisi = array(
                 'pendaftaran_id' => $pendaftaran_id,
                 'kelaspelayanan_id' => $kelaspelayanan,
                 'no_masukadmisi' => $data_pendaftaran->no_pendaftaran,
                 'tgl_masukadmisi' => date('Y-m-d-H-i-s'),
                 'statusrawat' => 'SEDANG DI OK',
                 'poli_ruangan_id' => $poliruangan,
                 'kamarruangan_id' => $kamarruangan,
                 'create_time' => date('Y-m-d-H-i-s'),
                // 'create_by' => $this->data['users']->id,
                 'pasien_id' => $data_pendaftaran->pasien_id,
                 'instalasi_asal_id' => $instalasi_id,
                 'instalasi_tujuan_id' => '6',
                 'catatan' => $catatan,
                 // 'dokter_id' => $dokter_ri,
            );
            $ins = $this->Pasien_rd_model->insert_admisi($dataadmisi);
            if ($ins) {
                $masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $ins;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                $masukkamar['tanggal_rencana_tindakan'] =$jamperiksa;
               // $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasien_rd_model->insert_kamar($masukkamar);

                $updatekamar['status_bed'] = '1';
               $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
                $updatependaftaran['status_periksa'] = "SEDANG DI OK";
                $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            }
            //$ins=true;
            if($ins != 0){
                $paketOperasiArr = json_decode($paketOperasiArr);
                if (count($paketOperasiArr) > 0 ) {
                    foreach ($paketOperasiArr as $item) {
                        $data = array(
                            'daftartindakan_id' =>$item->id_nama_paket,
                            'instalasi_id' => '3',
                            'pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_tindakanpasien_po($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_delete_alergi(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $alergi_pasien_id = $this->input->post('alergi_pasien_id', TRUE);

        $delete = $this->Pasien_rd_model->delete_alergi($alergi_pasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Alergi Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $alergi_pasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $alergi_pasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosapasien_id = $this->input->post('diagnosapasien_id', TRUE);

        $delete = $this->Pasien_rd_model->delete_diagnosa($diagnosapasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakanpasien_id = $this->input->post('tindakanpasien_id', TRUE);

        $delete = $this->Pasien_rd_model->delete_tindakan($tindakanpasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function ajax_list_resep_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rd_model->get_resep_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $resep){
            $tgl_ = date_create($resep->tgl_reseptur);
            $row = array();
            $row[] = date_format($tgl_, 'd-M-Y');
            $row[] = $resep->nama_barang;
            $row[] = $resep->qty;
            $row[] = $resep->harga_jual;
            // $row[] = $resep->satuan;
            // $row[] = $resep->signa;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus Resep" onclick="hapusResep('."'".$resep->reseptur_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    function autocomplete_obat(){
        $val_obat = $this->input->get('val_obat');
        if(empty($val_obat)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Obat dipilih"
                );
        }
        $obat_list = $this->Pasien_rd_model->get_obat_autocomplete($val_obat);
        if(count($obat_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($obat_list); $i++){
                $list[] = array(
                    "id" => $obat_list[$i]->obat_id,
                    "value" => $obat_list[$i]->nama_obat,
                    "deskripsi" => $obat_list[$i]->jenisoa_nama,
                    "satuan" => $obat_list[$i]->satuan,
                    "harga_netto" => $obat_list[$i]->harga_modal,
                    "harga_jual" => $obat_list[$i]->harga_jual,
                    "current_stok" => $obat_list[$i]->qtystok
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Obat ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Obat Not Found"
                );
        }
        echo json_encode($res);
    }

    // insert resep
    public function do_create_resep_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_resep','Tanggal Resep', 'required');
        $this->form_validation->set_rules('nama_obat','Nama Obat', 'required|trim');
        $this->form_validation->set_rules('qty_obat','Jumlah Obat', 'required');
        $this->form_validation->set_rules('harga_jual','Harga Obat', 'required');
        $this->form_validation->set_rules('satuan_obat','Satuan Obat', 'required');
        $this->form_validation->set_rules('signa_1','Signa Obat 1', 'required');
        $this->form_validation->set_rules('signa_2','Signa Obat 2', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input resep pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id  = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id       = $this->input->post('pasien_id', TRUE);
            $dokter          = $this->input->post('dokter_id', TRUE);
            $obat_id         = $this->input->post('obat_id', TRUE);
            $nama_obat       = $this->input->post('nama_obat', TRUE);
            $qty_obat        = $this->input->post('qty_obat', TRUE);
            $harga_jual      = $this->input->post('harga_jual', TRUE);
            $harga_netto     = $this->input->post('harga_netto', TRUE);
            $satuan_obat     = $this->input->post('satuan_obat', TRUE);
            $id_jenis_barang = $this->input->post('id_jenis_barang', TRUE);
            $dataRacikan = $this->input->post("dataRacikan", TRUE);

            $signa_obat      = $this->input->post('signa_obat', TRUE);


            $data_reseppasien = array(
                'kode_barang'    => $obat_id,
                'nama_barang'    => $nama_obat,
                'qty'            => $qty_obat,
                'harga_netto'    => $harga_netto,
                'harga_jual'     => $harga_jual,
                'id_sediaan'     => $satuan_obat,
                'id_jenis_barang'=> $id_jenis_barang,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id'      => $pasien_id,
                'dokter_id'      => $dokter,
                'tgl_reseptur'   => date('Y-m-d-H-i-s'). substr((string)microtime(), 1, 4),
                'instalasi_id'   => 1
            );

            $data_reseppasien2 = array(
                'kode_barang'    => $obat_id,
                'nama_barang'    => $nama_obat,
                'jumlah_barang'  => $qty_obat,
                'id_sediaan'     => $satuan_obat,
                'id_jenis_barang'=> $id_jenis_barang,
                'harga_satuan'   => $harga_jual,
                'tanggal_keluar' => date('Y-m-d-H-i-s'). substr((string)microtime(), 1, 4),
            );
            //start transaction
            // $this->db->trans_begin();

            $query1 = $this->db->get_where('t_apotek_stok',"stok_akhir <='$qty_obat'"); //cek stok tersedia


            if ($query1->num_rows() > 0) {
            // insert for table stok farmasi

            // $data_reseppasien2 = array(
            //     'kode_barang'    => $obat_id,
            //     'nama_barang'    => $nama_obat,
            //     'jumlah_barang'  => $qty_obat,
            //     'id_sediaan'     => $satuan_obat,
            //     'id_jenis_barang'=> $id_jenis_barang,
            //     'harga_satuan'   => $harga_jual,
            //     'tanggal_keluar' => date('Y-m-d-H-i-s'). substr((string)microtime(), 1, 4),
            // );


            // $query1 = $this->db->get_where('t_apotek_stok',"stok_akhir <='$qty_obat'"); //cek stok tersedia
            // if ($query1->num_rows() > 0) {
            $ins = $this->Pasien_rd_model->insert_reseppasien($data_reseppasien);
            if ($ins) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Stok barang tidak mencukupi');

                    // if permitted, do logit

                    $perms = "rawat_jalan_pasienrj_view";
                    $comments = "Gagal menambahkan kode barang dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{

                // // $ins = $this->Pasien_rd_model->insert_reseppasien($data_reseppasien);
                // $ins = $this->Pasien_rd_model->insert_resep_to_apotek($data_reseppasien2);
                // $ins = $this->Pasien_rd_model->insert_reseppasien($data_reseppasien);
                // //$ins=true;
                // if($ins){
                //     // $updatestok = array(
                //     //     'penjualandetail_id' => $ins,
                //     //     'obat_id' => $obat_id,
                //     //     'tglstok_out' => date('Y-m-d'),
                //     //     'qtystok_out' => $qty_obat,
                //     //     'create_time' => date('Y-m-d H:i:s'),
                //     //     'create_user' => $this->data['users']->id
                //     // );

                //     // $this->Pasien_rd_model->insert_stokobatalkes_keluar($updatestok);
                // }

                // if ($this->db->trans_status() === FALSE){
                //     $this->db->trans_rollback();
                //     $res = array(
                //     'csrfTokenName' => $this->security->get_csrf_token_name(),
                //     'csrfHash' => $this->security->get_csrf_hash(),
                //     'success' => false,
                //     'messages' => 'Gagal menambahkan Resep, hubungi web administrator.');

                //     // if permitted, do logit
                //     $perms = "rawat_darurat_pasienrd_view";
                //     $comments = "Gagal menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                //     $this->aauth->logit($perms, current_url(), $comments);
                // }else{
                //     $this->db->trans_commit();
                //     $res = array(
                //         'csrfTokenName' => $this->security->get_csrf_token_name(),
                //         'csrfHash' => $this->security->get_csrf_hash(),
                //         'success' => true,
                //         'messages' => 'Resep berhasil ditambahkan'
                //     );

                //     // if permitted, do logit
                //     $perms = "rawat_darurat_pasienrd_view";
                //     $comments = "Berhasil menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                //     $this->aauth->logit($perms, current_url(), $comments);
                // }

                // $ins = $this->Pasien_rd_model->insert_resep_to_apotek($data_reseppasien2);
                $ins = $this->Pasien_rd_model->insert_reseppasien($data_reseppasien);
                if($ins){

                    if (!empty($dataRacikan)) {
                        $dataRacikan = json_decode($dataRacikan);
                        foreach($dataRacikan as $item) {
                            $barang_farmasi_id = $item->obat->id;
                            $jumlah = $item->jumlah;
                            $sediaan_obat_id = $item->sediaan->id;
                            $reseptur_id = $ins;

                            $dataRacikanArr = array(
                                "barang_farmasi_id" => $barang_farmasi_id,
                                "jumlah" => $jumlah,
                                "sediaan_obat_id"   => $sediaan_obat_id,
                                "reseptur_id"   => $reseptur_id
                            );
                            $this->Pasien_rd_model->insert_reseppasien_racikan($dataRacikanArr);
                        }
                    }
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Resep berhasil ditambahkan'
                    );

                    if (!empty($dataRacikan)) {
                        $dataRacikan = json_decode($dataRacikan);
                        foreach($dataRacikan as $item) {
                            $barang_farmasi_id = $item->obat->id;
                            $jumlah = $item->jumlah;
                            $sediaan_obat_id = $item->sediaan->id;
                            $reseptur_id = $ins;

                            $dataRacikanArr = array(
                                "barang_farmasi_id" => $barang_farmasi_id,
                                "jumlah" => $jumlah,
                                "sediaan_obat_id"   => $sediaan_obat_id,
                                "reseptur_id"   => $reseptur_id
                            );
                            $this->Pasien_rd_model->insert_reseppasien_racikan($dataRacikanArr);
                        }
                    }
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Resep berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "rawat_darurat_pasienrd_view";
                    $comments = "Berhasil menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "rawat_darurat_pasienrd_view";
                    $comments = "Gagal menambahkan Resep dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }
    }
    public function do_delete_resep(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        //start transaction
        $this->db->trans_begin();

        $reseptur_id = $this->input->post('reseptur_id', TRUE);

        $delete = $this->Pasien_rd_model->delete_resep($reseptur_id);
        if($delete){
            $this->Pasien_rd_model->delete_stok_resep($reseptur_id);
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal menghapus data Resep Pasien dengan ID = '". $reseptur_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
                $this->db->trans_commit();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Resep Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Berhasil menghapus Resep Pasien dengan id Resep Pasien = '". $reseptur_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }



    // batal periksa
    public function do_batal_periksa(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $delete = $this->Pasien_rd_model->delete_periksa($pendaftaran_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function get_kelasruangan_list(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_kelasruangan = $this->Pasien_rd_model->get_kelas_poliruangan($poliruangan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        foreach($list_kelasruangan as $list_kelas){
            $list .= "<option value='".$list_kelas->kelaspelayanan_id."'>".$list_kelas->kelaspelayanan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_kamarruangan(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $list_kamarruangan = $this->Pasien_rd_model->get_kamar_aktif($poliruangan_id, $kelaspelayanan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kamar</option>";
        foreach($list_kamarruangan as $list_kamar){
            $status = $list_kamar->status_bed == '1' ? 'IN USE' : 'OPEN';
            if($list_kamar->status_bed == '0'){
                $list .= "<option value='".$list_kamar->kamarruangan_id."'>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }else{
                $list .= "<option value='".$list_kamar->kamarruangan_id."' disabled>".$list_kamar->no_kamar." - ".$list_kamar->no_bed."(".$status.")</option>";
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function do_kirimke_rawatinap(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        // $this->form_validation->set_rules('pasien_id','Pasien', 'required');
        // $this->form_validation->set_rules('no_pendaftaran','Pendaftaran', 'required');
        // $this->form_validation->set_rules('poli_ruangan','Ruangan', 'require/d');
        // $this->form_validation->set_rules('kelas_pelayanan','Kelas Pelayanan', 'required');
        // $this->form_validation->set_rules('kamarruangan','Kamar', 'required');
        // $this->form_validation->set_rules('dokter_ri', 'Dokter Penanggung jawab', 'required');
        // $this->form_validation->set_rules('dokter_anastesi_ri', 'Dokter Anastesi', 'required');
        // $this->form_validation->set_rules('perawat_ri', 'Perawat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal mengirim pasien ke rawat inap dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan = $this->input->post('kamarruangan', TRUE);

            //start transaction kirim ke RI
            $this->db->trans_begin();

            $dataadmisi['kelaspelayanan_id'] = $kelaspelayanan;
            $dataadmisi['pendaftaran_id'] = $pendaftaran_id;
            $dataadmisi['pasien_id'] = $pasien_id;
            $dataadmisi['no_masukadmisi'] = $no_pendaftaran;
            $dataadmisi['tgl_masukadmisi'] = date('Y-m-d H:i:s');
            $dataadmisi['statusrawat'] = "SEDANG RAWAT INAP";
            $dataadmisi['poli_ruangan_id'] = $poliruangan;
            $dataadmisi['kamarruangan_id'] = $kamarruangan;
            $dataadmisi['create_time'] = date('Y-m-d H:i:s');
            $dataadmisi['dokter_id'] = $this->input->post('dokter_ri',TRUE);
            $dataadmisi['dokteranastesi_id'] = $this->input->post('dokter_anastesi_ri',TRUE);
            $dataadmisi['dokterperawat_id'] = $this->input->post('perawat_ri',TRUE);
            $dataadmisi['create_by'] = $this->data['users']->id;
            $insert_admisi = $this->Pasien_rd_model->insert_admisi($dataadmisi);
            if ($insert_admisi){
                $masukkamar['poliruangan_id'] =  $poliruangan;
                $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
                $masukkamar['kamarruangan_id'] =  $kamarruangan;
                $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                $masukkamar['pasienadmisi_id'] =  $insert_admisi;
                $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                $masukkamar['create_user'] =  $this->data['users']->id;
                $this->Pasien_rd_model->insert_kamar($masukkamar);

                $updatekamar['status_bed'] = '1';
                $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
                $updatependaftaran['status_periksa'] = "SEDANG RAWAT INAP";
                $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengirim pasien, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal mengirim pasien dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $this->db->trans_commit();
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Pasien berhasil dikirim ke rawat inap'
                );

                // if permitted, do logit
                $perms    = "rawat_darurat_pasienrd_view";
                $comments = "Pasien berhasil dikirim ke rawat inap dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
    }


    // insert rawat inap
    function do_kirimke_rawatinap_conflict(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        $this->form_validation->set_rules('ruangan','Ruangan', 'required');
        $this->form_validation->set_rules('rencana_tindakan[]','Rencana Tindakan', 'required');
        $this->form_validation->set_rules('pasien_id','Pasien', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error
            );

            // if permitted, do logit
            $perms      = "rawat_darurat_pasienrd_view";
            $comments   = "Gagal mengirim pasien ke rawat inap dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id      = $this->input->post('pasien_id', TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran', TRUE);
            $poliruangan    = $this->input->post('poli_ruangan', TRUE);
            $kelaspelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $kamarruangan   = $this->input->post('kamarruangan', TRUE);
            $ruangan        = $this->input->post('ruangan', TRUE);
            $tindakan       = $this->input->post('rencana_tindakan[]', TRUE);
            $harga_tindakan = $this->input->post('harga_tindakan_rd', TRUE);
            $nama_tindakan  = $this->input->post('nama_tindakan_rd', TRUE);
            $discount_tindakan = $this->input->post('discount_tindakan_rd', TRUE);
            $no_rekam_medis = $this->input->post('no_rekam_medis_rd', TRUE);
            //insert to table tpp
            $this->db->trans_start();
            $data = array(
                'tanggal'           => date('Y-m-d'),
                'pendaftaran_id'    => $pendaftaran_id,
                'pasien_id'         => $pasien_id,
                'no_rekam_medis'    => $no_rekam_medis,
                'jenis_permintaan'  => 1,
                'status_approve'    => 'Pending',
                'ruangan'           => $ruangan,
                'created_by'        => $this->data['users']->id
            );
            $ins = $this->Pasien_rd_model->insert_tpp($data);

            //update table pendaftaran
            $updatependaftaran['status_periksa'] = "SEDANG PROSES DI TPP";
            $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);

            //insert to det tpp
            //define rencana tindakan
            $tpp_id = $ins;
            $arrHargaTindakan    = explode(',',$harga_tindakan);
            $arrDiscountTindakan = explode(',',$discount_tindakan);
            $arrNamaTindakan     = explode(',',$nama_tindakan);
            for($i = 0;$i < sizeof($arrHargaTindakan);$i++){
                $data = array(
                    'det_tpp_id'     => $tpp_id,
                    'tindakan_id'    => $tindakan[$i],
                    'tindakan_nama'  => $arrNamaTindakan[$i],
                    'harga'          => $arrHargaTindakan[$i],
                    'discount'       => $arrDiscountTindakan[$i]
                );
                $ins = $this->Pasien_rd_model->insert_det_tpp($data);
            }
            $this->db->trans_complete();


            //start transaction kirim ke RI
            // $this->db->trans_begin();

            // $dataadmisi['kelaspelayanan_id'] = $kelaspelayanan;
            // $dataadmisi['pendaftaran_id']    = $pendaftaran_id;
            // $dataadmisi['pasien_id']         = $pasien_id;
            // $dataadmisi['no_masukadmisi']    = $no_pendaftaran;
            // $dataadmisi['tgl_masukadmisi']   = date('Y-m-d H:i:s');
            // $dataadmisi['statusrawat']       = "SEDANG RAWAT INAP";
            // $dataadmisi['poli_ruangan_id']   = $poliruangan;
            // $dataadmisi['kamarruangan_id']   = $kamarruangan;
            // $dataadmisi['create_time']       = date('Y-m-d H:i:s');
            // $dataadmisi['dokter_id']         = $this->input->post('dokter_ri',TRUE);
            // $dataadmisi['dokteranastesi_id'] = $this->input->post('dokter_anastesi_ri',TRUE);
            // $dataadmisi['dokterperawat_id']  = $this->input->post('perawat_ri',TRUE);
            // $dataadmisi['create_by']         = $this->data['users']->id;

            // $insert_admisi = $this->Pasien_rd_model->insert_admisi($dataadmisi);

            // if ($insert_admisi){
            //     $masukkamar['poliruangan_id']    =  $poliruangan;
            //     $masukkamar['kelaspelayanan_id'] =  $kelaspelayanan;
            //     $masukkamar['kamarruangan_id']   =  $kamarruangan;
            //     $masukkamar['tgl_masukkamar']    =  date('Y-m-d H:i:s');
            //     $masukkamar['pasienadmisi_id']   =  $insert_admisi;
            //     $masukkamar['create_time']       =  date('Y-m-d H:i:s');
            //     $masukkamar['create_user']       =  $this->data['users']->id;

            //     $this->Pasien_rd_model->insert_kamar($masukkamar);

            //     $updatekamar['status_bed'] = '1';
            //     $this->Pasien_rd_model->update_kamar($updatekamar, $kamarruangan);
            //     $updatependaftaran['status_periksa'] = "SEDANG RAWAT INAP";
            //     $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);
            // }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal mengirim pasien, hubungi web administrator.');

                // if permitted, do logit
                $perms    = "rawat_darurat_pasienrd_view";
                $comments = "Gagal mengirim pasien dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Pasien berhasil dikirim ke rawat inap'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Pasien berhasil dikirim ke rawat inap dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }



    // insert to penunjang


    public function kirim_ke_penunjang(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id_','Pendaftaran', 'required');
        $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        //$this->form_validation->set_rules('dokter_id','Penunjang', 'required');


        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal mengirim pasien ke penunjang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id_', TRUE);
            $penunjang_id = $this->input->post('penunjang_id', TRUE);
            //$dokter_id = $this->input->post('dokter_id', TRUE);
            $catatan = $this->input->post('catatan', TRUE);
            $last_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);

            if(count($last_pendaftaran) > 0){
                $datapenunjang['kelaspelayanan_id'] = $last_pendaftaran->kelaspelayanan_id;
                $datapenunjang['pendaftaran_id'] = $last_pendaftaran->pendaftaran_id;
                $datapenunjang['pasien_id'] = $last_pendaftaran->pasien_id;
                $datapenunjang['no_masukpenunjang'] = $last_pendaftaran->no_pendaftaran;
                $datapenunjang['tgl_masukpenunjang'] = date('Y-m-d H:i:s');
                $datapenunjang['statusperiksa'] = "ANTRIAN";
                $datapenunjang['poli_ruangan_asal'] = $last_pendaftaran->poliruangan_id;
                if($penunjang_id == 6){
                    $keberadaan = "RADIOLOGI";
                }else {
                    $keberadaan = "LABORATORIUM";
                }
                $datapenunjang['poli_ruangan_id']   = $penunjang_id;
                $datapenunjang['status_keberadaan'] = $keberadaan;
                $datapenunjang['dokterpengirim_id'] = $last_pendaftaran->dokter_id;
                $datapenunjang['catatan'] = $catatan;
                $datapenunjang['create_time'] = date('Y-m-d H:i:s');
                $datapenunjang['create_by'] = $this->data['users']->id;
                $insert_penunjang = $this->Pasien_rd_model->insert_penunjang($datapenunjang);
            }

            if(isset($insert_penunjang)){
                $this->Pasien_rd_model->update_to_penunjang($pendaftaran_id);
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Pasien Berhasil dikirim ke penunjang'
                );
                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Pasien Berhasil dikirim ke penunjang";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal mengirim pasien ke penunjang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal mengirim pasien ke penunjang";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }



    //insert pulang


    public function set_status_pulang(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id_pulang','Pendaftaran', 'required');
        $this->form_validation->set_rules('status_pulang','Status Pulang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms      = "rawat_darurat_pasienrd_view";
            $comments   = "Gagal set status pulang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id_pulang', TRUE);
            $status_pulang  = $this->input->post('status_pulang', TRUE);
            $nama_rs        = $this->input->post('nama_rs', TRUE);
            $alasan_rujuk   = $this->input->post('alasan_rujuk', TRUE);
            $user_id        = $this->data['users']->id;

            $data = array(
                'status_periksa' => 'DIPULANGKAN',
                'carapulang'     => $status_pulang,
                'rs_rujukan'     => $nama_rs,
                'petugas_id'     => $user_id
                // 'alasan_rujuk'   => $alasan_rujuk
            );
            $update_pendaftaran = $this->Pasien_rd_model->update_pendaftaran($data, $pendaftaran_id);

            if($update_pendaftaran){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Berhasil set status pulang'
                );
                // if permitted, do logit
                $perms      = "rawat_darurat_pasienrd_view";
                $comments   = "Berhasil set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Gagal set status pulang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms      = "rawat_darurat_pasienrd_view";
                $comments   = "Gagal set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    // insert ok
    public function do_kirim_ok(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id_ok','Pendaftaran', 'required');
        $this->form_validation->set_rules('pasien_id_ok','Status Pulang', 'required');
        $this->form_validation->set_rules('dokter_pj_ok','Dokter DPJD OK', 'required');
        $this->form_validation->set_rules('jam_operasi_ok','Jam Operasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal set status pulang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id_ok', TRUE);
            $pasien_id      = $this->input->post('pasien_id_ok', TRUE);
            $dokter_id      = $this->input->post('dokter_pj_ok', TRUE);
            $jam_operasi    = $this->input->post('jam_operasi_ok', TRUE);
            $no_rekam_medis = $this->input->post('no_rekam_medis_ok', TRUE);

            $this->db->trans_start();

            //insert to table kirim ok
            $data = array(
                'tanggal'          => date('Y-m-d'),
                'pendaftaran_id'   => $pendaftaran_id,
                'pasien_id'        => $pasien_id,
                'no_rekam_medis'   => $no_rekam_medis,
                'jenis_permintaan' => 2,
                'status_approve'   => 'Pending',
                'dokter_id'        => $dokter_id,
                'jam_operasi'      => $jam_operasi,
                'created_by'       => $this->data['users']->id
            );
            $ins = $this->Pasien_rd_model->insert_tpp($data);

            //update table pendaftaran
            $updatependaftaran['status_periksa'] = "SEDANG PROSES DI OK";
            $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);

            // die();
            $this->db->trans_complete();


            if($this->db->trans_status() === FALSE){
            // if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Gagal kirim OK, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms      = "rawat_darurat_pasienrd_view";
                $comments   = "Gagal kirim OK";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Berhasil kirim OK'
                );
                // if permitted, do logit
                $perms      = "rawat_darurat_pasienrd_view";
                $comments   = "Berhasil kirim OK";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    //insert vk
    public function do_kirim_vk(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id_vk','Pendaftaran', 'required');
        $this->form_validation->set_rules('pasien_id_vk','Status Pulang', 'required');
        $this->form_validation->set_rules('dokter_pj_vk','Dokter DPJD VK', 'required');
        $this->form_validation->set_rules('jam_operasi_vk','Jam Operasi', 'required');
        $this->form_validation->set_rules('jenis_operasi_vk','Jenis Operasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal set status pulang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id_pulang', TRUE);
            $status_pulang = $this->input->post('status_pulang', TRUE);
            $user_id = $this->data['users']->id;
            $nama_rs = $this->input->post('nama_rs', TRUE);
            $data = array(
                'status_periksa' => 'DIPULANGKAN',
                'carapulang' => $status_pulang,
                'rs_rujukan' => $nama_rs,
                'petugas_id' => $user_id
            );
            $update_pendaftaran = $this->Pasien_rd_model->update_pendaftaran($data, $pendaftaran_id);
            $pendaftaran_id = $this->input->post('pendaftaran_id_vk', TRUE);
            $pasien_id      = $this->input->post('pasien_id_vk', TRUE);
            $dokter_id      = $this->input->post('dokter_pj_vk', TRUE);
            $no_rekam_medis = $this->input->post('no_rekam_medis_vk', TRUE);
            $tindakan_id    = $this->input->post('jenis_operasi_vk', TRUE);
            $jam_operasi    = $this->input->post('jam_operasi_vk', TRUE);
            $harga_tindakan = $this->input->post('harga_tindakan_vk', TRUE);
            $discount       = $this->input->post('discount_vk', TRUE);


            $this->db->trans_start();

            //insert to table kirim vk
            $data = array(
                'tanggal'          => date('Y-m-d'),
                'pendaftaran_id'   => $pendaftaran_id,
                'pasien_id'        => $pasien_id,
                'no_rekam_medis'   => $no_rekam_medis,
                'jenis_permintaan' => 3,
                'status_approve'   => 'Pending',
                'dokter_id'        => $dokter_id,
                'jam_operasi'      => $jam_operasi,
                'created_by'       => $this->data['users']->id
            );
            $ins = $this->Pasien_rd_model->insert_tpp($data);

            // insert to det tpp
            $tpp_id = $ins;
            $data_tindakan = array(
                'det_tpp_id'    => $tpp_id,
                'tindakan_id'   => $tindakan_id,
                'tindakan_nama' => '',
                'harga'         => $harga_tindakan,
                'discount'      => $discount
            );
            $ins_tindakan = $this->Pasien_rd_model->insert_det_tpp($data_tindakan);


            // update table pendaftaran
            $updatependaftaran['status_periksa'] = "SEDANG PROSES DI TPP";
            $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $pendaftaran_id);

            $this->db->trans_complete();

            if($update_pendaftaran){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Berhasil set status pulang'
                );
                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal set status pulang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_paket_operasi(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id_po','Pendaftaran', 'required');
        $this->form_validation->set_rules('pasien_id_po','Status Pulang', 'required');
        $this->form_validation->set_rules('paket_operasi_po','Paket Operasi', 'required');
        $this->form_validation->set_rules('poli_ruangan_po','Ruangan', 'required');
        $this->form_validation->set_rules('kamarruangan_po','Kamar', 'required');
        $this->form_validation->set_rules('dokter_po','Dokter', 'required');
        $this->form_validation->set_rules('dokter_anastesi_po','Dokter Anastesi', 'required');
        $this->form_validation->set_rules('perawat_po','Perawat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal kirim paket operasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $pendaftaran_id = $this->input->post('pendaftaran_id_po', TRUE);
            $pasien_id = $this->input->post('pasien_id_po', TRUE);
            $status_pulang = 'MRS';
            $user_id = $this->data['users']->id;

            $data = array(
                'status_periksa' => 'SEDANG DI OPERASI',
                'carapulang' => $status_pulang,
                'petugas_id' => $user_id
            );
            //start transaction pendaftaran
            $this->db->trans_begin();

            $update_pendaftaran = $this->Pasien_rd_model->update_pendaftaran($data, $pendaftaran_id);
            if($update_pendaftaran){
                $get_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
                if(count($get_pendaftaran) > 0){
                    $datapendaftaran['tgl_pendaftaran'] = date('Y-m-d H:i:s');
                    $datapendaftaran['status_periksa'] = "ANTRIAN";
                    $datapendaftaran['status_pasien'] = 1;
                    $datapendaftaran['pasien_id'] = $pasien_id;
                    $datapendaftaran['instalasi_id'] = 3; //rawat inap
                    $datapendaftaran['poliruangan_id'] = $this->input->post('poli_ruangan_po',TRUE);
                    $datapendaftaran['dokter_id'] = $this->input->post('dokter_po',TRUE);
                    //instalasi_id = 3 rawat inap
                    $datapendaftaran['no_pendaftaran'] = $this->Pasien_rd_model->get_no_pendaftaran_ri();
                    $datapendaftaran['paketoperasi_id'] = $this->input->post('paket_operasi_po',TRUE);
                    $kelaspelayanan_id = $this->Pasien_rd_model->get_kelas_paket_operasi($datapendaftaran['paketoperasi_id']);
                    $datapendaftaran['kelaspelayanan_id'] = $kelaspelayanan_id;
                    $datapendaftaran['nama_perujuk'] = 'IGD';
                    $datapendaftaran['alamat_perujuk'] = 'IGD';
                    $datapendaftaran['penanggungjawab_id'] = $get_pendaftaran->penanggungjawab_id;
                    $datapendaftaran['pembayaran_id'] = $get_pendaftaran->pembayaran_id;
                    $datapendaftaran['status_kunjungan'] = "PASIEN LAMA";
                    $datapendaftaran['petugas_id'] = $this->data['users']->id;
                    $datapendaftaran['dokteranastesi_id'] = $this->input->post('dokter_anastesi_po',TRUE);
                    $datapendaftaran['perawat_id'] = $this->input->post('perawat_po',TRUE);
                    $insert_pendaftaran = $this->Pasien_rd_model->insert_pendaftaran($datapendaftaran);

                    if($insert_pendaftaran){
                        $new_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($insert_pendaftaran);

                        $dataadmisi['kelaspelayanan_id'] = $new_pendaftaran->kelaspelayanan_id;
                        $dataadmisi['pendaftaran_id'] = $new_pendaftaran->pendaftaran_id;
                        $dataadmisi['pasien_id'] = $new_pendaftaran->pasien_id;
                        $dataadmisi['no_masukadmisi'] = $new_pendaftaran->no_pendaftaran;
                        $dataadmisi['tgl_masukadmisi'] = date('Y-m-d H:i:s');
                        $dataadmisi['statusrawat'] = "SEDANG RAWAT INAP";
                        $dataadmisi['poli_ruangan_id'] = $new_pendaftaran->poliruangan_id;
                        $dataadmisi['kamarruangan_id'] = $this->input->post('kamarruangan_po',TRUE);
                        $dataadmisi['create_time'] = date('Y-m-d H:i:s');
                        $dataadmisi['dokter_id'] = $new_pendaftaran->dokter_id;
                        $dataadmisi['dokteranastesi_id'] = $this->input->post('dokter_anastesi_po',TRUE);
                        $dataadmisi['dokterperawat_id'] = $this->input->post('perawat_po',TRUE);
                        $dataadmisi['create_by'] = $this->data['users']->id;
                        $insert_admisi = $this->Pasien_rd_model->insert_admisi($dataadmisi);

                        if ($insert_admisi){
                            $masukkamar['poliruangan_id'] =  $new_pendaftaran->poliruangan_id;
                            $masukkamar['kelaspelayanan_id'] =  $new_pendaftaran->kelaspelayanan_id;
                            $masukkamar['kamarruangan_id'] =  $this->input->post('kamarruangan_po',TRUE);
                            $masukkamar['tgl_masukkamar'] =  date('Y-m-d H:i:s');
                            $masukkamar['pasienadmisi_id'] =  $insert_admisi;
                            $masukkamar['create_time'] =  date('Y-m-d H:i:s');
                            $masukkamar['create_user'] =  $this->data['users']->id;
                            $this->Pasien_rd_model->insert_kamar($masukkamar);

                            $updatekamar['status_bed'] = '1';
                            $this->Pasien_rd_model->update_kamar($updatekamar, $dataadmisi['kamarruangan_id']);
                            $updatependaftaran['status_periksa'] = "SEDANG RAWAT INAP";
                            $this->Pasien_rd_model->update_pendaftaran($updatependaftaran, $insert_pendaftaran);
                        }

                        $tariftindakan = $this->Pasien_rd_model->get_tarif_tindakan('2');
                        $data_tindakanpasien = array(
                            'pendaftaran_id' => $insert_pendaftaran,
                            'pasien_id' => $new_pendaftaran->pasien_id,
                            'daftartindakan_id' => TARIF_PENDAFTARAN,
                            'jml_tindakan' => '1',
                            'total_harga_tindakan' => $tariftindakan->harga_tindakan,
                            'is_cyto' => '0',
                            'total_harga' => $tariftindakan->harga_tindakan,
                            'dokter_id' => null,
                            'tgl_tindakan' => date('Y-m-d'),
                            'ruangan_id' => $new_pendaftaran->poliruangan_id,
                            'petugas_id' => $this->data['users']->id
                        );
                        $tindakanpendafataran = $this->Pasien_rd_model->insert_tindakanpasien_po($data_tindakanpasien);
                        if($tindakanpendafataran){
                            $this->Pasien_rd_model->insert_tindakan_paket($new_pendaftaran->paketoperasi_id,$insert_pendaftaran,$new_pendaftaran->pasien_id,$new_pendaftaran->dokter_id,$new_pendaftaran->dokteranastesi_id,$new_pendaftaran->perawat_id);
                            $this->Pasien_rd_model->insert_obat_paket($new_pendaftaran->paketoperasi_id, $insert_pendaftaran, $new_pendaftaran->pasien_id, $new_pendaftaran->dokter_id);
                        }
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => true,
                            'messages' => 'Pendaftaran Paket Operasi Berhasil.'
                        );
                        // if permitted, do logit
                        $perms = "rawat_darurat_pasienrd_view";
                        $comments = "Pendaftaran Paket Operasi Berhasil";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }else{
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'messages' => 'Pendaftaran Paket Operasi Gagal, silakan hubungi web administrator.'
                        );
                        // if permitted, do logit
                        $perms = "rawat_darurat_pasienrd_view";
                        $comments = "Pendaftaran Paket Operasi Gagal";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }
                }else{
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => 'Data Pendaftaran tidak ditemukan, silakan hubungi web administrator.'
                    );
                    // if permitted, do logit
                    $perms = "rawat_darurat_pasienrd_view";
                    $comments = "Data Pendaftaran tidak ditemukan";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal set status pulang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal set status pulang";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }
        echo json_encode($res);
    }

	public function ajax_list_obat(){
		$data_obat = $this->Pasien_rd_model->get_list_obat();

		$data = array();
		foreach($data_obat as $obat){
			$row = array();

			$row[] = $obat->nama_barang;
			$row[] = $obat->nama_jenis;
			$row[] = $obat->nama_sediaan;
			$row[] = number_format($obat->harga_satuan);
			$row[] = number_format($obat->stok_akhir);
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihObat('."'".$obat->kode_barang."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" =>   $this->Pasien_rd_model->count_list_obat(),
			"recordsFiltered" =>   $this->Pasien_rd_model->count_list_filtered_obat(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}

  public function ajax_list_dokter_tindakan(){
    $data_list_dokter = $this->Pasien_rd_model->get_list_dokter();
    $id = 1;
    $data = array();
    foreach($data_list_dokter as $dokter){
      $row = array();

      $row[] = $dokter->NAME_DOKTER;
      $row[] = $dokter->kelompokdokter_nama;
      $row[] = '<button class="btn btn-info btn-circle" onclick="pilihListDokter('."'".$dokter->id_M_DOKTER."'".','."'".$id."'".')"><i class="fa fa-check"></i></button>';

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->get('draw'),
      "recordsTotal" =>   $this->Pasien_rd_model->count_list_dokter(),
      "recordsFiltered" =>   $this->Pasien_rd_model->count_list_filtered_dokter(),
      "data" => $data,
    );
        //output to json format
        echo json_encode($output);
  }

  public function ajax_list_user_tindakan(){
    $data_list_dokter = $this->Pasien_rd_model->get_list_dokter();
    $id = 1;
    $data = array();
    foreach($data_list_dokter as $dokter){
      $row = array();

      $row[] = $dokter->NAME_DOKTER;
      $row[] = $dokter->kelompokdokter_nama;
      $row[] = '<button class="btn btn-info btn-circle" onclick="pilihListDokter('."'".$dokter->id_M_DOKTER."'".','."'".$id."'".')"><i class="fa fa-check"></i></button>';

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->get('draw'),
      "recordsTotal" =>   $this->Pasien_rd_model->count_list_dokter(),
      "recordsFiltered" =>   $this->Pasien_rd_model->count_list_filtered_dokter(),
      "data" => $data,
    );
        //output to json format
        echo json_encode($output);
  }

  public function ajax_list_dokter1(){
		$data_list_dokter = $this->Pasien_rd_model->get_list_dokter();
    $id = 1;
		$data = array();
		foreach($data_list_dokter as $dokter){
			$row = array();

			$row[] = $dokter->NAME_DOKTER;
			$row[] = $dokter->kelompokdokter_nama;
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihListDokter('."'".$dokter->id_M_DOKTER."'".','."'".$id."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" =>   $this->Pasien_rd_model->count_list_dokter(),
			"recordsFiltered" =>   $this->Pasien_rd_model->count_list_filtered_dokter(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}

  public function ajax_list_dokter2(){
		$data_list_dokter = $this->Pasien_rd_model->get_list_dokter();
    $id = 2;
		$data = array();
		foreach($data_list_dokter as $dokter){
			$row = array();

			$row[] = $dokter->NAME_DOKTER;
			$row[] = $dokter->kelompokdokter_nama;
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihListDokter('."'".$dokter->id_M_DOKTER."'".','."'".$id."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" =>   $this->Pasien_rd_model->count_list_dokter(),
			"recordsFiltered" =>   $this->Pasien_rd_model->count_list_filtered_dokter(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}

  public function ajax_list_dokter3(){
		$data_list_dokter = $this->Pasien_rd_model->get_list_dokter();
    $id = 3;
		$data = array();
		foreach($data_list_dokter as $dokter){
			$row = array();

			$row[] = $dokter->NAME_DOKTER;
			$row[] = $dokter->kelompokdokter_nama;
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihListDokter('."'".$dokter->id_M_DOKTER."'".','."'".$id."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" =>   $this->Pasien_rd_model->count_list_dokter(),
			"recordsFiltered" =>   $this->Pasien_rd_model->count_list_filtered_dokter(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}

	public function ajax_get_obat_by_id(){
		$kode_barang = $this->input->get('kode_barang',TRUE);

		$data_obat = $this->Pasien_rd_model->get_obat_by_id($kode_barang);
		if (sizeof($data_obat)>0){
			$obat = $data_obat[0];

			$res = array(
				"success" => true,
				"messages" => "Data obat ditemukan",
				"data" => $obat
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data obat tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
	}

  public function ajax_get_dokter_by_id(){
		$dokter_id = $this->input->get('id_M_DOKTER',TRUE);

		$data_dokter = $this->Pasien_rd_model->get_dokter_by_id($dokter_id);
		if (sizeof($data_dokter)>0){
			$dokter = $data_dokter[0];

			$res = array(
				"success" => true,
				"messages" => "Data Dokter ditemukan",
				"data" => $dokter
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data Dokter tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
	}

  public function ajax_get_diagnosa_by_id(){
    $id = $this->input->get('diagnosa2_id',TRUE);

    $get_data = $this->Pasien_rd_model->get_diagnosa_by_id($id);
    if (sizeof($get_data)>0){
      $result = $get_data[0];

      $res = array(
        "success" => true,
        "messages" => "Data Dokter ditemukan",
        "data" => $result
      );
    } else {
      $res = array(
        "success" => false,
        "messages" => "Data Dokter tidak ditemukan",
        "data" => null
      );
    }
    echo json_encode($res);
  }

  public function ajax_get_diagnosa_icd10_by_id(){
    $id = $this->input->get('diagnosa_id',TRUE);

    $get_data = $this->Pasien_rd_model->get_diagnosa_icd10_by_id($id);
    if (sizeof($get_data)>0){
      $result = $get_data[0];

      $res = array(
        "success" => true,
        "messages" => "Data Dokter ditemukan",
        "data" => $result
      );
    } else {
      $res = array(
        "success" => false,
        "messages" => "Data Dokter tidak ditemukan",
        "data" => null
      );
    }
    echo json_encode($res);
  }

    public function keterangan_keluar($pendaftaran_id){
        $pasienrd['list_pasien'] = $this->Pasien_rd_model->get_pendaftaran_pasien($pendaftaran_id);

        $this->load->view('v_pulang_pasienrd', $pasienrd);


    }
    public function ajax_get_tindakan_by_id(){
		$tindakan_id = $this->input->get('tindakan_id',TRUE);

		$data_tindakan = $this->Pasien_rd_model->get_tindakan_by_id($tindakan_id);
		if (sizeof($data_tindakan)>0){
			$tindakan = $data_tindakan[0];

			$res = array(
				"success"  => true,
				"messages" => "Tindakan ditemukan",
				"data"     => $tindakan
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data Tindakan tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
    }

    public function ajax_get_pasien_rd_by_id(){
		$pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
		$data_pasien = $this->Pasien_rd_model->get_pendaftaran_pasien_by_id($pendaftaran_id);
		if (sizeof($data_pasien)>0){
			$pasien = $data_pasien[0];

			$res = array(
				"success"  => true,
				"messages" => "Tindakan ditemukan",
				"data"     => $pasien
			);
		} else {
			$res = array(
				"success"  => false,
				"messages" => "Data Tindakan tidak ditemukan",
				"data"     => null
			);
		}
		echo json_encode($res);
    }


    public function printResep($id){
        $data['resep_list'] = $this->Pasien_rd_model->get_resep_pasien_list($id);
        $this->load->view('rawatdarurat/v_print_resep', $data);
    }


    public function do_create_assestment_ok(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('tensiok','Tensi', 'required|trim');
        $this->form_validation->set_rules('suhuok','Suhu', 'required');
        // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        //$this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('tensiok', TRUE);
            $suhu = $this->input->post('suhuok', TRUE);
            $nadi = $this->input->post('nadiok', TRUE);
            $nadi_2 = $this->input->post('nadi_1ok', TRUE);
            $penggunaan = $this->input->post('penggunaanok', TRUE);
            $saturasi = $this->input->post('saturasiok', TRUE);
            $nyeri = $this->input->post('nama_nyeriok', TRUE);
            $numeric = $this->input->post('numericok', TRUE);
            $resiko = $this->input->post('resikook', TRUE);
            $theraphyArr = $this->input->post('theraphyArr', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'pendaftaran_id' => $pendaftaran_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'nadi_2' => $nadi_2,
                'suhu' => $suhu,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko,
                'pasien_id' => $data_pendaftaran->pasien_id,
                'instalasi_id' => '6',

                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->Pasien_rd_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_detassestment($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }



    public function do_create_assestment_hcu(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        // $this->form_validation->set_rules('tensihcu','Tensi', 'required|trim');
        // $this->form_validation->set_rules('suhuhcu','Suhu', 'required');
        // // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        $this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('tensihcu', TRUE);
            $suhu = $this->input->post('suhuhcu', TRUE);
            $nadi = $this->input->post('nadihcu', TRUE);
            $nadi_2 = $this->input->post('nadi_1hcu', TRUE);
            $penggunaan = $this->input->post('penggunaanhcu', TRUE);
            $saturasi = $this->input->post('saturasihcu', TRUE);
            $nyeri = $this->input->post('nama_nyerihcu', TRUE);
            $numeric = $this->input->post('numerichcu', TRUE);
            $resiko = $this->input->post('resikohcu', TRUE);
            $theraphyArr = $this->input->post('theraphyArr', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'pendaftaran_id' => $pendaftaran_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'nadi_2' => $nadi_2,
                'suhu' => $suhu,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko,
                'pasien_id' => $data_pendaftaran->pasien_id,
                'instalasi_id' => '8',

                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->Pasien_rd_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_detassestment($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_create_assestment_pathologi(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        // $this->form_validation->set_rules('tensihcu','Tensi', 'required|trim');
        // $this->form_validation->set_rules('suhuhcu','Suhu', 'required');
        // // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        $this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('tensipathologi', TRUE);
            $suhu = $this->input->post('suhupathologi', TRUE);
            $nadi = $this->input->post('nadipathologi', TRUE);
            $nadi_2 = $this->input->post('nadi_1pathologi', TRUE);
            $penggunaan = $this->input->post('penggunaanpathologi', TRUE);
            $saturasi = $this->input->post('saturasipathologi', TRUE);
            $nyeri = $this->input->post('nama_nyeripathologi', TRUE);
            $numeric = $this->input->post('numericpathologi', TRUE);
            $resiko = $this->input->post('resikopathologi', TRUE);
            $theraphyArr = $this->input->post('theraphyArr', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'pendaftaran_id' => $pendaftaran_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'nadi_2' => $nadi_2,
                'suhu' => $suhu,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko,
                'pasien_id' => $data_pendaftaran->pasien_id,
                'instalasi_id' => '9',

                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->Pasien_rd_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_detassestment($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }



    public function do_create_assestment_rujukrs(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        // $this->form_validation->set_rules('tensihcu','Tensi', 'required|trim');
        // $this->form_validation->set_rules('suhuhcu','Suhu', 'required');
        // // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        $this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('tensirujuk', TRUE);
            $suhu = $this->input->post('suhurujuk', TRUE);
            $nadi = $this->input->post('nadirujuk', TRUE);
            $nadi_2 = $this->input->post('nadi_1rujuk', TRUE);
            $penggunaan = $this->input->post('penggunaanrujuk', TRUE);
            $saturasi = $this->input->post('saturasirujuk', TRUE);
            $nyeri = $this->input->post('nama_nyerirujuk', TRUE);
            $numeric = $this->input->post('numericrujuk', TRUE);
            $resiko = $this->input->post('resikorujuk', TRUE);
            $theraphyArr = $this->input->post('theraphyArr', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'pendaftaran_id' => $pendaftaran_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'nadi_2' => $nadi_2,
                'suhu' => $suhu,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko,
                'pasien_id' => $data_pendaftaran->pasien_id,
                 'instalasi_id' => '1',

                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->Pasien_rd_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_detassestment($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }



    public function do_create_assestment_isolasi(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        // $this->form_validation->set_rules('tensiisolasi','Tensi', 'required|trim');
        // $this->form_validation->set_rules('suhuisolasi','Suhu', 'required');
        // // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        $this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('tensiisolasi', TRUE);
            $suhu = $this->input->post('suhuisolasi', TRUE);
            $nadi = $this->input->post('nadiisolasi', TRUE);
            $nadi_2 = $this->input->post('nadi_1isolasi', TRUE);
            $penggunaan = $this->input->post('penggunaanisolasi', TRUE);
            $saturasi = $this->input->post('saturasiisolasi', TRUE);
            $nyeri = $this->input->post('nama_nyeriisolasi', TRUE);
            $numeric = $this->input->post('numericisolasi', TRUE);
            $resiko = $this->input->post('resikoisolasi', TRUE);
            $theraphyArr = $this->input->post('theraphyArr', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'pendaftaran_id' => $pendaftaran_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'nadi_2' => $nadi_2,
                'suhu' => $suhu,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko,
                'pasien_id' => $data_pendaftaran->pasien_id,
                'instalasi_id' => '7',

                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->Pasien_rd_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_detassestment($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

        public function print_resep($pendaftaran_id){
        $print['pendaftaran_id'] = $pendaftaran_id;
        $get_resep = $this->Pasien_rd_model->get_detail_resep_pasien($pendaftaran_id);
        //$print['reseptur'] = $this->Pasien_rd_model->get_pendaftran_resep_pasien($get_resep->pendaftaran_id);
         // $print['reseprd'] = $this->Pasien_rd_model->get_pendaftran_resep_pasien($pendaftaran_id);
        // $print['pembayarankasir_id'] = $pembayarankasir_id;
        // $print['tot_obat'] = $this->Kasirrd_model->sum_harga_obat_sudah_bayar($pembayarankasir_id);
         $print['tot_rad'] = $this->Pasien_rd_model->sum_total_resep($pendaftaran_id);
        // $print['tot_lab'] = $this->Kasirrd_model->sum_harga_lab_sudah_bayar($pembayarankasir_id);
        // $print['tot_tind'] = $this->Kasirrd_model->sum_tindakan_sudah_bayar($pembayarankasir_id);
        // $print['tindakan_list'] = $this->Kasirrd_model->get_tindakan_pasien($pembayarankasir_id);
        // $print['total_bayar'] = $this->Kasirrd_model->get_print_bayar($pembayarankasir_id);

        // e
        // echo "<pre>";
        // print_r($print);die();
        // echo "</pre>";

        //$this->load->view('rawatdarurat/print_reseptur', $print);

        $this->load->view('rawatdarurat/print_reseptur', $print);
    }


    public function do_create_assestment_vk(){
        $is_permit = $this->aauth->control_no_redirect('rawat_darurat_pasienrd_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Tindakan', 'required');
        $this->form_validation->set_rules('tensivk','Tensi', 'required|trim');
        $this->form_validation->set_rules('suhuvk','Suhu', 'required');
        // $this->form_validation->set_rules('subtotal','Total Harga Tindakan', 'required');
        //$this->form_validation->set_rules('pendaftaran_id','No Pendaftaran', 'required');
      //   $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $tensi = $this->input->post('tensivk', TRUE);
            $suhu = $this->input->post('suhuvk', TRUE);
            $nadi = $this->input->post('nadivk', TRUE);
            $nadi_2 = $this->input->post('nadi_1vk', TRUE);
            $penggunaan = $this->input->post('penggunaanvk', TRUE);
            $saturasi = $this->input->post('saturasivk', TRUE);
            $nyeri = $this->input->post('nama_nyerivk', TRUE);
            $numeric = $this->input->post('numericvk', TRUE);
            $resiko = $this->input->post('resikovk', TRUE);
            $theraphyArr = $this->input->post('theraphyArr', TRUE);
            $data_pendaftaran = $this->Pasien_rd_model->get_last_pendaftaran($pendaftaran_id);
            $data_pendaftaran_pasien =$this->Pasien_rd_model->get_last_pendaftaran_pasien($data_pendaftaran->pasien_id);
            $data_assestment = array(
                'pendaftaran_id' => $pendaftaran_id,
                'tensi' => $tensi,
                'nadi_1' => $nadi,
                'nadi_2' => $nadi_2,
                'suhu' => $suhu,
                'penggunaan' => $penggunaan,
                'saturasi' => $saturasi,
                'nyeri' => $nyeri,
                'numeric_wong_baker' => $numeric,
                'resiko_jatuh' => $resiko,
                'pasien_id' => $data_pendaftaran->pasien_id,
                'instalasi_id' => '5',

                //'tgl_tindakan' => date('Y-m-d'),
            );

            $ins = $this->Pasien_rd_model->insert_assestment($data_assestment);
            //$ins=true;
            if($ins != 0){
                $theraphyArr = json_decode($theraphyArr);
                if (count($theraphyArr) > 0 ) {
                    foreach ($theraphyArr as $item) {
                        $data = array(
                            'program_therapy_id' =>$item->id_nama_paket,
                            'assestment_pasien_id' => $ins
                        );

                        $rencana = $this->Pasien_rd_model->insert_detassestment($data);
                    }
                }
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Ajukan Berhasil Ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_darurat_pasienrd_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

}
