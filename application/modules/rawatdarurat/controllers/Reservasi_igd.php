<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Reservasi_igd extends CI_Controller{
        public $data = array();

        public function __construct(){
            parent::__construct();
            $this->load->library("Aauth");
            $this->load->model('Menu_model');
            $this->load->model('Reservasi_model_igd');
            $this->data['users']                = $this->aauth->get_user();
            $this->data['groups']               = $this->aauth->get_user_groups();
            $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
            $this->data['list_alasan']   = array('kamar'=>'Kamar penuh','administrasi'=>'Administrasi tidak lengkap', 'lainnya'=>'Lainnya');
            // data ketersediaan kamar di header
            $this->load->model('Ketersediaan_kamar_model');
            $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
            foreach($get_data_kamar as $key => $value){
              // print_r($value);
              $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
            }
            $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;

         }

        public function index(){
            $is_permit = $this->aauth->control_no_redirect('rekam_medis_reservasi_igd_view');
            if(!$is_permit) {
                $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
                redirect('no_permission');
            }
            // if permitted, do logit
            $perms = "rekam_medis_reservasi_igd_view";
            $comments = "Listing User.";
            $this->aauth->logit($perms, current_url(), $comments);
            $this->load->view('rawatdarurat/v_reservasi_igd', $this->data);
        }

        // autocomplete pasien di reservasi
        function autocomplete_pasien(){
            $val_pasien = $this->input->post('keyword');
            if(!empty($val_pasien)){
                $pasien_list = $this->Reservasi_model_igd->get_pasien_autocomplete($val_pasien);
                if(!empty($pasien_list)) {
                    for ($i=0; $i<count($pasien_list); $i++){
                        $status_pasien = $this->Reservasi_model_igd->get_status_pendaftaran($pasien_list[$i]->pasien_id);
                        $list[] = array(
                            "id"                    => $pasien_list[$i]->pasien_id,
                            "value"                 => $pasien_list[$i]->no_rekam_medis." - ".$pasien_list[$i]->pasien_nama,
                            "no_rekam_medis"        => $pasien_list[$i]->no_rekam_medis,
                            "pasien_nama"           => $pasien_list[$i]->pasien_nama,
                            "alamat"                => $pasien_list[$i]->pasien_alamat,
                        );
                    }
                    $res = array(
                        'success' => true,
                        'messages' => "Pasien ditemukan",
                        'data' => $list
                    );
                }else{
                  // dummy list
                  $list = array(
                      "id"                    => 1,
                      "value"                 => "jos",
                      "no_rekam_medis"        => "jos",
                      "pasien_nama"           => "jos",
                      "alamat"                => "jos"
                  );
                    $res = array(
                        'success' => false,
                        'messages' => "Pasien tidak ditemukan",
                        'data' => $list
                    );
                }
            }
            echo json_encode($res);
        }

        // Insert data reservasi
        public function do_reservasi_igd_verify($id){
            $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_create');
            if(!$is_permit) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => $this->lang->line('aauth_error_no_access')
                );
                echo json_encode($res);
                exit;
            }

            $push = $this->Reservasi_model_igd->insert_reservasi_igd($id);
            // print_r($push);die;
            if($push){
              $res = array(
                  'csrfTokenName' => $this->security->get_csrf_token_name(),
                  'csrfHash' => $this->security->get_csrf_hash(),
                  'success' => true,
                  'new_id' => $push,
                  'messages' => 'data reservasi berhasil dimasukan'
              );
            }else{
              $res = array(
                  'csrfTokenName' => $this->security->get_csrf_token_name(),
                  'csrfHash' => $this->security->get_csrf_hash(),
                  'success' => false,
                  'messages' => 'data reservasi gagal dimasukan'
              );
            }
            echo json_encode($res);
        }

        public function do_create_reservasi_igd(){
            $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_create');
            if(!$is_permit) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => $this->lang->line('aauth_error_no_access'));
                echo json_encode($res);
                exit;
            }

            // reservasi form data
            $status_pasien = $this->input->post('no_rm',TRUE);
            $layanan_id = $this->input->post('layanan',TRUE);
            $tglres = $this->input->post('tgl_reservasi',TRUE);
            $tglres = $this->input->post('tgl_reservasi',TRUE);
            $tglres2 = $this->Reservasi_model_igd->casting_date_indo($tglres);
            $count_pasien_res = $this->Reservasi_model_igd->get_status_rm($status_pasien,$tglres2, $layanan_id);

            // response for testing
            // $dates = strtolower($this->input->post('tanggal_lahir',TRUE));
            // $month = "";
            // list($tgl,$bln,$thn) = explode(" ", $dates) ;
            //
            //
            // $result = array(
            //     '$tgl' => $tgl,
            //     '$bln' => $bln,
            //     '$thn' => $thn
            // );
            // echo json_encode($result);
            // exit;

            // ---------------
            // Mengecek pasien sudah melakukan reservasi
            if ($count_pasien_res >= 1) {
                $result = array(
                    'count' => $count_pasien_res,
                    'messages' => "Pasien sudah melakukan reservasi"
                );
                echo json_encode($result);
                exit;
            }

            // Form validation
            $this->load->library('form_validation');
            $this->form_validation->set_rules('pasien_nama', 'Nama Pasien', 'required|trim');
            $this->form_validation->set_rules('no_rm', 'No Rekam Medis', 'required|trim');
            // $this->form_validation->set_rules('jk', 'jenis_kelamin', 'required|trim');
            // $this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'required|trim');
            // $this->form_validation->set_rules('metode_regis', 'metode metode_registrasi', 'required|trim');
            $this->form_validation->set_rules('tgl_reservasi', 'tanggal Reservasi', 'required|trim');
            // $this->form_validation->set_rules('layanan', 'Layanan', 'required|trim');

            if ($this->form_validation->run() == FALSE)  {
                $error = validation_errors();
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => $error);

                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Gagal melakukan pendaftaran with errors = '". validation_errors('', "\n") ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else if ($this->form_validation->run() == TRUE){
                // perujuk form data
                $id_perujuk = $this->input->post('id_perujuk',TRUE);
                $rm_noteleponperujuk = $this->input->post('rm_noteleponperujuk',TRUE);
                // $rm_alamatperujuk = $this->input->post('rm_alamatperujuk',TRUE);
                $datapasien['perujuk_id'] = $this->input->post('id_perujuk',TRUE);
                // set local time to indoesia
                date_default_timezone_set('Asia/Jakarta');
                setlocale(LC_ALL, 'IND');
                setlocale(LC_ALL, 'id_ID');
                $no_book = $this->Reservasi_model_igd->get_no_booking();
                $datapasien['kode_booking'] = $no_book;
                $datapasien['nama_pasien'] = $this->input->post('pasien_nama',TRUE);
                // $tgl_lahir1 = $this->input->post('tanggal_lahir',TRUE);
                // $date_lhr = $this->Reservasi_model_igd->casting_date_indo(strtolower($tgl_lahir1));
                // $datapasien['tanggal_lahir'] = $date_lhr;
                // $datapasien['jenis_kelamin'] = $this->input->post('jk',TRUE);
                // $datapasien['alamat_pasien'] = $this->input->post('alamat',TRUE);
                // $metode_regis = $this->input->post('metode_regis',TRUE);
                // $datapasien['metode_registrasi'] = $metode_regis;
                $tgl_res1= $this->input->post('tgl_reservasi',TRUE);
                $date_res = $this->Reservasi_model_igd->casting_date_indo(strtolower($tgl_res1));
                $datapasien['tanggal_reservasi'] = $date_res;
                // $datapasien['kelas_pelayanan_id'] = $this->input->post('layanan',TRUE);
                // $dokter_id = $this->input->post('dokter',TRUE);
                // $datapasien['dokter_id'] = $dokter_id;
                $datapasien['jam'] = $this->input->post('jam_periksa',TRUE);
                // $datapasien['asuransi1'] = $this->input->post('asuransi1',TRUE);
                // $datapasien['asuransi2'] = $this->input->post('asuransi2',TRUE);
                // $datapasien['keterangan'] = $this->input->post('ket',TRUE);
                $datapasien['tgl_created'] = date('Y-m-d H:i:s');
                $datapasien['no_pendaftaran'] = $this->Reservasi_model_igd->get_no_pendaftaran_reg();
                // $datapasien['no_telp'] = $this->input->post('no_telp',TRUE);
                $is_pasien_baru = $this->input->post('is_checked',TRUE);
                $data_reg['no_reg'] = $this->Reservasi_model_igd->get_no_pendaftaran_reg();
                $this->Reservasi_model_igd->insert_reg($data_reg);

                $no_rm = "";
                if($is_pasien_baru == "1") {
                    $status_pasien = "PASIEN BARU";
                    $no_rm = $this->Reservasi_model_igd->get_no_rm();
                    $data_rm['no_rekam_medis'] =$no_rm;
                    $this->Reservasi_model_igd->insert_rm($data_rm);
                }else{
                  // if($string NOT LIKE %word%) { do something }
                    $no_rm = $this->input->post('no_rm',TRUE);
                    $status_pasien = "PASIEN LAMA";
                }
                $datapasien['no_rekam_medis'] = $no_rm;
                // if ($metode_regis == "LANGSUNG") {
                //           $status = "HADIR";
                //     }else{
                //         $status = "";
                //     }
                // $datapasien['status'] = $status;
                $datapasien['status_pasien'] = $status_pasien;
                $no_urut = $this->Reservasi_model_igd->get_no_urut($date_res); // no urut tanpa dokter. daftar igd belum pilih dokter
                $datapasien['no_urut'] = $no_urut;
                // $datapasien['sumber_reservasi'] = 'IGD'; // perbedaan IGD & biasa disini
                $push = $this->Reservasi_model_igd->insert_pasien($datapasien);
                if($push){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Register has been saved to database'
                    );
                    // if permitted, do logit
                    $perms = "rekam_medis_pendaftaran_create";
                    $comments = "Success to Create a new patient register with post data";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
                else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Failed insert user group to database, please contact web administrator.');
                    // if permitted, do logit
                    $perms = "rekam_medis_pendaftaran_create";
                    $comments = "Failed to Create a new patient register when saving to database";
                    $this->aauth->logit($perms, current_url(), $comments);
                }

                //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                }else{
                    $this->db->trans_commit();
                }
            }
            echo json_encode($res);
        }

        // fungsi batal data reservasi
        function do_batal_reservasi($id){
            $is_permit = $this->aauth->control_no_redirect('rekam_medis_reservasi_update');
            if(!$is_permit) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => $this->lang->line('aauth_error_no_access'));
                echo json_encode($res);
                exit;
            }
            $datares['status'] = "BATAL";
            $datares['tgl_verif'] = date('Y-m-d H:i:s');
            $update = $this->Reservasi_model_igd->update_reservasi_igd($datares, $id);

            // Jika batal reservasi sukses
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'reservasi berhasil dibatalkan'
                );

                // if permitted, do logit
                $perms = "rekam_medis_reservasi_view";
                $comments = "Berhasil mengubah reservasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah list_pasien , hubungi web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_resevasi_view";
                $comments = "Gagal mengubah reservasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
            echo json_encode($res);
        }

        function do_tolak_reservasi_igd(){
			      $id = $this->input->post('reservasi_igd_id',TRUE);
			     // $datares['id'] = $this->input->post('reservasi_igd_id',TRUE);
            $is_permit = $this->aauth->control_no_redirect('rekam_medis_reservasi_update');
            if(!$is_permit){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => $this->lang->line('aauth_error_no_access'));
                echo json_encode($res);
                exit;
            }
            $datares['status'] = "TOLAK";
            // $datares['tgl_verif'] = date('Y-m-d H:i:s');
      			// $datares['alasan'] = $this->input->post('reservasi_igd_alasan',TRUE);
      			$datares['catatan'] = $this->input->post('reservasi_igd_catatan',TRUE);
            // insert insert_alasan_m_lookup dulu
            $alasan = $this->input->post('reservasi_igd_alasan',TRUE);
            switch ($alasan) {
                case "kamar":
                    $description = "kamar penuh";
                    $code_alasan = "2";
                    break;
                case "administrasi":
                    $description = "administrasi tidak lengkap";
                    $code_alasan = "1";
                    break;
                default:
                    $description = "lainnya";
                    $code_alasan = "3";
            }

            $datares['alasan_id'] = $code_alasan;
            // $data_alasan['type'] = "alasan.tolak.reservasi"; // type > constant data
            // $data_alasan['code'] = $code_alasan;
            // $data_alasan['name'] = $alasan; // alasan name
            // $data_alasan['description'] = $description;
			      // $insert_alasan = $this->Reservasi_model_igd->insert_alasan_m_lookup($data_alasan);
            $update = $this->Reservasi_model_igd->update_reservasi_igd($datares, $id);

            // Jika tolak reservasi sukses
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'reservasi berhasil ditolak'
                );

                // if permitted, do logit
                $perms = "rekam_medis_reservasi_view";
                $comments = "Berhasil mengubah reservasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menolak pasien, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_resevasi_view";
                $comments = "Gagal mengubah reservasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
            echo json_encode($res);
        }

        // menampilkan pasien list pasien reservasi
        function get_pasien_reservasi_igd(){
            $tgl_awal       = $this->input->get('tgl_res_awal');
            // $poli           = $this->input->get('poli');
            // $dokter         = $this->input->get('dokter');
            $nama_pasien    = $this->input->get('nama_pasien');
            $no_rekam_medis = $this->input->get('no_rekam_medis');
            $kode_booking   = $this->input->get('kode_booking');
            $data = $this->Reservasi_model_igd->get_pasien_reservasi_igd($tgl_awal,$nama_pasien,$no_rekam_medis,$kode_booking);
            // echo json_encode($data);die;
            $output = '';
            foreach ($data as $val) {
                // menentukan ava pasien
                if($val->jenis_kelamin == "Laki-laki"){
                    $img_url = "boy.png";
                }elseif ($val->jenis_kelamin == "Perempuan") {
                    $img_url = "girl.png";
                }else{
                    $img_url = "boy.png";
                }
                // menentukan style hadir dan batal
                if ($val->status == "HADIR") {
                    $text = "HADIR";
                    $class = "";
                    $color = "btn-success";
                    $is_batal = "";
                }elseif ($val->status == "BATAL") {
                    $text = "BATAL";
                    $class = "";
                    $color = "btn-danger";
                    $is_batal = "hidden";
                }else{
                    $color = "";
                    $text = "-";
                    $is_batal = "";
                    $class = "hidden";
                }
                if ($val->status_verif) {
                    $is_batal = "hidden";
                }

                    $output .= '
                    <div class="comment-body" style="padding: 8px; width: 100% ; ">
                        <div class="user-img"> <img style="margin-top: 24px;" class="img-circle" alt="user" src="'.base_url().'assets/plugins/images/'.$img_url.'" ></div>
                        <div  class="mail-contnet" >
                            <div class="col-md-1 pull-right" >
                                <span class="btn "  style="font-size: 10px; background:"white"; color: #FFFFFF; border-radius: 20%; padding: 3px; font-weight: bold;" >'.$val->no_urut.'
                                </span>
                            </div>';
                            // <span class="mytooltip tooltip-effect-5">
                            //     <span class="btn tooltip-item" style="background:"white"; margin-bottom:8px;  font-size: 9px; color: #FFFFFF;  padding: 3px; font-weight: bold;" >'. $val->nama_poliruangan .'</span>
                            //     <span class="tooltip-content clearfix ">
                            //         <span class="tooltip-text">'. $val->NAME_DOKTER .'</span>
                            //     </span>
                            // </span>
                    $output .='
                            <h5 style="font-size: 11px; margin-top:8px; ">'.$val->no_rekam_medis.'</h5>
                            <h5 id="list_pasien_nama">'.$val->nama_pasien.'</h5>
                            <span style="font-style:italic;font-size: 12px;"> '.$val->nama_perujuk.' </span><br>
                            <span style="font-style:italic;font-size: 12px;"> '.$val->telp_hp.' </span>
                            <br>
                            <button type="button" class="btn btn-outline btn-rounded btn-danger" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="batalReservasi('.$val->reservasi_igd_id.')" '.$is_batal.' >BATAL</button>
                            <button type="button" class="btn btn-outline btn-rounded btn-success" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="print('.$val->reservasi_igd_id.')" >PRINT</button>
                            <button type="button" class="btn btn-outline btn-rounded btn-primary" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="verify('.$val->reservasi_igd_id.')" '.$is_batal.' >VERIFY</button>
							<button type="button" class="btn btn-outline btn-rounded btn-danger" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="dialogTolak('.$val->reservasi_igd_id.')" '.$is_batal.' >TOLAK</button>
                            <!--
							<button type="button" class="btn btn-outline btn-rounded btn-danger"
                            title="Tolak Reservasi" data-toggle="modal" data-target="#modal_tolak_reservasi_igd" data-backdrop="static" data-keyboard="false"
                            style="font-size: 10px; width:inherit; font-weight: bold;" onclick="dialogTolak('.$val->reservasi_igd_id.')" '.$is_batal.' >TOLAK</button>
                            <span class="btn '.$color.' pull-right"  style="margin-top: 8px; font-size: 6px; color: #FFFFFF; border-radius: 20%; padding: 4px; font-weight: bold;"'.$class.'>'.$text.'</span>
							-->
                        </div>
                    </div>
                    ';
            }
            // $jos = base_url();
            echo json_encode($output);
        }

        // menampilkan pasien list pasien batal reservasi
        function get_pasien_reservasi_igd_tolak(){
            $tgl_awal       = $this->input->get('tgl_res_awal');
            // $poli           = $this->input->get('poli');
            // $dokter         = $this->input->get('dokter');
            $nama_pasien    = $this->input->get('nama_pasien');
            $no_rekam_medis = $this->input->get('no_rekam_medis');
            $kode_booking   = $this->input->get('kode_booking');

            $data = $this->Reservasi_model_igd->get_pasien_reservasi_igd_tolak($tgl_awal,$nama_pasien,$no_rekam_medis,$kode_booking);
            $output = '';
            foreach ($data as $val) {
                if($val->jenis_kelamin == "Laki-laki"){
                    $img_url = "boy.png";
                }elseif ($val->jenis_kelamin == "Perempuan") {
                    $img_url = "girl.png";
                }else{
                    $img_url = "boy.png";
                }

                if ($val->status == "HADIR") {
                    $text = "HADIR";
                    $class = "";
                    $color = "btn-success";
                    $is_batal = "";


                }elseif ($val->status == "BATAL") {
                    $text = "BATAL";
                    $class = "";
                    $color = "btn-danger";
                    $is_batal = "hidden";

                }elseif ($val->status == "TOLAK") {
                    $text = "TOLAK";
                    $class = "";
                    $color = "btn-danger";
                    $is_batal = "hidden";

                }else{
                    $color = "";
                    $text = "-";
                    $is_batal = "";
                    $class = "hidden";
                }

                    $output .= '
                    <div class="comment-body" style="padding: 8px; width: 100%">
                        <div class="user-img"> <img style="margin-top: 16px;" class="img-circle" alt="user" src="'.base_url().'assets/plugins/images/'.$img_url.'" ></div>

                        <div class="mail-contnet">

                            <h5 style="font-size: 10px">'.$val->no_rekam_medis.'</h5><h5>'.$val->nama_pasien.'</h5>
                            <span style="font-size: 14px;"> alasan : '.$val->description.' </span><br>
                            <span style="font-size: 14px;"> catatan : '.$val->catatan.' </span><br>
                            <button type="button" class="btn btn-outline btn-rounded btn-danger" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="batalReservasi('.$val->reservasi_igd_id.')" '.$is_batal.' >BATAL</button>
                            <button type="button" class="btn btn-outline btn-rounded btn-primary" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="verify('.$val->reservasi_igd_id.')" '.$is_batal.' >VERIFY</button>

                            <span class="btn '.$color.' pull-right"  style="margin-top: 8px; font-size: 6px; color: #FFFFFF; border-radius: 20%; padding: 4px; font-weight: bold;"'.$class.'>'.$text.'</span>

                        </div>
                    </div>
                ';
            }
            echo json_encode($output);
        }

        // mengirim data ke form pendaftaran
        function verifikasi_data($id){
            $this->data['js1'] = '<script src="'.base_url().'assets/dist/js/pages/rekam_medis/pendaftaran.js"></script>';
            $this->data['js2'] = '<script src="'.base_url().'assets/dist/js/pages/rekam_medis/reservasi.js"></script>';
            $this->data['reservasi_igd_id'] = $id;
            $this->data['pasien'] = $this->Reservasi_model->get_data_reservasi_by_id($id);

            if($this->data['pasien']->status_pasien == "PASIEN LAMA" ){
                $this->data['get_method'] = "PASIEN LAMA";
            }else{
                $this->data['get_method'] = "PASIEN BARU";
            }
            $this->load->view('rekam_medis/v_form_pendaftaran', $this->data);
        }

        //test data
        // function jos(){
        //     $data = $this->Reservasi_model_igd->get_pasien_reservasi_igd_tolak("2018-12-28","","","");
        //     echo '<pre>';
        //     echo json_encode($data);
        // }

        // menampilkan data reservasi di pendaftaran
        function get_data_reservasi($id){
            // $pasien = $this->Reservasi_model->get_data_reservasi_by_norm($id);
            $pasien = $this->Reservasi_model->get_data_reservasi_by_id($id);
            $norm = $pasien->no_rekam_medis;

            date_default_timezone_set('Asia/Jakarta');
            setlocale(LC_ALL, 'IND');
            setlocale(LC_ALL, 'id_ID');
            $data_res = $this->Reservasi_model->get_data_reservasi_by_id($id);
            $tgl_lahir_pas = $data_res->tanggal_lahir;
            $tanggal_lahir_pas = ($tgl_lahir_pas == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lahir_pas));

            if($pasien->status_pasien == "PASIEN LAMA"){
                $m_pasien = $this->Reservasi_model->get_data_m_pasien($norm);
                $data_reservasi = $this->Reservasi_model->get_data_reservasi_by_id($id);
                $tgl_lhr_ortu =  $m_pasien->tgl_lahir_ortu;
                $tgl_lahir_ortu = ($tgl_lhr_ortu == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_ortu));
                $tgl_lhr_suami = $m_pasien->tgl_lahir_suami_istri;
                $tgl_lahir_suami = ($tgl_lhr_suami == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($tgl_lhr_suami));
                $tgl_rsvs = date_create($data_reservasi->tanggal_reservasi);
                $tgl_reservasi = date_format($tgl_rsvs, 'd F Y');

                $data = array(
                    'data'=> $m_pasien,
                    'reservasi' => $data_reservasi,
                    'tgl_reservasi' => $tgl_reservasi,
                    'tgl_lahir' =>$tanggal_lahir_pas,
                    'tgl_lahir_ortu' =>$tgl_lahir_ortu,
                    'tgl_lahir_suami' =>$tgl_lahir_suami
                );
            }else{
                $data_pasien = $this->Reservasi_model->get_data_reservasi_by_id($id);
                $data_reservasi = $this->Reservasi_model->get_data_reservasi_by_id($id);
                $tgl_rsvs = date_create($data_pasien->tanggal_reservasi);
                $tgl_reservasi = date_format($tgl_rsvs, 'd F Y');

                $data = array(
                    'data' => $data_pasien,
                    'reservasi' => $data_reservasi,
                    'tgl_lahir' => $tanggal_lahir_pas,
                    'tgl_reservasi' => $tgl_reservasi
                );
            }
            echo json_encode($data);
        }

        public function print_rm($id){
            $print['reservasi_igd_id'] = $id;
            $print['data_pasien'] = $this->Reservasi_model_igd->get_data_print($id);
            $this->load->view('rekam_medis/print_reservasi_igd',$print);
        }

        // menampilkan pasien list pasien batal reservasi
        function get_pasien_reservasi_igd_batal(){
            $tgl_awal       = $this->input->get('tgl_res_awal');
            // $poli           = $this->input->get('poli');
            // $dokter         = $this->input->get('dokter');
            $nama_pasien    = $this->input->get('nama_pasien');
            $no_rekam_medis = $this->input->get('no_rekam_medis');
            $kode_booking   = $this->input->get('kode_booking');

            $data = $this->Reservasi_model_igd->get_pasien_reservasi_igd_batal($tgl_awal,$nama_pasien,$no_rekam_medis,$kode_booking);
            $output = '';
            foreach ($data as $val) {
                if($val->jenis_kelamin == "Laki-laki"){
                    $img_url = "boy.png";
                }elseif ($val->jenis_kelamin == "Perempuan") {
                    $img_url = "girl.png";
                }else{
                    $img_url = "boy.png";
                }

                if ($val->status == "HADIR") {
                    $text = "HADIR";
                    $class = "";
                    $color = "btn-success";
                    $is_batal = "";


                }elseif ($val->status == "BATAL") {
                    $text = "BATAL";
                    $class = "";
                    $color = "btn-danger";
                    $is_batal = "hidden";

                }else{
                    $color = "";
                    $text = "-";
                    $is_batal = "";
                    $class = "hidden";
                }

                    $output .= '
                    <div class="comment-body" style="padding: 8px; width: 100%">
                        <div class="user-img"> <img style="margin-top: 16px;" class="img-circle" alt="user" src="'.base_url().'assets/plugins/images/'.$img_url.'" ></div>

                        <div class="mail-contnet">

                            <h5 style="font-size: 10px">'.$val->no_rekam_medis.'</h5><h5>'.$val->nama_pasien.'</h5>
                            <span style="font-size: 12px;"> '.$val->alamat_pasien.' </span>
                            <br>
                            <button type="button" class="btn btn-outline btn-rounded btn-danger" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="batalReservasi('.$val->reservasi_igd_id.')" '.$is_batal.' >BATAL</button>
                            <button type="button" class="btn btn-outline btn-rounded btn-primary" style="font-size: 10px; width:inherit; font-weight: bold;" onclick="verify('.$val->reservasi_igd_id.')" '.$is_batal.' >VERIFY</button>

                            <span class="btn '.$color.' pull-right"  style="margin-top: 8px; font-size: 6px; color: #FFFFFF; border-radius: 20%; padding: 4px; font-weight: bold;"'.$class.'>'.$text.'</span>

                        </div>
                    </div>
                ';
            }
            echo json_encode($output);
        }

        function update_telepon_perujuk(){
          $is_permit = $this->aauth->control_no_redirect('rekam_medis_reservasi_update');
          if(!$is_permit) {
              $res = array(
                  'csrfTokenName' => $this->security->get_csrf_token_name(),
                  'csrfHash' => $this->security->get_csrf_hash(),
                  'success' => false,
                  'messages' => $this->lang->line('aauth_error_no_access'));
              echo json_encode($res);
              exit;
          }

          $id = $this->input->post('id_perujuk');
          $datares['telp_hp'] = $this->input->post('rm_noteleponperujuk_edit');

          $update = $this->Reservasi_model_igd->update_telepon_perujuk($datares, $id);
          // Jika tolak reservasi sukses
          if($update){
              $res = array(
                  'csrfTokenName' => $this->security->get_csrf_token_name(),
                  'csrfHash' => $this->security->get_csrf_hash(),
                  'success' => true,
                  'messages' => 'nomor telepon perujuk berhasil di update'
              );

              // if permitted, do logit
              $perms = "rekam_medis_reservasi_view";
              $comments = "Berhasil mengubah reservasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
              $this->aauth->logit($perms, current_url(), $comments);
          }else{
              $res = array(
              'csrfTokenName' => $this->security->get_csrf_token_name(),
              'csrfHash' => $this->security->get_csrf_hash(),
              'success' => false,
              'messages' => 'Gagal update nomor telepon, hubungi web administrator.');

              // if permitted, do logit
              $perms = "rekam_medis_resevasi_view";
              $comments = "Gagal mengubah nomor telepon dengan data berikut = '". json_encode($_REQUEST) ."'.";
              $this->aauth->logit($perms, current_url(), $comments);
          }
          echo json_encode($res);
        }
    }

?>
