<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_diagnosa_model extends CI_Model {
    var $column = array('diagnosa2_id','diagnosa_kode','diagnosa_nama');
    var $order = array('diagnosa_nama' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_diagnosa_list(){
        $this->db->select('*, m_diagnosa_icd10.kode_diagnosa, m_diagnosa_icd10.nama_diagnosa');
        $this->db->join('m_diagnosa_icd10', 'm_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
        $this->db->from('m_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_diagnosa_all(){
        $this->db->select('*, m_diagnosa_icd10.kode_diagnosa');
        $this->db->join('m_diagnosa_icd10', 'm_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
        $this->db->from('m_diagnosa');

        return $this->db->count_all_results();
    }
    
    public function count_diagnosa_filtered(){
        $this->db->select('*, m_diagnosa_icd10.kode_diagnosa');
        $this->db->join('m_diagnosa_icd10', 'm_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
        $this->db->from('m_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_diagnosa($data=array()){
        $insert = $this->db->insert('m_diagnosa',$data);
        
        return $insert;
    }

    public function list_diagnosa_icd(){
        return $this->db->get("m_diagnosa_icd10")->result();
    }
    
    public function get_diagnosa_by_id($id){
        $query = $this->db->get_where('m_diagnosa', array('diagnosa2_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_diagnosa($data, $id){
        $update = $this->db->update('m_diagnosa', $data, array('diagnosa2_id' => $id));
        
        return $update;
    }
    
    public function delete_diagnosa($id){
        $delete = $this->db->delete('m_diagnosa', array('diagnosa2_id' => $id));
        
        return $delete;
    }

    // public function load_data($data_array){
    //     for ($i= 0; $i < count($data_array); $i++) {
    //         $data = array(
    //             'agama_id' => $data_array[$i]['agama_id'],
    //             'agama_nama' => $data_array[$i]['agama_nama']
    //         );

    //         $cek = $this->db->where('agama_nama', $this->input->post('agama_nama'));
    //         if ($cek) {
    //             $this->db->insert('m_agama', $data);
    //         }
    //     }
    // }
    
 
}