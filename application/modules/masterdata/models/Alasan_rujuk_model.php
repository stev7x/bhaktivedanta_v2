<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Alasan_rujuk_model extends CI_Model {
    var $column = array('alasan_rujuk_id','nama','code','keterangan');
    var $order = array('alasan_rujuk_id' => 'ASC');

    public function __construct() {
        parent::__construct();
    }

    public function get_tindakan_list(){
        $this->db->select("*");
        $this->db->from('m_alasan_rujuk');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }

    public function count_tindakan_all(){
       $this->db->select("*");
       $this->db->from('m_alasan_rujuk');

        return $this->db->count_all_results();
    }

    public function count_tindakan_filtered(){
       $this->db->select("*");
       $this->db->from('m_alasan_rujuk');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function insert_alasan_rujuk($data=array()){
        $insert = $this->db->insert('m_alasan_rujuk',$data);
        
        return $insert;
    }

    public function get_alasan_rujuk_by_id($id){
        $query = $this->db->get_where('m_alasan_rujuk', array('alasan_rujuk_id' => $id), 1, 0);
        
        return $query->row();
    }

    public function update_alasan_rujuk($data, $id){
        $update = $this->db->update('m_alasan_rujuk', $data, array('alasan_rujuk_id' => $id));
        
        return $update;
    }

    public function delete_alasan_rujuk($id){
        $delete = $this->db->delete('m_alasan_rujuk', array('alasan_rujuk_id' => $id));
        
        return $delete;
    }
    
}
?>