<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_diagnosa_model extends CI_Model {
    var $column = array('diagnosa_id','kode_diagnosa','nama_diagnosa');
    var $order = array('kode_diagnosa' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_daftar_diagnosa_list_2(){
        $this->db->select('m_diagnosa_icd10.*, m_kelompok_diagnosa.kelompokdiagnosa_nama');
        $this->db->from('m_diagnosa_icd10');
        $this->db->join('m_kelompok_diagnosa','m_kelompok_diagnosa.kelompokdiagnosa_id = m_diagnosa_icd10.kelompokdiagnosa_id');
        $this->db->order_by('kode_diagnosa','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_daftar_diagnosa_list(){
        $this->db->select('m_diagnosa_icd10.*');
        $this->db->from('m_diagnosa_icd10');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_daftar_diagnosa_all(){
        $this->db->select('m_diagnosa_icd10.*');
        $this->db->from('m_diagnosa_icd10');

        return $this->db->count_all_results();
    }
    
    public function count_daftar_diagnosa_filtered(){
        $this->db->select('m_diagnosa_icd10.*');
        $this->db->from('m_diagnosa_icd10');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_daftar_diagnosa($data=array()){
         $insert = $this->db->insert('m_diagnosa_icd10',$data);
        
        return $insert;
    } 
    
    public function get_daftar_diagnosa_by_id($id){
        $query = $this->db->get_where('m_diagnosa_icd10', array('diagnosa_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_daftar_diagnosa($data, $id){
        $update = $this->db->update('m_diagnosa_icd10', $data, array('diagnosa_id' => $id));
        
        return $update;
    }
    
    public function delete_daftar_diagnosa($id){
        $delete = $this->db->delete('m_diagnosa_icd10', array('diagnosa_id' => $id));
        
        return $delete;
    }   
    
    // public function get_kelompokdiagnosa_list(){
    //      return $this->db->get('m_kelompok_diagnosa')->result();
    // }
 
}