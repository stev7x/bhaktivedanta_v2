<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel_model extends CI_Model {
    var $column = array('id_hotel', 'nama_hotel','alamat','no_telp','email','PIC','expired');
    var $order = array('id_hotel' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_hotel_list(){
        $this->db->select('*');
        $this->db->from('m_hotel');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_hotel_all(){
        $this->db->select('*');
        $this->db->from('m_hotel');

        return $this->db->count_all_results();
    }
    
    public function count_hotel_filtered(){
        $this->db->select('*');
        $this->db->from('m_hotel');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_hotel($data=array()){
        $insert = $this->db->insert('m_hotel',$data);
        
        return $insert;
    }
    
    public function get_hotel_by_id($id){
        $query = $this->db->get_where('m_hotel', array('id_hotel' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_hotel($data, $id){
        $update = $this->db->update('m_hotel', $data, array('id_hotel' => $id));
        
        return $update;
    }
    
    public function delete_hotel($id){
        $delete = $this->db->delete('m_hotel', array('id_hotel' => $id));
        
        return $delete;
    }
    
    public function get_obat_list(){
        return $this->db->get('m_hotel')->result();
    }
 
}