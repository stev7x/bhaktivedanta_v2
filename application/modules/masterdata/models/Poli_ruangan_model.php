<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poli_ruangan_model extends CI_Model {
    var $column = array('poliruangan_id','nama_poliruangan','instalasi_nama','nama_ruangan');
    var $order = array('nama_poliruangan' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_poli_ruangan_list_2(){
        $this->db->select("m_poli_ruangan.*,m_instalasi.instalasi_nama,if(type_ruangan=1,'Poliklinik',if(type_ruangan=2,'Ruang Rawat Inap',if(type_ruangan=3,'Ruang IGD','dll'))) as nama_ruangan");
        $this->db->join('m_instalasi','m_instalasi.instalasi_id = m_poli_ruangan.instalasi_id');
        $this->db->from('m_poli_ruangan');
        $this->db->order_by('instalasi_nama','ASC');

        $query = $this->db->get();
        return $query->result();
    }   

    public function get_poli_ruangan_list(){
        $this->db->select("m_poli_ruangan.*,m_instalasi.instalasi_nama,if(type_ruangan=1,'Poliklinik',if(type_ruangan=2,'Ruang Rawat Inap',if(type_ruangan=3,'Ruang IGD','dll'))) as nama_ruangan");
        $this->db->join('m_instalasi','m_instalasi.instalasi_id = m_poli_ruangan.instalasi_id');
        $this->db->from('m_poli_ruangan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                if ($item=='nama_ruangan') {
                    $item ='type_ruangan';
                }
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_poli_ruangan_all(){
        $this->db->from('m_poli_ruangan');

        return $this->db->count_all_results();
    }
    
    public function count_poli_ruangan_filtered(){
        $this->db->select("m_poli_ruangan.*,m_instalasi.instalasi_nama,if(type_ruangan=1,'Poliklinik',if(type_ruangan=2,'Ruang Rawat Inap',if(type_ruangan=3,'Ruang IGD','dll'))) as nama_ruangan");
        $this->db->join('m_instalasi','m_instalasi.instalasi_id = m_poli_ruangan.instalasi_id');
        $this->db->from('m_poli_ruangan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                if ($item=='nama_ruangan') {
                    $item ='type_ruangan';
                }
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_poli_ruangan($data=array()){
         $insert = $this->db->insert('m_poli_ruangan',$data);
        
        return $insert;
    }
    
    public function get_poli_ruangan_by_id($id){
        $query = $this->db->get_where('m_poli_ruangan', array('poliruangan_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_poli_ruangan($data, $id){
        $update = $this->db->update('m_poli_ruangan', $data, array('poliruangan_id' => $id));
        
        return $update;
    }
    
    public function delete_poli_ruangan($id){
        $delete = $this->db->delete('m_poli_ruangan', array('poliruangan_id' => $id));
        
        return $delete;
    }
    public function get_instalasi_list(){
         return $this->db->get('m_instalasi')->result();
    }
    
 
}