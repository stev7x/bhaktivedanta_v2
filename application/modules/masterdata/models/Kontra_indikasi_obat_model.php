<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontra_indikasi_obat_model extends CI_Model {
    var $column = array('id_kontra_indikasi','kode_kontra_indikasi', 'nama_kontra_indikasi','keterangan');
    var $order = array('id_kontra_indikasi' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_kontra_indikasi_obat_list(){
        $this->db->from('m_kontraindikasi_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_kontra_indikasi_obat_all(){
         $this->db->from('m_kontraindikasi_obat');

        return $this->db->count_all_results();
    }
    
    public function count_kontra_indikasi_obat_filtered(){
         $this->db->from('m_kontraindikasi_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_kontra_indikasi_obat($data=array()){
        $insert = $this->db->insert('m_kontraindikasi_obat',$data);
        
        return $insert;
    }
    
    public function get_kontra_indikasi_obat_by_id($id){
        $query = $this->db->get_where('m_kontraindikasi_obat', array('id_kontra_indikasi' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_kontra_indikasi_obat($data, $id){
        $update = $this->db->update('m_kontraindikasi_obat', $data, array('id_kontra_indikasi' => $id));
        
        return $update;
    }
    
    public function delete_kontra_indikasi_obat($id){
        $delete = $this->db->delete('m_kontraindikasi_obat', array('id_kontra_indikasi' => $id));
        
        return $delete;
    }
    
    // public function get_kelas_terapi_list(){
    //     return $this->db->get('m_kelasterapi_obat')->result();
    // }
 
}