<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model {
    var $column = array('karyawan_id','no_karyawan','no_rekam_medis','nama_karyawan','tempat_lahir','tanggal_lahir','jenis_kelamin','alamat','email','jabatan');
    var $order = array('no_karyawan' => 'ASC');
	var $column_ref = array('no_rekam_medis','pasien_nama','jenis_kelamin','tanggal_lahir','tempat_lahir','pasien_alamat');
    var $order_ref = array('no_rekam_medis' => 'ASC');

    public function __construct() {
        parent::__construct();
    }


    public function get_no_karyawan(){
    	$default = "00000001";
        $sql = "SELECT CAST(MAX(no_karyawan) AS UNSIGNED) nomaksimal
                        FROM m_karyawan";
        $no_current =  $this->db->query($sql)->result();
        
        
        $next_no_karyawan = (isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_no_karyawan;
    }

    function get_no_rm(){
        $default = "000001";
        $prefix = "RM";
        
        $sql = "SELECT CAST(MAX(SUBSTR(no_rekam_medis,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_detail_rm
                        WHERE no_rekam_medis LIKE ('RM%')";
        $no_current =  $this->db->query($sql)->result();
        
        
        $next_rm = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_rm;
    }

    public function get_karyawan_list(){
        $this->db->from('m_karyawan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }


    public function get_ref_list(){
        $this->db->from('v_karyawan');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column_ref as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column_ref[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order_ref)){
            $order_ref = $this->order_ref;
            $this->db->order_by(key($order_ref), $order_ref[key($order_ref)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_karyawan_all(){
        $this->db->from('m_karyawan');

        return $this->db->count_all_results();
    }

    public function count_ref_karyawan_all(){
        $this->db->from('v_karyawan');

        return $this->db->count_all_results();
    }
    
    public function count_karyawan_filtered(){
        $this->db->from('m_karyawan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_ref_karyawan_filtered(){
        $this->db->from('v_karyawan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column_ref as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column_ref[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order_ref)){
            $order_ref = $this->order_ref;
            $this->db->order_by(key($order_ref), $order_ref[key($order_ref)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    

    public function insert_karyawan($data){
    	$sql = $this->db->insert('m_karyawan',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }

    public function insert_pend_umum($data){
    	$this->db->insert('m_pendidikan_u_karyawan',$data);
    }

    public function insert_pend_profesi($data){
    	$this->db->insert('m_pendidikan_p_karyawan',$data);
    }

    public function insert_ijin_p($data){
    	$this->db->insert('m_izin_profesi_kary',$data);
    }

    public function insert_rm($data){
        $this->db->insert('t_detail_rm', $data);
    }

    public function get_karyawan_by_id($id){
        $query = $this->db->get_where('m_karyawan', array('karyawan_id' => $id), 1, 0);
  
        return $query->row();
    }

    public function get_pendidikan_p($id){ 
        $query = $this->db->get_where('m_pendidikan_p_karyawan', array('karyawan_id' => $id), 1, 0);
  
        return $query->row();   
    }

    public function get_pendidikan_u($id){ 
        $query = $this->db->get_where('m_pendidikan_u_karyawan', array('karyawan_id' => $id), 1, 0);      
  
        return $query->row();
    }   
    public function get_izin_p($id){ 
        $query = $this->db->get_where('m_izin_profesi_kary', array('karyawan_id' => $id), 1, 0);      
   
        return $query->row();
    }

    public function delete_karyawan($id){
        $delete = $this->db->delete('m_karyawan', array('karyawan_id' => $id));
        
        return $delete;
    }


    public function get_list_karyawan_by_id($id){
        $this->db->from('m_karyawan');
        $this->db->join('m_pendidikan_u_karyawan','m_pendidikan_u_karyawan.karyawan_id = m_karyawan.karyawan_id');
        $this->db->join('m_pendidikan_p_karyawan','m_pendidikan_p_karyawan.karyawan_id = m_karyawan.karyawan_id');
        $this->db->join('m_izin_profesi_kary','m_izin_profesi_kary.karyawan_id = m_karyawan.karyawan_id');
        $this->db->where('m_karyawan.karyawan_id', $id);
        $query = $this->db->get();
        // $query = $this->db->get_where('m_karyawan', array('karyawan_id' => $id), 1, 0);
        
        return $query->row();
    }

    public function get_list_ref_by_id($id){
        $this->db->from('m_pasien');
        $this->db->where('m_pasien.pasien_id', $id);
        $query = $this->db->get();
        
        return $query->row();
    }

    public function update_karyawan($data, $id){
        $update = $this->db->update('m_karyawan', $data, array('karyawan_id' => $id));
        
        return $update;
    }

    public function update_pend_umum($data, $id){
        $update = $this->db->update('m_pendidikan_u_karyawan', $data, array('karyawan_id' => $id));
        
        return $update;
    }

    public function update_pend_profesi($data, $id){
        $update = $this->db->update('m_pendidikan_p_karyawan', $data, array('karyawan_id' => $id));
        
        return $update;
    }
    
    public function update_ijin_p($data, $id){
        $update = $this->db->update('m_izin_profesi_kary', $data, array('karyawan_id' => $id));
        
        return $update;
    }

    function get_sekola(){
        $this->db->from('m_sekolah');
        $query = $this->db->get();
        
        return $query->result();
    }

}