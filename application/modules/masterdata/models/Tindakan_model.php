<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tindakan_model extends CI_Model {
    var $column = array('daftartindakan_id', 'daftartindakan_nama','harga_tindakan','jenis_pasien','type_call', 'escort');
    var $order = array('daftartindakan_id' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_tindakan_list(){
        $this->db->select('*');
        $this->db->from('m_tindakan');
        
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value['value'] != ''){
            // die($search_value);
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $this->db->where('type_tindakan', 'TINDAKAN');
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        // print_r($query->result());die;
        return $query->result();
    }
    
    public function count_tindakan_all(){
        $this->db->select('*');
        $this->db->from('m_tindakan');
        $this->db->where('type_tindakan', 'TINDAKAN');
        return $this->db->count_all_results();
    }
    
    public function count_tindakan_filtered(){
        $this->db->select('*');
        $this->db->from('m_tindakan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value['value'] != ''){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $this->db->where('type_tindakan', 'TINDAKAN');
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_tindakan($data=array()){
        $insert = $this->db->insert('m_tindakan',$data);
        
        return $insert;
    }
    
    public function get_tindakan_by_id($id){
        $query = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_tindakan($data, $id){
        $update = $this->db->update('m_tindakan', $data, array('daftartindakan_id' => $id));
        
        return $update;
    }
    
    public function delete_tindakan($id){
        // var_dump($id);die;
        $delete = $this->db->delete('m_tindakan', array('daftartindakan_id' => $id));
        // print_r($this->db->error());die;
        return $delete;
    }
    
    public function get_obat_list(){
        return $this->db->get('m_hotel')->result();
    }
 
}