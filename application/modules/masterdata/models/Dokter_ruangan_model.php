<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter_ruangan_model extends CI_Model {
    var $column = array('id_M_DOKTER','NAME_DOKTER','ALAMAT','m_poli_ruangan.poliruangan_id','nama_poliruangan');
    var $order = array('nama_dokterruangan' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }


    public function get_dokter_ruangan_list_2(){
        $this->db->select("m_poli_ruangan.*,m_dokter.*");
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
        $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id = m_dokterruangan.poliruangan_id');
        $this->db->from('m_dokterruangan');
        $this->db->order_by('NAME_DOKTER','ASC');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_dokter_ruangan_list(){
        $this->db->select("m_poli_ruangan.*,m_dokter.*");
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
        $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id = m_dokterruangan.poliruangan_id');
        $this->db->from('m_dokterruangan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_dokter_ruangan_all(){
        $this->db->from('m_dokterruangan');

        return $this->db->count_all_results();
    }
    
    public function count_dokter_ruangan_filtered(){
        $this->db->select("m_poli_ruangan.*,m_dokter.*");
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = m_dokterruangan.dokter_id');
        $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id = m_dokterruangan.poliruangan_id');
        $this->db->from('m_dokterruangan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_dokter_ruangan($data=array()){
         $insert = $this->db->insert('m_dokterruangan',$data);
        
        return $insert;
    }
    
    public function get_dokter_ruangan_by_id($id, $ruanganid){
        $query = $this->db->get_where('m_dokterruangan', array('dokter_id' => $id, 'poliruangan_id' => $ruanganid));
        
        return $query->row();
    }
    
    public function update_dokter_ruangan($data, $id, $ruanganid){
        $update = $this->db->update('m_dokterruangan', $data, array('dokter_id' => $id, 'poliruangan_id' => $ruanganid));
        return $update;
    }
    
    public function delete_dokter_ruangan($id,$ruangid){
        $delete = $this->db->delete('m_dokterruangan', array('dokter_id' => $id,'poliruangan_id' => $ruangid));
        
        return $delete;
    }
    public function get_dokter_list(){
         return $this->db->get('m_dokter')->result();
    }
    public function get_ruangan_list(){
         return $this->db->get('m_poli_ruangan')->result();
    }
 
}