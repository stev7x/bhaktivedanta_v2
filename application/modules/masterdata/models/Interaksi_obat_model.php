<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interaksi_obat_model extends CI_Model {
    var $column = array('id_interaksi','kode_interaksi', 'nama_interaksi','keterangan');
    var $order = array('id_interaksi' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_interaksi_obat_list(){
        $this->db->from('m_interaksi_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_interaksi_obat_all(){
         $this->db->from('m_interaksi_obat');

        return $this->db->count_all_results();
    }
    
    public function count_interaksi_obat_filtered(){
         $this->db->from('m_interaksi_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_interaksi_obat($data=array()){
        $insert = $this->db->insert('m_interaksi_obat',$data);
        
        return $insert;
    }
    
    public function get_interaksi_obat_by_id($id){
        $query = $this->db->get_where('m_interaksi_obat', array('id_interaksi' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_interaksi_obat($data, $id){
        $update = $this->db->update('m_interaksi_obat', $data, array('id_interaksi' => $id));
        
        return $update;
    }
    
    public function delete_interaksi_obat($id){
        $delete = $this->db->delete('m_interaksi_obat', array('id_interaksi' => $id));
        
        return $delete;
    }
    
    // public function get_kelas_terapi_list(){
    //     return $this->db->get('m_kelasterapi_obat')->result();
    // }
 
}