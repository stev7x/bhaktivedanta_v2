<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar_model extends CI_Model {
    var $column = array('kamarruangan_id','kelaspelayanan_nama','nama_poliruangan','no_kamar','no_bed','status_bed','status_aktif','keterangan');
    var $order = array('kelaspelayanan_nama' => 'ASC');

    public function __construct() {
        parent::__construct();
    }

    public function get_kamar_list_2(){
        $this->db->from('m_kamarruangan');
        $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id=m_kamarruangan.poliruangan_id');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id=m_kamarruangan.kelaspelayanan_id');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_kamar_list(){
        $this->db->from('m_kamarruangan');
        $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id=m_kamarruangan.poliruangan_id');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id=m_kamarruangan.kelaspelayanan_id');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_kamar_all(){
        $this->db->from('m_kamarruangan');

        return $this->db->count_all_results();
    }
    function get_ruangan(){
        $this->db->where('instalasi_id','3');
        $this->db->from('m_poli_ruangan');
        $query = $this->db->get();

        return $query->result();
    }
    function get_kelas_pelayanan($ruangan_id){
        $this->db->select('m_kelaspoliruangan.kelaspelayanan_id, m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_kelaspoliruangan');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_kelaspoliruangan.kelaspelayanan_id');
        $this->db->where('m_kelaspoliruangan.poliruangan_id', $ruangan_id);
        $query = $this->db->get();
        
        return $query->result();
    }
    public function count_kamar_filtered(){
      $this->db->from('m_kamarruangan');
      $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id=m_kamarruangan.poliruangan_id');
      $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id=m_kamarruangan.kelaspelayanan_id');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function insert_kamar($data=array()){
         $insert = $this->db->insert('m_kamarruangan',$data);

        return $insert;
    }
    public function get_kamar_by_id($id){
        $query = $this->db->get_where('m_kamarruangan', array('kamarruangan_id' => $id), 1, 0);

        return $query->row();
    }
    public function delete_kamar($id){
        $delete = $this->db->delete('m_kamarruangan', array('kamarruangan_id' => $id));

        return $delete;
    }

    public function update_kamar($data, $id){
        $update = $this->db->update('m_kamarruangan', $data, array('kamarruangan_id' => $id));

        return $update;
    }

    function get_data_instalasi(){
        $this->db->from('m_instalasi');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_pelayanan_list(){
        return $this->db->get('m_kelaspelayanan')->result();
   
   }

    
}
