<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_operasi_model extends CI_Model {
    var $column = array('nama_paket','total_harga','kelaspelayanan_nama');
    var $columnorder = array('','nama_paket','total_harga','kelaspelayanan_nama');
    var $order = array('nama_paket' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_paket_operasi_list_2(){
        $this->db->select('m_paketoperasi.*,m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_paketoperasi');
        $this->db->join('m_kelaspelayanan','m_paketoperasi.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $this->db->order_by('nama_paket','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_paket_operasi_list(){
        $this->db->select('m_paketoperasi.*,m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_paketoperasi');
        $this->db->join('m_kelaspelayanan','m_paketoperasi.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnorder[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_paket_operasi_all(){
        $this->db->select('m_paketoperasi.*,m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_paketoperasi');
        $this->db->join('m_kelaspelayanan','m_paketoperasi.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');

        return $this->db->count_all_results();
    }
    
    public function count_paket_operasi_filtered(){
        $this->db->select('m_paketoperasi.*,m_kelaspelayanan.kelaspelayanan_nama');
        $this->db->from('m_paketoperasi');
        $this->db->join('m_kelaspelayanan','m_paketoperasi.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnorder[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_paket($data=array()){
         $insert = $this->db->insert('m_paketoperasi',$data);
        
        return $insert;
    }
    
    public function get_paket_by_id($id){
        $query = $this->db->get_where('m_paketoperasi', array('paketoperasi_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_paket($data, $id){
        $update = $this->db->update('m_paketoperasi', $data, array('paketoperasi_id' => $id));
        
        return $update;
    }
    
    public function delete_paket($id){
        $tindakan = $this->db->delete('m_paketoperasidetail', array('paketoperasi_id' => $id));
        $obat = $this->db->delete('m_paketoperasiobat', array('paketoperasi_id' => $id));
        $delete = $this->db->delete('m_paketoperasi', array('paketoperasi_id' => $id));
        
        return $delete && $tindakan && $obat;
    }
    
    public function get_pelayanan_list(){
         return $this->db->get('m_kelaspelayanan')->result();
    }
    
    public function get_tindakan_paket($paketoperasi_id){
        $this->db->select('m_paketoperasidetail.*, m_daftartindakan.daftartindakan_nama');
        $this->db->from('m_paketoperasidetail');
        $this->db->join('m_tariftindakan','m_tariftindakan.tariftindakan_id = m_paketoperasidetail.tariftindakan_id');
        $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('m_paketoperasidetail.paketoperasi_id',$paketoperasi_id);

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tindakan($kelaspelayanan_id, $is_bpjs){
        $this->db->select('m_tariftindakan.*, m_daftartindakan.daftartindakan_nama');
        $this->db->from('m_tariftindakan');
        $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('m_tariftindakan.kelaspelayanan_id',$kelaspelayanan_id);
        //$this->db->where('m_daftartindakan.kelompoktindakan_id','2');
        $this->db->where('m_tariftindakan.komponentarif_id','1'); //komponen total
        if($is_bpjs == '1'){
            $this->db->where('m_tariftindakan.bpjs_non_bpjs','BPJS');
        }else{
            $this->db->where('m_tariftindakan.bpjs_non_bpjs','Non BPJS');
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tarif_tindakan($tariftindakan_id){
        $query = $this->db->get_where('m_tariftindakan', array('tariftindakan_id' => $tariftindakan_id), 1, 0);

        return $query->row();
    }
    
    public function insert_tindakan_paket($data){
        $insert = $this->db->insert('m_paketoperasidetail',$data);
        
        return $insert;
    }
    
    public function get_total_harga($paketoperasi_id){
        $totalhargaobat = $this->get_total_harga_obat($paketoperasi_id);
        $this->db->select('m_paketoperasi.paketoperasi_id,SUM(m_paketoperasidetail.total_bayar) as tot_harga_tindakan');
        $this->db->from('m_paketoperasi');
        $this->db->join('m_paketoperasidetail','m_paketoperasidetail.paketoperasi_id = m_paketoperasi.paketoperasi_id','left');
        $this->db->where('m_paketoperasi.paketoperasi_id', $paketoperasi_id);
        $this->db->group_by('paketoperasi_id');
        
        $query = $this->db->get();
        $arr = $query->result();
        $totalhargatindakan = !empty($arr[0]->tot_harga_tindakan) ? $arr[0]->tot_harga_tindakan : 0;
        
        $jumlah = intval($totalhargatindakan) + intval($totalhargaobat);
        return $jumlah;
    }
    
    function get_total_harga_obat($paketoperasi_id){
        $this->db->select('m_paketoperasi.paketoperasi_id,SUM(m_paketoperasiobat.total_harga) as tot_harga_obat');
        $this->db->from('m_paketoperasi');
        $this->db->join('m_paketoperasiobat','m_paketoperasiobat.paketoperasi_id = m_paketoperasi.paketoperasi_id','left');
        $this->db->where('m_paketoperasi.paketoperasi_id', $paketoperasi_id);
        $this->db->group_by('paketoperasi_id');
        
        $query = $this->db->get();
        $arr = $query->result();
        $totalhargaobat = !empty($arr[0]->tot_harga_obat) ? $arr[0]->tot_harga_obat : 0;
        
        return $totalhargaobat;
    }
    
    public function hapus_paket_tindakan($paketoperasidetail_id, $paketoperasi_id){
        $tindakan = $this->db->delete('m_paketoperasidetail', array('detailpaket_id' => $paketoperasidetail_id));
        
        $count_total = $this->get_total_harga($paketoperasi_id);
        $dataupdate = array(
            'totalharga' => $count_total
        );
        $update = $this->update_paket($dataupdate, $paketoperasi_id);
        
        return $tindakan && $update;
    }
    
    public function get_obat_paket($paketoperasi_id){
        $this->db->select('m_paketoperasiobat.*, m_obat.nama_obat,m_jenisobatalkes.jenisoa_nama');
        $this->db->from('m_paketoperasiobat');
        $this->db->join('m_obat','m_obat.obat_id = m_paketoperasiobat.obat_id');
        $this->db->join('m_jenisobatalkes','m_jenisobatalkes.jenisoa_id = m_obat.jenis_obat');
        $this->db->where('m_paketoperasiobat.paketoperasi_id',$paketoperasi_id);

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_obat($jenis_oa){
        $this->db->from('v_stokobatalkes');
        $this->db->where('jenis_obat',$jenis_oa);
        
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tarif_obat($obat_id){
        $query = $this->db->get_where('v_stokobatalkes', array('obat_id' => $obat_id), 1, 0);

        return $query->row();
    }
    
    public function insert_obat_paket($data){
        $insert = $this->db->insert('m_paketoperasiobat',$data);
        
        return $insert;
    }
    
    public function hapus_paket_obat($paketoperasiobat_id, $paketoperasi_id){
        $obat = $this->db->delete('m_paketoperasiobat', array('paketoperasiobat_id' => $paketoperasiobat_id));
        
        $count_total = $this->get_total_harga($paketoperasi_id);
        $dataupdate = array(
            'totalharga' => $count_total
        );
        $update = $this->update_paket($dataupdate, $paketoperasi_id);
        
        return $obat && $update;
    }
}