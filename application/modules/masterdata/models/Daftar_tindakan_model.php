<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_tindakan_model extends CI_Model {
    var $column = array('daftartindakan_id','daftartindakan_nama','m_kelompoktindakan.kelompoktindakan_nama');
    var $order = array('daftartindakan_nama' => 'ASC');    
    
    public function __construct() {
        parent::__construct();
    }

    public function get_sediaan_obat(){
        $this->db->from('m_sediaan_obat');  
        $query = $this->db->get();

        return $query->result(); 
    }

    public function get_bhp_dan_alkes(){
        // $query = $this->db->get_where('m_barang_farmasi', array('id_jenis_barang' => 2, 'id_jenis_barang' => 3));

        // $array = array('id_jenis_barang' => 2, 'id_jenis_barang' => 3);
        // $this->db->select('m_barang_farmasi.*');
        // $this->db->from('m_sediaan_obat');
        // $this->db->where('id_jenis_barang', '2');
        // $this->db->or_where('id_jenis_barang', '3'); 
        // $this->db->or_where($array); 

        // $id = array('2', '3');
        // $this->db->select('m_barang_farmasi');
        // $this->db->from('m_sediaan_obat');
        // $this->db->where_in('id_jenis_barang', $array('2', '3'));

        // $this->db->get_where("(id_jenis_barang='2' OR id_jenis_barang='3')");
        // $query = $this->db->get();

        $query = $this->db->query("select * from m_barang_farmasi where id_jenis_barang='2' OR id_jenis_barang='3'");
        return $query->result();


        // $this->db->where("(id_jenis_barang='2' OR id_jenis_barang='3')");

        // $this->db->where('id_jenis_barang',  '2');
        // $this->db->or_where('id_jenis_barang', '3');
    }

    public function get_name_tindakan(){
        // $this->db->select('m_tindakan.daftartindakan_nama');
        $this->db->from('m_tindakan');  
        $query = $this->db->get();

        return $query->result(); 
    }
    
    public function get_daftar_tindakan_list_2(){
        $this->db->select('m_tindakan.*, m_kelompoktindakan.kelompoktindakan_nama, m_kelaspelayanan.kelaspelayanan_nama, m_komponentarif.komponentarif_nama');
        $this->db->from('m_tindakan');
        $this->db->join('m_kelompoktindakan','m_kelompoktindakan.kelompoktindakan_id = m_tindakan.kelompoktindakan_id','left');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_tindakan.kelaspelayanan_id','left');
        $this->db->join('m_komponentarif','m_komponentarif.komponentarif_id = m_tindakan.komponentarif_id','left');  
        $this->db->order_by('daftartindakan_nama','ASC');
        $query = $this->db->get();
        
        return $query->result();
    }

    public function get_daftar_tindakan_list(){
        $this->db->select('m_tindakan.*, m_kelompoktindakan.kelompoktindakan_nama, m_kelaspelayanan.kelaspelayanan_nama, m_komponentarif.komponentarif_nama');
        $this->db->from('m_tindakan');
        $this->db->join('m_kelompoktindakan','m_kelompoktindakan.kelompoktindakan_id = m_tindakan.kelompoktindakan_id','left');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_tindakan.kelaspelayanan_id','left');
        $this->db->join('m_komponentarif','m_komponentarif.komponentarif_id = m_tindakan.komponentarif_id','left');  
         $i = 0;   
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_daftar_tindakan_all(){
        $this->db->select('m_tindakan.*, m_kelompoktindakan.kelompoktindakan_nama, m_kelaspelayanan.kelaspelayanan_nama, m_komponentarif.komponentarif_nama');
        $this->db->from('m_tindakan');
        $this->db->join('m_kelompoktindakan','m_kelompoktindakan.kelompoktindakan_id = m_tindakan.kelompoktindakan_id','left');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_tindakan.kelaspelayanan_id','left');
        $this->db->join('m_komponentarif','m_komponentarif.komponentarif_id = m_tindakan.komponentarif_id','left'); 

        return $this->db->count_all_results();
    }
    
    public function count_daftar_tindakan_filtered(){
         $this->db->select('m_tindakan.*, m_kelompoktindakan.kelompoktindakan_nama, m_kelaspelayanan.kelaspelayanan_nama, m_komponentarif.komponentarif_nama');
        $this->db->from('m_tindakan');
        $this->db->join('m_kelompoktindakan','m_kelompoktindakan.kelompoktindakan_id = m_tindakan.kelompoktindakan_id','left');
        $this->db->join('m_kelaspelayanan','m_kelaspelayanan.kelaspelayanan_id = m_tindakan.kelaspelayanan_id','left');
        $this->db->join('m_komponentarif','m_komponentarif.komponentarif_id = m_tindakan.komponentarif_id','left'); 
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_daftar_tindakan($data=array()){
        $insert = $this->db->insert('m_tindakan',$data);
        if ($insert){
            $this->db->select("daftartindakan_id");
            $this->db->from("m_tindakan");
            $this->db->order_by("daftartindakan_id", "desc");
            $this->db->limit(1);
            return $this->db->get()->row()->daftartindakan_id;
        }
        return 0;
    }
    
    public function get_daftar_tindakan_by_id($id){ 
        $query = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_daftar_tindakan($data, $id){
        $update = $this->db->update('m_tindakan', $data, array('daftartindakan_id' => $id));
        return $update;  
    }
    
    public function delete_daftar_tindakan($id){
        $delete = $this->db->delete('m_tindakan', array('daftartindakan_id' => $id));
        
        return $delete;
    }
    
    public function get_kelompoktindakan_list(){
         return $this->db->get('m_kelompoktindakan')->result();
    }
    public function get_kelas_list(){  
         return $this->db->get('m_kelaspelayanan')->result();
    }
    public function get_daftar_list(){  
         return $this->db->get('m_daftartindakan')->result();
    }
    public function get_komponen_tarif(){
         return $this->db->get('m_komponentarif')->result();
    }
    
    // TODO:: Ambil daftar ICD dari tabel m_icd_9
    public function get_icd_9_cm() {
        $this->db->order_by("nama_diagnosa", "asc");
        return $this->db->get("m_icd_9_cm")->result();
    }
 
    public function get_komponen_tarif_by_name($name) {
        $this->db->select("m_komponentarif.komponentarif_id");
        $this->db->from("m_komponentarif");
        $this->db->where("m_komponentarif.komponentarif_nama", $name);
        return $this->db->get()->row()->komponentarif_id;
    }

    public function insertKomponenTarif($data) {
        $insert = $this->db->insert("m_tindakan_komponen_tarif", $data);
        return $insert;
    }

    public function updateKomponenTarif($data, $daftartindakan_id, $komponentarif_id) {
        $insert = $this->db->update("m_tindakan_komponen_tarif", $data, array(
            'daftar_tindakan_id'    => $daftartindakan_id,
            'komponentarif_id'      => $komponentarif_id
        ));
        return $insert;
    }

    public function insertBHP($data) {
        $insert = $this->db->insert("m_det_tindakan_bhp", $data);
        return $insert;
    }

    public function updateBHP($data, $daftartindakan_id) {
        $this->db->where("daftar_tindakan_id", $daftartindakan_id);
        $insert = $this->db->update("m_det_tindakan_bhp", $data, array(
            'daftar_tindakan_id'    => $daftartindakan_id
        ));
        return $insert;
    }

    public function get_daftar_tindakan_bhp($daftartindakan_id) {
        $this->db->select("m_det_tindakan_bhp.*,
                        m_barang_farmasi.kode_barang,
                        m_barang_farmasi.nama_barang,
                        m_sediaan_obat.nama_sediaan");
        $this->db->join("m_barang_farmasi", "m_barang_farmasi.id_barang = m_det_tindakan_bhp.barang_farmasi_id", "left");
        $this->db->join("m_sediaan_obat", "m_sediaan_obat.id_sediaan = m_det_tindakan_bhp.sediaan_obat_id", "left");
        $this->db->where("m_det_tindakan_bhp.daftar_tindakan_id", $daftartindakan_id)
                ->where("(m_barang_farmasi.id_jenis_barang='2' OR m_barang_farmasi.id_jenis_barang='3')");
        return $this->db->get("m_det_tindakan_bhp")->result();
    }

    public function get_daftar_tindakan_komponentarif($daftartindakan_id) {
        $this->db->select("m_tindakan_komponen_tarif.*, m_komponentarif.komponentarif_nama");
        $this->db->join("m_komponentarif", "m_komponentarif.komponentarif_id = m_tindakan_komponen_tarif.komponentarif_id", "left");
        $this->db->where("m_tindakan_komponen_tarif.daftar_tindakan_id", $daftartindakan_id);
        return $this->db->get("m_tindakan_komponen_tarif")->result();
    }
}