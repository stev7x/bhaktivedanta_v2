<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_kerja_model extends CI_Model {
    var $column = array('unit_kerja_id','unit_kerja_nama');
    var $order = array('unit_kerja_nama' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_unit_kerja_list_2(){
        $this->db->from('m_unit_kerja');
        $this->db->order_by('unit_kerja_nama','ASC');
        $query = $this->db->get();
        
        return $query->result();
    }

    public function get_unit_kerja_list(){
        $this->db->from('m_unit_kerja');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_unit_kerja_all(){
        $this->db->from('m_unit_kerja');

        return $this->db->count_all_results();
    }
    
    public function count_unit_kerja_filtered(){
        $this->db->from('m_unit_kerja');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_unit_kerja($data=array()){
        $insert = $this->db->insert('m_unit_kerja',$data);
        
        return $insert;
    }
    
    public function get_unit_kerja_by_id($id){
        $query = $this->db->get_where('m_unit_kerja', array('unit_kerja_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_unit_kerja($data, $id){
        $update = $this->db->update('m_unit_kerja', $data, array('unit_kerja_id' => $id));
        
        return $update;
    }
    
    public function delete_unit_kerja($id){
        $delete = $this->db->delete('m_unit_kerja', array('unit_kerja_id' => $id));
        
        return $delete;
    }
    
 
}