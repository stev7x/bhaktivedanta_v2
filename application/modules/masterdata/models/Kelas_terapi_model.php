<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_terapi_model extends CI_Model {
    var $column = array('id_kelas_terapi','nama_kelas_terapi', 'keterangan');
    var $order = array('id_kelas_terapi' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_kelas_terapi_list(){
        $this->db->from('m_kelasterapi_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_kelas_terapi_all(){
        $this->db->from('m_kelasterapi_obat');

        return $this->db->count_all_results();
    }
    
    public function count_kelas_terapi_filtered(){
        $this->db->from('m_kelasterapi_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_kelas_terapi($data=array()){
        $insert = $this->db->insert('m_kelasterapi_obat',$data);
        
        return $insert;
    }
    
    public function get_kelas_terapi_by_id($id){
        $query = $this->db->get_where('m_kelasterapi_obat', array('id_kelas_terapi' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_kelas_terapi($data, $id){
        $update = $this->db->update('m_kelasterapi_obat', $data, array('id_kelas_terapi' => $id));
        
        return $update;
    }
    
    public function delete_kelas_terapi($id){
        $delete = $this->db->delete('m_kelasterapi_obat', array('id_kelas_terapi' => $id));
        
        return $delete;
    }
    
 
}