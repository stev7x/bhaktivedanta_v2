<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten_model extends CI_Model {
    var $column = array('kabupaten_id','kabupaten_nama','propinsi_nama');
    var $order = array('kabupaten_nama' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_kabupaten_list(){
        $this->db->select('m_kabupaten.*,m_propinsi.propinsi_nama');
        $this->db->join('m_propinsi','m_propinsi.propinsi_id = m_kabupaten.propinsi_id');
        $this->db->from('m_kabupaten');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_kabupaten_all(){
        $this->db->from('m_kabupaten');

        return $this->db->count_all_results();
    }
    
    public function count_kabupaten_filtered(){
        $this->db->select('m_kabupaten.*,m_propinsi.propinsi_nama');
        $this->db->join('m_propinsi','m_propinsi.propinsi_id = m_kabupaten.propinsi_id');
        $this->db->from('m_kabupaten');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_kabupaten($data=array()){
         $insert = $this->db->insert('m_kabupaten',$data);
        
        return $insert;
    }
    
    public function get_kabupaten_by_id($id){
        $query = $this->db->get_where('m_kabupaten', array('kabupaten_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_kabupaten($data, $id){
        $update = $this->db->update('m_kabupaten', $data, array('kabupaten_id' => $id));
        
        return $update;
    }
    
    public function delete_kabupaten($id){
        $delete = $this->db->delete('m_kabupaten', array('kabupaten_id' => $id));
        
        return $delete;
    }
    public function get_provinsi_list(){
        $this->db->order_by("propinsi_nama","ASC");
        $this->db->from('m_propinsi');
        $query = $this->db->get();
        
        return $query->result();
    }

    function get_data_instalasi(){
        $this->db->from('m_instalasu');
        $query = $this->db->get();
        return $query->result();
    }
    
 
}