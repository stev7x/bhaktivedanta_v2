<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instalasi_model extends CI_Model {
    var $column = array('instalasi_id','instalasi_nama');
    var $order = array('instalasi_nama' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_instalasi_list_2(){
        $this->db->from('m_instalasi');
        $this->db->order_by('instalasi_nama','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_instalasi_list(){
        $this->db->from('m_instalasi');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_instalasi_all(){
        $this->db->from('m_instalasi');

        return $this->db->count_all_results();
    }
    
    public function count_instalasi_filtered(){
        $this->db->from('m_instalasi');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_instalasi($data=array()){
         $insert = $this->db->insert('m_instalasi',$data);
        
        return $insert;
    }
    
    public function get_instalasi_by_id($id){
        $query = $this->db->get_where('m_instalasi', array('instalasi_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_instalasi($data, $id){
        $update = $this->db->update('m_instalasi', $data, array('instalasi_id' => $id));
        
        return $update;
    }
    
    public function delete_instalasi($id){
        $delete = $this->db->delete('m_instalasi', array('instalasi_id' => $id));
        
        return $delete;
    }
    
 
}