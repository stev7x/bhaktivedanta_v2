<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan_model extends CI_Model {
    var $column = array('kecamatan_id','kecamatan_nama','kabupaten_nama');
    var $order = array('kecamatan_nama' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_kecamatan_list(){
        $this->db->select('m_kecamatan.*,m_kabupaten.kabupaten_nama');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_kecamatan.kabupaten_id');
        $this->db->from('m_kecamatan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_kecamatan_all(){
        $this->db->from('m_kecamatan');

        return $this->db->count_all_results();
    }
    
    public function count_kecamatan_filtered(){
        $this->db->select('m_kecamatan.*,m_kabupaten.kabupaten_nama');
        $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_kecamatan.kabupaten_id');
        $this->db->from('m_kecamatan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_kecamatan($data=array()){
         $insert = $this->db->insert('m_kecamatan',$data);
        
        return $insert;
    }
    
    public function get_kecamatan_by_id($id){
        $query = $this->db->get_where('m_kecamatan', array('kecamatan_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_kecamatan($data, $id){
        $update = $this->db->update('m_kecamatan', $data, array('kecamatan_id' => $id));
        
        return $update;
    }
    
    public function delete_kecamatan($id){
        $delete = $this->db->delete('m_kecamatan', array('kecamatan_id' => $id));
        
        return $delete;
    }
    public function get_kabupaten_list(){
         return $this->db->get('m_kabupaten')->result();
    }
    
 
}