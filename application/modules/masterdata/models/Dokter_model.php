<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter_model extends CI_Model {
    var $column = array('id_M_DOKTER','NAME_DOKTER','ALAMAT','m_kelompokdokter.kelompokdokter_nama');
    var $order = array('NAME_DOKTER' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_dokter_list_2(){
        $this->db->select('m_dokter.*, m_kelompokdokter.kelompokdokter_nama');
        $this->db->join('m_kelompokdokter','m_kelompokdokter.kelompokdokter_id = m_dokter.kelompokdokter_id');
        // $this->db->get('m_dokter');  
        $this->db->order_by('NAME_DOKTER','ASC'); 
        $query = $this->db->get('m_dokter'); 
        return $query->result(); 
    } 

    public function get_dokter_list(){
        $this->db->from('m_dokter');
        $this->db->join('m_kelompokdokter', 'm_kelompokdokter.kelompokdokter_id = m_dokter.kelompokdokter_id' , 'left' );
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_dokter_all(){
        $this->db->from('m_dokter');
        $this->db->join('m_kelompokdokter', 'm_kelompokdokter.kelompokdokter_id = m_dokter.kelompokdokter_id' , 'left' );

        return $this->db->count_all_results();
    }
    
    public function count_dokter_filtered(){
        $this->db->from('m_dokter');
        $this->db->join('m_kelompokdokter', 'm_kelompokdokter.kelompokdokter_id = m_dokter.kelompokdokter_id' , 'left' );
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_dokter($data=array()){
         $insert = $this->db->insert('m_dokter',$data);
        
        return $insert;
    }
    public function get_dokter_by_id($id){
        $query = $this->db->get_where('m_dokter', array('id_M_DOKTER' => $id), 1, 0);
        
        return $query->row();
    }
    public function update_dokter($data, $id){
        $update = $this->db->update('m_dokter', $data, array('id_M_DOKTER' => $id));
        
        return $update;
    }
    public function delete_dokter($id){
        $delete = $this->db->delete('m_dokter', array('id_M_DOKTER' => $id));
        
        return $delete;
    }
    
    public function get_kel_dokter_list(){
         return $this->db->get('m_kelompokdokter')->result();
    }
 
}