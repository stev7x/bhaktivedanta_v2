<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan_model extends CI_Model {
    var $column = array('kelurahan_id','kelurahan_nama','kecamatan_nama');
    var $order = array('kelurahan_nama' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_kelurahan_list(){
        $this->db->select('m_kelurahan.*,m_kecamatan.kecamatan_nama');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_kelurahan.kecamatan_id');
        $this->db->from('m_kelurahan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_kelurahan_all(){
        $this->db->from('m_kelurahan');

        return $this->db->count_all_results();
    }
    
    public function count_kelurahan_filtered(){
        $this->db->select('m_kelurahan.*,m_kecamatan.kecamatan_nama');
        $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_kelurahan.kecamatan_id');
        $this->db->from('m_kelurahan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_kelurahan($data=array()){
         $insert = $this->db->insert('m_kelurahan',$data);
        
        return $insert;
    }
    
    public function get_kelurahan_by_id($id){
        $query = $this->db->get_where('m_kelurahan', array('kelurahan_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_kelurahan($data, $id){
        $update = $this->db->update('m_kelurahan', $data, array('kelurahan_id' => $id));
        
        return $update;
    }
    
    public function delete_kelurahan($id){
        $delete = $this->db->delete('m_kelurahan', array('kelurahan_id' => $id));
        
        return $delete;
    }
    public function get_kecamatan_list(){
         return $this->db->get('m_kecamatan')->result();
    }
    
 
}