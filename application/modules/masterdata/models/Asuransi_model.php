<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asuransi_model extends CI_Model {
    var $column = array('asuransi_id','NAME_DOKTER','ALAMAT','m_kelompokasuransi.kelompokasuransi_nama');
    var $order = array('NAME_DOKTER' => 'ASC');

    public function __construct() {
        parent::__construct();
    }

    public function get_asuransi_list_2(){
        $this->db->select('m_asuransi.*, m_kelompokasuransi.kelompokasuransi_nama');
        $this->db->join('m_kelompokasuransi','m_kelompokasuransi.kelompokasuransi_id = m_asuransi.kelompokasuransi_id');
        // $this->db->get('m_asuransi');
        $this->db->order_by('NAME_DOKTER','ASC');
        $query = $this->db->get('m_asuransi');
        return $query->result();
    }

    public function get_asuransi_list(){
        $this->db->from('m_asuransi');
        $i = 0;

        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
//        $order_column = $this->input->get('order');
//        if($order_column !== false){
//            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
//        }
//        else if(isset($this->order)){
//            $order = $this->order;
//            $this->db->order_by(key($order), $order[key($order)]);
//        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_asuransi_all(){
        $this->db->from('m_asuransi');

        return $this->db->count_all_results();
    }

    public function count_asuransi_filtered(){
        $this->db->from('m_asuransi');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
//        $order_column = $this->input->get('order');
//        if($order_column !== false){
//            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
//        }
//        else if(isset($this->order)){
//            $order = $this->order;
//            $this->db->order_by(key($order), $order[key($order)]);
//        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function insert_asuransi($data=array()){
        $insert = $this->db->insert('m_asuransi',$data);

        return $insert;
    }
    public function get_asuransi_by_id($id){
        $query = $this->db->get_where('m_asuransi', array('asuransi_id' => $id), 1, 0);

        return $query->row();
    }
    public function update_asuransi($data, $id){
        $update = $this->db->update('m_asuransi', $data, array('asuransi_id' => $id));

        return $update;
    }
    public function delete_asuransi($id){
        $delete = $this->db->delete('m_asuransi', array('asuransi_id' => $id));

        return $delete;
    }

    public function get_kel_asuransi_list(){
        return $this->db->get('m_kelompokasuransi')->result();
    }

    function get_warganegara_by_id($id) {
        $this->db->select('*');
        $this->db->from('m_kewarganegaraan');
        $this->db->where("id", $id);
        $query = $this->db->get();

        return $query->row();
    }
}