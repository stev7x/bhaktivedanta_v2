<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah_model extends CI_Model {
    var $column = array('sekolah_id','sekolah_nama');
    var $order = array('sekolah_id' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_sekolah_list2(){
        $query = $this->db->get('m_sekolah');

        return $query->result();
    }

    public function get_sekolah_list(){
        $this->db->from('m_sekolah');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_sekolah_all(){
        $this->db->from('m_sekolah');

        return $this->db->count_all_results();
    }
    
    public function count_sekolah_filtered(){
        $this->db->from('m_sekolah');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_sekolah($data){
        $insert = $this->db->insert('m_sekolah',$data);
        
        return $insert;
    }
    
    public function get_sekolah_by_id($id){
        $query = $this->db->get_where('m_sekolah', array('sekolah_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_sekolah($data, $id){
        $update = $this->db->update('m_sekolah', $data, array('sekolah_id' => $id));
        
        return $update;
    }
    
    public function delete_sekolah($id){
        $delete = $this->db->delete('m_sekolah', array('sekolah_id' => $id));
        
        return $delete;
    }


    function get_export_sekolah(){
        $this->db->from('m_sekolah');
        // $this->db->where('tgl_masukadmisi >="'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and tgl_pulang <="'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $query = $this->db->get();
        
        return $query->result();
    }
    
 
}