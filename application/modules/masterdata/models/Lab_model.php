<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lab_model extends CI_Model {
    var $column = array('lab_id','nama_pemeriksaan');
    var $order = array('lab_id' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_lab_list_2(){
        $this->db->order_by("lab_id", "ASC");
        $query = $this->db->get("m_lab");
        return $query->result();

    }

    public function get_lab_list(){
        $this->db->from('m_lab');
        $this->db->order_by("lab_id", "ASC");
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_lab_all(){
        $this->db->from('m_lab');

        return $this->db->count_all_results();
    }
    
    public function count_lab_filtered(){
        $this->db->from('m_lab');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_lab($data){
        $insert = $this->db->insert('m_lab',$data);
        
        return $insert;
    }
    
    public function get_lab_by_id($id){
        $query = $this->db->get_where('m_lab', array('lab_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_lab($data, $id){
        $update = $this->db->update('m_lab', $data, array('lab_id' => $id));
        
        return $update;
    }
    
    // public function delete_lab($id){
    //     $delete = $this->db->delete('m_lab', array('lab_id' => $id));
        
    //     return $delete;
    // }


    // function get_export_lab(){
    //     $this->db->from('m_lab');
    //     // $this->db->where('tgl_masukadmisi >="'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and tgl_pulang <="'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
    //     $query = $this->db->get();
        
    //     return $query->result();
    // }
    
 
}