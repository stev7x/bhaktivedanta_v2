<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rencana_tindakan_model extends CI_Model {
    var $column = array('rencana_tindakan_id','rencana_tindakan_nama','m_kelompoktindakan.kelompoktindakan_nama');
    var $order = array('rencana_tindakan_id' => 'ASC');

    public function __construct() {
        parent::__construct();
    }

    public function get_data_kelompoktindakan() {
        return $this->db->get('m_kelompoktindakan')->result();
    }

    public function get_tindakan_list_2(){
        $this->db->select("m_rencana_tindakan.*, m_kelompoktindakan.kelompoktindakan_nama");
        $this->db->join('m_kelompoktindakan','m_rencana_tindakan.kelompoktindakan_id = m_kelompoktindakan.kelompoktindakan_id');
        $this->db->from('m_rencana_tindakan');

        $query = $this->db->get();
        return $query->result();
    }

    public function get_tindakan_list(){
        $this->db->select("m_rencana_tindakan.*, m_kelompoktindakan.kelompoktindakan_nama");
        $this->db->join('m_kelompoktindakan','m_rencana_tindakan.kelompoktindakan_id = m_kelompoktindakan.kelompoktindakan_id');
        $this->db->from('m_rencana_tindakan');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }

    public function count_tindakan_all(){
        $this->db->select("m_rencana_tindakan.*, m_kelompoktindakan.kelompoktindakan_nama");
        $this->db->join('m_kelompoktindakan','m_rencana_tindakan.kelompoktindakan_id = m_kelompoktindakan.kelompoktindakan_id');
        $this->db->from('m_rencana_tindakan');

        return $this->db->count_all_results();
    }

    public function count_tindakan_filtered(){
        $this->db->select("m_rencana_tindakan.*, m_kelompoktindakan.kelompoktindakan_nama");
        $this->db->join('m_kelompoktindakan','m_rencana_tindakan.kelompoktindakan_id = m_kelompoktindakan.kelompoktindakan_id');
        $this->db->from('m_rencana_tindakan');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function insert_rencana_tindakan($data=array()){
        $insert = $this->db->insert('m_rencana_tindakan',$data);
        
        return $insert;
    }

    public function get_rencana_tindakan_by_id($id){
        $query = $this->db->get_where('m_rencana_tindakan', array('rencana_tindakan_id' => $id), 1, 0);
        
        return $query->row();
    }

    public function update_rencana_tindakan($data, $id){
        $update = $this->db->update('m_rencana_tindakan', $data, array('rencana_tindakan_id' => $id));
        
        return $update;
    }

    public function delete_rencana_tindakan($id){
        $delete = $this->db->delete('m_rencana_tindakan', array('rencana_tindakan_id' => $id));
        
        return $delete;
    }
}


?>