<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Efek_samping_obat_model extends CI_Model {
    var $column = array('id_efek_samping','kode_efek_samping', 'nama_efek_samping','keterangan');
    var $order = array('id_efek_samping' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_efek_samping_list_2(){
        $this->db->from('m_efeksamping_obat');
        $this->db->order_by('kode_efek_samping','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_efek_samping_list(){
        $this->db->from('m_efeksamping_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_efek_samping_all(){
         $this->db->from('m_efeksamping_obat');

        return $this->db->count_all_results();
    }
    
    public function count_efek_samping_filtered(){
         $this->db->from('m_efeksamping_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_efek_samping($data=array()){
        $insert = $this->db->insert('m_efeksamping_obat',$data);
        
        return $insert;
    }
    
    public function get_efek_samping_by_id($id){
        $query = $this->db->get_where('m_efeksamping_obat', array('id_efek_samping' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_efek_samping($data, $id){
        $update = $this->db->update('m_efeksamping_obat', $data, array('id_efek_samping' => $id));
        
        return $update;
    }
    
    public function delete_efek_samping($id){
        $delete = $this->db->delete('m_efeksamping_obat', array('id_efek_samping' => $id));
        
        return $delete;
    }
    
    // public function get_kelas_terapi_list(){
    //     return $this->db->get('m_kelasterapi_obat')->result();
    // }
 
}