<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarif_tindakan_model extends CI_Model {
    var $column = array('tariftindakan_id','harga_tindakan','daftartindakan_nama','kelaspelayanan_nama','cyto_tindakan');
    var $order = array('harga_tindakan' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_tarif_tindakan_list(){
        $this->db->select("m_tariftindakan.*,m_daftartindakan.daftartindakan_nama,kelaspelayanan_nama,m_komponentarif.komponentarif_nama");
        $this->db->join('m_kelaspelayanan','m_tariftindakan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $this->db->join('m_daftartindakan','m_tariftindakan.daftartindakan_id = m_daftartindakan.daftartindakan_id');
        $this->db->join('m_komponentarif','m_tariftindakan.komponentarif_id = m_komponentarif.komponentarif_id');
        $this->db->from('m_tariftindakan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_tarif_tindakan_all(){
        $this->db->join('m_kelaspelayanan','m_tariftindakan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $this->db->join('m_daftartindakan','m_tariftindakan.daftartindakan_id = m_daftartindakan.daftartindakan_id');
        $this->db->join('m_komponentarif','m_tariftindakan.komponentarif_id = m_komponentarif.komponentarif_id');
        $this->db->from('m_tariftindakan');

        return $this->db->count_all_results();
    }
    
    public function count_tarif_tindakan_filtered(){
        $this->db->select("m_tariftindakan.*,m_daftartindakan.daftartindakan_nama,kelaspelayanan_nama,m_komponentarif.komponentarif_nama");
        $this->db->join('m_kelaspelayanan','m_tariftindakan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $this->db->join('m_daftartindakan','m_tariftindakan.daftartindakan_id = m_daftartindakan.daftartindakan_id');
        $this->db->join('m_komponentarif','m_tariftindakan.komponentarif_id = m_komponentarif.komponentarif_id');
        $this->db->from('m_tariftindakan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_tarif_tindakan($data=array()){
         $insert = $this->db->insert('m_tariftindakan',$data);
        
        return $insert;
    }
    
    public function get_tarif_tindakan_by_id($id){
        $this->db->select("m_tariftindakan.*,m_daftartindakan.daftartindakan_nama,kelaspelayanan_nama");
        $this->db->join('m_kelaspelayanan','m_tariftindakan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $this->db->join('m_daftartindakan','m_tariftindakan.daftartindakan_id = m_daftartindakan.daftartindakan_id');
        $query = $this->db->get_where('m_tariftindakan', array('tariftindakan_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_tarif_tindakan($data, $id){
        $update = $this->db->update('m_tariftindakan', $data, array('tariftindakan_id' => $id));
        
        return $update;
    }
    
    public function delete_tarif_tindakan($id){
        $delete = $this->db->delete('m_tariftindakan', array('tariftindakan_id' => $id));
        
        return $delete;
    }
    public function get_kelas_list(){
         return $this->db->get('m_kelaspelayanan')->result();
    }
    public function get_daftar_list(){
         return $this->db->get('m_daftartindakan')->result();
    }
    public function get_komponen_tarif(){
         return $this->db->get('m_komponentarif')->result();
    }
}