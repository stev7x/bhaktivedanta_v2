<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok_dokter_model extends CI_Model{
	var $table = "m_kelompokdokter";
    var $column = array('kelompokdokter_id','kelompokdokter_nama');
    var $order = array('kelompokdokter_nama' => 'ASC');

	public function __construct() {
        parent::__construct();
    }

    public function get_kel_dokter_datatables(){
    	$this->db->from($this->table);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();

    }
    
    public function count_kel_dokter_all(){
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }
    
    public function count_kel_dokter_filtered(){
        $this->db->from($this->table);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_kel_dokter($data=array()){
         $insert = $this->db->insert($this->table,$data);
        
        return $insert;
    }
    public function get_kel_dokter_by_id($id){
        $query = $this->db->get_where($this->table, array('kelompokdokter_id' => $id), 1, 0);
        
        return $query->row();
    }
    public function update_kel_dokter($data, $id){
        $update = $this->db->update($this->table, $data, array('kelompokdokter_id' => $id));
        
        return $update;
    }
    public function delete_kel_dokter($id){
        $delete = $this->db->delete($this->table, array('kelompokdokter_id' => $id));
        
        return $delete;
    }
}