<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Jadwal_dokter_model extends CI_Model {
    var $column = array('NAME_DOKTER','hari','nama_poliruangan');       
    var $order = array('jadwal_dokter_id' => 'asc');          
    
    public function __construct() {
        parent::__construct();
    }  
       
    public function get_jadwaldokter_list(){
        $this->db->from('v_jadwal_dokter');         
        $this->db->order_by('jadwal_dokter_id','DESC'); 
        $i = 0;       
        $search_value = $this->input->get('search');
        if($search_value){ 
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }   
        } 
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_jadwaldokter_all(){
        $this->db->from('v_jadwal_dokter');

        return $this->db->count_all_results();
    }
    
    public function count_jadwaldokter_filtered(){
        $this->db->from('v_jadwal_dokter');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_jadwaldokter($data=array()){
        var_dump($data);die;
        $insert = $this->db->insert('m_jadwal_dokter',$data);   
        
        return $insert;
    }
    
    public function get_jadwaldokter_by_id($id){                 
        $query = $this->db->get_where('m_jadwal_dokter', array('jadwal_dokter_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_jadwaldokter($data, $id){
        $update = $this->db->update('m_jadwal_dokter', $data, array('jadwal_dokter_id' => $id));
        
        return $update;
    }
          
    public function delete_jadwaldokter($id){
        $delete = $this->db->delete('m_jadwal_dokter', array('jadwal_dokter_id' => $id));
        
        return $delete;    
    }

    public function load_data($data_array){
        for ($i= 0; $i < count($data_array); $i++) {
            $data = array(
                'agama_id' => $data_array[$i]['agama_id'],
                'agama_nama' => $data_array[$i]['agama_nama']
            );

            $cek = $this->db->where('agama_nama', $this->input->post('agama_nama'));
            if ($cek) {
                $this->db->insert('m_agama', $data);
            }
        }
    }

    public function get_dokter(){
        $this->db->from('m_dokter');
        $query = $this->db->get();
        return $query->result();        
    }

    public function getHari(){
        return $this->db->get('m_detail_jadwal')->result();
    }    
    public function get_branch(){   
        $this->db->select('*');   
        $this->db->from('m_branch');  
        $query = $this->db->get();
        return $query->result(); 
    }
}