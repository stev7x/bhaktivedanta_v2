<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imunisasi_model extends CI_Model {
    var $column = array('imunisasi_id','jenis');
    var $order = array('imunisasi_id' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_imunisasi_list_2(){
        $this->db->order_by("imunisasi_id", "ASC");
        $query = $this->db->get("m_imunisasi");
        return $query->result();

    }

    public function get_imunisasi_list(){
        $this->db->from('m_imunisasi');
        $this->db->order_by("imunisasi_id", "ASC");
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_imunisasi_all(){
        $this->db->from('m_imunisasi');

        return $this->db->count_all_results();
    }
    
    public function count_imunisasi_filtered(){
        $this->db->from('m_imunisasi');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_imunisasi($data){
        $insert = $this->db->insert('m_imunisasi',$data);
        
        return $insert;
    }
    
    public function get_imunisasi_by_id($id){
        $query = $this->db->get_where('m_imunisasi', array('imunisasi_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_imunisasi($data, $id){
        $update = $this->db->update('m_imunisasi', $data, array('imunisasi_id' => $id));
        
        return $update;
    }
    
    public function delete_imunisasi($id){
        $delete = $this->db->delete('m_imunisasi', array('imunisasi_id' => $id));
        
        return $delete;
    }


    function get_export_imunisasi(){
        $this->db->from('m_imunisasi');
        // $this->db->where('tgl_masukadmisi >="'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and tgl_pulang <="'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $query = $this->db->get();
        
        return $query->result();
    }
    
 
}