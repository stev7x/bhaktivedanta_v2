<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vaksinasi_model extends CI_Model {
    var $column = array('vaksinasi_id','vaksinasi_nama');
    var $order = array('vaksinasi_id' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_vaksinasi_list_2(){
        $this->db->order_by("vaksinasi_id", "ASC");
        $query = $this->db->get("m_vaksinasi");
        return $query->result();

    }

    public function get_vaksinasi_list(){
        $this->db->from('m_vaksinasi');
        $this->db->order_by("vaksinasi_id", "ASC");
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_vaksinasi_all(){
        $this->db->from('m_vaksinasi');

        return $this->db->count_all_results();
    }
    
    public function count_vaksinasi_filtered(){
        $this->db->from('m_vaksinasi');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_vaksinasi($data){
        $insert = $this->db->insert('m_vaksinasi',$data);
        
        return $insert;
    }
    
    public function get_vaksinasi_by_id($id){
        $query = $this->db->get_where('m_vaksinasi', array('vaksinasi_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_vaksinasi($data, $id){
        $update = $this->db->update('m_vaksinasi', $data, array('vaksinasi_id' => $id));
        
        return $update;
    }
    
    public function delete_vaksinasi($id){
        $delete = $this->db->delete('m_vaksinasi', array('vaksinasi_id' => $id));
        
        return $delete;
    }


    function get_export_vaksinasi(){
        $this->db->from('m_vaksinasi');
        // $this->db->where('tgl_masukadmisi >="'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and tgl_pulang <="'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $query = $this->db->get();
        
        return $query->result();
    }
    
 
}