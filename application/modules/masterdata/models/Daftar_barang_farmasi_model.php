<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_barang_farmasi_model extends CI_Model {
    var $column = array('nama_barang');
    var $order = array('id_barang' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_efek_samping($id){
        $this->db->select('m_detail_barang_farmasi.*, m_efeksamping_obat.nama_efek_samping');
        $this->db->join('m_efeksamping_obat', 'm_efeksamping_obat.id_efek_samping = m_detail_barang_farmasi.id_efek_samping');
        $this->db->from('m_detail_barang_farmasi');
        $this->db->where('m_detail_barang_farmasi.id_barang',$id);

        $query = $this->db->get();
        return $query->result();
    }
    public function get_barang_farmasi_list(){
        // $this->db->from('m_barang_farmasi');
        $this->db->select('m_barang_farmasi.*, m_jenis_barang_farmasi.nama_jenis, m_sediaan_obat.nama_sediaan');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = m_barang_farmasi.id_jenis_barang','left');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = m_barang_farmasi.id_sediaan','left');
        $this->db->from('m_barang_farmasi');

        // $this->db->query('SELECT m_barang_farmasi.* , m_efeksamping_obat.`nama_efek_samping`
        // FROM m_barang_farmasi, m_detail_barang_farmasi, m_efeksamping_obat
        // WHERE m_barang_farmasi.`id_barang` = m_detail_barang_farmasi.`id_barang` AND m_detail_barang_farmasi.`id_efek_samping` = m_efeksamping_obat.`id_efek_samping`');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_barang_farmasi_all(){
        $this->db->from('m_barang_farmasi');

        return $this->db->count_all_results();
    }
    
    public function count_barang_farmasi_filtered(){
        $this->db->from('m_barang_farmasi');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_barang_farmasi($data=array()){
        $insert = $this->db->insert('m_barang_farmasi',$data);
        return $insert;
    }
    
    public function get_barang_farmasi_by_id($id){
        if($id == 1){
        $sql = "SELECT m_barang_farmasi.*, m_detail_barang_farmasi.`id_efek_samping` 
        FROM m_barang_farmasi
        INNER JOIN m_detail_barang_farmasi ON m_detail_barang_farmasi.`id_barang` = m_barang_farmasi.`id_barang`
        WHERE m_barang_farmasi.`id_barang` 
        IN(SELECT id_barang FROM m_detail_barang_farmasi WHERE id_barang = $id)";
        }else{
        $sql = "SELECT * FROM m_barang_farmasi where id_barang = $id";
        }
        $query = $this->db->query($sql);
        return $query->row(); 
    }

    public function get_barang_farmasi_by_id_detail($id){
        $this->db->select('m_barang_farmasi.* ,m_interaksi_obat.nama_interaksi, m_golongan_obat.nama_golongan, m_indikasi_obat.nama_indikasi, m_kategori_obat.nama_kategori, m_kontraindikasi_obat.nama_kontra_indikasi, m_kelompok_obat.nama_kelompok');
        $this->db->join('m_interaksi_obat','m_interaksi_obat.id_interaksi = m_barang_farmasi.id_interaksi','left');
        $this->db->join('m_golongan_obat','m_golongan_obat.id_golongan = m_barang_farmasi.id_golongan','left');
        $this->db->join('m_indikasi_obat','m_indikasi_obat.id_indikasi = m_barang_farmasi.id_indikasi','left');
        $this->db->join('m_kategori_obat','m_kategori_obat.id_kategori = m_barang_farmasi.id_kategori','left');
        $this->db->join('m_kontraindikasi_obat','m_kontraindikasi_obat.id_kontra_indikasi = m_barang_farmasi.id_kontra_indikasi','left');
        $this->db->join('m_kelompok_obat','m_kelompok_obat.id_kelompok = m_barang_farmasi.id_kelompok','left');
        $this->db->from('m_barang_farmasi');
        $this->db->where('m_barang_farmasi.id_barang',$id);
        
        $query = $this->db->get();

        return $query->row();
    }
    
    public function update_barang_farmasi($data, $id){
        $update = $this->db->update('m_barang_farmasi', $data, array('id_barang' => $id));
        
        return $update;
    }
    
    public function delete_barang_farmasi($id){
        $delete = $this->db->delete('m_barang_farmasi', array('id_barang' => $id));
        
        return $delete;
    }

    public function insert_farmasi($data=array()){
        $this->db->insert('m_barang_farmasi', $data);
        $id = $this->db->insert_id();
        return $id;
    }
    

    public function get_list_kelas_terapi(){
        return $this->db->get('m_kelasterapi_obat')->result();
   
    }

    public function get_list_golongan(){
    return $this->db->get('m_golongan_obat')->result();

    }

    public function get_list_kategori(){
    return $this->db->get('m_kategori_obat')->result();

    }
    public function get_list_kelompok(){
    return $this->db->get('m_kelompok_obat')->result();

    }
    public function get_list_auto_stop(){
    return $this->db->get('m_autostop_obat')->result();

    }
    public function get_list_indikasi(){
    return $this->db->get('m_indikasi_obat')->result();

    }
    public function get_list_kontra_indikasi(){
    return $this->db->get('m_kontraindikasi_obat')->result();

    }
    public function get_list_interaksi(){
    return $this->db->get('m_interaksi_obat')->result();

    }
    public function get_list_sediaan(){
    return $this->db->get('m_sediaan_obat')->result();

    }
    public function get_list_efek_samping(){
    return $this->db->get('m_efeksamping_obat')->result();

    }
    // public function get_list_kelompok(){
    // return $this->db->get('m_kelompok_obat')->result();

    // }
 
}