<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lasa_model extends CI_Model {
    var $column = array(    'kode_lasa','keterangan');
    var $order = array('id_lasa' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_lasa_list(){
        $this->db->select('m_lasa_obat.*, m_obat_farmasi.nama_obat');
        $this->db->join('m_obat_farmasi','m_obat_farmasi.id_obat = m_lasa_obat.id_obat');
        $this->db->from('m_lasa_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_lasa_all(){
         $this->db->from('m_lasa_obat');

        return $this->db->count_all_results();
    }
    
    public function count_lasa_filtered(){
         $this->db->from('m_lasa_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_lasa($data=array()){
        $insert = $this->db->insert('m_lasa_obat',$data);
        
        return $insert;
    }
    
    public function get_lasa_by_id($id){
        $query = $this->db->get_where('m_lasa_obat', array('id_lasa' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_lasa($data, $id){
        $update = $this->db->update('m_lasa_obat', $data, array('id_lasa' => $id));
        
        return $update;
    }
    
    public function delete_lasa($id){
        $delete = $this->db->delete('m_lasa_obat', array('id_lasa' => $id));
        
        return $delete;
    }
    
    public function get_obat_list(){
        return $this->db->get('m_obat_farmasi')->result();
    }
 
}