<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sediaan_obat_model extends CI_Model {
    var $column = array('id_sediaan','kode_sediaan', 'nama_sediaan','keterangan');
    var $order = array('id_sediaan' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_sediaan_list(){
        $this->db->from('m_sediaan_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_sediaan_all(){
         $this->db->from('m_sediaan_obat');

        return $this->db->count_all_results();
    }
    
    public function count_sediaan_filtered(){
         $this->db->from('m_sediaan_obat');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_sediaan($data=array()){
        $insert = $this->db->insert('m_sediaan_obat',$data);
        
        return $insert;
    }
    
    public function get_sediaan_by_id($id){
        $query = $this->db->get_where('m_sediaan_obat', array('id_sediaan' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_sediaan($data, $id){
        $update = $this->db->update('m_sediaan_obat', $data, array('id_sediaan' => $id));
        
        return $update;
    }
    
    public function delete_sediaan($id){
        $delete = $this->db->delete('m_sediaan_obat', array('id_sediaan' => $id));
        
        return $delete;
    }
    
    // public function get_kelas_terapi_list(){
    //     return $this->db->get('m_kelasterapi_obat')->result();
    // }
 
}