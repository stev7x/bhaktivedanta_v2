<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tindakan_lab_model extends CI_Model {
    var $column = array('tindakan_lab_id','tindakan_lab_nama','harga_bpjs','harga_non_bpjs','harga_cyto','harga_non_cyto','m_kelompoktindakan_lab.kelompoktindakan_lab_nama');
    var $order = array('tindakan_lab_id' => 'ASC');

    public function __construct() {
        parent::__construct();
    }

    public function  get_tindakan_lab_list_2(){
        $this->db->select("m_tindakan_lab.*, m_kelompoktindakan_lab.kelompoktindakan_lab_nama");
        $this->db->join('m_kelompoktindakan_lab','m_tindakan_lab.kelompoktindakan_lab_id = m_kelompoktindakan_lab.kelompoktindakan_lab_id');
        $this->db->from('m_tindakan_lab');

        $query = $this->db->get();
        return $query->result();
    }

    public function get_tindakan_lab_list(){
        $this->db->select("m_tindakan_lab.*,m_kelompoktindakan_lab.kelompoktindakan_lab_nama");
        $this->db->join('m_kelompoktindakan_lab','m_tindakan_lab.kelompoktindakan_lab_id = m_kelompoktindakan_lab.kelompoktindakan_lab_id');
        $this->db->from('m_tindakan_lab');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_tindakan_lab_all(){
        $this->db->select("m_tindakan_lab.*,m_kelompoktindakan_lab.kelompoktindakan_lab_nama");
        $this->db->join('m_kelompoktindakan_lab','m_tindakan_lab.kelompoktindakan_lab_id = m_kelompoktindakan_lab.kelompoktindakan_lab_id');
        $this->db->from('m_tindakan_lab');

        return $this->db->count_all_results();
    }

    public function count_tindakan_lab_filtered(){
        $this->db->select("m_tindakan_lab.*,m_kelompoktindakan_lab.kelompoktindakan_lab_nama");
        $this->db->join('m_kelompoktindakan_lab','m_tindakan_lab.kelompoktindakan_lab_id = m_kelompoktindakan_lab.kelompoktindakan_lab_id');
        $this->db->from('m_tindakan_lab');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function insert_tindakan_lab($data=array()){
        $insert = $this->db->insert('m_tindakan_lab',$data);

        return $insert;
    }

    public function insert_nilai_rujukan($data=array()){
        $insert = $this->db->insert_batch('m_nilai_rujukan_tindakan_lab',$data);

        return $insert;
    }

    public function get_tindakan_lab_by_id($id){
        $query = $this->db->get_where('m_tindakan_lab', array('tindakan_lab_id' => $id), 1, 0);

        return $query->row();
    }

    public function get_nilai_rujukan_tindakan_lab_by_id($id){
        $this->db->select('tindakan_lab_id, jenis_kelamin, kategori_usia, jenis_cairan, nilai_rujukan_bawah, nilai_rujukan_atas, satuan');
        $this->db->from('m_nilai_rujukan_tindakan_lab');
        $this->db->where('tindakan_lab_id = '.$id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_tindakan_lab_join_nilai_rujukan_by_id($id){
        // fail function
        // $this->db->select('*');
        // $this->db->from('m_tindakan_lab');
        // $this->db->join('m_nilai_rujukan_tindakan_lab', 'm_tindakan_lab.tindakan_lab_id = m_nilai_rujukan_tindakan_lab.tindakan_lab_id', 'right');
        // // $this->db->where('m_tindakan_lab.tindakan_lab_id', $id);
        // $this->db->where('m_tindakan_lab.tindakan_lab_id = '.$id);
        // $query = $this->db->get();
        // // $where = "name='Joe' AND status='boss' OR status='active'";
        // // $query = $this->db->get_where('m_tindakan_lab', array('tindakan_lab_id' => $id), 1, 0);
        //
        // return $query->result();
    }

    public function get_tindakan_lab_id(){
        $query = $this->db->query("SELECT MAX(tindakan_lab_id)+1 as id FROM m_tindakan_lab");

        return $query->row();
    }

    public function update_tindakan_lab($data, $id){
        $update = $this->db->update('m_tindakan_lab', $data, array('tindakan_lab_id' => $id));

        return $update;
    }

    public function delete_tindakan_lab($id){
        $delete = $this->db->delete('m_tindakan_lab', array('tindakan_lab_id' => $id));

        return $delete;
    }

    public function delete_nilai_rujukan_tindakan_lab($id){
        $delete = $this->db->delete('m_nilai_rujukan_tindakan_lab', array('tindakan_lab_id' => $id));

        if($this->db->affected_rows() > 0 && $delete){
          return true;
        }else{
          return false;
        }
    }


    public function get_data_kelompoktindakan(){
        return $this->db->get('m_kelompoktindakan_lab')->result();
    }

}
