<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok_diagnosa_model extends CI_Model{
	var $table = "m_kelompok_diagnosa";
    var $column = array('kelompokdiagnosa_id','kelompokdiagnosa_nama');
    var $order = array('kelompokdiagnosa_nama' => 'ASC');

	public function __construct() {
        parent::__construct();
    }

    public function get_kelompok_diagnosa_list_2(){
        $this->db->from('m_kelompok_diagnosa');
        $this->db->order_by('kelompokdiagnosa_nama','ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_kelompok_diagnosa_list(){
    	$this->db->from($this->table);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();

    }
    
    public function count_kelompok_diagnosa_all(){
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }
    
    public function count_kelompok_diagnosa_filtered(){
        $this->db->from($this->table);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_kelompok_diagnosa($data=array()){
         $insert = $this->db->insert($this->table,$data);
        
        return $insert;
    }
    public function get_kelompok_diagnosa_by_id($id){
        $query = $this->db->get_where($this->table, array('kelompokdiagnosa_id' => $id), 1, 0);
        
        return $query->row();
    }
    public function update_kelompok_diagnosa($data, $id){
        $update = $this->db->update($this->table, $data, array('kelompokdiagnosa_id' => $id));
        
        return $update;
    }
    public function delete_kelompok_diagnosa($id){
        $delete = $this->db->delete($this->table, array('kelompokdiagnosa_id' => $id));
        
        return $delete;
    }
}