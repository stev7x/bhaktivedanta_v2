<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komponen_Tarif_model extends CI_Model {
    var $column = array('komponentarif_id','komponentarif_nama');
    var $order = array('komponentarif_nama' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_komponen_tarif_list(){
        $this->db->from('m_komponentarif');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_komponentarif_all(){
        $this->db->from('m_komponentarif');

        return $this->db->count_all_results();
    }
    
    public function count_komponentarif_filtered(){
        $this->db->from('m_komponentarif');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_komponentarif($data=array()){
        $insert = $this->db->insert('m_komponentarif',$data);
        return $insert;
    }
    
    public function get_komponentarif_by_id($id){
        $query = $this->db->get_where('m_komponentarif', array('komponentarif_id' => $id), 1, 0);
        return $query->row();
    }
    
    public function update_komponen_tarif($data, $id){
        $update = $this->db->update('m_komponentarif', $data, array('komponentarif_id' => $id));
        return $update;
    }
    
    public function delete_komponen_tarif($id){
        $delete = $this->db->delete('m_komponentarif', array('komponentarif_id' => $id));
        return $delete;
    }
    
 
}