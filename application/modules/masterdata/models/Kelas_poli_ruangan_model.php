<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_poli_ruangan_model extends CI_Model {
    var $column = array('nama_poliruangan','kelaspelayanan_nama');
    var $order = array('nama_poliruangan' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_kelas_poli_ruangan_list_2(){
        $this->db->select("m_kelaspoliruangan.*,m_poli_ruangan.nama_poliruangan,kelaspelayanan_nama");
        $this->db->join('m_kelaspelayanan','m_kelaspoliruangan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $this->db->join('m_poli_ruangan','m_kelaspoliruangan.poliruangan_id = m_poli_ruangan.poliruangan_id');
        $this->db->from('m_kelaspoliruangan');
        $this->db->order_by('nama_poliruangan','ASC');
         $query = $this->db->get();
        return $query->result();
    }

    public function get_kelas_poli_ruangan_list(){
        $this->db->select("m_kelaspoliruangan.*,m_poli_ruangan.nama_poliruangan,kelaspelayanan_nama");
        $this->db->join('m_kelaspelayanan','m_kelaspoliruangan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $this->db->join('m_poli_ruangan','m_kelaspoliruangan.poliruangan_id = m_poli_ruangan.poliruangan_id');
        $this->db->from('m_kelaspoliruangan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start')); 
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_kelas_poli_ruangan_all(){
        $this->db->from('m_poli_ruangan');

        return $this->db->count_all_results();
    }
    
    public function count_kelas_poli_ruangan_filtered(){
        $this->db->select("m_kelaspoliruangan.*,m_poli_ruangan.nama_poliruangan,kelaspelayanan_nama");
        $this->db->join('m_kelaspelayanan','m_kelaspoliruangan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id');
        $this->db->join('m_poli_ruangan','m_kelaspoliruangan.poliruangan_id = m_poli_ruangan.poliruangan_id');
        $this->db->from('m_kelaspoliruangan');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_kelas_poli_ruangan($data=array()){
         $jml= $this->db->get_where('m_kelaspoliruangan',$data)->num_rows();
         if ($jml > 0) {
             $insert = false;
         }else{
            $insert = $this->db->insert('m_kelaspoliruangan',$data);   
         }
         
        
        return $insert;
    }
    
    public function get_kelas_poli_ruangan_by_id($idruang,$idkelas){
        $this->db->select("m_kelaspoliruangan.*,m_poli_ruangan.nama_poliruangan,kelaspelayanan_nama");
        $this->db->join('m_kelaspelayanan','m_kelaspoliruangan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id'); 
        $this->db->join('m_poli_ruangan','m_kelaspoliruangan.poliruangan_id = m_poli_ruangan.poliruangan_id');
        $this->db->from('m_kelaspoliruangan');
        $this->db->where('m_kelaspoliruangan.poliruangan_id',$idruang);
        $this->db->where('m_kelaspoliruangan.kelaspelayanan_id',$idkelas);
        
        $query = $this->db->get(); 
  
        // $query = $this->db->get_where('m_kelaspoliruangan', array('poliruangan_id' => $idruang,'kelaspelayanan_id' => $idkelas), 1, 0);
        
        return $query->row();  
    }
    
    public function update_kelas_poli_ruangan($data, $id_poli, $id_kelas){
        // $update = $this->db->update('m_kelaspoliruangan', $data, array('kelaspoliruangan_id' => $id));
        $array = array('poliruangan_id' => $id_poli, 'kelaspelayanan_id' => $id_kelas);

        $this->db->where($array);
        $update = $this->db->update('m_kelaspoliruangan',$data);
        
        return $update;
    }
    
    public function delete_kelas_poli_ruangan($poliruangan_id,$kelaspelayanan_id){
        $delete = $this->db->delete('m_kelaspoliruangan', array('poliruangan_id' => $poliruangan_id,'kelaspelayanan_id' => $kelaspelayanan_id));
        
        return $delete;
    }
    public function get_ruangan_list(){
         return $this->db->get('m_poli_ruangan')->result();
    }
    public function get_pelayanan_list_aktif($ruangan_id){ 
    $this->db->order_by('kelaspelayanan_nama', 'ASC');
        
    $this->db->select("m_kelaspoliruangan.*,m_kelaspelayanan.*");
    $this->db->join('m_kelaspelayanan','m_kelaspoliruangan.kelaspelayanan_id = m_kelaspelayanan.kelaspelayanan_id','left');  
    $this->db->from('m_kelaspoliruangan');         
    $this->db->where('m_kelaspoliruangan.poliruangan_id',$ruangan_id);       
    // $this->db->where('m_kelaspoliruangan.status_aktif','1');                   

    // $query = $this->db->get_where('m_kelaspoliruangan', array('poliruangan_id' => $ruangan_id, 'status_aktif' => '1'));   
    $query = $this->db->get();   

    return $query->result();    
    } 

    public function get_pelayanan_list(){
         return $this->db->get('m_kelaspelayanan')->result();
    
    }
}