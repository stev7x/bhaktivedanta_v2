<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metode_reservasi_model extends CI_Model {
    var $column = array('metode_id','metode_nama','metode_persen','keterangan');
    var $order = array('metode_id' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_metode_reservasi_list_2(){
        $this->db->order_by("metode_id", "ASC");
        $query = $this->db->get("m_mestode");
        return $query->result();

    }

    public function get_metode_reservasi_list(){
        $this->db->from('m_metode');
        $this->db->order_by("metode_id", "ASC");
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_metode_reservasi_all(){
        $this->db->from('m_metode');

        return $this->db->count_all_results();
    }
    
    public function count_metode_reservasi_filtered(){
        $this->db->from('m_metode');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function insert_metode_reservasi($data){
        $insert = $this->db->insert('m_metode',$data);
        
        return $insert;
    }
    
    public function get_metode_reservasi_by_id($id){
        $query = $this->db->get_where('m_metode', array('metode_id' => $id), 1, 0);
        
        return $query->row();
    }
    
    public function update_metode_reservasi($data, $id){
        $update = $this->db->update('m_metode', $data, array('metode_id' => $id));
        
        return $update;
    }
    
    public function delete_metode_reservasi($id){
        $delete = $this->db->delete('m_metode', array('metode_id' => $id));
        
        return $delete;
    }


    function get_export_metode(){
        $this->db->from('m_metode');
        // $this->db->where('tgl_masukadmisi >="'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and tgl_pulang <="'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $query = $this->db->get();
        
        return $query->result();
    }
    
 
}