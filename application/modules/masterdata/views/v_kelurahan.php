<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Kelurahan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Kelurahan</li>
                        </ol>   
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Master Kelurahan  
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                   <div class="panel-body">
                                        <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <button  type="button" class="btn btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="addkelurahan()">Tambah Kelurahan</button>      
                                        </div>  
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />   
                                       <table id="table_kelurahan_list" class="table table-striped dataTable" cellspacing="0">    
                                       <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Kecamatan</th>
                                                <th>Nama Kelurahan</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Kecamatan</th>
                                                <th>Nama Kelurahan</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table> 
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- modal -->   
<div id="modal_add_kelurahan" class="modal fade"> 
    <div class="modal-dialog modal-lg"> 
       <?php echo form_open('#',array('id' => 'fmCreateKelurahan'))?>
       <div class="modal-content" style="margin-top: 150px"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Kelurahan</h2> 
            </div>     
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Pilih Kecamatan<span style="color: red">*</span></label>   
                        <select id="select_kecamatan" name="select_kecamatan" class="form-control">
                            <option value="" disabled selected>Pilih Kecamatan</option>
                            <?php
                            $list_kelurahan = $this->Kelurahan_model->get_kecamatan_list();
                            foreach($list_kelurahan as $list){
                                echo "<option value=".$list->kecamatan_id.">".$list->kecamatan_nama."</option>";
                            }
                            ?>
                        </select>  
                    </div>
                    <div class="form-group col-md-12">
                        <label>Nama Kelurahan<span style="color: red">*</span></label> 
                        <input type="text" id="nama_kelurahan" name="nama_kelurahan" class="form-control"> 
                    </div>  
                </div> 

                <div class="row"> 
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="saveKelurahan"><i class="fa fa-floppy-o p-r-10"></i> SIMPAN</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
 
<!-- modal -->
<div id="modal_update_kelurahan" class="modal fade bs-example-modal-lg modal-add" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog modal-lg">      
       <?php echo form_open('#',array('id' => 'fmUpdateKelurahan'))?>
       <div class="modal-content" style="margin-top: 150px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Perbarui Kelurahan</h2> 
            </div>      
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Pilih Kecamatan<span style="color: red">*</span></label>
                        <select id="upd_select_kecamatan" name="upd_select_kecamatan" class="form-control">
                            <option value="" disabled selected>Pilih Kecamatan</option>
                            <?php
                            $list_kelurahan = $this->Kelurahan_model->get_kecamatan_list();
                            foreach($list_kelurahan as $list){
                                echo "<option value=".$list->kecamatan_id.">".$list->kecamatan_nama."</option>";
                            }
                            ?>
                        </select> 
                    </div>
                    <div class="form-group col-md-12">
                        <label>Nama Kelurahan<span style="color: red">*</span></label>
                        <input type="text" id="upd_nama_kelurahan" name="upd_nama_kelurahan" class="form-control" value=""> 
                    </div>  
                </div> 

                <div class="row"> 
                    <div class="col-md-12">    
                    <input type="hidden" id="upd_id_kelurahan" name="upd_id_kelurahan" class="validate">
                        <button type="button" class="btn btn-success pull-right" id="changeKelurahan"><i class="fa fa-floppy-o p-r-10"></i> PERBARUI</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
 


<?php $this->load->view('footer');?>
      