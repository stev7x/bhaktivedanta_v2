<?php $this->load->view('header');?>
<title>SIMRS | Master Tarif Tindakan</title>
<?php $this->load->view('sidebar');?>
      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Master Tarif Tindakan</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Master</a></li>
                    <li class="active">Tarif Tindakan</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        <!--start container-->
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <p class="right-align"><a class="btn waves-effect waves-light indigo modal-trigger" href="#modal_add_tarif_tindakan">Tambah Tarif Tindakan</a></p>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="divider"></div>
                            <h4 class="header">Master Tarif Tindakan</h4>
                            <div id="card-alert" class="card green notif" style="display:none;">
                                <div class="card-content white-text">
                                    <p id="card_message">SUCCESS : The page has been added.</p>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div id="table-datatables">
                                <div class="row">
                                    <div class="col s12 m4 l12">
                                        <table id="table_tarif_tindakan_list" class="responsive-table display" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kelas Pelayanan</th>
                                                    <th>Tindakan</th>
                                                    <th>Cyto Tindakan</th>
                                                    <th>Tarif Tindakan</th>
                                                    <th>Komponen Tarif</th>
                                                    <th>BPJS/Non BPJS</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kelas Pelayanan</th>
                                                    <th>Tindakan</th>
                                                    <th>Cyto Tindakan</th>
                                                    <th>Tarif Tindakan</th>
                                                    <th>Komponen Tarif</th>
                                                    <th>BPJS/Non BPJS</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
    <!-- END CONTENT -->
    
    <!-- Start Modal Add Tarif Tindakan -->
    <div id="modal_add_tarif_tindakan" class="modal">
        <?php echo form_open('#',array('id' => 'fmCreateTarif_tindakan'))?>
        <div class="modal-content">
            <h1>Tambah Tarif Tindakan</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="select_kelas" name="select_kelas">
                                <option value="" disabled selected>Pilih Kelas</option>
                                <?php
                                $list_kelas = $this->Tarif_tindakan_model->get_kelas_list();
                                foreach($list_kelas as $list){
                                    echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Kelas<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="select_tindakan" name="select_tindakan">
                                <option value="" disabled selected>Pilih Pelayanan</option>
                                <?php
                                $list_tindakan = $this->Tarif_tindakan_model->get_daftar_list();
                                foreach($list_tindakan as $list){
                                    echo "<option value=".$list->daftartindakan_id.">".$list->daftartindakan_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Pelayanan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="select_komponen" name="select_komponen">
                                <option value="" disabled selected>Pilih Komponen Tarif</option>
                                <?php
                                $list_komponen = $this->Tarif_tindakan_model->get_komponen_tarif();
                                foreach($list_komponen as $list){
                                    echo "<option value=".$list->komponentarif_id.">".$list->komponentarif_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Komponen Tarif<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="bpjs_non_bpjs" name="bpjs_non_bpjs">
                                <option value="" disabled selected>Pilih Jenis Tarif</option>
                                <option value="BPJS">BPJS</option>
                                <option value="Non BPJS">Non BPJS</option>
                            </select>
                            <label>Jenis Tarif<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="nama_tarif_tindakan" name="nama_tarif_tindakan" class="validate">
                            <label for="nama_lengkap">Tarif Tindakan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="cyto_tindakan" name="cyto_tindakan" class="validate">
                            <label for="cyto_tindakan">Cyto Tindakan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="saveTarif_tindakan">Save<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- End Modal Add Tarif Tindakan -->
     <!-- Modal Update Tarif Tindakan-->
     <div id="modal_update_tarif_tindakan" class="modal fadeIn" tabindex="-1" role="dialog">
        <?php echo form_open('#',array('id' => 'fmUpdateTarif_tindakan'))?>
        <div class="modal-content">
            <h1>Update Tarif Tindakan</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green upd_modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="upd_modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="upd_select_kelas" name="upd_select_kelas">
                                <option value="" disabled selected>Pilih Kelas</option>
                                <?php
                                $list_kelas = $this->Tarif_tindakan_model->get_kelas_list();
                                foreach($list_kelas as $list){
                                    echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Kelas<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="upd_select_tindakan" name="upd_select_tindakan">
                                <option value="" disabled selected>Pilih Tindakan</option>
                                <?php
                                $list_tindakan = $this->Tarif_tindakan_model->get_daftar_list();
                                foreach($list_tindakan as $list){
                                    echo "<option value=".$list->daftartindakan_id.">".$list->daftartindakan_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Tindakan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="upd_select_komponen" name="upd_select_komponen">
                                <option value="" disabled selected>Pilih Komponen Tarif</option>
                                <?php
                                $list_komponen = $this->Tarif_tindakan_model->get_komponen_tarif();
                                foreach($list_komponen as $list){
                                    echo "<option value=".$list->komponentarif_id.">".$list->komponentarif_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Komponen Tarif<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="upd_bpjs_non_bpjs" name="upd_bpjs_non_bpjs">
                                <option value="" disabled selected>Pilih Jenis Tarif</option>
                                <option value="BPJS">BPJS</option>
                                <option value="Non BPJS">Non BPJS</option>
                            </select>
                            <label>Jenis Tarif<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="upd_nama_tarif_tindakan" name="upd_nama_tarif_tindakan" class="validate" value="">
                            <label for="label_upd_nama_tarif_tindakan" id="label_upd__nama_tarif_tindakan">Tarif Tindakan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="upd_cyto_tindakan" name="upd_cyto_tindakan" class="validate">
                            <label for="label_upd_cyto_tindakan" id="label_upd_cyto_tindakan">Cyto Tindakan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="upd_id_tarif_tindakan" name="upd_id_tarif_tindakan" class="validate">
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="changeTarif_tindakan">Save Change<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php $this->load->view('footer');?>
      