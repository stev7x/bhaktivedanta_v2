<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Metode Reservasi</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Metode Reservasi</li>
                        </ol> 
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1"> 
                            <div class="panel-heading"> Master Metode Reservasi
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                        <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_metode_reservasi" data-backdrop="static" data-keyboard="false" onclick="openmetode_reservasi()"><i class="fa fa-plus"></i> Tambah Metode Reservasi</button>
                                            &nbsp;&nbsp;&nbsp;
                                        </div>
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                        <table id="table_metode_reservasi_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                        <thead> 
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Metode Reservasi</th>
                                                <th>Persentase</th>
                                                <th>Keterangan</th>
                                                <th>Aksi</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
            </div> 
            <!-- /.container-fluid -->


<!-- Modal  --> 
<div id="modal_metode_reservasi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreatemetode_reservasi', 'data-toggle' => 'validator', 'class' => 'form_aksi'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b id="form_judul">Tambah Data Imunisasi</b></h2> 
            </div>
            <div class="modal-body"> 
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden" name="metode_id" id="metode_id">
                        <div class="form-group col-md-12">
                            <label>Nama Metode Reservasi<span style="color: red">*</span></label>
                            <input type="text" name="metode_nama" id="metode_nama" class="form-control" placeholder="Nama Metode Reservasi">
                        </div>    
                        <div class="form-group col-md-12">
                           
                            <label>Persentase<span style="color: red">*</span></label>
                           <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" name="metode_persen" id="metode_persen" class="form-control no" maxlength="2" placeholder="Persentase Metode Reservasi" style="float: right;">                          </div> 
                        <div class="form-group col-md-12">
                            <label>Keterangan</label>
                            <input type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan">
                        </div>    
                        <div class="col-md-12"> 
                            <button type="button" class="btn btn-success pull-right aksi_button" ><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                        </div>                            
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->


<?php $this->load->view('footer');?>