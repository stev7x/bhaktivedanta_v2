<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Jadwal Dokter</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">  
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Jadwal Dokter</li>
                        </ol> 
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1"> 
                            <div class="panel-heading"> Master Data Jadwal Dokter  
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <div class="col-md-6">
                                                    <button  type="button" class="btn  btn-info" onclick="tambahJadwal()" >TAMBAH JADWAL DOKTER</button>  
                                                </div>    
                                                
 
                                            </div> 
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />   
                                           <table id="table_jadwaldokter_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                            <thead>    
                                                <tr>    
                                                    <th>No</th>
                                                    <th>Nama Dokter</th>
                                                    <th>Jumlah Pasien</th>
                                                    <th>Hari</th>
                                                    <th>Jam Mulai</th>
                                                    <th>Jam Selesai</th>  
                                                    <th>Nama Poliruangan</th>
                                                    <th>Nama Branch</th>
                                                    <th>Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                
                                            </tbody>
                                            <tfoot>  
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Dokter</th>
                                                    <th>Jumlah Pasien</th>
                                                    <th>Hari</th>
                                                    <th>Jam Mulai</th>
                                                    <th>Jam Selesai</th>  
                                                    <th>Nama Poliruangan</th>
                                                    <th>Nama Branch</th>
                                                    <th>Action</th>

                                                </tr>
                                            </tfoot>
                                        </table>    
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div> 
            <!-- /.container-fluid --> 
 
   
<!-- Modal  --> 
<div id="modal_jadwal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog modal-lg"> 
    <?php echo form_open('#',array('id' => 'fmJadwalDokter'))?>           
       <div class="modal-content" style="margin-top: 104px">    
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Jadwal Dokter</h2> 
            </div> 
            <div class="modal-body"> 
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />    
                        <div class="col-md-7" style="border-right: 1px solid #d0d1d2">
                            <div class="form-group col-md-12"> 
                                <label>Nama Dokter<span style="color: red">*</span></label>    
                                <select name="dokter_id" id="dokter_id" class="form-control select2">     
                                    <option>PILIH DOKTER</option>
                                <?php  
                                    $data = $this->Jadwal_dokter_model->get_dokter();
                                    foreach ($data as $d) { 

                                        echo '<option value="'.$d->id_M_DOKTER.'">'.$d->NAME_DOKTER.'</option>';
                                    }
                                ?>   
                                </select>
                            </div>   
                            <div class="form-group col-md-12"> 
                                <label>Hari<span style="color: red">*</span></label> 
                                <select name="detail_jadwal_id" id="detail_jadwal_id" class="form-control">
                                    <option disabled selected>PILIH HARI</option>
                                    <?php 
                                        $list_hari = $this->Jadwal_dokter_model->getHari();
                                        foreach ($list_hari as $list) {
                                            # code...
                                            echo "<option value=".$list->detail_jadwal_id.">".$list->hari."</option>";
                                        }
                                    ?>
                                </select>           
                            </div>
                            <div class="form-group col-md-12"> 
                                <label>Kuota Pasien</label>
                                <input type="number" name="kuota_pasien" id="kuota_pasien" placeholder="Kuota Pasien" class="form-control" />  
                            </div>
                            <div class="form-group col-md-12"> 
                                <label>Branch</label>
                                <select class="form-control" name="id_branch" placeholder="Branch">
                                    
                                </select> 
                            </div> 
                        </div>     
                        <div class="col-md-5"> 
                            <div class="col-md-6" style="margin-top:-32px ">
                                <label class="m-t-40">Jam Mulai</label>
                                <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                    <input type="text" name="jam_mulai" id="jam_mulai" class="form-control" value="13:14"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                </div>
                            </div> 
                            <div class="col-md-6" style="margin-top:-32px "> 
                                <label class="m-t-40">Jam Selesai</label>
                                <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                    <input type="text" name="jam_selesai" id="jam_selesai"  class="form-control"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                </div> 
                            </div>

                        </div>  
                </div>
                <div class="row">  
                    <div class="col-md-12">     
                        <input type="hidden" name="Jadwal_dokter_id" id="Jadwal_dokter_id"> 
                        <button type="button" class="btn btn-success pull-right" id="btnJadwalDokter" onclick="saveJadwal()"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>  
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

 <!-- Modal  --> 
<div id="modal_edit_agama" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">   
    <?php echo form_open('#',array('id' => 'fmUpdateAgama'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Perbarui Agama</h2> 
            </div>
            <div class="modal-body" >
                <div class="alert alert-success alert-dismissable hide" id="upd_modal_notif">     
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> Data Berhasil di Update. 
                </div> 

                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <div class="form-group col-md-12">
                            <label>Nama Agama<span style="color: red">*</span></label>
                            <input type="text" name="upd_nama_agama" id="upd_nama_agama" class="form-control" placeholder="Nama Pendidikan">
                            <input type="hidden" id="upd_id_agama" name="upd_id_agama" class="validate">  
                        </div>         
                </div>
                <div class="row">
                    <div class="col-md-12">     
                        <button type="button" class="btn btn-success pull-right" id="changeAgama"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button> 
                    </div>
                </div>  
            </div>
            
        </div>
        <!-- /.modal-content -->
        <?php echo form_close(); ?> 
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

<?php $this->load->view('footer');?>
      