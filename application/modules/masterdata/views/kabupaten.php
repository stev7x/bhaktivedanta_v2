<?php $this->load->view('header');?>
<title>SIMRS | Master Kabupaten</title>
<?php $this->load->view('sidebar');?>
      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Master Kabupaten</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Master</a></li>
                    <li class="active">Kabupaten</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        <!--start container-->
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <p class="right-align"><a class="btn waves-effect waves-light indigo modal-trigger" href="#modal_add_kabupaten">Tambah Kabupaten</a></p>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="divider"></div>
                            <h4 class="header">Master Kabupaten</h4>
                            <div id="card-alert" class="card green notif" style="display:none;">
                                <div class="card-content white-text">
                                    <p id="card_message">SUCCESS : The page has been added.</p>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div id="table-datatables">
                                <div class="row">
                                    <div class="col s12 m4 l12">
                                        <table id="table_kabupaten_list" class="responsive-table display" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Provinsi</th>
                                                    <th>Nama Kabupaten</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Provinsi</th>
                                                    <th>Nama Kabupaten</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
    <!-- END CONTENT -->
    
    <!-- Start Modal Add Kabupaten -->
    <div id="modal_add_kabupaten" class="modal">
        <?php echo form_open('#',array('id' => 'fmCreateKabupatenOperasi'))?>
        <div class="modal-content">
            <h1>Tambah Kabupaten</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="select_provinsi" name="select_provinsi">
                                <option value="" disabled selected>Pilih Provinsi</option>
                                <?php
                                $list_kabupaten = $this->Kabupaten_model->get_provinsi_list();
                                foreach($list_kabupaten as $list){
                                    echo "<option value=".$list->propinsi_id.">".$list->propinsi_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Provinsi<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="nama_kabupaten" name="nama_kabupaten" class="validate">
                            <label for="nama_lengkap">Nama Kabupaten<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="saveKabupatenOperasi">Save<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- End Modal Add Kabupaten -->
     <!-- Modal Update Kabupaten-->
     <div id="modal_update_kabupaten" class="modal fadeIn" tabindex="-1" role="dialog">
        <?php echo form_open('#',array('id' => 'fmUpdateKabupatenOperasi'))?>
        <div class="modal-content">
            <h1>Update Kabupaten</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green upd_modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="upd_modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="upd_select_privinsi" name="upd_select_privinsi">
                                <option value="" disabled selected>Pilih Provinsi</option>
                                <?php
                                $list_kabupaten = $this->Kabupaten_model->get_provinsi_list();
                                foreach($list_kabupaten as $list){
                                    echo "<option value=".$list->propinsi_id.">".$list->propinsi_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Provinsi<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="upd_nama_kabupaten" name="upd_nama_kabupaten" class="validate" value="">
                            <label for="upd_nama_kabupaten" id="label_upd__nama_kabupaten">Nama Kabupaten<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="upd_id_kabupaten" name="upd_id_kabupaten" class="validate">
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="changeKabupatenOperasi">Save Change<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php $this->load->view('footer');?>
      