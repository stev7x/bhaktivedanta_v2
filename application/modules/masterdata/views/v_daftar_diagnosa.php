<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Daftar Diagnosa</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Daftar Diagnosa</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div> 
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">     
                            <div class="panel-heading"> Master Daftar Diagnosa   
                            </div>     
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                        <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <div class="col-md-6">
                                                <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_daftar_diagnosa" data-backdrop="static" data-keyboard="false">Tambah Daftar Diagnosa</button>
                                                 &nbsp;&nbsp;&nbsp;
                                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Daftar Diagnosa</button>
                                            </div>
                                            <div class="col-md-6">   
                                                <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>

                                                    &nbsp;&nbsp;&nbsp;  
                                                   <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" onchange="showbtnimport()" />  
                                                    <label for="file" style="float: right;"><span></span> <strong>     
                                                       <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                   <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                         
                                                <?php echo form_close(); ?>  
                                                </div>
                                        </div>    
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />    
                                    <div class="table-responsive"> 
                                       <table id="table_daftar_diagnosa_list" class="table table-striped" cellspacing="0">  
                                            <thead>  
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kode Diagnosa</th>
                                                    <th>Nama Diagnosa</th>
                                                    <th>Action</th>
                                                </tr> 
                                            </thead>
                                            <tbody>
 
                                            </tbody>
                                            <tfoot>  
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kode Diagnosa</th>
                                                    <th>Nama Diagnosa</th>
                                                    <th>Action</th>
                                                </tr>   
                                            </tfoot>
                                        </table>  
                                    </div> 
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>

<!-- Modal  -->
<div id="modal_add_daftar_diagnosa" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg"> 
    <?php echo form_open('#',array('id' => 'fmCreateDiagnosa'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Daftar Daignosa</h2> 
            </div>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-12">
                            <label>Kode Diagnosa<span style="color: red">*</span></label>
                            <input type="text" name="kode_diagnosa" id="kode_diagnosa" class="form-control" placeholder="Kode Diagnosa">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Nama Diagnosa<span style="color: red">*</span></label>
                            <input type="text" name="nama_diagnosa" id="nama_diagnosa" class="form-control" placeholder="Nama Tindakan">
                        </div>      
                </div><br><br>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveDaftarDiagnosa"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div> 
                </div> 
            </div>
             
        </div>
        <?php echo form_close(); ?> 
    </div>
</div>
 
<div id="modal_edit_daftar_diagnosa" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg"> 
    <?php echo form_open('#',array('id' => 'fmUpdateDiagnosa'))?>
       <div class="modal-content">  
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Edit Daftar Diagnosa</h2> 
            </div>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p> 
                </div>
            </div> 
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                <input type="hidden" id="diagnosa_id" name="diagnosa_id">      
                        <div class="form-group col-md-12">
                            <label>Kode Diagnosa<span style="color: red">*</span></label>
                            <input type="text" name="upd_kode_diagnosa" id="upd_kode_diagnosa" class="form-control" placeholder="Kode Diagnosa">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Nama Diagnosa<span style="color: red">*</span></label>
                            <input type="text" name="upd_nama_diagnosa" id="upd_nama_diagnosa" class="form-control" placeholder="Nama Tindakan"> 
                        </div>       
                </div><br><br>     
                <div class="row">  
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="changeDaftarDiagnosa"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                    </div> 
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?> 
    </div>
</div>


 
 

<?php $this->load->view('footer');?>
      