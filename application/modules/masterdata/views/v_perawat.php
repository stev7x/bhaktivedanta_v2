<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Master Perawat</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Master</a></li>
                    <li class="active">Perawat</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Master Perawat
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">

                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                <div class="col-md-6">
                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add" data-backdrop="static" data-keyboard="false">Tambah Perawat</button>
                                    &nbsp;&nbsp;&nbsp;
<!--                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Perawat</button>-->
                                </div>
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <table id="table_perawat_list" class="table table-striped" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nomor</th>
                                    <th>Nama</th>
                                    <th>Jenis kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Tempat Lahir</th>
<!--                                    <th>No Telepon</th>-->
<!--                                    <th>Email</th>-->
                                    <th>Driver</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nomor</th>
                                    <th>Nama</th>
                                    <th>Jenis kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Tempat Lahir</th>
<!--                                    <th>No Telepon</th>-->
<!--                                    <th>Email</th>-->
                                    <th>Driver</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->

    </div>
    <!-- /.container-fluid -->

    <!-- Modal  -->
    <div id="modal_add" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmCreatePerawat'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Tambah Perawat</h2>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message" class=""></p>
                        </div>
                    </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-12">
                            <label>Nomor Perawat <span style="color: red">*</span></label>
                            <input type="text" name="no_perawat" id="no_perawat" class="form-control" placeholder="No Perawat">
                        </div>

                        <div class="form-group col-md-12">
                            <label>Nama Perawat <span style="color: red">*</span></label>
                            <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Perawat">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="jenis_kelamin">Jenis Kelamin <span style="color: red">*</span></label>
                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                <option value="1">Laki-laki</option>
                                <option value="0">Perempuan</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Tempat Lahir <span style="color: red">*</span></label>
                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="tanggal_lahir" class="control-label">Tanggal lahir <span style="color: red;" id="span_tgl" >*</span></label>
                            <div class="input-group">
                                <input id="tanggal_lahir" name="tanggal_lahir" type="text" class="form-control datepick" placeholder="mm/dd/yyyy" value=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="driver">Driver <span style="color: red">*</span></label>
                            <select name="driver" id="driver" class="form-control">
                                <option value="" disabled selected>Pilih Driver</option>
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="savePerawat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal  -->
    <div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmUpdatePerawat'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Perbarui Perawat</h2>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-12">
                            <label>Nomor Perawat <span style="color: red">*</span></label>
                            <input type="text" name="no_perawat" id="edit_no_perawat" class="form-control" placeholder="No Perawat">
                            <input type="hidden" name="id" id="edit_id">
                        </div>

                        <div class="form-group col-md-12">
                            <label>Nama Perawat <span style="color: red">*</span></label>
                            <input type="text" name="nama" id="edit_nama" class="form-control" placeholder="Nama Perawat">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="jenis_kelamin">Jenis Kelamin <span style="color: red">*</span></label>
                            <select name="jenis_kelamin" id="edit_jenis_kelamin" class="form-control">
                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                <option value="1">Laki-laki</option>
                                <option value="0">Perempuan</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Tempat Lahir <span style="color: red">*</span></label>
                            <input type="text" name="tempat_lahir" id="edit_tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="tanggal_lahir" class="control-label">Tanggal lahir <span style="color: red;" id="span_tgl" >*</span></label>
                            <div class="input-group">
                                <input id="edit_tanggal_lahir" name="tanggal_lahir" type="text" class="form-control datepick" placeholder="mm/dd/yyyy" value=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="driver">Driver <span style="color: red">*</span></label>
                            <select name="driver" id="edit_driver" class="form-control">
                                <option value="" disabled selected>Pilih Driver</option>
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="updatePerawat"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <?php $this->load->view('footer');?>
      