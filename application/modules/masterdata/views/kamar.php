<?php $this->load->view('header');?>
<title>SIMRS | Master Kamar Ruangan</title>
<?php $this->load->view('sidebar');?>
      <!-- START CONTENT -->
      <section id="content">

        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Master Kamar Ruangan</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Master</a></li>
                    <li class="active">Kamar Ruangan</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->


        <!--start container-->
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <p class="right-align"><a class="btn waves-effect waves-light indigo modal-trigger" href="#modal_add_kamar">Tambah Kamar Ruangan</a></p>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="divider"></div>
                            <h4 class="header">Master Kamar Ruangan</h4>
                            <div id="card-alert" class="card green notif" style="display:none;">
                                <div class="card-content white-text">
                                    <p id="card_message">SUCCESS : The page has been added.</p>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div id="table-datatables">
                                <div class="row">
                                    <div class="col s12 m4 l12">
                                        <table id="table_kamar_list" class="responsive-table display" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kelas Pelayanan</th>
                                                    <th>Ruangan</th>
                                                    <th>No Kamar</th>
                                                    <th>No Bed</th>
                                                    <th>Status Terpakai</th>
                                                    <th>Status Aktif</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                  <th>No</th>
                                                  <th>Kelas Pelayanan</th>
                                                  <th>Ruangan</th>
                                                  <th>No Kamar</th>
                                                  <th>No Bed</th>
                                                  <th>Status Terpakai</th>
                                                  <th>Status Aktif</th>
                                                  <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
    <!-- END CONTENT -->

    <!-- Start Modal Add Kamar -->
    <div id="modal_add_kamar" class="modal">
        <?php echo form_open('#',array('id' => 'fmCreatekamar'))?>
        <div class="modal-content">
            <h1>Tambah Kamar Ruangan</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="col s4" style="padding:13px">
                          Ruangan<span style="color: red;"> *</span>
                        </div>
                        <div class="col s8">
                            <select id="ruangan" name="ruangan" class="validate browser-default" onchange="getPelayanan()">
                                <option value="" disabled selected>Pilih Ruangan</option>
                                <?php
                                $list_instalasi = $this->Kamar_model->get_ruangan();
                                foreach($list_instalasi as $list){
                                    echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Kelas Pelayanan<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                          <select id="kelas_pelayanan" name="kelas_pelayanan" class="validate browser-default">
                              <option value="" disabled selected>Kelas Pelayanan</option>
                          </select>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Nomor Kamar<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                            <input type="text" id="nomor_kamar" name="nomor_kamar" class="validate">
                        </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Nomor Bed<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                            <input type="text" id="nomor_bed" name="nomor_bed" class="validate">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Status Bed<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                        <input type="checkbox" class="filled-in" id="filled-in-box" checked="checked" id="bed" name="bed">
                        <label for="filled-in-box"Terpakai</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Status Aktif<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                        <div class="switch">
                          <label>
                            Non Active
                            <input type="checkbox" id="aktif" name="aktif">
                            <span class="lever"></span> Active
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Keterangan<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                            <textarea id="keterangan" name="keterangan" class="validate"></textarea>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="savekamar">Save<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- End Modal Add Kamar -->
     <!-- Modal Update Kamar-->
    <!-- Modal Update Produk-->
     <div id="modal_update_kamar" class="modal fadeIn" tabindex="-1" role="dialog">
        <?php echo form_open('#',array('id' => 'fmUpdateKamar'))?>
        <div class="modal-content">
            <h1>Update Kamar</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green upd_modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="upd_modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="col s4" style="padding:13px">
                          Ruangan<span style="color: red;"> *</span>
                        </div>
                        <div class="col s8">
                            <select id="upd_ruangan" name="upd_ruangan" class="validate browser-default" onchange="getPelayananid()">
                                <option value="" disabled selected>Pilih Ruangan</option>
                                <?php
                                $list_instalasi = $this->Kamar_model->get_ruangan();
                                foreach($list_instalasi as $list){
                                    echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Kelas Pelayanan<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                          <select id="upd_kelas_pelayanan" name="upd_kelas_pelayanan" class="validate browser-default">
                              <option value="" disabled selected>Kelas Pelayanan</option>
                          </select>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Nomor Kamar<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                            <input type="text" id="upd_nomor_kamar" name="upd_nomor_kamar" class="validate">
                        </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Nomor Bed<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                            <input type="text" id="upd_nomor_bed" name="upd_nomor_bed" class="validate">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Status Bed<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                        <div class="switch">
                          <label>
                            Tidak Terpakai
                            <input type="checkbox" id="upd_bed" name="upd_bed">
                            <span class="lever"></span> Terpakai
                          </label>
                        </div>
                        <!-- <input type="checkbox"  id="upd_bed" name="upd_bed">a -->
                        <!-- <label for="filled-in-box"Terpakai</label> -->
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Status Aktif<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                        <div class="switch">
                          <label>
                            Non Active
                            <input type="checkbox" id="upd_aktif" name="upd_aktif">
                            <span class="lever"></span> Active
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s4" style="padding:13px">
                        Keterangan<span style="color: red;"> *</span>
                      </div>
                      <div class="col s8">
                            <textarea id="upd_keterangan" name="upd_keterangan" class="validate"></textarea>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="upd_id_kamar" name="upd_id_kamar" class="validate">
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="changeKamar">Save Change<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php $this->load->view('footer');?>
