<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master lab</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">lab</li>
                        </ol> 
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1"> 
                            <div class="panel-heading"> Master lab
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                        <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_lab" data-backdrop="static" data-keyboard="false" onclick="openLab()"><i class="fa fa-plus"></i> Tambah lab</button>
                                            &nbsp;&nbsp;&nbsp;
                                            <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export lab</button>
                                        </div>
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                        <table id="table_lab_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                        <thead> 
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Pemeriksaan</th>
                                                <th>Jenis</th>
                                                <th>Range Perempuan Dewasa</th>
                                                <th>Range Perempuan Anak</th>
                                                <th>Range Laki-laki Dewasa</th>
                                                <th>Range Laki-laki Anak</th>
                                                <th>Metode</th>
                                                <th>Tarif</th>
                                                <th>Ket</th>
                                                <th>Aksi</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>    
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
            </div> 
            <!-- /.container-fluid -->


<!-- Modal  --> 
<div id="modal_lab" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateLab', 'data-toggle' => 'validator', 'class' => 'form_aksi'))?>
       <div class="modal-content" style="height:600px;overflow:auto;"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b id="form_judul">Tambah Data lab</b></h2> 
            </div>
            <div class="modal-body"> 
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden" name="lab_id" id="lab_id">
                        <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif_lab" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                            <div>
                                <p id="card_message_lab" class=""></p>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Nama Pemeriksaan<span style="color: red">*</span></label>
                            <input type="text" name="nama_pemeriksaan" id="nama_pemeriksaan" class="form-control" placeholder="Nama Pemeriksaan">
                        </div>    
                       
                        <div class="form-group col-md-12">
                            <label>Jenis<span style="color: red">*</span></label>
                            <input type="text" name="jenis" id="jenis" class="form-control" placeholder="Jenis Pemeriksaan">
                        </div>    
                        <div class="form-group col-md-12">
                            <label>Metode<span style="color: red">*</span></label>
                            <input type="text" name="metode_pemeriksaan" id="metode_pemeriksaan" class="form-control" placeholder="Metode Pemeriksaan">
                        </div>    
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Range Terendah Perempuan<span style="color: red">*</span></label>
                                    <input type="number" name="terendah_per" id="terendah_per" class="form-control" placeholder="Range Terendah Perempuan">
                                </div>    
                                <div class="form-group col-md-12">
                                    <label>Range Tertinggi Perempuan<span style="color: red">*</span></label>
                                    <input type="number" name="tertinggi_per" id="tertinggi_per" class="form-control" placeholder="Range Tertinggi Perempuan">
                                </div>    
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Range Terendah Perempuan Anak<span style="color: red">*</span></label>
                                    <input type="number" name="terendah_per_anak" id="terendah_per_anak" class="form-control" placeholder="Range Terendah Perempuan Anak">
                                </div>    
                                <div class="form-group col-md-12">
                                    <label>Range Tertinggi Perempuan Anak<span style="color: red">*</span></label>
                                    <input type="number" name="tertinggi_per_anak" id="tertinggi_per_anak" class="form-control" placeholder="Range Tertinggi Perempuan Anak">
                                </div>    
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Range Terendah Laki-laki<span style="color: red">*</span></label>
                                    <input type="number" name="terendah_laki" id="terendah_laki" class="form-control" placeholder="Range Terendah Laki-laki">
                                </div>    
                                <div class="form-group col-md-12">
                                    <label>Range Tertinggi Laki-laki<span style="color: red">*</span></label>
                                    <input type="number" name="tertinggi_laki" id="tertinggi_laki" class="form-control" placeholder="Range Tertinggi Laki-laki">
                                </div>    
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Range Terendah Laki-laki Anak<span style="color: red">*</span></label>
                                    <input type="number" name="terendah_laki_anak" id="terendah_laki_anak" class="form-control" placeholder="Range Terendah Laki-laki Anak">
                                </div>    
                                <div class="form-group col-md-12">
                                    <label>Range Tertinggi Laki-laki Anak<span style="color: red">*</span></label>
                                    <input type="number" name="tertinggi_laki_anak" id="tertinggi_laki_anak" class="form-control" placeholder="Range Tertinggi Laki-laki Anak">
                                </div>    
                            </div>
                        </div>
                         <div class="form-group col-md-12">
                            <label>Satuan<span style="color: red">*</span></label>
                            <input type="text" name="satuan" id="satuan" class="form-control" placeholder="Satuan">
                        </div>    
                        <div class="form-group col-md-12">
                            <label>Tarif<span style="color: red">*</span></label>
                            <input type="text" name="tarif" id="tarif" onkeyup="formatAsRupiah(this)" class="form-control" placeholder="Tarif">
                        </div>    
                        <div class="form-group col-md-12">
                            <label>Keterangan<span style="color: red">*</span></label>
                            <textarea class="form-control" name="ket" id="ket" cols="" rows="" placeholder="keterangan"></textarea>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right aksi_button"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->


<?php $this->load->view('footer');?>
      