<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
     <!-- Page Content -->
       <div id="page-wrapper">
           <div class="container-fluid">
               <div class="row bg-title">
                   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                       <h4 class="page-title">Master Supplier</h4> </div>
                   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                       <ol class="breadcrumb">
                           <li><a href="index.html">Master</a></li>
                           <li class="active">Supplier</li>
                       </ol> 
                   </div>
                   <!-- /.col-lg-12 -->
               </div>
               <!--row -->
               <div class="row">
                   <div class="col-sm-12">
                       <div class="panel panel-info1"> 
                           <div class="panel-heading"> Master Supplier
                           </div>
                           <div class="panel-wrapper collapse in" aria-expanded="true">
                               <div class="panel-body">

                                           <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_supplier" data-backdrop="static" data-keyboard="false">Tambah Supplier</button>
                                           </div>
                                           <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                          <table id="table_supplier_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                           <thead> 
                                               <tr>
                                                   <th>No</th>
                                                   <th>kode Supplier</th>
                                                   <th>Nama Supplier</th>
                                                   <th>Alamat</th>
                                                   <th>Marketing</th>
                                                   <th>Distributor</th>
                                                   <th>Jenis Supplier</th>
                                                   <th>Aksi</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               
                                           </tbody>
                                           <tfoot>
                                               <tr>
                                                    <th>No</th>
                                                    <th>kode Supplier</th>
                                                    <th>Nama Supplier</th>
                                                    <th>Alamat</th>
                                                    <th>Marketing</th>
                                                    <th>Distributor</th>
                                                    <th>Jenis Supplier</th>
                                                    <th>Aksi</th>
                                               </tr>
                                           </tfoot>
                                       </table>    
                               </div>
                           </div>  
                       </div>
                   </div>
               </div>
               <!--/row -->
               
           </div> 
           <!-- /.container-fluid -->


<!-- Modal  --> 
<div id="modal_add_supplier" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmCreateSupplier'))?>
      <div class="modal-content" style="overflow:auto; height:600px;"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Tambah Supplier</h2> 
           </div>
           <div class="modal-body">
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input type="hidden" name="id_supplier">
                       <div class="form-group col-md-12">
                           <label>Kode Supplier<span style="color: red">*</span></label>
                           <input type="text" name="kode_supplier" id="kode_supplier" class="form-control" placeholder="Kode Supplier">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Nama Supplier<span style="color: red">*</span></label>
                           <input type="text" name="nama_supplier" id="nama_supplier" class="form-control" placeholder="Nama Supplier">
                       </div>  
                       <div class="form-group col-md-12">
                            <label for="keterangan">Alamat</label>
                            <textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat"></textarea>
                       </div>
                       <div class="form-group col-md-12">
                           <label>Marketing<span style="color: red">*</span></label>
                           <input type="text" name="marketing" id="marketing" class="form-control" placeholder="Marketing">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Distributor<span style="color: red">*</span></label>
                           <input type="text" name="distributor" id="distributor" class="form-control" placeholder="Distributor">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Jenis Supplier<span style="color: red">*</span></label>
                           <input type="text" name="jenis_supplier" id="jenis_supplier" class="form-control" placeholder="Jenis Supplier">
                       </div>  
                      
                       
                       
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="saveSupplier"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_edit_supplier" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmUpdateSupplier'))?>
      <div class="modal-content" style="overflow:auto; height:600px;"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Supplier</h2> 
           </div>
           <div class="modal-body">
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input type="hidden" name="upd_id_supplier" id="upd_id_supplier">
                       <div class="form-group col-md-12">
                           <label>Kode Supplier<span style="color: red">*</span></label>
                           <input type="text" name="upd_kode_supplier" id="upd_kode_supplier" class="form-control" placeholder="Kode Supplier">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Nama Supplier<span style="color: red">*</span></label>
                           <input type="text" name="upd_nama_supplier" id="upd_nama_supplier" class="form-control" placeholder="Nama Supplier">
                       </div>  
                       <div class="form-group col-md-12">
                            <label for="keterangan">Alamat<span style="color: red">*</span></label>
                            <textarea name="upd_alamat" id="upd_alamat" class="form-control" placeholder="Alamat"></textarea>
                       </div>
                       <div class="form-group col-md-12">
                           <label>Marketing<span style="color: red">*</span></label>
                           <input type="text" name="upd_marketing" id="upd_marketing" class="form-control" placeholder="Marketing">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Distributor<span style="color: red">*</span></label>
                           <input type="text" name="upd_distributor" id="upd_distributor" class="form-control" placeholder="Distributor">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Jenis Supplier<span style="color: red">*</span></label>
                           <input type="text" name="upd_jenis_supplier" id="upd_jenis_supplier" class="form-control" placeholder="Jenis Supplier">
                       </div>  
                      
                      
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="changeSupplier"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>








<?php $this->load->view('footer');?>
     