<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Kelas Poli Ruangan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Kelas Kelas Poli Ruangan</li>
                        </ol>   
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Master Kelas Poli Ruangan  
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                   <div class="panel-body"> 
                                        <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <div class="col-md-6">   
                                                <div class="col-md-6">
                                                    <button  type="button" class="btn btn-info" data-toggle="modal" onclick="add_kelas_poli_ruangan()" data-backdrop="static" data-keyboard="false">Tambah Kelas Poli Ruangan</button>
                                                </div>
                                                <div class="col-md-6">       
                                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Kelas Poli Ruangan</button>
                                                </div>
                                            </div>  
                                            <!-- <div class="col-md-6"> 
                                                        <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>
                                                            &nbsp;&nbsp;&nbsp;  
                                                            <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />  
                                                            <label for="file" style="float: right;"><span></span> <strong>     
                                                            <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                            <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                                
                                                        <?php echo form_close(); ?> 
                                                </div> -->
                                        </div>   
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />   
                                       <table id="table_kelas_poli_ruangan_list" class="table table-striped dataTable" cellspacing="0">     
                                       <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Poli Ruangan</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Poli Ruangan</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- modal -->    
<div id="modal_add_kelas_poli_ruangan" class="modal fade"> 
    <div class="modal-dialog modal-lg"> 
       <?php echo form_open('#',array('id' => 'fmCreateKelas_poli_ruangan'))?> 
       <div class="modal-content" style="margin-top: 150px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Kelas Poli Ruangan</h2> 
            </div>     
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Ruangan<span style="color: red">*</span></label> 
                        <select id="select_poliruangan" name="select_poliruangan" class="form-control" >    
                            <option value="" disabled selected>Pilih Ruangan</option>
                            <?php
                            $list_kelas = $this->Kelas_poli_ruangan_model->get_ruangan_list();
                            foreach($list_kelas as $list){
                                echo "<option value=".$list->poliruangan_id.">".$list->nama_poliruangan."</option>";
                            }
                            ?>
                        </select>
                    </div> 
                    <div class="form-group col-md-12">
                        <label>Kelas Pelayanan<span style="color: red">*</span></label> 
                        <select id="select_kelaspelayanan" name="select_kelaspelayanan" class="form-control">
                            <option value="" disabled selected>Pilih Kelas Pelayanan</option> 
                            <?php
                            $list_tindakan = $this->Kelas_poli_ruangan_model->get_pelayanan_list();
                            foreach($list_tindakan as $list){
                                echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                            } 
                            ?>
                        </select>  
                    </div>   
                </div> 

                <div class="row"> 
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="saveKelas_poli_ruangan"><i class="fa fa-floppy-o p-r-10"></i> SIMPAN</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

 <!-- modal -->    
<div id="modal_update_kelas_poli_ruangan" class="modal fade"> 
    <div class="modal-dialog modal-lg">   
       <?php echo form_open('#',array('id' => 'fmUpdateKelas_poli_ruangan'))?> 
       <div class="modal-content" style="margin-top: 150px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Edit Kelas Poli Ruangan</h2> 
            </div>     
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Ruangan<span style="color: red">*</span></label> 
                        <select id="upd_select_poliruangan" name="upd_select_poliruangan" class="form-control">
                            <option value="" disabled selected>Pilih Ruangan</option>
                            <?php
                            $list_kelas = $this->Kelas_poli_ruangan_model->get_ruangan_list();
                            foreach($list_kelas as $list){
                                echo "<option value=".$list->poliruangan_id.">".$list->nama_poliruangan."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Kelas Pelayanan<span style="color: red">*</span></label> 
                        <select id="upd_select_kelaspelayanan" name="upd_select_kelaspelayanan" class="form-control">
                            <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                            <?php
                            $list_tindakan = $this->Kelas_poli_ruangan_model->get_pelayanan_list();
                            foreach($list_tindakan as $list){ 
                                echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                            }
                            ?>
                        </select> 
                    </div>   
                </div> 
                <input type="hidden" id="temp_id_poliruangan" name="temp_id_poliruangan" class="validate">
                <input type="hidden" id="temp_id_polikelas" name="temp_id_polikelas" class="validate">
                <div class="row"> 
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="editKelas_poli_ruangan"><i class="fa fa-floppy-o p-r-10"></i> SIMPAN</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
  
 


<?php $this->load->view('footer');?>
      