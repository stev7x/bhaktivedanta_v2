    <?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Obat</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Obat</li>
                        </ol> 
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1"> 
                            <div class="panel-heading"> Master Obat
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                               <div class="panel-body">
                                    <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_obat" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> TAMBAH OBAT</button>
                                            &nbsp;&nbsp;&nbsp;
                                            <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Obat</button>
                                        </div>
                                        <div class="col-md-6"> 
                                                <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>
                                                    &nbsp;&nbsp;&nbsp;  
                                                    <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />  
                                                    <label for="file" style="float: right;"><span></span> <strong>     
                                                    <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                    <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                        
                                                <?php echo form_close(); ?> 
                                        </div>                                                
                                    </div>  
                                            <table id="table_obat_list" class="table table-striped dataTable" cellspacing="0" width="100%"> 
                                                <thead> 
                                                <tr>
                                                    <th>No</th>
                                                    <th>Jenis</th>
                                                    <th>Nama Obat</th>
                                                    <th>Satuan</th>
                                                    <th>Harga Netto</th>
                                                    <th>Harga Jual</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Jenis</th>
                                                    <th>Nama Obat</th>
                                                    <th>Satuan</th>
                                                    <th>Harga Netto</th>
                                                    <th>Harga Jual</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>  
                                    </div>
                                </div>
                        </div> 
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->


<!-- Modal  --> 
<div id="modal_add_obat" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateObat'))?> 
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Obat</h2> 
            </div>
            <div class="modal-body" style="height:450px;overflow:auto">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-12">
                            <label>Nama Obat<span style="color: red">*</span></label>
                            <input type="text" name="nama_obat" class="form-control" placeholder="Nama Obat">
                        </div>
                        <div class="form-group col-md-12"> 
                            <label>Jenis Obat Alkes<span style="color: red">*</span></label>
                            <select name="jenis" id="jenis"class="form-control">
                                <option value="" disabled selected>Pilih Jenis Obat Alkes</option>
                                <option value="1">OBAT</option>
                                <option value="2">BHP</option>
                                <option value="3">ALKES</option>  
                            </select>  
                        </div>
                        <div class="form-group col-md-12">
                            <label>Satuan<span style="color: red">*</span></label>
                            <select required class="form-control select" id="satuan" name="satuan">
                                <option value="" disabled selected>Satuan Obat</option>
                                <option value="Btl">Btl</option>
                                <option value="Box">Box</option>
                                <option value="Cpl">Cpl</option>
                                <option value="Mg">Mg</option>
                                <option value="Ml">Ml</option>
                                <option value="g">g</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Harga Netto<span style="color: red">*</span></label>
                            <input type="text" name="harga_netto" onkeyup="formatAsRupiah(this)" class="form-control" placeholder="Harga Netto">
                        </div>     
                        <div class="form-group col-md-12">
                            <label>Harga Jual<span style="color: red">*</span></label>
                            <input type="text" name="harga" onkeyup="formatAsRupiah(this)" class="form-control" placeholder="Harga Jual">
                        </div> 
                         
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveObat"><i class="fa fa-floppy-o p-r-10"></i>
                        SIMPAN</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

 <!-- Modal  --> 
 <div id="modal_edit_obat" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog modal-lg">
 <?php echo form_open('#',array('id' => 'fmUpdateObat'))?> 
    <div class="modal-content"> 
         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
             <h2 class="modal-title" id="myLargeModalLabel">Perbarui Obat</h2> 
         </div>
         <div class="modal-body" style="height:450px;overflow:auto">
         <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                 <span style="color: red;">* Wajib diisi</span><br>
             <div class="row">
             <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                     <div class="form-group col-md-12">
                         <label>Nama Obat<span style="color: red">*</span></label>
                         <input type="text" name="upd_nama_obat" id="upd_nama_obat" class="form-control" placeholder="Nama Obat">
                         <input type="hidden" name="upd_id_obat" id="upd_id_obat" class="validate">
                     </div>
                     <div class="form-group col-md-12"> 
                         <label>Jenis Obat Alkes<span style="color: red">*</span></label>
                         <select name="upd_jenis" id="upd_jenis"class="form-control">
                             <option value="" disabled selected>Pilih Jenis Obat Alkes</option>
                             <option value="1">OBAT</option>
                             <option value="2">BHP</option>
                             <option value="3">ALKES</option>  
                         </select>  
                     </div>
                     <div class="form-group col-md-12">
                         <label>Satuan<span style="color: red">*</span></label>
                         <select required class="form-control select" id="upd_satuan" name="upd_satuan">
                                <option value="" disabled selected>Satuan Obat</option>
                                <option value="Btl">Btl</option>
                                <option value="Box">Box</option>
                                <option value="Cpl">Cpl</option>
                                <option value="Mg">Mg</option>
                                <option value="Ml">Ml</option>
                                <option value="g">g</option>
                            </select>
                     </div>
                     <div class="form-group col-md-12">
                         <label>Harga Netto<span style="color: red">*</span></label>
                         <input type="text" name="upd_harga_netto" id="upd_harga_netto" onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga Netto">
                     </div>     
                     <div class="form-group col-md-12">
                         <label>Harga Jual<span style="color: red">*</span></label>
                         <input type="text" name="upd_harga" id="upd_harga" onkeyup="formatAsRupiah(this)" class="form-control" placeholder="Harga Jual">
                     </div> 
                      
             </div>
             <div class="row">
                 <div class="col-md-12"> 
                     <button type="button" class="btn btn-success pull-right" id="changeObat"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                 </div>
             </div> 
         </div>
         
     </div>
     <?php echo form_close(); ?>
     <!-- /.modal-content -->
 </div>
  <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

<?php $this->load->view('footer');?>