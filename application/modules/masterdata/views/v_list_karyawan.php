
<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Data - Karyawan </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master Data</a></li>
                            <li class="active">Karyawan</li>  
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                                <div class="panel-heading"> Data Karyawan
                                </div>
                                
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body"> 
                                        <div class="col-md-6">  
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="tambahKaryawan()" data-target="#modal_karyawan" ><i class="fa fa-plus p-r-10"></i>Tambah Karyawan</button>
                                        </div>
                                        <div class="col-md-6"> 
                                                <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>

                                                    &nbsp;&nbsp;&nbsp;  
                                                   <input accept=".xls, .xlsx, .csv" type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected"  onchange="showbtnimport()" />  
                                                    <label for="file" style="float: right;"><span></span> <strong>     
                                                       <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                   <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                         
                                                <?php echo form_close(); ?> 
                                                </div>
                                    <!-- <div class="table-responsive">      -->
                                       <table id="table_list_karyawan" class="table table-striped dataTable table-responsive" cellspacing="0">
                                                <thead>  
                                                    <tr> 
                                                        <th>No</th>
                                                        <th>No. Karyawan</th>
                                                        <th>No. Rekam Medis</th>
                                                        <th>Nama Karyawan</th>
                                                        <th>Tempat Lahir</th> 
                                                        <th>Tanggal Lahir</th>
                                                        <th>Jenis Kelamin</th>
                                                        <th>Alamat</th>
                                                        <th>Email</th>
                                                        <th>Jabatan</th>
                                                        <th>Tindakan_Karyawan</th>
                                                    </tr>  
                                                </thead>
                                                <tbody>  
                                                    <tr>   
                                                        <td colspan="11">No Data to Display</td>
                                                    </tr>
                                                </tbody>
                                            </table>  
                                        <!-- </div> -->
                                    </div>
                                </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->

<!-- modal add karyawan -->
<div id="modal_karyawan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-large">          
        <div class="modal-content">    
            <div class="modal-header" style="background: #fafafa;"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b id="form_judul">Tambah Data Karyawan</b></h2>     
             </div>
             <div style="margin: 8px">
                <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">      
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                    <div >
                        <p id="card_message"></p>
                    </div>
                </div> 
             </div>
               
            <div class="modal-body" style="background: #fafafa;max-height: 500px;overflow: auto;">

                                <?php echo form_open('#',array('id' => 'fmCreateKaryawan', 'data-toggle' => 'validator', 'class' => 'form_aksi'))?>
                <div class="panel panel-info1">
                    <div class="panel-heading"> Data Karyawan         
                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> </div>
                    </div>  
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                               <!-- start white-box -->

                                <div class="white-box">
                                    
                                    
                                    <div class="row">
                                        <div id="card-alert" class="card green modal_notif" style="display:none;">
                                            <div class="card-content white-text">
                                                <p id="modal_card_message"></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>


                                        <div class="col-md-6">
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                                            <input type="text" name="karyawan_id" id="karyawan_id" hidden>
                                            <input type="text" name="pasien_id" id="pasien_id" hidden>
                                            <div class="form-group">
                                                <label><span style="color: red;">Wajib diisi *</span></label>
                                                 <div class="input-group custom-search-form">
                                              <input readonly type="text" id="no_rm" name="no_rm" class="form-control" placeholder="Cari No Referensi Rekam Medis ...">
                                                <span class="input-group-btn">  
                                                    <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_ref_rm" type="button" onclick="getRefKary()"> <i class="fa fa-bars"></i></button>    
                                                </span> 
                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <label>No.Karyawan<span style="color: red;">*</span></label>
                                                <input id="no_karyawan" type="text" name="no_karyawan" class="validate form-control" placeholder="No.Karyawan">
                                            </div> 
                                            <div class="form-group">
                                                <label>No.KTP<span style="color: red;">*</span></label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "16" class="form-control no" placeholder="No.KTP" name="no_ktp" id="no_ktp" required>
                                                <div class="help-block with-errors"></div>
                                            </div> 
                                            <div class="form-group">
                                                <label>Nama Lengkap Karyawan<span style="color: red;">*</span></label>
                                                <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" placeholder="Nama Lengkap Karyawan">
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Panggilan Karyawan<span style="color: red;">*</span></label>
                                                <input id="nama_panggilan" type="text" name="nama_panggilan" class="form-control" placeholder="Nama Panggilan Karyawan">
                                            </div>
                                            <div class="form-group">
                                                <label>Tempat Lahir<span style="color: red;">*</span></label>
                                                <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                                            </div>
                                            <div> 
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="tanggal_lahir" class="control-label">Tanggal Lahir<span style="color:red">*</span></label>
                                                        <div class="input-group"> 
                                                            <input name="tgl_lahir" id="tgl_lahir" type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy" onchange="setUmur()"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                        </div> 
                                                    </div>   
                                                    <div class="form-group col-sm-6">
                                                        <label for="inputPassword" class="control-label">Umur<span style="color:red">*</span></label>
                                                        <input type="text" name="umur" id="umur" class="form-control" placeholder="Umur" readonly required> 
                                                    </div>
                                                </div>
                                            </div>    
                                            <div class="form-group">
                                                <label>Jenis Kelamin<span style="color: red;">*</span></label>
                                                <select id="jenis_kelamin" name="jenis_kelamin" class="form-control">
                                                    <option disabled selected value="">Pilih Jenis Kelamin</option>
                                                    <option value="Laki-Laki">Laki-Laki</option>
                                                    <option value="Perempuan">Perempuan</option>
                                                </select>
                                            </div> 
                                            <div class="form-group">
                                                <label>Alamat<span style="color: red;">*</span></label>  
                                                <textarea id="alamat" name="alamat" class="form-control" placeholder="Alamat"></textarea>
                                            </div>    
                                            <div class="form-group">
                                                <label>Jabatan<span style="color: red;">*</span></label>
                                                <input id="jabatan" name="jabatan" class="form-control" type="text" placeholder="Jabatan" required>
                                            </div>   
                                        </div>
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <label>No.Rek Mandiri</label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "13" class="form-control no" placeholder="No.Rek Mandiri" name="no_rek_mandiri" id="no_rek_mandiri" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label>No.BPJS</label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "13" class="form-control no" placeholder="No. BPJS" name="no_bpjs" id="no_bpjs" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label>No.BPJS Ketenagakerjaan</label>
                                                <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "13" class="form-control no" placeholder="No.BPJS Ketenagakerjaan" name="no_bpjs_kerja" id="no_bpjs_kerja" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                             <div class="form-group">
                                                <label>Agama</label>
                                                    
                                                <select id="agama" name="agama" class="form-control selectpicker">
                                                    <option value="" disabled selected>Pilih Agama</option>
                                                    <?php
                                                    $list_agama = $this->Pendaftaran_model->get_agama();
                                                    foreach($list_agama as $list){
                                                        echo "<option value='".$list->agama_id."'>".$list->agama_nama."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Status Kawin<span style="color: red;">*</span></label>
                                                <select class="form-control status_kawin" id="status_kawin" name="status_kawin">      
                                                    <option value="" disabled selected>Pilih Status Kawin</option>
                                                    <?php
                                                        foreach($list_status_kawin as $list){
                                                            echo "<option value='".$list."'>".$list."</option>";
                                                        }
                                                    ?>   
                                                </select>
                                            </div> 
                                            <div class="form-group">
                                                <label>Email<span style="color: red;">*</span></label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-error="Maaf, alamat email anda tidak valid" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>  
                                    </div>  
                                </div>    
                                <!-- end white-box -->
                        </div>
                    </div>  
                </div>
                <div class="panel panel-info1">
                    <div class="panel-heading"> Data Riwayat Pendidikan Umum         
                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a> </div>
                    </div>  
                    <div class="panel-wrapper collapse" aria-expanded="false">
                        <div class="panel-body">
                               <!-- start white-box -->  
                                <div class="white-box">    
                                    <div class="row">   
                                        <div class="col-md-6">     
                                            <div class="form-group">
                                                <label>Nama Sekolah<span style="color: red;">*</span></label>
                                                <select id="nama_sekolah" name="nama_sekolah" class="form-control select2">
                                                    <option value="" selected>Pilih Sekolah</option>
                                                    <?php
                                                    $list_sekolah = $this->Karyawan_model->get_sekola();
                                                    foreach($list_sekolah as $list){
                                                        echo "<option value='".$list->sekolah_nama."'>".$list->sekolah_nama."</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>  
                                            <div class="form-group">
                                                <label>Jurusan<span style="color: red;">*</span></label>
                                                <input type="text" id="jurusan" name="jurusan" class="form-control" placeholder="Jurusan">
                                            </div> 
                                            <div class="form-group"> 
                                                <label>Tahun Lulus<span style="color: red;">*</span></label>
                                                <input type="text" id="tahun_lulus_sekolah" name="tahun_lulus_sekolah" class="form-control" placeholder="Tahun Lulus">
                                            </div>
                                              
                                        </div>
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <label>No Ijazah<span style="color: red;">*</span></label>
                                                <input type="text" name="no_ijazah_sekolah" id="no_ijazah_sekolah" class="form-control" placeholder="No Ijazah">
                                            </div>
                                            <div class="form-group">
                                                <label>Tanggal Terverifikasi<span style="color: red;">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_verif_sekolah" id="tgl_verif_sekolah" type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>  
                                            </div>
                                            <div class="form-group">
                                                <label>Strata<span style="color: red;">*</span></label>
                                                <select name="strata" id="strata" class="form-control">
                                                    <option disabled selected value="">Pilih Strata</option>
                                                    <option value="S1">S1</option>
                                                    <option value="S2">S2</option>
                                                    <option value="S3">S3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>  
                                </div> 
                                <!-- end white-box -->
                        </div>
                    </div>  
                </div>
                <div class="panel panel-info1">
                    <div class="panel-heading"> Data Riwayat Pendidikan Profesi         
                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a> </div>
                    </div>  
                    <div class="panel-wrapper collapse" aria-expanded="false">
                        <div class="panel-body">
                               <!-- start white-box -->  
                                <div class="white-box">    
                                    <div class="row">   
                                        <div class="col-md-6">     
                                            <div class="form-group">
                                                <label>Profesi<span style="color: red;">*</span></label>
                                                <input type="text" id="profesi" name="profesi" class="form-control" placeholder="Profesi">
                                            </div>     
                                            <div class="form-group">
                                                <label>Spesialis<span style="color: red;">*</span></label>
                                                <input type="text" id="spesialis" name="spesialis" class="form-control" placeholder="Spesialis">
                                            </div> 
                                            <div class="form-group"> 
                                                <label>Tahun Lulus<span style="color: red;">*</span></label>
                                                <input type="text" name="tahun_lulus_profesi" id="tahun_lulus_profesi" class="form-control" placeholder="Tahun Lulus">
                                            </div>
                                              
                                        </div>
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <label>No Ijazah<span style="color: red;">*</span></label>
                                                <input type="text" name="no_ijazah_profesi" id="no_ijazah_profesi" class="form-control" placeholder="No Ijazah">
                                            </div>
                                            <div class="form-group">
                                                <label>Tanggal Terverifikasi<span style="color: red;">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_verif_profesi" id="tgl_verif_profesi" type="text" class="form-control mydatepicker" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>  
                                </div> 
                                <!-- end white-box -->
                        </div>
                    </div>  
                </div>
                <div class="panel panel-info1">
                    <div class="panel-heading"> Data Izin Profesi         
                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a> </div>
                    </div>  
                    <div class="panel-wrapper collapse" aria-expanded="false">
                        <div class="panel-body">
                               <!-- start white-box -->  
                                <div class="white-box">    
                                    <div class="row">      
                                        <div class="col-md-6">     
                                            <div class="form-group">
                                                <label>No.Str<span style="color: red;">*</span></label>
                                                <input type="text" name="no_str" id="no_str" class="form-control" placeholder="No.Str">
                                            </div>  
                                            <div class="form-group"> 
                                                <label>Tanggal Diterbitkan<span style="color: red;">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_terbit_izin" id="tgl_terbit_izin" type="text" class="form-control mydatepicker" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div> 
                                            <div class="form-group"> 
                                                <label>Berlaku Sampai Dengan<span style="color: red;">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_berlaku_izin" id="tgl_berlaku_izin" type="text" class="form-control mydatepicker" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Tanggal Verifikasi STR<span style="color: red;">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_verif_str" id="tgl_verif_str" type="text" class="form-control mydatepicker" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div> 
                                            </div>

                                        </div>    
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <label>No.SIP<span style="color: red;">*</span></label>
                                                <input type="text" name="no_sip" id="no_sip" class="form-control" placeholder="No.SIP">
                                            </div>   
                                            <div class="form-group">
                                                <label>Tanggal Diterbitkan SIP<span style="color: red;">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_terbit_sip" id="tgl_terbit_sip" type="text" class="form-control mydatepicker" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>    
                                            </div>
                                            <div class="form-group">
                                                <label>Berlaku Sampai Dengan SIP<span style="color: red;">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_berlaku_sip" id="tgl_berlaku_sip" type="text" class="form-control mydatepicker" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div>    
                                            </div>       
                                            <div class="form-group">
                                                <label>Tanggal Verifikasi SIP<span style="color: red;">*</span></label>
                                                <div class="input-group"> 
                                                    <input name="tgl_verif_sip" id="tgl_verif_sip" type="text" class="form-control mydatepicker" placeholder="dd/mm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                </div> 
                                            </div>

                                        </div>
                                    </div>  
                                </div> 
                                <!-- end white-box -->
                        </div>
                    </div>  
                </div>
                <div class="panel panel-info1">
                    <div class="panel-heading"> Status Kepegawaian    
                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a></div>
                    </div>   
                    <div class="panel-wrapper collapse" aria-expanded="false">
                        <div class="panel-body">
                            <!-- start white-box --> 
                            <div class="white-box">
                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status<span style="color: red;">*</span></label>    
                                            <select class="form-control">
                                                <option value="1">AKTIF</option>
                                                <option value="2">NON-AKTIF</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Status Pegawai<span style="color: red;">*</span></label>    
                                            <select class="form-control">
                                                <option value="1">TETAP</option>
                                                <option value="2">KONTRAK</option>
                                                <option value="3">TRAINING</option>
                                                <option value="4">ORENTASI</option>
                                            </select>
                                            <!-- <input type="text" name="status_pegawai" id="status_pegawai" class="form-control" placeholder="Status Pegawai"> -->
                                        </div> 
                                        <div class="form-group">
                                            <label>Tanggal Mulai Orientasi<span style="color: red;">*</span></label>
                                            <input type="text" id="tgl_mulai_orientasi" name="orientasi" class="form-control mydatepicker" placeholder="Orientasi">
                                        </div>
                                         <div class="form-group">
                                            <label>Tanggal Berakhir Orientasi<span style="color: red;">*</span></label>
                                            <input type="text" id="tgl_berakhir_orientasi" name="orientasi" class="form-control mydatepicker" placeholder="Orientasi" onchange="hitung_orientasi()">
                                        </div>
                                        <div class="form-group">
                                            <label>Lama orientasi<span style="color: red;">*</span></label>
                                            <input type="text" name="lama_orientasi" id="lama_orientasi" class="form-control" placeholder="Lama orientasi" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Mulai Training<span style="color: red;">*</span></label>
                                            <input type="text" id="tgl_mulai_training" name="orientasi" class="form-control mydatepicker" placeholder="Training">
                                        </div>
                                         <div class="form-group">
                                            <label>Tanggal Berakhir Training<span style="color: red;">*</span></label>
                                            <input type="text" id="tgl_berakhir_training" name="orientasi" class="form-control mydatepicker" placeholder="Training" onchange="hitung_training()">
                                        </div>
                                        <div class="form-group">
                                            <label>Lama Training<span style="color: red;">*</span></label>
                                            <input type="text" name="lama_training" id="lama_training" class="form-control" placeholder="Lama Training" readonly>
                                        </div>
                                        
                                         <div class="form-group">
                                            <label>No.Kontrak<span style="color: red;">*</span></label>    
                                            <input type="text" name="no_kontrak" id="no_kontrak" class="form-control" placeholder="No.Kontrak">
                                        </div>
                                        <div class="form-group">
                                            <label>TMT<span style="color: red;">*</span></label>
                                            <input type="text" name="tmt" id="tmt" class="form-control mydatepicker" placeholder="TMT">
                                        </div>
                                        <div class="form-group">
                                            <label>Tgl.Berakhir<span style="color: red;">*</span></label>    
                                            <input type="text" name="tgl_berakhir" id="tgl_berakhir" class="form-control mydatepicker" placeholder="Tgl.Berakhir" onchange="hitung_masakerja()" >
                                        </div>   
                                        <div class="form-group">
                                            <label>Masa Kerja<span style="color: red;">*</span></label>
                                            <input type="text" id="masa_kerja" class="form-control" placeholder="Masa Kerja" readonly>
                                        </div>
                                       
                                       
                                     </div>  
                                     <div class="col-md-6">
                                      <div class="form-group">
                                            <label>Jabatan<span style="color: red;">*</span></label> 
                                             <select multiple name="jabatan[]" id="jabatan" class="select2" >
                                                <optgroup label="Pilih Jabatan">
                                                
                                                </optgroup>
                                            </select>   
                                        </div> 
                                         <div class="form-group">
                                            <label>Unit Kerja<span style="color: red;">*</span></label>    
                                            <input type="text" name="unit_kerja_id" id="unit_kerja_id" class="form-control " placeholder="Unit Kerja">
                                        </div> 
                                        <div class="form-group">
                                            <label>Ruang<span style="color: red;">*</span></label>    
                                            <input type="text" name="ruang" id="ruang" class="form-control" placeholder="Ruang">
                                        </div>
                                        <div class="form-group">
                                            <label>Penilaian Resiko<span style="color: red;">*</span></label>    
                                            <input type="text" name="penilaian_resiko" id="penilaian_resiko" class="form-control" placeholder="Penilaian Resiko">
                                        </div>
                                        <div class="form-group">
                                            <label>Kelompok Intensif<span style="color: red;">*</span></label>    
                                            <input type="text" name="kelompok_intensif" id="kelompok_intensif" class="form-control " placeholder="Kelompok Intensif">
                                        </div> 
                                        <div class="form-group">
                                            <label>Hak Insentif Bersama<span style="color: red;">*</span></label>    
                                            <input type="text" name="hak_bersama" id="hak_bersama" class="form-control" placeholder="Hak Bersama">
                                        </div>  
                                        <div class="form-group">
                                            <label>Hak Insentif Unit<span style="color: red;">*</span></label>    
                                            <input type="text" name="hak_unit" id="hak_unit" class="form-control " placeholder="Hak Unit">
                                        </div> 
                                        <div class="form-group">
                                            <label>Penempatan<span style="color: red;">*</span></label>    
                                            <input type="text" name="penempatan" id="penempatan" class="form-control" placeholder="Penempatan">
                                        </div>
                                         <div class="form-group">
                                            <label>Keluar<span style="color: red;">*</span></label>    
                                            <input type="text" name="keluar" id="keluar" class="form-control mydatepicker" placeholder="Keluar">
                                        </div>
                                        <div class="form-group">
                                            <label>Alasan Keluar<span style="color: red;">*</span></label>    
                                            <input type="text" name="alasan_keluar" id="alasan_keluar" class="form-control " placeholder="Alasan Keluar">
                                        </div>
                                     </div>
                                </div>  
                            </div>
                            <!-- end white-box -->
                        </div>
                    </div>  
                </div>

                    <div class="white-box"> 
                        <div class="row"> 
                            <a class="btn btn-success aksi_button" ><i class="fa fa-floppy-o p-r-10 "></i>SIMPAN</a>
                        </div>
                    </div> 
            <?php echo form_close(); ?>
                    
                   
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

 

  
<div id="modal_detail_karyawan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">  
    <div class="modal-dialog modal-large" style="max-width: 1300px">           
        <div class="modal-content" style="margin-top: 160px">    
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Karyawan</b></h2>     
             </div>   
            <div class="modal-body" style="height: 550px;overflow: auto;">
                <div class="embed-responsive embed-responsive-16by9" style="height:500px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>    
                </div> 



            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal_ref_rm" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content">   
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">List Referensi Rekam Medis</h2> </div>   
            <div class="modal-body" style="height:500px;overflow: auto;"> 
                    <div class="table-responsive">      
                        <table id="table_ref_rm" class="table table-striped dataTable no-footer" cellspacing="0">
                            <thead> 
                                <tr>
                                    <th>No</th>
                                    <th>No Rekam Medis</th>
                                    <th>Nama Pasien</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Tempat Lahir</th>
                                    <th>Alamat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                  </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
           

<?php $this->load->view('footer');?>
      