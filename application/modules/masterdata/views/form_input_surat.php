<?php $this->load->view("header_iframe") ?>
 
<div class="row">
    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
        <div class="form-group col-md-12">
            <label>Nama Obat<span style="color: red">*</span></label> 
            <input type="text" name="nama_obat" class="form-control" placeholder="Nama Obat">
        </div>
        <div class="form-group col-md-12"> 
            <label>Jenis Obat Alkes<span style="color: red">*</span></label>
            <select name="jenis" id="jenis"class="form-control">
                <option value="" disabled selected>Pilih Jenis Obat Alkes</option>
                <option value="1">OBAT</option>
                <option value="2">BHP</option>
                <option value="3">ALKES</option>  
            </select>  
        </div>
        <div class="form-group col-md-12">
            <label>Satuan<span style="color: red">*</span></label>
            <select required class="form-control select" id="satuan" name="satuan">
                <option value="" disabled selected>Satuan Obat</option>
                <option value="Btl">Btl</option>
                <option value="Box">Box</option>
                <option value="Cpl">Cpl</option>
                <option value="Mg">Mg</option>
                <option value="Ml">Ml</option>
                <option value="g">g</option>
            </select>
        </div>
        <div class="form-group col-md-12">
            <label>Harga Netto<span style="color: red">*</span></label>
            <input type="text" name="harga_netto" onkeyup="formatAsRupiah(this)" class="form-control" placeholder="Harga Netto">
        </div>     
        <div class="form-group col-md-12">
            <label>Harga Jual<span style="color: red">*</span></label>
            <input type="text" name="harga" onkeyup="formatAsRupiah(this)" class="form-control" placeholder="Harga Jual">
        </div> 
             
</div>
    <div class="row">
        <div class="col-md-12"> 
            <button type="button" class="btn btn-success pull-right" id="saveObat"><i class="fa fa-floppy-o p-r-10"></i>
            SIMPAN</button>
        </div>
    </div>
<?php $this->load->view("footer_iframe") ?>