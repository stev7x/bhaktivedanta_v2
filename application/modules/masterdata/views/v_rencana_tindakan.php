<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
      <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Rencana Tindakan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Rencana Tindakan </li>
                        </ol> 
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1"> 
                            <div class="panel-heading"> Master Rencana Tindakan
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <div class="col-md-6">
                                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_rencana_tindakan" data-backdrop="static" data-keyboard="false">Tambah Rencana Tindakan</button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <!-- <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Tindakan Lab</button>  -->
                                                </div>
                                                <!-- <div class="col-md-6"> 
                                                        <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>
                                                            &nbsp;&nbsp;&nbsp;  
                                                            <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />  
                                                            <label for="file" style="float: right;"><span></span> <strong>     
                                                            <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                            <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                                
                                                        <?php echo form_close(); ?> 
                                                </div>                                               -->
                                            </div>
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                           <table id="table_rencana_tindakan_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                            <thead> 
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Rencana Tindakan</th>
                                                    <th>Kelompok Rencana Tindakan</th>
                                                    <th>Action</th>
                                                    </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                
                                            </tfoot>
                                        </table>    
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div> 
            <!-- /.container-fluid -->
<div id="modal_add_rencana_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateRencanaTindakan'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Rencana Tindakan</h2> 
            </div>
            <div class="modal-body"> 
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden" name="rencana_tindakan_id">
                        <div class="form-group col-md-12">
                            <label>Nama Rencana Tindakan<span style="color: red">*</span></label>
                            <input type="text" name="rencana_tindakan_nama" id="rencana_tindakan_nama" class="form-control" placeholder="Nama Rencana Tindakan">
                        </div>    
                        <div class="form-group col-md-12">
                            <label>Kelompok Rencana Tindakan <span style="color: red">*</span></label>
                            <select name="kelompoktindakan_id" id="kelompoktindakan_id" name="kelompoktindakan_id" class="form-control">
                                <option value="" disabled selected>Pilih Kelompok Tindakan</option>
                                <?php
                                    $list_kelompoktindakan = $this->Rencana_tindakan_model->get_data_kelompoktindakan();
                                    foreach($list_kelompoktindakan as $list){
                                        echo "<option value='".$list->kelompoktindakan_id."'>".$list->kelompoktindakan_nama."</option>";
                                    }
                                    ?>
                            </select>
                        </div>      
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveRencanaTindakan"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
<div id="modal_edit_rencana_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmUpdateRencanaTindakan'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Edit Tindakan Lab</h2> 
            </div>
            <div class="modal-body"> 
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden" name="upd_rencana_tindakan_id" id="upd_rencana_tindakan_id" >
                        <div class="form-group col-md-12">
                            <label>Nama Tindakan Lab<span style="color: red">*</span></label>
                            <input type="text" name="upd_rencana_tindakan_nama" id="upd_rencana_tindakan_nama" class="form-control" placeholder="Nama Rencana Tindakan">
                        </div>    
                        <div class="form-group col-md-12">
                            <label>Kelompok Tindakan Lab<span style="color: red">*</span></label>
                            <select name="upd_kelompoktindakan_id" id="upd_kelompoktindakan_id" name="kelompoktindakan_id" class="form-control">
                                <option value="" disabled selected>Pilih Kelompok Tindakan Lab</option>
                                <?php
                                    $list_kelompoktindakan = $this->Rencana_tindakan_model->get_data_kelompoktindakan();
                                    foreach($list_kelompoktindakan as $list){
                                        echo "<option value='".$list->kelompoktindakan_id."'>".$list->kelompoktindakan_nama."</option>";
                                    }
                                    ?>
                            </select>
                        </div>    
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="changeRencanaTindakan"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

 <!-- Modal  --> 
<?php $this->load->view('footer');?>
      