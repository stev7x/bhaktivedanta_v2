<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
     <!-- Page Content -->
       <div id="page-wrapper">
           <div class="container-fluid">
               <div class="row bg-title">
                   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                       <h4 class="page-title">Master Tindakan</h4> </div>
                   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                       <ol class="breadcrumb">
                           <li><a href="index.html">Master</a></li>
                           <li class="active">Tindakan</li>
                       </ol> 
                   </div>
                   <!-- /.col-lg-12 -->
               </div>
               <!--row -->
               <div class="row">  
                   <div class="col-sm-12">
                       <div class="panel panel-info1"> 
                           <div class="panel-heading"> Master Tindakan
                           </div>
                           <div class="panel-wrapper collapse in" aria-expanded="true">
                               <div class="panel-body">

                                           <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <div class="col-md-6">
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add" data-backdrop="static" data-keyboard="false">Tambah Tindakan</button>  
                                               &nbsp;&nbsp;&nbsp;
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_hotel" data-backdrop="static" data-keyboard="false"><i class="fa fa-cloud-upload"></i> Eksport Data</button>  
                                            </div>    
                                            
                                           </div>
                                           <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                          <table id="table_master" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                           <thead>  
                                               <tr>
                                                   <th>No</th>
                                                   <th>Nama Tindakan</th>
                                                   <th>Harga</th>                          
                                                   <th>Jenis Pasien</th> 
                                                   <th>Tipe Call </th>
                                                   <th>Escort</th>
                                                   <th>Action </th>
                                                  
                                               </tr>
                                           </thead>
                                           <tbody>
                                               
                                           </tbody>
                                           <tfoot>
                                               <tr>
                                                   <th>No</th>
                                                   <th>Nama Tindakan</th>
                                                   <th>Harga</th>                          
                                                   <th>Jenis Pasien</th> 
                                                   <th>Tipe Call </th>
                                                   <th>Escort</th>
                                                   <th>Action </th>
                                               </tr>
                                           </tfoot>
                                       </table>    
                               </div>
                           </div>  
                       </div>
                   </div>
               </div>
               <!--/row -->
               
           </div> 
           <!-- /.container-fluid -->

<!-- Modal  --> 
<div id="modal_add" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmCreateAutoStopObat'))?>
      <div class="modal-content"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Tambah Tindakan</h2> 
           </div>
           <div class="modal-body"> 
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input id="daftartindakan_id" type="hidden" name="daftartindakan_id">
                       <div class="form-group col-md-12">
                            <label>Nama Tindakan<span style="color: red">*</span></label>
                            <input type="text" name="daftartindakan_nama" id="daftartindakan_nama" class="form-control" placeholder="Nama Tindakan">
                        </div>  
                        <div class="form-group col-md-12">
                            <label>Harga<span style="color: red">*</span></label>
                            <input type="text" name="harga_tindakan" id="harga_tindakan" class="form-control" placeholder="Harga">
                        </div> 
                        <div class="form-group col-md-12">
                            <label>Jenis Pasien<span style="color: red">*</span></label>
                            <select class="form-control" id="jenis_pasien" name="jenis_pasien">
                                                <option selected disabled>Choose</option>
                                                <option value="LOKAL">LOCAL</option>
                                                <option value="DOMESTIK">DOMESTIC</option>
                                                <option value="ASING">TOURIST</option>
                                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Tipe Call<span style="color: red">*</span></label>
                            <select class="form-control" id="type_call" name="type_call">
                                                <option selected disabled>Choose</option>
                                                <option value="VISIT">VISIT</option>
                                                <option value="CALL">CALL</option>
                                            </select>
                        </div>
                       <div class="form-group col-md-12">
                           <label>Escort<span style="color: red">*</span></label>
                           <select class="form-control" id="escort" name="escort">
                               <option selected disabled>Choose</option>
                               <option value="0">Non Escort</option>
                               <option value="1">Escort</option>
                           </select>
                       </div>
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="saveAutoStopObat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmUpdateAutoStopObat'))?>
      <div class="modal-content"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Tindakan</h2> 
           </div>
           <div class="modal-body"> 
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input id="upd_daftartindakan_id" type="hidden" name="daftartindakan_id">
                       <div class="form-group col-md-12">
                            <label>Nama Tindakan<span style="color: red">*</span></label>
                            <input type="text" name="daftartindakan_nama" id="upd_daftartindakan_nama" class="form-control" placeholder="Nama Tindakan">
                        </div>  
                        <div class="form-group col-md-12">
                            <label>Harga<span style="color: red">*</span></label>
                            <input type="text" name="harga_tindakan" id="upd_harga_tindakan" class="form-control" placeholder="Harga">
                        </div> 
                        <div class="form-group col-md-12">
                            <label>Jenis Pasien<span style="color: red">*</span></label>
                            <select class="form-control" id="upd_jenis_pasien" name="jenis_pasien">
                                                <option selected disabled>Choose</option>
                                                <option value="LOKAL">LOCAL</option>
                                                <option value="DOMESTIK">DOMESTIC</option>
                                                <option value="ASING">TOURIST</option>
                                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Tipe Call<span style="color: red">*</span></label>
                            <select class="form-control" id="upd_type_call" name="type_call">
                                                <option selected disabled>Choose</option>
                                                <option value="VISIT">VISIT</option>
                                                <option value="CALL">CALL</option>
                                            </select>
                        </div>
                   <div class="form-group col-md-12">
                       <label>Escort<span style="color: red">*</span></label>
                       <select class="form-control" id="upd_escort" name="escort">
                           <option selected disabled>Choose</option>
                           <option value="0">Non Escort</option>
                           <option value="1">Escort</option>
                       </select>
                   </div>
                       
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="changeAutoStopObat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>




<?php $this->load->view('footer');?>