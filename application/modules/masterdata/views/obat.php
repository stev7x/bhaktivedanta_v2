<?php $this->load->view('header');?>
<title>SIMRS | Master Obat</title>
<?php $this->load->view('sidebar');?>
      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Master Obat</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Master</a></li>
                    <li class="active">Obat</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        
        
        <!--start container-->
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <p class="right-align"><a class="btn waves-effect waves-light indigo modal-trigger" href="#modal_add_obat">Tambah Obat</a></p>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="divider"></div>
                            <h4 class="header">Master Obat</h4>
                            <div id="card-alert" class="card green notif" style="display:none;">
                                <div class="card-content white-text">
                                    <p id="card_message">SUCCESS : The page has been added.</p>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div id="table-datatables">
                                <div class="row">
                                    <div class="col s12 m4 l12">
                                        <table id="table_obat_list" class="responsive-table display" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Jenis</th>
                                                    <th>Nama Obat</th>
                                                    <th>Satuan</th>
                                                    <th>Harga Netto</th>
                                                    <th>Harga Jual</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Jenis</th>
                                                    <th>Nama Obat</th>
                                                    <th>Satuan</th>
                                                    <th>Harga Netto</th>
                                                    <th>Harga Jual</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
    <!-- END CONTENT -->
    
    <!-- Start Modal Add Obat -->
    <div id="modal_add_obat" class="modal">
        <?php echo form_open('#',array('id' => 'fmCreateobat'))?>
        <div class="modal-content">
            <h1>Tambah Obat</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="nama_obat" name="nama_obat" class="validate">
                            <label for="nama_obat">Nama Obat<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="jenis" name="jenis">
                                <option value="" disabled selected>Pilih Jenis Obat Alkes</option>
                                <?php
                                $list_jenis = $this->Obat_model->get_jenisobat_list();
                                foreach($list_jenis as $list){
                                    echo "<option value=".$list->jenisoa_id.">".$list->jenisoa_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Jenis Obat Alkes<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="satuan" name="satuan" class="validate">
                            <label for="satuan">Satuan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="number" id="harga_netto" name="harga_netto" class="validate">
                            <label for="harga_netto">Harga Netto<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="number" id="harga" name="harga" class="validate">
                            <label for="harga">Harga Jual<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="saveobat">Save<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- End Modal Add Obat -->
     <!-- Modal Update Obat-->
     <div id="modal_update_obat" class="modal fadeIn" tabindex="-1" role="dialog">
        <?php echo form_open('#',array('id' => 'fmUpdateObat'))?>
        <div class="modal-content">
            <h1>Update Obat</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green upd_modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="upd_modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="upd_nama_obat" name="upd_nama_obat" class="validate">
                            <label for="upd_nama_obat" id="lbl_upd_nama_obat">Nama Obat<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="upd_jenis" name="upd_jenis">
                                <option value="" disabled selected>Pilih Jenis Obat Alkes</option>
                                <?php
                                $list_jenis = $this->Obat_model->get_jenisobat_list();
                                foreach($list_jenis as $list){
                                    echo "<option value=".$list->jenisoa_id.">".$list->jenisoa_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Jenis Obat Alkes<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="upd_satuan" name="upd_satuan" class="validate">
                            <label for="upd_satuan" id="lbl_upd_satuan">Satuan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="number" id="upd_harga_netto" name="upd_harga_netto" class="validate">
                            <label for="upd_harga_netto" id="lbl_upd_harga_netto">Harga Netto<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="number" id="upd_harga" name="upd_harga" class="validate">
                            <label for="upd_harga" id="lbl_upd_harga">Harga Jual<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="upd_id_obat" name="upd_id_obat" class="validate">
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="changeObat">Save Change<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php $this->load->view('footer');?>
      
