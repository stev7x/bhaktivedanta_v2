<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?> 
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Dokter</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Dokter</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">   
                            <div class="panel-heading"> Master Dokter   
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <div class="col-md-6">
                                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_dokter" data-backdrop="static" data-keyboard="false">Tambah Dokter</button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Dokter</button>
                                                </div>
                                                <!-- <div class="col-md-6">  
                                                <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>

                                                    &nbsp;&nbsp;&nbsp;    
                                                   <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />  
                                                    <label for="file" style="float: right;"><span></span> <strong>     
                                                       <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                   <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                         
                                                <?php echo form_close(); ?>  
                                                </div> -->

                                            </div> 
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />    
                                           <table id="table_dokter_list" class="table table-striped" cellspacing="0"> 
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Dokter</th>
                                                        <th>Alamat</th>
                                                        <th>Kelompok Dokter</th>
                                                        <!-- <th>Jumlah Pasien</th> -->
                                                        <th>Action</th>
                                                    </tr>
                                                </thead> 
                                                <tbody> 

                                                </tbody> 
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th> 
                                                        <th>Nama Dokter</th>
                                                        <th>Alamat</th>
                                                        <th>Kelompok Dokter</th>
                                                        <!-- <th>Jumlah Pasien</th> -->
                                                        <th>Action</th>
                                                    </tr>  
                                                </tfoot>
                                            </table>  
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- Modal  -->
<div id="modal_add_dokter" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateDokter'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Dokter</h2> 
            </div>
            <div class="modal-body">
                <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                    <div>
                        <p id="card_message" class=""></p>
                    </div>
                </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-12">
                            <label>Nama Dokter<span style="color: red">*</span></label>
                            <input type="text" name="nama_dokter" id="nama_dokter" class="form-control" placeholder="Nama Dokter">
                        </div>   
                        
                        <div class="form-group col-md-12"> 
                            <label>Alamat<span style="color: red">*</span></label>
                            <textarea name="alamat" class="form-control" id="alamat" placeholder="Alamat"></textarea>
                        </div>   
                        <div class="form-group col-md-12"> 
                            <label>Kelompok Dokter<span style="color: red">*</span></label>
                            <select name="kel_dokter" id="kel_dokter" class="form-control">
                            <option value="" disabled selected>Pilih Kelompok Dokter</option>
                            <?php 
                            $list_kelompokdokter = $this->Dokter_model->get_kel_dokter_list();
                            foreach($list_kelompokdokter as $list){
                                echo "<option value=".$list->kelompokdokter_id.">".$list->kelompokdokter_nama."</option>";
                            }
                            ?>
                            </select>   
                        </div> 
                        <!-- <div class="form-group col-md-12"> 
                            <label>Jumlah Pasien<span style="color: red">*</span></label>
                            <input type="number" name="jumlah_pasien" id="jumlah_pasien" class="form-control" placeholder="Jumlah Pasien">  
                        </div>        -->
  
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveDokter"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?> 
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->


<!-- Modal  -->  
<div id="modal_edit_dokter" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-lg">
<?php echo form_open('#',array('id' => 'fmUpdateDokter'))?>
   <div class="modal-content"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel">Perbarui Dokter</h2> 
        </div>
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
            <div class="row">
            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <div class="form-group col-md-12">
                        <label>Nama Dokter<span style="color: red">*</span></label>
                        <input type="text" name="upd_nama_dokter" id="upd_nama_dokter" class="form-control" placeholder="Nama Dokter">
                        <input type="hidden" name="upd_id_dokter" id="upd_id_dokter">
                    </div>     
                    <div class="form-group col-md-12"> 
                        <label>Alamat<span style="color: red">*</span></label>
                        <textarea name="upd_alamat" id="upd_alamat" class="form-control" placeholder="Alamat"></textarea>
                    </div>   
                    <div class="form-group col-md-12"> 
                        <label>Kelompok Dokter<span style="color: red">*</span></label>
                        <select name="upd_kel_dokter" id="upd_kel_dokter" class="form-control">
                        <option value="" disabled selected>Pilih Kelompok Dokter</option>
                        <?php 
                        $list_kelompokdokter = $this->Dokter_model->get_kel_dokter_list();
                        foreach($list_kelompokdokter as $list){
                            echo "<option value=".$list->kelompokdokter_id.">".$list->kelompokdokter_nama."</option>";
                        } 
                        ?>
                        </select>  
                    </div>  
                    <!-- <div class="form-group col-md-12"> 
                        <label>Jumlah Pasien<span style="color: red">*</span></label>
                        <input type="number" name="upd_jumlah_pasien" id="upd_jumlah_pasien" class="form-control" placeholder="Jumlah Pasien">   
                    </div>       -->

            </div>
            <div class="row">
                <div class="col-md-12"> 
                    <button type="button" class="btn btn-success pull-right" id="changeDokter"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                </div>
            </div> 
        </div>
    </div>
    <?php echo form_close(); ?> 
    <!-- /.modal-content -->
</div>
 <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->
 

<?php $this->load->view('footer');?>
      