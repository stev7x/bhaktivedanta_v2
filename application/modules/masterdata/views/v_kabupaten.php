<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Kabupaten</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Kabupaten</li>
                        </ol>   
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row"> 
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Master Kabupaten  
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_kabupaten" data-backdrop="static" data-keyboard="false">Tambah Kabupaten</button>
                                            </div>
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />   
                                           <table id="table_kabupaten_list" class="table table-striped dataTable" cellspacing="0">     
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Provinsi</th> 
                                                        <th>Nama Kabupaten</th> 
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Nama Provinsi</th>
                                                      <th>Nama Kabupaten</th>
                                                      <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>  
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- modal -->
<div id="modal_add_kabupaten" class="modal fade bs-example-modal-lg modal-add" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog modal-lg"> 
       <?php echo form_open('#',array('id' => 'fmCreateKabupatenOperasi'))?>
       <div class="modal-content" style="margin-top: 100px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Kabupaten</h2> 
            </div>    
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>                
            <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Pilih Provinsi<span style="color: red">*</span></label>
                        <select id="select_provinsi" name="select_provinsi" class="form-control">
                                <option value="" disabled selected>Pilih Provinsi</option>
                                <?php
                                $list_kabupaten = $this->Kabupaten_model->get_provinsi_list();
                                foreach($list_kabupaten as $list){
                                    echo "<option value=".$list->propinsi_id.">".$list->propinsi_nama."</option>";
                                }
                                ?>
                        </select> 
                    </div>
                    <div class="form-group col-md-12">
                        <label>Nama Kabupaten<span style="color: red">*</span></label>
                        <input type="text" id="nama_kabupaten" name="nama_kabupaten" class="form-control"> 
                    </div>   
                </div> 

                <div class="row">
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="saveKabupatenOperasi"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
 
 <!-- Modal  -->  
<div id="modal_update_kabupaten" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg"> 
       <?php echo form_open('#',array('id' => 'fmUpdateKabupatenOperasi'))?>
       <div class="modal-content" style="margin-top: 100px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Perbarui Kabupaten</h2> 
            </div> 
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div> 
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Provinsi<span style="color: red">*</span></label>   
                        <select id="upd_select_privinsi" name="upd_select_privinsi" class="form-control">
                            <option value="" disabled selected>Pilih Provinsi</option>
                            <?php
                            $list_kabupaten = $this->Kabupaten_model->get_provinsi_list();
                            foreach($list_kabupaten as $list){
                                echo "<option value=".$list->propinsi_id.">".$list->propinsi_nama."</option>";
                            }
                            ?>
                        </select>  
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Nama Kabupaten<span style="color: red">*</span></label>   
                        <input type="text" id="upd_nama_kabupaten" name="upd_nama_kabupaten" class="form-control" value="">
                        <input type="hidden" id="upd_id_kabupaten" name="upd_id_kabupaten" class="validate">
                    </div>  
                </div> 

                <div class="row">
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="changeKabupatenOperasi"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                    </div>
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  

<?php $this->load->view('footer');?>
      