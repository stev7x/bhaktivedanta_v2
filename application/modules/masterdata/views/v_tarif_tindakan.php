<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Tarif Tindakan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Tarif Tindakan</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">   
                            <div class="panel-heading"> Master Tarif Tindakan   
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_tarif_tindakan" data-backdrop="static" data-keyboard="false">Tambah Tarif Tindakan</button>
                                            </div> 
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />    
                                           <table id="table_tarif_tindakan_list" class="table table-striped table-responsive" cellspacing="0">  
                                                <thead> 
                                                   <tr>
                                                        <th>No</th>
                                                        <th>Kelas Pelayanan</th>
                                                        <th>Tindakan</th>
                                                        <th>Cyto Tindakan</th>
                                                        <th>Tarif Tindakan</th>
                                                        <th>Komponen Tarif</th>
                                                        <th>BPJS/Non BPJS</th>
                                                        <th width="100px">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody> 

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kelas Pelayanan</th>
                                                        <th>Tindakan</th>
                                                        <th>Cyto Tindakan</th>
                                                        <th>Tarif Tindakan</th>
                                                        <th>Komponen Tarif</th>
                                                        <th>BPJS/Non BPJS</th>
                                                        <th>Action</th>
                                                    </tr>   
                                                </tfoot>
                                            </table>  
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- Modal  -->
<div id="modal_add_tarif_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Tarif Tindakan</h2> 
            </div>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                    <span style="color: red; font-size:12px;">*) Wajib diisi</span><br>
                    <span>&nbsp;</span><br>
                <div class="row">
                <?php echo form_open('#',array('id' => 'fmCreateTarif_tindakan'))?>
                        <div class="form-group col-md-6"> 
                            <label>Kelas<span style="color: red">*</span></label>
                            <select id="select_kelas" name="select_kelas" class="form-control">
                                <option value="" disabled selected>Pilih Kelas</option>
                                <?php
                                $list_kelas = $this->Tarif_tindakan_model->get_kelas_list();
                                foreach($list_kelas as $list){
                                    echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                }
                                ?>
                            </select> 
                        </div>
                        <div class="form-group col-md-6"> 
                            <label>Pelayanan<span style="color: red">*</span></label>
                            <select id="select_tindakan" name="select_tindakan" class="form-control">
                                <option value="" disabled selected>Pilih Pelayanan</option>
                                <?php
                                $list_tindakan = $this->Tarif_tindakan_model->get_daftar_list();
                                foreach($list_tindakan as $list){
                                    echo "<option value=".$list->daftartindakan_id.">".$list->daftartindakan_nama."</option>";
                                }
                                ?>
                            </select>  
                        </div>
                        <div class="form-group col-md-6"> 
                            <label>Komponen tarif<span style="color: red">*</span></label>
                            <select id="select_komponen" name="select_komponen" class="form-control">
                                <option value="" disabled selected>Pilih Komponen Tarif</option>
                                <?php
                                $list_komponen = $this->Tarif_tindakan_model->get_komponen_tarif();
                                foreach($list_komponen as $list){
                                    echo "<option value=".$list->komponentarif_id.">".$list->komponentarif_nama."</option>";
                                }
                                ?>
                            </select>  
                        </div>
                        <div class="form-group col-md-6"> 
                            <label>Jenis tarif<span style="color: red">*</span></label>
                            <select id="bpjs_non_bpjs" name="bpjs_non_bpjs" class="form-control">
                                <option value="" disabled selected>Pilih Jenis Tarif</option>
                                <option value="BPJS">BPJS</option>
                                <option value="Non BPJS">Non BPJS</option>
                            </select>
                        </div> 
                        <div class="form-group col-md-6">
                            <label>Tarif Tindakan<span style="color: red">*</span></label>
                            <input type="text" id="nama_tarif_tindakan" onkeyup="formatAsRupiah(this)" name="nama_tarif_tindakan" class="form-control no" placeholder="Tarif Tindakan">
                        </div> 
                        <div class="form-group col-md-6">
                            <label>Cyto Tindakan<span style="color: red">*</span></label>
                            <input type="text" id="cyto_tindakan" name="cyto_tindakan" onkeyup="formatAsRupiah(this)" class="form-control no" placeholder="Harga Cyto">                            
                        </div>      
                </div><br><br>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveTarif_tindakan"><i class="fa fa-floppy-o p-r-10"></i> SIMPAN</button>
                    </div> 
                </div> 
                <?php echo form_close(); ?>
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->


<!-- Modal  -->
<div id="modal_edit_tarif_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Perbarui Tarif Tindakan</h2> 
            </div> 
            <?php echo form_open('#',array('id' => 'fmUpdateTarif_tindakan'))?>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <input type="hidden" id="upd_id_tarif_tindakan" name="upd_id_tarif_tindakan" class="validate">
                        <div class="form-group col-md-6"> 
                            <label>Kelas<span style="color: red">*</span></label>
                            <select id="upd_select_kelas" name="upd_select_kelas" class="form-control">
                                <option value="" disabled selected>Pilih Kelas</option>
                                <?php
                                $list_kelas = $this->Tarif_tindakan_model->get_kelas_list();
                                foreach($list_kelas as $list){
                                    echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                }
                                ?>
                            </select>  
                        </div>
                        <div class="form-group col-md-6"> 
                            <label>Pelayanan<span style="color: red">*</span></label>
                            <select id="upd_select_tindakan" name="upd_select_tindakan" class="form-control">
                                <option value="" disabled selected>Pilih Pelayanan</option>
                                <?php
                                $list_tindakan = $this->Tarif_tindakan_model->get_daftar_list();
                                foreach($list_tindakan as $list){
                                    echo "<option value=".$list->daftartindakan_id.">".$list->daftartindakan_nama."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6"> 
                            <label>Komponen tarif<span style="color: red">*</span></label>
                            <select id="upd_select_komponen" name="upd_select_komponen" class="form-control">
                                <option value="" disabled selected>Pilih Komponen Tarif</option>
                                <?php
                                $list_komponen = $this->Tarif_tindakan_model->get_komponen_tarif();
                                foreach($list_komponen as $list){
                                    echo "<option value=".$list->komponentarif_id.">".$list->komponentarif_nama."</option>";
                                }
                                ?>
                            </select>  
                        </div>
                        <div class="form-group col-md-6"> 
                            <label>Jenis tarif<span style="color: red">*</span></label>
                            <select id="upd_bpjs_non_bpjs" name="upd_bpjs_non_bpjs" class="form-control">
                                <option value="" disabled selected>Pilih Jenis Tarif</option>
                                <option value="BPJS">BPJS</option>
                                <option value="Non BPJS">Non BPJS</option>
                            </select>  
                        </div> 
                        <div class="form-group col-md-6">
                            <label>Tarif Tindakan<span style="color: red">*</span></label>
                            <input type="text" id="upd_nama_tarif_tindakan" name="upd_nama_tarif_tindakan" onkeyup="formatAsRupiah(this)" class="form-control no" value="">
                        </div> 
                        <div class="form-group col-md-6">
                            <label>Cyto Tindakan<span style="color: red">*</span></label>
                            <input type="text" id="upd_cyto_tindakan" name="upd_cyto_tindakan" onkeyup="formatAsRupiah(this)"  class="form-control no">
                        </div>      
                </div><br><br>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="changeTarif_tindakan"><i class="fa fa-floppy-o p-r-10"></i> PERBARUI</button>
                    </div> 
                </div> 
                <?php echo form_close(); ?>
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal --> 

<?php $this->load->view('footer');?>
      