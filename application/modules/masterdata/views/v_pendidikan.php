<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Pendidikan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Pendidikan</li>
                        </ol> 
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1"> 
                            <div class="panel-heading"> Master Pendidikan
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_pendidikan" data-backdrop="static" data-keyboard="false">Tambah Pendidikan</button>
                                            </div>  
                                           <table id="table_pendidikan_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                            <thead> 
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Pendidikan</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Pendidikan</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>    
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->


<!-- Modal  --> 
<div id="modal_add_pendidikan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreatePendidikan'))?>  
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Pendidikan</h2> 
            </div>
            <div class="modal-body"> 
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
               
                        <div class="form-group col-md-12">
                            <label>Nama Pendidikan<span style="color: red">*</span></label>
                            <input type="text" name="nama_pendidikan" id="nama_pendidikan" class="form-control" placeholder="Nama Pendidikan">
                        </div>    
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="savePendidikan"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->


 <div id="modal_edit_pendidikan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmUpdatePendidikan'))?>  
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Perbarui Pendidikan</h2> 
            </div>
            <div class="modal-body"> 
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
               
                        <div class="form-group col-md-12">
                            <label>Nama Pendidikan<span style="color: red">*</span></label>
                            <input type="text" name="upd_nama_pendidikan" id="upd_nama_pendidikan" class="form-control" placeholder="Nama Pendidikan">
                            <input type="hidden" name="upd_id_pendidikan" id="upd_id_pendidikan" class="validate">
                        </div>    
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="changePendidikan"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>


<?php $this->load->view('footer');?>
      