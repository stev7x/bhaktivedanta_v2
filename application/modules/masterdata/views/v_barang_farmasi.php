<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
     <!-- Page Content -->
       <div id="page-wrapper">
           <div class="container-fluid">
               <div class="row bg-title">
                   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                       <h4 class="page-title">Master Barang Farmasi</h4> </div>
                   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                       <ol class="breadcrumb">
                           <li><a href="index.html">Master</a></li>
                           <li class="active">Barang Farmasi</li>
                       </ol> 
                   </div>
                   <!-- /.col-lg-12 -->
               </div>
               <!--row -->
               <div class="row">
                   <div class="col-sm-12">
                       <div class="panel panel-info1"> 
                           <div class="panel-heading"> Master Barang Farmasi
                           </div>
                           <div class="panel-wrapper collapse in" aria-expanded="true">
                               <div class="panel-body">

                                           <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_barang_farmasi" data-backdrop="static" data-keyboard="false">Tambah Barang Farmasi</button>
                                           </div>
                                           <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                            <div class="table-responsive">
                                                <table id="table_barang_farmasi_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                                <thead> 
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode Barang</th>
                                                        <th>Nama Barang</th>
                                                        <th>Satuan Barang</th>
                                                        <th>Jenis Barang</th>
                                                        <th>BPJS/NON BPJS</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                    <th>No</th>
                                                    <th>Kode Barang</th>
                                                    <th>Nama Barang</th>
                                                    <th>Satuan Barang</th>
                                                    <th>Jenis Barang</th>
                                                    <th>BPJS/NON BPJS</th>
                                                    <th>Aksi</th>
                                                    </tr>
                                                </tfoot>
                                            </table>    
                                        </div>
                               </div>
                           </div>  
                       </div>
                   </div>
               </div>
               <!--/row -->
               
           </div> 
           <!-- /.container-fluid -->


<!-- Modal  --> 
<div id="modal_add_barang_farmasi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmCreateBarangFarmasi'))?>
            <input type="hidden" name="id_barang">
      <div class="modal-content" style="overflow:auto;height:650px;" > 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Tambah Barang Farmasi</h2> 
           </div>
           <div class="modal-body">
                <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                    <div>
                        <p id="card_message" class=""></p>
                    </div>
                </div>
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="col-md-12">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input type="hidden" name="id_barang">
                       <div class="form-group col-md-12">
                           <label>Kode Barang Farmasi<span style="color: red">*</span></label>
                           <input type="text" name="kode_barang" id="kode_barang" class="form-control" placeholder="Kode Barang Farmasi">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Nama Barang Farmasi<span style="color: red">*</span></label>
                           <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama Barang Farmasi">
                       </div>  
                       <div class="form-group col-md-12">
                                    <label>Sediaan</label>
                                        <select name="sediaan" id="sediaan" class="form-control select2">
                                            <option value="" selected>Pilih Sediaan</option>
                                            <?php
                                                $list_sediaan = $this->Daftar_barang_farmasi_model->get_list_sediaan();
                                                foreach($list_sediaan as $list){
                                                    echo "<option value='".$list->id_sediaan."'>".$list->nama_sediaan."</option>";
                                                }

                                            ?>
                                        </select>
                                </div>
                       <div class="form-group col-md-12">
                           <label>BPJS/NON BPJS<span style="color: red">*</span></label>
                            <select name="is_bpjs" id="is_bpjs" class="form-control">
                                <option value="" disabled selected>Pilih</option>
                                <option value="0">NON BPJS</option>
                                <option value="1">BPJS</option>
                            </select>
                       </div>
                       <div class="form-group col-md-12">
                           <label>Jenis Barang Farmasi<span style="color: red">*</span></label>
                            <select name="id_jenis_barang" id="id_jenis_barang" class="form-control" onchange="pilih()">
                                <option value="" disabled selected>Pilih Jenis Farmasi</option>
                                <option value="1">Obat</option>
                                <option value="2">Alkes</option>
                                <option value="3">BHP</option>
                            </select>
                       </div>
                       <div id="obat" style="display:none;">
                            <div class="col-md-6">
                                <!-- <div class="form-group">
                                    <label>Kode Obat</label>
                                    <input type="text" name="kode_obat" id="kode_obat" class="form-control" placeholder="Kode Obat">
                                </div> -->
                                <div class="form-group">
                                    <label>Golongan</label>
                                        <select name="golongan" id="golongan" class="form-control select2">
                                            <option value=""  selected>Pilih Golongan</option>
                                            <?php
                                                $list_golongan = $this->Daftar_barang_farmasi_model->get_list_golongan();
                                                foreach($list_golongan as $list){
                                                    echo "<option value='".$list->id_golongan."'>".$list->nama_golongan."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label>Kategori</label>
                                        <select name="kategori" id="kategori" class="form-control select2" >
                                            <option value=""  selected>Pilih Kategori</option>
                                            <?php
                                                $list_kategori = $this->Daftar_barang_farmasi_model->get_list_kategori();
                                                foreach($list_kategori as $list){
                                                    echo "<option value='".$list->id_kategori."'>".$list->nama_kategori."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label>Kelompok</label>
                                        <select name="kelompok" id="kelompok" class="form-control select2" >
                                            <option value="" selected>Pilih Kelompok</option>
                                            <?php
                                                $list_kelompok = $this->Daftar_barang_farmasi_model->get_list_kelompok();
                                                foreach($list_kelompok as $list){
                                                    var_dump($list);
                                                    echo "<option value='".$list->id_kelompok."'>".$list->nama_kelompok."</option>";
                                                }

                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label>Indikasi</label>
                                        <select name="indikasi" id="indikasi" class="form-control select2" >
                                            <option value="" selected>Pilih Indikasi</option>
                                            <?php
                                                $list_indikasi = $this->Daftar_barang_farmasi_model->get_list_indikasi();
                                                foreach($list_indikasi as $list){
                                                    echo "<option value='".$list->id_indikasi."'>".$list->nama_indikasi."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                                <div class="form-group">
                                    <label>Kontra Indikasi</label>
                                        <select name="kontra_indikasi" id="kontra_indikasi" class="form-control select2" >
                                            <option value="" selected>Pilih Kontra Indikasi</option>
                                            <?php
                                                $list_kontra_indikasi = $this->Daftar_barang_farmasi_model->get_list_kontra_indikasi();
                                                foreach($list_kontra_indikasi as $list){
                                                    echo "<option value='".$list->id_kontra_indikasi."'>".$list->nama_kontra_indikasi."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label>Interaksi</label>
                                        <select name="interaksi" id="interaksi" class="form-control select2">
                                            <option value="" selected>Pilih Interaksi</option>
                                            <?php
                                                $list_interaksi = $this->Daftar_barang_farmasi_model->get_list_interaksi();
                                                foreach($list_interaksi as $list){
                                                    echo "<option value='".$list->id_interaksi."'>".$list->nama_interaksi."</option>";
                                                }

                                            ?>
                                        </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>Efek Samping</label>
                                        <select multiple name="efek_samping[]" id="efek_samping" class="select2" >
                                            <optgroup label="Pilih Efek Samping">
                                            <?php
                                                $list_efek_samping = $this->Daftar_barang_farmasi_model->get_list_efek_samping();
                                                foreach($list_efek_samping as $list){
                                                    echo "<option value='".$list->id_efek_samping."'>".$list->nama_efek_samping."</option>";
                                                }
                                            ?>
                                            </optgroup>
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="row">
                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="saveBarangFarmasi"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>
<div id="modal_edit_barang_farmasi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmUpdateBarangFarmasi'))?>
   <input type="hidden" name="upd_id_barang" id="upd_id_barang">
      <div class="modal-content" style="overflow:auto;"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Barang Farmasi</h2> 
           </div>
           <div class="modal-body">
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
                   <div class="row">
                    <div class="col-md-12">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input type="hidden" name="id_barang">
                       <div class="form-group col-md-12">
                           <label>Kode Barang Farmasi<span style="color: red">*</span></label>
                           <input type="text" name="upd_kode_barang" id="upd_kode_barang" class="form-control" placeholder="Kode Barang Farmasi">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Nama Barang Farmasi<span style="color: red">*</span></label>
                           <input type="text" name="upd_nama_barang" id="upd_nama_barang" class="form-control" placeholder="Nama Barang Farmasi">
                       </div>
                       <div class="form-group col-md-12">
                           <label>BPJS/NON BPJS<span style="color: red">*</span></label>
                            <select name="upd_is_bpjs" id="upd_is_bpjs" class="form-control">
                                <option value="" disabled selected>Pilih</option>
                                <option value="0">NON BPJS</option>
                                <option value="1">BPJS</option>
                            </select>
                       </div>
                       <div class="form-group col-md-12">
                           <label>Jenis Barang Farmasi<span style="color: red">*</span></label>
                            <select name="upd_id_jenis_barang" id="upd_id_jenis_barang" class="form-control" onchange="pilih()">
                                <option value="" disabled selected>Pilih Jenis Farmasi</option>
                                <option value="1">Obat</option>
                                <option value="2">Alkes</option>
                                <option value="3">BHP</option>
                            </select>
                       </div>
                       <div id="upd_obat" style="display:none;">
                            <div class="col-md-6">
                                <!-- <div class="form-group">
                                    <label>Kode Obat</label>
                                    <input type="text" name="upd_kode_obat" id="upd_kode_obat" class="form-control" placeholder="Kode Obat">
                                </div> -->
                                <div class="form-group">
                                    <label>Golongan</label>
                                        <select name="upd_golongan" id="upd_golongan" class="form-control select2">
                                            <option value=""  selected>Pilih Golongan</option>
                                            <?php
                                                $list_golongan = $this->Daftar_barang_farmasi_model->get_list_golongan();
                                                foreach($list_golongan as $list){
                                                    echo "<option value='".$list->id_golongan."'>".$list->nama_golongan."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label>Kategori</label>
                                        <select name="upd_kategori" id="upd_kategori" class="form-control select2" >
                                            <option value=""  selected>Pilih Kategori</option>
                                            <?php
                                                $list_kategori = $this->Daftar_barang_farmasi_model->get_list_kategori();
                                                foreach($list_kategori as $list){
                                                    echo "<option value='".$list->id_kategori."'>".$list->nama_kategori."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label>Kelompok</label>
                                        <select name="upd_kelompok" id="upd_kelompok" class="form-control select2" >
                                            <option value="" selected>Pilih Kelompok</option>
                                            <?php
                                                $list_kelompok = $this->Daftar_barang_farmasi_model->get_list_kelompok();
                                                foreach($list_kelompok as $list){
                                                    var_dump($list);
                                                    echo "<option value='".$list->id_kelompok."'>".$list->nama_kelompok."</option>";
                                                }

                                            ?>
                                        </select>
                                </div>
                                 <div class="form-group">
                                    <label>Indikasi</label>
                                        <select name="upd_indikasi" id="upd_indikasi" class="form-control select2" >
                                            <option value="" selected>Pilih Indikasi</option>
                                            <?php
                                                $list_indikasi = $this->Daftar_barang_farmasi_model->get_list_indikasi();
                                                foreach($list_indikasi as $list){
                                                    echo "<option value='".$list->id_indikasi."'>".$list->nama_indikasi."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                               
                                <div class="form-group">
                                    <label>Kontra Indikasi</label>
                                        <select name="upd_kontra_indikasi" id="upd_kontra_indikasi" class="form-control select2" >
                                            <option value="" selected>Pilih Kontra Indikasi</option>
                                            <?php
                                                $list_kontra_indikasi = $this->Daftar_barang_farmasi_model->get_list_kontra_indikasi();
                                                foreach($list_kontra_indikasi as $list){
                                                    echo "<option value='".$list->id_kontra_indikasi."'>".$list->nama_kontra_indikasi."</option>";
                                                }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label>Interaksi</label>
                                        <select name="upd_interaksi" id="upd_interaksi" class="form-control select2">
                                            <option value="" selected>Pilih Interaksi</option>
                                            <?php
                                                $list_interaksi = $this->Daftar_barang_farmasi_model->get_list_interaksi();
                                                foreach($list_interaksi as $list){
                                                    echo "<option value='".$list->id_interaksi."'>".$list->nama_interaksi."</option>";
                                                }

                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label>Efek Samping</label>
                                        <select multiple name="upd_efek_samping[]" id="upd_efek_samping" class="select2" >
                                            <optgroup label="Pilih Efek Samping">
                                            <?php
                                                $list_efek_samping = $this->Daftar_barang_farmasi_model->get_list_efek_samping();
                                                foreach($list_efek_samping as $list){
                                                    echo "<option value='".$list->id_efek_samping."'>".$list->nama_efek_samping."</option>";
                                                }
                                            ?>
                                            </optgroup>
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="changeBarangFarmasi"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_detail_barang_farmasi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
      <div class="modal-content" style="overflow:auto;"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Detail Barang Farmasi</h2> 
           </div>
           <div class="modal-body">
              <div class="row">
                <div class="col-md-12"> 
                  <div class="row">  
                  <div class="col-md-6">  
                      <!-- <div class="tag-colom">     
                        <span class="col-md-6 m-colom ">Kode Obat <span style="float: right;">:</span></span>
                        <span class="col-md-4" id="det_kode_obat"></span>
                      </div>   -->
                      <div class="tag-colom">
                        <span class="col-md-6 m-colom ">Golongan<span style="float: right;">:</span> </span>
                        <span class="col-md-6" id="det_golongan"></span>
                      </div>   
                      <div class="tag-colom">   
                        <span class="col-md-6 m-colom ">Kategori <span style="float: right;">:</span></span>
                        <span class="col-md-6 " id="det_kategori"></span>
                      </div>   
                      <div class="tag-colom">  
                        <span class="col-md-6 m-colom ">Kelompok <span style="float: right;">:</span></span>
                        <span class="col-md-6" id="det_kelompok"></span>
                      </div>
                      <div class="tag-colom">
                       <span class="col-md-6 m-colom">Indikasi<span style="float: right;">:</span></span>
                        <span class="col-md-6" id="det_indikasi"></span>
                      </div>
                  </div> 
                  <div class="col-md-6 ">
                       
                      <div class="tag-colom">
                        <span class="col-md-6 m-colom">Kontra Indikasi<span style="float: right;">:</span> </span>
                        <span class="col-md-6" id="det_kontra_indikasi"></span>
                      </div> 
                      <div class="tag-colom">  
                        <span class="col-md-6 m-colom">Interaksi<span style="float: right;">:</span> </span>
                        <span class="col-md-6" id="det_interaksi"></span>
                      </div>
                      <div class="tag-colom">  
                        <span class="col-md-6 m-colom">Efek Samping <span style="float: right;">:</span></span>
                        <span class="col-md-6" id="det_efek_samping"> </span>
                      </div>
                  </div>
                </div> 
                </div>   

               
              </div>
           </div>
       </div>
   </div>
</div>












<?php $this->load->view('footer');?>
     