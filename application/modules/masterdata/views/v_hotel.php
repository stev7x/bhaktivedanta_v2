<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
     <!-- Page Content -->
       <div id="page-wrapper">
           <div class="container-fluid">
               <div class="row bg-title">
                   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                       <h4 class="page-title">Master Hotel</h4> </div>
                   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                       <ol class="breadcrumb">
                           <li><a href="index.html">Master</a></li>
                           <li class="active">Hotel</li>
                       </ol> 
                   </div>
                   <!-- /.col-lg-12 -->
               </div>
               <!--row -->
               <div class="row">  
                   <div class="col-sm-12">
                       <div class="panel panel-info1"> 
                           <div class="panel-heading"> Master Hotel
                           </div>
                           <div class="panel-wrapper collapse in" aria-expanded="true">
                               <div class="panel-body">

                                           <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <div class="col-md-6">
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_hotel" data-backdrop="static" data-keyboard="false">Tambah Hotel</button>  
                                               &nbsp;&nbsp;&nbsp;
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_hotel" data-backdrop="static" data-keyboard="false"><i class="fa fa-cloud-upload"></i> Eksport Data</button>  
                                            </div>    
                                            
                                           </div>
                                           <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                          <table id="table_hotel_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                           <thead>  
                                               <tr>
                                                   <th>No</th>
                                                   <th>Nama Hotel</th>
                                                   <th>Alamat</th>                          
                                                   <th>No telp</th> 
                                                   <th>Email </th>
                                                   <th>Pic </th>
                                                   <th>Expired</th>
                                                   <th>Action</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               
                                           </tbody>
                                           <tfoot>
                                               <tr>
                                                   <th>No</th>
                                                   <th>Nama Hotel</th>
                                                   <th>Alamat</th>                          
                                                   <th>No telp</th> 
                                                   <th>Email </th>
                                                   <th>Pic </th>
                                                   <th>Expired</th>
                                                   <th>Action</th>
                                               </tr>
                                           </tfoot>
                                       </table>    
                               </div>
                           </div>  
                       </div>
                   </div>
               </div>
               <!--/row -->
               
           </div> 
           <!-- /.container-fluid -->

<!-- Modal  --> 
<div id="modal_hotel" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmCreateAutoStopObat'))?>
      <div class="modal-content"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Tambah hotel</h2> 
           </div>
           <div class="modal-body"> 
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input type="hidden" name="id_hotel">
                       <div class="form-group col-md-12">
                            <label>Nama<span style="color: red">*</span></label>
                            <input type="text" name="nama_hotel" id="nama_hotel" class="form-control" placeholder="Nama hotel">
                        </div>  
                        <div class="form-group col-md-12">
                            <label>Alamat<span style="color: red">*</span></label>
                            <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat">
                        </div> 
                        <div class="form-group col-md-12">
                            <label>No Kontak<span style="color: red">*</span></label>
                            <input type="text" name="no_telp" id="no_telp" class="form-control" placeholder="No Kontak">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Email<span style="color: red">*</span></label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="PIC">
                        </div>  
                        <div class="form-group col-md-12">
                            <label>PIC<span style="color: red">*</span></label>
                            <input type="text" name="pic" id="pic" class="form-control" placeholder="PIC">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Expired<span style="color: red">*</span></label>
                            <input onkeydown="return false" name="expired" id="expired" type="text" class="form-control datepick" value="" placeholder="mm/dd/yyyy"> 
                        </div>  
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="saveAutoStopObat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_edit_hotel" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmUpdateAutoStopObat'))?>
      <div class="modal-content"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">hotel</h2> 
           </div>
           <div class="modal-body"> 
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input id="upd_id_hotel" type="hidden" name="id_hotel">
                       <div class="form-group col-md-12">
                            <label>Nama<span style="color: red">*</span></label>
                            <input type="text" name="nama_hotel" id="upd_nama_hotel" class="form-control" placeholder="Nama hotel">
                        </div>  
                        <div class="form-group col-md-12">
                            <label>Alamat<span style="color: red">*</span></label>
                            <input type="text" name="alamat" id="upd_alamat" class="form-control" placeholder="Alamat">
                        </div> 
                        <div class="form-group col-md-12">
                            <label>No Telepon<span style="color: red">*</span></label>
                            <input type="text" name="no_telp" id="upd_no_telp" class="form-control" placeholder="No Kontak">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Email<span style="color: red">*</span></label>
                            <input type="text" name="email" id="upd_email" class="form-control" placeholder="Email">
                        </div>  
                        <div class="form-group col-md-12">
                            <label>PIC<span style="color: red">*</span></label>
                            <input type="text" name="pic" id="upd_pic" class="form-control" placeholder="PIC">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Expired<span style="color: red">*</span></label>
                            <input onkeydown="return false" name="expired" id="upd_expired" type="text" class="form-control datepick" value="" placeholder="mm/dd/yyyy"> 
                        </div>   
                       
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="changeAutoStopObat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>




<?php $this->load->view('footer');?>