<?php $this->load->view('header');?>
<title>SIMRS | Master Poli Ruangan</title>
<?php $this->load->view('sidebar');?>
      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Master Poli Ruangan</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Master</a></li>
                    <li class="active">Poli Ruangan</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        <!--start container-->
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <p class="right-align"><a class="btn waves-effect waves-light indigo modal-trigger" href="#modal_add_poli_ruangan">Tambah Poli Ruangan</a></p>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="divider"></div>
                            <h4 class="header">Master Poli Ruangan</h4>
                            <div id="card-alert" class="card green notif" style="display:none;">
                                <div class="card-content white-text">
                                    <p id="card_message">SUCCESS : The page has been added.</p>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div id="table-datatables">
                                <div class="row">
                                    <div class="col s12 m4 l12">
                                        <table id="table_poli_ruangan_list" class="responsive-table display" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Instalasi</th>
                                                    <th>Nama Poli Ruangan</th>
                                                    <th>Type Ruangan</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Instalasi</th>
                                                    <th>Nama Poli Ruangan</th>
                                                    <th>Type Ruangan</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
    <!-- END CONTENT -->
    
    <!-- Start Modal Add Poli Ruangan -->
    <div id="modal_add_poli_ruangan" class="modal">
        <?php echo form_open('#',array('id' => 'fmCreatePoli_ruangan'))?>
        <div class="modal-content">
            <h1>Tambah Poli Ruangan</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="select_instalasi" name="select_instalasi">
                                <option value="" disabled selected>Pilih Instalasi</option>
                                <?php
                                $list_poli_ruangan = $this->Poli_ruangan_model->get_instalasi_list();
                                foreach($list_poli_ruangan as $list){
                                    echo "<option value=".$list->instalasi_id.">".$list->instalasi_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Instalasi<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="nama_poli_ruangan" name="nama_poli_ruangan" class="validate">
                            <label for="nama_lengkap">Nama Poli Ruangan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="select_ruangan" name="select_ruangan">
                                <option value="" disabled selected>Pilih Type Ruangan</option>
                                <?php
                                foreach($data_ruangan as $id=>$value){
                                    echo "<option value=".$id.">".$value."</option>";
                                }
                                ?>
                            </select>
                            <label>Type Ruangan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="savePoli_ruangan">Save<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- End Modal Add Poli Ruangan -->
     <!-- Modal Update Poli Ruangan-->
     <div id="modal_update_poli_ruangan" class="modal fadeIn" tabindex="-1" role="dialog">
        <?php echo form_open('#',array('id' => 'fmUpdatePoli_ruangan'))?>
        <div class="modal-content">
            <h1>Update Poli Ruangan</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green upd_modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="upd_modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="upd_select_instalasi" name="upd_select_instalasi">
                                <option value="" disabled selected>Pilih Instalasi</option>
                                <?php
                                $list_poli_ruangan = $this->Poli_ruangan_model->get_instalasi_list();
                                foreach($list_poli_ruangan as $list){
                                    echo "<option value=".$list->instalasi_id.">".$list->instalasi_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Instalasi<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="upd_nama_poli_ruangan" name="upd_nama_poli_ruangan" class="validate" value="">
                            <label for="upd_nama_poli_ruangan" id="label_upd__nama_poli_ruangan">Nama Poli Ruangan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="upd_select_ruangan" name="upd_select_ruangan">
                                <option value="" disabled selected>Pilih Type Ruangan</option>
                                <?php
                                foreach($data_ruangan as $idupd=>$valueupd){
                                    echo "<option value=".$idupd." $select>".$valueupd."</option>";
                                }
                                ?>
                            </select>
                            <label>Type Ruangan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="upd_id_poli_ruangan" name="upd_id_poli_ruangan" class="validate">
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="changePoli_ruangan">Save Change<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
<?php $this->load->view('footer');?>
      