<?php $this->load->view('header');?>
<title>SIMRS | Master Paket Operasi</title>
<?php $this->load->view('sidebar');?>
<!-- START CONTENT -->
<section id="content">
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h5 class="breadcrumbs-title">Master Paket Operasi</h5>
                    <ol class="breadcrumbs">
                        <li><a href="#">Master</a></li>
                        <li class="active">Paket Operasi</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
    <!--start container-->
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <p class="right-align"><a class="btn waves-effect waves-light indigo modal-trigger" href="#modal_add_paket_operasi">Tambah Paket Operasi</a></p>
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="divider"></div>
                        <h4 class="header">Master Paket Operasi</h4>
                        <div id="card-alert" class="card green notif" style="display:none;">
                            <div class="card-content white-text">
                                <p id="card_message">SUCCESS : The page has been added.</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div id="table-datatables">
                            <div class="row">
                                <div class="col s12 m4 l12">
                                    <table id="table_paket_operasi_list" class="responsive-table display" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Paket</th>
                                                <th>Total Harga</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Jumlah Hari Rawat</th>
                                                <th>Type Pembayaran</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Paket</th>
                                                <th>Total Harga</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Jumlah Hari Rawat</th>
                                                <th>Type Pembayaran</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div> 
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
    
<!-- Start Modal Add Paket -->
<div id="modal_add_paket_operasi" class="modal">
    <?php echo form_open('#',array('id' => 'fmCreatePaketOperasi'))?>
    <div class="modal-content">
        <h1>Tambah Paket Operasi</h1>
        <div class="divider"></div>
        <div id="card-alert" class="card green modal_notif" style="display:none;">
            <div class="card-content white-text">
                <p id="modal_card_message"></p>
            </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <!--jqueryvalidation-->
        <div id="jqueryvalidation" class="section">
            <div class="col s12 formValidate">
                <span style="color: red;"> *) wajib diisi</span>
                <div class="row">
                    <div class="input-field col s10">
                        <input type="text" id="nama_paket" name="nama_paket" class="validate">
                        <label for="nama_lengkap">Nama Paket Operasi<span style="color: red;"> *</span></label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s10">
                        <input type="text" id="jml_hari" name="jml_hari" class="validate" onkeypress="return numbersOnly(event);">
                        <label for="jml_hari">Jumlah Hari<span style="color: red;"> *</span></label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s10">
                        <select id="kelaspelayanan" name="kelaspelayanan">
                            <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                            <?php
                            $list_kelas = $this->Paket_operasi_model->get_pelayanan_list();
                            foreach($list_kelas as $list){
                                echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                            }
                            ?>
                        </select>
                        <label>Kelas Pelayanan<span style="color: red;"> *</span></label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s10">
                        <select id="is_bpjs" name="is_bpjs">
                            <option value="" disabled selected>Pilih Pembayaran</option>
                            <option value="1">BPJS</option>
                            <option value="0">Non BPJS</option>
                        </select>
                        <label>Pembayaran<span style="color: red;"> *</span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
        <button type="button" class="btn waves-effect waves-light teal modal-action" id="savePaketOperasi">Save<i class="fa fa-floppy-o"></i></button>
    </div>
    <?php echo form_close(); ?>
</div>
<!-- End Modal Add Paket -->
<!-- Modal Update Paket-->
<div id="modal_update_paket_operasi" class="modal fadeIn" tabindex="-1" role="dialog" style="width: 80% !important ; max-height: 90% !important ;">
    
    <div class="modal-content">
        <h1>Update Paket Operasi</h1>
        <div class="divider"></div>
        <div id="card-alert" class="card green upd_modal_notif" style="display:none;">
            <div class="card-content white-text">
                <p id="upd_modal_card_message"></p>
            </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card-panel">
                    <?php echo form_open('#',array('id' => 'fmUpdatePaketOperasi'))?>
                    <div class="row">
                        <div class="col s6">
                            <div class="row">
                                <div class="col s12 formValidate">
                                <span style="color: red;"> *) wajib diisi</span>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="hidden" id="upd_id_paket" name="upd_id_paket" class="validate" readonly="true">
                                        <input type="text" id="upd_nama_paket" name="upd_nama_paket" class="validate" value="">
                                        <label for="upd_nama_paket" id="label_upd_nama_paket">Nama Paket Operasi<span style="color: red;"> *</span></label>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col s6">
                            <div class="row">
                                <div class="col s12 formValidate">
                                    <span style="color: red;"> &nbsp;</span>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <select id="upd_kelaspelayanan" name="upd_kelaspelayanan" disabled>
                                                <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                                                <?php
                                                $list_kelas_upd = $this->Paket_operasi_model->get_pelayanan_list();
                                                foreach($list_kelas_upd as $list){
                                                    echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                                }
                                                ?>
                                            </select>
                                            <label>Kelas Pelayanan<span style="color: red;"> *</span></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s6">
                            <div class="row">
                                <div class="col s12 formValidate">
                                    <span style="color: red;"> &nbsp;</span>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <select id="upd_bpjs" name="upd_bpjs" disabled>
                                                <option value="">Pilih Jenis Pembayaran</option>
                                                <option value="1">BPJS</option>
                                                <option value="0">Non BPJS</option>
                                            </select>
                                            <label>Jenis Pembayaran<span style="color: red;"> *</span></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs tab-demo z-depth-1">
                                <li class="tab col s4"><a class="active" href="#tindakan_tab">Tindakan</a>
                                </li>
                                <li class="tab col s4"><a class="active" href="#obat_bhp">Obat BHP</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col s12">
                            <div id="tindakan_tab" class="col s12">
                                <div>&nbsp;</div>
                                <div id="table-datatables">
                                    <div class="row">
                                        <div class="col s12 m4 l12">
                                            <table id="table_tindakan_paket" class="responsive-table display" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Hari ke</th>
                                                        <th>Nama Tindakan</th>
                                                        <th>Tarif Tindakan</th>
                                                        <th>Jumlah</th>
                                                        <th>Subsidi</th>
                                                        <th>Total Harga</th>
                                                        <th>Jenis Tindakan</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="8">No Data to Display</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div>&nbsp;</div>
                                    <div class="row">
                                        <h5>Input Tindakan</h5>
                                        <div class="divider"></div>
                                        <div>&nbsp;</div>
                                        <div class="col s12">
                                            <?php echo form_open('#',array('id' => 'fmCreateTindakanPaket'))?>
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                                            <div class="row">
                                                <div class="col s4">
                                                    <label>Hari Ke- <span style="color: red;"> *</span></label>
                                                    <select id="harike" name="harike" class="validate browser-default">
                                                        <option value="" disabled selected>Pilih Hari</option>
                                                    </select>
                                                </div>
                                                <div class="col s4">
                                                    <label>Tindakan <span style="color: red;"> *</span></label>
                                                    <select id="tindakan" name="tindakan" class="validate browser-default" onchange="getTarifTindakan()">
                                                        <option value="" disabled selected>Pilih Tindakan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s4">
                                                    <input id="harga_tindakan" type="text" name="harga_tindakan" class="validate" value="" placeholder="Harga Tindakan" readonly="true">
                                                    <label for="harga_tindakan" class="active">Harga Tindakan <span style="color: red;"> *</span></label>
                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="subsidi" type="text" name="subsidi" class="validate" value="" placeholder="Subsidi" onkeypress="return numbersOnly(event);">
                                                    <label for="subsidi" class="active">Subsidi <span style="color: red;"> *</span></label>
                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="jml_tindakan" type="text" name="jml_tindakan" class="validate" value="" onkeypress="return numbersOnly(event);" onkeyup="hitungHarga()">
                                                    <label for="jml_tindakan" class="active">Jumlah Tindakan <span style="color: red;"> *</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s4">
                                                    <select id="jenistindakan" name="jenistindakan" class="validate">
                                                        <option value="" disabled selected>Pilih Jenis Tindakan</option>
                                                        <option value="1">Umum</option>
                                                        <option value="2">Radiologi</option>
                                                        <option value="3">Laboratorium</option>
                                                    </select>
                                                    <label>Jenis Tindakan <span style="color: red;"> *</span></label>
                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="totalharga" type="text" name="totalharga" class="validate" value="" placeholder="Sub Total" readonly>
                                                    <label for="totalharga" class="active">Total Harga <span style="color: red;"> *</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s12 m8 l9">
                                                    <button class="btn light-green waves-effect waves-light darken-4" type="button" id="saveTindakanPaket">
                                                        <i class="mdi-navigation-check left"></i>Simpan
                                                    </button>
                                                </div>
                                            </div>
                                            <?php echo form_close()?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="obat_bhp" class="col s12">
                                <div>&nbsp;</div>
                                <div id="table-datatables">
                                    <div class="row">
                                        <div class="col s12 m4 l12">
                                            <table id="table_obat_paket" class="responsive-table display" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Hari ke</th>
                                                        <th>Jenis Obat</th>
                                                        <th>Nama Obat</th>
                                                        <th>Harga Satuan</th>
                                                        <th>Subsidi</th>
                                                        <th>Jumlah</th>
                                                        <th>Total Harga</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="8">No Data to Display</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div>&nbsp;</div>
                                    <div class="row">
                                        <h5>Input Obat</h5>
                                        <div class="divider"></div>
                                        <div>&nbsp;</div>
                                        <div class="col s12">
                                            <?php echo form_open('#',array('id' => 'fmCreateObatPaket'))?>
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delobat" name="<?php echo $this->security->get_csrf_token_name()?>_delobat" value="<?php echo $this->security->get_csrf_hash()?>" />
                                            <div class="row">
                                                <div class="col s4">
                                                    <label>Hari Ke- <span style="color: red;"> *</span></label>
                                                    <select id="harike_obat" name="harike_obat" class="validate browser-default">
                                                        <option value="" disabled selected>Pilih Hari</option>
                                                    </select>
                                                </div>
                                                <div class="col s4">
                                                    <label>Jenis Obat <span style="color: red;"> *</span></label>
                                                    <select id="jenisobat" name="jenisobat" class="validate browser-default" onchange="getObat()">
                                                        <option value="" disabled selected>Pilih Jenis Obat</option>
                                                        <option value="1">Obat</option>
                                                        <option value="2">BHP</option>
                                                        <option value="3">Alkes</option>
                                                    </select>
                                                </div>
                                                <div class="col s4">
                                                    <label>Obat<span style="color: red;"> *</span></label>
                                                    <select id="obat_id" name="obat_id" class="validate browser-default" onchange="getHargaObat()">
                                                        <option value="" disabled selected>Pilih Obat</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s4">
                                                    <input id="harga_satuan" type="text" name="harga_satuan" class="validate" value="" placeholder="Harga Satuan" readonly="true">
                                                    <label for="harga_satuan" class="active">Harga Satuan <span style="color: red;"> *</span></label>
                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="subsidi_obat" type="text" name="subsidi_obat" class="validate" value="" placeholder="Subsidi" onkeypress="return numbersOnly(event);" onkeyup="hitungHargaObat()">
                                                    <label for="subsidi_obat" class="active">Subsidi <span style="color: red;"> *</span></label>
                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="jml_obat" type="text" name="jml_obat" class="validate" value="" onkeypress="return numbersOnly(event);" onkeyup="hitungHargaObat()">
                                                    <label for="jml_obat" class="active">Jumlah Obat<span style="color: red;"> *</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s4">
                                                    <input id="totalhargaobat" type="text" name="totalhargaobat" class="validate" value="" placeholder="Total Harga" readonly>
                                                    <label for="totalhargaobat" class="active">Total Harga <span style="color: red;"> *</span></label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s12 m8 l9">
                                                    <button class="btn light-green waves-effect waves-light darken-4" type="button" id="saveObatPaket">
                                                        <i class="mdi-navigation-check left"></i>Simpan
                                                    </button>
                                                </div>
                                            </div>
                                            <?php echo form_close()?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
        <button type="button" class="btn waves-effect waves-light teal modal-action" id="changePaketOperasi">Save Change<i class="mdi-content-save left"></i></button>
    </div>
</div>
<?php $this->load->view('footer');?>
      