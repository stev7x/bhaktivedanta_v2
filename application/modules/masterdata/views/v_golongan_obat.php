<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
     <!-- Page Content -->
       <div id="page-wrapper">
           <div class="container-fluid">
               <div class="row bg-title">
                   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                       <h4 class="page-title">Master Golongan Obat</h4> </div>
                   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                       <ol class="breadcrumb">
                           <li><a href="index.html">Master</a></li>
                           <li class="active">Golongan Obat</li>
                       </ol> 
                   </div>
                   <!-- /.col-lg-12 -->
               </div>
               <!--row -->
               <div class="row">
                   <div class="col-sm-12">
                       <div class="panel panel-info1"> 
                           <div class="panel-heading"> Master Golongan Obat
                           </div>
                           <div class="panel-wrapper collapse in" aria-expanded="true">
                               <div class="panel-body">

                                           <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_golongan_obat" data-backdrop="static" data-keyboard="false">Tambah Golongan Obat</button>
                                           </div>
                                           <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                          <table id="table_golongan_obat_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                           <thead> 
                                               <tr>
                                                   <th>No</th>
                                                   <th>Kode Golongan</th>
                                                   <th>Nama Golongan</th>
                                                   <th>Kelas Terapi</th>
                                                   <th>Keterangan</th>
                                                   <th>Action</th> 
                                               </tr>
                                           </thead>
                                           <tbody>
                                               
                                           </tbody>
                                           <tfoot>
                                               <tr>
                                                    <th>No</th>
                                                    <th>Kode Golongan</th>
                                                    <th>Nama Golongan</th>
                                                    <th>Kelas Terapi</th>
                                                    <th>Keterangan</th>
                                                    <th>Action</th>
                                               </tr>
                                           </tfoot>
                                       </table>    
                               </div>
                           </div>  
                       </div>
                   </div>
               </div>
               <!--/row -->
               
           </div> 
           <!-- /.container-fluid -->


<!-- Modal  --> 
<div id="modal_add_golongan_obat" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmCreateGolonganObat'))?>
      <div class="modal-content"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Tambah Golongan Obat</h2> 
           </div>
           <div class="modal-body">
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input type="hidden" name="id_golongan">
                       <div class="form-group col-md-12">
                           <label>Kode Golongan<span style="color: red">*</span></label>
                           <input type="text" name="kode_golongan" id="kode_golongan" class="form-control" placeholder="Kode Golongan">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Nama Kelas Terapi<span style="color: red">*</span></label>
                           <input type="text" name="nama_golongan" id="nama_golongan" class="form-control" placeholder="Nama Golongan">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Kelas Terapi<span style="color: red">*</span></label>
                           <select id="id_kelas_terapi" name="id_kelas_terapi" class="form-control">
                                <option value="" disabled selected>Pilih Kelas</option>
                                <?php
                                $list_kelas_terapi = $this->Golongan_obat_model->get_kelas_terapi_list();
                                foreach($list_kelas_terapi as $list){
                                    echo "<option value=".$list->id_kelas_terapi.">".$list->nama_kelas_terapi."</option>";
                                }
                                ?>
                            </select>                           
                       </div>  
                       <div class="col-md-12">
                            <label for="keterangan">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                       </div>
                       
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="saveGolonganObat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_edit_golongan_obat" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmUpdateGolonganObat'))?>
      <div class="modal-content"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Golongan Obat</h2> 
           </div>
           <div class="modal-body">
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input type="hidden" name="upd_id_golongan" id="upd_id_golongan">
                       <div class="form-group col-md-12">
                           <label>Kode Golongan<span style="color: red">*</span></label>
                           <input type="text" name="upd_kode_golongan" id="upd_kode_golongan" class="form-control" placeholder="Kode Golongan">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Nama Kelas Terapi<span style="color: red">*</span></label>
                           <input type="text" name="upd_nama_golongan" id="upd_nama_golongan" class="form-control" placeholder="Nama Golongan">
                       </div>  
                       <div class="form-group col-md-12">
                           <label>Kelas Terapi<span style="color: red">*</span></label>
                           <select id="upd_id_kelas_terapi" name="upd_id_kelas_terapi" class="form-control">
                                <option value="" disabled selected>Pilih Kelas</option>
                                <?php
                                $list_kelas_terapi = $this->Golongan_obat_model->get_kelas_terapi_list();
                                foreach($list_kelas_terapi as $list){
                                    echo "<option value=".$list->id_kelas_terapi.">".$list->nama_kelas_terapi."</option>";
                                }
                                ?>
                            </select>                           
                       </div>  
                       <div class="col-md-12">
                            <label for="keterangan">Keterangan</label>
                            <textarea name="upd_keterangan" id="upd_keterangan" class="form-control" placeholder="Keterangan"></textarea>
                       </div>
                       
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="changeGolonganObat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>





<?php $this->load->view('footer');?>
     