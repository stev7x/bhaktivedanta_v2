<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Dokter Ruangan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Kelas Dokter Ruangan</li>
                        </ol>   
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Master Dokter Ruangan  
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                   <div class="panel-body">
                                        <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <div class="col-md-6">
                                                <button  type="button" class="btn btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="add_dokter_ruangan()">Tambah Dokter Ruangan</button>      
                                                &nbsp;&nbsp;&nbsp;
                                                <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Dokter Ruangan</button> 
                                            </div>
                                            <!-- <div class="col-md-6">   
                                                <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>
 
                                                    &nbsp;&nbsp;&nbsp;  
                                                   <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />  
                                                    <label for="file" style="float: right;"><span></span> <strong>     
                                                       <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                   <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                         
                                                <?php echo form_close(); ?>  
                                            </div>   -->

                                        </div>        
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />   
                                       <table id="table_dokter_ruangan_list" class="table table-striped dataTable" cellspacing="0">      
                                       <thead>
                                            <tr> 
                                                <th>No</th>
                                                <th>Nama Dokter</th>
                                                <th>Poli Ruangan</th>
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- modal -->    
<div id="modal_add_dokter_ruangan" class="modal fade"> 
    <div class="modal-dialog modal-lg">  
       <?php echo form_open('#',array('id' => 'fmCreatedokter_ruangan'))?> 
       <div class="modal-content" style="margin-top: 100px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Dokter Ruangan</h2> 
            </div>     
            <div class="modal-body">
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Dokter<span style="color: red">*</span></label> 
                        <select id="select_dokter" name="select_dokter" class="form-control select2">
                            <option value="" selected>Pilih Dokter</option>
                            <?php
                            $list_dokter_ruangan = $this->Dokter_ruangan_model->get_dokter_list();
                            foreach($list_dokter_ruangan as $list){
                                echo "<option value=".$list->id_M_DOKTER.">".$list->NAME_DOKTER."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Ruangan<span style="color: red">*</span></label> 
                        <select id="select_ruangan" name="select_ruangan" class="form-control select2">
                            <option value=""  selected>Pilih Ruangan</option>       
                            <?php
                            $list_ruangan = $this->Dokter_ruangan_model->get_ruangan_list();
                            foreach($list_ruangan as $list){
                                echo "<option value=".$list->poliruangan_id.">".$list->nama_poliruangan."</option>";
                            }
                            ?>
                        </select>       
                    </div>   
                </div> 

                <div class="row"> 
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="savedokter_ruangan"><i class="fa fa-floppy-o p-r-10"></i> SIMPAN</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>


<div id="modal_update_dokter_ruangan" class="modal fade"> 
    <div class="modal-dialog modal-lg">  
       <?php echo form_open('#',array('id' => 'fmUpdatedokter_ruangan'))?> 
       <div class="modal-content" style="margin-top: 100px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Edit Dokter Ruangan</h2> 
            </div>     
            <div class="modal-body">
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <!-- <input type="hidden" name="select_dokter" id="select_dokter"> -->
                        <label>Dokter<span style="color: red">*</span></label>           
                        <select readonly id="upd_select_dokter" name="upd_select_dokter" class="form-control" disabled > 
                            <!-- <option value="" disabled selected>Pilih Dokter</option> -->
                            <?php
                            $list_dokter_ruangan = $this->Dokter_ruangan_model->get_dokter_list();
                            foreach($list_dokter_ruangan as $list){
                                echo "<option value=".$list->id_M_DOKTER.">".$list->NAME_DOKTER."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Ruangan<span style="color: red">*</span></label> 
                        <select id="upd_select_ruangan" name="upd_select_ruangan" class="form-control">
                            <!-- <option value="" disabled selected>Pilih Ruangan</option> -->
                            <?php
                            $list_ruangan = $this->Dokter_ruangan_model->get_ruangan_list();
                            foreach($list_ruangan as $list){
                                echo "<option value=".$list->poliruangan_id.">".$list->nama_poliruangan."</option>";
                            }
                            ?>
                        </select>       
                    </div>   
                </div> 
                <div class="row">
                <input type="hidden" id="temp_dokterruangan_id" name="temp_dokterruangan_id" class="validate">
                <input type="hidden" id="temp_ruangan_id" name="temp_ruangan_id" class="validate">
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="changedokter_ruangan"><i class="fa fa-floppy-o p-r-10"></i> SIMPAN</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
 


<?php $this->load->view('footer');?>
      