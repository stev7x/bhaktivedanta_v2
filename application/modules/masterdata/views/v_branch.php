<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
     <!-- Page Content -->
       <div id="page-wrapper">
           <div class="container-fluid">
               <div class="row bg-title">
                   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                       <h4 class="page-title">Master Branch</h4> </div>
                   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                       <ol class="breadcrumb">
                           <li><a href="index.html">Master</a></li>
                           <li class="active">Branch</li>
                       </ol> 
                   </div>
                   <!-- /.col-lg-12 -->
               </div>
               <!--row -->
               <div class="row">  
                   <div class="col-sm-12">
                       <div class="panel panel-info1"> 
                           <div class="panel-heading"> Master Branch
                           </div>
                           <div class="panel-wrapper collapse in" aria-expanded="true">
                               <div class="panel-body">

                                           <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <div class="col-md-6">
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_branch" data-backdrop="static" data-keyboard="false">Tambah Branch</button>  
                                               &nbsp;&nbsp;&nbsp;
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_branch" data-backdrop="static" data-keyboard="false"><i class="fa fa-cloud-upload"></i> Eksport Data</button>  
                                            </div>    
                                            <!-- <div class="col-md-6">
                                                &nbsp;&nbsp;&nbsp;
                                               <input type="file" name="file-7[]" id="file-7"   class="inputfile inputfile-6 " data-multiple-caption="{count} files selected" multiple />  
                                                <label for="file-7" style="float: right;"><span></span> <strong>     
                                                   <i class="fa fa-cloud-download p-r-10"></i> Choose a file&hellip;</strong></label><br><br><br>    
                                                   <input style="float: right;" type="submit" value="Import" class="btn btn-info btn-import" /> 
                                               <button style="float: right;"  type="button" class="btn btn-info btn-import" data-toggle="modal" data-target="#modal_branch"><i class="fa fa-sign-in p-r-10 "></i> Import Data</button>  
                                            </div>  -->
                                           </div>
                                           <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                          <table id="table_branch_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                           <thead>  
                                               <tr>
                                                   <th>No</th>
                                                   <th>Nama Branch</th>
                                                   <th>Alamat</th>
                                                   <th>Pic </th>
                                                   <th>No telp</th> 
                                                   <th>Action</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               
                                           </tbody>
                                           <tfoot>
                                               <tr>
                                                    <th>No</th>
                                                   <th>Nama Branch</th>
                                                   <th>Alamat</th>
                                                   <th>Pic </th>
                                                   <th>No telp</th> 
                                                   <th>Action</th>
                                               </tr>
                                           </tfoot>
                                       </table>    
                               </div>
                           </div>  
                       </div>
                   </div>
               </div>
               <!--/row -->
               
           </div> 
           <!-- /.container-fluid -->

<!-- Modal  --> 
<div id="modal_branch" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmCreateAutoStopObat'))?>
      <div class="modal-content"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Tambah Branch</h2> 
           </div>
           <div class="modal-body"> 
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input type="hidden" name="id_branch">
                       <div class="form-group col-md-12">
                            <label>Nama<span style="color: red">*</span></label>
                            <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Branch">
                        </div>  
                        <div class="form-group col-md-12">
                            <label>Alamat<span style="color: red">*</span></label>
                            <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat">
                        </div>   
                       <div class="form-group col-md-12">
                            <label>PIC<span style="color: red">*</span></label>
                            <input type="text" name="pic" id="pic" class="form-control" placeholder="PIC">
                        </div> 
                       <div class="form-group col-md-12">
                            <label>No Kontak<span style="color: red">*</span></label>
                            <input type="text" name="no_kontak" id="no_kontak" class="form-control" placeholder="No Kontak">
                        </div> 
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="saveAutoStopObat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_edit_branch" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog modal-lg">
   <?php echo form_open('#',array('id' => 'fmUpdateAutoStopObat'))?>
      <div class="modal-content"> 
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <h2 class="modal-title" id="myLargeModalLabel">Branch</h2> 
           </div>
           <div class="modal-body"> 
           <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                   <span style="color: red;">* Wajib diisi</span><br>
               <div class="row">
                       <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                       <input id="upd_id_branch" type="hidden" name="id_branch">
                       <div class="form-group col-md-12">
                            <label>Nama<span style="color: red">*</span></label>
                            <input type="text" name="nama" id="upd_nama" class="form-control" placeholder="Nama Branch">
                        </div>  
                        <div class="form-group col-md-12">
                            <label>Alamat<span style="color: red">*</span></label>
                            <input type="text" name="alamat" id="upd_alamat" class="form-control" placeholder="Alamat">
                        </div>   
                       <div class="form-group col-md-12">
                            <label>PIC<span style="color: red">*</span></label>
                            <input type="text" name="pic" id="upd_pic" class="form-control" placeholder="PIC">
                        </div> 
                       <div class="form-group col-md-12">
                            <label>No Kontak<span style="color: red">*</span></label>
                            <input type="text" name="no_kontak" id="upd_no_kontak" class="form-control" placeholder="No Kontak">
                        </div> 
                       
               </div>
               <div class="row">

                   <div class="col-md-12"> 
                   <br><br>
                       <button type="button" class="btn btn-success pull-right" id="changeAutoStopObat"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                   </div>
               </div> 
           </div>
       </div>
       <?php echo form_close(); ?>
       <!-- /.modal-content -->
   </div>
    <!-- /.modal-dialog -->
</div>




<?php $this->load->view('footer');?>

     