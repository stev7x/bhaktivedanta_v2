<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
    <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Master Batch</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Master</a></li>
                    <li class="active">Batch</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Master Batch
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> TAMBAH</button>
                                </div>
                            </div>
                            <table id="table_batch_list" class="table table-striped dataTable" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Batch Number</th>
                                    <th>Tanggal Kadaluarsa</th>
                                    <th>Nama Obat</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Batch Number</th>
                                    <th>Tanggal Kadaluarsa</th>
                                    <th>Nama Obat</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->

    </div>
    <!-- /.container-fluid -->


    <!-- Modal  -->
    <div id="modal_add" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmCreateBatch'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Tambah batch</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <span style="color: red;">* Wajib diisi</span><br>
                                <div class="form-group">
                                    <label>Batch Number <span style="color: red">*</span></label>
                                    <input type="text" name="number" id="number" class="form-control" placeholder="Batch Number">
                                </div>
                                <div class="form-group">
                                    <label for="exp_date" class="control-label">Tanggal Kadaluarsa <span style="color: red;" id="span_tgl" >*</span></label>
                                    <div class="input-group">
                                        <input name="exp_date" id="exp_date" type="text" class="form-control datepick" placeholder="mm/dd/yyyy" value=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Nama Obat <span style="color: red;">*</span></label>
                                    <select class="form-control" name="obat_id" id="obat_id">
                                        <option selected value="">Pilih Obat</option>
                                        <?php
                                            foreach($obat as $key => $value){
                                                echo "<option value=".$value['id'].">".$value['nama']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="saveBatch"><i class="fa fa-floppy-o p-r-10"></i>
                                SIMPAN</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal  -->
    <div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmUpdateBatch'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="edit_modal_label"Edit batch</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <span style="color: red;">* Wajib diisi</span><br>
                                <div class="form-group">
                                    <label for="edit_number">Batch Number <span style="color: red">*</span></label>
                                    <input type="text" name="number" id="edit_number" class="form-control" placeholder="Batch Number">
                                    <input type="hidden" name="id" id="edit_id">
                                </div>
                                <div class="form-group">
                                    <label for="edit_exp_date" class="control-label">Tanggal Kadaluarsa <span style="color: red;" id="span_tgl" >*</span></label>
                                    <div class="input-group">
                                        <input name="exp_date" id="edit_exp_date" type="text" class="form-control datepick" placeholder="mm/dd/yyyy" value=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit_obat_id">Nama Obat <span style="color: red;">*</span></label>
                                    <select class="form-control" name="obat_id" id="edit_obat_id">
                                        <option  selected>Pilih Obat</option>
                                        <?php
                                        foreach($obat as $key => $value){
                                            echo "<option value=".$value['id'].">".$value['nama']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="updateBatch"><i class="fa fa-floppy-o p-r-10"></i>
                                SIMPAN</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

<?php $this->load->view('footer');?>