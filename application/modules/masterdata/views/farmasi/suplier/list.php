<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
    <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Master Suplier</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Master</a></li>
                    <li class="active">Suplier</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Master Suplier
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> TAMBAH</button>
                                </div>
                            </div>
                            <table id="table_suplier_list" class="table table-striped dataTable" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Suplier</th>
                                    <th>Alamat</th>
                                    <th>NPWP</th>
                                    <th>Nama Marketing</th>
                                    <th>Telepon Marketing</th>
                                    <th>Perjanjian</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Suplier</th>
                                    <th>Alamat</th>
                                    <th>NPWP</th>
                                    <th>Nama Marketing</th>
                                    <th>Telepon Marketing</th>
                                    <th>Perjanjian</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->

    </div>
    <!-- /.container-fluid -->


    <!-- Modal  -->
    <div id="modal_add" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmCreateSuplier'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Tambah suplier</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <span style="color: red;">* Wajib diisi</span><br>
                                <div class="form-group">
                                    <label>Nama Suplier <span style="color: red">*</span></label>
                                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama suplier">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea class="form-control" id="alamat" name="alamat" placeholder="Alamat"> </textarea>
                                </div>
                                <div class="form-group">
                                    <label>Nomor NPWP</label>
                                    <input type="text"id="npwp" name="npwp" class="form-control" placeholder="NPWP">
                                </div>
                                <div class="form-group">
                                    <label>Nama Marketing</label>
                                    <input type="text" name="nama_marketing" id="nama_marketing" class="form-control" placeholder="Nama Marketing">
                                </div>
                                <div class="form-group">
                                    <label>Nomor Telepon Marketing</label>
                                    <input type="text" maxlength="12" name="no_telp_marketing" id="no_telp_marketing" class="form-control" placeholder="No Telepon Marketing">
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_perjanjian_habis" class="control-label">Tanggal Perjanjian Habis<span style="color: red;" id="span_tgl" >*</span></label>
                                    <div class="input-group">
                                        <input name="tanggal_perjanjian_habis" id="tanggal_perjanjian_habis" type="text" class="form-control datepick" placeholder="dd/mm/yyyy" value=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="saveSuplier"><i class="fa fa-floppy-o p-r-10"></i>
                                SIMPAN</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal  -->
    <div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmUpdateSuplier'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="edit_modal_label"Edit suplier</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <span style="color: red;">* Wajib diisi</span><br>
                                <div class="form-group">
                                    <label>Nama Suplier <span style="color: red">*</span></label>
                                    <input type="text" name="nama" id="edit_nama" class="form-control" placeholder="Nama suplier">
                                    <input type="hidden" name="id" id="edit_id">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea class="form-control" id="edit_alamat" name="alamat" placeholder="Alamat"> </textarea>
                                </div>
                                <div class="form-group">
                                    <label>Nomor NPWP</label>
                                    <input type="text" id="edit_npwp" name="npwp" class="form-control" placeholder="NPWP">
                                </div>
                                <div class="form-group">
                                    <label>Nama Marketing</label>
                                    <input type="text" id="edit_nama_marketing" name="nama_marketing" class="form-control" placeholder="Nama Marketing">
                                </div>
                                <div class="form-group">
                                    <label>Nomor Telepon Marketing</label>
                                    <input type="text" maxlength="12" id="edit_no_telp_marketing" name="no_telp_marketing" class="form-control" placeholder="No Telepon Marketing">
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_perjanjian_habis" class="control-label">Tanggal Perjanjian Habis<span style="color: red;" id="span_tgl" >*</span></label>
                                    <div class="input-group">
                                        <input  id="edit_tanggal_perjanjian_habis" name="tanggal_perjanjian_habis" type="text" class="form-control datepick" placeholder="mm/dd/yyyy" value=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="updateSuplier"><i class="fa fa-floppy-o p-r-10"></i>
                                SIMPAN</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

<?php $this->load->view('footer');?>