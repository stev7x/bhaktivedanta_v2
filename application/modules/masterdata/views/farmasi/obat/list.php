<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
    <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Master Obat</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Master</a></li>
                    <li class="active">Obat</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Master Obat
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> TAMBAH</button>
                                </div>
                            </div>
                            <table id="table_obat_list" class="table table-striped dataTable" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>No</th>
<!--                                    <th>Batch Nuber</th>-->
                                    <th>Nama Obat</th>
                                    <th>Jenis Barang</th>
<!--                                    <th>Kadaluarsa</th>-->
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
<!--                                    <th>Batch Nuber</th>-->
                                    <th>Nama Obat</th>
                                    <th>Jenis Barang</th>
<!--                                    <th>Kadaluarsa</th>-->
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->

    </div>
    <!-- /.container-fluid -->


    <!-- Modal  -->
    <div id="modal_add" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmCreateObat'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Tambah Obat</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <span style="color: red;">* Wajib diisi</span><br>
                                <div class="form-group">
                                    <label>Nama Obat <span style="color: red">*</span></label>
                                    <input type="text" name="nama_obat" class="form-control" placeholder="Nama Obat">
                                </div>
                                <div class="form-group">
                                    <label>Jenis Obat <span style="color: red">*</span></label>
                                    <select name="jenis_barang" id="jenis_barang" class="form-control">
                                        <option value="" disabled selected>Pilih Jenis Obat</option>
                                        <?php
                                            foreach ($jenis_barang as $key => $value) {
                                                echo "<option value=".$value['id'].">".$value['nama']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea class="form-control" id="Keterangan" name="Keterangan" placeholder="Keterangan"> </textarea>
                                </div>
                                <div class="form-group">
                                    <label>Persediaan <span style="color: red">*</span></label>
                                    <select name="persediaan" id="persediaan" class="form-control" onchange="setPersediaan(this);">
                                        <option value="" disabled selected>Pilih Persediaan</option>
                                        <?php
                                            foreach ($persediaan as $key => $value) {
                                                echo "<option value=".$value['id'].">".$value['persediaan_1'].", ".$value['persediaan_2'].", ".$value['persediaan_3']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_1">Persediaan 1</span> <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="jumlah_persediaan_1" name="jumlah_persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_2">Persediaan 2</span> <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="jumlah_persediaan_2" name="jumlah_persediaan_2" class="form-control" placeholder="Persediaan 2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_3">Persediaan 3</span> <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="jumlah_persediaan_3" name="jumlah_persediaan_3" class="form-control" placeholder="Persediaan 3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Eceran Tertinggi <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="het_persediaan_1" name="het_persediaan_1" class="form-control" placeholder="Persediaan 1">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="het_persediaan_2" name="het_persediaan_2" class="form-control" placeholder="Persediaan 2">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="het_persediaan_3" name="het_persediaan_3" class="form-control" placeholder="Persediaan 3">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Jual Visit <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_visit_local" name="harga_visit_local" class="form-control" placeholder="Local">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_visit_dome" name="harga_visit_dome" class="form-control" placeholder="Domestik">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_visit_asing" name="harga_visit_asing" class="form-control" placeholder="Asing">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Jual On Call <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_oncall_local" name="harga_oncall_local" class="form-control" placeholder="Local">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_oncall_dome" name="harga_oncall_dome" class="form-control" placeholder="Domestik">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_oncall_asing" name="harga_oncall_asing" class="form-control" placeholder="Asing">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="saveObat"><i class="fa fa-floppy-o p-r-10"></i>
                                SIMPAN</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal  -->
    <div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmUpdateObat'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="edit_modal_label"Edit Obat</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <span style="color: red;">* Wajib diisi</span><br>
                                <div class="form-group">
                                    <label>Nama Obat <span style="color: red">*</span></label>
                                    <input type="text" name="nama_obat" id="edit_nama_obat" class="form-control" placeholder="Nama Obat">
                                    <input type="hidden" name="id" id="edit_id_obat">
                                </div>
                                <div class="form-group">
                                    <label>Jenis Obat <span style="color: red">*</span></label>
                                    <select name="jenis_barang" id="edit_jenis_barang" class="form-control">
                                        <option value="" disabled selected>Pilih Jenis Obat</option>
                                        <?php
                                        foreach ($jenis_barang as $key => $value) {
                                            echo "<option value=".$value['id'].">".$value['nama']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea class="form-control" id="edit_keterangan" name="keterangan" placeholder="Keterangan"> </textarea>
                                </div>
                                <div class="form-group">
                                    <label>Persediaan <span style="color: red">*</span></label>
                                    <select name="persediaan" id="edit_persediaan" class="form-control" onchange="setPersediaanEdit(this);">
                                        <option value="" disabled selected>Pilih Persediaan</option>
                                        <?php
                                        foreach ($persediaan as $key => $value) {
                                            echo "<option value=".$value['id'].">".$value['persediaan_1'].", ".$value['persediaan_2'].", ".$value['persediaan_3']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_1">Persediaan 1</span> <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_jumlah_persediaan_1" name="jumlah_persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_2">Persediaan 2</span> <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_jumlah_persediaan_2" name="jumlah_persediaan_2" class="form-control" placeholder="Persediaan 2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_3">Persediaan 3</span> <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_jumlah_persediaan_3" name="jumlah_persediaan_3" class="form-control" placeholder="Persediaan 3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Eceran Tertinggi <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_het_persediaan_1" name="het_persediaan_1" class="form-control" placeholder="Persediaan 1">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_het_persediaan_2" name="het_persediaan_2" class="form-control" placeholder="Persediaan 2">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_het_persediaan_3" name="het_persediaan_3" class="form-control" placeholder="Persediaan 3">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Jual Visit <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_visit_local" name="harga_visit_local" class="form-control" placeholder="Local">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_visit_dome" name="harga_visit_dome" class="form-control" placeholder="Domestik">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_visit_asing" name="harga_visit_asing" class="form-control" placeholder="Asing">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Jual On Call <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_oncall_local" name="harga_oncall_local" class="form-control" placeholder="Local">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_oncall_dome" name="harga_oncall_dome" class="form-control" placeholder="Domestik">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_oncall_asing" name="harga_oncall_asing" class="form-control" placeholder="Asing">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="updateObat"><i class="fa fa-floppy-o p-r-10"></i>
                                SIMPAN</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal  -->
    <div id="modal_detail" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="detail_modal_label"Edit Obat</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Obat </label>
                                    <input type="text" name="nama_obat" id="detail_nama_obat" class="form-control" placeholder="Nama Obat" readonly/>
                                </div>
                                <div class="form-group">
                                    <label>Jenis Obat </label>
                                    <select name="jenis_barang" id="detail_jenis_barang" class="form-control" readonly>
                                        <option value="" disabled selected>Pilih Jenis Obat</option>
                                        <?php
                                        foreach ($jenis_barang as $key => $value) {
                                            echo "<option value=".$value['id'].">".$value['nama']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea class="form-control" id="detail_keterangan" name="Keterangan" placeholder="Keterangan" readonly> </textarea>
                                </div>
                                <div class="form-group">
                                    <label>Persediaan </label>
                                    <select name="persediaan" id="detail_persediaan" class="form-control" readonly>
                                        <option value="" disabled selected>Pilih Persediaan</option>
                                        <?php
                                        foreach ($persediaan as $key => $value) {
                                            echo "<option value=".$value['id'].">".$value['persediaan_1'].", ".$value['persediaan_2'].", ".$value['persediaan_3']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_1">Persediaan 1</span> </label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="detail_jumlah_persediaan_1" name="jumlah_persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_2">Persediaan 2</span> </label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="detail_jumlah_persediaan_2" name="jumlah_persediaan_2" class="form-control" placeholder="Persediaan 2" readonly>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;"><span class="label_persediaan_3">Persediaan 3</span> </label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="detail_jumlah_persediaan_3" name="jumlah_persediaan_3" class="form-control" placeholder="Persediaan 3" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Eceran Tertinggi </label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_het_persediaan_1" name="het_persediaan_1" class="form-control" placeholder="Persediaan 1" readonly>
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_het_persediaan_2" name="het_persediaan_2" class="form-control" placeholder="Persediaan 2" readonly>
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_het_persediaan_3" name="het_persediaan_3" class="form-control" placeholder="Persediaan 3" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Jual Visit </label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_harga_visit_local" name="harga_visit_local" class="form-control" placeholder="Local" readonly>
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_harga_visit_dome" name="harga_visit_dome" class="form-control" placeholder="Domestik" readonly>
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_harga_visit_asing" name="harga_visit_asing" class="form-control" placeholder="Asing" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Jual On Call </label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_harga_oncall_local" name="harga_oncall_local" class="form-control" placeholder="Local" readonly>
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_harga_oncall_dome" name="harga_oncall_dome" class="form-control" placeholder="Domestik" readonly>
                                            <input style="margin-bottom: 5px;" type="text" min="0" id="detail_harga_oncall_asing" name="harga_oncall_asing" class="form-control" placeholder="Asing" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

<?php $this->load->view('footer');?>