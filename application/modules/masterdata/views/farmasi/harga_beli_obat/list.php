<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
    <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Master Harga Beli Obat</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Master</a></li>
                    <li class="active">Harga Beli Obat</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Master Harga Beli Obat
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> TAMBAH</button>
                                </div>
                            </div>
                            <table id="table_harga_beli_obat_list" class="table table-striped dataTable" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Obat</th>
                                    <th>Batch Number</th>
                                    <th>Suplier</th>
                                    <th>Harga</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Obat</th>
                                    <th>Batch Number</th>
                                    <th>Suplier</th>
                                    <th>Harga</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->

    </div>
    <!-- /.container-fluid -->


    <!-- Modal  -->
    <div id="modal_add" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmCreateHargaBeliObat'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Tambah Harga Beli Obat</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <span style="color: red;">* Wajib diisi</span><br>
                                <div class="form-group">
                                    <label for="batch_id">Batch Number <span style="color: red;">*</span></label>
                                    <select class="form-control select2" name="batch_id" id="batch_id" onchange="setBatch(this);">
                                        <option selected value="">Pilih Batch Number</option>
                                        <?php
                                            foreach($batch as $key => $value){
                                                echo "<option value=".$value['id'].">".$value['number']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nama_obat">Nama Obat <span style="color: red">*</span></label>
                                    <input type="text" name="nama_obat" id="nama_obat" class="form-control" placeholder="Nama Obat" readonly>
                                    <input type="hidden" name="obat_id" id="obat_id">
                                </div>
                                <div class="form-group">
                                    <label for="suplier_id">Nama Suplier <span style="color: red;">*</span></label>
                                    <select class="form-control select2" name="suplier_id" id="suplier_id">
                                        <option selected value="">Pilih Suplier</option>
                                        <?php
                                            foreach($suplier as $key => $value){
                                                echo "<option value=".$value['id'].">".$value['nama']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Beli Obat <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_persediaan_1" name="harga_persediaan_1" class="form-control" placeholder="Harga Persediaan 1">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_persediaan_2" name="harga_persediaan_2" class="form-control" placeholder="Harga Persediaan 2">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="harga_persediaan_3" name="harga_persediaan_3" class="form-control" placeholder="Harga Persediaan 3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="saveHargaBeliObat"><i class="fa fa-floppy-o p-r-10"></i>
                                SIMPAN</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal  -->
    <div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmUpdateHargaBeliObat'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="edit_modal_label"Edit Harga Beli Obat</h2>
                </div>
                <div class="modal-body" style="height:450px;overflow:auto">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_add" name="<?php echo $this->security->get_csrf_token_name()?>_add" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <span style="color: red;">* Wajib diisi</span><br>
                                <div class="form-group">
                                    <label for="edit_batch_number">Batch Number <span style="color: red">*</span></label>
                                    <input type="text" name="batch_number" id="edit_batch_number" class="form-control" placeholder="Batch Number" readonly>
                                    <input type="hidden" name="batch_id" id="edit_batch_id">
                                    <input type="hidden" name="id" id="edit_id">
                                </div>
                                <div class="form-group">
                                    <label for="edit_nama_obat">Nama Obat <span style="color: red">*</span></label>
                                    <input type="text" name="nama_obat" id="edit_nama_obat" class="form-control" placeholder="Nama Obat" readonly>
                                    <input type="hidden" name="obat_id" id="edit_obat_id">
                                </div>
                                <div class="form-group">
                                    <label for="edit_suplier_id">Nama Suplier <span style="color: red;">*</span></label>
                                    <select class="form-control select2" name="suplier_id" id="edit_suplier_id">
                                        <option selected>Pilih Suplier</option>
                                        <?php
                                            foreach($suplier as $key => $value){
                                                echo "<option value=".$value['id'].">".$value['nama']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top: 9px; margin-left: 20px;">Harga Beli Obat <span style="color: red">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_persediaan_1" name="harga_persediaan_1" class="form-control" placeholder="Harga Persediaan 1">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_persediaan_2" name="harga_persediaan_2" class="form-control" placeholder="Harga Persediaan 2">
                                            <input style="margin-bottom: 5px;" type="number" min="0" id="edit_harga_persediaan_3" name="harga_persediaan_3" class="form-control" placeholder="Harga Persediaan 3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="updateHargaBeliObat"><i class="fa fa-floppy-o p-r-10"></i>
                                SIMPAN</button>
                        </div>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

<?php $this->load->view('footer');?>