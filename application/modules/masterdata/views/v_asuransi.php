<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Master Asuransi</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.html">Master</a></li>
                    <li class="active">Asuransi</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!--row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">
                    <div class="panel-heading"> Master Asuransi
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">

                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                <div class="col-md-6">
                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add" data-backdrop="static" data-keyboard="false">Tambah Asuransi</button>
                                    &nbsp;&nbsp;&nbsp;
                                    <!--                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Asuransi</button>-->
                                </div>
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <table id="table_asuransi_list" class="table table-striped" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Negara</th>
                                    <th>PIC</th>
                                    <th>Kontak</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Negara</th>
                                    <th>PIC</th>
                                    <th>Kontak</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/row -->

    </div>
    <!-- /.container-fluid -->

    <!-- Modal  -->
    <div id="modal_add" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmCreateAsuransi'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Tambah Asuransi</h2>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message" class=""></p>
                        </div>
                    </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

                        <div class="form-group col-md-12">
                            <label>Nama Asuransi <span style="color: red">*</span></label>
                            <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Asuransi">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="negara_id">Negara <span style="color: red">*</span></label>
                            <select name="negara_id" id="negara_id" class="form-control select2">
                                <option value="" disabled selected>Pilih Negara</option>
                                <?php
                                    foreach ($negara as $key => $value) {
                                        echo "<option value=".$value['id'].">".$value['nama_negara']."</option>";
                                    }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label>PIC <span style="color: red">*</span></label>
                            <input type="text" name="pic" id="pic" class="form-control" placeholder="PIC">
                        </div>

                        <div class="form-group col-md-12">
                            <label>Kontak <span style="color: red">*</span></label>
                            <input type="text" name="kontak" id="kontak" class="form-control" placeholder="Kontak">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="saveAsuransi"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!-- Modal  -->
    <div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <?php echo form_open('#',array('id' => 'fmUpdateAsuransi'))?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Perbarui Asuransi</h2>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                    <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-12">
                            <label>Nama Asuransi <span style="color: red">*</span></label>
                            <input type="text" name="nama" id="edit_nama" class="form-control" placeholder="Nama Asuransi">
                            <input type="hidden" name="id" id="edit_id">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="negara_id">Negara <span style="color: red">*</span></label>
                            <select name="negara_id" id="edit_negara_id" class="form-control select2">
                                <option value="" disabled selected>Pilih Negara</option>
                                <?php
                                foreach ($negara as $key => $value) {
                                    echo "<option value=".$value['id'].">".$value['nama_negara']."</option>";
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label>PIC <span style="color: red">*</span></label>
                            <input type="text" name="pic" id="edit_pic" class="form-control" placeholder="PIC">
                        </div>

                        <div class="form-group col-md-12">
                            <label>Kontak <span style="color: red">*</span></label>
                            <input type="text" name="kontak" id="edit_kontak" class="form-control" placeholder="Kontak">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="updateAsuransi"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <?php $this->load->view('footer');?>