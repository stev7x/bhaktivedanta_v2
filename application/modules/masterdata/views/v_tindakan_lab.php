<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Tindakan Lab</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Tindakan Lab</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Master Tindakan Lab
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <div class="col-md-6">
                                                    <!-- <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_tindakan_lab" onclick="tambahTindakan()">Tambah Tindakan Lab</button> -->
                                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_tindakan_lab" data-backdrop="static" data-keyboard="false">Tambah Tindakan Lab</button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Tindakan Lab</button>
                                                </div>
                                                <!-- <div class="col-md-6">
                                                        <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />
                                                            <label for="file" style="float: right;"><span></span> <strong>
                                                            <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>
                                                            <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>

                                                        <?php echo form_close(); ?>
                                                </div>                                               -->
                                            </div>
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                                           <table id="table_tindakan_lab_list" class="table table-striped table-hover" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Tindakan Lab</th>
                                                    <th>Kelompok Tindakan Lab</th>
                                                    <th>Harga BPJS</th>
                                                    <th>Harga non BPJS</th>
                                                    <th>Harga Cyto</th>
                                                    <th>Harga non Cyto</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Tindakan Lab</th>
                                                    <th>Kelompok Tindakan Lab</th>
                                                    <th>Harga BPJS</th>
                                                    <th>Harga non BPJS</th>
                                                    <th>Harga Cyto</th>
                                                    <th>Harga non Cyto</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row -->

            </div>
            <!-- /.container-fluid -->


<!-- Modal  -->
<div id="modal_add_tindakan_lab" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateTindakanLab'))?>
       <div class="modal-content" style="height:600px;overflow:auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Tindakan Lab</h2>
            </div>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button>
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden" name="tindakan_lab_id" id="tindakan_lab_id">
                        <input type="hidden" name="nilai_rujukan_json" id="nilai_rujukan_json">
                        <div class="form-group col-md-12">
                            <label>Nama Tindakan Lab<span style="color: red">*</span></label>
                            <input type="text" name="tindakan_lab_nama" id="tindakan_lab_nama" class="form-control" placeholder="Nama Tindakan Lab">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Kelompok Tindakan Lab<span style="color: red">*</span></label>
                            <select name="kelompoktindakan_lab_id" id="kelompoktindakan_lab_id" class="form-control">
                                <option value="" disabled selected>Pilih Kelompok Tindakan Lab</option>
                                <?php
                                    $list_kelompoktindakan = $this->Tindakan_lab_model->get_data_kelompoktindakan();
                                    foreach($list_kelompoktindakan as $list){
                                        echo "<option value='".$list->kelompoktindakan_lab_id."'>".$list->kelompoktindakan_lab_nama."</option>";
                                    }
                                    ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Harga Non BPJS<span style="color: red">*</span></label>
                            <input type="text" name="harga_non_bpjs" id="harga_non_bpjs"  onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga Non BPJS">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Harga Non Cyto<span style="color: red">*</span></label>
                            <input type="text" name="harga_non_cyto" id="harga_non_cyto"  onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga Non Cyto">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Harga BPJS<span style="color: red">*</span></label>
                            <input type="text" name="harga_bpjs" id="harga_bpjs"  onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga BPJS">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Harga Cyto<span style="color: red">*</span></label>
                            <input type="text" name="harga_cyto" id="harga_cyto"  onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga Cyto">
                        </div>
                </div>
                <div class="row">
                        <div class="white-box" style="border: 1px solid grey; margin: 10px;">
                          <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_add_nilai_rujukan" style="display:none;">
                              <button type="button" class="close" data-dismiss="alert" >&times;</button>
                              <div >
                                  <p id="card_message"></p>
                              </div>
                          </div>
                          <label>Nilai Rujukan<span style="color: red">*</span></label>
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Jenis Kelamin</th>
                                <th scope="col">Kategori Usia</th>
                                <th scope="col">Jenis Cairan</th>
                                <th scope="col">Nilai Rujukan</th>
                              </tr>
                            </thead>
                            <tbody id="tbody_nilai_rujukan">
                              <tr>
                                <td colspan=4 align=center>belum ada nilai rujukan</td>
                              </tr>
                            </tbody>
                          </table>
                          <hr>
                          <div class="form-group col-md-12">
                              <label>Jenis Kelamin<span style="color: red">*</span></label>
                              <select name="nilai_rujukan_jenis_kelamin" id="nilai_rujukan_jenis_kelamin" class="form-control">
                                  <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                  <option value="pria">Pria</option>
                                  <option value="wanita">Wanita</option>
                              </select>
                          </div>
                          <div class="form-group col-md-12">
                              <label>Kategori Usia<span style="color: red">*</span></label>
                              <select name="nilai_rujukan_kategori_usia" id="nilai_rujukan_kategori_usia" class="form-control">
                                  <option value="" disabled selected>Pilih Kategori Usia</option>
                                  <option value="dewasa">Dewasa</option>
                                  <option value="anak-anak">Anak - anak</option>
                                  <option value="bayi">Bayi</option>
                              </select>
                          </div>
                          <div class="form-group col-md-12">
                              <label>Jenis Cairan<span style="color: red">*</span></label>
                              <select name="nilai_rujukan_jenis_cairan" id="nilai_rujukan_jenis_cairan" class="form-control">
                                  <option value="" disabled selected>Pilih Jenis Cairan</option>
                                  <option value="cairan_a">Cairan a</option>
                                  <option value="cairan_b">Cairan b</option>
                              </select>

                          </div>
                          <div class="form-group col-md-12">
                            <label>Nilai Rujukan<span style="color: red">*</span></label><br>
                            <!-- <div class="form-group col-md-12"> -->
                              <div class="col-md-4" style="padding-left: 0px;">
                                <input name="nilai_rujukan_batas_bawah" id="nilai_rujukan_batas_bawah" type="text" class="form-control" placeholder="Batas Bawah">
                              </div>
                              <div class="col-md-4">
                                <input name="nilai_rujukan_batas_atas" id="nilai_rujukan_batas_atas" type="text" class="form-control" placeholder="Batas Atas">
                              </div>
                              <div class="col-md-4" style="padding-right: 0px;">
                                <select name="nilai_rujukan_satuan" id="nilai_rujukan_satuan" class="form-control">
                                    <option value="" disabled selected>Pilih Satuan</option>
                                    <option value="Hemoglobin">Hemoglobin</option>
                                </select>
                              </div>
                            <!-- </div> -->

                          </div>
                          <div class="col-md-12">
                            <br>
                            <button type="button" class="btn btn-success pull-right" id="tambahNilaiRujukan"><i class="fa fa-floppy-o p-r-10"></i>Tambah Nilai Rujukan</button>
                            <button type="button" class="btn btn-danger pull-right" id="resetNilaiRujukan" style="margin-right: 10px;"><i class="fa fa-refresh p-r-10"></i>Reset Nilai Rujukan</button>
                          </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <button type="button" class="btn btn-success pull-right" id="saveTindakanLab"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
<div id="modal_edit_tindakan_lab" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmUpdateTindakanLab'))?>
       <div class="modal-content" style="height:600px;overflow:auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Tindakan Lab</h2>
            </div>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button>
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden" name="upd_tindakan_lab_id" id="upd_tindakan_lab_id">
                        <input type="hidden" name="upd_nilai_rujukan_json" id="upd_nilai_rujukan_json">
                        <div class="form-group col-md-12">
                            <label>Nama Tindakan Lab<span style="color: red">*</span></label>
                            <input type="text" name="upd_tindakan_lab_nama" id="upd_tindakan_lab_nama" class="form-control" placeholder="Nama Tindakan Lab">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Kelompok Tindakan Lab<span style="color: red">*</span></label>
                            <select name="upd_kelompoktindakan_lab_id" id="upd_kelompoktindakan_lab_id" class="form-control">
                                <option value="" disabled selected>Pilih Kelompok Tindakan Lab</option>
                                <?php
                                    $list_kelompoktindakan = $this->Tindakan_lab_model->get_data_kelompoktindakan();
                                    foreach($list_kelompoktindakan as $list){
                                        echo "<option value='".$list->kelompoktindakan_lab_id."'>".$list->kelompoktindakan_lab_nama."</option>";
                                    }
                                    ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Harga Non BPJS<span style="color: red">*</span></label>
                            <input type="text" name="upd_harga_non_bpjs" id="upd_harga_non_bpjs"  onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga Non BPJS">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Harga Non Cyto<span style="color: red">*</span></label>
                            <input type="text" name="upd_harga_non_cyto" id="upd_harga_non_cyto"  onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga Non Cyto">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Harga BPJS<span style="color: red">*</span></label>
                            <input type="text" name="upd_harga_bpjs" id="upd_harga_bpjs"  onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga BPJS">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Harga Cyto<span style="color: red">*</span></label>
                            <input type="text" name="upd_harga_cyto" id="upd_harga_cyto"  onkeyup="formatAsRupiah(this)"  class="form-control" placeholder="Harga Cyto">
                        </div>
                </div>
                <div class="row">
                        <div class="white-box" style="border: 1px solid grey; margin: 10px;">
                          <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_add_nilai_rujukan" style="display:none;">
                              <button type="button" class="close" data-dismiss="alert" >&times;</button>
                              <div >
                                  <p id="card_message"></p>
                              </div>
                          </div>
                          <label>Nilai Rujukan<span style="color: red">*</span></label>
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Jenis Kelamin</th>
                                <th scope="col">Kategori Usia</th>
                                <th scope="col">Jenis Cairan</th>
                                <th scope="col">Nilai Rujukan</th>
                              </tr>
                            </thead>
                            <tbody id="upd_tbody_nilai_rujukan">
                              <tr>
                                <td colspan=4 align=center>belum ada nilai rujukan</td>
                              </tr>
                            </tbody>
                          </table>
                          <hr>
                          <div class="form-group col-md-12">
                              <label>Jenis Kelamin<span style="color: red">*</span></label>
                              <select name="upd_nilai_rujukan_jenis_kelamin" id="upd_nilai_rujukan_jenis_kelamin" class="form-control">
                                  <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                  <option value="pria">Pria</option>
                                  <option value="wanita">Wanita</option>
                              </select>
                          </div>
                          <div class="form-group col-md-12">
                              <label>Kategori Usia<span style="color: red">*</span></label>
                              <select name="upd_nilai_rujukan_kategori_usia" id="upd_nilai_rujukan_kategori_usia" class="form-control">
                                  <option value="" disabled selected>Pilih Kategori Usia</option>
                                  <option value="dewasa">Dewasa</option>
                                  <option value="anak-anak">Anak - anak</option>
                                  <option value="bayi">Bayi</option>
                              </select>
                          </div>
                          <div class="form-group col-md-12">
                              <label>Jenis Cairan<span style="color: red">*</span></label>
                              <select name="upd_nilai_rujukan_jenis_cairan" id="upd_nilai_rujukan_jenis_cairan" class="form-control">
                                  <option value="" disabled selected>Pilih Jenis Cairan</option>
                                  <option value="cairan_a">Cairan a</option>
                                  <option value="cairan_b">Cairan b</option>
                              </select>

                          </div>
                          <div class="form-group col-md-12">
                            <label>Nilai Rujukan<span style="color: red">*</span></label><br>
                            <!-- <div class="form-group col-md-12"> -->
                              <div class="col-md-4" style="padding-left: 0px;">
                                <input name="upd_nilai_rujukan_batas_bawah" id="upd_nilai_rujukan_batas_bawah" type="text" class="form-control" placeholder="Batas Bawah">
                              </div>
                              <div class="col-md-4">
                                <input name="upd_nilai_rujukan_batas_atas" id="upd_nilai_rujukan_batas_atas" type="text" class="form-control" placeholder="Batas Atas">
                              </div>
                              <div class="col-md-4" style="padding-right: 0px;">
                                <select name="upd_nilai_rujukan_satuan" id="upd_nilai_rujukan_satuan" class="form-control">
                                    <option value="" disabled selected>Pilih Satuan</option>
                                    <option value="Hemoglobin">Hemoglobin</option>
                                </select>
                              </div>
                            <!-- </div> -->

                          </div>
                          <div class="col-md-12">
                            <br>
                            <button type="button" class="btn btn-success pull-right" id="upd_tambahNilaiRujukan"><i class="fa fa-floppy-o p-r-10"></i>Tambah Nilai Rujukan</button>
                            <button type="button" class="btn btn-danger pull-right" id="upd_resetNilaiRujukan" style="margin-right: 10px;"><i class="fa fa-refresh p-r-10"></i>Reset Nilai Rujukan</button>
                          </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <button type="button" class="btn btn-success pull-right" id="changeTindakanLab"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

 <!-- Modal  -->

<?php $this->load->view('footer');?>
