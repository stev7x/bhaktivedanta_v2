<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Data</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master Data</a></li>
                            <li class="active">Paket Operasi</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Master Paket Operasi     
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <div class="col-md-6">
                                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_paket_operasi" data-backdrop="static" data-keyboard="false">Tambah Paket Operasi</button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Paket Operasi</button>
                                                </div>
                                                <!-- <div class="col-md-6"> 
                                                        <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>
                                                            &nbsp;&nbsp;&nbsp;  
                                                            <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />  
                                                            <label for="file" style="float: right;"><span></span> <strong>     
                                                            <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                            <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                        <?php echo form_close(); ?> 
                                                </div>                                             -->
                                            </div> 
                                           <table id="table_paket_operasi_list" class="table table-striped dataTable" cellspacing="0"> 
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Paket</th>
                                                        <th>Total Harga</th>
                                                        <th>Kelas Pelayanan</th>
                                                        <th>Jumlah Hari Rawat</th>
                                                        <th>Type Pembayaran</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody> 
                                            </table>
                                      

                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

           
<div id="modal_add_paket_operasi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    
    <?php echo form_open('#',array('id' => 'fmCreatePaketOperasi'))?>   
    <div class="modal-content" style="margin-top: 100px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Paket Operasi</h2> 
            </div>
            <div class="modal-body">
                <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                    <div>
                        <p id="card_message1" class=""></p>
                    </div>
                </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delope" name="<?php echo $this->security->get_csrf_token_name()?>_delope" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-6">
                            <label>Nama Paket Operasi<span style="color: red">*</span></label>
                            <input type="text" name="nama_paket" class="form-control" placeholder="Nama Paket Operasi">
                            
                        </div> 
                        <div class="form-group col-md-6"> 
                            <label>Jumlah Hari<span style="color: red">*</span></label>
                            <input type="number" name="jml_hari" class="form-control" placeholder="Jumlah Hari">
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Kelas Pelayanan<span style="color: red">*</span></label>
                            <select name="kelaspelayanan" class="form-control">
                            <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                            <?php
                            $list_kelas = $this->Paket_operasi_model->get_pelayanan_list();
                            foreach($list_kelas as $list){
                                echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                            }
                            ?>
                            </select>
                            
                        </div> 
                        <div class="form-group col-md-6">
                            <label>Pembayaran<span style="color: red">*</span></label>
                            <select name="is_bpjs" class="form-control">
                                <option value="" disabled selected>Pilih Pembayaran</option>
                                <option value="1">BPJS</option>
                                <option value="0">Non BPJS</option>
                            </select>
                            
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Total Harga<span style="color: red">*</span></label>
                            <input type="text" onkeyup="formatAsRupiah(this)" name="total_harga" id="total_harga" class="form-control" placeholder="Total Harga">
                        </div> 
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="savePaketOperasi"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

 <div id="modal_edit_paket_operasi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    
    <?php echo form_open('#',array('id' => 'fmUpdatePaketOperasi'))?>   <div class="modal-content" style="margin-top: 100px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Perbarui Paket Operasi</h2> 
            </div>
            <div class="modal-body">
                <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif2" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                    <div>
                        <p id="card_message2" class=""></p>
                    </div>
                </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delope" name="<?php echo $this->security->get_csrf_token_name()?>_delope" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-6">
                            <label>Nama Paket Operasi<span style="color: red">*</span></label>
                            <input type="text" name="upd_nama_paket" id="upd_nama_paket" class="form-control" placeholder="Nama Paket Operasi">
                            <input type="hidden" name="upd_id_paket" id="upd_id_paket" class="validate">
                        </div> 
                        <div class="form-group col-md-6"> 
                            <label>Jumlah Hari<span style="color: red">*</span></label>
                            <input type="number" name="upd_jml_hari" id="upd_jml_hari" class="form-control" placeholder="Jumlah Hari">
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Kelas Pelayanan<span style="color: red">*</span></label>
                            <select name="upd_kelaspelayanan" id="upd_kelaspelayanan"class="form-control">
                            <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                            <?php
                            $list_kelas = $this->Paket_operasi_model->get_pelayanan_list();
                            foreach($list_kelas as $list){
                                echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                            }
                            ?>
                            </select>
                            
                        </div> 
                        <div class="form-group col-md-6">
                            <label>Pembayaran<span style="color: red">*</span></label>
                            <select name="upd_bpjs" id="upd_bpjs" class="form-control">
                                <option value="" disabled selected>Pilih Pembayaran</option>
                                <option value="1">BPJS</option>
                                <option value="0">Non BPJS</option>
                            </select>
                            
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Total Harga<span style="color: red">*</span></label>
                            <input type="text" onkeyup="formatAsRupiah(this)"  name="upd_total_harga" id="upd_total_harga" class="form-control" placeholder="Total Harga">
                        </div> 
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="changePaketOperasi"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>

<?php $this->load->view('footer');?>
      