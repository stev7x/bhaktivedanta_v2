<?php $this->load->view('header');?>
<title>SIMRS | Master Kelas Poli Ruangan</title>
<?php $this->load->view('sidebar');?>
      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Master Kelas Poli Ruangan</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Master</a></li>
                    <li class="active">Kelas Poli Ruangan</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        <!--start container-->
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <p class="right-align"><a class="btn waves-effect waves-light indigo modal-trigger" href="#modal_add_kelas_poli_ruangan">Tambah Kelas Poli Ruangan</a></p>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="divider"></div>
                            <h4 class="header">Master Kelas Poli Ruangan</h4>
                            <div id="card-alert" class="card green notif" style="display:none;">
                                <div class="card-content white-text">
                                    <p id="card_message">SUCCESS : The page has been added.</p>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div id="table-datatables">
                                <div class="row">
                                    <div class="col s12 m4 l12">
                                        <table id="table_kelas_poli_ruangan_list" class="responsive-table display" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Poli Ruangan</th>
                                                    <th>Kelas Pelayanan</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Poli Ruangan</th>
                                                    <th>Kelas Pelayanan</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
    <!-- END CONTENT -->
    
    <!-- Start Modal Add Kelas Poli Ruangan -->
    <div id="modal_add_kelas_poli_ruangan" class="modal">
        <?php echo form_open('#',array('id' => 'fmCreateKelas_poli_ruangan'))?>
        <div class="modal-content">
            <h1>Tambah Kelas Poli Ruangan</h1>
            <div class="divider"></div>
            <div id="card-alert" class="card green modal_notif" style="display:none;">
                <div class="card-content white-text">
                    <p id="modal_card_message"></p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--jqueryvalidation-->
            <div id="jqueryvalidation" class="section">
                <div class="col s12 formValidate">
                    <span style="color: red;"> *) wajib diisi</span>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="select_poliruangan" name="select_poliruangan">
                                <option value="" disabled selected>Pilih Ruangan</option>
                                <?php
                                $list_kelas = $this->Kelas_poli_ruangan_model->get_ruangan_list();
                                foreach($list_kelas as $list){
                                    echo "<option value=".$list->poliruangan_id.">".$list->nama_poliruangan."</option>";
                                }
                                ?>
                            </select>
                            <label>Ruangan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="select_kelaspelayanan" name="select_kelaspelayanan">
                                <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                                <?php
                                $list_tindakan = $this->Kelas_poli_ruangan_model->get_pelayanan_list();
                                foreach($list_tindakan as $list){
                                    echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                }
                                ?>
                            </select>
                            <label>Kelas Pelayanan<span style="color: red;"> *</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close">Close<i class="mdi-navigation-close left"></i></button>
            <button type="button" class="btn waves-effect waves-light teal modal-action" id="saveKelas_poli_ruangan">Save<i class="mdi-content-save left"></i></button>
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- End Modal Add Kelas Poli Ruangan -->
<?php $this->load->view('footer');?>
      