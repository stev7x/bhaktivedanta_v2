<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Agama</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Agama</li>
                        </ol> 
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1"> 
                            <div class="panel-heading"> Master Data Agama
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <div class="col-md-6">
                                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_agama" data-backdrop="static" data-keyboard="false">TAMBAH AGAMA</button>
                                                </div>  
                                                <div class="col-md-6">
                                                <!-- <form id="frmImport" method="post" enctype="multipart/form-data"> -->
                                                <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>

                                                    &nbsp;&nbsp;&nbsp;
                                                   <input type="file" name="file" id="file"   class="inputfile inputfile-6 " data-multiple-caption="{count} files selected" multiple />  
                                                    <label for="file" style="float: right;"><span></span> <strong>     
                                                       <i class="fa fa-cloud-download p-r-10"></i> Choose a file&hellip;</strong></label><br><br><br>    
                                                    <!-- <input style="float: right;" type="submit" value="Import" class="btn btn-info btn-import" /> -->  
                                                   <button id="importData" style="float: right;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-sign-in p-r-10 "></i> Import Data</button>  
                                                        
   
<!-- 
                                                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_import" name="<?php echo $this->security->get_csrf_token_name()?>_import" value="<?php echo $this->security->get_csrf_hash()?>" />
                                                    
                                                    <input style="margin-left: 16px" type="file" id="file" name="file" accept=".xls, .xlsx, .csv">  
                                                    <input type="text" name="tes" id="tes" />           
       
                                                    <button id="importData" type="button" >UPLOAD</button>    -->
                                                <?php echo form_close(); ?> 
                                                </div>

                                            </div>
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                           <table id="table_agama_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                            <thead> 
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Agama</th>
                                                    <th>Action</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Agama</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>    
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div> 
            <!-- /.container-fluid -->


<!-- Modal  --> 
<div id="modal_add_agama" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateAgama'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Agama</h2> 
            </div>
            <div class="modal-body"> 
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden" name="agama_id">
                        <div class="form-group col-md-12">
                            <label>Nama Agama<span style="color: red">*</span></label>
                            <input type="text" name="nama_agama" id="nama_agama" class="form-control" placeholder="Nama Agama">
                        </div>    
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveAgama"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

 <!-- Modal  --> 
<div id="modal_edit_agama" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">   
    <?php echo form_open('#',array('id' => 'fmUpdateAgama'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Perbarui Agama</h2> 
            </div>
            <div class="modal-body" >
                <div class="alert alert-success alert-dismissable hide" id="upd_modal_notif">     
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> Data Berhasil di Update. 
                </div> 

                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <div class="form-group col-md-12">
                            <label>Nama Agama<span style="color: red">*</span></label>
                            <input type="text" name="upd_nama_agama" id="upd_nama_agama" class="form-control" placeholder="Nama Pendidikan">
                            <input type="hidden" id="upd_id_agama" name="upd_id_agama" class="validate">  
                        </div>         
                </div>
                <div class="row">
                    <div class="col-md-12">     
                        <button type="button" class="btn btn-success pull-right" id="changeAgama"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button> 
                    </div>
                </div>  
            </div>
            
        </div>
        <!-- /.modal-content -->
        <?php echo form_close(); ?> 
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

<?php $this->load->view('footer');?>
      