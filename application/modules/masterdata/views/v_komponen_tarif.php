    <?php $this->load->view('header');?>
     
    <?php $this->load->view('sidebar');?>
          <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row bg-title">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="page-title">Master Komponen Tarif</h4> </div>
                        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                            <ol class="breadcrumb">
                                <li><a href="index.html">Master</a></li>
                                <li class="active">Komponen Tarif</li>
                            </ol> 
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!--row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-info1"> 
                                <div class="panel-heading"> Master Data Komponen Tarif
                                </div>
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">

                                                <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                    <div class="col-md-6">
                                                        <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_komponentarif" data-backdrop="static" data-keyboard="false">TAMBAH Komponen Tarif</button>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                                               <table id="table_komponen_tarif_list" class="table table-striped table-hover" cellspacing="0" width="100%">  
                                                <thead> 
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Komponen Tarif</th>
                                                        <th>Allow Downpayment</th>
                                                        <th>Action</th> 
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Komponen Tarif</th>
                                                        <th>Allow Downpayment</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>    
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!--/row -->
                    
                </div> 
                <!-- /.container-fluid -->


    <!-- Modal  --> 
    <div id="modal_add_komponentarif" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
        <?php echo form_open('#',array('id' => 'fmCreateKomponenTarif'))?>
           <div class="modal-content"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Tambah Komponen Tarif</h2> 
                </div>
                <div class="modal-body"> 
                        <span style="color: red;">* Wajib diisi</span><br>
                    <div class="row">
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <input type="hidden" name="komponentarif_id">
                            <div class="form-group col-md-6">
                                <label>Nama Komponen Tarif<span style="color: red">*</span></label>
                                <input type="text" name="komponentarif_nama" id="komponentarif_nama" class="form-control" placeholder="Nama Komponen Tarif">
                            </div>    
                            <div class="form-group col-md-6">
                                <label>Downpayment</label>
                                <div style="margin-top: 5px;">
                                    <input type="checkbox" name="allow_downpayment" id="allow_downpayment">
                                    <span for="allow_downpayment" style="color: red"> Centang jika Bisa Downpayment</span>
                                </div>
                            </div>    
                    </div>
                    <div class="row">
                        <div class="col-md-12"> 
                            <button type="button" class="btn btn-success pull-right" id="saveKomponenTarif"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                        </div>
                    </div> 
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.modal-content -->
        </div>
         <!-- /.modal-dialog -->
    </div>
     <!-- /.modal -->

     <!-- Modal  --> 
    <div id="modal_edit_komponentarif" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">   
        <?php echo form_open('#',array('id' => 'fmUpdateKomponenTarif'))?>
           <div class="modal-content"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myLargeModalLabel">Perbarui Komponen Tarif</h2> 
                </div>
                <div class="modal-body" >
                    <div class="alert alert-success alert-dismissable hide" id="upd_modal_notif">     
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> Data Berhasil di Update. 
                    </div> 

                        <span style="color: red;">* Wajib diisi</span><br>
                    <div class="row">
                            <div class="form-group col-md-6">
                                <label>Nama Komponen Tarif<span style="color: red">*</span></label>
                                <input type="text" name="komponentarif_nama_update" id="komponentarif_nama_update" class="form-control" placeholder="Nama Komponen Tarif">
                                <input type="hidden" id="komponentarif_id_update" name="komponentarif_id_update" class="validate">  
                            </div>   
                            <div class="form-group col-md-6">
                                <label>Downpayment</label>
                                <div style="margin-top: 5px;">
                                    <input type="checkbox" name="allow_downpayment_update" id="allow_downpayment_update">
                                    <span for="allow_downpayment_update" style="color: red"> Centang jika Bisa Downpayment</span>
                                </div>
                            </div>        
                    </div>
                    <div class="row">
                        <div class="col-md-12">     
                            <button type="button" class="btn btn-success pull-right" id="changeKomponenTarif"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button> 
                        </div>
                    </div>  
                </div>
                
            </div>
            <!-- /.modal-content -->
            <?php echo form_close(); ?> 
        </div>
         <!-- /.modal-dialog -->
    </div>
     <!-- /.modal -->

    <?php $this->load->view('footer');?>
          