<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Kelas Pelayanan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Kelas Pelayanan</li>
                        </ol>   
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Master Kelas Pelayanan  
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                   <div class="panel-body">
                                        <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <div class="col-md-6"> 
                                                <button  type="button" class="btn btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="add_kelas_pelayanan()">Tambah Kelas Pelayanan</button>      
                                                &nbsp;&nbsp;&nbsp;
                                                <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Kelas Pelayanan</button>
                                            </div>
                                            <!-- <div class="col-md-6"> 
                                                <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>
                                                    &nbsp;&nbsp;&nbsp;  
                                                    <input type="file" name="file" id="file" accept=".xlsx"  class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />  
                                                    <label for="file" style="float: right;"><span></span> <strong>     
                                                    <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                    <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                <?php echo form_close(); ?> 
                                            </div>     -->
                                        </div>  
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />   
                                       <table id="table_kelas_pelayanan_list" class="table table-striped dataTable" cellspacing="0">     
                                       <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Kelas Pelayanan</th>
                                                <th>Jumlah Kasur</th>
                                                <th>Tarif</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Kelas Pelayanan</th>
                                                <th>Jumlah Kasur</th>
                                                <th>Tarif</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->


<!-- modal add  -->
<div id="modal_add_kelas_pelayanan" class="modal fade" style="overflow: hidden;">  
    <div class="modal-dialog modal-lg" > 
       <?php echo form_open('#',array('id' => 'fmCreateKelas_pelayanan'))?> 
       <div class="modal-content" style="margin-top: 160px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Kelas Pelayanan</h2> 
            </div>      
            <div class="modal-body" style="height: 500px;overflow: auto;">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">  
                    <div class="form-group col-md-12">
                        <label>Nama Kelas Pelayanan<span style="color: red">*</span></label> 
                        <input type="text" id="nama_kelas_pelayanan" name="nama_kelas_pelayanan" placeholder="Nama Kelas Pelayanan" class="form-control">
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Jumlah Kasur<span style="color: red">*</span></label>  
                        <input type="text" id="isi_bed" name="isi_bed" placeholder="Jumlah Kasur" class="form-control">
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Luas Ruangan<span style="color: red">*</span></label>  
                        <input type="text" id="luas_ruangan" name="luas_ruangan" placeholder="Luas Ruangan" class="form-control">
                    </div>    
                    <div class="form-group col-md-12">
                        <label>Tarif<span style="color: red">*</span></label>  
                        <input type="number" id="tarif" name="tarif" placeholder="Tarif" class="form-control">
                    </div> 
                </div>   
                <h3 style="margin-top: -10px">Fasilitas</h3><hr style="margin-top: -10px"/>
                <div class="row"> 
                    <div class="col-md-12" style="border-bottom: 1px solid #f1f1f1;">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="ceklis_semua" type="checkbox" class="ceklis"> 
                                <label for="ceklis_semua"> Ceklis Semua </label>   
                            </div> 
                    </div>
                </div>  
                <div class="row" style="margin-top: 5px"> 
                    <div class="col-md-4"> 
                        <div class="col-md-12"> 
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="kamar_mandi" name="kamar_mandi" type="checkbox" class="ceklis"> 
                                <label for="kamar_mandi"> Kamar Mandi Dalam </label> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="closet" name="closet" type="checkbox"> 
                                <label for="closet">Closed Duduk </label> 
                            </div>
                        </div> 
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="wastafel" name="wastafel" type="checkbox"> 
                                <label for="wastafel"> Wastafel </label> 
                            </div>
                        </div> 

                    </div> 
                     <div class="col-md-4"> 
                        <div class="col-md-12"> 
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="shower" name="shower" type="checkbox" > 
                                <label for="shower"> Shower </label> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="water_heater" name="water_heater"type="checkbox"> 
                                <label for="water_heater" > Water Heater </label> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="kipas_angin" name="kipas_angin" type="checkbox"> 
                                <label for="kipas_angin" > Kipas Angin </label> 
                            </div>
                        </div> 

                    </div>  
                     <div class="col-md-4"> 
                        <div class="col-md-12"> 
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="exhause_fan" name="exhause_fan" type="checkbox"> 
                                <label for="exhause_fan"> Exhause Fan </label> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="tv" name="tv" type="checkbox"> 
                                <label for="tv" > LED TV Digital </label> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="baby_bed" name="baby_bed" type="checkbox"> 
                                <label for="baby_bed" > Baby Bed </label> 
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4"> 
                        <div class="col-md-12"> 
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="ac" name="ac" type="checkbox"> 
                                <label for="ac"> AC </label> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="kulkas" name="kulkas" type="checkbox"> 
                                <label for="kulkas" > Kulkas </label> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="lemari" name="lemari" type="checkbox"> 
                                <label for="lemari" >Lemari </label> 
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4"> 
                        <div class="col-md-12"> 
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="ruang_tamu" name="ruang_tamu" type="checkbox"> 
                                <label for="ruang_tamu">Ruang Tamu </label> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="ruang_keluarga" name="ruang_keluarga" type="checkbox"> 
                                <label for="ruang_keluarga" >Ruang Keluarga </label> 
                            </div>
                        </div> 
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="extra_bed" name="extra_bed" type="checkbox"> 
                                <label for="extra_bed" > Extra Bed </label> 
                            </div>
                        </div> 
                    </div> 
                     <div class="col-md-4"> 
                        <div class="col-md-12"> 
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="sofa_bed" name="sofa_bed" type="checkbox"> 
                                <label for="sofa_bed">Sofa Bed </label> 
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="wifi" name="wifi" type="checkbox"> 
                                <label for="wifi" >Free Wifi </label> 
                            </div>
                        </div> 
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="peralatan_mandi" name="peralatan_mandi" type="checkbox"> 
                                <label for="peralatan_mandi" > Paket Peralatan Mandi </label> 
                            </div>
                        </div> 
                    </div>  
                    <div class="col-md-4"> 
                        <div class="col-md-12"> 
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="paket_buah" name="paket_buah" type="checkbox"> 
                                <label for="paket_buah">Paket Buah </label> 
                            </div>
                        </div>
                    </div>  
                </div> 

                <div class="row"> 
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="saveKelas_pelayanan"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
    </div>
</div>
<!-- end modal add -->


<!-- modal edit -->
<div id="modal_update_kelas_pelayanan" class="modal fade" style="overflow: hidden;">  
    <div class="modal-dialog modal-lg" > 
       <?php echo form_open('#',array('id' => 'fmUpdateKelas_pelayanan'))?> 
       <div class="modal-content" style="margin-top: 160px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Kelas Pelayanan</h2> 
            </div>      
            <div class="modal-body" style="height: 500px;overflow: auto;">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">  
                    <div class="form-group col-md-12">
                        <label>Nama Kelas Pelayanan<span style="color: red">*</span></label> 
                        <input type="text" id="upd_nama_kelas_pelayanan" name="upd_nama_kelas_pelayanan" placeholder="Nama Kelas Pelayanan" class="form-control">    
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Jumlah Kasur<span style="color: red">*</span></label>  
                        <input type="text" id="upd_isi_bed" name="upd_isi_bed" placeholder="Jumlah Kasur" class="form-control">
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Luas Ruangan<span style="color: red">*</span></label>  
                        <input type="text" id="upd_luas_ruangan" name="upd_luas_ruangan" placeholder="Luas Ruangan" class="form-control">
                    </div>    
                    <div class="form-group col-md-12">
                        <label>Tarif<span style="color: red">*</span></label>  
                        <input type="number" id="upd_tarif" name="upd_tarif" placeholder="Tarif" class="form-control">
                    </div> 
                </div>   
                <h3 style="margin-top: -10px">Fasilitas</h3><hr style="margin-top: -10px"/>
                <div class="row"> 
                    <div class="col-md-12" style="border-bottom: 1px solid #f1f1f1;">
                            <div class="checkbox checkbox-success pull-left p-t-0">
                                <input id="upd_ceklis_semua" type="checkbox"> 
                                <label for="upd_ceklis_semua"> Ceklis Semua </label>   
                            </div> 
                    </div>
                </div>  
                <div class="row" style="margin-top: 5px"> 
                <div class="col-md-4"> 
                    <div class="col-md-12"> 
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_kamar_mandi" name="upd_kamar_mandi" type="checkbox" class="ceklis"> 
                            <label for="upd_kamar_mandi"> Kamar Mandi Dalam </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_closet" name="upd_closet" type="checkbox"> 
                            <label for="upd_closet">Closed Duduk </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_wastafel" name="upd_wastafel" type="checkbox"> 
                            <label for="upd_wastafel"> Wastafel </label> 
                        </div>
                    </div> 

                </div> 
                 <div class="col-md-4"> 
                    <div class="col-md-12"> 
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_shower" name="upd_shower" type="checkbox" > 
                            <label for="upd_shower"> Shower </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_water_heater" name="upd_water_heater"type="checkbox"> 
                            <label for="upd_water_heater" > Water Heater </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_kipas_angin" name="upd_kipas_angin" type="checkbox"> 
                            <label for="upd_kipas_angin" > Kipas Angin </label> 
                        </div>
                    </div> 

                </div>  
                 <div class="col-md-4"> 
                    <div class="col-md-12"> 
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_exhause_fan" name="upd_exhause_fan" type="checkbox"> 
                            <label for="upd_exhause_fan"> Exhause Fan </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_tv" name="upd_tv" type="checkbox"> 
                            <label for="upd_tv" > LED TV Digital </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_baby_bed" name="upd_baby_bed" type="checkbox"> 
                            <label for="upd_baby_bed" > Baby Bed </label> 
                        </div>
                    </div> 
                </div>
                <div class="col-md-4"> 
                    <div class="col-md-12"> 
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_ac" name="upd_ac" type="checkbox"> 
                            <label for="upd_ac"> AC </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_kulkas" name="upd_kulkas" type="checkbox"> 
                            <label for="upd_kulkas" > Kulkas </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_lemari" name="upd_lemari" type="checkbox"> 
                            <label for="upd_lemari" >Lemari </label> 
                        </div>
                    </div> 
                </div>
                <div class="col-md-4"> 
                    <div class="col-md-12"> 
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_ruang_tamu" name="upd_ruang_tamu" type="checkbox"> 
                            <label for="upd_ruang_tamu">Ruang Tamu </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_ruang_keluarga" name="upd_ruang_keluarga" type="checkbox"> 
                            <label for="upd_ruang_keluarga" >Ruang Keluarga </label> 
                        </div>
                    </div> 
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_extra_bed" name="upd_extra_bed" type="checkbox"> 
                            <label for="upd_extra_bed" > Extra Bed </label> 
                        </div>
                    </div> 
                </div> 
                 <div class="col-md-4"> 
                    <div class="col-md-12"> 
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_sofa_bed" name="upd_sofa_bed" type="checkbox"> 
                            <label for="upd_sofa_bed">Sofa Bed </label> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_wifi" name="upd_wifi" type="checkbox"> 
                            <label for="upd_wifi" >Free Wifi </label> 
                        </div>
                    </div> 
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_peralatan_mandi" name="upd_peralatan_mandi" type="checkbox"> 
                            <label for="upd_peralatan_mandi" > Paket Peralatan Mandi </label> 
                        </div>
                    </div> 
                </div>
                <div class="col-md-4"> 
                    <div class="col-md-12"> 
                        <div class="checkbox checkbox-success pull-left p-t-0">
                            <input id="upd_paket_buah" name="upd_paket_buah" type="checkbox"> 
                            <label for="upd_paket_buah">Paket Buah </label> 
                        </div>
                    </div>
                </div>  
            </div> 

                <div class="row"> 
                    <div class="col-md-12">    
                        <input type="hidden" id="upd_id_kelas_pelayanan" name="upd_id_kelas_pelayanan" class="validate">
                        <button type="button" class="btn btn-success pull-right" id="changeKelas_pelayanan"><i class="fa fa-floppy-o p-r-10"></i> PERBARUI</button>
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
    </div>
</div>
<!-- end modal edit   -->


<!-- modal detail fasilitas -->
<div id="modal_detail_fasilitas" class="modal fade" style="overflow: hidden;">  
    <div class="modal-dialog modal-lg" > 
       <div class="modal-content" style="margin-top: "> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Detail Pelayanan</h2> 
            </div>      
            <div class="modal-body" style="height: 500px;overflow: auto;">
                <div class="row">    
                    <div class="col-md-12">  
                        <div class="panel panel-info1">
                            <div class="panel-heading" align="center"> Detail Fasilitas Pelayanan 
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body" style="border:1px solid #f5f5f5">
                                    <div class="row" style="margin-top: 5px"> 
                                    <div class="col-md-4"> 
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_kamar_mandi" name="upd_kamar_mandi" type="checkbox" class="ceklis"> 
                                                <label for="kamar-mandi-dalam"> Kamar Mandi Dalam </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_closet" name="upd_closet" type="checkbox"> 
                                                <label for="closed-duduk">Closed Duduk </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_wastafel" name="upd_wastafel" type="checkbox"> 
                                                <label for="Wastafel"> Wastafel </label> 
                                            </div>
                                        </div> 
                    
                                    </div> 
                                     <div class="col-md-4"> 
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_shower" name="upd_shower" type="checkbox" > 
                                                <label for="Shower"> Shower </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_water_heater" name="upd_water_heater"type="checkbox"> 
                                                <label for="water-heater" > Water Heater </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_kipas_angin" name="upd_kipas_angin" type="checkbox"> 
                                                <label for="kipas-angin" > Kipas Angin </label> 
                                            </div>
                                        </div> 
                    
                                    </div>  
                                     <div class="col-md-4"> 
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_exhause_fan" name="upd_exhause_fan" type="checkbox"> 
                                                <label for="Exhause-pan"> Exhause Fan </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_tv" name="upd_tv" type="checkbox"> 
                                                <label for="led-tv" > LED TV Digital </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_baby_bed" name="upd_baby_bed" type="checkbox"> 
                                                <label for="baby-bed" > Baby Bed </label> 
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4"> 
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_ac" name="upd_ac" type="checkbox"> 
                                                <label for="ac"> AC </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_kulkas" name="upd_kulkas" type="checkbox"> 
                                                <label for="kulkas" > Kulkas </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_lemari" name="upd_lemari" type="checkbox"> 
                                                <label for="lemari" >Lemari </label> 
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4"> 
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_ruang_tamu" name="upd_ruang_tamu" type="checkbox"> 
                                                <label for="ruangtamu">Ruang Tamu </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_ruang_keluarga" name="upd_ruang_keluarga" type="checkbox"> 
                                                <label for="ruang-keluarga" >Ruang Keluarga </label> 
                                            </div>
                                        </div> 
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_extra_bed" name="upd_extra_bed" type="checkbox"> 
                                                <label for="extra-bed" > Extra Bed </label> 
                                            </div>
                                        </div> 
                                    </div> 
                                     <div class="col-md-4"> 
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_sofa_bed" name="upd_sofa_bed" type="checkbox" readonly> 
                                                <label for="sofa-bed">Sofa Bed </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_wifi" name="upd_wifi" type="checkbox" readonly> 
                                                <label for="free-wifi" >Free Wifi </label> 
                                            </div>
                                        </div> 
                                        <div class="col-md-12">
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_peralatan_mandi" name="upd_peralatan_mandi" type="checkbox" readonly> 
                                                <label for="Peralatan-mandi" > Paket Peralatan Mandi </label> 
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4"> 
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input disabled id="det_paket_buah" name="upd_paket_buah" type="checkbox" readonly> 
                                                <label for="paket-buah">Paket Buah </label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <p><b>Ket : </b></p>
                                        </div>
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input id="" name="upd_paket_buah" checked type="checkbox" disabled> 
                                                <label for="paket-buah"><p><b>= Ya </b></p></label> 
                                            </div>
                                        </div>
                                        <div class="col-md-12"> 
                                            <div class="checkbox checkbox-success pull-left p-t-0">
                                                <input id="det_paket_buaha" n type="checkbox" disabled> 
                                                <label for="paket-buah"><p><b>= Tidak </b></p></label> 
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>
      