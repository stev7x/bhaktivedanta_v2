<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Daftar Tindakan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Daftar Tindakan</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">   
                            <div class="panel-heading"> Master Daftar Tindakan   
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <div class="col-md-6">
                                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_daftar_tindakan" data-backdrop="static" data-keyboard="false">Tambah Daftar Tindakan</button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Daftar Tindakan</button>
                                                </div>
                                                <div class="col-md-6"> 
                                                <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>

                                                    &nbsp;&nbsp;&nbsp;  
                                                   <input accept=".xls, .xlsx, .csv" type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected"  onchange="showbtnimport()" />  
                                                    <label for="file" style="float: right;"><span></span> <strong>     
                                                       <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                   <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                         
                                                <?php echo form_close(); ?>  
                                                </div>

                                            </div> 
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />    
                                           <table id="table_daftar_tindakan_list" class="table table-striped" cellspacing="0">  
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Daftar Tindakan</th>
                                                        <th>Kelompok Tindakan</th>
                                                        <th>Pelayanan</th>
                                                        <th>Komponen Tarif</th>
                                                        <th>Harga Tindakan</th>
                                                        <th>Cyto Tindakan</th>
                                                        <th>BPJS</th> 
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Daftar Tindakan</th>
                                                        <th>Kelompok Tindakan</th>
                                                        <th>Pelayanan</th>
                                                        <th>Komponen Tarif</th>
                                                        <th>Harga Tindakan</th>
                                                        <th>Cyto Tindakan</th>
                                                        <th>BPJS</th> 
                                                        <th>Action</th>
                                                    </tr>   
                                                </tfoot>
                                            </table>  
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- Modal  -->
<div id="modal_add_daftar_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateTindakan'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Daftar Tindakan</h2> 
            </div>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <div class="col-md-6">
                        <div class="form-group"> 
                            <label>Kelompok Tindakan<span style="color: red">*</span></label>
                            <select name="kelompoktindakan" id="kelompoktindakan" class="form-control">
                            <option value="" dsiabled selected>Pilih Kelompok Tindakan</option>
                            <?php
                            $list_kelompoktindakan = $this->Daftar_tindakan_model->get_kelompoktindakan_list();
                            foreach($list_kelompoktindakan as $list){
                                echo "<option value=".$list->kelompoktindakan_id.">".$list->kelompoktindakan_nama."</option>";
                            }
                            ?>
                            </select>  
                        </div> 
                        <div class="form-group">
                            <label>List Nama Tindakan</label>  
                            <select name="list_nama_daftar_tindakan" id="list_nama_daftar_tindakan" class="form-control select2">
                                <option disabled selected>Pilih Nama Tindakan</option>
                                <?php    
                                $list_daftartindakan = $this->Daftar_tindakan_model->get_name_tindakan(); 
                                foreach ($list_daftartindakan as $list) {     
                                    echo "<option value=".$list->daftartindakan_id.">".$list->daftartindakan_nama."</option>";
                                }
                                ?>
                            </select>      
                            <button type="button" class="btn btn-success " style="margin-top:5px;padding: 5px" onclick="showNamaTindakan()"><i class="fa fa-plus p-r-10"></i> Nama Tindakan</button> 
                        </div>      
                        <div class="form-group" id="tag_nama_tindakan" style="display: none;">
                            <label>Nama Tindakan<span style="color: red">*</span></label>    
                            <input type="text" name="nama_daftar_tindakan" id="nama_daftar_tindakan" class="form-control" placeholder="Nama Tindakan"> 
                        </div>
                         <div class="form-group "> 
                            <label>Kelas<span style="color: red">*</span></label>
                            <select id="kelaspelayanan_nama" name="kelaspelayanan_nama" class="form-control">
                                <option value="" disabled selected>Pilih Kelas</option> 
                                <?php
                                $list_kelas = $this->Daftar_tindakan_model->get_kelas_list();
                                foreach($list_kelas as $list){
                                    echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                }
                                ?> 
                            </select>   
                        </div>
                        <!-- TODO :: Combo box ICD 9 -->
                        <div class="form-group "> 
                            <label>ICD 9<span style="color: red">*</span></label>
                            <select id="icd9_cm_id" name="icd9_cm_id" class="form-control select2">
                                <option value="" disabled selected>Pilih ICD 9</option> 
                                    <?php
                                    $list_kelas = $this->Daftar_tindakan_model->get_icd_9_cm();
                                    foreach($list_kelas as $list){
                                        echo "<option value=".$list->diagnosa_id.">".$list->nama_diagnosa." (".$list->kode_diagnosa.")</option>";
                                    }
                                    ?> 
                            </select>   
                        </div>
                    </div> 
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label for="">Total Harga BPJS Cyto</label>
                            <div class="form-control" style="border:none" id="total_bpjs_cyto">Rp.0</div>
                        </div>
                        <div class="form-group">
                            <label for="">Total Harga BPJS Non Cyto</label>
                            <div class="form-control" style="border:none" id="total_bpjs_non_cyto">Rp.0</div>
                        </div>
                        <div class="form-group">
                            <label for="">Total Harga Umum Cyto</label>
                            <div class="form-control" style="border:none" id="total_umum_cyto">Rp.0</div>
                        </div>
                        <div class="form-group">
                            <label for="">Total Harga Umum Non Cyto</label>
                            <div class="form-control" style="border:none" id="total_umum_non_cyto">Rp.0</div>
                        </div>
                    </div>
                </div><br><br>
                <div class="container-fluid">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active nav-item"><a href="#bhp" class="nav-link" aria-controls="bhp" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">BHP</span></a></li>
                        <li role="presentation" class="nav-item"><a href="#komponen-tarif" class="nav-link" aria-controls="komponen-tarif" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-user-md"></i></span><span class="hidden-xs"> Komponen Tarif</span></a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="bhp">
                            <div class="row">
                                <table id="table_bhp_alkes" class="table table-striped dataTable" cellspacing="0" role="grid" style="width: 1106px;">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 50px;">No</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">Kode</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 64px;">Nama</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Jumlah</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Satuan</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_bhp_alkes">   
                                        <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>
                                    </tbody> 
                                    <tfoot>
                                        <tr>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 50px;">No</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">Kode</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 64px;">Nama</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Jumlah</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Satuan</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Hapus</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><br>
                            <div class="row"> 
                                <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_bhp" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <div>
                                        <p id="card_message_bhp"></p>
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <form action="http://localhost/puri-bunda-app/#" id="fmCreateResepPasien" method="post" accept-charset="utf-8">
                                    <input type="hidden" id="ci_csrf_token_delres" name="ci_csrf_token_delres" value="">
                                    <div class="row">   
                                        <div class="form-group col-sm-4">   
                                            <div class="form-group">
                                                <select required name="pilih_bhp" id="pilih_bhp" class="form-control select">
                                                    <option value="0" dsiabled selected>Pilih BHP / Alkes</option>
                                                    <?php
                                                        $list_bhp_alkes = $this->Daftar_tindakan_model->get_bhp_dan_alkes();
                                                        foreach ($list_bhp_alkes as $list_ba) {
                                                            echo "<option value=".$list_ba->id_barang.">".$list_ba->kode_barang."-".$list_ba->nama_barang."</option>";
                                                        }
                                                    ?>
                                                </select> 
                                            </div>
                                        </div> 
                                        <div class="form-group col-sm-4">
                                            <input required="" type="text" class="form-control" id="nama_barang_bhp" name="nama_barang_bhp" placeholder="Nama Barang" readonly="">
                                        </div> 
                                        <div class="form-group col-sm-4">
                                            <input style="margin : 0; margin-right: 5%; float: left; width: 45%;" required="" type="number" class="form-control" id="jumlah_bhp" name="jumlah_bhp" placeholder="Jumlah">
                                            <div class="form-group" style="margin : 0; float: left; width: 45%;">
                                                <select required name="satuan_bhp" id="satuan_bhp" class="form-control select">
                                                    <option value="0" dsiabled selected>Satuan</option>
                                                    <?php
                                                        $list_sediaan = $this->Daftar_tindakan_model->get_sediaan_obat();
                                                        foreach ($list_sediaan as $list) {
                                                            echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12">
                                            <button type="button" class="btn btn-success" id="saveBHP"><i class="fa fa-floppy-o"></i> TAMBAH BHP</button>  
                                        </div>
                                    </div>
                                </form>
                            </div><br><br>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="komponen-tarif">
                            <div class="row">
                                <table id="table_komponen_tarif" class="table table-striped" cellspacing="0" role="grid" style="width: 1106px;">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 50px;">Komponen Tarif</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">BPJS-Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 64px;">BPJS-Non Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Umum Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Umum-Non Cyto</th>
                                        </tr>
                                    </thead>
                                    <tbody>   
                                        <form id="komponentarif">
                                        <?php
                                            $list_komponen = $this->Daftar_tindakan_model->get_komponen_tarif();
                                            foreach($list_komponen as $list){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $list->komponentarif_nama;?>
                                                        <input type="hidden" name="komponentarif_nama[]" value="<?php echo $list->komponentarif_nama;?>">
                                                    </td>
                                                    <td><input type="number" name="bpjs_cyto[]" value="0" class="form-control" onkeyup="getValueKomponenTarif()"></td>
                                                    <td><input type="number" name="bpjs_non_cyto[]" value="0" class="form-control"></td>
                                                    <td><input type="number" name="umum_cyto[]" value="0" class="form-control"></td>
                                                    <td><input type="number" name="umum_non_cyto[]" value="0" class="form-control"></td>
                                                </tr>
                                                <?php
                                            }
                                        ?>
                                        </form>
                                    </tbody> 
                                    <tfoot>
                                        <tr>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 50px;">Komponen Tarif</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">BPJS-Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 64px;">BPJS-Non Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Umum Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Umum-Non Cyto</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveDaftarTindakan"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div> 
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?> 
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

<!-- Modal  -->      
<div id="modal_edit_daftar_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmUpdateTindakan'))?>
       <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Edit Daftar Tindakan</h2> 
            </div>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p> 
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <div class="col-md-6">
                        <div class="form-group">     
                             <input type="hidden" id="upd_id_daftar_tindakan" name="upd_id_daftar_tindakan" class="validate"> 
                            <label>Kelompok Tindakan<span style="color: red">*</span></label> 
                            <select name="upd_kelompoktindakan" id="upd_kelompoktindakan" class="form-control">
                            <option value="" dsiabled selected>Pilih Kelompok Tindakan</option>
                            <?php
                            $list_kelompoktindakan = $this->Daftar_tindakan_model->get_kelompoktindakan_list();
                            foreach($list_kelompoktindakan as $list){
                                echo "<option value=".$list->kelompoktindakan_id.">".$list->kelompoktindakan_nama."</option>";
                            }
                            ?> 
                            </select>  
                        </div> 
                        <div class="form-group ">
                            <label>Nama Tindakan<span style="color: red">*</span></label> 
                            <input type="text" name="upd_nama_daftar_tindakan" id="upd_nama_daftar_tindakan" class="form-control" placeholder="Nama Tindakan">  
                        </div>
                         <div class="form-group "> 
                            <label>Kelas<span style="color: red">*</span></label>
                            <select id="upd_kelaspelayanan_nama" name="upd_kelaspelayanan_nama" class="form-control">
                                <option value="" disabled selected>Pilih Kelas</option>
                                <?php
                                $list_kelas = $this->Daftar_tindakan_model->get_kelas_list();
                                foreach($list_kelas as $list){
                                    echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                }
                                ?> 
                            </select>   
                        </div>
                        <!-- TODO :: Combo box ICD 9 -->
                        <div class="form-group "> 
                            <label>ICD 9<span style="color: red">*</span></label>
                            <select id="upd_icd9_cm_id" name="upd_icd9_cm_id" class="form-control">
                                <option value="" disabled selected>Pilih ICD 9</option> 
                                <?php
                                $list_kelas = $this->Daftar_tindakan_model->get_icd_9_cm();
                                foreach($list_kelas as $list){
                                    echo "<option value=".$list->diagnosa_id.">".$list->nama_diagnosa." (".$list->kode_diagnosa.")</option>";
                                }
                                ?> 
                            </select>   
                        </div>
                    </div> 
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label for="">Total Harga BPJS Cyto</label>
                            <div class="form-control" style="border:none" id="total_bpjs_cyto_edit">Rp.0</div>
                        </div>
                        <div class="form-group">
                            <label for="">Total Harga BPJS Non Cyto</label>
                            <div class="form-control" style="border:none" id="total_bpjs_non_cyto_edit">Rp.0</div>
                        </div>
                        <div class="form-group">
                            <label for="">Total Harga Umum Cyto</label>
                            <div class="form-control" style="border:none" id="total_umum_cyto_edit">Rp.0</div>
                        </div>
                        <div class="form-group">
                            <label for="">Total Harga Umum Non Cyto</label>
                            <div class="form-control" style="border:none" id="total_umum_non_cyto_edit">Rp.0</div>
                        </div>
                    </div> 
                </div><br><br>
                <div class="container-fluid">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active nav-item"><a href="#bhp_edit" class="nav-link" aria-controls="bhp_edit" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">BHP</span></a></li>
                        <li role="presentation" class="nav-item"><a href="#komponen-tarif_edit" class="nav-link" aria-controls="komponen-tarif_edit" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-user-md"></i></span><span class="hidden-xs"> Komponen Tarif</span></a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="bhp_edit">
                            <div class="row">
                                <table id="table_bhp_alkes_edit" class="table table-striped" cellspacing="0" role="grid" style="width: 1106px;">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 50px;">No</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">Kode</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 64px;">Nama</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Jumlah</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Satuan</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_bhp_alkes_edit">   
                                        <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>
                                    </tbody> 
                                    <tfoot>
                                        <tr>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 50px;">No</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">Kode</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 64px;">Nama</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Jumlah</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Satuan</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Hapus</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><br>
                            <div class="row"> 
                                <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_bhp_edit" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <div>
                                        <p id="card_message_bhp_edit"></p>
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <form action="http://localhost/puri-bunda-app/#" id="fmCreateResepPasien" method="post" accept-charset="utf-8">
                                    <input type="hidden" id="ci_csrf_token_delres" name="ci_csrf_token_delres" value="">
                                    <div class="row">   
                                        <div class="form-group col-sm-4">   
                                            <div class="form-group">
                                                <select required name="pilih_bhp" id="pilih_bhp_edit" class="form-control select">
                                                    <option value="0" dsiabled selected>Pilih BHP / Alkes</option>
                                                    <?php
                                                        $list_bhp_alkes = $this->Daftar_tindakan_model->get_bhp_dan_alkes();
                                                        foreach ($list_bhp_alkes as $list_ba) {
                                                            echo "<option value=".$list_ba->id_barang.">".$list_ba->kode_barang."-".$list_ba->nama_barang."</option>";
                                                        }
                                                    ?>
                                                </select> 
                                            </div>
                                        </div> 
                                        <div class="form-group col-sm-4">
                                            <input required="" type="text" class="form-control" id="nama_barang_bhp_edit" name="nama_barang_bhp" placeholder="Nama Barang" readonly="">
                                        </div> 
                                        <div class="form-group col-sm-4">
                                            <input style="margin : 0; margin-right: 5%; float: left; width: 45%;" required="" type="number" class="form-control" id="jumlah_bhp_edit" name="jumlah_bhp" placeholder="Jumlah">
                                            <div class="form-group" style="margin : 0; float: left; width: 45%;">
                                                <select required name="satuan_bhp" id="satuan_bhp_edit" class="form-control select">
                                                    <option value="0" dsiabled selected>Satuan</option>
                                                    <?php
                                                        $list_sediaan = $this->Daftar_tindakan_model->get_sediaan_obat();
                                                        foreach ($list_sediaan as $list) {
                                                            echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12">
                                            <button type="button" class="btn btn-success" id="saveBHPEdit"><i class="fa fa-floppy-o"></i> TAMBAH BHP</button>  
                                        </div>
                                    </div>
                                </form>
                            </div><br><br>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="komponen-tarif_edit">
                            <div class="row">
                                <table id="table_komponen_tarif_edit" class="table table-striped" cellspacing="0" role="grid" style="width: 1106px;">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 50px;">Komponen Tarif</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">BPJS-Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 64px;">BPJS-Non Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Umum Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Umum-Non Cyto</th>
                                        </tr>
                                    </thead>
                                    <tbody>   
                                        <form id="komponentarif_edit">
                                        <?php
                                            $list_komponen = $this->Daftar_tindakan_model->get_komponen_tarif();
                                            foreach($list_komponen as $list){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $list->komponentarif_nama;?>
                                                        <input type="hidden" name="komponentarif_nama_edit[]" value="<?php echo $list->komponentarif_nama;?>">
                                                    </td>
                                                    <td><input type="number" name="bpjs_cyto_edit[]" value="0" class="form-control" onkeyup="getValueKomponenTarifEdit()"></td>
                                                    <td><input type="number" name="bpjs_non_cyto_edit[]" value="0" class="form-control"></td>
                                                    <td><input type="number" name="umum_cyto_edit[]" value="0" class="form-control"></td>
                                                    <td><input type="number" name="umum_non_cyto_edit[]" value="0" class="form-control"></td>
                                                </tr>
                                                <?php
                                            }
                                        ?>
                                        </form>
                                    </tbody> 
                                    <tfoot>
                                        <tr>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 50px;">Komponen Tarif</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 124px;">BPJS-Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 64px;">BPJS-Non Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 84px;">Umum Cyto</th>
                                            <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 87px;">Umum-Non Cyto</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="changeDaftarTindakan"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div> 
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?> 
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

<?php $this->load->view('footer');?>
      