<?php $this->load->view('header_iframe');?>

            <!-- row -->
                <div class="row">  
                    <div class="col-md-6">  
                        <!-- panel data karyawan -->
                        <div class="panel panel-info1">
                            <div class="panel-heading" align="center"> Data Karyawan     
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body" style="border:1px solid #f5f5f5">
                                    <div class="row">   
                                        <div class="col-md-12"> 
                                            <table  max-width="100%" align="left" style="font-size:16px;text-align: left;">       
                                                <tr>        
                                                    <td valign="top"><b>No Karyawan</b></td>     
                                                    <td valign="top" style="padding-left:10px;">:</td> 
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->no_karyawan?></td>
                                                </tr>   
                                                <tr> 
                                                    <td valign="top"><b>No.Rekam Medis</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->no_rekam_medis?></td>
                                                </tr>
                                                <tr> 
                                                    <td valign="top"><b>No.KTP</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->no_ktp?></td>
                                                </tr>     
                                                <tr>
                                                    <td valign="top"><b>Nama Lengkap Karyawan</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->nama_karyawan?></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><b>Nama Panggilan Karyawan</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->nama_panggil_karyawan?> </td>
                                                </tr>  
                                                <tr>
                                                    <td valign="top"><b>Tempat Lahir</b></td> 
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->tempat_lahir?></td>
                                                </tr>  
                                                <tr>
                                                    <td valign="top"><b>Tanggal Lahir</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($data_karyawan->tanggal_lahir))?></td>
                                                </tr> 
                                                <tr>
                                                    <td valign="top"><b>Jenis Kelamin</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->jenis_kelamin?></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><b>No.Rek Mandiri</b></td> 
                                                    <td valign="top" style="padding-left:10px;">:</td> 
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->no_rek_mandiri?></td>
                                                </tr>    
                                                <tr>
                                                    <td valign="top"><b>No.Bpjs</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>
                                                    <td valign="top" style="padding-left: 15px">
                                                        <?= $data_karyawan->no_bpjs?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><b>No.BPJS Ketenagakerjaan</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>
                                                    <td valign="top" style="padding-left: 15px"> <?= $data_karyawan->no_bpjs_ketker?></td>     
                                                </tr>
                                                <tr>
                                                    <td valign="top"><b>Agama</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td> 
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->agama_id?></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><b>Status Kawin</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>  
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->status_kawin?></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><b>Email</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>  
                                                    <td  style="padding-left: 15px">
                                                        <?= $data_karyawan->email?>
                                                    </td>
                                                </tr>  
                                                <tr>
                                                    <td valign="top"><b>Alamat</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->alamat?></td>
                                                </tr>   
                                               <!--  <tr> 
                                                    <td valign="top"><b>Jabatan</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>        
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->no_rek_mandiri?></td>
                                                </tr> -->
                                                  
                                            </table>   
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>  
                        <!-- end panel data karyawan -->
                    </div> 
                    <div class="col-md-6">  
                        <!-- panel data Status Kepegawaian -->    
                        <div class="panel panel-info1">
                            <div class="panel-heading" align="center">Status Kepegawaian     
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body" style="border:1px solid #f5f5f5">
                                    <div class="row">   
                                        <div class="col-md-6"> 
                                            <table  max-width="100%" align="left" style="font-size:16px;text-align: left;">       
                                                <tr>   
                                                    <td valign="top"><b>Orientasi</b></td>  
                                                    <td valign="top" style="padding-left:10px;">:</td>   
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->orientasi?></td>
                                                </tr>
                                                <tr> 
                                                    <td valign="top"><b>Lama Orientasi</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>     
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->lama_orientasi?></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><b>TMT</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>
                                                    <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($data_karyawan->tmt))?></td>
                                                </tr>     
                                                <tr>
                                                    <td valign="top"><b>Masa Kerja</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>
                                                    <td valign="top" style="padding-left: 15px">
                                                    <?php 
                                                        $habis = $data_karyawan->tgl_berakhir;
                                                        $expired  = new Datetime($habis);
                                                        $today = new Datetime();
                                                        $diff  = $today->diff($expired);
                                                        echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";

                                                    ?>
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"><b>Keluar</b></td>
                                                    <td valign="top"  style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($data_karyawan->keluar))?></td>
                                                </tr>  
                                                <tr>
                                                    <td valign="top"><b>Alasan Keluar</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->alasan_keluar?></td>
                                                </tr>  
                                                <tr>
                                                    <td valign="top"><b>Unit Kerja</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"></td>
                                                </tr> 
                                                <tr>
                                                    <td valign="top"><b>Ruang</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->ruang?></td>
                                                </tr>
                                                 <tr>
                                                    <td ><b>Penilaian Resiko</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->penilaian_resiko?></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top"><b>Status Pegawai</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->status_pegawai?></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top"><b>No.Kontrak</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->no_kontrak?></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top"><b>Tgl.Berakhir</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($data_karyawan->tgl_berakhir))?></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top"><b>Kelompok Intensif</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->kelompok_intensif?></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top"><b>Jabatan</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->jabatan?></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top"><b>Status</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->status?></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top"><b>Hak Bersama</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->hak_bersama?></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top"><b>Hak Unit</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>       
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->hak_unit?></td>
                                                </tr>
                                                 <tr>       
                                                    <td valign="top"><b>Penempatan</b></td>
                                                    <td valign="top" style="padding-left:10px;">:</td>         
                                                    <td valign="top" style="padding-left: 15px"><?= $data_karyawan->penempatan?></td>
                                                </tr>
                                                  
                                            </table>   
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>      
                        <!-- end panel data Status Kepegawaian -->
                    </div>

                </div>
                <!-- end row -->

                <!-- row -->
                <div class="row">  
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <!-- panel data riwayat pendidikan umum -->
                                <div class="panel panel-info1">
                                    <div class="panel-heading" align="center">  Riwayat Pendidikan Umum     
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body" style="border:1px solid #f5f5f5">
                                            <div class="row">   
                                                <div class="col-md-12"> 
                                                    <table  max-width="100%" align="left" style="font-size:16px;text-align: left;">       
                                                        <tr>         
                                                            <td valign="top"><b>Nama Sekolah</b></td> 
                                                            <td valign="top" style="padding-left:10px;">:</td> 
                                                            <td valign="top" style="padding-left: 15px"><?= $pendidikan_umum->nama_sekolah ?></td>
                                                        </tr>
                                                        <tr> 
                                                            <td valign="top"><b>Jurusan</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= $pendidikan_umum->jurusan ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top"><b>Tahun Lulus</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($pendidikan_umum->tahun_lulus)) ?></td>
                                                        </tr>     
                                                        <tr>
                                                            <td valign="top"><b>No.Ijazah</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= $pendidikan_umum->no_ijazah ?></td>  
                                                        </tr>
                                                        <tr>
                                                            <td valign="top"><b>Tgl.Terverifikasi</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>       
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($pendidikan_umum->tgl_terverifikasi)) ?></td>
                                                        </tr>  
                                                        <tr>
                                                            <td valign="top"><b>Strata</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>           
                                                            <td valign="top" style="padding-left: 15px"><?= $pendidikan_umum->strata ?></td>
                                                        </tr>  
                                                    </table>   
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div> 
                                <!-- end panel data riwayat pendidikan umum -->
                            </div>
                            <div class="col-md-4">
                                <!-- panel data riwayat pendidikan profesi -->
                                <div class="panel panel-info1">
                                    <div class="panel-heading" align="center">  riwayat pendidikan profesi     
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body" style="border:1px solid #f5f5f5">
                                            <div class="row">    
                                                <div class="col-md-12">   
                                                    <table  max-width="100%" align="left" style="font-size:16px;text-align: left;">       
                                                        <tr>   
                                                            <td valign="top"><b>Profesi</b></td> 
                                                            <td valign="top" style="padding-left:10px;">:</td> 
                                                            <td valign="top" style="padding-left: 15px"><?= $pendidikan_profesi->profesi ?></td>
                                                        </tr>
                                                        <tr> 
                                                            <td valign="top"><b>Spesialis</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= $pendidikan_profesi->spesialis ?></td>  
                                                        </tr>
                                                        <tr>
                                                            <td valign="top"><b>Tahun Lulus</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($pendidikan_profesi->tahun)) ?></td>
                                                        </tr>     
                                                        <tr>
                                                            <td valign="top"><b>No.Ijazah</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= $pendidikan_profesi->no_ijazah ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top"><b>Tgl.Terverifikasi</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>       
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($pendidikan_profesi->tgl_terverifikasi)) ?></td>
                                                        </tr>  
                                                    </table>   
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>  
                                <!-- end panel data riwayat pendidikan profesi -->
                            </div>
                            <div class="col-md-4">
                                <!-- panel data riwayat pendidikan profesi -->
                                <div class="panel panel-info1">
                                    <div class="panel-heading" align="center"> Izin Profesi     
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body" style="border:1px solid #f5f5f5">
                                            <div class="row">    
                                                <div class="col-md-12">      
                                                    <table  max-width="100%" align="left" style="font-size:16px;text-align: left;">       
                                                        <tr>   
                                                            <td valign="top"><b>No.Str</b></td> 
                                                            <td valign="top" style="padding-left:10px;">:</td> 
                                                            <td valign="top" style="padding-left: 15px"><?= $izin_profesi->no_str ?></td>
                                                        </tr>
                                                        <tr>   
                                                            <td valign="top"><b>No.SIP</b></td> 
                                                            <td valign="top" style="padding-left:10px;">:</td>  
                                                            <td valign="top" style="padding-left: 15px"><?= $izin_profesi->no_sip ?></td>
                                                        </tr>
                                                        <tr> 
                                                            <td valign="top"><b>Tgl.Diterbitkan</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($izin_profesi->tgl_diterbitkan)) ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top"><b>Berlaku S.d</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($izin_profesi->berlaku_sd))?></td>
                                                        </tr>     
                                                        <tr>
                                                            <td valign="top"><b>Tgl.Verifikasi STR</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($izin_profesi->tgl_terverifikasi)) ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top"><b>Tgl.Diterbitkan SIP</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>       
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($izin_profesi->tgl_diterbitkan_sip)) ?></td>
                                                        </tr>  
                                                        <tr>
                                                            <td valign="top"><b>Berlaku S.d SIP</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>       
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($izin_profesi->berlaku_sd_sip)) ?></td>
                                                        </tr>  
                                                        <tr>
                                                            <td valign="top"><b>Tgl.Verifikasi SIP</b></td>
                                                            <td valign="top" style="padding-left:10px;">:</td>         
                                                            <td valign="top" style="padding-left: 15px"><?= date('d-m-Y',strtotime($izin_profesi->tgl_terverifikasi_sip)) ?></td> 
                                                        </tr>   
                                                    </table>    
                                                </div>
                                            </div>
                                        </div>     
                                    </div>
                                </div>  
                                <!-- end panel data riwayat pendidikan profesi -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->  
<?php $this->load->view('footer_iframe');?>