<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Kamar Ruangan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master Kamar Ruangan</a></li>
                            <li class="active">Kamar</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Master Kamar Ruangan  
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_kamar" data-backdrop="static" data-keyboard="false">Tambah Kamar Ruangan</button>
                                                 &nbsp;&nbsp;&nbsp;
                                                <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Kamar</button>
                                            
                                            </div> 
                                           <table id="table_kamar_list" class="table table-striped table-responsive dataTable" cellspacing="0" > 
                                                <thead>
                                                    <tr>
                                                        <th rowspan="2">No</th>
                                                        <th rowspan="2">Kelas Pelayanan</th>
                                                        <th rowspan="2">Ruangan</th>
                                                        <th rowspan="2">No Kamar</th>
                                                        <th rowspan="2">No Bed</th>
                                                        <th rowspan="2">Status Terpakai</th>
                                                        <th rowspan="2">Status Aktif</th>
                                                        <th colspan="2">Peruntukan</th>
                                                        <th rowspan="2">Lokasi</th>
                                                        <th rowspan="2">Instalasi</th>
                                                        <th rowspan="2">Penanggung Jawab</th>
                                                        <th rowspan="2">Status Kamar</th>
                                                        <th rowspan="2" style="width:50px;">Action</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Usia</th>
                                                        <th>jenis kelamin</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                
                                            </table>  
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- Modal  -->
<div id="modal_add_kamar" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateKamar'))?>   
       <div class="modal-content" style="margin-top: "> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel reloadTableKamar">Tambah Kamar</h2> 
            </div>
            <div class="modal-body" style="overflow: auto;height: 500px"> 
                <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                    <div>
                        <p id="card_message" class=""></p>
                    </div>
                </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
              
                         <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-6">
                            <label>Ruangan<span style="color: red">*</span></label>
                            <!-- <input type="text" name="" class="form-control" placeholder="Nama Paket Operasi"> -->
                            <select name="ruangan" id="ruangan" class="form-control" onchange="getPelayanan()">
                            <?php
                            $list_instalasi = $this->Kamar_model->get_ruangan();
                            foreach($list_instalasi as $list){
                                echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                            }
                            ?>
                            </select>
                        </div>  
                        <div class="form-group col-md-6"> 
                            <label>Kelas Pelayanan<span style="color: red">*</span></label>
                            <select  name="kelas_pelayanan" id="kelas_pelayanan"class="form-control validate browser-default">
                                <option value="" disabled selected>Kelas Pelayanan</option>
                                <?php
                                    $list_tindakan = $this->Kamar_model->get_pelayanan_list();
                                    foreach($list_tindakan as $list){
                                        echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                    } 
                                ?>
                            </select>
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Peruntukan<span style="color: red">*</span></label>
                            <!-- <input type="text" name="" class="form-control" placeholder="Nama Paket Operasi"> -->
                            <select name="usia" id="usia" class="form-control">
                            <option disabled selected>Pilih Peruntukan</option>
                            <?php
                                foreach($list_peruntukan as $list){
                                    echo "<option value='".$list."'>".$list."</option>";
                                }
                            ?>  
                            </select>
                        </div>  
                        <div class="form-group col-md-6"> 
                            <label>Jenis Kelamin<span style="color: red">*</span></label>
                            <select  name="jenis_kelamin" id="jenis_kelamin"class="form-control validate browser-default">
                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                <?php
                                foreach($list_kelamin as $list){
                                    echo "<option value='".$list."'>".$list."</option>";
                                }
                                ?>  
                            </select>
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Lokasi Kamar<span style="color: red">*</span></label>
                            <!-- <input type="text" name="" class="form-control" placeholder="Nama Paket Operasi"> -->
                            <select name="lokasi" id="lokasi" class="form-control">
                            <option disabled selected>Pilih Lantai</option>
                           <?php
                            foreach($list_lantai as $list){
                                echo "<option value='".$list."'>".$list."</option>";
                            }
                           ?>
                            </select>
                        </div>    
                        <div class="form-group col-md-6"> 
                            <label>Instalasi <span style="color: red">*</span></label>
                            <select  name="instalasi" id="instalasi" class="form-control validate browser-default">
                                <option value="" disabled selected>Pilih Instalasi</option>
                                <?php
                                $list_instalasii = $this->Kamar_model->get_data_instalasi();
                                foreach($list_instalasii as $list){
                                    echo "<option value='".$list->instalasi_nama."'>".$list->instalasi_nama."</option>";
                                }
                                ?>
                            </select>
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Status Kamar<span style="color: red">*</span></label>
                            <!-- <input type="text" name="" class="form-control" placeholder="Nama Paket Operasi"> -->
                            <select name="status_kamar" id="status_kamar" class="form-control">
                            <option disabled selected>Pilih status kamar</option>
                            <?php
                                foreach($list_status_kamar as $list){
                                    echo "<option value='".$list."'>".$list."</option>";
                                }
                                ?> 
                            </select>
                        </div>  
                        <div class="form-group col-md-6"> 
                            <label>Penanggung Jawab Ruangan<span style="color: red">*</span></label>
                            <input type="text" name="penanggung_jawab" id="penanggung_jawab" class="form-control" placeholder="Penanggung Jawab Ruangan">
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">  
                            <label>Nomor Kamar<span style="color: red">*</span></label>
                            <input type="text" name="nomor_kamar" class="form-control" placeholder="Nomor Kamar">
                        </div> 
                        <div class="form-group col-md-6">
                            <label>Nomor Bed<span style="color: red">*</span></label>
                            <input type="text" name="nomor_bed"  class="form-control" placeholder="Nomor Bed">
                        </div> 
                </div>
                <div class="row">
                    <div class="col-md-6">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Status Aktif<span style="color: red;">*</span></label>
                                    <div class="radio radio-success">  
                                        <input type="radio" name="status_aktif"  id="status_aktif" value="1" checked>
                                        <label for="status_aktif"> Aktif </label>
                                    </div>       
                                    <div class="radio radio-success">
                                        <input type="radio" name="status_aktif" id="status_tidak_aktif" value="0"  >
                                        <label for="status_tidak_aktif"> Tidak Aktif </label>
                                    </div> 
                                </div>  
                                <div class="form-group col-md-6">
                                    <label>Status bed<span style="color: red;">*</span></label>
                                    <div class="radio radio-success">  
                                        <input type="radio" name="status_bed" id="bed_aktif" value="1" checked>
                                        <label for="bed_aktif"> Aktif </label> 
                                    </div>                                          
                                    <div class="radio radio-success">  
                                        <input type="radio" name="status_bed" id="bed_tidak_aktif"  value="0" >
                                        <label for="bed_tidak_aktif"> Tidak Aktif </label>
                                    </div>
                                </div>  
                            </div> 
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan"  class="form-control" placeholder="Keterangan"></textarea>
                        </div> 
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveKamar"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->

 <div id="modal_edit_kamar" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmUpdateKamar'))?>   
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Perbarui Kamar</h2> 
            </div>
            <div class="modal-body" style="overflow: auto;height: 500px">
                <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                    <div>
                        <p id="card_message1" class=""></p>
                    </div>
                </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
              
                
                        <div class="form-group col-md-6">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden"  name="upd_id_kamar" id="upd_id_kamar" class="validate">
                            <label>Ruangan<span style="color: red">*</span></label>
                            <!-- <input type="text" name="" class="form-control" placeholder="Nama Paket Operasi"> -->
                            <select name="upd_ruangan" id="upd_ruangan" class="form-control" onchange="getPelayanan()">
                            
                            <?php
                            $list_instalasi = $this->Kamar_model->get_ruangan();
                            foreach($list_instalasi as $list){
                                echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                            }
                            ?>
                            </select>
                        </div>  
                        <div class="form-group col-md-6"> 
                            <label>Kelas Pelayanan<span style="color: red">*</span></label>
                            <select  name="upd_kelas_pelayanan" id="upd_kelas_pelayanan" class="form-control">
                                <option value="" disabled selected>Kelas Pelayanan</option>
                                <?php
                                    $list_tindakan = $this->Kamar_model->get_pelayanan_list();
                                    foreach($list_tindakan as $list){
                                        echo "<option value=".$list->kelaspelayanan_id.">".$list->kelaspelayanan_nama."</option>";
                                    } 
                                ?>
                            </select>
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Peruntukan<span style="color: red">*</span></label>
                            <!-- <input type="text" name="" class="form-control" placeholder="Nama Paket Operasi"> -->
                            <select name="upd_usia" id="upd_usia" class="form-control">
                            <option disabled selected>Pilih Peruntukan</option>
                            <?php
                                foreach($list_peruntukan as $list){
                                    echo "<option value='".$list."'>".$list."</option>";
                                }
                            ?>  
                            </select>
                        </div>  
                        <div class="form-group col-md-6"> 
                            <label>Jenis Kelamin<span style="color: red">*</span></label>
                            <select  name="upd_jenis_kelamin" id="upd_jenis_kelamin"class="form-control validate browser-default">
                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                <?php
                                foreach($list_kelamin as $list){
                                    echo "<option value='".$list."'>".$list."</option>";
                                }
                                ?>  
                            </select>
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Lokasi Kamar<span style="color: red">*</span></label>
                            <!-- <input type="text" name="" class="form-control" placeholder="Nama Paket Operasi"> -->
                            <select name="upd_lokasi" id="upd_lokasi" class="form-control">
                            <option disabled selected>Pilih Lantai</option>
                           <?php
                            foreach($list_lantai as $list){
                                echo "<option value='".$list."'>".$list."</option>";
                            }
                           ?>
                            </select>
                        </div>  
                        <div class="form-group col-md-6"> 
                            <label>Instalasi <span style="color: red">*</span></label>
                            <select  name="upd_instalasi" id="upd_instalasi" class="form-control validate browser-default">
                                <option value="" disabled selected>Pilih Instalasi</option>
                                <?php
                                $list_instalasii = $this->Kamar_model->get_data_instalasi();
                                foreach($list_instalasii as $list){
                                    echo "<option value='".$list->instalasi_nama."'>".$list->instalasi_nama."</option>";
                                }
                                ?>
                            </select>
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">
                            <label>Status Kamar<span style="color: red">*</span></label>
                            <!-- <input type="text" name="" class="form-control" placeholder="Nama Paket Operasi"> -->
                            <select name="upd_status_kamar" id="upd_status_kamar" class="form-control">
                            <option disabled selected>Pilih status kamar</option>
                            <?php
                                foreach($list_status_kamar as $list){
                                    echo "<option value='".$list."'>".$list."</option>";
                                }
                                ?> 
                            </select>
                        </div>  
                        <div class="form-group col-md-6"> 
                            <label>Penanggung Jawab Ruangan<span style="color: red">*</span></label>
                            <input type="text" name="upd_penanggung_jawab" id="upd_penanggung_jawab" class="form-control" placeholder="Penanggung Jawab Ruangan">
                        </div> 
                </div>
                <div class="row">
                        <div class="form-group col-md-6">  
                            <label>Nomor Kamar<span style="color: red">*</span></label>
                            <input type="text" name="upd_nomor_kamar" id="upd_nomor_kamar" class="form-control" placeholder="Nomor Kamar">
                        </div> 
                        <div class="form-group col-md-6"> 
                            <label>Nomor Bed<span style="color: red">*</span></label>
                            <input type="text" name="upd_nomor_bed" id="upd_nomor_bed" class="form-control" placeholder="Nomor Bed">
                        </div> 
                </div>
                <div class="row">
                    <div class="col-md-6">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Status Aktif<span style="color: red;">*</span></label>
                                    <div class="radio radio-success">  
                                        <input type="radio" name="upd_status_aktif" id="upd_status_aktif" value="1">
                                        <label for="upd_status_aktif"> Aktif </label>
                                    </div>       
                                    <div class="radio radio-success">
                                        <input type="radio" name="upd_status_aktif" id="upd_status_tidak_aktif" value="0" >
                                        <label for="upd_status_tidak_aktif"> Tidak Aktif </label>
                                    </div> 
                                </div>  
                                <div class="form-group col-md-6">
                                    <label>Status bed<span style="color: red;">*</span></label>
                                    <div class="radio radio-success">  
                                        <input type="radio" name="upd_status_bed" id="upd_bed_aktif" value="1">
                                        <label for="upd_bed_aktif"> Aktif </label> 
                                    </div>                                       
                                    <div class="radio radio-success">
                                        <input type="radio" name="upd_status_bed" id="upd_bed_tidak_aktif"  value="0" >
                                        <label for="upd_bed_tidak_aktif"> Tidak Aktif </label>
                                    </div>
                                </div> 
                            </div> 
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="upd_keterangan" id="upd_keterangan" class="form-control" placeholder="Keterangan"></textarea>
                        </div> 
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="changeKamar"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>

<?php $this->load->view('footer');?>
      