<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Poli Ruangan</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Master</a></li>
                            <li class="active">Poli Ruangan</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1"> 
                            <div class="panel-heading"> Master Poli Ruangan  
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <div class="col-md-6">
                                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_poli_ruangan" data-backdrop="static" data-keyboard="false">Tambah Poli Ruangan</button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <button  type="button" class="btn btn-info"  onclick="exportToExcel()"><i class="fa fa-cloud-upload"></i> Export Poli Ruangan</button>
                                                </div>
                                                <!-- <div class="col-md-6"> 
                                                        <?php echo form_open('#',array('id' => 'frmImport', 'enctype' => 'multipart/form-data'))?>
                                                            &nbsp;&nbsp;&nbsp;  
                                                            <input type="file" name="file" id="file"   class="inputfile inputfile-6 "    data-multiple-caption="{count} files selected" multiple onchange="showbtnimport()" />  
                                                            <label for="file" style="float: right;"><span></span> <strong>     
                                                            <i class="fa fa-file-excel-o p-r-7"></i> Choose a file&hellip;</strong></label><br><br><br>               
                                                            <button  id="importData" style="float: right;display: none;"  type="button" class="btn btn-info btn-import" ><i class="fa fa-cloud-download p-r-7 "></i> Import Data</button>  
                                                                
                                                        <?php echo form_close(); ?> 
                                                </div> -->
                                            </div> 
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />    
                                           <table id="table_poli_ruangan_list" class="table table-striped dataTable " cellspacing="0"> 
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Instalasi</th>
                                                        <th>Nama Poli Ruangan</th>
                                                        <th>Type Ruangan</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Instalasi</th>
                                                        <th>Nama Poli Ruangan</th>
                                                        <th>Type Ruangan</th>
                                                        <th>Action</th>
                                                    </tr> 
                                                </tfoot>
                                            </table>  
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- Modal  -->
<div id="modal_add_poli_ruangan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreatePoliRuangan'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Poli Ruangan</h2> 
            </div>
            <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                    <div>
                        <p id="card_message" class=""></p>
                    </div>
                </div>
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="form-group col-md-12">
                            <label>Instalasi<span style="color: red">*</span></label>
                            <select name="select_instalasi" id="select_instalasi" class="form-control">
                            <option value="" disabled selected>Pilih Poli Ruangan</option>
                            <?php 
                            $list_instalasi = $this->Poli_ruangan_model->get_instalasi_list();
                            foreach($list_instalasi as $list){
                                echo "<option value=".$list->instalasi_id.">".$list->instalasi_nama."</option>";
                            }
                            ?>
                            </select>
                        </div>  
                        <div class="form-group col-md-12"> 
                            <label>Nama Poli Ruangan<span style="color: red">*</span></label>
                            <input type="text" name="nama_poli_ruangan" class="form-control" placeholder="Nama Poli Ruangan">
                        </div>
                        <div class="form-group col-md-12"> 
                            <label>Nama Singkatan<span style="color: red">*</span></label>
                            <input type="text" name="nama_singkatan" class="form-control" placeholder="Nama Singkatan">
                            <span style="color: red;font-size: 12px">* Singkatan untuk No.Antrian</span>
                        </div>  
                        <div class="form-group col-md-12"> 
                            <label>Type Ruangan<span style="color: red">*</span></label>
                            <select name="select_ruangan" id="select_ruangan" class="form-control">
                                <option value="" disabled selected>Pilih Ruangan</option>
                                <option value="1">Poliklinik</option>
                                <option value="2">Ruang Rawat Inap</option>
                                <option value="3">Ruang IGD</option>
                            </select>  
                        </div> 
                </div>
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="savePoliRuangan"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>
                    </div>
                </div> 
            </div>
            
        </div>
        <?php echo form_close(); ?> 
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->


<!-- Modal  -->  
<div id="modal_edit_poli_ruangan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-lg">
<?php echo form_open('#',array('id' => 'fmUpdatePoliRuangan'))?>
   <div class="modal-content"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel">Perbarui Poli Ruangan</h2> 
        </div>
        <div class="modal-body">
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message1" class=""></p>
                </div>
            </div>
                <span style="color: red;">* Wajib diisi</span><br>
            <div class="row">
            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <div class="form-group col-md-12">
                        <label>Instalasi<span style="color: red">*</span></label>
                        <select name="upd_select_instalasi" id="upd_select_instalasi" class="form-control">
                        <?php 
                        $list_instalasi = $this->Poli_ruangan_model->get_instalasi_list();
                        foreach($list_instalasi as $list){
                            echo "<option value=".$list->instalasi_id.">".$list->instalasi_nama."</option>";
                        }
                        ?>
                        </select>
                    </div>  
                    <div class="form-group col-md-12"> 
                        <label>Nama Poli Ruangan<span style="color: red">*</span></label>
                        <input type="text" name="upd_nama_poli_ruangan" id="upd_nama_poli_ruangan" class="form-control" placeholder="Nama Poli Ruangan">
                        <input type="hidden" name="upd_id_poli_ruangan" id="upd_id_poli_ruangan">
                    </div>
                    <div class="form-group col-md-12">   
                            <label>Nama Singkatan<span style="color: red">*</span></label>
                            <input type="text" name="upd_nama_singkatan"  id="upd_nama_singkatan" class="form-control" placeholder="Nama Singkatan">
                            <span style="color: red;font-size: 12px">* Singkatan untuk No.Antrian</span>
                        </div> 
                    <div class="form-group col-md-12"> 
                        <label>Type Ruangan<span style="color: red">*</span></label>
                        <select name="upd_select_ruangan" id="upd_select_ruangan"class="form-control">
                            <option value="1">Poliklinik</option>
                            <option value="2">Ruang Rawat Inap</option>
                            <option value="3">Ruang IGD</option>
                        </select>  
                    </div> 
            </div>
            <div class="row">
                <div class="col-md-12"> 
                    <button type="button" class="btn btn-success pull-right" id="changePoliRuangan"><i class="fa fa-floppy-o p-r-10"></i>PERBARUI</button>
                </div>
            </div> 
        </div>
        
    </div>
    <?php echo form_close(); ?> 
    <!-- /.modal-content -->
</div>
 <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->
 

<?php $this->load->view('footer');?>
      