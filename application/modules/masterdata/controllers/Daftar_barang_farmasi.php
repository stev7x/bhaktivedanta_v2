<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_barang_farmasi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Daftar_barang_farmasi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_barang_farmasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_barang_farmasi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_barang_farmasi', $this->data);
    }

    public function ajax_list_barang_farmasi(){
        $list = $this->Daftar_barang_farmasi_model->get_barang_farmasi_list();
        $data = array();
        $ini = array();
        // print_r($list);die();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $barang_farmasi){
            $no++;
            $id = $barang_farmasi->id_barang;
            $efeksamping = $this->Daftar_barang_farmasi_model->get_efek_samping($id);
            // var_dump($efeksamping);die();
            $row = array();
            $row[] = $no;
            $row[] = $barang_farmasi->kode_barang;
            $row[] = $barang_farmasi->nama_barang;
            $row[] = $barang_farmasi->nama_sediaan;
            $row[] = $barang_farmasi->nama_jenis;
            if($barang_farmasi->is_bpjs == 1){
                $row[] = "BPJS";
            }else{
                $row[] = "NON BPJS";
            }

            if($barang_farmasi->id_jenis_barang == 1){
                $row[] = '
                <button class="btn btn-info btn-circle" data-position="bottom" data-delay="50" data-toggle="modal" data-target="#modal_detail_barang_farmasi" data-backdrop="static" data-keyboard="false" title="klik untuk melihat detail barang farmasi" id="detailBarangFarmasi" onclick="detailBarangFarmasi('."'".$barang_farmasi->id_barang."'".')"><i class="fa fa-eye"></i></button>
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_barang_farmasi" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit barang_farmasi" onclick="editBarangFarmasi('."'".$barang_farmasi->id_barang."'".')"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus barang_farmasi" data-backdrop="static" data-keyboard="false" id="hapusBarangFarmasi" onclick="hapusBarangFarmasi('."'".$barang_farmasi->id_barang."'".')"><i class="fa fa-trash"></i></button>
              ';

            }else{
                $row[] = '
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_barang_farmasi" title="klik untuk mengedit barang_farmasi" data-backdrop="static" data-keyboard="false" onclick="editBarangFarmasi('."'".$barang_farmasi->id_barang."'".')"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus barang_farmasi" id="hapusBarangFarmasi" data-backdrop="static" data-keyboard="false" onclick="hapusBarangFarmasi('."'".$barang_farmasi->id_barang."'".')"><i class="fa fa-trash"></i></button>
              ';
            }
            //          foreach($efeksamping as $list){
            //             array_push($ini , " ".$list->nama_efek_samping);
            //          };
            // $row[] = $ini;

            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Daftar_barang_farmasi_model->count_barang_farmasi_all(),
                    "recordsFiltered" => $this->Daftar_barang_farmasi_model->count_barang_farmasi_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_barang_farmasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_barang_farmasi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_barang', 'Kode barang', 'required');
        $this->form_validation->set_rules('nama_barang', 'Nama barang', 'required');
        $this->form_validation->set_rules('sediaan', 'Sediaan Barang', 'required');

        $this->form_validation->set_rules('id_jenis_barang', 'jenis Barang', 'required');
//        $id_jenis_barang = $this->input->post('id_jenis_barang', TRUE);
//        if($id_jenis_barang == 1){
//             // $this->form_validation->set_rules('kode_obat', 'Kode Obat', 'required');
//          $this->form_validation->set_rules('golongan', 'Golongan Obat', 'required');
//             $this->form_validation->set_rules('kategori', 'kategori Obat', 'required');
//             $this->form_validation->set_rules('kelompok', 'Kelompok Obat', 'required');
//             $this->form_validation->set_rules('indikasi', 'Indikasi Obat', 'required');
//            $this->form_validation->set_rules('kontra_indikasi', 'Kontra Indikasi Obat', 'required');
//            $this->form_validation->set_rules('interaksi', 'Interaksi Obat', 'required');
// //           $this->form_validation->set_rules('sediaan', 'Sediaan Obat', 'required');
//             // $this->form_validation->set_rules('efek_samping', 'Efek Samping Obat', 'required');
//         }
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_barang_farmasi_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            // $id_barang = $this->input->post('id_barang', TRUE);
            $kode_barang        = $this->input->post('kode_barang', TRUE);
            $nama_barang        = $this->input->post('nama_barang', TRUE);
            $id_jenis_barang    = $this->input->post('id_jenis_barang', TRUE);
            $efek_samping       = $this->input->post('efek_samping', TRUE);
            $golongan           = $this->input->post('golongan', TRUE);
            $kategori           = $this->input->post('kategori', TRUE);
            $kelompok           = $this->input->post('kelompok', TRUE);
            $indikasi           = $this->input->post('indikasi', TRUE);
            $kontra_indikasi    = $this->input->post('kontra_indikasi', TRUE);
            $interaksi          = $this->input->post('interaksi', TRUE);
            $sediaan            = $this->input->post('sediaan', TRUE);
            $is_bpjs            = $this->input->post('is_bpjs', TRUE);

            // var_dump($efek_samping);

           if($id_jenis_barang == 1){
             $data_barang_farmasi = array(
                   'kode_barang'       => $kode_barang,
                   'nama_barang'       => $nama_barang,
                   'id_jenis_barang'   => $id_jenis_barang,
                   'id_golongan'       => $golongan,
                   'id_kategori'       => $kategori,
                   'id_kelompok'       => $kelompok,
                   'id_indikasi'       => $indikasi,
                   'id_kontra_indikasi'=> $kontra_indikasi,
                   'id_interaksi'      => $interaksi,
                   'id_sediaan'        => $sediaan,
                   'is_bpjs'           => $is_bpjs
                );
            }else{
                $data_barang_farmasi = array(
                'kode_barang'       => $kode_barang,
                'nama_barang'       => $nama_barang,
                'id_sediaan'        => $sediaan,
                'id_jenis_barang'   => $id_jenis_barang,
                'is_bpjs'           => $is_bpjs

              );
            }

            $query = $this->db->get_where('t_stok_farmasi',array('kode_barang'=>$kode_barang));//cek kode barang di gudang
            $query = $this->db->get_where('m_barang_farmasi',array('kode_barang'=>$kode_barang));//cek kode barang di daftar barang
            // $query = $this->db->get_where('t_stok_farmasi',array('kode_barang'=>$kode_barang));//cek kode barang
            if ($query->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Kode Barang Sudah Pernah Di Gunakan');

                    // if permitted, do logit
                    $perms = "master_data_barang_farmasi_create";
                    $comments = "Gagal menambahkan kode barang dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $ins = $this->Daftar_barang_farmasi_model->insert_farmasi($data_barang_farmasi);
                // var_dump($ins);


                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'barang_farmasi berhasil ditambahkan'
                    );

                    // $id = $this->Daftar_barang_farmasi_model->insert_farmasi($data_barang_farmasi);
                    if($id_jenis_barang == 1){
                        foreach($efek_samping as $list) {
                            $data= array(
                                'id_barang' => $ins,
                                'id_efek_samping' => $list
                            );
                            $this->db->insert('m_detail_barang_farmasi', $data);
                        }
                    }

                    // if permitted, do logit
                    $perms = "master_data_barang_farmasi_create";
                    $comments = "Berhasil menambahkan barang_farmasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan barang_farmasi, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "master_data_barang_farmasi_create";
                    $comments = "Gagal menambahkan barang_farmasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_barang_farmasi_by_id(){
        $id_barang = $this->input->get('id_barang',TRUE);
        $old_data = $this->Daftar_barang_farmasi_model->get_barang_farmasi_by_id($id_barang);
        // print_r($old_data);die();
        $efeksamping = $this->Daftar_barang_farmasi_model->get_efek_samping($id_barang);
        $ini = array();
        foreach($efeksamping as $list){
            array_push($ini , " ".$list->id_efek_samping);
        };

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data,
                'efeksamping' => $ini
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function ajax_get_barang_farmasi_by_id_detail(){
        $id_barang = $this->input->get('id_barang',TRUE);
        $old_data = $this->Daftar_barang_farmasi_model->get_barang_farmasi_by_id_detail($id_barang);
        $efeksamping = $this->Daftar_barang_farmasi_model->get_efek_samping($id_barang);
        $ini = array();
        foreach($efeksamping as $list){
            array_push($ini , " ".$list->nama_efek_samping);
        };


        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data,
                'efeksamping' => $ini
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_barang_farmasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_barang_farmasi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kode_barang', 'Kode barang', 'required');
        $this->form_validation->set_rules('upd_nama_barang', 'Nama barang', 'required');
        $this->form_validation->set_rules('upd_id_jenis_barang', 'Jenis barang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_barang_farmasi_update";
            $comments = "Gagal update barang_farmasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_barang = $this->input->post('upd_id_barang', TRUE);
            $kode_barang = $this->input->post('upd_kode_barang', TRUE);
            $nama_barang = $this->input->post('upd_nama_barang', TRUE);
            $id_jenis_barang = $this->input->post('upd_id_jenis_barang', TRUE);

            $efek_samping       = $this->input->post('upd_efek_samping', TRUE);
            $golongan           = $this->input->post('upd_golongan', TRUE);
            $kategori           = $this->input->post('upd_kategori', TRUE);
            $kelompok           = $this->input->post('upd_kelompok', TRUE);
            $indikasi           = $this->input->post('upd_indikasi', TRUE);
            $kontra_indikasi    = $this->input->post('upd_kontra_indikasi', TRUE);
            $interaksi          = $this->input->post('upd_interaksi', TRUE);

            if ($id_jenis_barang == 1){
            $data_barang_farmasi = array(
                'kode_barang' => $kode_barang,
                'nama_barang' => $nama_barang,
                'id_jenis_barang' => $id_jenis_barang,

                'id_golongan'       => $golongan,
                'id_kategori'       => $kategori,
                'id_kelompok'       => $kelompok,
                'id_indikasi'       => $indikasi,
                'id_kontra_indikasi'=> $kontra_indikasi,
                'id_interaksi'      => $interaksi
            );
            }else{
                $data_barang_farmasi = array(
                'kode_barang'       => $kode_barang,
                'nama_barang'       => $nama_barang,
                'id_jenis_barang'   => $id_jenis_barang

              );
            }

            $update = $this->Daftar_barang_farmasi_model->update_barang_farmasi($data_barang_farmasi, $id_barang);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'barang_farmasi berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_barang_farmasi_update";
                $comments = "Berhasil mengubah barang_farmasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah barang_farmasi , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_barang_farmasi_update";
                $comments = "Gagal mengubah barang_farmasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_barang_farmasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_barang_farmasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_barang = $this->input->post('id_barang', TRUE);

        $delete = $this->Daftar_barang_farmasi_model->delete_barang_farmasi($id_barang);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'barang_farmasi berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_barang_farmasi_delete";
            $comments = "Berhasil menghapus barang_farmasi dengan id barang_farmasi = '". $id_barang ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_barang_farmasi_delete";
            $comments = "Gagal menghapus data barang_farmasi dengan ID = '". $id_barang ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }


    public function ajax_list_daftar_barang(){
        $list = $this->Daftar_barang_farmasi_model->get_daftar_barang_list();
        die("aya");
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $barang_farmasi){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $barang_farmasi->kode_obat;
            $row[] = $barang_farmasi->nama_barang_farmasi;
            $row[] = $barang_farmasi->alamat;
            $row[] = $barang_farmasi->marketing;
            $row[] = $barang_farmasi->distributor;
            $row[] = $barang_farmasi->jenis_barang_farmasi;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_barang_farmasi" title="klik untuk mengedit barang_farmasi" onclick="editbarang_farmasi('."'".$barang_farmasi->id_barang_farmasi."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus barang_farmasi" id="hapusbarang_farmasi" onclick="hapusbarang_farmasi('."'".$barang_farmasi->id_barang_farmasi."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Daftar_barang_farmasi_model->count_daftar_barang_all(),
                    "recordsFiltered" => $this->Daftar_barang_farmasi_model->count_daftar_barang_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
}
