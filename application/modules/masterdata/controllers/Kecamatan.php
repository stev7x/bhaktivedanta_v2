<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kecamatan_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kecamatan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kecamatan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kecamatan', $this->data);
    }

    public function ajax_list_kecamatan(){
        $list = $this->Kecamatan_model->get_kecamatan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kecamatan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kecamatan->kabupaten_nama;
            $row[] = $kecamatan->kecamatan_nama;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" onclick="editKecamatan('."'".$kecamatan->kecamatan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusKecamatan('."'".$kecamatan->kecamatan_id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kecamatan_model->count_kecamatan_all(),
                    "recordsFiltered" => $this->Kecamatan_model->count_kecamatan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_kecamatan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kecamatan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_kecamatan', 'Nama Kecamatan', 'required');
        $this->form_validation->set_rules('select_kabupaten', 'Nama Kabupaten', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kecamatan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_kecamatan = $this->input->post('nama_kecamatan', TRUE);
            $select_kabupaten = $this->input->post('select_kabupaten', TRUE);
            $data_kecamatan = array(
                'kecamatan_nama'    => $nama_kecamatan,
                'kabupaten_id'       => $select_kabupaten,
            );

            $ins = $this->Kecamatan_model->insert_kecamatan($data_kecamatan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kecamatan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kecamatan_create";
                $comments = "Berhasil menambahkan Kecamatan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Kecamatan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kecamatan_create";
                $comments = "Gagal menambahkan Kecamatan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_kecamatan_by_id(){
        $kecamatan_id = $this->input->get('kecamatan_id',TRUE);
        $old_data = $this->Kecamatan_model->get_kecamatan_by_id($kecamatan_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_kecamatan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kecamatan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_kecamatan', 'Nama Kecamatan', 'required');
        $this->form_validation->set_rules('upd_select_kabupaten', 'Nama Kabupaten', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kecamatan_update";
            $comments = "Gagal update kecamatan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $kecamatan_id = $this->input->post('upd_id_kecamatan',TRUE);
            $nama_kecamatan = $this->input->post('upd_nama_kecamatan', TRUE);
            $select_kabupaten = $this->input->post('upd_select_kabupaten', TRUE);

            $data_kecamatan = array(
                'kecamatan_nama'    => $nama_kecamatan,
                'kabupaten_id'       => $select_kabupaten,
            );

            $update = $this->Kecamatan_model->update_kecamatan($data_kecamatan, $kecamatan_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kecamatan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kecamatan_update";
                $comments = "Berhasil mengubah kecamatan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah kecamatan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kecamatan_update";
                $comments = "Gagal mengubah kecamatan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_kecamatan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kecamatan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $kecamatan_id = $this->input->post('kecamatan_id', TRUE);

        $delete = $this->Kecamatan_model->delete_kecamatan($kecamatan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Kecamatan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kecamatan_delete";
            $comments = "Berhasil menghapus Kecamatan dengan id Kecamatan = '". $kecamatan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kecamatan_delete";
            $comments = "Gagal menghapus data Kecamatan dengan ID = '". $kecamatan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
