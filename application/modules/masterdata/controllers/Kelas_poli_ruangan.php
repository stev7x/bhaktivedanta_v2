<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_poli_ruangan extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kelas_poli_ruangan_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_poli_ruangan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kelas_poli_ruangan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kelas_poli_ruangan', $this->data);
    }

    public function ajax_list_kelas_poli_ruangan(){
        $list = $this->Kelas_poli_ruangan_model->get_kelas_poli_ruangan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kelas_poli_ruangan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kelas_poli_ruangan->nama_poliruangan;
            $row[] = $kelas_poli_ruangan->kelaspelayanan_nama;
            //add html for action
            $row[] =
            '<a class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_update_kelas_poli_ruangan" data-backdrop="static" data-keyboard="false" title="klik untuk mengubah data poli ruangan" onclick="editPoliRuangan('."'".$kelas_poli_ruangan->poliruangan_id."','".$kelas_poli_ruangan->kelaspelayanan_id."'".')"><i class="fa fa-pencil"></i>
            </a>
            <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk mengubah data poli ruangan" onclick="hapusKelas_poli_ruangan('."'".$kelas_poli_ruangan->poliruangan_id."','".$kelas_poli_ruangan->kelaspelayanan_id."'".')"><i class="fa fa-trash"></i>
            </a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kelas_poli_ruangan_model->count_kelas_poli_ruangan_all(),
                    "recordsFiltered" => $this->Kelas_poli_ruangan_model->count_kelas_poli_ruangan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

     function get_kelas_pelayanan(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        // $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        // $list_kelas_pelayanan = $this->Kelas_poli_ruangan_model->get_pelayanan_list();
        $list_kelas_pelayanan_aktif = $this->Kelas_poli_ruangan_model->get_pelayanan_list_aktif($poliruangan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        foreach($list_kelas_pelayanan_aktif as $list_kelas){
            $status = $list_kelas->status_aktif == '1' ? 'SUDAH ADA' : 'BELUM DIGUNAKAN';
            if($list_kelas->status_aktif == '0'){
                // foreach ($list_kelas_pelayanan as $list_kel) {
                // }
                $list .= "<option value='".$list_kelas->kelaspelayanan_id."'>".$list_kelas->kelaspelayanan_nama."(".$status.")</option>";
            }else{

                $list .= "<option value='".$list_kelas->kelaspelayanan_id."' disabled>".$list_kelas->kelaspelayanan_nama."(".$status.")</option>";
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    public function do_create_kelas_poli_ruangan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_poli_ruangan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('select_poliruangan', 'Poli Ruangan', 'required');
        $this->form_validation->set_rules('select_kelaspelayanan', 'Kelas Pelayanan', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelas_poli_ruangan_create";
            $comments = "Gagal input Kelas Pelayanan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $select_poliruangan           =  $this->input->post('select_poliruangan', TRUE);
            $select_kelaspelayanan        = $this->input->post('select_kelaspelayanan', TRUE);
            $data_kelas_poli_ruangan = array(
                'poliruangan_id'        => $select_poliruangan,
                'kelaspelayanan_id'     => $select_kelaspelayanan,
            );

            $ins = $this->Kelas_poli_ruangan_model->insert_kelas_poli_ruangan($data_kelas_poli_ruangan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kelas Pelayanan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kelas_poli_ruangan_create";
                $comments = "Berhasil menambahkan Kelas_poli_ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Data sudah ditambahkan.');

                // if permitted, do logit
                $perms = "master_data_kelas_poli_ruangan_create";
                $comments = "Gagal menambahkan Kelas Pelayanan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_kelas_poli_ruangan_by_id(){
        $poliruangan_id     = $this->input->get('poliruangan_id',TRUE);
        $kelaspelayanan_id  = $this->input->get('kelaspelayanan_id',TRUE);
        $old_data = $this->Kelas_poli_ruangan_model->get_kelas_poli_ruangan_by_id($poliruangan_id,$kelaspelayanan_id);


   if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_kelas_poli_ruangan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_poli_ruangan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_select_poliruangan', 'Poli Ruangan', 'required');
        $this->form_validation->set_rules('upd_select_kelaspelayanan', 'Kelas Pelayanan', 'required');



        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelas_poli_ruangan_update";
            $comments = "Gagal update tarif tindakan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $select_poliruangan           =  $this->input->post('upd_select_poliruangan', TRUE);
            $select_kelaspelayanan        = $this->input->post('upd_select_kelaspelayanan', TRUE);
            $data_kelas_poli_ruangan = array(
                'poliruangan_id'        => $select_poliruangan,
                'kelaspelayanan_id'     => $select_kelaspelayanan,
            );
            $poliruangan_id = $this->input->post('temp_id_poliruangan',TRUE);
            $kelaspelayanan_id = $this->input->post('temp_id_polikelas',TRUE);


            $update = $this->Kelas_poli_ruangan_model->update_kelas_poli_ruangan($data_kelas_poli_ruangan, $poliruangan_id, $kelaspelayanan_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kelas Pelayanan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kelas_poli_ruangan_update";
                $comments = "Berhasil mengubah kelas_poli_ruangan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah tarif tindakan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelas_poli_ruangan_update";
                $comments = "Gagal mengubah tarif tindakan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_kelas_poli_ruangan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_poli_ruangan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $poliruangan_id     = $this->input->post('poliruangan_id', TRUE);
        $kelaspelayanan_id  = $this->input->post('kelaspelayanan_id', TRUE);

        $delete = $this->Kelas_poli_ruangan_model->delete_kelas_poli_ruangan($poliruangan_id,$kelaspelayanan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Kelas Pelayanan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kelas_poli_ruangan_delete";
            $comments = "Berhasil menghapus Kelas_poli_ruangan dengan id Kelas_poli_ruangan = '". $poliruangan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kelas_poli_ruangan_delete";
            $comments = "Gagal menghapus data Kelas Pelayanan dengan ID = '". $poliruangan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
         $object->getActiveSheet()->setTitle("Kelas Poli Ruangan");
        $object->getActiveSheet()
                        ->getStyle("A1:C1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:C1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No","Poli Ruangan","Kelas Pelayanan");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Kelas_poli_ruangan_model->get_kelas_poli_ruangan_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            // $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->id_M_DOKTER);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nama_poliruangan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->kelaspelayanan_nama);

            $excel_row++;
        }

        foreach(range('A','C') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Kelas Poli Ruangan.xlsx"');
        $object_writer->save('php://output');
    }


    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType  = PHPExcel_IOFactory::identify($input_file_name);
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel    = $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows      = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet   = $objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {
                // $unit_kerja_id      = $objWorksheet->getCellByColumnAndRow(0,$row)->getValue(); /
                $poliruangan_id       = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1
                $kelaspelayanan_id    = $objWorksheet->getCellByColumnAndRow(2,$row)->getValue(); //Excel Column 1


                $data = array(
                    'poliruangan_id'  => $poliruangan_id,
                    'kelaspelayanan_id'  => $kelaspelayanan_id
                );

                $this->db->insert("m_kelaspoliruangan", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);
        }
    }
}
