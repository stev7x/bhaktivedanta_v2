<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metode_reservasi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_keterangan', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Metode_reservasi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_metode_reservasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_metode_reservasi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('v_metode_reservasi', $this->data);
    }

    public function ajax_list_metode_reservasi(){
        $list = $this->Metode_reservasi_model->get_metode_reservasi_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $metode_reservasi){
            $metode_persen ="" .number_format($metode_reservasi->metode_persen)."";

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $metode_reservasi->metode_nama;
            $row[] = $metode_persen;
            $row[] = $metode_reservasi->keterangan;

            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_metode_reservasi" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit metode_reservasi" onclick="getEditmetode_reservasi('."'".$metode_reservasi->metode_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus agama" id="hapus_metode_reservasi" onclick="hapusmetode_reservasi('."'".$metode_reservasi->metode_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Metode_reservasi_model->count_metode_reservasi_all(),
                    "recordsFiltered" => $this->Metode_reservasi_model->count_metode_reservasi_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function convert_to_number($rupiah){
       return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function do_create_metode_reservasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_metode_reservasi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('metode_nama', 'metode_reservasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_metode_reservasi_create";
            $comments = "Gagal input metode_reservasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $metode_nama        = $this->input->post('metode_nama', TRUE);
            $metode_persen      = $this->input->post('metode_persen', TRUE);
            $keterangan         = $this->input->post('keterangan', FALSE);

            $metode_persen = $this->convert_to_number($metode_persen);


            $data_metode_reservasi = array(
                'metode_nama'        => $metode_nama,
                'metode_persen'      => $metode_persen,
                'keterangan'         => $keterangan,
            );

            $ins = $this->Metode_reservasi_model->insert_metode_reservasi($data_metode_reservasi);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'metode_reservasi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_metode_reservasi_create";
                $comments = "Berhasil menambahkan metode_reservasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan metode_reservasi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_metode_reservasi_create";
                $comments = "Gagal menambahkan metode_reservasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_metode_reservasi_by_id(){
        $metode_id = $this->input->get('metode_id',TRUE);
        $old_data = $this->Metode_reservasi_model->get_metode_reservasi_by_id($metode_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_metode_reservasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_metode_reservasi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('metode_id', 'Metode reservasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_metode_reservasi_update";
            $comments = "Gagal update metode_reservasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $metode_id          = $this->input->post('metode_id',     TRUE);
            $metode_nama        = $this->input->post('metode_nama',   TRUE);
            $metode_persen      = $this->input->post('metode_persen', TRUE);
            $keterangan         = $this->input->post('keterangan',    FALSE);

            $metode_persen      = $this->convert_to_number($metode_persen);

            $data_metode_reservasi = array(
                'metode_nama'      => $metode_nama,
                'metode_persen'    => $metode_persen,
                'keterangan'       => $keterangan,
            );


            $update = $this->Metode_reservasi_model->update_metode_reservasi($data_metode_reservasi, $metode_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Metode berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_metode_reservasi_update";
                $comments = "Berhasil mengubah metode_reservasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah metode_reservasi , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_metode_reservasi_update";
                $comments = "Gagal mengubah metode_reservasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_metode_reservasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_metode_reservasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $metode_id = $this->input->post('metode_id', TRUE);

        $delete = $this->Metode_reservasi_model->delete_metode_reservasi($metode_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'metode_reservasi berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_metode_reservasi_delete";
            $comments = "Berhasil menghapus metode_reservasi dengan id Metode Reservasi = '". $metode_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_metode_reservasi_delete";
            $comments = "Gagal menghapus data metode_reservasi dengan ID = '". $metode_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("metode_reservasi");
         $object->getActiveSheet()
                        ->getStyle("A1:E1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:E1")
                        ->getFill()
                        ->setFillketerangan(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nama metode_reservasi","harga metode_reservasi","harga_cyto","keterangan");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Metode_reservasi_model->get_metode_reservasi_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->jenis);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->metode_reservasi_harga);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->harga_cyto);
            if($row->keterangan == 1){
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, "Umum");
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, "Dokter");
            }
            $excel_row++;
        }

         foreach(range('A','E') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-keterangan: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="metode_reservasi.xlsx"');
        $object_writer->save('php://output');
    }
}
