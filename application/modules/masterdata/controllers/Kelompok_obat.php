<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok_obat extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kelompok_obat_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelompok_obat_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kelompok_obat_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kelompok_obat', $this->data);
    }

    public function ajax_list_kelompok_obat(){
        $list = $this->Kelompok_obat_model->get_kelompok_obat_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kelompok_obat){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kelompok_obat->kode_kelompok;
            $row[] = $kelompok_obat->nama_kelompok;
            $row[] = $kelompok_obat->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_kelompok_obat" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit kelompok_obat" onclick="editKelompokObat('."'".$kelompok_obat->id_kelompok."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus kelompok_obat" id="hapusKelompokObat" onclick="hapusKelompokObat('."'".$kelompok_obat->id_kelompok."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kelompok_obat_model->count_kelompok_obat_all(),
                    "recordsFiltered" => $this->Kelompok_obat_model->count_kelompok_obat_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_kelompok_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelompok_obat_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_kelompok', 'Kode Kelompok obat', 'required');
        $this->form_validation->set_rules('nama_kelompok', 'Nama Kelompok obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelompok_obat_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $kode_kelompok = $this->input->post('kode_kelompok', TRUE);
            $nama_kelompok = $this->input->post('nama_kelompok', TRUE);
            $keterangan    = $this->input->post('keterangan', TRUE);
            $data_kelompok_obat = array(
                'kode_kelompok' => $kode_kelompok,
                'nama_kelompok' => $nama_kelompok,
                'keterangan'         => $keterangan,
            );

            $ins = $this->Kelompok_obat_model->insert_kelompok_obat($data_kelompok_obat);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'kelompok_obat berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kelompok_obat_create";
                $comments = "Berhasil menambahkan kelompok_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan kelompok_obat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelompok_obat_create";
                $comments = "Gagal menambahkan kelompok_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_kelompok_obat_by_id(){
        $id_kelompok = $this->input->get('id_kelompok',TRUE);
        $old_data = $this->Kelompok_obat_model->get_kelompok_obat_by_id($id_kelompok);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_kelompok_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelompok_obat_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kode_kelompok', 'Kode golongan', 'required');
        $this->form_validation->set_rules('upd_nama_kelompok', 'Nama golongan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelompok_obat_update";
            $comments = "Gagal update kelompok_obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_kelompok = $this->input->post('upd_id_kelompok',TRUE);
            $kode_kelompok = $this->input->post('upd_kode_kelompok', TRUE);
            $nama_kelompok = $this->input->post('upd_nama_kelompok', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_kelompok_obat = array(
                'kode_kelompok' => $kode_kelompok,
                'nama_kelompok' => $nama_kelompok,
                'keterangan' => $keterangan
            );

            $update = $this->Kelompok_obat_model->update_kelompok_obat($data_kelompok_obat, $id_kelompok);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'kelompok_obat berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kelompok_obat_update";
                $comments = "Berhasil mengubah kelompok_obat  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah kelompok_obat , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelompok_obat_update";
                $comments = "Gagal mengubah kelompok_obat  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_kelompok_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelompok_obat_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_kelompok = $this->input->post('id_kelompok', TRUE);

        $delete = $this->Kelompok_obat_model->delete_kelompok_obat($id_kelompok);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'kelompok_obat berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kelompok_obat_delete";
            $comments = "Berhasil menghapus kelompok_obat dengan id kelompok_obat = '". $id_kelompok ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kelompok_obat_delete";
            $comments = "Gagal menghapus data kelompok_obat dengan ID = '". $id_kelompok ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
