<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_pelayanan extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library("Excel");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kelas_pelayanan_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_pelayanan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kelas_pelayanan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kelas_pelayanan', $this->data);
    }

    public function ajax_list_kelas_pelayanan(){
        $list = $this->Kelas_pelayanan_model->get_kelas_pelayanan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kelas_pelayanan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kelas_pelayanan->kelaspelayanan_nama;
            $row[] = $kelas_pelayanan->isi_bed;
            $row[] = $kelas_pelayanan->tarif;
            //add html for action
            $row[] = '<button class="btn btn-info btn-circle" data-position="bottom" data-target="#modal_detail_fasilitas" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-delay="50" title="klik untuk melihat fasilitas" onclick="detailKelas_pelayanan('."'".$kelas_pelayanan->kelaspelayanan_id."'".')"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-warning btn-circle" onclick="editKelas_pelayanan('."'".$kelas_pelayanan->kelaspelayanan_id."'".')"><i class="fa fa-pencil"></i></button>
                        <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusKelas_pelayanan('."'".$kelas_pelayanan->kelaspelayanan_id."'".')"><i class="fa fa-trash"></i></a>
            ';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kelas_pelayanan_model->count_kelas_pelayanan_all(),
                    "recordsFiltered" => $this->Kelas_pelayanan_model->count_kelas_pelayanan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_kelas_pelayanan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_pelayanan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_kelas_pelayanan', 'Nama Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('isi_bed', 'Jumlah Kasur', 'required');
        $this->form_validation->set_rules('luas_ruangan', 'Luas Ruangan', 'required');
        $this->form_validation->set_rules('tarif', 'Tarif', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelas_pelayanan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_kelas_pelayanan   = $this->input->post('nama_kelas_pelayanan', TRUE);
            $luas_ruangan           = $this->input->post('luas_ruangan', TRUE);
            $isi_bed                = $this->input->post('isi_bed', TRUE);
            $tarif                  = $this->input->post('tarif', TRUE);
            $kamar_mandi            = $this->input->post('kamar_mandi', TRUE);
            $closet                 = $this->input->post('closet', TRUE);
            $wastafel               = $this->input->post('wastafel', TRUE);
            $shower                 = $this->input->post('shower', TRUE);
            $water_heater           = $this->input->post('water_heater', TRUE);
            $kipas_angin            = $this->input->post('kipas_angin', TRUE);
            $exhause_fan            = $this->input->post('exhause_fan', TRUE);
            $tv                     = $this->input->post('tv', TRUE);
            $baby_bed               = $this->input->post('baby_bed', TRUE);
            $ac                     = $this->input->post('ac', TRUE);
            $kulkas                 = $this->input->post('kulkas', TRUE);
            $lemari                 = $this->input->post('lemari', TRUE);
            $ruang_tamu             = $this->input->post('ruang_tamu', TRUE);
            $ruang_keluarga         = $this->input->post('ruang_keluarga', TRUE);
            $extra_bed              = $this->input->post('extra_bed', TRUE);
            $sofa_bed               = $this->input->post('sofa_bed', TRUE);
            $wifi                   = $this->input->post('wifi', TRUE);
            $peralatan_mandi        = $this->input->post('peralatan_mandi', TRUE);
            $paket_buah            = $this->input->post('paket_buah', TRUE);

            $data_kelas_pelayanan = array(
                'kelaspelayanan_nama'   => $nama_kelas_pelayanan,
                'luas_ruangan'          => $luas_ruangan,
                'isi_bed'               => $isi_bed,
                'tarif'                 => $tarif,
                'kamar_mandi_dalam'     => (isset($kamar_mandi) && $kamar_mandi=='on' ?'1' : '0'),
                'closed_duduk'          => (isset($closet) && $closet=='on' ?'1' : '0'),
                'wastafel'              => (isset($wastafel) && $wastafel=='on' ?'1' : '0'),
                'shower'                => (isset($shower) && $shower=='on' ?'1' : '0'),
                'water_heater'          => (isset($water_heater) && $water_heater=='on' ?'1' : '0'),
                'kipas_angin'           => (isset($kipas_angin) && $kipas_angin=='on' ?'1' : '0'),
                'exhause_fan'           => (isset($exhause_fan) && $exhause_fan=='on' ?'1' : '0'),
                'led_tv_digital'        => (isset($tv) && $tv=='on' ?'1' : '0'),
                'baby_bed'              => (isset($baby_bed) && $baby_bed=='on' ?'1' : '0'),
                'ac'                    => (isset($ac) && $ac=='on' ?'1' : '0'),
                'kulkas'                => (isset($kulkas) && $kulkas=='on' ?'1' : '0'),
                'lemari'                => (isset($lemari) && $lemari=='on' ?'1' : '0'),
                'ruang_tamu'            => (isset($ruang_tamu) && $ruang_tamu=='on' ?'1' : '0'),
                'ruang_keluarga'        => (isset($ruang_keluarga) && $ruang_keluarga=='on' ?'1' : '0'),
                'extra_bed'             => (isset($extra_bed) && $extra_bed=='on' ?'1' : '0'),
                'sofa_bed'              => (isset($sofa_bed) && $sofa_bed=='on' ?'1' : '0'),
                'free_wifi'             => (isset($wifi) && $wifi=='on' ?'1' : '0'),
                'paket_peralatan_mandi' => (isset($peralatan_mandi) && $peralatan_mandi=='on' ?'1' : '0'),
                'paket_buah'            => (isset($paket_buah) && $paket_buah=='on' ?'1' : '0')
            );

            $ins = $this->Kelas_pelayanan_model->insert_kelas_pelayanan($data_kelas_pelayanan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kelas Pelayanan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kelas_pelayanan_create";
                $comments = "Berhasil menambahkan Kelas Pelayanan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Kelas Pelayanan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelas_pelayanan_create";
                $comments = "Gagal menambahkan Kelas Pelayanan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_kelas_pelayanan_by_id(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $old_data = $this->Kelas_pelayanan_model->get_kelas_pelayanan_by_id($kelaspelayanan_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_kelas_pelayanan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_pelayanan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_kelas_pelayanan', 'Nama Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('upd_isi_bed', 'Jumlah Kasur', 'required');
        $this->form_validation->set_rules('upd_luas_ruangan', 'Luas Ruangan', 'required');
        $this->form_validation->set_rules('upd_tarif', 'Tarif', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelas_pelayanan_update";
            $comments = "Gagal update kelas_pelayanan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $kelaspelayanan_id      = $this->input->post('upd_id_kelas_pelayanan',TRUE);
            $nama_kelas_pelayanan   = $this->input->post('upd_nama_kelas_pelayanan', TRUE);
            $luas_ruangan           = $this->input->post('upd_luas_ruangan', TRUE);
            $isi_bed                = $this->input->post('upd_isi_bed', TRUE);
            $tarif                  = $this->input->post('upd_tarif', TRUE);
            $kamar_mandi            = $this->input->post('upd_kamar_mandi', TRUE);
            $closet                 = $this->input->post('upd_closet', TRUE);
            $wastafel               = $this->input->post('upd_wastafel', TRUE);
            $shower                 = $this->input->post('upd_shower', TRUE);
            $water_heater           = $this->input->post('upd_water_heater', TRUE);
            $kipas_angin            = $this->input->post('upd_kipas_angin', TRUE);
            $exhause_fan            = $this->input->post('upd_exhause_fan', TRUE);
            $tv                     = $this->input->post('upd_tv', TRUE);
            $baby_bed               = $this->input->post('upd_baby_bed', TRUE);
            $ac                     = $this->input->post('upd_ac', TRUE);
            $kulkas                 = $this->input->post('upd_kulkas', TRUE);
            $lemari                 = $this->input->post('upd_lemari', TRUE);
            $ruang_tamu             = $this->input->post('upd_ruang_tamu', TRUE);
            $ruang_keluarga         = $this->input->post('upd_ruang_keluarga', TRUE);
            $extra_bed              = $this->input->post('upd_extra_bed', TRUE);
            $sofa_bed               = $this->input->post('upd_sofa_bed', TRUE);
            $wifi                   = $this->input->post('upd_wifi', TRUE);
            $peralatan_mandi        = $this->input->post('upd_peralatan_mandi', TRUE);
            $paket_buah             = $this->input->post('upd_paket_buah', TRUE);

            $data_kelas_pelayanan = array(
                'kelaspelayanan_nama'   => $nama_kelas_pelayanan,
                'luas_ruangan'          => $luas_ruangan,
                'isi_bed'               => $isi_bed,
                'tarif'                 => $tarif,
                'kamar_mandi_dalam'     => (isset($kamar_mandi) && $kamar_mandi=='on' ?'1' : '0'),
                'closed_duduk'          => (isset($closet) && $closet=='on' ?'1' : '0'),
                'wastafel'              => (isset($wastafel) && $wastafel=='on' ?'1' : '0'),
                'shower'                => (isset($shower) && $shower=='on' ?'1' : '0'),
                'water_heater'          => (isset($water_heater) && $water_heater=='on' ?'1' : '0'),
                'kipas_angin'           => (isset($kipas_angin) && $kipas_angin=='on' ?'1' : '0'),
                'exhause_fan'           => (isset($exhause_fan) && $exhause_fan=='on' ?'1' : '0'),
                'led_tv_digital'        => (isset($tv) && $tv=='on' ?'1' : '0'),
                'baby_bed'              => (isset($baby_bed) && $baby_bed=='on' ?'1' : '0'),
                'ac'                    => (isset($ac) && $ac=='on' ?'1' : '0'),
                'kulkas'                => (isset($kulkas) && $kulkas=='on' ?'1' : '0'),
                'lemari'                => (isset($lemari) && $lemari=='on' ?'1' : '0'),
                'ruang_tamu'            => (isset($ruang_tamu) && $ruang_tamu=='on' ?'1' : '0'),
                'ruang_keluarga'        => (isset($ruang_keluarga) && $ruang_keluarga=='on' ?'1' : '0'),
                'extra_bed'             => (isset($extra_bed) && $extra_bed=='on' ?'1' : '0'),
                'sofa_bed'              => (isset($sofa_bed) && $sofa_bed=='on' ?'1' : '0'),
                'free_wifi'             => (isset($wifi) && $wifi=='on' ?'1' : '0'),
                'paket_peralatan_mandi' => (isset($peralatan_mandi) && $peralatan_mandi=='on' ?'1' : '0'),
                'paket_buah'            => (isset($paket_buah) && $paket_buah=='on' ?'1' : '0')
            );

            $update = $this->Kelas_pelayanan_model->update_kelas_pelayanan($data_kelas_pelayanan, $kelaspelayanan_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kelas Pelayanan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kelas_pelayanan_update";
                $comments = "Berhasil mengubah kelas_pelayanan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah kelas_pelayanan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelas_pelayanan_update";
                $comments = "Gagal mengubah kelas_pelayanan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_kelas_pelayanan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_pelayanan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $kelaspelayanan_id = $this->input->post('kelaspelayanan_id', TRUE);

        $delete = $this->Kelas_pelayanan_model->delete_kelas_pelayanan($kelaspelayanan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Kelas Pelayanan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kelas_pelayanan_delete";
            $comments = "Berhasil menghapus Kelas Pelayanan dengan id Kelas Pelayanan = '". $kelaspelayanan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kelas_pelayanan_delete";
            $comments = "Gagal menghapus data Kelas Pelayanan dengan ID = '". $kelaspelayanan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Kelas Pelayanan");
         $object->getActiveSheet()
                        ->getStyle("A1:X1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:X1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nama Kelas Pelayanan","Luas Ruangan","Isi bed","Tarif","Kamar Mandi","Closed Duduk","Wastafel","Shower","Water Heater","Kipas Angin","Exhause Fan","TV LED","Baby Bed","AC","Kulkas","Lemari","Ruang Tamu","Ruang Keluarga","Extra Bed","Sofa Bed","Free Wifi","Peralatan Masak","Paket Buah");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Kelas_pelayanan_model->get_kelas_pelayanan_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->kelaspelayanan_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->luas_ruangan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->isi_bed);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->tarif);

            if($row->kamar_mandi_dalam == 1){ //kamar mandi
                $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, 'Tidak');
            }

            if($row->closed_duduk == 1){ //closed duduk
                $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, 'Tidak');
            }

            if($row->wastafel == 1){ //wastafel
                $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, 'Tidak');
            }

            if($row->shower == 1){ //shower
                $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, 'Tidak');
            }

            if($row->water_heater == 1){ //water heater
                $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, 'Tidak');
            }

            if($row->kipas_angin == 1){ //kipas angin
                $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, 'Tidak');
            }
            if($row->exhause_fan == 1){ //exhause fan
                $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, 'Tidak');
            }
            if($row->led_tv_digital == 1){ //led tv
                $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, 'Tidak');
            }

            if($row->baby_bed == 1){ //baby led
                $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, 'Tidak');
            }

            if($row->ac == 1){ //ac
                $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, 'Tidak');
            }

            if($row->kulkas == 1){ //kulkas
                $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, 'Tidak');
            }

            if($row->lemari == 1){ //lemari
                $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, 'Tidak');
            }

            if($row->ruang_tamu == 1){ //ruang tamu
                $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, 'Tidak');
            }

            if($row->ruang_keluarga == 1){ //ruang keluarga
                $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, 'Tidak');
            }

            if($row->extra_bed == 1){ //extra bed
                $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, 'Tidak');
            }

            if($row->sofa_bed == 1){ //sofa bed
                $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, 'Tidak');
            }

            if($row->free_wifi == 1){ //free wifi
                $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, 'Tidak');
            }

            if($row->paket_peralatan_mandi == 1){ //paket peralatan
                $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, 'Tidak');
            }

            if($row->paket_buah == 1){ //paket bauh
                $object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, 'Ya');
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, 'Tidak');
            }


            $excel_row++;
        }

         foreach(range('A','X') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Kelas Pelayanan.xlsx"');
        $object_writer->save('php://output');
    }

    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType  = PHPExcel_IOFactory::identify($input_file_name);
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel    = $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows      = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet   = $objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {
                // $unit_kerja_id      = $objWorksheet->getCellByColumnAndRow(0,$row)->getValue(); /
                $kelaspelayanan_nama    = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1
                $luas_ruangan           = $objWorksheet->getCellByColumnAndRow(2,$row)->getValue(); //Excel Column 1
                $isi_bed                = $objWorksheet->getCellByColumnAndRow(3,$row)->getValue(); //Excel Column 1
                $tarif                  = $objWorksheet->getCellByColumnAndRow(4,$row)->getValue(); //Excel Column 1
                $kamar_mandi_dalam      = $objWorksheet->getCellByColumnAndRow(5,$row)->getValue(); //Excel Column 1
                $closed_duduk           = $objWorksheet->getCellByColumnAndRow(6,$row)->getValue(); //Excel Column 1
                $wastafel               = $objWorksheet->getCellByColumnAndRow(7,$row)->getValue(); //Excel Column 1
                $shower                 = $objWorksheet->getCellByColumnAndRow(8,$row)->getValue(); //Excel Column 1
                $water_heater           = $objWorksheet->getCellByColumnAndRow(9,$row)->getValue(); //Excel Column 1
                $kipas_angin            = $objWorksheet->getCellByColumnAndRow(10,$row)->getValue(); //Excel Column 1
                $exhause_fan            = $objWorksheet->getCellByColumnAndRow(11,$row)->getValue(); //Excel Column 1
                $led_tv_digital         = $objWorksheet->getCellByColumnAndRow(12,$row)->getValue(); //Excel Column 1
                $baby_bed               = $objWorksheet->getCellByColumnAndRow(13,$row)->getValue(); //Excel Column 1
                $ac                     = $objWorksheet->getCellByColumnAndRow(14,$row)->getValue(); //Excel Column 1
                $kulkas                 = $objWorksheet->getCellByColumnAndRow(15,$row)->getValue(); //Excel Column 1
                $lemari                 = $objWorksheet->getCellByColumnAndRow(16,$row)->getValue(); //Excel Column 1
                $ruang_tamu             = $objWorksheet->getCellByColumnAndRow(17,$row)->getValue(); //Excel Column 1
                $ruang_keluarga         = $objWorksheet->getCellByColumnAndRow(18,$row)->getValue(); //Excel Column 1
                $extra_bed              = $objWorksheet->getCellByColumnAndRow(19,$row)->getValue(); //Excel Column 1
                $sofa_bed               = $objWorksheet->getCellByColumnAndRow(20,$row)->getValue(); //Excel Column 1
                $free_wifi              = $objWorksheet->getCellByColumnAndRow(21,$row)->getValue(); //Excel Column 1
                $paket_peralatan_mandi  = $objWorksheet->getCellByColumnAndRow(22,$row)->getValue(); //Excel Column 1
                $paket_buah             = $objWorksheet->getCellByColumnAndRow(23,$row)->getValue(); //Excel Column 1

                $data = array(
                    'kelaspelayanan_nama'  => $kelaspelayanan_nama,
                    'luas_ruangan'          => $luas_ruangan,
                    'isi_bed'               => $isi_bed,
                    'tarif'                 => $tarif,
                    'kamar_mandi_dalam'     => $kamar_mandi_dalam,
                    'closed_duduk'          => $closed_duduk,
                    'wastafel'              => $wastafel,
                    'shower'                => $shower,
                    'water_heater'          => $water_heater,
                    'kipas_angin'           => $kipas_angin,
                    'exhause_fan'           => $exhause_fan,
                    'led_tv_digital'        => $led_tv_digital,
                    'baby_bed'              => $baby_bed,
                    'ac'                    => $ac,
                    'kulkas'                => $kulkas,
                    'lemari'                => $lemari,
                    'ruang_tamu'            => $ruang_tamu,
                    'ruang_keluarga'        => $ruang_keluarga,
                    'extra_bed'             => $extra_bed,
                    'sofa_bed'              => $sofa_bed,
                    'free_wifi'             => $free_wifi,
                    'paket_peralatan_mandi' => $paket_peralatan_mandi,
                    'paket_buah'            => $paket_buah
                );

                $this->db->insert("m_kelaspelayanan", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);
        }
    }
}
