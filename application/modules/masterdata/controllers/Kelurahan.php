<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kelurahan_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelurahan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kelurahan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kelurahan', $this->data);
    }

    public function ajax_list_kelurahan(){
        $list = $this->Kelurahan_model->get_kelurahan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kelurahan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kelurahan->kecamatan_nama;
            $row[] = $kelurahan->kelurahan_nama;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" onclick="editKelurahan('."'".$kelurahan->kelurahan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusKelurahan('."'".$kelurahan->kelurahan_id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kelurahan_model->count_kelurahan_all(),
                    "recordsFiltered" => $this->Kelurahan_model->count_kelurahan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_kelurahan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelurahan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_kelurahan', 'Nama Kelurahan', 'required');
        $this->form_validation->set_rules('select_kecamatan', 'Nama Kecamatan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelurahan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_kelurahan = $this->input->post('nama_kelurahan', TRUE);
            $select_kecamatan = $this->input->post('select_kecamatan', TRUE);
            $data_kelurahan = array(
                'kelurahan_nama'    => $nama_kelurahan,
                'kecamatan_id'       => $select_kecamatan,
            );

            $ins = $this->Kelurahan_model->insert_kelurahan($data_kelurahan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kelurahan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kelurahan_create";
                $comments = "Berhasil menambahkan Kelurahan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Kelurahan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelurahan_create";
                $comments = "Gagal menambahkan Kelurahan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_kelurahan_by_id(){
        $kelurahan_id = $this->input->get('kelurahan_id',TRUE);
        $old_data = $this->Kelurahan_model->get_kelurahan_by_id($kelurahan_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_kelurahan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelurahan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_kelurahan', 'Nama Kelurahan', 'required');
        $this->form_validation->set_rules('upd_select_kecamatan', 'Nama Kecamatan', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelurahan_update";
            $comments = "Gagal update kelurahan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $kelurahan_id = $this->input->post('upd_id_kelurahan',TRUE);
            $nama_kelurahan = $this->input->post('upd_nama_kelurahan', TRUE);
            $select_kecamatan = $this->input->post('upd_select_kecamatan', TRUE);

            $data_kelurahan = array(
                'kelurahan_nama'    => $nama_kelurahan,
                'kecamatan_id'       => $select_kecamatan,
            );

            $update = $this->Kelurahan_model->update_kelurahan($data_kelurahan, $kelurahan_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kelurahan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kelurahan_update";
                $comments = "Berhasil mengubah kelurahan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah kelurahan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelurahan_update";
                $comments = "Gagal mengubah kelurahan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_kelurahan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelurahan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $kelurahan_id = $this->input->post('kelurahan_id', TRUE);

        $delete = $this->Kelurahan_model->delete_kelurahan($kelurahan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Kelurahan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kelurahan_delete";
            $comments = "Berhasil menghapus Kelurahan dengan id Kelurahan = '". $kelurahan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kelurahan_delete";
            $comments = "Gagal menghapus data Kelurahan dengan ID = '". $kelurahan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
