<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('rekam_medis/Pendaftaran_model');
        $this->load->model('Karyawan_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['list_status_kawin']    = array('Belum Kawin'=>'Belum Kawin','Kawin'=>'Kawin','Duda/Janda'=>'Duda/Janda');
        $this->data['next_no_rm'] = $this->Karyawan_model->get_no_rm();
        $this->data['next_no_karyawan'] = $this->Karyawan_model->get_no_karyawan();
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function get_no(){
        $data['next_no_rm'] = $this->Karyawan_model->get_no_rm();
        $data['next_no_karyawan'] = $this->Karyawan_model->get_no_karyawan();

        echo json_encode($data);

    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_karyawan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "master_data_karyawan_view";
        $comments = "List Karyawan";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_karyawan', $this->data);
    }

    public function ajax_list_karyawan(){
        $list = $this->Karyawan_model->get_karyawan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $karyawan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $karyawan->no_karyawan;
            $row[] = $karyawan->no_rekam_medis;
            $row[] = $karyawan->nama_karyawan;
            $row[] = $karyawan->tempat_lahir;
            $row[] = $karyawan->tanggal_lahir;
            $row[] = $karyawan->jenis_kelamin;
            $row[] = $karyawan->alamat;
            $row[] = $karyawan->email;
            $row[] = $karyawan->jabatan;
            //add html for action
            $row[] = '
            <button class="btn btn-info " data-toggle="modal" data-target="#" data-backdrop="static" data-keyboard="false" title="klik untuk detail Karyawan" onclick="detailKaryawan('."'".$karyawan->karyawan_id."'".')"><i class="fa fa-eye"></i></button>
            <button class="btn btn-warning " data-toggle="modal" data-target="#modal_karyawan" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit Karyawan" onclick="editDetailKaryawan('."'".$karyawan->karyawan_id."'".')"><i class="fa fa-pencil"></i></button>
            <button class="btn btn-danger" title="klik untuk menghapus karyawan" id="hapusKaryawan" onclick="hapusKaryawan('."'".$karyawan->karyawan_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Karyawan_model->count_karyawan_all(),
                    "recordsFiltered" => $this->Karyawan_model->count_karyawan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_ref(){
        $list = $this->Karyawan_model->get_ref_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasien){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasien->no_rekam_medis;
            $row[] = $pasien->pasien_nama;
            $row[] = $pasien->jenis_kelamin;
            $row[] = $pasien->tanggal_lahir;
            $row[] = $pasien->tempat_lahir;
            $row[] = $pasien->pasien_alamat;
            //add html for action
            $row[] = '
            <button class="btn btn-info btn-circle" title="klik untuk pilih ref karyawan" id="ref_karyawan" onclick="pilihRefKaryawan('."'".$pasien->pasien_id."'".')"><i class="fa fa-check"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Karyawan_model->count_ref_karyawan_all(),
                    "recordsFiltered" => $this->Karyawan_model->count_ref_karyawan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_karyawan(){

        /* Permission setting di auuth perms */
        $is_permit = $this->aauth->control_no_redirect('master_data_karyawan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');

        /* Data Karyawan */
        $this->form_validation->set_rules('no_rm', 'No. Rekam Medis', 'required|trim');
        $this->form_validation->set_rules('no_ktp', 'No. KTP', 'required|trim');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap Karyawan', 'required|trim');
        $this->form_validation->set_rules('nama_panggilan', 'Nama Panggilan Karyawan', 'required|trim');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|trim');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required|trim');
        $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');

        /* Data Riwayat Pendidikan Umum*/
        $this->form_validation->set_rules('nama_sekolah', 'Nama Sekolah', 'required|trim');
        $this->form_validation->set_rules('jurusan', 'Jurusan', 'required|trim');
        $this->form_validation->set_rules('tahun_lulus_sekolah', 'Tahun Lulus Sekolah', 'required|trim');
        $this->form_validation->set_rules('no_ijazah_sekolah', 'No Ijazah Sekolah', 'required|trim');
        $this->form_validation->set_rules('tgl_verif_sekolah', 'Tanggal Terverifikasi Sekolah', 'required|trim');
        $this->form_validation->set_rules('strata', 'Strata', 'required|trim');

        /* Data Riwayat Pendidikan Profesi */
        $this->form_validation->set_rules('profesi', 'Profesi', 'required|trim');
        $this->form_validation->set_rules('spesialis', 'Spesialis', 'required|trim');
        $this->form_validation->set_rules('tahun_lulus_profesi', 'Tahun Lulus Profesi', 'required|trim');
        $this->form_validation->set_rules('no_ijazah_profesi', 'No Ijazah Profesi', 'required|trim');
        $this->form_validation->set_rules('tgl_verif_profesi', 'Tanggal Terverifikasi Profesi', 'required|trim');

        /* Jika ada form required yg belum diisi */
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error
            );

            // if permitted, do logit
            $perms = "master_data_karyawan_create";
            $comments = "Gagal melakukan pendaftaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{

            /* Data Karyawan */
            $data_karyawan['no_karyawan'] = $this->input->post('no_karyawan',TRUE);
            $data_karyawan['nama_karyawan'] = $this->input->post('nama_lengkap',TRUE);
            $data_karyawan['nama_panggil_karyawan'] = $this->input->post('nama_panggilan',TRUE);
            $data_karyawan['no_rek_mandiri'] = $this->input->post('no_rek_mandiri',TRUE);
            $data_karyawan['no_bpjs'] = $this->input->post('no_bpjs',TRUE);
            $data_karyawan['no_bpjs_ketker'] = $this->input->post('no_bpjs_kerja',TRUE);
            $data_karyawan['no_rekam_medis'] = $this->input->post('no_rm',TRUE);
            $data_karyawan['no_ktp'] = $this->input->post('no_ktp',TRUE);
            $data_karyawan['email'] = $this->input->post('email',TRUE);
            $data_karyawan['jenis_kelamin'] = $this->input->post('jenis_kelamin',TRUE);
            $data_karyawan['alamat'] = $this->input->post('alamat',TRUE);
            $data_karyawan['agama_id'] = $this->input->post('agama',TRUE);
            $data_karyawan['status_kawin'] = $this->input->post('status_kawin',TRUE);
            $data_karyawan['tempat_lahir'] = $this->input->post('tempat_lahir',TRUE);

            $format_tanggal = date_create($this->input->post('tgl_lahir',TRUE));
            $tgl_lahir = date_format($format_tanggal, 'Y-m-d');

            $data_karyawan['tanggal_lahir'] = $tgl_lahir;
            $data_karyawan['umur'] = $this->input->post('umur',TRUE);
            $data_karyawan['orientasi'] = $this->input->post('orientasi',TRUE);
            $data_karyawan['lama_orientasi'] = $this->input->post('lama_orientasi',TRUE);
            $data_karyawan['tmt'] = $this->input->post('tmt',TRUE);

            $masa_kerja = date_create($this->input->post('masa_kerja',TRUE));
            $masa_kerja1 = date_format($masa_kerja, 'Y-m-d');
            $data_karyawan['masa_kerja'] = $masa_kerja1;

            $keluar = date_create($this->input->post('keluar',TRUE));
            $keluar1 = date_format($keluar, 'Y-m-d');
            $data_karyawan['keluar'] = $keluar1;
            $data_karyawan['alasan_keluar'] = $this->input->post('alasan_keluar',TRUE);
            $data_karyawan['unit_kerja_id'] = $this->input->post('unit_kerja_id',TRUE);
            $data_karyawan['ruang'] = $this->input->post('ruang',TRUE);
            $data_karyawan['penilaian_resiko'] = $this->input->post('penilaian_resiko',TRUE);
            $data_karyawan['status_pegawai'] = $this->input->post('status_pegawai',TRUE);
            $data_karyawan['no_kontrak'] = $this->input->post('no_kontrak',TRUE);

            $tgl_berakhir = date_create($this->input->post('tgl_berakhir',TRUE));
            $tgl_berakhir1 = date_format($tgl_berakhir, 'Y-m-d');
            $data_karyawan['tgl_berakhir'] = $tgl_berakhir1;
            $data_karyawan['kelompok_intensif'] = $this->input->post('kelompok_intensif',TRUE);
            $data_karyawan['jabatan'] = $this->input->post('jabatan',TRUE);
            $data_karyawan['jabatan_status'] = $this->input->post('jabatan_status',TRUE);
            $data_karyawan['status'] = $this->input->post('status',TRUE);
            $data_karyawan['hak_bersama'] = $this->input->post('hak_bersama',TRUE);
            $data_karyawan['hak_unit'] = $this->input->post('hak_unit',TRUE);
            $data_karyawan['penempatan'] = $this->input->post('penempatan',TRUE);

            $this->db->trans_begin();


            /* Data Riwayat Pendidikan Umum */
            $data_pu['strata'] = $this->input->post('strata',TRUE);
            $data_pu['jurusan'] = $this->input->post('jurusan',TRUE);
            $data_pu['tahun_lulus'] = $this->input->post('tahun_lulus_sekolah',TRUE);
            $data_pu['nama_sekolah'] = $this->input->post('nama_sekolah',TRUE);
            $data_pu['no_ijazah'] = $this->input->post('no_ijazah_sekolah',TRUE);
            $tgl_ver1 = date_create($this->input->post('tgl_verif_sekolah',TRUE));
            $tgl_terverifikasi1 = date_format($tgl_ver1, 'Y-m-d');
            $data_pu['tgl_terverifikasi'] = $tgl_terverifikasi1;


            /* Data Riwayat Pendidika Profesi */
            $data_pp['profesi'] = $this->input->post('profesi',TRUE);
            $data_pp['spesialis'] = $this->input->post('spesialis',TRUE);
            $data_pp['tahun'] = $this->input->post('tahun_lulus_profesi',TRUE);
            $data_pp['no_ijazah'] = $this->input->post('no_ijazah_profesi',TRUE);
            $tgl_ver2 = date_create($this->input->post('tgl_verif_profesi',TRUE));
            $tgl_terverifikasi2 = date_format($tgl_ver2, 'Y-m-d');
            $data_pp['tgl_terverifikasi'] = $tgl_terverifikasi2;

            /* Data Izin Profesi */
            $data_ijin_p['no_str'] = $this->input->post('no_str',TRUE);
            $tgl_terbit = date_create($this->input->post('tgl_terbit_izin',TRUE));
            $tgl_diterbitkan = date_format($tgl_terbit, 'Y-m-d');
            $data_ijin_p['tgl_diterbitkan'] = $tgl_diterbitkan;

            $tgl_berlaku = date_create($this->input->post('tgl_berlaku_izin',TRUE));
            $tgl_berlaku1 = date_format($tgl_berlaku, 'Y-m-d');
            $data_ijin_p['berlaku_sd'] = $tgl_berlaku1;

            $tgl_ver3 = date_create($this->input->post('tgl_verif_str',TRUE));
            $tgl_terverifikasi3 = date_format($tgl_ver3, 'Y-m-d');
            $data_ijin_p['tgl_terverifikasi'] = $tgl_terverifikasi3;

            $data_ijin_p['no_sip'] = $this->input->post('no_sip',TRUE);

            $tgl_terbit2 = date_create($this->input->post('tgl_terbit_sip',TRUE));
            $tgl_diterbitkan2 = date_format($tgl_terbit2, 'Y-m-d');
            $data_ijin_p['tgl_diterbitkan_sip'] = $tgl_diterbitkan2;

            $tgl_berlaku2 = date_create($this->input->post('tgl_berlaku_sip',TRUE));
            $tgl_berlaku_sip = date_format($tgl_berlaku2, 'Y-m-d');
            $data_ijin_p['berlaku_sd_sip'] = $tgl_berlaku_sip;

            $tgl_ver4 = date_create($this->input->post('tgl_verif_sip',TRUE));
            $tgl_terverifikasi4 = date_format($tgl_ver4, 'Y-m-d');
            $data_ijin_p['tgl_terverifikasi_sip'] = $tgl_terverifikasi4;

            /* Data Detail No Rekam Medis */
            $data_rm['no_rekam_medis'] = $this->input->post('no_rm',TRUE);


            $insert_data_karyawan = $this->Karyawan_model->insert_karyawan($data_karyawan);
            // print_r($data_karyawan);die();
            if ($insert_data_karyawan) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Register has been saved to database'
                );

                // if permitted, do logit
                $perms = "master_data_karyawan_create";
                $comments = "Success to Create a new patient register with post data";
                $this->aauth->logit($perms, current_url(), $comments);


                $data_pu['karyawan_id'] = $insert_data_karyawan;
                $data_pp['karyawan_id'] = $insert_data_karyawan;
                $data_ijin_p['karyawan_id'] = $insert_data_karyawan;

                $this->Karyawan_model->insert_pend_umum($data_pu);
                $this->Karyawan_model->insert_rm($data_rm);
                $this->Karyawan_model->insert_pend_profesi($data_pp);
                $this->Karyawan_model->insert_ijin_p($data_ijin_p);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Failed insert user group to database, please contact web administrator.');

                // if permitted, do logit
                $perms = "master_data_karyawan_create";
                $comments = "Failed to Create a new patient register when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }

        }
        echo json_encode($res);

    }

    public function detail_karyawan($karyawan_id){
        $karyawan['data_karyawan'] = $this->Karyawan_model->get_karyawan_by_id($karyawan_id);
        $karyawan['pendidikan_umum'] = $this->Karyawan_model->get_pendidikan_u($karyawan_id);
        $karyawan['pendidikan_profesi'] = $this->Karyawan_model->get_pendidikan_p($karyawan_id);
        $karyawan['izin_profesi'] = $this->Karyawan_model->get_izin_p($karyawan_id);

        $this->load->view('v_detail_karyawan',$karyawan);
    }

    public function do_delete_karyawan(){

        /* Permission setting di auuth perms */
        $is_permit = $this->aauth->control_no_redirect('master_data_karyawan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $karyawan_id = $this->input->post('karyawan_id', TRUE);
        $delete = $this->Karyawan_model->delete_karyawan($karyawan_id);
        if ($delete) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Karyawan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_karyawan_delete";
            $comments = "Berhasil menghapus list_pasien dengan id list_pasien = '". $karyawan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_karyawan_delete";
            $comments = "Gagal menghapus data list_pasien dengan ID = '". $karyawan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);

    }

    public function ajax_get_list_karyawan_by_id(){

        $karyawan_id = $this->input->get('karyawan_id',TRUE);
        $old_data = $this->Karyawan_model->get_list_karyawan_by_id($karyawan_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function ajax_get_list_ref_by_id(){

        $pasien_id = $this->input->get('pasien_id',TRUE);
        $old_data = $this->Karyawan_model->get_list_ref_by_id($pasien_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "ID tidak ditemukan");
        }

        echo json_encode($res);
    }


    public function do_update_karyawan(){

         /* Permission setting di auuth perms */
        $is_permit = $this->aauth->control_no_redirect('master_data_karyawan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');

        /* Data Karyawan */
        $this->form_validation->set_rules('no_rm', 'No. Rekam Medis', 'required|trim');
        $this->form_validation->set_rules('no_ktp', 'No. KTP', 'required|trim');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap Karyawan', 'required|trim');
        $this->form_validation->set_rules('nama_panggilan', 'Nama Panggilan Karyawan', 'required|trim');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|trim');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required|trim');
        $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim');

        /* Data Riwayat Pendidikan Umum*/
        $this->form_validation->set_rules('nama_sekolah', 'Nama Sekolah', 'required|trim');
        $this->form_validation->set_rules('jurusan', 'Jurusan', 'required|trim');
        $this->form_validation->set_rules('tahun_lulus_sekolah', 'Tahun Lulus Sekolah', 'required|trim');
        $this->form_validation->set_rules('no_ijazah_sekolah', 'No Ijazah Sekolah', 'required|trim');
        $this->form_validation->set_rules('tgl_verif_sekolah', 'Tanggal Terverifikasi Sekolah', 'required|trim');
        $this->form_validation->set_rules('strata', 'Strata', 'required|trim');

        /* Data Riwayat Pendidikan Profesi */
        $this->form_validation->set_rules('profesi', 'Profesi', 'required|trim');
        $this->form_validation->set_rules('spesialis', 'Spesialis', 'required|trim');
        $this->form_validation->set_rules('tahun_lulus_profesi', 'Tahun Lulus Profesi', 'required|trim');
        $this->form_validation->set_rules('no_ijazah_profesi', 'No Ijazah Profesi', 'required|trim');
        $this->form_validation->set_rules('tgl_verif_profesi', 'Tanggal Terverifikasi Profesi', 'required|trim');

        /* Jika ada form required yg belum diisi */
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error
            );

            // if permitted, do logit
            $perms = "master_data_karyawan_update";
            $comments = "Gagal melakukan update with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{

            /* Data Karyawan */
            $data_karyawan['no_karyawan'] = $this->input->post('no_karyawan',TRUE);
            $data_karyawan['nama_karyawan'] = $this->input->post('nama_lengkap',TRUE);
            $data_karyawan['nama_panggil_karyawan'] = $this->input->post('nama_panggilan',TRUE);
            $data_karyawan['no_rek_mandiri'] = $this->input->post('no_rek_mandiri',TRUE);
            $data_karyawan['no_bpjs'] = $this->input->post('no_bpjs',TRUE);
            $data_karyawan['no_bpjs_ketker'] = $this->input->post('no_bpjs_kerja',TRUE);
            $data_karyawan['no_rekam_medis'] = $this->input->post('no_rm',TRUE);
            $data_karyawan['no_ktp'] = $this->input->post('no_ktp',TRUE);
            $data_karyawan['email'] = $this->input->post('email',TRUE);
            $data_karyawan['jenis_kelamin'] = $this->input->post('jenis_kelamin',TRUE);
            $data_karyawan['alamat'] = $this->input->post('alamat',TRUE);
            $data_karyawan['agama_id'] = $this->input->post('agama',TRUE);
            $data_karyawan['status_kawin'] = $this->input->post('status_kawin',TRUE);
            $data_karyawan['tempat_lahir'] = $this->input->post('tempat_lahir',TRUE);

            $format_tanggal = date_create($this->input->post('tgl_lahir',TRUE));
            $tgl_lahir = date_format($format_tanggal, 'Y-m-d');

            $data_karyawan['tanggal_lahir'] = $tgl_lahir;
            $data_karyawan['umur'] = $this->input->post('umur',TRUE);
            $data_karyawan['orientasi'] = $this->input->post('orientasi',TRUE);
            $data_karyawan['lama_orientasi'] = $this->input->post('lama_orientasi',TRUE);
            $data_karyawan['tmt'] = $this->input->post('tmt',TRUE);

            $masa_kerja = date_create($this->input->post('masa_kerja',TRUE));
            $masa_kerja1 = date_format($masa_kerja, 'Y-m-d');
            $data_karyawan['masa_kerja'] = $masa_kerja1;

            $keluar = date_create($this->input->post('keluar',TRUE));
            $keluar1 = date_format($keluar, 'Y-m-d');
            $data_karyawan['keluar'] = $keluar1;
            $data_karyawan['alasan_keluar'] = $this->input->post('alasan_keluar',TRUE);
            $data_karyawan['unit_kerja_id'] = $this->input->post('unit_kerja_id',TRUE);
            $data_karyawan['ruang'] = $this->input->post('ruang',TRUE);
            $data_karyawan['penilaian_resiko'] = $this->input->post('penilaian_resiko',TRUE);
            $data_karyawan['status_pegawai'] = $this->input->post('status_pegawai',TRUE);
            $data_karyawan['no_kontrak'] = $this->input->post('no_kontrak',TRUE);

            $tgl_berakhir = date_create($this->input->post('tgl_berakhir',TRUE));
            $tgl_berakhir1 = date_format($tgl_berakhir, 'Y-m-d');
            $data_karyawan['tgl_berakhir'] = $tgl_berakhir1;
            $data_karyawan['kelompok_intensif'] = $this->input->post('kelompok_intensif',TRUE);
            $data_karyawan['jabatan'] = $this->input->post('jabatan',TRUE);
            $data_karyawan['jabatan_status'] = $this->input->post('jabatan_status',TRUE);
            $data_karyawan['status'] = $this->input->post('status',TRUE);
            $data_karyawan['hak_bersama'] = $this->input->post('hak_bersama',TRUE);
            $data_karyawan['hak_unit'] = $this->input->post('hak_unit',TRUE);
            $data_karyawan['penempatan'] = $this->input->post('penempatan',TRUE);

            $this->db->trans_begin();


            /* Data Riwayat Pendidikan Umum */
            $data_pu['strata'] = $this->input->post('strata',TRUE);
            $data_pu['jurusan'] = $this->input->post('jurusan',TRUE);
            $data_pu['tahun_lulus'] = $this->input->post('tahun_lulus_sekolah',TRUE);
            $data_pu['nama_sekolah'] = $this->input->post('nama_sekolah',TRUE);
            $data_pu['no_ijazah'] = $this->input->post('no_ijazah_sekolah',TRUE);
            $tgl_ver1 = date_create($this->input->post('tgl_verif_sekolah',TRUE));
            $tgl_terverifikasi1 = date_format($tgl_ver1, 'Y-m-d');
            $data_pu['tgl_terverifikasi'] = $tgl_terverifikasi1;


            /* Data Riwayat Pendidika Profesi */
            $data_pp['profesi'] = $this->input->post('profesi',TRUE);
            $data_pp['spesialis'] = $this->input->post('spesialis',TRUE);
            $data_pp['tahun'] = $this->input->post('tahun_lulus_profesi',TRUE);
            $data_pp['no_ijazah'] = $this->input->post('no_ijazah_profesi',TRUE);
            $tgl_ver2 = date_create($this->input->post('tgl_verif_profesi',TRUE));
            $tgl_terverifikasi2 = date_format($tgl_ver2, 'Y-m-d');
            $data_pp['tgl_terverifikasi'] = $tgl_terverifikasi2;

            /* Data Izin Profesi */
            $data_ijin_p['no_str'] = $this->input->post('no_str',TRUE);
            $tgl_terbit = date_create($this->input->post('tgl_terbit_izin',TRUE));
            $tgl_diterbitkan = date_format($tgl_terbit, 'Y-m-d');
            $data_ijin_p['tgl_diterbitkan'] = $tgl_diterbitkan;

            $tgl_berlaku = date_create($this->input->post('tgl_berlaku_izin',TRUE));
            $tgl_berlaku1 = date_format($tgl_berlaku, 'Y-m-d');
            $data_ijin_p['berlaku_sd'] = $tgl_berlaku1;

            $tgl_ver3 = date_create($this->input->post('tgl_verif_str',TRUE));
            $tgl_terverifikasi3 = date_format($tgl_ver3, 'Y-m-d');
            $data_ijin_p['tgl_terverifikasi'] = $tgl_terverifikasi3;

            $data_ijin_p['no_sip'] = $this->input->post('no_sip',TRUE);

            $tgl_terbit2 = date_create($this->input->post('tgl_terbit_sip',TRUE));
            $tgl_diterbitkan2 = date_format($tgl_terbit2, 'Y-m-d');
            $data_ijin_p['tgl_diterbitkan_sip'] = $tgl_diterbitkan2;

            $tgl_berlaku2 = date_create($this->input->post('tgl_berlaku_sip',TRUE));
            $tgl_berlaku_sip = date_format($tgl_berlaku2, 'Y-m-d');
            $data_ijin_p['berlaku_sd_sip'] = $tgl_berlaku_sip;

            $tgl_ver4 = date_create($this->input->post('tgl_verif_sip',TRUE));
            $tgl_terverifikasi4 = date_format($tgl_ver4, 'Y-m-d');
            $data_ijin_p['tgl_terverifikasi_sip'] = $tgl_terverifikasi4;

            /* Data Detail No Rekam Medis */
            // $data_rm['no_rekam_medis'] = $this->input->post('no_rm',TRUE);

            $karyawan_id = $this->input->post('karyawan_id',TRUE);


            $update_data_karyawan = $this->Karyawan_model->update_karyawan($data_karyawan,$karyawan_id);
            // print_r($data_karyawan);die();
            if ($update_data_karyawan) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Register has been saved to database'
                );

                // if permitted, do logit
                $perms = "master_data_karyawan_update";
                $comments = "Success to Create a new patient register with post data";
                $this->aauth->logit($perms, current_url(), $comments);


                // $data_pu['karyawan_id'] = $karyawan_id;
                // $data_pp['karyawan_id'] = $karyawan_id;
                // $data_ijin_p['karyawan_id'] = $karyawan_id;

                $this->Karyawan_model->update_pend_umum($data_pu,$karyawan_id);
                // $this->Karyawan_model->insert_rm($data_rm);
                $this->Karyawan_model->update_pend_profesi($data_pp,$karyawan_id);
                $this->Karyawan_model->update_ijin_p($data_ijin_p,$karyawan_id);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Failed insert user group to database, please contact web administrator.');

                // if permitted, do logit
                $perms = "master_data_karyawan_update";
                $comments = "Failed to Create a new patient register when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }

        }
        echo json_encode($res);

    }


}
