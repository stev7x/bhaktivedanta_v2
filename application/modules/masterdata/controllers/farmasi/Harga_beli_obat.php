<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Harga_beli_obat extends CI_Controller {
    public $data = array();

    public $table_harga_beli_obat = "farmasi_harga_obat";
    public $table_batch = "farmasi_batch";
    public $table_suplier = "farmasi_suplier";
    public $table_persediaan = "farmasi_persediaan";
    public $table_obat = "farmasi_obat";

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('farmasi/Models');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }

    public function cekdup($table, $wheres=array()) {
        if (count($this->Models->where($table, $wheres)) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_farmasi_harga_beli_obat_view');

        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        $this->data['batch'] = $this->Models->all($this->table_batch);
        $this->data['obat'] = $this->Models->all($this->table_obat);
        $this->data['suplier'] = $this->Models->all($this->table_suplier);

        // if permitted, do logit
        $perms = "master_data_farmasi_harga_beli_obat_view";
        $comments = "Listing Harga Beli Obat.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('farmasi/harga_beli_obat/list', $this->data);
    }

    public function ajax_list(){
        $list = $this->Models->all($this->table_harga_beli_obat);
        $obat = $this->Models->all($this->table_obat);
        $batch = $this->Models->all($this->table_batch);
        $suplier = $this->Models->all($this->table_suplier);
        $persediaan = $this->Models->all($this->table_persediaan);

        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value) {
            if ($value['status'] == 1) {
                $harga =
                    " - " . $this->convert_to_rupiah($value['harga_persediaan_1']) . ",00" . " / 1 " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                    " - " . $this->convert_to_rupiah($value['harga_persediaan_2']) . ",00" . " / 1 " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                    " - " . $this->convert_to_rupiah($value['harga_persediaan_3']) . ",00" . " / 1 " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'];

                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $obat[$value['obat_id']]['nama'];
                $row[] = $batch[$value['batch_id']]['number'];
                $row[] = $suplier[$value['suplier_id']]['nama'];
                $row[] = $harga;
                //add html for action
                $row[] = '
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit obat" onclick="edit('."'".$value['id']."'".')"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus obat" id="delete" onclick="hapus('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
                $data[] = $row;
            }
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_harga_beli_obat, $id);
        $data['batch'] = $this->Models->show($this->table_batch, $data['data']['batch_id']);
        $data['obat'] = $this->Models->show($this->table_obat, $data['data']['obat_id']);
        $data['persediaan'] = $this->Models->show($this->table_persediaan, $data['obat']['persediaan_id']);
        $data['suplier'] = $this->Models->show($this->table_suplier, $data['data']['suplier_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail Harga Beli Harga Beli Obat ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function ajax_detail_batch () {
        $id = $this->input->get('id', TRUE);
        $data['batch'] = $this->Models->show($this->table_batch, $id);
        $data['obat'] = $this->Models->show($this->table_obat, $data['batch']['obat_id']);
        $data['persediaan'] = $this->Models->show($this->table_persediaan, $data['obat']['persediaan_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail Harga Beli Harga Beli Obat ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function create() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('harga_persediaan_1', 'Harga Persediaan 1', 'required');
        $this->form_validation->set_rules('harga_persediaan_2', 'Harga Persediaan 2', 'required');
        $this->form_validation->set_rules('harga_persediaan_3', 'Harga Persediaan 3', 'required');
        $this->form_validation->set_rules('batch_id', 'Batch Number', 'required');
        $this->form_validation->set_rules('suplier_id', 'Suplier', 'required');
        $this->form_validation->set_rules('obat_id', 'Obat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal input harga_beli_obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $data = array(
                'harga_persediaan_1' => $this->input->post('harga_persediaan_1', TRUE),
                'harga_persediaan_2' => $this->input->post('harga_persediaan_2', TRUE),
                'harga_persediaan_3' => $this->input->post('harga_persediaan_3', TRUE),
                'batch_id' => $this->input->post('batch_id', TRUE),
                'suplier_id' => $this->input->post('suplier_id', TRUE),
                'obat_id' => $this->input->post('obat_id', TRUE),
                'status' => 1
            );

            $wheres = array(
                'batch_id' => $this->input->post('batch_id', TRUE),
                'suplier_id' => $this->input->post('suplier_id', TRUE),
                'obat_id' => $this->input->post('obat_id', TRUE)
            );

            if ($this->cekdup($this->table_harga_beli_obat, $wheres)) {
                if ($this->Models->insert($this->table_harga_beli_obat, $data)) {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'harga_beli_obat berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "";
                    $comments = "Berhasil menambahkan harga_beli_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                } else {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => 'Gagal menambahkan harga_beli_obat, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "";
                    $comments = "Gagal menambahkan harga_beli_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Data sudah ada'
                );
            }
        }

        echo json_encode($res);
    }

    public function update() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('harga_persediaan_1', 'Harga Persediaan 1', 'required');
        $this->form_validation->set_rules('harga_persediaan_2', 'Harga Persediaan 2', 'required');
        $this->form_validation->set_rules('harga_persediaan_3', 'Harga Persediaan 3', 'required');
        $this->form_validation->set_rules('batch_id', 'Batch Number', 'required');
        $this->form_validation->set_rules('suplier_id', 'Suplier', 'required');
        $this->form_validation->set_rules('obat_id', 'Obat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal memperbarui harga_beli_obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id = $this->input->post('id', TRUE);
            $data = array(
                'harga_persediaan_1' => $this->input->post('harga_persediaan_1', TRUE),
                'harga_persediaan_2' => $this->input->post('harga_persediaan_2', TRUE),
                'harga_persediaan_3' => $this->input->post('harga_persediaan_3', TRUE),
                'batch_id' => $this->input->post('batch_id', TRUE),
                'suplier_id' => $this->input->post('suplier_id', TRUE),
                'obat_id' => $this->input->post('obat_id', TRUE)
            );

            if ($this->Models->update($this->table_harga_beli_obat, $data, $id)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'harga_beli_obat berhasil diperbarui'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil memperbarui harga_beli_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal memperbarui harga_beli_obat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal memperbarui harga_beli_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function delete(){
        $id = $this->input->post('id', TRUE);

        if($this->Models->delete($this->table_harga_beli_obat, $id)){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'harga_beli_obat berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Berhasil menghapus harga_beli_obat dengan id = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus harga_beli_obat, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Gagal menghapus harga_beli_obat dengan ID = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }

    public function ajax_get_harga_beli_obat_by_id() {
        $id = $this->input->get('id', TRUE);

        $output = array(
            "success" => true,
            "message" => "Data Detail Harga Beli Obat ",
            "data" => $this->Models->show($this->table_harga_beli_obat, $id)
        );

        echo json_encode($output);
    }
}
