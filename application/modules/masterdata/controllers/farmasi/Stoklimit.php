<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stoklimit extends CI_Controller {
    public $data = array();
    public $table_stoklimit = "farmasi_stok_limit";
    public $table_obat = "farmasi_obat";
    public $table_persediaan = "farmasi_persediaan";

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('farmasi/Models');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_farmasi_stoklimit_view');

        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        $this->data['obat'] = $this->Models->all($this->table_obat);

        // if permitted, do logit
        $perms = "master_data_farmasi_stoklimit_view";
        $comments = "Listing StokLimit.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('farmasi/stok_limit/list', $this->data);
    }

    function convert_stok ($stok, $p2, $p3) {
        return array(
            'p1' => @bcdiv(($stok / ($p2 * $p3)), 1, 0),
            'p2' => @bcdiv(($stok % ($p2 * $p3) / $p3), 1, 0),
            'p3' => @$stok % $p3
        );
    }

    public function ajax_list(){
        $list = $this->Models->all($this->table_stoklimit);
        $obat = $this->Models->all($this->table_obat);
        $persediaan = $this->Models->all($this->table_persediaan);

        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value) {
            if (($value['status'] == 1) && !(empty($obat[$value['obat_id']]))) {
                $min = $this->convert_stok($value['min'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);
                $max = $this->convert_stok($value['max'], $obat[$value['obat_id']]['jumlah_persediaan_2'], $obat[$value['obat_id']]['jumlah_persediaan_3']);

                $p_min =
                    " - " . $min['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                    " - " . $min['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                    " - " . $min['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

                $p_max =
                    " - " . $max['p1'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_1'] . "<br>" .
                    " - " . $max['p2'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_2'] . "<br>" .
                    " - " . $max['p3'] . " " . $persediaan[$obat[$value['obat_id']]['persediaan_id']]['persediaan_3'] . "<br>";

                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $obat[$value['obat_id']]['nama'];
                $row[] = $p_min;
                $row[] = $p_max;
                //add html for action
                $row[] = '
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit obat" onclick="edit('."'".$value['id']."'".')"><i class="fa fa-pencil"></i></button>';
//                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus obat" id="delete" onclick="hapus('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
                $data[] = $row;
            }
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail() {
        $id = $this->input->get('id', TRUE);

        $data['data'] = $this->Models->show($this->table_stoklimit, $id);
        $data['obat'] =  $this->Models->show($this->table_obat, $data['data']['obat_id']);
        $data['persediaan'] =  $this->Models->show($this->table_persediaan, $data['obat']['persediaan_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail StokLimit ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function ajax_obat() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_obat, $id);
        $data['persediaan'] = $this->Models->show($this->table_persediaan, $data['data']['persediaan_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail Obat ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function cekdup($table, $wheres=array()) {
        if (count($this->Models->where($table, $wheres)) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function create() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('obat_id', 'Nama Obat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal input stok limit dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $obat_id = $this->input->post('obat_id', TRUE);
            $obat = $this->Models->show($this->table_obat, $obat_id);

            $min_1 = $this->input->post('min_1', TRUE);
            $min_2 = $this->input->post('min_2', TRUE);
            $min_3 = $this->input->post('min_3', TRUE);
            $max_1 = $this->input->post('max_1', TRUE);
            $max_2 = $this->input->post('max_2', TRUE);
            $max_3 = $this->input->post('max_3', TRUE);

            $p_min_1 = $min_1 * ($obat['jumlah_persediaan_2'] * $obat['jumlah_persediaan_3']);
            $p_min_2 = $min_2 * $obat['jumlah_persediaan_3'];
            $p_min_3 = $min_3;
            $p_max_1 = $max_1 * ($obat['jumlah_persediaan_2'] * $obat['jumlah_persediaan_3']);
            $p_max_2 = $max_2 * $obat['jumlah_persediaan_3'];
            $p_max_3 = $max_3;

            $min = $p_min_1 + $p_min_2 + $p_min_3;
            $max = $p_max_1 + $p_max_2 + $p_max_3;

            $data = array(
                'min' => $min,
                'max' => $max,
                'obat_id' => $this->input->post('obat_id', TRUE),
                'status' => 1
            );

            if ($this->cekdup($this->table_stoklimit, array('obat_id' => $this->input->post('obat_id', TRUE)))) {
                if ($this->Models->insert($this->table_stoklimit, $data)) {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'stok lmit berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "";
                    $comments = "Berhasil menambahkan stok limit dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                } else {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => 'Gagal menambahkan stok limit, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "";
                    $comments = "Gagal menambahkan stok limit dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Data sudah ada'
                );
            }
        }

        echo json_encode($res);
    }

    public function update() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('obat_id', 'Nama Obat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal memperbarui stok limit dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id = $this->input->post('id', TRUE);
            $obat_id = $this->input->post('obat_id', TRUE);
            $obat = $this->Models->show($this->table_obat, $obat_id);

            $min_1 = $this->input->post('min_1', TRUE);
            $min_2 = $this->input->post('min_2', TRUE);
            $min_3 = $this->input->post('min_3', TRUE);
            $max_1 = $this->input->post('max_1', TRUE);
            $max_2 = $this->input->post('max_2', TRUE);
            $max_3 = $this->input->post('max_3', TRUE);

            $p_min_1 = $min_1 * ($obat['jumlah_persediaan_2'] * $obat['jumlah_persediaan_3']);
            $p_min_2 = $min_2 * $obat['jumlah_persediaan_3'];
            $p_min_3 = $min_3;
            $p_max_1 = $max_1 * ($obat['jumlah_persediaan_2'] * $obat['jumlah_persediaan_3']);
            $p_max_2 = $max_2 * $obat['jumlah_persediaan_3'];
            $p_max_3 = $max_3;

            $min = $p_min_1 + $p_min_2 + $p_min_3;
            $max = $p_max_1 + $p_max_2 + $p_max_3;

            $data = array(
                'min' => $min,
                'max' => $max,
                'obat_id' => $this->input->post('obat_id', TRUE)
            );

            if ($this->Models->update($this->table_stoklimit, $data, $id)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'stok limit berhasil diperbarui'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil memperbarui stok limit dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal memperbarui stok limit, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal memperbarui stok limit dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function delete(){
        $id = $this->input->post('id', TRUE);

        if($this->Models->delete($this->table_stoklimit, $id)){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'stok limit berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Berhasil menghapus stok limit dengan id = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus stok limit, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Gagal menghapus stok limit dengan ID = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }
}