<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {
    public $data = array();
    public $table_obat = "farmasi_obat";
    public $table_persediaan = "farmasi_persediaan";
    public $table_jenis_barang = "farmasi_jenis_barang";

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('farmasi/Models');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_farmasi_obat_view');

        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        $this->data['jenis_barang'] = $this->Models->all($this->table_jenis_barang);
        $this->data['persediaan'] = $this->Models->all($this->table_persediaan);

        // if permitted, do logit
        $perms = "master_data_farmasi_obat_view";
        $comments = "Listing Obat.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('farmasi/obat/list', $this->data);
    }

    public function ajax_list(){
        $list = $this->Models->all($this->table_obat);
        $jenisBarang = $this->Models->all($this->table_jenis_barang);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value) {
            $no++;
            $row = array();
            $row[] = $no;
//            $row[] = $value['batch_number'];
            $row[] = $value['nama'];
            $row[] = $jenisBarang[$value['jenis_barang_id']]['nama'];
//            $row[] = date('d - m - Y', strtotime($value['exp_date']));
            $row[] = $value['keterangan'];
            //add html for action
            $row[] = '
                <button class="btn btn-info btn-circle" data-toggle="modal" data-target="#modal_detail" data-backdrop="static" data-keyboard="false" title="klik untuk detail obat" onclick="detail('."'".$value['id']."'".')"><i class="fa fa-info"></i></button>
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit obat" onclick="edit('."'".$value['id']."'".')"><i class="fa fa-pencil"></i></button>';
//                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus obat" id="delete" onclick="hapus('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_obat, $id);
        $data['jenis_barang'] = $this->Models->show($this->table_jenis_barang, $data['data']['jenis_barang_id']);
        $data['persediaan'] = $this->Models->show($this->table_persediaan, $data['data']['persediaan_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail Obat ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function create() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_obat', 'Nama Obat', 'required');
        $this->form_validation->set_rules('jenis_barang', 'Jenis Barang', 'required');
        $this->form_validation->set_rules('persediaan', 'Persediaan', 'required');

        $this->form_validation->set_rules('jumlah_persediaan_1', 'Jumlah Persediaan 1', 'required');
        $this->form_validation->set_rules('jumlah_persediaan_2', 'Jumlah Persediaan 2', 'required');
        $this->form_validation->set_rules('jumlah_persediaan_3', 'Jumlah Persediaan 3', 'required');

        $this->form_validation->set_rules('het_persediaan_1', 'HET Persediaan 1', 'required');
        $this->form_validation->set_rules('het_persediaan_2', 'HET Persediaan 2', 'required');
        $this->form_validation->set_rules('het_persediaan_3', 'HET Persediaan 3', 'required');

        $this->form_validation->set_rules('harga_visit_local', 'Harga Visit Local', 'required');
        $this->form_validation->set_rules('harga_visit_dome', 'Harga Visit Domestik', 'required');
        $this->form_validation->set_rules('harga_visit_asing', 'Harga Visit Asing', 'required');

        $this->form_validation->set_rules('harga_oncall_local', 'Harga Oncall Local', 'required');
        $this->form_validation->set_rules('harga_oncall_dome', 'Harga Oncall Dome', 'required');
        $this->form_validation->set_rules('harga_oncall_asing', 'Harga Oncall Asing', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal input obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $data = array(
                'nama' => $this->input->post('nama_obat', TRUE),
                'jumlah_persediaan_1' => $this->input->post('jumlah_persediaan_1', TRUE),
                'jumlah_persediaan_2' => $this->input->post('jumlah_persediaan_2', TRUE),
                'jumlah_persediaan_3' => $this->input->post('jumlah_persediaan_3', TRUE),
                'het_persediaan_1' => $this->input->post('het_persediaan_1', TRUE),
                'het_persediaan_2' => $this->input->post('het_persediaan_2', TRUE),
                'het_persediaan_3' => $this->input->post('het_persediaan_3', TRUE),
                'harga_visit_local' => $this->input->post('harga_visit_local', TRUE),
                'harga_visit_dome' => $this->input->post('harga_visit_dome', TRUE),
                'harga_visit_asing' => $this->input->post('harga_visit_asing', TRUE),
                'harga_oncall_local' => $this->input->post('harga_oncall_local', TRUE),
                'harga_oncall_dome' => $this->input->post('harga_oncall_dome', TRUE),
                'harga_oncall_asing' => $this->input->post('harga_oncall_asing', TRUE),
                'keterangan' => $this->input->post('keterangan', TRUE),
                'jenis_barang_id' => $this->input->post('jenis_barang', TRUE),
                'persediaan_id' => $this->input->post('persediaan', TRUE)
            );

            if ($data['jumlah_persediaan_1'] < 1 || $data['jumlah_persediaan_2'] < 1 || $data['jumlah_persediaan_3'] < 1) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Jumlah persediaan tidak boleh diisi dengan angka 0 (nol)');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal menambahkan obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);

                echo json_encode($res);
                exit;
            }

            if ($this->Models->insert($this->table_obat, $data)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'obat berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil menambahkan obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan obat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal menambahkan obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function update() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'id', 'required');
        $this->form_validation->set_rules('nama_obat', 'Nama Obat', 'required');
        $this->form_validation->set_rules('jenis_barang', 'Jenis Barang', 'required');
        $this->form_validation->set_rules('persediaan', 'Persediaan', 'required');

        $this->form_validation->set_rules('jumlah_persediaan_1', 'Jumlah Persediaan 1', 'required');
        $this->form_validation->set_rules('jumlah_persediaan_2', 'Jumlah Persediaan 2', 'required');
        $this->form_validation->set_rules('jumlah_persediaan_3', 'Jumlah Persediaan 3', 'required');

        $this->form_validation->set_rules('het_persediaan_1', 'HET Persediaan 1', 'required');
        $this->form_validation->set_rules('het_persediaan_2', 'HET Persediaan 2', 'required');
        $this->form_validation->set_rules('het_persediaan_3', 'HET Persediaan 3', 'required');

        $this->form_validation->set_rules('harga_visit_local', 'Harga Visit Local', 'required');
        $this->form_validation->set_rules('harga_visit_dome', 'Harga Visit Domestik', 'required');
        $this->form_validation->set_rules('harga_visit_asing', 'Harga Visit Asing', 'required');

        $this->form_validation->set_rules('harga_oncall_local', 'Harga Oncall Local', 'required');
        $this->form_validation->set_rules('harga_oncall_dome', 'Harga Oncall Dome', 'required');
        $this->form_validation->set_rules('harga_oncall_asing', 'Harga Oncall Asing', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal memperbarui obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id = $this->input->post('id', TRUE);
            $data = array(
                'nama' => $this->input->post('nama_obat', TRUE),
                'jumlah_persediaan_1' => $this->input->post('jumlah_persediaan_1', TRUE),
                'jumlah_persediaan_2' => $this->input->post('jumlah_persediaan_2', TRUE),
                'jumlah_persediaan_3' => $this->input->post('jumlah_persediaan_3', TRUE),
                'het_persediaan_1' => $this->input->post('het_persediaan_1', TRUE),
                'het_persediaan_2' => $this->input->post('het_persediaan_2', TRUE),
                'het_persediaan_3' => $this->input->post('het_persediaan_3', TRUE),
                'harga_visit_local' => $this->input->post('harga_visit_local', TRUE),
                'harga_visit_dome' => $this->input->post('harga_visit_dome', TRUE),
                'harga_visit_asing' => $this->input->post('harga_visit_asing', TRUE),
                'harga_oncall_local' => $this->input->post('harga_oncall_local', TRUE),
                'harga_oncall_dome' => $this->input->post('harga_oncall_dome', TRUE),
                'harga_oncall_asing' => $this->input->post('harga_oncall_asing', TRUE),
                'keterangan' => $this->input->post('keterangan', TRUE),
                'jenis_barang_id' => $this->input->post('jenis_barang', TRUE),
                'persediaan_id' => $this->input->post('persediaan', TRUE)
            );

            if ($this->Models->update($this->table_obat, $data, $id)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'obat berhasil diperbarui'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil memperbarui obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal memperbarui obat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal memperbarui obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function delete(){
        $id = $this->input->post('id', TRUE);

        if($this->Models->delete($this->table_obat, $id)){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'obat berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Berhasil menghapus obat dengan id = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus obat, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Gagal menghapus obat dengan ID = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }
}
