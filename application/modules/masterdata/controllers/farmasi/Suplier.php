<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suplier extends CI_Controller {
    public $data = array();
    public $table_suplier = "farmasi_suplier";

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('farmasi/Models');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_farmasi_suplier_view');

        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_farmasi_suplier_view";
        $comments = "Listing Suplier.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('farmasi/suplier/list', $this->data);
    }

    public function ajax_list(){
        $list = $this->Models->all($this->table_suplier);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $value['nama'];
            $row[] = $value['alamat'];
            $row[] = $value['npwp'];
            $row[] = $value['nama_marketing'];
            $row[] = $value['no_telp_marketing'];
            $row[] = date('d - m - Y', strtotime($value['tanggal_perjanjian_habis']));
            //add html for action
            $row[] = '
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit obat" onclick="edit('."'".$value['id']."'".')"><i class="fa fa-pencil"></i></button>';
//                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus obat" id="delete" onclick="hapus('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_suplier, $id);
        $data['data']['tanggal_perjanjian_habis'] = date('d/m/Y', strtotime($data['data']['tanggal_perjanjian_habis']));

        $output = array(
            "success" => true,
            "message" => "Data Detail Suplier ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function create() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Suplier', 'required');
        $this->form_validation->set_rules('tanggal_perjanjian_habis', 'Tanggal Perjanjian Habis', 'required');
//        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
//        $this->form_validation->set_rules('npwp', 'No NPWP', 'required');
//        $this->form_validation->set_rules('nama_marketing', 'Nama Marketing', 'required');
//        $this->form_validation->set_rules('no_telp_marketing', 'No Telepon Marketing', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal input suplier dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
                'tanggal_perjanjian_habis' => date('Y-m-d', strtotime($this->input->post('tanggal_perjanjian_habis', TRUE))),
                'alamat' => $this->input->post('alamat', TRUE),
                'npwp' => $this->input->post('npwp', TRUE),
                'nama_marketing' => $this->input->post('nama_marketing', TRUE),
                'no_telp_marketing' => $this->input->post('no_telp_marketing', TRUE)
            );

            if ($this->Models->insert($this->table_suplier, $data)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'supplier berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil menambahkan suplier dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan suplier, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal menambahkan suplier dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function update() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'Nama Suplier', 'required');
        $this->form_validation->set_rules('tanggal_perjanjian_habis', 'Tanggal Perjanjian Habis', 'required');
//        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
//        $this->form_validation->set_rules('npwp', 'No NPWP', 'required');
//        $this->form_validation->set_rules('nama_marketing', 'Nama Marketing', 'required');
//        $this->form_validation->set_rules('no_telp_marketing', 'No Telepon Marketing', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal memperbarui suplier dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id = $this->input->post('id', TRUE);
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
                'tanggal_perjanjian_habis' => date('Y-m-d', strtotime($this->input->post('tanggal_perjanjian_habis', TRUE))),
                'alamat' => $this->input->post('alamat', TRUE),
                'npwp' => $this->input->post('npwp', TRUE),
                'nama_marketing' => $this->input->post('nama_marketing', TRUE),
                'no_telp_marketing' => $this->input->post('no_telp_marketing', TRUE)
            );

            if ($this->Models->update($this->table_suplier, $data, $id)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'suplier berhasil diperbarui'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil memperbarui suplier dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal memperbarui suplier, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal memperbarui suplier dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function delete(){
        $id = $this->input->post('id', TRUE);

        if($this->Models->delete($this->table_suplier, $id)){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'suplier berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Berhasil menghapus suplier dengan id = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus suplier, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Gagal menghapus suplier dengan ID = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }
}
