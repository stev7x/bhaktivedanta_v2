<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Batch extends CI_Controller {
    public $data = array();
    public $table_batch = "farmasi_batch";
    public $table_obat = "farmasi_obat";

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('farmasi/Models');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_farmasi_batch_view');

        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        $this->data['obat'] = $this->Models->all($this->table_obat);

        // if permitted, do logit
        $perms = "master_data_farmasi_batch_view";
        $comments = "Listing Batch.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('farmasi/batch/list', $this->data);
    }

    public function ajax_list(){
        $list = $this->Models->all($this->table_batch);
        $obat = $this->Models->all($this->table_obat);

        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value) {
            if ($value['status'] == 1) {
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $value['number'];
                $row[] = date('d - m - Y', strtotime($value['exp_date']));
                $row[] = $obat[$value['obat_id']]['nama'];
                //add html for action
                $row[] = '
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit obat" onclick="edit('."'".$value['id']."'".')"><i class="fa fa-pencil"></i></button>';
//                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus obat" id="delete" onclick="hapus('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
                $data[] = $row;
            }
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_batch, $id);

        $output = array(
            "success" => true,
            "message" => "Data Detail Batch ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function create() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('number', 'number', 'required');
        $this->form_validation->set_rules('exp_date', 'exp_date', 'required');
        $this->form_validation->set_rules('obat_id', 'obat_id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal input batch dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $data = array(
                'number'   => $this->input->post('number', TRUE),
                'exp_date' =>  date('Y-m-d', strtotime($this->input->post('exp_date', TRUE))),
                'obat_id'  => $this->input->post('obat_id', TRUE),
                'status'   => 1
            );

            if ($this->Models->insert($this->table_batch, $data)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'batch berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil menambahkan batch dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan batch, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal menambahkan batch dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function update() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('number', 'number', 'required');
        $this->form_validation->set_rules('exp_date', 'exp_date', 'required');
        $this->form_validation->set_rules('obat_id', 'obat_id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal memperbarui batch dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id = $this->input->post('id', TRUE);
            $data = array(
                'number' => $this->input->post('number', TRUE),
                'exp_date' =>  date('Y-m-d', strtotime($this->input->post('exp_date', TRUE))),
                'obat_id' => $this->input->post('obat_id', TRUE)
            );

            if ($this->Models->update($this->table_batch, $data, $id)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'batch berhasil diperbarui'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil memperbarui batch dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal memperbarui batch, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal memperbarui batch dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function delete(){
        $id = $this->input->post('id', TRUE);

        if($this->Models->delete($this->table_batch, $id)){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'batch berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Berhasil menghapus batch dengan id = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus batch, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Gagal menghapus batch dengan ID = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }

    public function ajax_get_batch_by_id() {
        $id = $this->input->get('id', TRUE);

        $output = array(
            "success" => true,
            "message" => "Data Detail Batch ",
            "data" => $this->Models->show($this->table_batch, $id)
        );

        echo json_encode($output);
    }
}
