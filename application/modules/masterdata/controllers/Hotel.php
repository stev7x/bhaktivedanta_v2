<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Hotel_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_hotel_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_hotel_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_hotel', $this->data);
    }

    public function ajax_list_hotel(){
        $list = $this->Hotel_model->get_hotel_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $hotel){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $hotel->nama_hotel;
            $row[] = $hotel->alamat;
            $row[] = $hotel->no_telp;
            $row[] = $hotel->email;
            $row[] = $hotel->PIC;
            $row[] = date('d M Y', strtotime($hotel->expired));
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_hotel" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit hotel" onclick="editHotel('."'".$hotel->id_hotel."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus hotel" id="hapusHotel" onclick="hapusHotel('."'".$hotel->id_hotel."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Hotel_model->count_hotel_all(),
                    "recordsFiltered" => $this->Hotel_model->count_hotel_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_hotel(){
        $is_permit = $this->aauth->control_no_redirect('master_data_hotel_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        // $this->form_validation->set_rules('id_hotel', 'id_hotel', 'required');
        $this->form_validation->set_rules('nama_hotel', 'Nama Hotel', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('pic', 'pic', 'required');
        $this->form_validation->set_rules('no_telp', 'No Telp', 'required');
        $this->form_validation->set_rules('expired', 'Expired', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_hotel_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            // $id_hotel = $this->input->post('id_hotel', TRUE);
            $nama_hotel = $this->input->post('nama_hotel', TRUE);
            $alamat    = $this->input->post('alamat', TRUE);
            $email    = $this->input->post('email', TRUE);
            $pic    = $this->input->post('pic', TRUE);
            $no_telp    = $this->input->post('no_telp', TRUE);
            $expired    = $this->input->post('expired', TRUE);
            $data_hotel = array(
                
                'nama_hotel' => $nama_hotel,
                'alamat'         => $alamat,
                'no_telp'         => $no_telp,
                'email'         => $email,                
                'PIC' => $pic,
                'expired' => date('Y-m-d', strtotime($expired))
            );

            $ins = $this->Hotel_model->insert_hotel($data_hotel);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'hotel berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_hotel_create";
                $comments = "Berhasil menambahkan hotel dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan hotel, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_hotel_create";
                $comments = "Gagal menambahkan hotel dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_hotel_by_id(){
        $id_hotel = $this->input->get('id_hotel',TRUE);
        $old_data = $this->Hotel_model->get_hotel_by_id($id_hotel);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_hotel(){
        $is_permit = $this->aauth->control_no_redirect('master_data_hotel_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_hotel', 'Nama Hotel', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('pic', 'pic', 'required');
        $this->form_validation->set_rules('no_telp', 'No Telp', 'required');
        $this->form_validation->set_rules('expired', 'Expired', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_hotel_update";
            $comments = "Gagal update hotel dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_hotel = $this->input->post('id_hotel', TRUE);
            $nama_hotel = $this->input->post('nama_hotel', TRUE);
            $alamat    = $this->input->post('alamat', TRUE);
            $email    = $this->input->post('email', TRUE);
            $pic    = $this->input->post('pic', TRUE);
            $no_telp    = $this->input->post('no_telp', TRUE);
            $expired    = $this->input->post('expired', TRUE);

            $data_hotel = array(

                'nama_hotel' => $nama_hotel,
                'alamat'         => $alamat,
                'no_telp'         => $no_telp,
                'email'         => $email,
                'PIC' => $pic,
                'expired' => date('Y-m-d', strtotime($expired))
            );

            $update = $this->Hotel_model->update_hotel($data_hotel, $id_hotel);
            // print_r($data_hotel);
            // echo $id_hotel;
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'hotel berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_hotel_update";
                $comments = "Berhasil mengubah hotel  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah hotel , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_hotel_update";
                $comments = "Gagal mengubah hotel  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_hotel(){
        $is_permit = $this->aauth->control_no_redirect('master_data_hotel_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_hotel = $this->input->post('id_hotel', TRUE);

        $delete = $this->Hotel_model->delete_hotel($id_hotel);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'hotel berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_hotel_delete";
            $comments = "Berhasil menghapus hotel dengan id hotel = '". $id_hotel ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_hotel_delete";
            $comments = "Gagal menghapus data hotel dengan ID = '". $id_hotel ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
