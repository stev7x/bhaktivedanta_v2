<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Efek_samping_obat extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Efek_samping_obat_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_efek_samping_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_efek_samping_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_efek_samping_obat', $this->data);
    }

    public function ajax_list_efek_samping(){
        $list = $this->Efek_samping_obat_model->get_efek_samping_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $efek_samping){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $efek_samping->kode_efek_samping;
            $row[] = $efek_samping->nama_efek_samping;
            $row[] = $efek_samping->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_efek_samping_obat" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit efek_samping" onclick="editEfekSamping('."'".$efek_samping->id_efek_samping."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus efek_samping" id="hapusEfekSamping" onclick="hapusEfekSamping('."'".$efek_samping->id_efek_samping."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Efek_samping_obat_model->count_efek_samping_all(),
                    "recordsFiltered" => $this->Efek_samping_obat_model->count_efek_samping_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_efek_samping(){
        $is_permit = $this->aauth->control_no_redirect('master_data_efek_samping_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_efek_samping', 'Kode Efek Samping obat', 'required');
        $this->form_validation->set_rules('nama_efek_samping', 'Nama Efek Samping obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_efek_samping_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $kode_efek_samping = $this->input->post('kode_efek_samping', TRUE);
            $nama_efek_samping = $this->input->post('nama_efek_samping', TRUE);
            $keterangan    = $this->input->post('keterangan', TRUE);
            $data_efek_samping = array(
                'kode_efek_samping' => $kode_efek_samping,
                'nama_efek_samping' => $nama_efek_samping,
                'keterangan'         => $keterangan,
            );

            $ins = $this->Efek_samping_obat_model->insert_efek_samping($data_efek_samping);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'efek_samping berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_efek_samping_create";
                $comments = "Berhasil menambahkan efek_samping dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan efek_samping, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_efek_samping_create";
                $comments = "Gagal menambahkan efek_samping dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_efek_samping_by_id(){
        $id_efek_samping = $this->input->get('id_efek_samping',TRUE);
        $old_data = $this->Efek_samping_obat_model->get_efek_samping_by_id($id_efek_samping);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_efek_samping(){
        $is_permit = $this->aauth->control_no_redirect('master_data_efek_samping_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kode_efek_samping', 'Kode Efek Samping obat', 'required');
        $this->form_validation->set_rules('upd_nama_efek_samping', 'Nama Efek Samping obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_efek_samping_update";
            $comments = "Gagal update efek_samping dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_efek_samping = $this->input->post('upd_id_efek_samping',TRUE);
            $kode_efek_samping = $this->input->post('upd_kode_efek_samping', TRUE);
            $nama_efek_samping = $this->input->post('upd_nama_efek_samping', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_efek_samping = array(
                'kode_efek_samping' => $kode_efek_samping,
                'nama_efek_samping' => $nama_efek_samping,
                'keterangan' => $keterangan
            );

            $update = $this->Efek_samping_obat_model->update_efek_samping($data_efek_samping, $id_efek_samping);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'efek_samping berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_efek_samping_update";
                $comments = "Berhasil mengubah efek_samping  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah efek_samping , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_efek_samping_update";
                $comments = "Gagal mengubah efek_samping  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_efek_samping(){
        $is_permit = $this->aauth->control_no_redirect('master_data_efek_samping_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_efek_samping = $this->input->post('id_efek_samping', TRUE);

        $delete = $this->Efek_samping_obat_model->delete_efek_samping($id_efek_samping);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'efek_samping berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_efek_samping_delete";
            $comments = "Berhasil menghapus efek_samping dengan id efek_samping = '". $id_efek_samping ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_efek_samping_delete";
            $comments = "Gagal menghapus data efek_samping dengan ID = '". $id_efek_samping ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

     function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Efek Samping Obat");
        $object->getActiveSheet()
                        ->getStyle("A1:D1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:D1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No","Kode Efek Samping","Nama Efek Samping","Keterangan");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Efek_samping_obat_model->get_efek_samping_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->kode_efek_samping);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama_efek_samping);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->keterangan);

            $excel_row++;
        }

        foreach(range('A','D') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);


        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Efek Samping Obat.xlsx"');
        $object_writer->save('php://output');
    }
}
