<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_kerja extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library('excel');


        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Unit_kerja_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_unit_kerja_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_unit_kerja_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_unit_kerja', $this->data);
    }

    public function ajax_list_unit_kerja(){
        $list = $this->Unit_kerja_model->get_unit_kerja_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $unit_kerja){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $unit_kerja->unit_kerja_nama;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_unit_kerja" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit unit kerja" onclick="editUnitKerja('."'".$unit_kerja->unit_kerja_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus unit kerja" id="hapusUnitKerja" onclick="hapusUnitKerja('."'".$unit_kerja->unit_kerja_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Unit_kerja_model->count_unit_kerja_all(),
                    "recordsFiltered" => $this->Unit_kerja_model->count_unit_kerja_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_unit_kerja(){
        $is_permit = $this->aauth->control_no_redirect('master_data_unit_kerja_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('unit_kerja_nama', 'Nama Unit Kerja', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_unit_kerja_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $unit_kerja_nama = $this->input->post('unit_kerja_nama', TRUE);
            $data_unit_kerja = array(
                'unit_kerja_nama' => $unit_kerja_nama,
            );

            $ins = $this->Unit_kerja_model->insert_unit_kerja($data_unit_kerja);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Unit Kerja berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_unit_kerja_create";
                $comments = "Berhasil menambahkan Unit Kerja dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Unit Kerja, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_unit_kerja_create";
                $comments = "Gagal menambahkan Unit Kerja dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_unit_kerja_by_id(){
        $unit_kerja_id = $this->input->get('unit_kerja_id',TRUE);
        $old_data = $this->Unit_kerja_model->get_unit_kerja_by_id($unit_kerja_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_unit_kerja(){
        $is_permit = $this->aauth->control_no_redirect('master_data_unit_kerja_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_unit_kerja_nama', 'Unit Kerja Nama', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_unit_kerja_update";
            $comments = "Gagal update unit kerja dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $unit_kerja_id = $this->input->post('upd_unit_kerja_id',TRUE);
            $unit_kerja_nama = $this->input->post('upd_unit_kerja_nama', TRUE);

            $data_unit_kerja = array(
                'unit_kerja_nama' => $unit_kerja_nama
            );

            $update = $this->Unit_kerja_model->update_unit_kerja($data_unit_kerja, $unit_kerja_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Agama berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_unit_kerja_update";
                $comments = "Berhasil mengubah unit kerja  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah unit kerja , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_unit_kerja_update";
                $comments = "Gagal mengubah agama  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_unit_kerja(){
        $is_permit = $this->aauth->control_no_redirect('master_data_unit_kerja_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $unit_kerja_id = $this->input->post('unit_kerja_id', TRUE);

        $delete = $this->Unit_kerja_model->delete_unit_kerja($unit_kerja_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Unit Kerja berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_unit_kerja_delete";
            $comments = "Berhasil menghapus unit kerja = '". $unit_kerja_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_unit_kerja_delete";
            $comments = "Gagal menghapus data unit kerja dengan ID = '". $unit_kerja_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }


    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Unit Kerja");
        $object->getActiveSheet()
                        ->getStyle("A1:B1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:B1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nama Unit Kerja");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Unit_kerja_model->get_unit_kerja_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->unit_kerja_nama);
            $excel_row++;
        }

        foreach(range('A','B') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Unit Kerja.xlsx"');
        $object_writer->save('php://output');
    }

    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType  = PHPExcel_IOFactory::identify($input_file_name);
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel    = $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows      = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet   = $objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {
                // $unit_kerja_id      = $objWorksheet->getCellByColumnAndRow(0,$row)->getValue(); /
                $unit_kerja_nama    = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1

                $data = array(
                    'unit_kerja_nama' => $unit_kerja_nama
                );

                $this->db->insert("m_unit_kerja", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);
        }
    }
}
