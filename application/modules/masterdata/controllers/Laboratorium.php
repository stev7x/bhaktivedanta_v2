<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Laboratorium_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_laboratorium_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_laboratorium_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_laboratorium', $this->data);
    }

    public function ajax_list_lab(){
        $list = $this->Laboratorium_model->get_lab_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $hotel){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $hotel->daftartindakan_nama;
            $row[] = $hotel->harga_tindakan;
            $row[] = $hotel->jenis_pasien;
            $row[] = $hotel->type_call;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit hotel" onclick="editMaster('."'".$hotel->daftartindakan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus hotel" id="hapusHotel" onclick="hapus_master('."'".$hotel->daftartindakan_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laboratorium_model->count_lab_all(),
                    "recordsFiltered" => $this->Laboratorium_model->count_lab_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_lab(){
        $is_permit = $this->aauth->control_no_redirect('master_data_laboratorium_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('daftartindakan_nama', 'Nama Tindakan', 'required');
        $this->form_validation->set_rules('harga_tindakan', 'Harga Tindakan', 'required');
        $this->form_validation->set_rules('jenis_pasien', 'Jenis Pasien', 'required');
        $this->form_validation->set_rules('type_call', 'Tipe Call', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_laboratorium_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            // $id_hotel = $this->input->post('id_hotel', TRUE);
            $daftartindakan_nama = $this->input->post('daftartindakan_nama', TRUE);
            $harga_tindakan    = $this->input->post('harga_tindakan', TRUE);
            $jenis_pasien    = $this->input->post('jenis_pasien', TRUE);
            $type_call    = $this->input->post('type_call', TRUE);

            $data_hotel = array(
                
                'daftartindakan_nama' => $daftartindakan_nama,
                'kelompoktindakan_id' => '2',
                'kelaspelayanan_id' => '4',
                'harga_tindakan'         => $harga_tindakan,
                'cyto_tindakan' => $harga_tindakan,
                'bpjs_non_bpjs' => 'Non BPJS',
                'komponentarif_id' => '1',
                'icd9_cm_id' => '1051',
                'total_harga_bpjs_cyto' => '0',
                'total_harga_bpjs_non_cyto' => '0',
                'total_harga_umum_cyto' => '0',
                'total_harga_umum_non_cyto' => '0',           
                'jenis_pasien'         => $jenis_pasien,
                'type_call'         => $type_call,
                'type_tindakan' => 'LABORATORIUM'                
            );

            $ins = $this->Laboratorium_model->insert_lab($data_hotel);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_laboratorium_create";
                $comments = "Berhasil menambahkan Hotel dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Laboratorium, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_laboratorium_create";
                $comments = "Gagal menambahkan Laboratorium dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_lab_by_id(){
        $daftartindakan_id = $this->input->get('daftartindakan_id',TRUE);
        $old_data = $this->Laboratorium_model->get_lab_by_id($daftartindakan_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_lab(){
        $is_permit = $this->aauth->control_no_redirect('master_data_laboratorium_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('daftartindakan_nama', 'Nama Tindakan', 'required');
        $this->form_validation->set_rules('harga_tindakan', 'Harga Tindakan', 'required');
        $this->form_validation->set_rules('jenis_pasien', 'Jenis Pasien', 'required');
        $this->form_validation->set_rules('type_call', 'Tipe Call', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_tindakan_update";
            $comments = "Gagal update hotel dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $daftartindakan_id = $this->input->post('daftartindakan_id', TRUE);
            $daftartindakan_nama = $this->input->post('daftartindakan_nama', TRUE);
            $harga_tindakan    = $this->input->post('harga_tindakan', TRUE);
            $jenis_pasien    = $this->input->post('jenis_pasien', TRUE);
            $type_call    = $this->input->post('type_call', TRUE);

            $data_hotel = array(
                'daftartindakan_nama' => $daftartindakan_nama,
                'harga_tindakan'         => $harga_tindakan,
                'cyto_tindakan' => $harga_tindakan,
                'jenis_pasien'         => $jenis_pasien,
                'type_call'         => $type_call               
            );

            $update = $this->Laboratorium_model->update_lab($data_hotel, $daftartindakan_id);
            // print_r($data_hotel);
            // echo $id_hotel;
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Laboratorium berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_laboratorium_update";
                $comments = "Berhasil mengubah Laboratorium  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah Tindakan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_laboratorium_update";
                $comments = "Gagal mengubah Lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_lab(){
        $is_permit = $this->aauth->control_no_redirect('master_data_laboratorium_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $daftartindakan_id = $this->input->post('id', TRUE);

        $delete = $this->Laboratorium_model->delete_lab($daftartindakan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Laboratorium berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_laboratorium_delete";
            $comments = "Berhasil menghapus Lab dengan id Lab = '". $daftartindakan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_laboratorium_delete";
            $comments = "Gagal menghapus data Laboratorium dengan ID = '". $daftartindakan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
