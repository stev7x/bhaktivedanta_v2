<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vaksinasi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Vaksinasi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_vaksinasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_vaksinasi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_vaksinasi', $this->data);
    }

    public function ajax_list_vaksinasi(){
        $list = $this->Vaksinasi_model->get_vaksinasi_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $vaksinasi){
            $tarif ="Rp. " .number_format($vaksinasi->tarif).".00";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $vaksinasi->vaksinasi_nama;
            $row[] = $vaksinasi->vaksinasi_merk;
            $row[] = $tarif;
            $row[] = $vaksinasi->type;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_vaksinasi" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit vaksinasi" onclick="getEditVaksinasi('."'".$vaksinasi->vaksinasi_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus agama" id="hapus_vaksinasi" onclick="hapusVaksinasi('."'".$vaksinasi->vaksinasi_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Vaksinasi_model->count_vaksinasi_all(),
                    "recordsFiltered" => $this->Vaksinasi_model->count_vaksinasi_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function convert_to_number($rupiah){
       return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function do_create_vaksinasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_vaksinasi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('vaksinasi_nama', 'vaksinasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_vaksinasi_create";
            $comments = "Gagal input vaksinasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $vaksinasi_nama = $this->input->post('vaksinasi_nama', TRUE);
            $merk = $this->input->post('merk', TRUE);
            $tarif = $this->input->post('tarif', TRUE);
            $type = $this->input->post('type', TRUE);


            $tarif = $this->convert_to_number($tarif);

            $data_vaksinasi = array(
                'vaksinasi_nama' => $vaksinasi_nama,
                'vaksinasi_merk' => $merk,
                'tarif' => $tarif,
                'type' => $type,
            );

            $ins = $this->Vaksinasi_model->insert_vaksinasi($data_vaksinasi);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'vaksinasi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_vaksinasi_create";
                $comments = "Berhasil menambahkan vaksinasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan vaksinasi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_vaksinasi_create";
                $comments = "Gagal menambahkan vaksinasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_vaksinasi_by_id(){
        $vaksinasi_id = $this->input->get('vaksinasi_id',TRUE);
        $old_data = $this->Vaksinasi_model->get_vaksinasi_by_id($vaksinasi_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_vaksinasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_vaksinasi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('vaksinasi_id', 'Nama vaksinasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_vaksinasi_update";
            $comments = "Gagal update vaksinasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $vaksinasi_id = $this->input->post('vaksinasi_id', TRUE);
            $vaksinasi_nama = $this->input->post('vaksinasi_nama', TRUE);
            $merk = $this->input->post('merk', TRUE);
            $tarif = $this->input->post('tarif', TRUE);
            $type = $this->input->post('type', TRUE);


            $tarif = $this->convert_to_number($tarif);

            $data_vaksinasi = array(
                'vaksinasi_nama' => $vaksinasi_nama,
                'vaksinasi_merk' => $merk,
                'tarif' => $tarif,
                'type' => $type,
            );


            $update = $this->Vaksinasi_model->update_vaksinasi($data_vaksinasi, $vaksinasi_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Agama berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_vaksinasi_update";
                $comments = "Berhasil mengubah vaksinasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah vaksinasi , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_vaksinasi_update";
                $comments = "Gagal mengubah vaksinasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_vaksinasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_vaksinasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $vaksinasi_id = $this->input->post('vaksinasi_id', TRUE);

        $delete = $this->Vaksinasi_model->delete_vaksinasi($vaksinasi_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'vaksinasi berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_vaksinasi_delete";
            $comments = "Berhasil menghapus vaksinasi dengan id Agama = '". $vaksinasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_vaksinasi_delete";
            $comments = "Gagal menghapus data vaksinasi dengan ID = '". $vaksinasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("vaksinasi");
         $object->getActiveSheet()
                        ->getStyle("A1:E1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:E1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nama Vaksinasi","Merk Vaksinasi","Tarif","Type");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Vaksinasi_model->get_vaksinasi_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->vaksinasi_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->vaksinasi_merk);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->tarif);
            if($row->type == 1){
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, "Umum");
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, "Dokter");
            }
            $excel_row++;
        }

         foreach(range('A','E') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Vaksinasi.xlsx"');
        $object_writer->save('php://output');
    }
}
