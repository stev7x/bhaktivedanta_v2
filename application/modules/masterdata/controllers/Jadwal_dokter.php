<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_dokter extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        // $this->load->library('PHPExcel/IOFactory');
        $this->load->library('excel');


        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Jadwal_dokter_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kamar_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kamar_view";
        $comments = "Jadwal_dokter";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_jadwaldokter', $this->data);
    }

    public function ajax_list_jadwaldokter(){
        $list = $this->Jadwal_dokter_model->get_jadwaldokter_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $jadwal){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $jadwal->NAME_DOKTER;
            $row[] = $jadwal->kuota;
            $row[] = $jadwal->hari;
            $row[] = $jadwal->jam_mulai;
            $row[] = $jadwal->jam_selesai;
            $row[] = $jadwal->nama_poliruangan;
            $row[] = $jadwal->nama_branch;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle"  title="klik untuk mengedit Jadwal Dokter" onclick="editJadwal('."'".$jadwal->jadwal_dokter_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus Jadwal Dokter" id="hapusJadwal" onclick="hapusJadwal('."'".$jadwal->jadwal_dokter_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Jadwal_dokter_model->count_jadwaldokter_all(),
                    "recordsFiltered" => $this->Jadwal_dokter_model->count_jadwaldokter_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_jadwal(){
        $is_permit = $this->aauth->control_no_redirect('master_data_jadwal_dokter_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('dokter_id', 'Nama Dokter', 'required');
        $this->form_validation->set_rules('detail_jadwal_id', 'Hari', 'required');
        $this->form_validation->set_rules('kuota_pasien', 'Kuota', 'required');
        $this->form_validation->set_rules('jam_mulai', 'Jam Mulai', 'required');
        $this->form_validation->set_rules('id_branch', 'Nama Branch', 'required');
        // $this->form_validation->set_rules('jam_selesai', 'Jam Selesai', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_agama_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
            echo json_encode($res);
            exit;
        }else if ($this->form_validation->run() == TRUE)  {
            $dokter_id          = $this->input->post('dokter_id', TRUE);
            $detail_jadwal_id   = $this->input->post('detail_jadwal_id', TRUE);
            $jam_mulai          = $this->input->post('jam_mulai', TRUE);
            $jam_selesai        = $this->input->post('jam_selesai', TRUE);
            $kuota_pasien       = $this->input->post('kuota_pasien', TRUE);
            $id_branch          = $this->input->post('id_branch', TRUE);

            if(empty($jam_selesai)){
                $jam_selesai  = "Selesai";
            }
            $data_push = array(
                'dokter_id'         => $dokter_id,
                'detail_jadwal_id'  => $detail_jadwal_id,
                'jam_mulai'         => $jam_mulai,
                'jam_selesai'       => $jam_selesai,
                'kuota'             => $kuota_pasien,
                'id_branch'         => $id_branch,
            );

            $ins = $this->Jadwal_dokter_model->insert_jadwaldokter($data_push);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Jadwal Dokter berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_jadwal_dokter_create";
                $comments = "Berhasil menambahkan Jadwal Dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Jadwal Dokter, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_jadwal_dokter_create";
                $comments = "Gagal menambahkan Jadwal Dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_jadwaldokter_by_id(){
        $jadwal_id = $this->input->get('jadwal_id',TRUE);
        $old_data = $this->Jadwal_dokter_model->get_jadwaldokter_by_id($jadwal_id);


        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_jadwal($id){
        $is_permit = $this->aauth->control_no_redirect('master_data_jadwal_dokter_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('dokter_id', 'Nama Dokter', 'required');
        $this->form_validation->set_rules('detail_jadwal_id', 'Hari', 'required');
        $this->form_validation->set_rules('jam_mulai', 'Jam Mulai', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_agama_update";
            $comments = "Gagal update agama dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $dokter_id          = $this->input->post('dokter_id', TRUE);
            $detail_jadwal_id   = $this->input->post('detail_jadwal_id', TRUE);
            $jam_mulai          = $this->input->post('jam_mulai', TRUE);
            $jam_selesai        = $this->input->post('jam_selesai', TRUE);
            $kuota_pasien       = $this->input->post('kuota_pasien', TRUE);

            if(empty($jam_selesai)){
                $jam_selesai  = "Selesai";
            }
            $data_push = array(
                'dokter_id'         => $dokter_id,
                'detail_jadwal_id'  => $detail_jadwal_id,
                'jam_mulai'         => $jam_mulai,
                'jam_selesai'       => $jam_selesai,
                'kuota'             => $kuota_pasien
            );

            // $ins = $this->Jadwal_dokter_model->insert_jadwaldokter($data_push);

            $update = $this->Jadwal_dokter_model->update_jadwaldokter($data_push, $id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Jadwal dokter berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_jadwal_dokter_update";
                $comments = "Berhasil mengubah agama  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah agama , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_jadwal_dokter_update";
                $comments = "Gagal mengubah agama  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_delete_jadwal(){
        $is_permit = $this->aauth->control_no_redirect('master_data_jadwal_dokter_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $jadwal_id = $this->input->post('jadwal_id', TRUE);

        $delete = $this->Jadwal_dokter_model->delete_jadwaldokter($jadwal_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Jadwal Dokter berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_jadwal_dokter_delete";
            $comments = "Berhasil menghapus Jadwal Dokter dengan id jadwal = '". $jadwal_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_agama_delete";
            $comments = "Gagal menghapus data Agama dengan ID = '". $jadwal_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType = PHPExcel_IOFactory::identify($input_file_name);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel= $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {

                $agama_id= $objWorksheet->getCellByColumnAndRow(0,$row)->getValue();
                $agama_nama= $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1

                $data = array(
                    // 'agama_id' => $agama_id,
                    'agama_nama' => $agama_nama
                );

                $this->db->insert("m_agama", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);



        }

    }
    function get_branch_dokter(){
        $list_branch = $this->Jadwal_dokter_model->get_branch();
        if(count($list_branch) > 1){
            $list = "<option value=\"\" disabled selected>PILIH BRANCH</option>";
        }else{
            $list = "<option value=\"\" disabled selected>PILIH BRANCH</option>";
        }
        foreach($list_branch as $list_b){
            $list .= "<option value='".$list_b->id_branch."'>".$list_b->nama."</option>";
        }
        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }
}
