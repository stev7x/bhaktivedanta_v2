<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imunisasi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_keterangan', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('imunisasi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_imunisasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_imunisasi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('v_imunisasi', $this->data);
    }

    public function ajax_list_imunisasi(){
        $list = $this->imunisasi_model->get_imunisasi_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $imunisasi){
            $harga ="Rp. " .number_format($imunisasi->harga).".00";
            $harga_cyto ="Rp. " .number_format($imunisasi->harga_cyto).".00";

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $imunisasi->jenis;
            $row[] = $harga;
            $row[] = $harga_cyto;
            $row[] = $imunisasi->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_imunisasi" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit imunisasi" onclick="getEditImunisasi('."'".$imunisasi->imunisasi_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus agama" id="hapus_imunisasi" onclick="hapusimunisasi('."'".$imunisasi->imunisasi_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->imunisasi_model->count_imunisasi_all(),
                    "recordsFiltered" => $this->imunisasi_model->count_imunisasi_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function convert_to_number($rupiah){
       return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function do_create_imunisasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_imunisasi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('jenis', 'imunisasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_imunisasi_create";
            $comments = "Gagal input imunisasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $jenis      = $this->input->post('jenis', TRUE);
            $harga      = $this->input->post('harga', TRUE);
            $harga_cyto = $this->input->post('harga_cyto', TRUE);
            $keterangan = $this->input->post('keterangan', FALSE);

            $harga      = $this->convert_to_number($harga);
            $harga_cyto = $this->convert_to_number($harga_cyto);


            $data_imunisasi = array(
                'jenis'      => $jenis,
                'harga'      => $harga,
                'harga_cyto' => $harga_cyto,
                'keterangan' => $keterangan,
            );

            $ins = $this->imunisasi_model->insert_imunisasi($data_imunisasi);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'imunisasi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_imunisasi_create";
                $comments = "Berhasil menambahkan imunisasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan imunisasi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_imunisasi_create";
                $comments = "Gagal menambahkan imunisasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_imunisasi_by_id(){
        $imunisasi_id = $this->input->get('imunisasi_id',TRUE);
        $old_data = $this->imunisasi_model->get_imunisasi_by_id($imunisasi_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_imunisasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_imunisasi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('imunisasi_id', 'Nama imunisasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_imunisasi_update";
            $comments = "Gagal update imunisasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $imunisasi_id = $this->input->post('imunisasi_id', TRUE);
            $jenis        = $this->input->post('jenis', TRUE);
            $harga        = $this->input->post('harga', TRUE);
            $harga_cyto   = $this->input->post('harga_cyto', TRUE);
            $keterangan   = $this->input->post('keterangan', FALSE);

            $harga        = $this->convert_to_number($harga);
            $harga_cyto   = $this->convert_to_number($harga_cyto);

            $data_imunisasi = array(
                'jenis'           => $jenis,
                'harga' => $harga,
                'harga_cyto'      => $harga_cyto,
                'keterangan'      => $keterangan,
            );


            $update = $this->imunisasi_model->update_imunisasi($data_imunisasi, $imunisasi_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Agama berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_imunisasi_update";
                $comments = "Berhasil mengubah imunisasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah imunisasi , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_imunisasi_update";
                $comments = "Gagal mengubah imunisasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_imunisasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_imunisasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $imunisasi_id = $this->input->post('imunisasi_id', TRUE);

        $delete = $this->imunisasi_model->delete_imunisasi($imunisasi_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'imunisasi berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_imunisasi_delete";
            $comments = "Berhasil menghapus imunisasi dengan id Agama = '". $imunisasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_imunisasi_delete";
            $comments = "Gagal menghapus data imunisasi dengan ID = '". $imunisasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("imunisasi");
         $object->getActiveSheet()
                        ->getStyle("A1:E1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:E1")
                        ->getFill()
                        ->setFillketerangan(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nama imunisasi","harga imunisasi","harga_cyto","keterangan");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->imunisasi_model->get_imunisasi_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->jenis);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->imunisasi_harga);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->harga_cyto);
            if($row->keterangan == 1){
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, "Umum");
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, "Dokter");
            }
            $excel_row++;
        }

         foreach(range('A','E') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-keterangan: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="imunisasi.xlsx"');
        $object_writer->save('php://output');
    }
}
