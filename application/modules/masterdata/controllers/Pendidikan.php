<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Pendidikan_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_pendidikan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_pendidikan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_pendidikan', $this->data);
    }

    public function ajax_list_pendidikan(){
        $list = $this->Pendidikan_model->get_pendidikan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pendidikan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pendidikan->pendidikan_nama;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_pendidikan"  data-backdrop="static" data-keyboard="false"  title="klik untuk mengedit pendidikan" onclick="editPendidikan('."'".$pendidikan->pendidikan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus pendidikan" id="hapusPendidikan" onclick="hapusPendidikan('."'".$pendidikan->pendidikan_id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pendidikan_model->count_pendidikan_all(),
                    "recordsFiltered" => $this->Pendidikan_model->count_pendidikan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_pendidikan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_pendidikan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_pendidikan', 'Nama Pendidikan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_pendidikan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_pendidikan = $this->input->post('nama_pendidikan', TRUE);
            $data_pendidikan = array(
                'pendidikan_nama' => $nama_pendidikan,
            );

            $ins = $this->Pendidikan_model->insert_pendidikan($data_pendidikan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Pendidikan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_pendidikan_create";
                $comments = "Berhasil menambahkan Pendidikan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Pendidikan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_pendidikan_create";
                $comments = "Gagal menambahkan Pendidikan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_pendidikan_by_id(){
        $pendidikan_id = $this->input->get('pendidikan_id',TRUE);
        $old_data = $this->Pendidikan_model->get_pendidikan_by_id($pendidikan_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_pendidikan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_pendidikan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_pendidikan', 'Nama Pendidikan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_pendidikan_update";
            $comments = "Gagal update pendidikan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $pendidikan_id = $this->input->post('upd_id_pendidikan',TRUE);
            $nama_pendidikan = $this->input->post('upd_nama_pendidikan', TRUE);

            $data_pendidikan = array(
                'pendidikan_nama' => $nama_pendidikan
            );

            $update = $this->Pendidikan_model->update_pendidikan($data_pendidikan, $pendidikan_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Pendidikan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_pendidikan_update";
                $comments = "Berhasil mengubah pendidikan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah pendidikan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_pendidikan_update";
                $comments = "Gagal mengubah pendidikan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_pendidikan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_pendidikan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pendidikan_id = $this->input->post('pendidikan_id', TRUE);

        $delete = $this->Pendidikan_model->delete_pendidikan($pendidikan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Pendidikan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_pendidikan_delete";
            $comments = "Berhasil menghapus Pendidikan dengan id Pendidikan = '". $pendidikan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_pendidikan_delete";
            $comments = "Gagal menghapus data Pendidikan dengan ID = '". $pendidikan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
