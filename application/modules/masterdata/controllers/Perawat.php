<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawat extends CI_Controller {
    public $data = array();
    public $table_perawat = "masterdata_perawat";

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('farmasi/Models');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_farmasi_dosis_view');

        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_farmasi_dosis_view";
        $comments = "Listing Perawat.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('v_perawat', $this->data);
    }

    public function ajax_list(){
        $list = $this->Models->all($this->table_perawat);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $value['no_perawat'];
            $row[] = $value['nama'];
            $row[] = $value['jenis_kelamin'] == 1 ? "Laki-laki" : "Perempuan";
            $row[] = date('d - M - Y', strtotime($value['tanggal_lahir']));
            $row[] = $value['tempat_lahir'];
//            $row[] = $value['email'];
//            $row[] = $value['no_telp'];
            $row[] = $value['driver'] == 1 ? "Ya" : "Tidak";
            //add html for action
            $row[] = '
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit perawat" onclick="edit('."'".$value['id']."'".')"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus perawat" id="delete" onclick="hapus('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_perawat, $id);
        $data['data']['tanggal_lahir'] = date("d/m/Y", strtotime($data['data']['tanggal_lahir']));

        $output = array(
            "success" => true,
            "message" => "Data Detail Perawat ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function create() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('no_perawat', 'No Perawat', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('driver', 'Driver', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal input perawat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $data = array(
                'no_perawat'    => $this->input->post('no_perawat', TRUE),
                'nama'          => $this->input->post('nama', TRUE),
                'jenis_kelamin' => $this->input->post('jenis_kelamin', TRUE),
                'tempat_lahir'  => $this->input->post('tempat_lahir', TRUE),
                'tanggal_lahir' => date('Y-m-d', strtotime($this->input->post('tanggal_lahir', TRUE))),
                'driver'        => $this->input->post('driver', TRUE)
            );

            if ($this->Models->insert($this->table_perawat, $data)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'perawat berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil menambahkan perawat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan perawat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal menambahkan perawat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function update() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('no_perawat', 'No Perawat', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('driver', 'Driver', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal memperbarui perawat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id = $this->input->post('id', TRUE);
            $data = array(
                'no_perawat'    => $this->input->post('no_perawat', TRUE),
                'nama'          => $this->input->post('nama', TRUE),
                'jenis_kelamin' => $this->input->post('jenis_kelamin', TRUE),
                'tempat_lahir'  => $this->input->post('tempat_lahir', TRUE),
                'tanggal_lahir' => date('Y-m-d', strtotime($this->input->post('tanggal_lahir', TRUE))),
                'driver'        => $this->input->post('driver', TRUE)
            );

            if ($this->Models->update($this->table_perawat, $data, $id)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'perawat berhasil diperbarui'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil memperbarui perawat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal memperbarui perawat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal memperbarui perawat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function delete(){
        $id = $this->input->post('id', TRUE);

        if($this->Models->delete($this->table_perawat, $id)){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'perawat berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Berhasil menghapus perawat dengan id = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus perawat, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Gagal menghapus perawat dengan ID = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }

    public function ajax_get_dosis_by_id() {
        $id = $this->input->get('id', TRUE);

        $output = array(
            "success" => true,
            "message" => "Data Detail Obat ",
            "data" => $this->Models->show($this->table_dosis, $id)
        );

        echo json_encode($output);
    }
}