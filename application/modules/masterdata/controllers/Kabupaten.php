<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kabupaten_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kabupaten_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kabupaten_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kabupaten', $this->data);
    }

    public function ajax_list_kabupaten(){
        $list = $this->Kabupaten_model->get_kabupaten_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kabupaten){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kabupaten->propinsi_nama;
            $row[] = $kabupaten->kabupaten_nama;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_update_kabupaten" data-backdrop="static" data-keyboard="false" onclick="editKabupaten('."'".$kabupaten->kabupaten_id."'".')"><i class="fa fa-pencil"></i></button>
                      <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusKabupaten('."'".$kabupaten->kabupaten_id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kabupaten_model->count_kabupaten_all(),
                    "recordsFiltered" => $this->Kabupaten_model->count_kabupaten_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_kabupaten(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kabupaten_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_kabupaten', 'Nama Kabupaten', 'required');
        $this->form_validation->set_rules('select_provinsi', 'Nama Provinsi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kabupaten_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_kabupaten = $this->input->post('nama_kabupaten', TRUE);
            $select_provinsi = $this->input->post('select_provinsi', TRUE);
            $data_kabupaten = array(
                'kabupaten_nama'    => $nama_kabupaten,
                'propinsi_id'       => $select_provinsi,
            );

            $ins = $this->Kabupaten_model->insert_kabupaten($data_kabupaten);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kabupaten berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kabupaten_create";
                $comments = "Berhasil menambahkan Kabupaten dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Kabupaten, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kabupaten_create";
                $comments = "Gagal menambahkan Kabupaten dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_kabupaten_by_id(){
        $kabupaten_id = $this->input->get('kabupaten_id',TRUE);
        $old_data = $this->Kabupaten_model->get_kabupaten_by_id($kabupaten_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_kabupaten(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kabupaten_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_kabupaten', 'Nama Kabupaten', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kabupaten_update";
            $comments = "Gagal update kabupaten dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $kabupaten_id = $this->input->post('upd_id_kabupaten',TRUE);
            $nama_kabupaten = $this->input->post('upd_nama_kabupaten', TRUE);
            $select_provinsi = $this->input->post('upd_select_privinsi', TRUE);

            $data_kabupaten = array(
                'kabupaten_nama'    => $nama_kabupaten,
                'propinsi_id'       => $select_provinsi,
            );

            $update = $this->Kabupaten_model->update_kabupaten($data_kabupaten, $kabupaten_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kabupaten berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kabupaten_update";
                $comments = "Berhasil mengubah kabupaten  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah kabupaten , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kabupaten_update";
                $comments = "Gagal mengubah kabupaten  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_kabupaten(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kabupaten_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $kabupaten_id = $this->input->post('kabupaten_id', TRUE);

        $delete = $this->Kabupaten_model->delete_kabupaten($kabupaten_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Kabupaten berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kabupaten_delete";
            $comments = "Berhasil menghapus Kabupaten dengan id Kabupaten = '". $kabupaten_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kabupaten_delete";
            $comments = "Gagal menghapus data Kabupaten dengan ID = '". $kabupaten_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
