<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dokter_ruangan extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library('excel');

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Dokter_ruangan_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['data_ruangan'] = array('1'=>'dokterklinik','2'=>'Ruang Rawat Inap','3'=>'Ruang IGD');
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_dokter_ruangan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_dokter_ruangan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_dokter_ruangan', $this->data);
    }

    public function ajax_list_dokter_ruangan(){
        $list = $this->Dokter_ruangan_model->get_dokter_ruangan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $dokter_ruangan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $dokter_ruangan->NAME_DOKTER;
            // $row[] = $dokter_ruangan->ALAMAT;
            $row[] = $dokter_ruangan->nama_poliruangan;
            //add html for action
            // <button class="btn-floating waves-effect waves-light teal" onclick="editdokter_ruangan('."'".$dokter_ruangan->id_M_DOKTER."'".')"><i class="mdi-content-create"></i></button>
            $row[] = '<a class="btn btn-warning btn-circle" data-position="bottom" data-toggle="modela" data-target="#modal_update_dokter_ruangan" data-backdrop="static" data-keyboard="false" data-delay="50" title="klik untuk memperbarui" onclick="editdokter_ruangan('."'".$dokter_ruangan->id_M_DOKTER."',"."'".$dokter_ruangan->poliruangan_id."'".')"><i class="fa fa-pencil"></i></a>
            <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusdokter_ruangan('."'".$dokter_ruangan->id_M_DOKTER."',"."'".$dokter_ruangan->poliruangan_id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Dokter_ruangan_model->count_dokter_ruangan_all(),
                    "recordsFiltered" => $this->Dokter_ruangan_model->count_dokter_ruangan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_dokter_ruangan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_dokter_ruangan_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('select_dokter', 'Nama dokter', 'required');
        $this->form_validation->set_rules('select_ruangan', 'Nama Ruangan', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_dokter_ruangan_view";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $select_dokter = $this->input->post('select_dokter', TRUE);
            $select_ruangan = $this->input->post('select_ruangan', TRUE);
            $data_dokter_ruangan = array(
                'dokter_id'       => $select_dokter,
                'poliruangan_id'       => $select_ruangan,
            );
            $query = $this->db->get_where('m_dokterruangan',array('dokter_id'=>$select_dokter,'poliruangan_id'=>$select_ruangan));
            if ($query->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Dokter ini sudah terdaftar diruangan yang sama');

                    // if permitted, do logit
                    $perms = "master_data_dokter_ruangan_view";
                    $comments = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $ins = $this->Dokter_ruangan_model->insert_dokter_ruangan($data_dokter_ruangan);
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'dokter Ruangan berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "master_data_dokter_ruangan_view";
                    $comments = "Berhasil menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan dokter Ruangan, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "master_data_dokter_ruangan_view";
                    $comments = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_dokter_ruangan_by_id(){
        $dokterruangan_id = $this->input->get('dokterruangan_id',TRUE);
        $ruangan_id = $this->input->get('ruangan_id',TRUE);
        $old_data = $this->Dokter_ruangan_model->get_dokter_ruangan_by_id($dokterruangan_id, $ruangan_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_dokter_ruangan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_dokter_ruangan_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        // $this->form_validation->set_rules('upd_select_dokter', 'Nama Dokter', 'required');
        $this->form_validation->set_rules('upd_select_ruangan', 'Nama Ruangan', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_dokter_ruangan_view";
            $comments = "Gagal update dokter_ruangan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $dokterruangan_id = $this->input->post('upd_select_dokter', TRUE);
            $ruangan_id = $this->input->post('upd_select_ruangan', TRUE);
            $temp_dokterruangan_id = $this->input->post('temp_dokterruangan_id', TRUE);
            $temp_ruangan_id = $this->input->post('temp_ruangan_id', TRUE);

            // $select_dokter = $this->input->post('upd_select_dokter', TRUE);
            // $select_ruangan = $this->input->post('upd_select_ruangan', TRUE);


            $data_dokter_ruangan = array(
                'dokter_id'             => $temp_dokterruangan_id,
                'poliruangan_id'        => $ruangan_id,
            );
            $query = $this->db->get_where('m_dokterruangan',array('dokter_id'=>$temp_dokterruangan_id,'poliruangan_id'=>$ruangan_id));


            // $qu = $query->num_rows();
            // print_r($qu);die();


            if ($query->num_rows() > 0) {

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Dokter ini sudah terdaftar diruangan yang sama');

                    // if permitted, do logit
                    $perms = "master_data_dokter_ruangan_view";
                    $comments = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{
            $update = $this->Dokter_ruangan_model->update_dokter_ruangan($data_dokter_ruangan, $temp_dokterruangan_id, $temp_ruangan_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'dokter Ruangan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_dokter_ruangan_view";
                $comments = "Berhasil mengubah dokter_ruangan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah dokter_ruangan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_dokter_ruangan_view";
                $comments = "Gagal mengubah dokter_ruangan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        }
        echo json_encode($res);
    }

    public function do_delete_dokter_ruangan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_dokter_ruangan_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $dokterruangan_id = $this->input->post('dokterruangan_id', TRUE);
        $poliruangan_id = $this->input->post('poliruangan_id', TRUE);

        $delete = $this->Dokter_ruangan_model->delete_dokter_ruangan($dokterruangan_id,$poliruangan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'dokter Ruangan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_dokter_ruangan_view";
            $comments = "Berhasil menghapus dokter Ruangan dengan id dokter Ruangan = '". $dokterruangan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_dokter_ruangan_view";
            $comments = "Gagal menghapus data dokter Ruangan dengan ID = '". $dokterruangan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Dokter Ruangan");
        $object->getActiveSheet()
                        ->getStyle("A1:C1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:C1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No","Nama Dokter","Poli Ruangan");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Dokter_ruangan_model->get_dokter_ruangan_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            // $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->id_M_DOKTER);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->NAME_DOKTER);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama_poliruangan);

            $excel_row++;
        }
        foreach(range('A','C') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);


        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Dokter Ruangan.xlsx"');
        $object_writer->save('php://output');
    }


    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType = PHPExcel_IOFactory::identify($input_file_name);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel= $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {

                $dokter_id          = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1
                $poliruangan_id     = $objWorksheet->getCellByColumnAndRow(2,$row)->getValue(); //Excel Column 2


                $data = array(
                    // 'agama_id' => $agama_id,
                    'dokter_id'         => $dokter_id,
                    'poliruangan_id'    => $poliruangan_id
                );

                $this->db->insert("m_dokterruangan", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);



        }

    }
}
