<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fee_dokter_by_hotel extends CI_Controller {
    public $data = array();
    public $table_fee_dokter = "fee_dokter_by_hotel";
    public $table_hotel = "m_hotel";

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('farmasi/Models');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_farmasi_fee_dokter_view');

        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
//            redirect('no_permission');
        }

        $this->data['hotel'] = $this->Models->allCustom($this->table_hotel, 'id_hotel');

        // if permitted, do logit
        $perms = "master_data_farmasi_fee_dokter_view";
        $comments = "Listing Fee Dokter.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('fee_dokter_by_hotel', $this->data);
    }

    public function ajax_list(){
        $list = $this->Models->where($this->table_fee_dokter, array('branch' => $this->session->tempdata('data_session')[2]));
        $hotel = $this->Models->allCustom($this->table_hotel, 'id_hotel');
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $key => $value) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = @$hotel[$value['id_hotel']]['nama_hotel'];
            $row[] = $value['vs_local'];
            $row[] = $value['vs_domestic'];
            $row[] = $value['vs_guest'];
            $row[] = $value['oc_local'];
            $row[] = $value['oc_domestic'];
            $row[] = $value['oc_guest'];
            //add html for action
            $row[] = '
                <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit obat" onclick="edit('."'".$value['id']."'".')"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus obat" id="delete" onclick="hapus('."'".$value['id']."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail() {
        $id = $this->input->get('id', TRUE);
        $data['data'] = $this->Models->show($this->table_fee_dokter, $id);

        $output = array(
            "success" => true,
            "message" => "Data Detail Fee Dokter ",
            "data" => $data
        );

        echo json_encode($output);
    }

    public function create() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('hotel_id', 'Hotel', 'required');
        $this->form_validation->set_rules('vs_local', 'Visit Lokal', 'required');
        $this->form_validation->set_rules('vs_dome', 'Visit Domestik', 'required');
        $this->form_validation->set_rules('vs_asing', 'Visit Asing', 'required');
        $this->form_validation->set_rules('oc_local', 'Oncall Lokal', 'required');
        $this->form_validation->set_rules('oc_dome', 'Oncall Domestik', 'required');
        $this->form_validation->set_rules('oc_asing', 'Oncall Asing', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal input fee_dokter dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $data = array(
                'id_hotel' => $this->input->post('hotel_id', TRUE),
                'vs_local' => $this->input->post('vs_local', TRUE),
                'vs_domestic' => $this->input->post('vs_dome', TRUE),
                'vs_guest' => $this->input->post('vs_asing', TRUE),
                'oc_local' => $this->input->post('oc_local', TRUE),
                'oc_domestic' => $this->input->post('oc_dome', TRUE),
                'oc_guest' => $this->input->post('oc_asing', TRUE),
                'branch' => $this->session->tempdata('data_session')[2]
            );

            if ($this->Models->insert($this->table_fee_dokter, $data)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'fee_dokter berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil menambahkan fee_dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menambahkan fee_dokter, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal menambahkan fee_dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function update() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id', 'ID', 'required');
        $this->form_validation->set_rules('vs_local', 'Visit Lokal', 'required');
        $this->form_validation->set_rules('vs_dome', 'Visit Domestik', 'required');
        $this->form_validation->set_rules('vs_asing', 'Visit Asing', 'required');
        $this->form_validation->set_rules('oc_local', 'Oncall Lokal', 'required');
        $this->form_validation->set_rules('oc_dome', 'Oncall Domestik', 'required');
        $this->form_validation->set_rules('oc_asing', 'Oncall Asing', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "";
            $comments = "Gagal memperbarui fee_dokter dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else {
            $id = $this->input->post('id', TRUE);
            $data = array(
                'vs_local' => $this->input->post('vs_local', TRUE),
                'vs_domestic' => $this->input->post('vs_dome', TRUE),
                'vs_guest' => $this->input->post('vs_asing', TRUE),
                'oc_local' => $this->input->post('oc_local', TRUE),
                'oc_domestic' => $this->input->post('oc_dome', TRUE),
                'oc_guest' => $this->input->post('oc_asing', TRUE)
            );

            if ($this->Models->update($this->table_fee_dokter, $data, $id)) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'fee_dokter berhasil diperbarui'
                );

                // if permitted, do logit
                $perms = "";
                $comments = "Berhasil memperbarui fee_dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            } else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal memperbarui fee_dokter, hubungi web administrator.');

                // if permitted, do logit
                $perms = "";
                $comments = "Gagal memperbarui fee_dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function delete(){
        $id = $this->input->post('id', TRUE);

        if($this->Models->delete($this->table_fee_dokter, $id)){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'fee_dokter berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Berhasil menghapus fee_dokter dengan id = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus fee_dokter, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "";
            $comments = "Gagal menghapus fee_dokter dengan ID = '". $id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }

    public function ajax_get_fee_dokter_by_id() {
        $id = $this->input->get('id', TRUE);

        $output = array(
            "success" => true,
            "message" => "Data Detail Obat ",
            "data" => $this->Models->show($this->table_fee_dokter, $id)
        );

        echo json_encode($output);
    }
}