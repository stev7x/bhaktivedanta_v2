<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_operasi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library("Excel");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Paket_operasi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_paket_operasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_paket_operasi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_paket_operasi', $this->data);
    }

    public function ajax_list_paket_operasi(){
        $list = $this->Paket_operasi_model->get_paket_operasi_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $paket){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $paket->nama_paket;
            $row[] = $this->convert_to_rupiah($paket->total_harga).",00";
            $row[] = $paket->kelaspelayanan_nama;
            $row[] = $paket->jml_hari." Hari";
            $row[] = $paket->is_bpjs == '0' ? 'Non BPJS' : 'BPJS';
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_paket_operasi" data-backdrop="static" data-keyboard="false" id="editPaketOperasi" onclick="editPaketOperasi('."'".$paket->paketoperasi_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle " data-position="bottom" data-delay="50" title="klik untuk menghapus paket operasi" id="hapusPaketOperasi" onclick="hapusPaketOperasi('."'".$paket->paketoperasi_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Paket_operasi_model->count_paket_operasi_all(),
                    "recordsFiltered" => $this->Paket_operasi_model->count_paket_operasi_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function convert_to_rupiah($angka)
    {
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }

    function convert_to_number($rupiah)
    {
       return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function do_create_paket_operasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_paket_operasi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_paket', 'Nama Paket Operasi', 'required');
        $this->form_validation->set_rules('kelaspelayanan', 'Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('jml_hari', 'Jumlah Hari', 'required|numeric');
        $this->form_validation->set_rules('is_bpjs', 'Metode Pembayaran', 'required');
        $this->form_validation->set_rules('total_harga', 'Total Harga', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_paket_operasi_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_paket = $this->input->post('nama_paket', TRUE);
            $kelaspelayanan = $this->input->post('kelaspelayanan',TRUE);
            $jml_hari = $this->input->post('jml_hari',TRUE);
            $is_bpjs = $this->input->post('is_bpjs',TRUE);
            $total_harga = $this->input->post('total_harga',TRUE);

            $total_harga_int = $this->convert_to_number($total_harga);
            $data_paket = array(
                'nama_paket' => $nama_paket,
                'kelaspelayanan_id' => $kelaspelayanan,
                'jml_hari' => $jml_hari,
                'is_bpjs' => $is_bpjs,
                'total_harga' => $total_harga_int
            );

            $ins = $this->Paket_operasi_model->insert_paket($data_paket);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Paket Operasi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_paket_operasi_create";
                $comments = "Berhasil menambahkan Paket Operasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Paket Operasi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_paket_operasi_create";
                $comments = "Gagal menambahkan Paket Operasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_paket_by_id(){
        $paket_id = $this->input->get('paket_id',TRUE);
        $old_data = $this->Paket_operasi_model->get_paket_by_id($paket_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_paket_operasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_paket_operasi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_paket', 'Nama Paket Operasi', 'required');
        $this->form_validation->set_rules('upd_kelaspelayanan', 'Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('upd_jml_hari', 'Jumlah Hari', 'required|numeric');
        $this->form_validation->set_rules('upd_bpjs', 'Metode Pembayaran', 'required');
        $this->form_validation->set_rules('upd_total_harga', 'Total Harga', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_paket_operasi_update";
            $comments = "Gagal update paket operasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $paket_id       = $this->input->post('upd_id_paket',TRUE);
            $nama_paket     = $this->input->post('upd_nama_paket', TRUE);
            $jml_hari       = $this->input->post('upd_jml_hari', TRUE);
            $kelaspelayanan = $this->input->post('upd_kelaspelayanan', TRUE);
            $is_bpjs        = $this->input->post('upd_bpjs', TRUE);
            $total_harga    = $this->input->post('upd_total_harga', TRUE);

            $total_harga_int = $this->convert_to_number($total_harga);
            $data_paket_operasi = array(
                'nama_paket'         => $nama_paket,
                'jml_hari'           => $jml_hari,
                'kelaspelayanan_id' => $kelaspelayanan,
                'is_bpjs'            => $is_bpjs,
                'total_harga'            => $total_harga_int
            );

            $update = $this->Paket_operasi_model->update_paket($data_paket_operasi, $paket_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Paket Operasi berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_paket_operasi_update";
                $comments = "Berhasil mengubah paket operasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah paket operasi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_paket_operasi_update";
                $comments = "Gagal mengubah paket operasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_paket(){
        $is_permit = $this->aauth->control_no_redirect('master_data_paket_operasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $paket_id = $this->input->post('paket_id', TRUE);

        $delete = $this->Paket_operasi_model->delete_paket($paket_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Paket berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_paket_operasi_delete";
            $comments = "Berhasil menghapus Paket dengan id Paket Operasi = '". $paket_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_paket_operasi_delete";
            $comments = "Gagal menghapus data Paket dengan ID = '". $paket_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_list_tindakan_paket(){
        $paketoperasi_id = $this->input->get('paketoperasi_id',TRUE);
        $list = $this->Paket_operasi_model->get_tindakan_paket($paketoperasi_id);
        $data = array();
        foreach($list as $tindakan){
            $row = array();
            $row[] = $tindakan->hari_ke;
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = number_format($tindakan->tarif_tindakan);
            $row[] = $tindakan->jumlah;
            $row[] = number_format($tindakan->subsidi);
            $row[] = number_format($tindakan->total_bayar);
            $row[] = $tindakan->jenistindakan == '1' ? 'Umum' : ($tindakan->jenistindakan == '2' ? 'Radiologi' : 'Laboratorium');
            $row[] = '<button type="button" class="btn-floating yellow waves-effect waves-light darken-4" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->detailpaket_id."','".$tindakan->paketoperasi_id."'".')"><i class="mdi-content-clear"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function get_tindakan_list(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $is_bpjs = $this->input->get('type_pembayaran',TRUE);
        $list_tindakan = $this->Paket_operasi_model->get_tindakan($kelaspelayanan_id, $is_bpjs);
        $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->tariftindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    public function get_hari_list(){
        $jmlhari = $this->input->get('jmlhari',TRUE);
        $list = "<option value=\"\" disabled selected>Pilih Hari</option>";
        if($jmlhari > 0){
            $i = 1;
            while($i <= $jmlhari){
                $list .= "<option value='".$i."'>".$i."</option>";
                $i++;
            }
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_tarif_tindakan(){
        $tariftindakan_id = $this->input->get('tariftindakan_id',TRUE);
        $tarif_tindakan = $this->Paket_operasi_model->get_tarif_tindakan($tariftindakan_id);

        $res = array(
            "list" => $tarif_tindakan,
            "success" => true
        );

        echo json_encode($res);
    }

    function do_create_tindakan_paket(){
        $is_permit = $this->aauth->control_no_redirect('master_data_paket_operasi_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('harike','Hari', 'required');
        $this->form_validation->set_rules('tindakan','Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','JUmlah', 'required');
        $this->form_validation->set_rules('subsidi','Subsidi', 'required|trim');
        $this->form_validation->set_rules('jenistindakan','Jenis Tindakan', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_paket_operasi_view";
            $comments = "Gagal input tindakan paket dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if($this->form_validation->run() == TRUE){
            $paketoperasi_id = $this->input->post('paketoperasi_id', TRUE);
            $harike = $this->input->post('harike',TRUE);
            $tariftindakan_id = $this->input->post('tindakan',TRUE);
            $tarif = $this->input->post('harga_tindakan',TRUE);
            $subsidi = $this->input->post('subsidi',TRUE);
            $totalbayar = $this->input->post('totalharga',TRUE);
            $jenistindakan = $this->input->post('jenistindakan',TRUE);
            $jumlah = $this->input->post('jml_tindakan',TRUE);

            $data_tindakan = array(
                'paketoperasi_id' => $paketoperasi_id,
                'tariftindakan_id' => $tariftindakan_id,
                'tarif_tindakan' => $tarif,
                'subsidi' => $subsidi,
                'total_bayar' => $totalbayar,
                'jenistindakan' => $jenistindakan,
                'hari_ke' => $harike,
                'jumlah' => $jumlah
            );
            $ins = $this->Paket_operasi_model->insert_tindakan_paket($data_tindakan);

            if($ins){
                $count_total = $this->Paket_operasi_model->get_total_harga($paketoperasi_id);
                $dataupdate = array(
                    'totalharga' => $count_total
                );
                $this->Paket_operasi_model->update_paket($dataupdate, $paketoperasi_id);

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan Paket Operasi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_paket_operasi_view";
                $comments = "Berhasil menambahkan Tindakan Paket Operasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan Paket Operasi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_paket_operasi_view";
                $comments = "Gagal menambahkan Tindakan Paket Operasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    function do_delete_tindakan_paket(){
        $is_permit = $this->aauth->control_no_redirect('master_data_paket_operasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $detailpaket_id = $this->input->post('detailpaket_id', TRUE);
        $paketoperasi_id = $this->input->post('paketoperasi_id', TRUE);

        $delete = $this->Paket_operasi_model->hapus_paket_tindakan($detailpaket_id, $paketoperasi_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Paket berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_paket_operasi_delete";
            $comments = "Berhasil menghapus Tindakan Paket dengan id Paket Operasi = '". $paketoperasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_paket_operasi_delete";
            $comments = "Gagal menghapus data tindakan Paket dengan ID = '". $paketoperasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_list_obat_paket(){
        $paketoperasi_id = $this->input->get('paketoperasi_id',TRUE);
        $list = $this->Paket_operasi_model->get_obat_paket($paketoperasi_id);
        $data = array();
        foreach($list as $obat){
            $row = array();
            $row[] = $obat->hari_ke;
            $row[] = $obat->jenisoa_nama;
            $row[] = $obat->nama_obat;
            $row[] = number_format($obat->harga_satuan);
            $row[] = number_format($obat->subsidi_obat);
            $row[] = $obat->jumlah_obat;
            $row[] = number_format($obat->total_harga);
            $row[] = '<button type="button" class="btn-floating yellow waves-effect waves-light darken-4" title="Klik untuk menghapus obat" onclick="hapusObat('."'".$obat->paketoperasiobat_id."','".$obat->paketoperasi_id."'".')"><i class="mdi-content-clear"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function get_obat_list(){
        $jenisoa_id = $this->input->get('jenisoa_id',TRUE);
        $list_obat = $this->Paket_operasi_model->get_obat($jenisoa_id);
        $list = "<option value=\"\" disabled selected>Pilih Obat</option>";
        foreach($list_obat as $list_obat){
            $list .= "<option value='".$list_obat->obat_id."'>".$list_obat->nama_obat."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_tarif_obat(){
        $obat_id = $this->input->get('obat_id',TRUE);
        $harga_obat = $this->Paket_operasi_model->get_tarif_obat($obat_id);

        $res = array(
            "list" => $harga_obat,
            "success" => true
        );

        echo json_encode($res);
    }

    function do_create_obat_paket(){
        $is_permit = $this->aauth->control_no_redirect('master_data_paket_operasi_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('harike_obat','Hari', 'required');
        $this->form_validation->set_rules('jenisobat','Jenis Obat', 'required');
        $this->form_validation->set_rules('obat_id','Nama Obat', 'required');
        $this->form_validation->set_rules('subsidi_obat','Subsidi', 'required|numeric');
        $this->form_validation->set_rules('jml_obat','Jumlah Obat', 'required|numeric');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_paket_operasi_view";
            $comments = "Gagal input paket obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if($this->form_validation->run() == TRUE){
            $paketoperasi_id = $this->input->post('paketoperasi_id', TRUE);
            $harike = $this->input->post('harike_obat',TRUE);
            $obat_id = $this->input->post('obat_id',TRUE);
            $harga_satuan = $this->input->post('harga_satuan',TRUE);
            $subsidi = $this->input->post('subsidi_obat',TRUE);
            $totalharga = $this->input->post('totalhargaobat',TRUE);
            $jumlah = $this->input->post('jml_obat',TRUE);

            $data_obat = array(
                'paketoperasi_id' => $paketoperasi_id,
                'obat_id' => $obat_id,
                'harga_satuan' => $harga_satuan,
                'subsidi_obat' => $subsidi,
                'jumlah_obat' => $jumlah,
                'total_harga' => $totalharga,
                'hari_ke' => $harike
            );
            $ins = $this->Paket_operasi_model->insert_obat_paket($data_obat);

            if($ins){
                $count_total = $this->Paket_operasi_model->get_total_harga($paketoperasi_id);
                $dataupdate = array(
                    'totalharga' => $count_total
                );
                $this->Paket_operasi_model->update_paket($dataupdate, $paketoperasi_id);

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Obat Paket Operasi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_paket_operasi_view";
                $comments = "Berhasil menambahkan Obat Paket Operasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Obat Paket Operasi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_paket_operasi_view";
                $comments = "Gagal menambahkan Obat Paket Operasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    function do_delete_obat_paket(){
        $is_permit = $this->aauth->control_no_redirect('master_data_paket_operasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $paketoperasiobat_id = $this->input->post('paketoperasiobat_id', TRUE);
        $paketoperasi_id = $this->input->post('paketoperasi_id', TRUE);

        $delete = $this->Paket_operasi_model->hapus_paket_obat($paketoperasiobat_id, $paketoperasi_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Paket Obat berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_paket_operasi_delete";
            $comments = "Berhasil menghapus Obat Paket dengan id Paket Operasi = '". $paketoperasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_paket_operasi_delete";
            $comments = "Gagal menghapus data obat Paket dengan ID = '". $paketoperasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Paket Operasi");
        $object->getActiveSheet()
                        ->getStyle("A1:F1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:F1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nama Paket","Total Harga","Kelas Pelayanan","Jumlah Hari Rawat","Type Pembayaran");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Paket_operasi_model->get_paket_operasi_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nama_paket);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->total_harga);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->kelaspelayanan_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->jml_hari);
            if($row->is_bpjs == 0 ){
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, "NON BPJS");
            }else{
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, "BPJS");

            }$excel_row++;
        }

         foreach(range('A','F') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Paket Operasi.xlsx"');
        $object_writer->save('php://output');
    }


     public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType  = PHPExcel_IOFactory::identify($input_file_name);
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel    = $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows      = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet   = $objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {
                // $unit_kerja_id      = $objWorksheet->getCellByColumnAndRow(0,$row)->getValue(); /
                $nama_paket          = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1
                $total_harga         = $objWorksheet->getCellByColumnAndRow(2,$row)->getValue(); //Excel Column 2
                $kelaspelayanan_id   = $objWorksheet->getCellByColumnAndRow(3,$row)->getValue(); //Excel Column 3
                $jml_hari            = $objWorksheet->getCellByColumnAndRow(4,$row)->getValue(); //Excel Column 4
                $is_bpjs             = $objWorksheet->getCellByColumnAndRow(5,$row)->getValue(); //Excel Column 5

                $data = array(
                    'nama_paket'        => $nama_paket,
                    'total_harga'       => $total_harga,
                    'kelaspelayanan_id' => $kelaspelayanan_id,
                    'jml_hari'          => $jml_hari,
                    'is_bpjs'           => $is_bpjs
                );

                $this->db->insert("m_paketoperasi", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);
        }
    }
}
