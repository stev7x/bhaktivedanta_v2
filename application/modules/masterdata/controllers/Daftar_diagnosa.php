<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_diagnosa extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library("excel");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Daftar_diagnosa_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_daftar_diagnosa_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_daftar_diagnosa_view";
        $comments = "Listing User";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_daftar_diagnosa', $this->data);
    }

    public function ajax_list_daftar_diagnosa(){
        $list = $this->Daftar_diagnosa_model->get_daftar_diagnosa_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $daftar_diagnosa){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $daftar_diagnosa->kode_diagnosa;
            $row[] = $daftar_diagnosa->nama_diagnosa;
            // $row[] = $daftar_diagnosa->kelompokdiagnosa_nama;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_daftar_diagnosa" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit diagnosa" id="editDaftarDiagnosa" onclick="editDaftarDiagnosa('."'".$daftar_diagnosa->diagnosa_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-backdrop="static" data-keyboard="false" data-delay="50" title="klik untuk menghapus diagnosa" id="hapusDaftarDiagnosa"onclick="hapusDaftarDiagnosa('."'".$daftar_diagnosa->diagnosa_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Daftar_diagnosa_model->count_daftar_diagnosa_all(),
                    "recordsFiltered" => $this->Daftar_diagnosa_model->count_daftar_diagnosa_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_daftar_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('master_data_daftar_diagnosa_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_diagnosa', 'Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('nama_diagnosa', 'Nama Diagnosa', 'required|trim');
        // $this->form_validation->set_rules('kelompokdiagnosa', 'Kelompok Diagnosa', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_daftar_tindakan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_diagnosa = $this->input->post('nama_diagnosa', TRUE);
            $kode_diagnosa = $this->input->post('kode_diagnosa', TRUE);
            // $kelompokdiagnosa = $this->input->post('kelompokdiagnosa', TRUE);
            $data_daftar_diagnosa = array(
                'nama_diagnosa' => $nama_diagnosa,
                'kode_diagnosa' => $kode_diagnosa,
                // 'kelompokdiagnosa_id' => $kelompokdiagnosa
            );

            $ins = $this->Daftar_diagnosa_model->insert_daftar_diagnosa($data_daftar_diagnosa);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Daftar diagnosa berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_daftar_diagnosa_create";
                $comments = "Berhasil menambahkan Daftar diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Daftar diagnosa, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_daftar_diagnosa_create";
                $comments = "Gagal menambahkan Daftar diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_daftar_diagnosa_by_id(){
        $diagnosa_id = $this->input->get('diagnosa_id',TRUE);
        $old_data = $this->Daftar_diagnosa_model->get_daftar_diagnosa_by_id($diagnosa_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_daftar_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('master_data_daftar_diagnosa_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kode_diagnosa', 'Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('upd_nama_diagnosa', 'Nama Diagnosa', 'required|trim');
        // $this->form_validation->set_rules('upd_kelompokdiagnosa', 'Kelompok Diagnosa', 'required|trim');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_daftar_diagnosa_update";
            $comments = "Gagal update daftar diagnosa dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $diagnosa_id = $this->input->post('diagnosa_id',TRUE);
            $kode_diagnosa = $this->input->post('upd_kode_diagnosa', TRUE);
            $nama_diagnosa = $this->input->post('upd_nama_diagnosa', TRUE);
            // $kelompokdiagnosa = $this->input->post('upd_kelompokdiagnosa', TRUE);

            $data_daftar_diagnosa = array(
                'kode_diagnosa' => $kode_diagnosa,
                'nama_diagnosa' => $nama_diagnosa,
                // 'kelompokdiagnosa_id' => $kelompokdiagnosa
            );

            $update = $this->Daftar_diagnosa_model->update_daftar_diagnosa($data_daftar_diagnosa, $diagnosa_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Daftar tindakan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_daftar_diagnosa_update";
                $comments = "Berhasil mengubah daftar diagnosa  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah daftar diagnosa , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_daftar_diagnosa_update";
                $comments = "Gagal mengubah daftar diagnosa  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_daftar_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('master_data_daftar_diagnosa_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosa_id = $this->input->post('diagnosa_id', TRUE);

        $delete = $this->Daftar_diagnosa_model->delete_daftar_diagnosa($diagnosa_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Daftar diagnosa berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_daftar_diagnosa_delete";
            $comments = "Berhasil menghapus Daftar Diagnosa dengan id daftar diagnosa = '". $diagnosa_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_daftar_diagnosa_delete";
            $comments = "Gagal menghapus data Daftar tindakan dengan ID = '". $diagnosa_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Daftar Diagnosa");
        $object->getActiveSheet()
                        ->getStyle("A1:D1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:D1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No","Kode Diagnosa","Nama Diagnosa","Kelompok Diagnosa");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Daftar_diagnosa_model->get_daftar_diagnosa_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->kode_diagnosa);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama_diagnosa);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->kelompokdiagnosa_nama);

            $excel_row++;
        }


        foreach(range('A','D') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }


        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Daftar Diagnosa.xlsx"');
        $object_writer->save('php://output');
    }

    public function upload(){
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType = PHPExcel_IOFactory::identify($input_file_name);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel= $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {

                $kode_diagnosa        = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1
                $nama_diagnosa        = $objWorksheet->getCellByColumnAndRow(2,$row)->getValue(); //Excel Column 2

                $data = array(
                    // 'agama_id' => $agama_id,
                    'kode_diagnosa'   => $kode_diagnosa,
                    'nama_diagnosa'   => $nama_diagnosa
                );

                $this->db->insert("m_diagnosa_icd10", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);



        }

    }
}
