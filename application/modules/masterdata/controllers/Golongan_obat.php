<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Golongan_obat extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Golongan_obat_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_golongan_obat_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_golongan_obat_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_golongan_obat', $this->data);
    }

    public function ajax_list_golongan_obat(){
        $list = $this->Golongan_obat_model->get_golongan_obat_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $golongan_obat){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $golongan_obat->kode_golongan;
            $row[] = $golongan_obat->nama_golongan;
            $row[] = $golongan_obat->nama_kelas_terapi;
            $row[] = $golongan_obat->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_golongan_obat" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit golongan_obat" onclick="editGolonganObat('."'".$golongan_obat->id_golongan."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus golongan_obat" id="hapusGolonganObat" onclick="hapusGolonganObat('."'".$golongan_obat->id_golongan."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Golongan_obat_model->count_golongan_obat_all(),
                    "recordsFiltered" => $this->Golongan_obat_model->count_golongan_obat_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_golongan_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_golongan_obat_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_golongan', 'Kode golongan obat', 'required');
        $this->form_validation->set_rules('nama_golongan', 'Nama golongan obat', 'required');
        $this->form_validation->set_rules('id_kelas_terapi', 'Kelas Terapi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_golongan_obat_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $kode_golongan = $this->input->post('kode_golongan', TRUE);
            $nama_golongan = $this->input->post('nama_golongan', TRUE);
            $id_kelas_terapi    = $this->input->post('id_kelas_terapi', TRUE);
            $keterangan    = $this->input->post('keterangan', TRUE);
            $data_golongan_obat = array(
                'kode_golongan' => $kode_golongan,
                'nama_golongan' => $nama_golongan,
                'id_kelas_terapi'    => $id_kelas_terapi,
                'keterangan'         => $keterangan,
            );

            $ins = $this->Golongan_obat_model->insert_golongan_obat($data_golongan_obat);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'golongan_obat berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_golongan_obat_create";
                $comments = "Berhasil menambahkan golongan_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan golongan_obat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_golongan_obat_create";
                $comments = "Gagal menambahkan golongan_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_golongan_obat_by_id(){
        $id_golongan = $this->input->get('id_golongan',TRUE);
        $old_data = $this->Golongan_obat_model->get_golongan_obat_by_id($id_golongan);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_golongan_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_golongan_obat_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kode_golongan', 'Kode golongan', 'required');
        $this->form_validation->set_rules('upd_nama_golongan', 'Nama golongan', 'required');
        $this->form_validation->set_rules('upd_id_kelas_terapi', 'Kelas terapi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_golongan_obat_update";
            $comments = "Gagal update golongan_obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_golongan = $this->input->post('upd_id_golongan',TRUE);
            $kode_golongan = $this->input->post('upd_kode_golongan', TRUE);
            $nama_golongan = $this->input->post('upd_nama_golongan', TRUE);
            $id_kelas_terapi = $this->input->post('upd_id_kelas_terapi', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_golongan_obat = array(
                'kode_golongan' => $kode_golongan,
                'nama_golongan' => $nama_golongan,
                'id_kelas_terapi' => $id_kelas_terapi,
                'keterangan' => $keterangan
            );

            $update = $this->Golongan_obat_model->update_golongan_obat($data_golongan_obat, $id_golongan);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'golongan_obat berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_golongan_obat_update";
                $comments = "Berhasil mengubah golongan_obat  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah golongan_obat , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_golongan_obat_update";
                $comments = "Gagal mengubah golongan_obat  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_golongan_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_golongan_obat_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_golongan = $this->input->post('id_golongan', TRUE);

        $delete = $this->Golongan_obat_model->delete_golongan_obat($id_golongan);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'golongan_obat berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_golongan_obat_delete";
            $comments = "Berhasil menghapus golongan_obat dengan id golongan_obat = '". $id_golongan ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_golongan_obat_delete";
            $comments = "Gagal menghapus data golongan_obat dengan ID = '". $id_golongan ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
