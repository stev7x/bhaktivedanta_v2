<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alasan_penolakan extends CI_Controller {
    public $data = array();
    public function __construct() {
    parent::__construct();
    // Your own constructor code
    $this->load->library("Aauth");

    if (!$this->aauth->is_loggedin()) {
        $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
    }
    $this->load->model('Menu_model');
    $this->load->model('Alasan_penolakan_model');
    $this->data['users'] = $this->aauth->get_user();
    $this->data['groups'] = $this->aauth->get_user_groups();
    $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index() {
        $is_permit = $this->aauth->control_no_redirect('master_data_alasan_penolakan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "master_data_alasan_penolakan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_alasan_penolakan', $this->data);
    }

    public function ajax_list_alasan(){
        $list = $this->Alasan_penolakan_model->get_tindakan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $alasan_rujuk){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $alasan_rujuk->nama;
            $row[] = $alasan_rujuk->code;
            $row[] = $alasan_rujuk->keterangan;
            //add html for action
           $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_alasan_rujuk" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit tindakan lab" onclick="editAlasanRujuk('."'".$alasan_rujuk->alasan_penundaan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus tindakan lab" id="hapusAlasanRujuk" onclick="hapusAlasanRujuk('."'".$alasan_rujuk->alasan_penundaan_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Alasan_penolakan_model->count_tindakan_all(),
                    "recordsFiltered" => $this->Alasan_penolakan_model->count_tindakan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_alasan_rujuk(){
        $is_permit = $this->aauth->control_no_redirect('master_data_alasan_penolakan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_alasan', 'Nama Alasan Rujuk','required');
        $this->form_validation->set_rules('kode_alasan', 'Kode Alasan Rujuk','required');
        $this->form_validation->set_rules('keterangan_alasan', 'Keterangan Alasan Rujuk','required');
        
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_alasan_penolakan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_alasan = $this->input->post('nama_alasan', TRUE);
            $kode_alasan = $this->input->post('kode_alasan', TRUE);
            $keterangan_alasan = $this->input->post('keterangan_alasan', TRUE);
            $data_tindakan = array(
                'nama' => $nama_alasan,
                'code' => $kode_alasan,
                'keterangan' => $keterangan_alasan,
            );

            $ins = $this->Alasan_penolakan_model->insert_alasan_rujuk($data_tindakan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan Lab berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_alasan_penolakan_create";
                $comments = "Berhasil menambahkan Tindakan Lab dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan Lab, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_alasan_penolakan_create";
                $comments = "Gagal menambahkan Tindakan Lab dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_alasan_rujuk_by_id(){
        $tindakan_lab_id = $this->input->get('alasan_rujuk_id',TRUE);
        $old_data = $this->Alasan_penolakan_model->get_alasan_rujuk_by_id($tindakan_lab_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_alasan_rujuk(){
        $is_permit = $this->aauth->control_no_redirect('master_data_alasan_penolakan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_alasan', 'Nama Alasan Rujuk', 'required');
        $this->form_validation->set_rules('upd_kode_alasan', 'Kode Alasan Rujuk', 'required');
        $this->form_validation->set_rules('upd_keterangan_alasan', 'Keterangan Alasan Rujuk', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_alasan_penolakan_update";
            $comments = "Gagal update Rencana Tindakan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $tindakan_lab_id = $this->input->post('upd_alasan_rujuk_id', TRUE);
            $nama_alasan = $this->input->post('upd_nama_alasan', TRUE);
            $kode_alasan = $this->input->post('upd_kode_alasan', TRUE);
            $keterangan_alasan = $this->input->post('upd_keterangan_alasan', TRUE);

            $data_tindakan_lab = array(
                'nama' => $nama_alasan,
                'code' => $kode_alasan,
                'keterangan' => $keterangan_alasan,
            );

            $update = $this->Alasan_penolakan_model->update_alasan_rujuk($data_tindakan_lab, $tindakan_lab_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan Lab berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_alasan_penolakan_update";
                $comments = "Berhasil mengubah tindakan lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah tindakan lab , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_alasan_penolakan_update";
                $comments = "Gagal mengubah tindakan lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_rencana_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_alasan_penolakan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakan_lab_id = $this->input->post('alasan_rujuk_id', TRUE);

        $delete = $this->Alasan_penolakan_model->delete_alasan_rujuk($tindakan_lab_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Lab berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_alasan_penolakan_delete";
            $comments = "Berhasil menghapus Tindakan Lab dengan id Tindakan Lab = '". $tindakan_lab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_alasan_penolakan_delete";
            $comments = "Gagal menghapus data Tindakan Lab dengan ID = '". $tindakan_lab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}



?>