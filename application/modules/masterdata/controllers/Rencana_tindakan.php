<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rencana_tindakan extends CI_Controller {
    public $data = array();
    public function __construct() {
    parent::__construct();
    // Your own constructor code
    $this->load->library("Aauth");

    if (!$this->aauth->is_loggedin()) {
        $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
    }

    $this->load->model('Menu_model');
    $this->load->model('Rencana_tindakan_model');
    $this->data['users'] = $this->aauth->get_user();
    $this->data['groups'] = $this->aauth->get_user_groups();
    $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    
    }

    public function index() {
        $is_permit = $this->aauth->control_no_redirect('master_data_tindakan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "master_data_tindakan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_rencana_tindakan', $this->data);
    }

    public function do_create_rencana_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_rencana_tindakan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('rencana_tindakan_nama', 'Nama Rencana Tindakan','required');
        $this->form_validation->set_rules('kelompoktindakan_id', 'Kelompok Tindakan','required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_rencana_tindakan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $rencana_tindakan_nama = $this->input->post('rencana_tindakan_nama', TRUE);
            $kelompoktindakan_id = $this->input->post('kelompoktindakan_id', TRUE);
            $data_tindakan = array(
                'rencana_tindakan_nama' => $rencana_tindakan_nama,
                'kelompoktindakan_id' => $kelompoktindakan_id,
            );

            $ins = $this->Rencana_tindakan_model->insert_rencana_tindakan($data_tindakan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan Lab berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_rencana_tindakan_create";
                $comments = "Berhasil menambahkan Tindakan Lab dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan Lab, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_rencana_tindakan_create";
                $comments = "Gagal menambahkan Tindakan Lab dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_list_tindakan(){
        $list = $this->Rencana_tindakan_model->get_tindakan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rencana_tindakan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $rencana_tindakan->rencana_tindakan_nama;
            $row[] = $rencana_tindakan->kelompoktindakan_nama;
            //add html for action
            // <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_tindakan_lab" title="klik untuk mengedit tindakan lab" onclick="editTindakanLab('."'".$tindakan_lab->tindakan_lab_id."'".')"><i class="fa fa-pencil"></i></button>
            // <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus tindakan lab" id="hapusTindakanLab" onclick="hapusTindakanLab('."'".$tindakan_lab->tindakan_lab_id."'".')"><i class="fa fa-trash"></i></button>';
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_rencana_tindakan" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit tindakan lab" onclick="editRencanaTindakan('."'".$rencana_tindakan->rencana_tindakan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus tindakan lab" id="hapusRencanaTindakan" onclick="hapusRencanaTindakan('."'".$rencana_tindakan->rencana_tindakan_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Rencana_tindakan_model->count_tindakan_all(),
                    "recordsFiltered" => $this->Rencana_tindakan_model->count_tindakan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_rencana_tindakan_by_id(){
        $tindakan_lab_id = $this->input->get('rencana_tindakan_id',TRUE);
        $old_data = $this->Rencana_tindakan_model->get_rencana_tindakan_by_id($tindakan_lab_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_rencana_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_rencana_tindakan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_rencana_tindakan_nama', 'Nama Rencana Tindakan', 'required');
        $this->form_validation->set_rules('upd_kelompoktindakan_id', 'Kelompok Tindakan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_rencana_tindakan_update";
            $comments = "Gagal update Rencana Tindakan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $tindakan_lab_id = $this->input->post('upd_rencana_tindakan_id', TRUE);
            $tindakan_lab_nama = $this->input->post('upd_rencana_tindakan_nama', TRUE);
            $kelompoktindakan_lab_id = $this->input->post('upd_kelompoktindakan_id', TRUE);

            $data_tindakan_lab = array(
                'rencana_tindakan_nama' => $tindakan_lab_nama,
                'kelompoktindakan_id' => $kelompoktindakan_lab_id,
            );

            $update = $this->Rencana_tindakan_model->update_rencana_tindakan($data_tindakan_lab, $tindakan_lab_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan Lab berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_tindakan_lab_update";
                $comments = "Berhasil mengubah tindakan lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah tindakan lab , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_tindakan_lab_update";
                $comments = "Gagal mengubah tindakan lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
    
    public function do_delete_rencana_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_rencana_tindakan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakan_lab_id = $this->input->post('rencana_tindakan_id', TRUE);

        $delete = $this->Rencana_tindakan_model->delete_rencana_tindakan($tindakan_lab_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Lab berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_rencana_tindakan_delete";
            $comments = "Berhasil menghapus Tindakan Lab dengan id Tindakan Lab = '". $tindakan_lab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_rencana_tindakan_delete";
            $comments = "Gagal menghapus data Tindakan Lab dengan ID = '". $tindakan_lab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}



?>