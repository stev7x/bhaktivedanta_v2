<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tindakan_lab extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library("Excel");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Tindakan_lab_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_tindakan_lab_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_tindakan_lab_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_tindakan_lab', $this->data);
    }



    public function ajax_list_tindakan_lab(){
        $list = $this->Tindakan_lab_model->get_tindakan_lab_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $tindakan_lab){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $tindakan_lab->tindakan_lab_nama;
            $row[] = $tindakan_lab->kelompoktindakan_lab_nama;
            $row[] = "Rp. " .number_format($tindakan_lab->harga_bpjs).".00";
            $row[] = "Rp. " .number_format($tindakan_lab->harga_non_bpjs).".00";
            $row[] = "Rp. " .number_format($tindakan_lab->harga_cyto).".00";
            $row[] = "Rp. " .number_format($tindakan_lab->harga_non_cyto).".00";
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_tindakan_lab" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit tindakan lab" onclick="editTindakanLab('."'".$tindakan_lab->tindakan_lab_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus tindakan lab" id="hapusTindakanLab" onclick="hapusTindakanLab('."'".$tindakan_lab->tindakan_lab_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Tindakan_lab_model->count_tindakan_lab_all(),
                    "recordsFiltered" => $this->Tindakan_lab_model->count_tindakan_lab_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

     function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }

    function convert_to_number($rupiah){
       return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    function get_tindakan_lab_id(){
        $result =  $this->Tindakan_lab_model->get_tindakan_lab_id();
        echo $result->id;
    }

    public function do_create_tindakan_lab(){
        $is_permit = $this->aauth->control_no_redirect('master_data_tindakan_lab_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tindakan_lab_nama', 'Nama Tindakan Lab', 'required');
        $this->form_validation->set_rules('kelompoktindakan_lab_id', 'Kelompok Tindakan', 'required');
        $this->form_validation->set_rules('harga_non_bpjs', 'Harga Non BPJS', 'required');
        $this->form_validation->set_rules('harga_bpjs', 'Harga BPJS', 'required');
        $this->form_validation->set_rules('harga_non_cyto', 'Harga Non Cyto', 'required');
        $this->form_validation->set_rules('harga_cyto', 'Harga Cyto', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error
            );

            // if permitted, do logit
            $perms = "master_data_tindakan_lab_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $tindakan_lab_nama = $this->input->post('tindakan_lab_nama', TRUE);
            $kelompoktindakan_lab_id = $this->input->post('kelompoktindakan_lab_id', TRUE);
            $harga_non_bpjs = $this->input->post('harga_non_bpjs', TRUE);
            $harga_bpjs = $this->input->post('harga_bpjs', TRUE);
            $harga_non_cyto = $this->input->post('harga_non_cyto', TRUE);
            $harga_cyto = $this->input->post('harga_cyto', TRUE);

            $harga_non_bpjs_int = $this->convert_to_number($harga_non_bpjs);
            $harga_bpjs_int = $this->convert_to_number($harga_bpjs);
            $harga_non_cyto_int = $this->convert_to_number($harga_non_cyto);
            $harga_cyto_int = $this->convert_to_number($harga_cyto);

            $data_tindakan_lab = array(
                'tindakan_lab_nama' => $tindakan_lab_nama,
                'kelompoktindakan_lab_id' => $kelompoktindakan_lab_id,
                'harga_non_bpjs' => $harga_non_bpjs_int,
                'harga_bpjs' => $harga_bpjs_int,
                'harga_non_cyto' => $harga_non_cyto_int,
                'harga_cyto' => $harga_cyto_int
            );

            // insert data tindakan lab
            $ins = $this->Tindakan_lab_model->insert_tindakan_lab($data_tindakan_lab);
            // insert data nilai Rujukan tindakan lab JIKA DIISI
            $nilai_rujukan = $this->input->post('nilai_rujukan_json', TRUE);
            if(!empty($nilai_rujukan)){
              $nilai_rujukan_decoded = json_decode($nilai_rujukan);
              $data_nilai_rujukan = array();
              foreach($nilai_rujukan_decoded as $key=>$value){
                $data_nilai_rujukan[$key]['tindakan_lab_id'] = $value->tindakan_lab_id;
                $data_nilai_rujukan[$key]['jenis_kelamin'] = $value->jenis_kelamin;
                $data_nilai_rujukan[$key]['kategori_usia'] = $value->kategori_usia;
                $data_nilai_rujukan[$key]['jenis_cairan'] = $value->jenis_cairan;
                $data_nilai_rujukan[$key]['nilai_rujukan_bawah'] = $value->nilai_rujukan_bawah;
                $data_nilai_rujukan[$key]['nilai_rujukan_atas'] = $value->nilai_rujukan_atas;
                $data_nilai_rujukan[$key]['satuan'] = $value->satuan;
              }
              $ins2 = $this->Tindakan_lab_model->insert_nilai_rujukan($data_nilai_rujukan);
              $note = 'nilai rujukan telah diisi';
            }else{
              $ins2 = TRUE;
              $note = 'nilai rujukan empty';
            }

            if($ins == $ins2){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan Lab berhasil ditambahkan',
                    'note' => $note
                );

                // if permitted, do logit
                $perms = "master_data_tindakan_lab_create";
                $comments = "Berhasil menambahkan Tindakan Lab dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan Lab, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_tindakan_lab_create";
                $comments = "Gagal menambahkan Tindakan Lab dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function jos($id){
      // for testing purpose
    }

    public function ajax_get_tindakan_lab_by_id(){
        $tindakan_lab_id = $this->input->get('tindakan_lab_id',TRUE);
        $old_data = $this->Tindakan_lab_model->get_tindakan_lab_by_id($tindakan_lab_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
            );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan"
            );
        }

        echo json_encode($res);
    }

    public function ajax_get_nilai_rujukan_tindakan_lab_by_id(){
        $tindakan_lab_id = $this->input->get('tindakan_lab_id',TRUE);
        $old_data = $this->Tindakan_lab_model->get_nilai_rujukan_tindakan_lab_by_id($tindakan_lab_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
            );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk nilai_rujukan ID kosong"
            );
        }

        echo json_encode($res);
    }

    public function do_update_tindakan_lab(){
        $is_permit = $this->aauth->control_no_redirect('master_data_tindakan_lab_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_tindakan_lab_nama', 'Nama Tindakan Lab', 'required');
        $this->form_validation->set_rules('upd_kelompoktindakan_lab_id', 'Kelompok Tindakan', 'required');
        $this->form_validation->set_rules('upd_harga_non_bpjs', 'Harga Non BPJS', 'required');
        $this->form_validation->set_rules('upd_harga_bpjs', 'Harga BPJS', 'required');
        $this->form_validation->set_rules('upd_harga_non_cyto', 'Harga Non Cyto', 'required');
        $this->form_validation->set_rules('upd_harga_cyto', 'Harga Cyto', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error
            );

            // if permitted, do logit
            $perms = "master_data_tindakan_lab_update";
            $comments = "Gagal update agama dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $tindakan_lab_id = $this->input->post('upd_tindakan_lab_id', TRUE);
            $tindakan_lab_nama = $this->input->post('upd_tindakan_lab_nama', TRUE);
            $kelompoktindakan_lab_id = $this->input->post('upd_kelompoktindakan_lab_id', TRUE);
            $harga_non_bpjs = $this->input->post('upd_harga_non_bpjs', TRUE);
            $harga_bpjs = $this->input->post('upd_harga_bpjs', TRUE);
            $harga_non_cyto = $this->input->post('upd_harga_non_cyto', TRUE);
            $harga_cyto = $this->input->post('upd_harga_cyto', TRUE);

            $harga_non_bpjs_int = $this->convert_to_number($harga_non_bpjs);
            $harga_bpjs_int = $this->convert_to_number($harga_bpjs);
            $harga_non_cyto_int = $this->convert_to_number($harga_non_cyto);
            $harga_cyto_int = $this->convert_to_number($harga_cyto);

            $data_tindakan_lab = array(
                'tindakan_lab_nama' => $tindakan_lab_nama,
                'kelompoktindakan_lab_id' => $kelompoktindakan_lab_id,
                'harga_non_bpjs' => $harga_non_bpjs_int,
                'harga_bpjs' => $harga_bpjs_int,
                'harga_non_cyto' => $harga_non_cyto_int,
                'harga_cyto' => $harga_cyto_int
            );

            $update = $this->Tindakan_lab_model->update_tindakan_lab($data_tindakan_lab, $tindakan_lab_id);
            // insert data nilai Rujukan tindakan lab JIKA DIISI
            $nilai_rujukan = $this->input->post('upd_nilai_rujukan_json', TRUE);
            $nilai_rujukan_rows = count(json_decode($nilai_rujukan));
            if(!empty($nilai_rujukan)){
              // var_dump($nilai_rujukan);
              // echo '<br>ndak nilai_rujukan nda kosonk';
              // exit;
              $nilai_rujukan_decoded = json_decode($nilai_rujukan);
              $data_nilai_rujukan = array();
              foreach($nilai_rujukan_decoded as $key=>$value){
                $data_nilai_rujukan[$key]['tindakan_lab_id'] = $value->tindakan_lab_id;
                $data_nilai_rujukan[$key]['jenis_kelamin'] = $value->jenis_kelamin;
                $data_nilai_rujukan[$key]['kategori_usia'] = $value->kategori_usia;
                $data_nilai_rujukan[$key]['jenis_cairan'] = $value->jenis_cairan;
                $data_nilai_rujukan[$key]['nilai_rujukan_bawah'] = $value->nilai_rujukan_bawah;
                $data_nilai_rujukan[$key]['nilai_rujukan_atas'] = $value->nilai_rujukan_atas;
                $data_nilai_rujukan[$key]['satuan'] = $value->satuan;
              }
              // REPLACE >> delete dulu truss insert lagi, soalnya ada multiple nilai rujukan per tindakan_lab_id
              $del = $this->Tindakan_lab_model->delete_nilai_rujukan_tindakan_lab($tindakan_lab_id);
              $insert = $this->Tindakan_lab_model->insert_nilai_rujukan($data_nilai_rujukan);
              $process = 'nilai rujukan diperbaharui sejumlah '.$nilai_rujukan_rows.' rows';
            }else{
              $del = $this->Tindakan_lab_model->delete_nilai_rujukan_tindakan_lab($tindakan_lab_id);
              $insert = TRUE;
              $process = 'nilai rujukan dihapus';
            }

            if($update == $insert){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan Lab berhasil diubah',
                    'process' => $process
                );

                // if permitted, do logit
                $perms = "master_data_tindakan_lab_update";
                $comments = "Berhasil mengubah tindakan lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah tindakan lab , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_tindakan_lab_update";
                $comments = "Gagal mengubah tindakan lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_tindakan_lab(){
        $is_permit = $this->aauth->control_no_redirect('master_data_tindakan_lab_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakan_lab_id = $this->input->post('tindakan_lab_id', TRUE);

        $delete = $this->Tindakan_lab_model->delete_tindakan_lab($tindakan_lab_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Lab berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_tindakan_lab_delete";
            $comments = "Berhasil menghapus Tindakan Lab dengan id Tindakan Lab = '". $tindakan_lab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_tindakan_lab_delete";
            $comments = "Gagal menghapus data Tindakan Lab dengan ID = '". $tindakan_lab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Tindakan Lab");
        $object->getActiveSheet()
                        ->getStyle("A1:E1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:E1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $table_columns = array("No", "Nama Tindakan","Kelompok Tindakan Lab","Harga Non BPJS","Harga BPJS","Harga Non Cyto","Harga Cyto","Normal");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Tindakan_lab_model->get_tindakan_lab_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->tindakan_lab_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->kelompoktindakan_lab_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->harga_non_bpjs);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->harga_bpjs);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->harga_non_cyto);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->harga_cyto);
            $excel_row++;
        }

        foreach(range('A','E') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Tindakan Lab.xlsx"');
        $object_writer->save('php://output');
    }

    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType  = PHPExcel_IOFactory::identify($input_file_name);
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel    = $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows      = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet   = $objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {
                // $unit_kerja_id      = $objWorksheet->getCellByColumnAndRow(0,$row)->getValue(); /
                $tindakan_lab_nama          = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1
                $kelompoktindakan_lab_id    = $objWorksheet->getCellByColumnAndRow(2,$row)->getValue(); //Excel Column 1
                $harga                      = $objWorksheet->getCellByColumnAndRow(3,$row)->getValue(); //Excel Column 1
                $normal                     = $objWorksheet->getCellByColumnAndRow(4,$row)->getValue(); //Excel Column 1

                $data = array(
                    'tindakan_lab_nama'         => $tindakan_lab_nama,
                    'kelompoktindakan_lab_id'   => $kelompoktindakan_lab_id,
                    'harga'                     => $harga,
                    'normal'                    => $normal
                );

                $this->db->insert("m_tindakan_lab", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);
        }
    }
}
