<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instalasi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library("excel");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Instalasi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_instalasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_instalasi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_instalasi', $this->data);
    }

    public function ajax_list_instalasi(){
        $list = $this->Instalasi_model->get_instalasi_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $instalasi){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $instalasi->instalasi_nama;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" onclick="editInstalasi('."'".$instalasi->instalasi_id."'".')"><i class="fa fa-pencil"></i></button>
                      <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusInstalasi('."'".$instalasi->instalasi_id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Instalasi_model->count_instalasi_all(),
                    "recordsFiltered" => $this->Instalasi_model->count_instalasi_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_instalasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_instalasi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_instalasi', 'Nama Instalasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_instalasi_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_instalasi = $this->input->post('nama_instalasi', TRUE);
            $data_instalasi = array(
                'instalasi_nama' => $nama_instalasi,
            );

            $ins = $this->Instalasi_model->insert_instalasi($data_instalasi);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Instalasi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_instalasi_create";
                $comments = "Berhasil menambahkan Instalasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Instalasi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_instalasi_create";
                $comments = "Gagal menambahkan Instalasi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_instalasi_by_id(){
        $instalasi_id = $this->input->get('instalasi_id',TRUE);
        $old_data = $this->Instalasi_model->get_instalasi_by_id($instalasi_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_instalasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_instalasi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_instalasi', 'Nama Instalasi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_instalasi_update";
            $comments = "Gagal update instalasi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $instalasi_id = $this->input->post('upd_id_instalasi',TRUE);
            $nama_instalasi = $this->input->post('upd_nama_instalasi', TRUE);

            $data_instalasi = array(
                'instalasi_nama' => $nama_instalasi
            );

            $update = $this->Instalasi_model->update_instalasi($data_instalasi, $instalasi_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Instalasi berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_instalasi_update";
                $comments = "Berhasil mengubah instalasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah instalasi , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_instalasi_update";
                $comments = "Gagal mengubah instalasi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_instalasi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_instalasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $instalasi_id = $this->input->post('instalasi_id', TRUE);

        $delete = $this->Instalasi_model->delete_instalasi($instalasi_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Instalasi berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_instalasi_delete";
            $comments = "Berhasil menghapus Instalasi dengan id Instalasi = '". $instalasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_instalasi_delete";
            $comments = "Gagal menghapus data Instalasi dengan ID = '". $instalasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Instalasi");
        $object->getActiveSheet()
                        ->getStyle("A1:B1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:B1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No","Nama Instalasi");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Instalasi_model->get_instalasi_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->instalasi_nama);

            $excel_row++;
        }

        foreach(range('A','B') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);


        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Instalasi.xlsx"');
        $object_writer->save('php://output');
    }

    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType = PHPExcel_IOFactory::identify($input_file_name);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel= $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {

                $instalasi_nama    = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1


                $data = array(
                    // 'agama_id' => $agama_id,
                    'instalasi_nama'   => $instalasi_nama,

                );

                $this->db->insert("m_instalasi", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);



        }

    }
}
