<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lab extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Lab_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_lab_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_lab_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_lab', $this->data);
    }

    public function ajax_list_lab(){
        $list = $this->Lab_model->get_lab_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $lab){
            $tarif ="Rp. " .number_format($lab->tarif).".00";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $lab->nama_pemeriksaan;
            $row[] = $lab->jenis;
            $row[] = $lab->tampil_per." ".$lab->satuan;
            $row[] = $lab->tampil_per_anak." ".$lab->satuan;
            $row[] = $lab->tampil_laki." ".$lab->satuan;
            $row[] = $lab->tampil_laki_anak." ".$lab->satuan;
            $row[] = $lab->metode;
            $row[] = $tarif;
            $row[] = $lab->keterangan;
            //add html for action
            $row[] = '
                        <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_lab" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit lab" onclick="getEditLab('."'".$lab->lab_id."'".')"><i class="fa fa-pencil"></i></button>
                    ';
            // <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus agama" disabled id="hapus_lab" onclick="hapusLab('."'".$lab->lab_id."'".')"><i class="fa fa-trash"></i></button>
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Lab_model->count_lab_all(),
                    "recordsFiltered" => $this->Lab_model->count_lab_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function convert_to_number($rupiah){
       return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function do_create_lab(){
        $is_permit = $this->aauth->control_no_redirect('master_data_lab_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_pemeriksaan', 'Nama Pemeriksaan', 'required');
        $this->form_validation->set_rules('jenis', 'Jenis', 'required');
        $this->form_validation->set_rules('metode_pemeriksaan', 'Metode', 'required');
        $this->form_validation->set_rules('satuan', 'Satuan', 'required');
        $this->form_validation->set_rules('terendah_per', 'Range Perempuan', 'required');
        $this->form_validation->set_rules('terendah_laki', 'Range Laki-laki', 'required');
        $this->form_validation->set_rules('tertinggi_laki', 'Range Laki-laki', 'required');
        $this->form_validation->set_rules('tertinggi_per', 'Range Perempuan', 'required');
        $this->form_validation->set_rules('tarif', 'Tarif', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_lab_create";
            $comments = "Gagal input lab dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_pemeriksaan   = $this->input->post('nama_pemeriksaan', TRUE);
            $jenis_pemeriksaan  = $this->input->post('jenis', TRUE);
            $metode_pemeriksaan = $this->input->post('metode_pemeriksaan', TRUE);
            $satuan             = $this->input->post('satuan', TRUE);
            $terendah_per       = $this->input->post('terendah_per', TRUE);
            $tertinggi_per      = $this->input->post('tertinggi_per', TRUE);
            $terendah_laki      = $this->input->post('terendah_laki', TRUE);
            $tertinggi_laki     = $this->input->post('tertinggi_laki', TRUE);
            $terendah_per_anak  = $this->input->post('terendah_per_anak', TRUE);
            $tertinggi_per_anak = $this->input->post('tertinggi_per_anak', TRUE);
            $terendah_laki_anak = $this->input->post('terendah_laki_anak', TRUE);
            $tertinggi_laki_anak= $this->input->post('tertinggi_laki_anak', TRUE);
            $tarif              = $this->input->post('tarif', TRUE);
            $ket                = $this->input->post('ket', TRUE);


            $tarif = $this->convert_to_number($tarif);

            $data_lab = array(
                'nama_pemeriksaan'  => $nama_pemeriksaan,
                'jenis'             => $jenis_pemeriksaan,
                'metode'            => $metode_pemeriksaan,
                'tarif'             => $tarif,
                'terendah_per'      => $terendah_per,
                'tertinggi_per'     => $tertinggi_per,
                'terendah_laki'     => $terendah_laki,
                'tertinggi_laki'    => $tertinggi_laki,
                'terendah_per_anak' => $terendah_per_anak,
                'tertinggi_per_anak'=> $tertinggi_per_anak,
                'terendah_laki_anak'=> $terendah_laki_anak,
                'tertinggi_laki_anak'=> $tertinggi_laki_anak,
                'tampil_per'        => $terendah_per.",00 - ".$tertinggi_per.",00",
                'tampil_laki'       => $terendah_laki.",00 - ".$tertinggi_laki.",00",
                'tampil_per_anak'   => $terendah_per_anak.",00 - ".$tertinggi_per_anak.",00",
                'tampil_laki_anak'  => $terendah_laki_anak.",00 - ".$tertinggi_laki_anak.",00",
                'satuan'            => $satuan,
                'keterangan'        => $ket,
            );

            $ins = $this->Lab_model->insert_lab($data_lab);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'lab berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_lab_create";
                $comments = "Berhasil menambahkan lab dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan lab, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_lab_create";
                $comments = "Gagal menambahkan lab dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_lab_by_id(){
        $lab_id = $this->input->get('lab_id',TRUE);
        $old_data = $this->Lab_model->get_lab_by_id($lab_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_lab(){
        $is_permit = $this->aauth->control_no_redirect('master_data_lab_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('lab_id', 'ID LAB', 'required');
        // $this->form_validation->set_rules('nama_pemeriksaan', 'Nama Pemeriksaan', 'required');
        // $this->form_validation->set_rules('jenis', 'Jenis', 'required');
        // $this->form_validation->set_rules('metode_pemeriksaan', 'Metode', 'required');
        // $this->form_validation->set_rules('satuan', 'Satuan', 'required');
        // $this->form_validation->set_rules('terendah_per', 'Range Perempuan', 'required');
        // $this->form_validation->set_rules('terendah_laki', 'Range Laki-laki', 'required');
        // $this->form_validation->set_rules('tertinggi_laki', 'Range Laki-laki', 'required');
        // $this->form_validation->set_rules('tertinggi_per', 'Range Perempuan', 'required');
        // $this->form_validation->set_rules('tarif', 'Tarif', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_lab_update";
            $comments = "Gagal update lab dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $lab_id             = $this->input->post('lab_id', TRUE);
            $nama_pemeriksaan   = $this->input->post('nama_pemeriksaan', TRUE);
            $jenis_pemeriksaan  = $this->input->post('jenis', TRUE);
            $metode_pemeriksaan = $this->input->post('metode_pemeriksaan', TRUE);
            $satuan             = $this->input->post('satuan', TRUE);
            $terendah_per       = $this->input->post('terendah_per', TRUE);
            $tertinggi_per      = $this->input->post('tertinggi_per', TRUE);
            $terendah_laki      = $this->input->post('terendah_laki', TRUE);
            $tertinggi_laki     = $this->input->post('tertinggi_laki', TRUE);
            $terendah_per_anak  = $this->input->post('terendah_per_anak', TRUE);
            $tertinggi_per_anak = $this->input->post('tertinggi_per_anak', TRUE);
            $terendah_laki_anak = $this->input->post('terendah_laki_anak', TRUE);
            $tertinggi_laki_anak= $this->input->post('tertinggi_laki_anak', TRUE);
            $tarif              = $this->input->post('tarif', TRUE);
            $ket                = $this->input->post('ket', TRUE);


            $tarif = $this->convert_to_number($tarif);

            $data_lab = array(
                'nama_pemeriksaan'  => $nama_pemeriksaan,
                'jenis'             => $jenis_pemeriksaan,
                'metode'            => $metode_pemeriksaan,
                'tarif'             => $tarif,
                'terendah_per'      => $terendah_per,
                'tertinggi_per'     => $tertinggi_per,
                'terendah_laki'     => $terendah_laki,
                'tertinggi_laki'    => $tertinggi_laki,
                'terendah_per_anak' => $terendah_per_anak,
                'tertinggi_per_anak'=> $tertinggi_per_anak,
                'terendah_laki_anak'=> $terendah_laki_anak,
                'tertinggi_laki_anak'=> $tertinggi_laki_anak,
                'tampil_per'        => $terendah_per.",00 - ".$tertinggi_per.",00",
                'tampil_laki'       => $terendah_laki.",00 - ".$tertinggi_laki.",00",
                'tampil_per_anak'   => $terendah_per_anak.",00 - ".$tertinggi_per_anak.",00",
                'tampil_laki_anak'  => $terendah_laki_anak.",00 - ".$tertinggi_laki_anak.",00",
                'satuan'            => $satuan,
                'keterangan'        => $ket,
            );


            $update = $this->Lab_model->update_lab($data_lab, $lab_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Agama berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_lab_update";
                $comments = "Berhasil mengubah lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah lab , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_lab_update";
                $comments = "Gagal mengubah lab  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    // public function do_delete_lab(){
    //     $is_permit = $this->aauth->control_no_redirect('master_data_lab_delete');
    //     if(!$is_permit) {
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash' => $this->security->get_csrf_hash(),
    //             'success' => false,
    //             'messages' => $this->lang->line('aauth_error_no_access'));
    //         echo json_encode($res);
    //         exit;
    //     }

    //     $lab_id = $this->input->post('lab_id', TRUE);

    //     $delete = $this->Lab_model->delete_lab($lab_id);
    //     if($delete){
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash' => $this->security->get_csrf_hash(),
    //             'success' => true,
    //             'messages' => 'lab berhasil dihapus'
    //         );
    //         // if permitted, do logit
    //         $perms = "master_data_lab_delete";
    //         $comments = "Berhasil menghapus lab dengan id Agama = '". $lab_id ."'.";
    //         $this->aauth->logit($perms, current_url(), $comments);
    //     }else{
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash' => $this->security->get_csrf_hash(),
    //             'success' => false,
    //             'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
    //         );
    //         // if permitted, do logit
    //         $perms = "master_data_lab_delete";
    //         $comments = "Gagal menghapus data lab dengan ID = '". $lab_id ."'.";
    //         $this->aauth->logit($perms, current_url(), $comments);
    //     }
    //     echo json_encode($res);
    // }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("lab");
         $object->getActiveSheet()
                        ->getStyle("A1:J1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:J1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nama Pemeriksaan","Jenis","Range Perempuan","Range Perempuan Anak","Range Laki-laki","Range Laki-laki Anak","Metode","Tarif","Keterangan");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Lab_model->get_lab_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nama_pemeriksaan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->jenis);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->tampil_per."".$row->satuan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->tampil_per_anak."".$row->satuan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->tampil_laki."".$row->satuan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->tampil_laki_anak."".$row->satuan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->metode);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->tarif);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->keterangan);
            $excel_row++;
        }

         foreach(range('A','J') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="lab.xlsx"');
        $object_writer->save('php://output');
    }
}
