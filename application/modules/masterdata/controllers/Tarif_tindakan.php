<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarif_tindakan extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Tarif_tindakan_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        // $is_permit = $this->aauth->control_no_redirect('master_data_tarif_tindakan_view');
        // if(!$is_permit) {
        //     $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
        //     redirect('no_permission');
        // }

        // // if permitted, do logit
        // $perms = "master_data_tarif_tindakan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_tarif_tindakan', $this->data);
    }

    public function ajax_list_tarif_tindakan(){
        $list = $this->Tarif_tindakan_model->get_tarif_tindakan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $tarif_tindakan){
            $tarif ="Rp. " .number_format($tarif_tindakan->harga_tindakan).".00";
            $cyto ="Rp. " .number_format($tarif_tindakan->cyto_tindakan).".00";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $tarif_tindakan->kelaspelayanan_nama;
            $row[] = $tarif_tindakan->daftartindakan_nama;
            $row[] = $cyto;
            $row[] = $tarif;
            $row[] = $tarif_tindakan->komponentarif_nama;
            $row[] = $tarif_tindakan->bpjs_non_bpjs;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_tarif_tindakan" data-backdrop="static" data-keyboard="false" onclick="editTarif_tindakan('."'".$tarif_tindakan->tariftindakan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusTarif_tindakan('."'".$tarif_tindakan->tariftindakan_id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Tarif_tindakan_model->count_tarif_tindakan_all(),
                    "recordsFiltered" => $this->Tarif_tindakan_model->count_tarif_tindakan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }



    public function do_create_tarif_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_tarif_tindakan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_tarif_tindakan', 'Tarif Tindakan', 'required');
        $this->form_validation->set_rules('select_kelas', 'Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('select_tindakan', 'Tindakan', 'required');
        $this->form_validation->set_rules('select_komponen', 'Komponen Tarif', 'required');
        $this->form_validation->set_rules('bpjs_non_bpjs', 'Jenis Tarif', 'required');
        $this->form_validation->set_rules('cyto_tindakan', 'Cyto Tindakan', 'required');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_tarif_tindakan_create";
            $comments = "Gagal input Tarif dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_tarif_tindakan    = $this->input->post('nama_tarif_tindakan', TRUE);
            $select_kelas           =  $this->input->post('select_kelas', TRUE);
            $select_tindakan        = $this->input->post('select_tindakan', TRUE);
            $select_komponen        = $this->input->post('select_komponen', TRUE);
            $is_bpjs                = $this->input->post('bpjs_non_bpjs', TRUE);
            $cyto_tindakan          = $this->input->post('cyto_tindakan', TRUE);

            $nama_tarif_tindakan_int = $this->convert_to_number($nama_tarif_tindakan);
            $cyto_tindakan_int = $this->convert_to_number($cyto_tindakan);

            $data_tarif_tindakan = array(
                'harga_tindakan'        => $nama_tarif_tindakan_int,
                'kelaspelayanan_id'     => $select_kelas,
                'daftartindakan_id'     => $select_tindakan,
                'cyto_tindakan'         => $cyto_tindakan_int,
                'komponentarif_id' => $select_komponen,
                'bpjs_non_bpjs' => $is_bpjs
            );

            $ins = $this->Tarif_tindakan_model->insert_tarif_tindakan($data_tarif_tindakan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tarif tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_tarif_tindakan_create";
                $comments = "Berhasil menambahkan Tarif_tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tarif tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_tarif_tindakan_create";
                $comments = "Gagal menambahkan Tarif tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }

    function convert_to_number($rupiah){
       return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function ajax_get_tarif_tindakan_by_id(){
        $tariftindakan_id = $this->input->get('tariftindakan_id',TRUE);
        $old_data = $this->Tarif_tindakan_model->get_tarif_tindakan_by_id($tariftindakan_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_tarif_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_tarif_tindakan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_tarif_tindakan', 'Nama Tarif Tindakan', 'required');
        $this->form_validation->set_rules('upd_select_kelas', 'Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('upd_select_tindakan', 'Tindakan', 'required');
        $this->form_validation->set_rules('upd_select_komponen', 'Komponen Tarif', 'required');
        $this->form_validation->set_rules('upd_bpjs_non_bpjs', 'Jenis Tarif', 'required');
        $this->form_validation->set_rules('upd_cyto_tindakan', 'Cyto Tindakan', 'required');



        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_tarif_tindakan_update";
            $comments = "Gagal update tarif tindakan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $tariftindakan_id       = $this->input->post('upd_id_tarif_tindakan',TRUE);
            $nama_tarif_tindakan    = $this->input->post('upd_nama_tarif_tindakan', TRUE);
            $upd_select_kelas       = $this->input->post('upd_select_kelas', TRUE);
            $upd_select_tindakan    = $this->input->post('upd_select_tindakan', TRUE);
            $upd_select_komponen    = $this->input->post('upd_select_komponen', TRUE);
            $upd_is_bpjs    = $this->input->post('upd_bpjs_non_bpjs', TRUE);
            $upd_cyto_tindakan      = $this->input->post('upd_cyto_tindakan', TRUE);


            $nama_tarif_tindakan_int = $this->convert_to_number($nama_tarif_tindakan);
            $upd_cyto_tindakan_int = $this->convert_to_number($upd_cyto_tindakan);

            $data_tarif_tindakan = array(
                'harga_tindakan'        => $nama_tarif_tindakan_int,
                'kelaspelayanan_id'     => $upd_select_kelas,
                'daftartindakan_id'     => $upd_select_tindakan,
                'cyto_tindakan'         => $upd_cyto_tindakan_int,
                'komponentarif_id' => $upd_select_komponen,
                'bpjs_non_bpjs' => $upd_is_bpjs
            );

            $update = $this->Tarif_tindakan_model->update_tarif_tindakan($data_tarif_tindakan, $tariftindakan_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tarif tindakan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_tarif_tindakan_update";
                $comments = "Berhasil mengubah tarif_tindakan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah tarif tindakan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_tarif_tindakan_update";
                $comments = "Gagal mengubah tarif tindakan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_tarif_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_tarif_tindakan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tariftindakan_id = $this->input->post('tariftindakan_id', TRUE);

        $delete = $this->Tarif_tindakan_model->delete_tarif_tindakan($tariftindakan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tarif tindakan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_tarif_tindakan_delete";
            $comments = "Berhasil menghapus Tarif_tindakan dengan id Tarif_tindakan = '". $tariftindakan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_tarif_tindakan_delete";
            $comments = "Gagal menghapus data Tarif tindakan dengan ID = '". $tariftindakan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
