<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indikasi_obat extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Indikasi_obat_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_indikasi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_indikasi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_indikasi_obat', $this->data);
    }

    public function ajax_list_indikasi_obat(){
        $list = $this->Indikasi_obat_model->get_indikasi_obat_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $indikasi_obat){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $indikasi_obat->kode_indikasi;
            $row[] = $indikasi_obat->nama_indikasi;
            $row[] = $indikasi_obat->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_indikasi_obat" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit indikasi_obat" onclick="editIndikasiObat('."'".$indikasi_obat->id_indikasi."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus indikasi_obat" id="hapusIndikasiObat" onclick="hapusIndikasiObat('."'".$indikasi_obat->id_indikasi."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Indikasi_obat_model->count_indikasi_obat_all(),
                    "recordsFiltered" => $this->Indikasi_obat_model->count_indikasi_obat_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_indikasi_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_indikasi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_indikasi', 'Kode Kelompok obat', 'required');
        $this->form_validation->set_rules('nama_indikasi', 'Nama Kelompok obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_indikasi_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $kode_indikasi = $this->input->post('kode_indikasi', TRUE);
            $nama_indikasi = $this->input->post('nama_indikasi', TRUE);
            $keterangan    = $this->input->post('keterangan', TRUE);
            $data_indikasi_obat = array(
                'kode_indikasi' => $kode_indikasi,
                'nama_indikasi' => $nama_indikasi,
                'keterangan'         => $keterangan,
            );

            $ins = $this->Indikasi_obat_model->insert_indikasi_obat($data_indikasi_obat);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'indikasi_obat berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_indikasi_create";
                $comments = "Berhasil menambahkan indikasi_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan indikasi_obat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_indikasi_create";
                $comments = "Gagal menambahkan indikasi_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_indikasi_obat_by_id(){
        $id_indikasi = $this->input->get('id_indikasi',TRUE);
        $old_data = $this->Indikasi_obat_model->get_indikasi_obat_by_id($id_indikasi);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_indikasi_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_indikasi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kode_indikasi', 'Kode golongan', 'required');
        $this->form_validation->set_rules('upd_nama_indikasi', 'Nama golongan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_indikasi_update";
            $comments = "Gagal update indikasi_obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_indikasi = $this->input->post('upd_id_indikasi',TRUE);
            $kode_indikasi = $this->input->post('upd_kode_indikasi', TRUE);
            $nama_indikasi = $this->input->post('upd_nama_indikasi', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_indikasi_obat = array(
                'kode_indikasi' => $kode_indikasi,
                'nama_indikasi' => $nama_indikasi,
                'keterangan' => $keterangan
            );

            $update = $this->Indikasi_obat_model->update_indikasi_obat($data_indikasi_obat, $id_indikasi);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'indikasi_obat berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_indikasi_update";
                $comments = "Berhasil mengubah indikasi_obat  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah indikasi_obat , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_indikasi_update";
                $comments = "Gagal mengubah indikasi_obat  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_indikasi_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_indikasi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_indikasi = $this->input->post('id_indikasi', TRUE);

        $delete = $this->Indikasi_obat_model->delete_indikasi_obat($id_indikasi);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'indikasi_obat berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_indikasi_delete";
            $comments = "Berhasil menghapus indikasi_obat dengan id indikasi_obat = '". $id_indikasi ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_indikasi_delete";
            $comments = "Gagal menghapus data indikasi_obat dengan ID = '". $id_indikasi ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
