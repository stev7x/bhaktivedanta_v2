<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Sekolah_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_sekolah_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "masterdata_sekolah_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_sekolah', $this->data);
    }

    public function ajax_list_sekolah(){
        $list = $this->Sekolah_model->get_sekolah_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $sekolah){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $sekolah->sekolah_nama;
            $row[] = $sekolah->sekolah_alamat;
            $row[] = $sekolah->telp;
            $row[] = $sekolah->web;
            $row[] = $sekolah->email;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_sekolah" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit sekolah" onclick="getEditSekolah('."'".$sekolah->sekolah_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus agama" id="hapus_sekolah" onclick="hapusSekolah('."'".$sekolah->sekolah_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Sekolah_model->count_sekolah_all(),
                    "recordsFiltered" => $this->Sekolah_model->count_sekolah_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_sekolah(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_sekolah_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('sekolah', 'sekolah', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('telp', 'telp', 'required');
        $this->form_validation->set_rules('web', 'web', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "masterdata_sekolah_create";
            $comments = "Gagal input sekolah dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $data_sekolah['sekolah_nama']   = $this->input->post('sekolah', TRUE);
            $data_sekolah['sekolah_alamat'] = $this->input->post('alamat', TRUE);
            $data_sekolah['telp']           = $this->input->post('telp', TRUE);
            $data_sekolah['email']          = $this->input->post('email', TRUE);
            $data_sekolah['web']            = $this->input->post('web', TRUE);


            $ins = $this->Sekolah_model->insert_sekolah($data_sekolah);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'sekolah berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "masterdata_sekolah_create";
                $comments = "Berhasil menambahkan sekolah dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan sekolah, hubungi web administrator.');

                // if permitted, do logit
                $perms = "masterdata_sekolah_create";
                $comments = "Gagal menambahkan sekolah dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_sekolah_by_id(){
        $sekolah_id = $this->input->get('sekolah_id',TRUE);
        $old_data = $this->Sekolah_model->get_sekolah_by_id($sekolah_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_sekolah(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_sekolah_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('sekolah', 'Nama sekolah', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "masterdata_sekolah_update";
            $comments = "Gagal update sekolah dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $data_sekolah['sekolah_nama']   = $this->input->post('sekolah', TRUE);
            $data_sekolah['sekolah_alamat'] = $this->input->post('alamat', TRUE);
            $data_sekolah['telp']           = $this->input->post('telp', TRUE);
            $data_sekolah['email']          = $this->input->post('email', TRUE);
            $data_sekolah['web']            = $this->input->post('web', TRUE);

            $sekolah_id = $this->input->post('sekolah_id',TRUE);


            $update = $this->Sekolah_model->update_sekolah($data_sekolah, $sekolah_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Agama berhasil diubah'
                );

                // if permitted, do logit
                $perms = "masterdata_sekolah_update";
                $comments = "Berhasil mengubah sekolah  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah sekolah , hubungi web administrator.');

                // if permitted, do logit
                $perms = "masterdata_sekolah_update";
                $comments = "Gagal mengubah sekolah  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_sekolah(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_sekolah_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $sekolah_id = $this->input->post('sekolah_id', TRUE);

        $delete = $this->Sekolah_model->delete_sekolah($sekolah_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'sekolah berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "masterdata_sekolah_delete";
            $comments = "Berhasil menghapus sekolah dengan id Agama = '". $sekolah_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "masterdata_sekolah_delete";
            $comments = "Gagal menghapus data sekolah dengan ID = '". $sekolah_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Master Data Sekolah");
         $object->getActiveSheet()
                        ->getStyle("A1:F1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:F1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Nama Sekolah","Alamat Sekolah","Telp","Web","Email");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Sekolah_model->get_sekolah_list2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->sekolah_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->sekolah_alamat);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->telp);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->web);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->email);
            $excel_row++;
        }

         foreach(range('A','F') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Master Data Sekolah.xlsx"');
        $object_writer->save('php://output');
    }
}
