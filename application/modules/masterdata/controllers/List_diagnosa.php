<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_diagnosa extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        // $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        // $this->load->library('PHPExcel/IOFactory');
        $this->load->library('excel');


        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('List_diagnosa_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_diagnosa_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "masterdata_diagnosa_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_diagnosa', $this->data);
    }

    public function ajax_list_diagnosa(){
        $list = $this->List_diagnosa_model->get_diagnosa_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $diagnosa){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $diagnosa->diagnosa_kode;
            $row[] = $diagnosa->diagnosa_nama;
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_diagnosa" title="klik untuk mengedit diagnosa" data-backdrop="static" data-keyboard="false" onclick="getEditDiagnosa('."'".$diagnosa->diagnosa2_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus diagnosa" id="hapusDIagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosa2_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->List_diagnosa_model->count_diagnosa_all(),
                    "recordsFiltered" => $this->List_diagnosa_model->count_diagnosa_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_diagnosa_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('diagnosa_kode', 'Kode Diagnosa', 'required');
        $this->form_validation->set_rules('diagnosa_nama', 'Nama Diagnosa', 'required');
        $this->form_validation->set_rules('kode_diagnosa_icd', 'Kelompok Diagnosa ICD', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "masterdata_diagnosa_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $diagnosa_kode      = $this->input->post('diagnosa_kode', TRUE);
            $diagnosa_nama      = $this->input->post('diagnosa_nama', TRUE);
            $kode_diagnosa_icd  = $this->input->post('kode_diagnosa_icd', TRUE);
            $data_diagnosa = array(
                'diagnosa_kode' => $diagnosa_kode,
                'diagnosa_nama' => $diagnosa_nama,
                'diagnosa_id'   => $kode_diagnosa_icd
            );

            $ins = $this->List_diagnosa_model->insert_diagnosa($data_diagnosa);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Diagnosa berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms      = "masterdata_diagnosa_create";
                $comments   = "Berhasil menambahkan Agama dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $comments   = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                // if permitted, do logit
                $perms      = "masterdata_diagnosa_create";
                $comments   = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_diagnosa_by_id(){
        $diagnosa2_id = $this->input->get('diagnosa2_id',TRUE);
        $old_data = $this->List_diagnosa_model->get_diagnosa_by_id($diagnosa2_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_diagnosa_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('diagnosa_kode', 'Kode Diagnosa', 'required');
        $this->form_validation->set_rules('diagnosa_nama', 'Nama Diagnosa', 'required');
        $this->form_validation->set_rules('kode_diagnosa_icd', 'Kelompok Diagnosa ICD', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "masterdata_diagnosa_update";
            $comments = "Gagal update agama dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $diagnosa2_id       = $this->input->post('diagnosa2_id', TRUE);
            $diagnosa_kode      = $this->input->post('diagnosa_kode', TRUE);
            $diagnosa_nama      = $this->input->post('diagnosa_nama', TRUE);
            $kode_diagnosa_icd  = $this->input->post('kode_diagnosa_icd', TRUE);
            $data_diagnosa = array(
                'diagnosa_kode' => $diagnosa_kode,
                'diagnosa_nama' => $diagnosa_nama,
                'diagnosa_id'   => $kode_diagnosa_icd
            );

            $update = $this->List_diagnosa_model->update_diagnosa($data_diagnosa, $diagnosa2_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Diagnosa berhasil diubah'
                );

                // if permitted, do logit
                $perms      = "masterdata_diagnosa_update";
                $comments   = "Berhasil mengubah agama  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal mengubah diagnosa , hubungi web administrator.');

                // if permitted, do logit
                $perms      = "masterdata_diagnosa_update";
                $comments   = "Gagal mengubah diagnosa  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('masterdata_diagnosa_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosa2_id = $this->input->post('diagnosa2_id', TRUE);

        $delete = $this->List_diagnosa_model->delete_diagnosa($diagnosa2_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => true,
                'messages'      => 'Diagnosa berhasil dihapus'
            );
            // if permitted, do logit
            $perms      = "masterdata_diagnosa_delete";
            $comments   = "Berhasil menghapus diagnosa dengan id diagnosa = '". $diagnosa2_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms      = "masterdata_diagnosa_delete";
            $comments   = "Gagal menghapus data diagnosa dengan ID = '". $diagnosa2_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    // public function upload(){
    //     // $tes = $this->input->post('tes', TRUE);
    //     $fileName = $_FILES['file']['name'];
    //     $config['upload_path'] = './temp_upload/';
    //     $config['file_name'] = $fileName;
    //     $config['allowed_types'] = 'xls|xlsx|csv';
    //     $config['max_size'] = 10000;
    //     $this->load->library('upload', $config);
    //     $this->upload->initialize($config);
    //     $this->upload->do_upload('file');

    //     $upload_data = $this->upload->data();

    //     if (!$upload_data){
    //         $this->upload->display_errors();
    //         echo "error";
    //     }else{
    //         // echo "true";

    //         $file_name = $upload_data['file_name'];

    //         echo "data : " . $file_name;
    //         // print_r("hasil : " .$file_name);die();
    //         $input_file_name = './temp_upload/'.$file_name;

    //         try{
    //             echo "berhassil";
    //             $inputFileType = PHPExcel_IOFactory::identify($input_file_name);
    //             $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    //             // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
    //             $objReader->setReadDataOnly(true);

    //             $objPHPExcel= $objReader->load(FCPATH.'temp_upload/'.$file_name);



    //         } catch(Exception $e){
    //             echo "gagal";
    //             die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
    //         }

    //         $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
    //         $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

    //         for ($row = 2; $row <= $totalrows; $row++) {

    //             $agama_id= $objWorksheet->getCellByColumnAndRow(0,$row)->getValue();
    //             $agama_nama= $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1

    //             $data = array(
    //                 // 'agama_id' => $agama_id,
    //                 'agama_nama' => $agama_nama
    //             );

    //             $this->db->insert("m_agama", $data);
    //         }

    //         echo "berhasil insert";

    //         // delete file
    //         $path = './temp_upload/' . $file_name;
    //         unlink($path);



    //     }

    // }
}
