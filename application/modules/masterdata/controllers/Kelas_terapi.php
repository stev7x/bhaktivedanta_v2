<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_terapi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kelas_terapi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_terapi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kelas_terapi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kelasterapi', $this->data);
    }

    public function ajax_list_kelas_terapi(){
        $list = $this->Kelas_terapi_model->get_kelas_terapi_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kelas_terapi){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kelas_terapi->nama_kelas_terapi;
            $row[] = $kelas_terapi->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_kelas_terapi" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit kelas_terapi" onclick="editKelasTerapi('."'".$kelas_terapi->id_kelas_terapi."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus kelas_terapi" id="hapusKelasTerapi" onclick="hapusKelasTerapi('."'".$kelas_terapi->id_kelas_terapi."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kelas_terapi_model->count_kelas_terapi_all(),
                    "recordsFiltered" => $this->Kelas_terapi_model->count_kelas_terapi_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_kelas_terapi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_terapi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_kelas_terapi', 'Nama kelas terapi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelas_terapi_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_kelas_terapi = $this->input->post('nama_kelas_terapi', TRUE);
            $data_kelas_terapi = array(
                'nama_kelas_terapi' => $nama_kelas_terapi,
            );

            $ins = $this->Kelas_terapi_model->insert_kelas_terapi($data_kelas_terapi);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'kelas_terapi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kelas_terapi_create";
                $comments = "Berhasil menambahkan kelas_terapi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan kelas_terapi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelas_terapi_create";
                $comments = "Gagal menambahkan kelas_terapi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_kelas_terapi_by_id(){
        $id_kelas_terapi = $this->input->get('id_kelas_terapi',TRUE);
        $old_data = $this->Kelas_terapi_model->get_kelas_terapi_by_id($id_kelas_terapi);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_kelas_terapi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_terapi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_kelas_terapi', 'Nama kelas terapi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelas_terapi_update";
            $comments = "Gagal update kelas_terapi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_kelas_terapi = $this->input->post('upd_id_kelas_terapi',TRUE);
            $nama_kelas_terapi = $this->input->post('upd_nama_kelas_terapi', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_kelas_terapi = array(
                'nama_kelas_terapi' => $nama_kelas_terapi,
                'keterangan' => $keterangan
            );

            $update = $this->Kelas_terapi_model->update_kelas_terapi($data_kelas_terapi, $id_kelas_terapi);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'kelas_terapi berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kelas_terapi_update";
                $comments = "Berhasil mengubah kelas_terapi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah kelas_terapi , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelas_terapi_update";
                $comments = "Gagal mengubah kelas_terapi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_kelas_terapi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelas_terapi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_kelas_terapi = $this->input->post('id_kelas_terapi', TRUE);

        $delete = $this->Kelas_terapi_model->delete_kelas_terapi($id_kelas_terapi);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'kelas_terapi berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kelas_terapi_delete";
            $comments = "Berhasil menghapus kelas_terapi dengan id kelas_terapi = '". $id_kelas_terapi ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kelas_terapi_delete";
            $comments = "Gagal menghapus data kelas_terapi dengan ID = '". $id_kelas_terapi ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
