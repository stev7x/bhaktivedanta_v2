<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interaksi_obat extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Interaksi_obat_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_interaksi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_interaksi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_interaksi_obat', $this->data);
    }

    public function ajax_list_interaksi_obat(){
        $list = $this->Interaksi_obat_model->get_interaksi_obat_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $interaksi_obat){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $interaksi_obat->kode_interaksi;
            $row[] = $interaksi_obat->nama_interaksi;
            $row[] = $interaksi_obat->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_interaksi_obat" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit interaksi_obat" onclick="editInteraksi('."'".$interaksi_obat->id_interaksi."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus interaksi_obat" id="hapusInteraksi" onclick="hapusInteraksi('."'".$interaksi_obat->id_interaksi."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Interaksi_obat_model->count_interaksi_obat_all(),
                    "recordsFiltered" => $this->Interaksi_obat_model->count_interaksi_obat_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_interaksi_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_interaksi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_interaksi', 'Kode Interaksi obat', 'required');
        $this->form_validation->set_rules('nama_interaksi', 'Nama Interaksi obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_interaksi_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $kode_interaksi = $this->input->post('kode_interaksi', TRUE);
            $nama_interaksi = $this->input->post('nama_interaksi', TRUE);
            $keterangan    = $this->input->post('keterangan', TRUE);
            $data_interaksi_obat = array(
                'kode_interaksi' => $kode_interaksi,
                'nama_interaksi' => $nama_interaksi,
                'keterangan'         => $keterangan,
            );

            $ins = $this->Interaksi_obat_model->insert_interaksi_obat($data_interaksi_obat);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'interaksi_obat berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_interaksi_create";
                $comments = "Berhasil menambahkan interaksi_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan interaksi_obat, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_interaksi_create";
                $comments = "Gagal menambahkan interaksi_obat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_interaksi_obat_by_id(){
        $id_interaksi = $this->input->get('id_interaksi',TRUE);
        $old_data = $this->Interaksi_obat_model->get_interaksi_obat_by_id($id_interaksi);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_interaksi_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_interaksi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kode_interaksi', 'Kode Interaksi Obat', 'required');
        $this->form_validation->set_rules('upd_nama_interaksi', 'Kode Interaksi Obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_interaksi_update";
            $comments = "Gagal update interaksi_obat dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_interaksi = $this->input->post('upd_id_interaksi',TRUE);
            $kode_interaksi = $this->input->post('upd_kode_interaksi', TRUE);
            $nama_interaksi = $this->input->post('upd_nama_interaksi', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_interaksi_obat = array(
                'kode_interaksi' => $kode_interaksi,
                'nama_interaksi' => $nama_interaksi,
                'keterangan' => $keterangan
            );

            $update = $this->Interaksi_obat_model->update_interaksi_obat($data_interaksi_obat, $id_interaksi);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'interaksi_obat berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_interaksi_update";
                $comments = "Berhasil mengubah interaksi_obat  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah interaksi_obat , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_interaksi_update";
                $comments = "Gagal mengubah interaksi_obat  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_interaksi_obat(){
        $is_permit = $this->aauth->control_no_redirect('master_data_interaksi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_interaksi = $this->input->post('id_interaksi', TRUE);

        $delete = $this->Interaksi_obat_model->delete_interaksi_obat($id_interaksi);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'interaksi_obat berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_interaksi_delete";
            $comments = "Berhasil menghapus interaksi_obat dengan id interaksi_obat = '". $id_interaksi ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_interaksi_delete";
            $comments = "Gagal menghapus data interaksi_obat dengan ID = '". $id_interaksi ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
