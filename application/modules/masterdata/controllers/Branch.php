<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Branch_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_branch_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_branch_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_branch', $this->data);
    }

    public function ajax_list_branch(){
        $list = $this->Branch_model->get_branch_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $branch){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $branch->nama;
            $row[] = $branch->alamat;
            $row[] = $branch->pic;
            $row[] = $branch->no_kontak;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_branch" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit branch" onclick="editBranch('."'".$branch->id_branch."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus branch" id="hapusBranch" onclick="hapusBranch('."'".$branch->id_branch."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Branch_model->count_branch_all(),
                    "recordsFiltered" => $this->Branch_model->count_branch_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_branch(){
        $is_permit = $this->aauth->control_no_redirect('master_data_branch_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        // $this->form_validation->set_rules('id_branch', 'id_branch', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('pic', 'pic', 'required');
        $this->form_validation->set_rules('no_kontak', 'No Kontak', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_branch_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            // $id_branch = $this->input->post('id_branch', TRUE);
            $nama = $this->input->post('nama', TRUE);
            $alamat    = $this->input->post('alamat', TRUE);
            $pic    = $this->input->post('pic', TRUE);
            $no_kontak    = $this->input->post('no_kontak', TRUE);
            $data_branch = array(
                
                'nama' => $nama,
                'alamat'         => $alamat,
                'pic' => $pic,
                'no_kontak' => $no_kontak
            );

            $ins = $this->Branch_model->insert_branch($data_branch);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'branch berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_branch_create";
                $comments = "Berhasil menambahkan branch dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan branch, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_branch_create";
                $comments = "Gagal menambahkan branch dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_branch_by_id(){
        $id_branch = $this->input->get('id_branch',TRUE);
        $old_data = $this->Branch_model->get_branch_by_id($id_branch);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_branch(){
        $is_permit = $this->aauth->control_no_redirect('master_data_branch_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_branch', 'id_branch', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('pic', 'pic', 'required');
        $this->form_validation->set_rules('no_kontak', 'No Kontak', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_branch_update";
            $comments = "Gagal update branch dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_branch = $this->input->post('id_branch', TRUE);
            $nama = $this->input->post('nama', TRUE);
            $alamat    = $this->input->post('alamat', TRUE);
            $pic    = $this->input->post('pic', TRUE);
            $no_kontak    = $this->input->post('no_kontak', TRUE);

            $data_branch = array(

                'nama' => $nama,
                'alamat'         => $alamat,
                'pic' => $pic,
                'no_kontak' => $no_kontak
            );

            $update = $this->Branch_model->update_branch($data_branch, $id_branch);
            // print_r($data_branch);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'branch berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_branch_update";
                $comments = "Berhasil mengubah branch  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah branch , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_branch_update";
                $comments = "Gagal mengubah branch  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_branch(){
        $is_permit = $this->aauth->control_no_redirect('master_data_branch_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_branch = $this->input->post('id_branch', TRUE);

        $delete = $this->Branch_model->delete_branch($id_branch);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'branch berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_branch_delete";
            $comments = "Berhasil menghapus branch dengan id branch = '". $id_branch ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_branch_delete";
            $comments = "Gagal menghapus data branch dengan ID = '". $id_branch ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
