<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }

        $this->data['list_peruntukan']  = array('dewasa'=>'Dewasa','anak'=>'Anak anak','dewasa/anak'=>'Dewasa/Anak anak');
        $this->data['list_kelamin']  = array('laki'=>'Laki laki','perempuan'=>'Perempuan','laki/perempuan' => 'Laki laki/Perempuan');
        $this->data['list_status_kamar']  = array('1'=>'SIAP','2'=>'Proses Sterilisasi','3' => 'Proses Pembersihan','4'=>'Perbaikan','5' => 'Renovasi','6'=>'Tidak Berfungsi');
        $this->data['list_lantai']  = array('1'=>'Lantai 1','2'=>'Lantai 2','3' => 'Lantai 3');

        $this->load->model('Menu_model');
        $this->load->model('Kamar_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kamar_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_kamar_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kamar', $this->data);
    }
    function get_pelayanan_list(){
        $ruangan_id = $this->input->get('ruangan_id',TRUE);
        $list_pelayanan = $this->Kamar_model->get_kelas_pelayanan($ruangan_id);
        $list = "<option value=\"\" disabled selected>Pilih Kelas Pelayanan</option>";
        foreach($list_pelayanan as $list_pelayan){
            $list .= "<option value='".$list_pelayan->kelaspelayanan_id."'>".$list_pelayan->kelaspelayanan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }
    public function ajax_list_kamar(){
        $list = $this->Kamar_model->get_kamar_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kamar){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kamar->kelaspelayanan_nama;
            $row[] = $kamar->nama_poliruangan;
            $row[] = $kamar->no_kamar;
            $row[] = $kamar->no_bed;
            $row[] = ($kamar->status_bed=='1' ? 'Ya' : 'Tidak');
            $row[] = $kamar->status_aktif =='1' ? 'Aktif' : 'Tidak';
            $row[] = $kamar->usia;
            $row[] = $kamar->jenis_kelamin;
            $row[] = $kamar->lokasi;
            $row[] = $kamar->instalasi;
            $row[] = $kamar->penanggung_jawab;
            $row[] = $kamar->status;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_kamar" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit kamar" id="editKamar" onclick="editKamar('."'".$kamar->kamarruangan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus kamar" id="hapusKamar" onclick="hapusKamar('."'".$kamar->kamarruangan_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kamar_model->count_kamar_all(),
                    "recordsFiltered" => $this->Kamar_model->count_kamar_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_kamar(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kamar_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }


        $this->load->library('form_validation');
        $this->form_validation->set_rules('ruangan', 'Ruangan', 'required');
        $this->form_validation->set_rules('kelas_pelayanan', 'Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('nomor_kamar', 'Nomor Kamar', 'required');
        $this->form_validation->set_rules('nomor_bed', 'Nomor Bed', 'required');
        // $this->form_validation->set_rules('status_bed', 'Status Bed', 'required');
        // $this->form_validation->set_rules('status_aktif', 'Status Aktif', 'required');
        $this->form_validation->set_rules('usia', 'Peruntukan', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis kelamin', 'required');
        $this->form_validation->set_rules('lokasi', 'Lokasi Lantai', 'required');
        $this->form_validation->set_rules('instalasi', 'Instalasi', 'required');
        $this->form_validation->set_rules('penanggung_jawab', 'Penanggung Jawab Kamar', 'required');
        $this->form_validation->set_rules('status_kamar', 'Status Kamar', 'required');


        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kamar_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $ruangan = $this->input->post('ruangan', TRUE);
            $kelas_pelayanan = $this->input->post('kelas_pelayanan', TRUE);
            $nomor_kamar = $this->input->post('nomor_kamar', TRUE);
            $nomor_bed = $this->input->post('nomor_bed', TRUE);
            $bed = $this->input->post('status_bed', TRUE);
            $aktif = $this->input->post('status_aktif', TRUE);
            $usia = $this->input->post('usia', TRUE);
            $jenis_kelamin = $this->input->post('jenis_kelamin', TRUE);
            $lokasi = $this->input->post('lokasi', TRUE);
            $instalasi = $this->input->post('instalasi', TRUE);
            $status_kamar = $this->input->post('status_kamar', TRUE);
            $penanggung_jawab = $this->input->post('penanggung_jawab', TRUE);

            $keterangan = $this->input->post('keterangan', TRUE);

            $data_kamar = array(
                'poliruangan_id'      => $ruangan,
                'kelaspelayanan_id'   => $kelas_pelayanan,
                'no_kamar'            => $nomor_kamar,
                'no_bed'              => $nomor_bed,
                'status_bed'          => $bed,
                'status_aktif'        => $aktif,
                'usia'                => $usia,
                'jenis_kelamin'       => $jenis_kelamin,
                'lokasi'              => $lokasi,
                'instalasi'           => $instalasi,
                'penanggung_jawab'    => $penanggung_jawab,
                'status'              => $status_kamar,
                'keterangan'          => $keterangan,
            );

            // print_r($data_kamar);die();
            $ins = $this->Kamar_model->insert_kamar($data_kamar);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kamar berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kamar_create";
                $comments = "Berhasil menambahkan Kamar dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Kamar, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kamar_create";
                $comments = "Gagal menambahkan Kamar dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
     public function ajax_get_kamar_by_id(){
        $kamar_id = $this->input->get('kamar_id',TRUE);
        $old_data = $this->Kamar_model->get_kamar_by_id($kamar_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }
    public function do_delete_kamar(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kamar_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $kamar_id = $this->input->post('kamar_id', TRUE);

        $delete = $this->Kamar_model->delete_kamar($kamar_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Paket berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kamar_delete";
            $comments = "Berhasil menghapus Paket dengan id Paket Operasi = '". $kamar_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kamar_delete";
            $comments = "Gagal menghapus data Paket dengan ID = '". $kamar_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function do_update_kamar(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kamar_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }


        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_ruangan', 'Ruangan', 'required');
        $this->form_validation->set_rules('upd_kelas_pelayanan', 'Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('upd_nomor_kamar', 'Nomor Kamar', 'required');
        $this->form_validation->set_rules('upd_nomor_bed', 'Nomor Bed', 'required');
        // $this->form_validation->set_rules('upd_bed', 'Status Bed', 'required');
        // $this->form_validation->set_rules('upd_aktif', 'Status Aktif', 'required');
        $this->form_validation->set_rules('upd_usia', 'Peruntukan', 'required');
        $this->form_validation->set_rules('upd_jenis_kelamin', 'Jenis kelamin', 'required');
        $this->form_validation->set_rules('upd_instalasi', 'Instalasi', 'required');
        $this->form_validation->set_rules('upd_lokasi', 'Lokasi lantai', 'required');
        $this->form_validation->set_rules('upd_penanggung_jawab', 'Penanggung Jawab Kamar', 'required');
        $this->form_validation->set_rules('upd_status_kamar', 'Status Kamar', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kamar_update";
            $comments = "Gagal update kamar dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $upd_id_kamar   = $this->input->post('upd_id_kamar',TRUE);
            $ruangan = $this->input->post('upd_ruangan', TRUE);
            $kelas_pelayanan = $this->input->post('upd_kelas_pelayanan', TRUE);
            $nomor_kamar = $this->input->post('upd_nomor_kamar', TRUE);
            $nomor_bed = $this->input->post('upd_nomor_bed', TRUE);
            $bed = $this->input->post('upd_status_bed', TRUE);
            $aktif = $this->input->post('upd_status_aktif', TRUE);
            $usia = $this->input->post('upd_usia', TRUE);
            $jenis_kelamin = $this->input->post('upd_jenis_kelamin', TRUE);
            $lokasi = $this->input->post('upd_lokasi', TRUE);
            $instalasi = $this->input->post('upd_instalasi', TRUE);
            $status_kamar = $this->input->post('upd_status_kamar', TRUE);
            $penanggung_jawab = $this->input->post('upd_penanggung_jawab', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_kamar = array(
                'poliruangan_id'      => $ruangan,
                'kelaspelayanan_id'   => $kelas_pelayanan,
                'no_kamar'            => $nomor_kamar,
                'no_bed'              => $nomor_bed,
                'status_bed'          => $bed,
                'status_aktif'        => $aktif,
                'usia'                => $usia,
                'jenis_kelamin'       => $jenis_kelamin,
                'lokasi'              => $lokasi,
                'instalasi'           => $instalasi,
                'penanggung_jawab'    => $penanggung_jawab,
                'status'              => $status_kamar,
                'keterangan'          => $keterangan,
            );

            $update = $this->Kamar_model->update_kamar($data_kamar, $upd_id_kamar);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kamar berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kamar_update";
                $comments = "Berhasil mengubah kamar dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah Kamar, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kamar_update";
                $comments = "Gagal mengubah kamar dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Kamar");
        $object->getActiveSheet()
                        ->getStyle("A1:N1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:N1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No", "Kelas Pelayanan","Ruangan","No Kamar","No Bed","Status Terpakai","Status Aktif","Peruntukan Usia","Peruntukan Jenis Kelamin","Lokasi","Instalasi","Penanggung Jawab","Status Kamar","keterangan");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Kamar_model->get_kamar_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->kelaspelayanan_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama_poliruangan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->no_kamar);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->no_bed);
            if($row->status_bed == 1){
                $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, "Terpakai");
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, "Tidak");
            }
            if($row->status_aktif == 1){
                $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, "Aktif");
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, "Tidak");
            }
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->usia);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->jenis_kelamin);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->lokasi);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->instalasi);
            $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->penanggung_jawab);
            $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->status);
            $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->keterangan);
            $excel_row++;
        }

        foreach(range('A','N') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Kamar.xlsx"');
        $object_writer->save('php://output');
    }
}
