<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Provinsi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_provinsi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_provinsi_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_provinsi', $this->data);
    }

    public function ajax_list_provinsi(){
        $list = $this->Provinsi_model->get_provinsi_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $provinsi){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $provinsi->propinsi_nama;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_update_provinsi" data-backdrop="static" data-keyboard="false" onclick="editProvinsi('."'".$provinsi->propinsi_id."'".')"><i class="fa fa-pencil"></i></button>
                      <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusProvinsi('."'".$provinsi->propinsi_id."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Provinsi_model->count_provinsi_all(),
                    "recordsFiltered" => $this->Provinsi_model->count_provinsi_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_provinsi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_provinsi_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_provinsi', 'Nama Provinsi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_provinsi_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_provinsi = $this->input->post('nama_provinsi', TRUE);
            $data_provinsi = array(
                'propinsi_nama' => $nama_provinsi,
            );

            $ins = $this->Provinsi_model->insert_provinsi($data_provinsi);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Provinsi berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_provinsi_create";
                $comments = "Berhasil menambahkan Provinsi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Provinsi, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_provinsi_create";
                $comments = "Gagal menambahkan Provinsi dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_provinsi_by_id(){
        $propinsi_id = $this->input->get('propinsi_id',TRUE);
        $old_data = $this->Provinsi_model->get_provinsi_by_id($propinsi_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_provinsi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_provinsi_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_provinsi', 'Nama Provinsi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_provinsi_update";
            $comments = "Gagal update provinsi dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $propinsi_id = $this->input->post('upd_id_provinsi',TRUE);
            $nama_provinsi = $this->input->post('upd_nama_provinsi', TRUE);

            $data_provinsi = array(
                'propinsi_nama' => $nama_provinsi
            );

            $update = $this->Provinsi_model->update_provinsi($data_provinsi, $propinsi_id);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Provinsi berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_provinsi_update";
                $comments = "Berhasil mengubah provinsi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah provinsi , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_provinsi_update";
                $comments = "Gagal mengubah provinsi  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_provinsi(){
        $is_permit = $this->aauth->control_no_redirect('master_data_provinsi_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $propinsi_id = $this->input->post('propinsi_id', TRUE);

        $delete = $this->Provinsi_model->delete_provinsi($propinsi_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Provinsi berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_provinsi_delete";
            $comments = "Berhasil menghapus Provinsi dengan id Provinsi = '". $propinsi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_provinsi_delete";
            $comments = "Gagal menghapus data Provinsi dengan ID = '". $propinsi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
