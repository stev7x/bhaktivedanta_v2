<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auto_stop_obat extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Auto_stop_obat_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_auto_stop_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_auto_stop_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_auto_stop_obat', $this->data);
    }

    public function ajax_list_auto_stop(){
        $list = $this->Auto_stop_obat_model->get_auto_stop_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $auto_stop){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $auto_stop->nama_obat;
            $row[] = $auto_stop->lama_pemakaian;
            $row[] = $auto_stop->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_auto_stop_obat" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit auto_stop" onclick="editAutoStopObat('."'".$auto_stop->id_auto_stop."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus auto_stop" id="hapusAutoStopObat" onclick="hapusAutoStopObat('."'".$auto_stop->id_auto_stop."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Auto_stop_obat_model->count_auto_stop_all(),
                    "recordsFiltered" => $this->Auto_stop_obat_model->count_auto_stop_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_auto_stop(){
        $is_permit = $this->aauth->control_no_redirect('master_data_auto_stop_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_obat', 'Obat', 'required');
        $this->form_validation->set_rules('lama_pemakaian', 'Lama Pemakaian', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_auto_stop_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $id_obat = $this->input->post('id_obat', TRUE);
            $lama_pemakaian = $this->input->post('lama_pemakaian', TRUE);
            $keterangan    = $this->input->post('keterangan', TRUE);
            $data_auto_stop = array(
                'id_obat' => $id_obat,
                'lama_pemakaian' => $lama_pemakaian,
                'keterangan'         => $keterangan,
            );

            $ins = $this->Auto_stop_obat_model->insert_auto_stop($data_auto_stop);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'auto_stop berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_auto_stop_create";
                $comments = "Berhasil menambahkan auto_stop dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan auto_stop, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_auto_stop_create";
                $comments = "Gagal menambahkan auto_stop dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_auto_stop_by_id(){
        $id_auto_stop = $this->input->get('id_auto_stop',TRUE);
        $old_data = $this->Auto_stop_obat_model->get_auto_stop_by_id($id_auto_stop);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_auto_stop(){
        $is_permit = $this->aauth->control_no_redirect('master_data_auto_stop_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_id_obat', 'Obat', 'required');
        $this->form_validation->set_rules('upd_lama_pemakaian', 'Lama Pemakaian', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_auto_stop_update";
            $comments = "Gagal update auto_stop dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_auto_stop = $this->input->post('upd_id_auto_stop',TRUE);
            $id_obat = $this->input->post('upd_id_obat', TRUE);
            $lama_pemakaian = $this->input->post('upd_lama_pemakaian', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_auto_stop = array(
                'id_obat' => $id_obat,
                'lama_pemakaian' => $lama_pemakaian,
                'keterangan' => $keterangan
            );

            $update = $this->Auto_stop_obat_model->update_auto_stop($data_auto_stop, $id_auto_stop);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'auto_stop berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_auto_stop_update";
                $comments = "Berhasil mengubah auto_stop  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah auto_stop , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_auto_stop_update";
                $comments = "Gagal mengubah auto_stop  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_auto_stop(){
        $is_permit = $this->aauth->control_no_redirect('master_data_auto_stop_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_auto_stop = $this->input->post('id_auto_stop', TRUE);

        $delete = $this->Auto_stop_obat_model->delete_auto_stop($id_auto_stop);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'auto_stop berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_auto_stop_delete";
            $comments = "Berhasil menghapus auto_stop dengan id auto_stop = '". $id_auto_stop ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_auto_stop_delete";
            $comments = "Gagal menghapus data auto_stop dengan ID = '". $id_auto_stop ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
