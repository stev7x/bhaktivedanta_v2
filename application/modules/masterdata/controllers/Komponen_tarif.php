<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komponen_tarif extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Komponen_Tarif_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_komponen_tarif_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_komponen_tarif_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('v_komponen_tarif', $this->data);
    }

    public function ajax_list_komponen_tarif(){

        $list = $this->Komponen_Tarif_model->get_komponen_tarif_list();
         
        $change_ad = "";
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;

        foreach($list as $komponentarif){
            $no++;
            switch ($komponentarif->allow_downpayment) {
                case 0:
                    $change_ad = "Tidak Bisa Downpayment";
                    break;
                case 1:
                    $change_ad = "Bisa Downpayment";
                    break;
                default:
                    $change_ad = "Undefined";
                    break;
            }
            $row = array();
            $row[] = $no;
            $row[] = $komponentarif->komponentarif_nama;
            $row[] = $change_ad;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_komponentarif" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit komponentarif" onclick="editKomponentarif('."'".$komponentarif->komponentarif_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus komponentarif" id="hapusKomponentarif" onclick="hapusKomponentarif('."'".$komponentarif->komponentarif_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Komponen_Tarif_model->count_komponentarif_all(),
                    "recordsFiltered" => $this->Komponen_Tarif_model->count_komponentarif_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_komponen_tarif(){
        $is_permit = $this->aauth->control_no_redirect('master_data_komponen_tarif_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('komponentarif_nama', 'Nama Komponen Tarif', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_komponen_tarif_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $komponentarif_nama = $this->input->post('komponentarif_nama', TRUE);
            $allow_downpayment = $this->input->post('allow_downpayment', TRUE);

            if ($allow_downpayment == null) {
                $allow_downpayment = 0;
            }else{
                $allow_downpayment = 1;
            }

            $data_komponentarif = array(
                'komponentarif_nama' => $komponentarif_nama,
                'allow_downpayment' => $allow_downpayment,
            );

            $ins = $this->Komponen_Tarif_model->insert_komponentarif($data_komponentarif);
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Komponen Tarif berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_komponen_tarif_create";
                $comments = "Berhasil menambahkan Komponen Tarif dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Komponen Tarif, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_komponen_tarif_create";
                $comments = "Gagal menambahkan Komponen Tarif dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_komponentarif_by_id(){
        $komponentarif_id = $this->input->get('komponentarif_id',TRUE);
        $old_data = $this->Komponen_Tarif_model->get_komponentarif_by_id($komponentarif_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_komponen_tarif(){
        $is_permit = $this->aauth->control_no_redirect('master_data_komponen_tarif_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('komponentarif_nama_update', 'Nama Komponen Tarif', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            $perms = "master_data_komponen_tarif_update";
            $comments = "Gagal update Komponen Tarif dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $komponentarif_id = $this->input->post('komponentarif_id_update', TRUE);
            $komponentarif_nama = $this->input->post('komponentarif_nama_update', TRUE);
            $allow_downpayment = $this->input->post('allow_downpayment_update', TRUE);

            if ($allow_downpayment == null) {
                $allow_downpayment = 0;
            }else if($allow_downpayment == "on"){
                $allow_downpayment = 1;
            }else{
                $allow_downpayment = 0;
            }

            $data_komponentarif = array(
                'komponentarif_nama' => $komponentarif_nama,
                'allow_downpayment' => $allow_downpayment,
            );

            $update = $this->Komponen_Tarif_model->update_komponen_tarif($data_komponentarif, $komponentarif_id);

            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Komponen Tarif berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_komponen_tarif_update";
                $comments = "Berhasil mengubah Komponen Tarif  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah Komponen Tarif , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_komponen_tarif_update";
                $comments = "Gagal mengubah Komponen Tarif  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_komponen_tarif(){
        $is_permit = $this->aauth->control_no_redirect('master_data_komponen_tarif_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $komponentarif_id = $this->input->post('komponentarif_id', TRUE);

        $delete = $this->Komponen_Tarif_model->delete_komponen_tarif($komponentarif_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Komponen Tarif berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_komponen_tarif_delete";
            $comments = "Berhasil menghapus Komponen Tarif dengan id = '". $komponentarif_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_komponen_tarif_delete";
            $comments = "Gagal menghapus data Komponen Tarif dengan id = '". $komponentarif_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
