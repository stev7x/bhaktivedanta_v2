<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sediaan_obat extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Sediaan_obat_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_sediaan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_sediaan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_sediaan_obat', $this->data);
    }

    public function ajax_list_sediaan(){
        $list = $this->Sediaan_obat_model->get_sediaan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $sediaan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $sediaan->kode_sediaan;
            $row[] = $sediaan->nama_sediaan;
            $row[] = $sediaan->keterangan;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_sediaan_obat" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit sediaan" onclick="editSediaan('."'".$sediaan->id_sediaan."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus sediaan" id="hapusSediaan" onclick="hapusSediaan('."'".$sediaan->id_sediaan."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Sediaan_obat_model->count_sediaan_all(),
                    "recordsFiltered" => $this->Sediaan_obat_model->count_sediaan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_sediaan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_sediaan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_sediaan', 'Kode Sediaan', 'required');
        $this->form_validation->set_rules('nama_sediaan', 'Nama Sediaan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_sediaan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $kode_sediaan = $this->input->post('kode_sediaan', TRUE);
            $nama_sediaan = $this->input->post('nama_sediaan', TRUE);
            $keterangan    = $this->input->post('keterangan', TRUE);
            $data_sediaan = array(
                'kode_sediaan' => $kode_sediaan,
                'nama_sediaan' => $nama_sediaan,
                'keterangan'         => $keterangan,
            );

            $ins = $this->Sediaan_obat_model->insert_sediaan($data_sediaan);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'sediaan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_sediaan_create";
                $comments = "Berhasil menambahkan sediaan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan sediaan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_sediaan_create";
                $comments = "Gagal menambahkan sediaan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function ajax_get_sediaan_by_id(){
        $id_sediaan = $this->input->get('id_sediaan',TRUE);
        $old_data = $this->Sediaan_obat_model->get_sediaan_by_id($id_sediaan);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_sediaan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_sediaan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kode_sediaan', 'Kode Sediaan', 'required');
        $this->form_validation->set_rules('upd_nama_sediaan', 'Nama Sediaan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_sediaan_update";
            $comments = "Gagal update sediaan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_sediaan = $this->input->post('upd_id_sediaan',TRUE);
            $kode_sediaan = $this->input->post('upd_kode_sediaan', TRUE);
            $nama_sediaan = $this->input->post('upd_nama_sediaan', TRUE);
            $keterangan = $this->input->post('upd_keterangan', TRUE);

            $data_sediaan = array(
                'kode_sediaan' => $kode_sediaan,
                'nama_sediaan' => $nama_sediaan,
                'keterangan' => $keterangan
            );

            $update = $this->Sediaan_obat_model->update_sediaan($data_sediaan, $id_sediaan);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'sediaan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_sediaan_update";
                $comments = "Berhasil mengubah sediaan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah sediaan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_sediaan_update";
                $comments = "Gagal mengubah sediaan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_sediaan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_sediaan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_sediaan = $this->input->post('id_sediaan', TRUE);

        $delete = $this->Sediaan_obat_model->delete_sediaan($id_sediaan);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'sediaan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_sediaan_delete";
            $comments = "Berhasil menghapus sediaan dengan id sediaan = '". $id_sediaan ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_sediaan_delete";
            $comments = "Gagal menghapus data sediaan dengan ID = '". $id_sediaan ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
}
