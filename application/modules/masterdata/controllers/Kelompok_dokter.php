<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok_dokter extends CI_Controller{
	public $data = array();

	public function __construct(){
		parent::__construct();

		$this->load->library("Aauth");
        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kelompok_dokter_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
				// data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
	}

	public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelompok_dokter_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        $perms = "master_data_kelompok_dokter_view";
        $comments = "Master Data Kelompok Dokter";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('masterdata/kelompok_dokter', $this->data);
	}

	public function ajax_list_kel_dokter(){
        $list = $this->Kelompok_dokter_model->get_kel_dokter_datatables();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kel_dokter){
        	$no++;
        	$row = array();
        	$row[] = $no;
        	$row[] = $kel_dokter->kelompokdokter_nama;
            $row[] = '<a class="btn-floating waves-effect waves-light teal" onclick="editKelDokter('."'".$kel_dokter->kelompokdokter_id."'".')"><i class="mdi-content-create"></i></a>
                      <a class="btn-floating waves-effect waves-light red tooltipped" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusKelDokter('."'".$kel_dokter->kelompokdokter_id."'".')"><i class="mdi-action-delete"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kelompok_dokter_model->count_kel_dokter_all(),
                    "recordsFiltered" => $this->Kelompok_dokter_model->count_kel_dokter_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
	}

    public function ajax_get_kel_dokter_by_id(){
        $kel_dokter_id = $this->input->get('kel_dokter_id',TRUE);
        $old_data = $this->Kelompok_dokter_model->get_kel_dokter_by_id($kel_dokter_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Dokter ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_kel_dokter(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelompok_dokter_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_kel_dokter', 'Nama Kelompok Dokter', 'required');
        $this->form_validation->set_rules('upd_id_kel_dokter', 'ID Kelompok Dokter', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_dokter_update";
            $comments = "Gagal update dokter dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $upd_id_kel_dokter      = $this->input->post('upd_id_kel_dokter',TRUE);
            $upd_nama_kel_dokter    = $this->input->post('upd_nama_kel_dokter', TRUE);

            $data_kel_dokter = array(
                'kelompokdokter_nama'   => $upd_nama_kel_dokter,
            );

            $update = $this->Kelompok_dokter_model->update_kel_dokter($data_kel_dokter, $upd_id_kel_dokter);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kelompok Dokter berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_kelompok_dokter_update";
                $comments = "Berhasil mengubah kelompok dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah kelompok dokter, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelompok_dokter_update";
                $comments = "Gagal mengubah kelompok dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_create_kel_dokter(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelompok_dokter_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_kel_dokter', 'Nama Kelompok Dokter', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_kelompok_dokter_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $nama_kel_dokter = $this->input->post('nama_kel_dokter', TRUE);
            $data_kel_dokter = array(
                'kelompokdokter_nama'    => $nama_kel_dokter,
            );

            $ins = $this->Kelompok_dokter_model->insert_kel_dokter($data_kel_dokter);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Kelompok Dokter berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_kelompok_dokter_create";
                $comments = "Berhasil menambahkan Kelompok Dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Kelompok Dokter, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_kelompok_dokter_create";
                $comments = "Gagal menambahkan Kelompok Dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_kel_dokter(){
        $is_permit = $this->aauth->control_no_redirect('master_data_kelompok_dokter_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $kel_dokter_id = $this->input->post('kel_dokter_id', TRUE);

        $delete = $this->Kelompok_dokter_model->delete_kel_dokter($kel_dokter_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Kelompok Dokter berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_kelompok_dokter_delete";
            $comments = "Berhasil menghapus Kelompok Dokter dengan id = '". $kel_dokter_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data Kelompok Dokter, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_kelompok_dokter_delete";
            $comments = "Gagal menghapus data Kelompok Dokter dengan id = '". $kel_dokter_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

}
