<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_tindakan extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library('excel');

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Daftar_tindakan_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_daftar_tindakan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_daftar_tindakan_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_daftar_tindakan', $this->data);
    }

    public function ajax_list_daftar_tindakan(){
        $list = $this->Daftar_tindakan_model->get_daftar_tindakan_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $daftar_tindakan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $daftar_tindakan->daftartindakan_nama;
            $row[] = $daftar_tindakan->kelompoktindakan_nama;
            $row[] = $daftar_tindakan->kelaspelayanan_nama;
            $row[] = $daftar_tindakan->komponentarif_nama;
            $row[] = $daftar_tindakan->harga_tindakan;
            $row[] = $daftar_tindakan->cyto_tindakan;
            $row[] = $daftar_tindakan->bpjs_non_bpjs;
            //add html for action
            $row[] = '<button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_daftar_tindakan" data-backdrop="static" data-keyboard="false" title="klik untuk mengedit tindakan" id="editDaftarTindakan" onclick="editDaftarTindakan('."'".$daftar_tindakan->daftartindakan_id."'".')"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus tindakan" id="hapusDaftarTindakan"onclick="hapusDaftarTindakan('."'".$daftar_tindakan->daftartindakan_id."'".')"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Daftar_tindakan_model->count_daftar_tindakan_all(),
                    "recordsFiltered" => $this->Daftar_tindakan_model->count_daftar_tindakan_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_daftar_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_daftar_tindakan_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        // $this->form_validation->set_rules('nama_daftar_tindakan', 'Nama Tindakan', 'required');
        $this->form_validation->set_rules('kelompoktindakan', 'Kelompok Tindakan', 'required');
        $this->form_validation->set_rules('kelaspelayanan_nama', 'Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('icd9_cm_id', 'ICD 9', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_daftar_tindakan_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $list_nama_daftar_tindakan = $this->input->post('list_nama_daftar_tindakan', TRUE);
            $nama_daftar_tindakan = $this->input->post('nama_daftar_tindakan', TRUE);
            $kelompoktindakan = $this->input->post('kelompoktindakan', TRUE);
            $kelaspelayanan_nama = $this->input->post('kelaspelayanan_nama', TRUE);
            $komponenTarifNamaArray = json_decode($this->input->post("komponenTarifNama"));
            $bhpArray = json_decode($this->input->post("bhpArray"));
            $icd9_cm_id = $this->input->post('icd9_cm_id', TRUE);

            if(!empty($list_nama_daftar_tindakan)){
                $data = $this->Daftar_tindakan_model->get_daftar_tindakan_by_id($list_nama_daftar_tindakan);
                $nama = $data->daftartindakan_nama;
            }else{
                $nama = $nama_daftar_tindakan;
            }

            $sumBPJSCyto = 0;
            $sumBPJSNonCyto = 0;
            $sumUmumCyto = 0;
            $sumUmumNonCyto = 0;

            if (count($komponenTarifNamaArray) > 0) {
                foreach($komponenTarifNamaArray as $list) {
                    $sumBPJSCyto += $list->harga_bpjs_cyto;
                    $sumBPJSNonCyto += $list->harga_bpjs_non_cyto;
                    $sumUmumCyto += $list->harga_umum_cyto;
                    $sumUmumNonCyto += $list->harga_umum_non_cyto;
                }
            }

            $data_daftar_tindakan = array(
                'daftartindakan_nama'   => $nama,
                'kelompoktindakan_id'   => $kelompoktindakan,
                'kelaspelayanan_id'     => $kelaspelayanan_nama,
                'icd9_cm_id'            => $icd9_cm_id,
                'total_harga_bpjs_cyto' => $sumBPJSCyto,
                'total_harga_bpjs_non_cyto' => $sumBPJSNonCyto,
                'total_harga_umum_cyto'     => $sumUmumCyto,
                'total_harga_umum_non_cyto' => $sumUmumNonCyto
            );

            $ins = $this->Daftar_tindakan_model->insert_daftar_tindakan($data_daftar_tindakan);
            //$ins=true;
            if($ins > 0){

                if (count($komponenTarifNamaArray) > 0) {
                    foreach($komponenTarifNamaArray as $list) {
                        $komponentarif_id = $this->Daftar_tindakan_model->get_komponen_tarif_by_name($list->nama);
                        $harga_bpjs_cyto = $list->harga_bpjs_cyto;
                        $harga_bpjs_non_cyto = $list->harga_bpjs_non_cyto;
                        $harga_umum_cyto = $list->harga_umum_cyto;
                        $harga_umum_non_cyto = $list->harga_umum_non_cyto;

                        $dataKomponenTarif = array(
                            'daftar_tindakan_id'    => $ins,
                            'komponentarif_id'      => $komponentarif_id,
                            'harga_bpjs_cyto'       => $harga_bpjs_cyto,
                            'harga_bpjs_non_cyto'   => $harga_bpjs_non_cyto,
                            'harga_umum_cyto'       => $harga_umum_cyto,
                            'harga_umum_non_cyto'   => $harga_umum_non_cyto
                        );

                        $this->Daftar_tindakan_model->insertKomponenTarif($dataKomponenTarif);
                    }
                }

                if (count($bhpArray) > 0) {
                    foreach($bhpArray as $list) {
                        $dataBHP = array(
                            'barang_farmasi_id' => $list->pilih_bhp->id,
                            'jumlah'            => $list->jumlah_bhp,
                            'sediaan_obat_id'   => $list->satuan_bhp->id,
                            'daftar_tindakan_id'=> $ins
                        );

                        $this->Daftar_tindakan_model->insertBHP($dataBHP);
                    }
                }

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Daftar tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_daftar_tindakan_create";
                $comments = "Berhasil menambahkan Daftar_tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Daftar tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_daftar_tindakan_create";
                $comments = "Gagal menambahkan Daftar tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }

    function convert_to_number($rupiah){
       return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    }

    public function ajax_get_daftar_tindakan_by_id(){
        $daftartindakan_id = $this->input->get('daftartindakan_id',TRUE);
        $data = array();
        $data['daftar_tindakan'] = $this->Daftar_tindakan_model->get_daftar_tindakan_by_id($daftartindakan_id);
        $data['daftar_tindakan_bhp'] = $this->Daftar_tindakan_model->get_daftar_tindakan_bhp($daftartindakan_id);
        $data['daftar_tindakan_komponen_tarif'] = $this->Daftar_tindakan_model->get_daftar_tindakan_komponentarif($daftartindakan_id);
        if(count($data['daftar_tindakan']) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Produk ID tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_update_daftar_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_daftar_tindakan_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_kelompoktindakan', 'Kelompok Tindakan', 'required');
        $this->form_validation->set_rules('upd_kelaspelayanan_nama', 'Kelas Pelayanan', 'required');
        $this->form_validation->set_rules('upd_icd9_cm_id', 'ICD 9', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_daftar_tindakan_update";
            $comments = "Gagal update daftar tindakan dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $daftartindakan_id = $this->input->post('upd_id_daftar_tindakan',TRUE);
            $nama_daftar_tindakan = $this->input->post('upd_nama_daftar_tindakan', TRUE);
            $kelompoktindakan = $this->input->post('upd_kelompoktindakan', TRUE);
            $kelaspelayanan_nama = $this->input->post('upd_kelaspelayanan_nama', TRUE);
            $komponenTarifNamaArray = json_decode($this->input->post("komponenTarifNama"));
            $bhpArray = json_decode($this->input->post("bhpArray"));
            $icd9_cm_id = $this->input->post('upd_icd9_cm_id', TRUE);

            $sumBPJSCyto = 0;
            $sumBPJSNonCyto = 0;
            $sumUmumCyto = 0;
            $sumUmumNonCyto = 0;

            if (count($komponenTarifNamaArray) > 0) {
                foreach($komponenTarifNamaArray as $list) {
                    $sumBPJSCyto += $list->harga_bpjs_cyto;
                    $sumBPJSNonCyto += $list->harga_bpjs_non_cyto;
                    $sumUmumCyto += $list->harga_umum_cyto;
                    $sumUmumNonCyto += $list->harga_umum_non_cyto;
                }
            }

            $data_daftar_tindakan = array(
                'daftartindakan_nama'   => $nama_daftar_tindakan,
                'kelompoktindakan_id'   => $kelompoktindakan,
                'kelaspelayanan_id'     => $kelaspelayanan_nama,
                'icd9_cm_id'            => $icd9_cm_id,
                'total_harga_bpjs_cyto' => $sumBPJSCyto,
                'total_harga_bpjs_non_cyto' => $sumBPJSNonCyto,
                'total_harga_umum_cyto'     => $sumUmumCyto,
                'total_harga_umum_non_cyto' => $sumUmumNonCyto
            );

            $update = $this->Daftar_tindakan_model->update_daftar_tindakan($data_daftar_tindakan, $daftartindakan_id);
            //$ins=true;
            if($update){
                if (count($komponenTarifNamaArray) > 0) {
                    foreach($komponenTarifNamaArray as $list) {
                        $komponentarif_id = $this->Daftar_tindakan_model->get_komponen_tarif_by_name($list->nama);
                        $harga_bpjs_cyto = $list->harga_bpjs_cyto;
                        $harga_bpjs_non_cyto = $list->harga_bpjs_non_cyto;
                        $harga_umum_cyto = $list->harga_umum_cyto;
                        $harga_umum_non_cyto = $list->harga_umum_non_cyto;

                        $dataKomponenTarif = array(
                            'harga_bpjs_cyto'       => $harga_bpjs_cyto,
                            'harga_bpjs_non_cyto'   => $harga_bpjs_non_cyto,
                            'harga_umum_cyto'       => $harga_umum_cyto,
                            'harga_umum_non_cyto'   => $harga_umum_non_cyto
                        );

                        $this->Daftar_tindakan_model->updateKomponenTarif($dataKomponenTarif, $daftartindakan_id, $komponentarif_id);
                    }
                }

                if (count($bhpArray) > 0) {
                    foreach($bhpArray as $list) {
                        $dataBHP = array(
                            'barang_farmasi_id' => $list->pilih_bhp->id,
                            'jumlah'            => $list->jumlah_bhp,
                            'sediaan_obat_id'   => $list->satuan_bhp->id
                        );
                        $this->Daftar_tindakan_model->updateBHP($dataBHP, $daftartindakan_id);
                    }
                }

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Daftar tindakan berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_daftar_tindakan_update";
                $comments = "Berhasil mengubah daftar_tindakan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah daftar tindakan , hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_daftar_tindakan_update";
                $comments = "Gagal mengubah daftar tindakan  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_daftar_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('master_data_daftar_tindakan_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $daftartindakan_id = $this->input->post('daftartindakan_id', TRUE);

        $delete = $this->Daftar_tindakan_model->delete_daftar_tindakan($daftartindakan_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Daftar tindakan berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_daftar_tindakan_delete";
            $comments = "Berhasil menghapus Daftar_tindakan dengan id Daftar_tindakan = '". $daftartindakan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_daftar_tindakan_delete";
            $comments = "Gagal menghapus data Daftar tindakan dengan ID = '". $daftartindakan_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Daftar Tindakan");
        $object->getActiveSheet()
                        ->getStyle("A1:H1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:H1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No","Nama Tindakan","Kelompok Tindakan","Pelayanan","Komponen Tarif","Harga Tindakan","Cyto Tindakan","BPJS");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Daftar_tindakan_model->get_daftar_tindakan_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            // $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->id_M_DOKTER);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->daftartindakan_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->kelompoktindakan_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->kelaspelayanan_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->komponentarif_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->harga_tindakan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->cyto_tindakan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->bpjs_non_bpjs);
            $excel_row++;
        }

         foreach(range('A','H') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Daftar Tindakan.xlsx"');
        $object_writer->save('php://output');
    }

    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType = PHPExcel_IOFactory::identify($input_file_name);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel= $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {

                $daftartindakan_nama= $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1
                $kelompoktindakan_id= $objWorksheet->getCellByColumnAndRow(2,$row)->getValue(); //Excel Column 2
                $kelaspelayanan_id  = $objWorksheet->getCellByColumnAndRow(3,$row)->getValue(); //Excel Column 3
                $komponentarif_id   = $objWorksheet->getCellByColumnAndRow(4,$row)->getValue(); //Excel Column 4
                $harga_tindakan     = $objWorksheet->getCellByColumnAndRow(5,$row)->getValue(); //Excel Column 5
                $cyto_tindakan      = $objWorksheet->getCellByColumnAndRow(6,$row)->getValue(); //Excel Column 6
                $bpjs_non_bpjs      = $objWorksheet->getCellByColumnAndRow(7,$row)->getValue(); //Excel Column 7
                $discount           = $objWorksheet->getCellByColumnAndRow(8,$row)->getValue(); //Excel Column 8

                $data = array(
                    // 'agama_id' => $agama_id,
                    'daftartindakan_nama'   => $daftartindakan_nama,
                    'kelompoktindakan_id'   => $kelompoktindakan_id,
                    'kelaspelayanan_id'     => $kelaspelayanan_id,
                    'komponentarif_id'      => $komponentarif_id,
                    'harga_tindakan'        => $harga_tindakan,
                    'cyto_tindakan'         => $cyto_tindakan,
                    'bpjs_non_bpjs'         => $bpjs_non_bpjs,
                    'discount '             => $discount,
                );

                $this->db->insert("m_tindakan", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);



        }

    }

    
}
