<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library("excel");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Dokter_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('master_data_dokter_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "master_data_dokter_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_dokter', $this->data);
    }

    public function ajax_list_dokter(){
        $list = $this->Dokter_model->get_dokter_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $dokter){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $dokter->NAME_DOKTER;
            $row[] = $dokter->ALAMAT;
            $row[] = $dokter->kelompokdokter_nama;
            // $row[] = $dokter->jumlah_pasien;
            //add html for action
            $row[] = '<a class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_edit_dokter" data-backdrop="static" data-keyboard="false" onclick="editDokter('."'".$dokter->id_M_DOKTER."'".')"><i class="fa fa-pencil"></i></a>
                      <a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="hapusDokter('."'".$dokter->id_M_DOKTER."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Dokter_model->count_dokter_all(),
                    "recordsFiltered" => $this->Dokter_model->count_dokter_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    public function do_update_dokter(){
        $is_permit = $this->aauth->control_no_redirect('master_data_dokter_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_nama_dokter', 'Nama Dokter', 'required');
        $this->form_validation->set_rules('upd_alamat', 'Alamat Dokter', 'required');
        $this->form_validation->set_rules('upd_id_dokter', 'ID Dokter', 'required');
        $this->form_validation->set_rules('upd_kel_dokter', 'Kelompok Dokter', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_dokter_update";
            $comments = "Gagal update dokter dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $upd_id_dokter      = $this->input->post('upd_id_dokter',TRUE);
            $upd_nama_dokter    = $this->input->post('upd_nama_dokter', TRUE);
            $upd_alamat         = $this->input->post('upd_alamat', TRUE);
            $upd_kel_dokter_id  = $this->input->post('upd_kel_dokter', TRUE);
            // $upd_jumlah_pasien  = $this->input->post('upd_jumlah_pasien', TRUE);

            $data_dokter = array(
                'NAME_DOKTER'       => $upd_nama_dokter,
                'ALAMAT'            => $upd_alamat,
                'kelompokdokter_id' => $upd_kel_dokter_id,
                // 'jumlah_pasien'     => $upd_jumlah_pasien
            );

            $update = $this->Dokter_model->update_dokter($data_dokter, $upd_id_dokter);
            //$ins=true;
            if($update){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Dokter berhasil diubah'
                );

                // if permitted, do logit
                $perms = "master_data_dokter_update";
                $comments = "Berhasil mengubah dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal mengubah dokter, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_dokter_update";
                $comments = "Gagal mengubah dokter dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
    public function do_create_dokter(){
        $is_permit = $this->aauth->control_no_redirect('master_data_dokter_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_dokter', 'Nama Dokter', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat Dokter', 'required');
        $this->form_validation->set_rules('kel_dokter', 'Kelompok Dokter', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "master_data_dokter_create";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $NAME_DOKTER    = $this->input->post('nama_dokter', TRUE);
            $ALAMAT         = $this->input->post('alamat', TRUE);
            $kel_dokter_id  = $this->input->post('kel_dokter', TRUE);
            // $jumlah_pasien  = $this->input->post('jumlah_pasien', TRUE);

            $data_dokter = array(
                'NAME_DOKTER'    => $NAME_DOKTER,
                'ALAMAT'   => $ALAMAT,
                'kelompokdokter_id' => $kel_dokter_id,
                // 'jumlah_pasien'     => $jumlah_pasien
            );

            $ins = $this->Dokter_model->insert_dokter($data_dokter);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Dokterr berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "master_data_dokter_create";
                $comments = "Berhasil menambahkan Kamar dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Kamar, hubungi web administrator.');

                // if permitted, do logit
                $perms = "master_data_dokter_create";
                $comments = "Gagal menambahkan Kamar dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
     public function ajax_get_dokter_by_id(){
        $dokter_id = $this->input->get('dokter_id',TRUE);
        $old_data = $this->Dokter_model->get_dokter_by_id($dokter_id);

        if(count($old_data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $old_data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Dokter ID tidak ditemukan");
        }

        echo json_encode($res);
    }
    public function do_delete_dokter(){
        $is_permit = $this->aauth->control_no_redirect('master_data_dokter_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $dokter_id = $this->input->post('dokter_id', TRUE);

        $delete = $this->Dokter_model->delete_dokter($dokter_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Dokter berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "master_data_dokter_delete";
            $comments = "Berhasil menghapus Dokter dengan id Dokter Operasi = '". $dokter_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "master_data_dokter_delete";
            $comments = "Gagal menghapus data Dokter dengan ID = '". $dokter_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);

    }

    function exportToExcel(){
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Dokter");
        $object->getActiveSheet()
                        ->getStyle("A1:D1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:D1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("No","Nama Dokter","Alamat","Kelompok Dokter");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Dokter_model->get_dokter_list_2();

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->NAME_DOKTER);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->ALAMAT);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->kelompokdokter_nama);
            $excel_row++;
        }

        foreach(range('A','D') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Dokter.xlsx"');
        $object_writer->save('php://output');
    }

    public function upload(){
        // $tes = $this->input->post('tes', TRUE);
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './temp_upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->do_upload('file');

        $upload_data = $this->upload->data();

        if (!$upload_data){
            $this->upload->display_errors();
            echo "error";
        }else{
            // echo "true";

            $file_name = $upload_data['file_name'];

            echo "data : " . $file_name;
            // print_r("hasil : " .$file_name);die();
            $input_file_name = './temp_upload/'.$file_name;

            try{
                echo "berhassil";
                $inputFileType = PHPExcel_IOFactory::identify($input_file_name);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                // $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel= $objReader->load(FCPATH.'temp_upload/'.$file_name);



            } catch(Exception $e){
                echo "gagal";
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);

            for ($row = 2; $row <= $totalrows; $row++) {

                $NAME_DOKTER            = $objWorksheet->getCellByColumnAndRow(1,$row)->getValue(); //Excel Column 1
                $ALAMAT                 = $objWorksheet->getCellByColumnAndRow(2,$row)->getValue(); //Excel Column 2
                $kelompokdokter_id      = $objWorksheet->getCellByColumnAndRow(3,$row)->getValue(); //Excel Column 3


                $data = array(
                    // 'agama_id' => $agama_id,
                    'NAME_DOKTER'           => $NAME_DOKTER,
                    'ALAMAT'                => $ALAMAT,
                    'kelompokdokter_id'     => $kelompokdokter_id
                );

                $this->db->insert("m_dokter", $data);
            }

            echo "berhasil insert";

            // delete file
            $path = './temp_upload/' . $file_name;
            unlink($path);



        }

    }

}
