<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_management extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('User_management_model');
        $this->load->model('Models');
        $data_menu = $this->Models->query("select z.* from (select * from aauth_perms where name like '%_view') x, aauth_perm_to_group y, aauth_role z where x.id = y.perm_id and x.role_id = z.role_id and y.group_id = 1 order by z.order_layout asc;");

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
        $this->data['all_menu'] = $data_menu;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('configuration_user_management_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "configuration_user_management_view";
        $comments = "Listing User.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('user_management', $this->data);
    }

    public function ajax_list_user(){
        $list = $this->User_management_model->get_user_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $user){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $user->nama_lengkap;
            $row[] = $user->alamat;
            $row[] = $user->email;
            $row[] = $user->username;
            //add html for action
//            '<a class="btn btn-warning btn-circle "onclick="editUser('."'".$user->id."'".')"><i class="fa fa-pencil"></i></a>
            $row[] = '<a class="btn btn-danger btn-circle " data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="deleteUser('."'".$user->id."'".')"><i class="fa fa-trash"></i></a>';
//            $row[] = '<a class="btn tooltipped col s4 l2 " data-position="bottom" data-delay="50" data-tooltip="I am tooltip"> Bottom</a>';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->User_management_model->count_user_all(),
                    "recordsFiltered" => $this->User_management_model->count_user_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_user(){
//        echo "<pre>";
//        print_r($_POST);
//        exit;

        $is_permit = $this->aauth->control_no_redirect('configuration_user_management_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');
        $this->form_validation->set_rules('email', 'Email Account', 'required|valid_email');
        if (count($this->input->post('user_access_permission',TRUE)) == 0) {
            $this->form_validation->set_rules('user_access_permission', 'User Access Permission', 'required');
        }

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "configuration_user_management_create";
            $comments = "Failed to Create a new user with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        } else if ($this->form_validation->run() == TRUE)  {
            $nama_lengkap = $this->input->post('nama_lengkap',TRUE);
            $alamat = $this->input->post('alamat',TRUE);
            $email = $this->input->post('email',TRUE);
            $pass = $this->input->post('password',TRUE);
            $username = $this->input->post('username',TRUE);
            $group = $this->input->post('select_profile',TRUE) ? $this->input->post('select_profile',TRUE) : "1";
            $user_access = json_encode($this->input->post('user_access_permission',TRUE), TRUE);
            $insertuser = $this->aauth->create_user($nama_lengkap, $email, $pass, $alamat, $username);

            if($insertuser){
                $data = array(
                    'user_id' => $insertuser,
                    'group_id' => $group
                );
                $ins_togroup = $this->User_management_model->add_user_to_group($data);
                if($ins_togroup){
                    if ($this->Models->update2("aauth_users", array('user_access_permission' => $user_access), $insertuser)) {
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => true,
                            'messages' => 'User has been saved to database'
                        );

                        // if permitted, do logit
                        $perms = "configuration_user_management_create";
                        $comments = "Success to Create a new user with post data = '". json_encode($_REQUEST) ."'.";
                        $this->aauth->logit($perms, current_url(), $comments);
                    } else {
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'messages' => 'User Access Failed to Update'
                        );

                        // if permitted, do logit
                        $perms = "configuration_user_management_create";
                        $comments = "Failed to Create a new user with post data = '". json_encode($_REQUEST) ."'.";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }
                } else {
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Failed insert user group to database, please contact web administrator.');

                    // if permitted, do logit
                    $perms = "configuration_user_management_create";
                    $comments = "Failed to Create a new user to group when saving to database with post data = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            } else {
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Failed insert user to database, please contact web administrator.');

                // if permitted, do logit
                $perms = "configuration_user_management_create";
                $comments = "Failed to Create a new user when saving to database with post data = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    public function do_delete_user(){
        $is_permit = $this->aauth->control_no_redirect('configuration_user_management_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $user_id = $this->input->post('user_id',TRUE);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_id', 'User ID', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "configuration_user_management_delete";
            $comments = "Gagal delete user dengan error = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {

            $deleteuser = $this->aauth->delete_user($user_id);
            if($deleteuser){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Berhasil delete group'
                );

                // if permitted, do logit
                $perms = "configuration_user_management_delete";
                $comments = "Berhasil delete User dengan atribut user_id = '". $user_id ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal delete user, hubungi web administrator.');

                // if permitted, do logit
                $perms = "configuration_user_management_delete";
                $comments = "Gagal delete group dengan atribut user_id = '". $user_id ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
}
