<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_management extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Group_management_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('configuration_group_management_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "configuration_group_management_view";
        $comments = "Listing Group.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('group_management', $this->data);
    }

	public function ajax_list_group(){
        $list_group = $this->Group_management_model->get_groups_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list_group as $group){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $group->name;
            $row[] = $group->definition;

			$edit = '<td><button class="btn btn-warning btn-circle" onclick="editGroup(\''.$group->id.'\')"><i class="fa fa-pencil"></i></button>';

			$delete = '<a class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" data-tooltip="I am tooltip" onclick="deleteGroup(\''.$group->id.'\')"><i class="fa fa-trash"></i></a></td>';

            $row[] = $edit."&nbsp;".$delete;
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Group_management_model->count_groups_all(),
                    "recordsFiltered" => $this->Group_management_model->count_groups_filtered(),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

	public function ajax_role_create(){
        $role = $this->Group_management_model->get_list_role();
        if(count($role) > 0){
            foreach ($role as $parent){
                $role_child = $this->Group_management_model->get_list_role($parent->role_id);
                $childs = array();
                if(count($role_child) > 0){
                    foreach ($role_child as $child){
                        $perm_child2 = $this->Group_management_model->get_list_role($child->role_id);
                        $childs2 = array();
                        if(count($perm_child2) > 0){
                            foreach ($perm_child2 as $child2){
                                $childs2[] = array('id'=>$child2->role_id, 'text'=>$child2->role_name, 'state'=>array('selected'=>false, 'opened'=>false));
                            }
                        }
                        $childs[] = array('id'=>$child->role_id, 'text'=>$child->role_name, 'children'=>$childs2, 'state'=>array('selected'=>false, 'opened'=>false));
                    }
                }
                $data_valid[] = array('id'=>$parent->role_id, 'text'=>$parent->role_name, 'children'=>$childs, 'state'=>array('selected'=>false, 'opened'=>false));

            }
            $success = TRUE;
            $messages = 'Ok';
        }else{
            $success = FALSE;
            $data_valid = '';
            $messages = '<h5 class="text-red">Tidak ada data permission</h5>';
        }

        $result = array(
            'success'=>$success,
            'messages'=>$messages,
            'data_valid'=>$data_valid,
        );

        echo json_encode($result);
    }

	public function do_create_group(){
		$is_permit = $this->aauth->control_no_redirect('configuration_group_management_create');
		if(!$is_permit) {
			$res = array(
				'csrfTokenName' => $this->security->get_csrf_token_name(),
				'csrfHash' => $this->security->get_csrf_hash(),
				'success' => false,
				'messages' => $this->lang->line('aauth_error_no_access'));
			echo json_encode($res);
			exit;
		}

		$group_name = $this->input->post('group_name',TRUE);
		$definition = $this->input->post('definition',TRUE);
		$roles = $this->input->post('tree_res',TRUE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('group_name', 'Nama Group', 'required');
		$this->form_validation->set_rules('definition', 'Definition', 'required');

		if ($this->form_validation->run() == FALSE)  {
			$error = validation_errors();
			$res = array(
				'csrfTokenName' => $this->security->get_csrf_token_name(),
				'csrfHash' => $this->security->get_csrf_hash(),
				'success' => false,
				'messages' => $error);

			// if permitted, do logit
			$perms = "configuration_group_management_create";
			$comments = "Gagal menambahkan group dengan errors = '". validation_errors('', "\n") ."'.";
			$this->aauth->logit($perms, current_url(), $comments);
		}else if ($this->form_validation->run() == TRUE)  {

			$insertgroup = $this->aauth->create_group($group_name, $definition);
			if($insertgroup){
				$arr_role = explode(',', $roles);
				if(count($arr_role) > 0){
					$hsl = false;
                    foreach ($arr_role as $role_id){
						$list_perm = $this->Group_management_model->get_perm_id_by_role_id($role_id);
						foreach($list_perm as $perm_id){
							$ins_permtogroup = $this->aauth->insert_perm_to_group($insertgroup, $perm_id->id);
							if($ins_permtogroup){
								$hsl = true;
							}
						}
                    }

                    if($hsl == true){
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => true,
                            'messages' => 'Berhasil menambahkan group baru.'
                        );

                        // if permitted, do logit
                        $perms = "configuration_group_management_create";
                        $comments = "Berhasil menambahkan group baru dengan atribut group_id = '". $insertgroup ."'.";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }else{
                        $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => 'Gagal menambahkan group baru, hubungi web administrator.');

                        // if permitted, do logit
                        $perms = "configuration_group_management_create";
                        $comments = "Gagal menambahkan group baru dengan atribut group_id = '". $insertgroup ."'.";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }
                }else{
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Gagal explode data role id.'
                    );

                    // if permitted, do logit
                    $perms = "configuration_group_management_create";
                    $comments = "Gagal explode data role id dengan atribut data roles = '". json_encode($roles) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
			}else{
				$res = array(
				'csrfTokenName' => $this->security->get_csrf_token_name(),
				'csrfHash' => $this->security->get_csrf_hash(),
				'success' => false,
				'messages' => 'Gagal menambahkan group baru, hubungi web administrator.');

				// if permitted, do logit
				$perms = "configuration_group_management_create";
				$comments = "Gagal menambahkan group baru dengan atribut group_name = '". $group_name ."' dan definition ".$definition."'.";
				$this->aauth->logit($perms, current_url(), $comments);
			}
		}
		echo json_encode($res);
    }

	public function get_groupby_id(){
        $group_id = $this->input->get('group_id',TRUE);
        $datagroup = $this->Group_management_model->get_group_by_id($group_id);
        if(count($datagroup) > 0){
            $res = array(
                'success' => true,
                'data' => $datagroup,
                'messages' => 'Ok'
            );
        }else{
            $res = array(
                'success' => false,
                'messages' => 'Group ID not exist',
                'data' => null
            );
        }

        echo json_encode($res);
    }

	public function ajax_role_update(){
        $group_id = $this->input->get('group_id',TRUE);
        $perm = $this->Group_management_model->get_list_role();
        if(count($perm) > 0){
            $perm_group = $this->Group_management_model->get_perm_by_group($group_id);
            $perm_on = array();
            foreach ($perm_group as $perm_list){
                $perm_on[] = $perm_list->perm_id;
            }

			$role_on = array();
			if (sizeof($perm_on)>0){
				$arr_role = $this->Group_management_model->get_role_by_perm($perm_on);
				foreach ($arr_role as $role){
					$role_on[] = $role->role_id;
				}
			}

            foreach ($perm as $parent){
                if (in_array($parent->role_id, $role_on)){
                    $checked = true;
                }else{
                    $checked = false;
                }
                $perm_child = $this->Group_management_model->get_list_role($parent->role_id);
                $childs = array();
                if(count($perm_child) > 0){
                    foreach ($perm_child as $child){
                        if (in_array($child->role_id, $role_on)){
                            $checked1 = true;
                        }else{
                            $checked1 = false;
                        }
                        $perm_child2 = $this->Group_management_model->get_list_role($child->role_id);
                        $childs2 = array();
                        if(count($perm_child2) > 0){
                            foreach ($perm_child2 as $child2){
                                if (in_array($child2->role_id, $role_on)){
                                    $checked2 = true;
                                }else{
                                    $checked2 = false;
                                }
                                $childs2[] = array('id'=>$child2->role_id, 'text'=>$child2->role_name, 'state'=>array('selected'=>$checked2, 'opened'=>false));
                            }
                        }
                        $childs[] = array('id'=>$child->role_id, 'text'=>$child->role_name, 'children'=>$childs2, 'state'=>array('selected'=>$checked1, 'opened'=>false));
                    }
                }
                $data_valid[] = array('id'=>$parent->role_id, 'text'=>$parent->role_name, 'children'=>$childs, 'state'=>array('selected'=>$checked, 'opened'=>false));

            }
            $success = TRUE;
            $messages = 'Ok';
        }else{
            $success = FALSE;
            $data_valid = '';
            $messages = '<h5 class="text-red">Tidak ada data permission</h5>';
        }
        $result = array(
            'success'=>$success,
            'messages'=>$messages,
            'data_valid'=>$data_valid,
        );

        echo json_encode($result);
    }

	public function do_update_group(){
        $is_permit = $this->aauth->control_no_redirect('configuration_group_management_update');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

		$group_id = $this->input->post('upd_group_id',TRUE);
		$group_name = $this->input->post('upd_group_name',TRUE);
		$definition = $this->input->post('upd_definition',TRUE);
		$role = $this->input->post('tree_res',TRUE);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('upd_group_name', 'Nama Group', 'required');
        $this->form_validation->set_rules('upd_definition', 'Definisi', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "configuration_group_management_update";
            $comments = "Gagal update group dengan error = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {

            $updategroup = $this->aauth->update_group($group_id, $group_name, $definition);
            if($updategroup){
                $this->Group_management_model->delete_perm_by_group($group_id);
                $arr_role = explode(',', $role);
                if(count($arr_role) > 0){
                    $hsl = false;
                    foreach ($arr_role as $role_id){
						$list_perm = $this->Group_management_model->get_perm_id_by_role_id($role_id);
						foreach($list_perm as $perm_id){
							$ins_permtogroup = $this->aauth->insert_perm_to_group($group_id, $perm_id->id);
							if($ins_permtogroup){
								$hsl = true;
							}
						}
                    }

                    if($hsl == true){
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => true,
                            'messages' => 'Berhasil update group'
                        );

                        // if permitted, do logit
                        $perms = "configuration_group_management_update";
                        $comments = "Berhasil update group dengan atribut group_id = '". $group_id ."'.";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }else{
                        $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => 'Gagal update group, hubungi web administrator.');

                        // if permitted, do logit
                        $perms = "configuration_group_management_update";
                        $comments = "Gagal update group dengan atribut group_id = '". $group_id ."'.";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }
                }else{
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Gagal explode data role id'
                    );

                    // if permitted, do logit
                    $perms = "configuration_group_management_update";
                    $comments = "Gagal explode data role id dengan atribut = '". json_encode($role) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal update group, hubungi web administrator.');

                // if permitted, do logit
                $perms = "configuration_group_management_update";
                $comments = "Gagal update group dengan atribut group_id = '". $group_id ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

	public function do_delete_group(){
        $is_permit = $this->aauth->control_no_redirect('configuration_group_management_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

		$group_id = $this->input->post('group_id',TRUE);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('group_id', 'Group ID', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "configuration_group_management_delete";
            $comments = "Gagal delete group dengan error = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {

            $deletegroup = $this->aauth->delete_group($group_id);
            if($deletegroup){
				$res = array(
					'csrfTokenName' => $this->security->get_csrf_token_name(),
					'csrfHash' => $this->security->get_csrf_hash(),
					'success' => true,
					'messages' => 'Berhasil delete group'
				);

				// if permitted, do logit
				$perms = "configuration_group_management_delete";
				$comments = "Berhasil delete group dengan atribut group_id = '". $group_id ."'.";
				$this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal delete group, hubungi web administrator.');

                // if permitted, do logit
                $perms = "configuration_group_management_delete";
                $comments = "Gagal delete group dengan atribut group_id = '". $group_id ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

}
/* End of file Group_management.php */
