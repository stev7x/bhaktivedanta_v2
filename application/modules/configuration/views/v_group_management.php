<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Group Management</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Configuration</a></li>
                            <li class="active">Group Management</li>
                        </ol>   
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Group Management  
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                   <div class="panel-body">
                                        <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <button  type="button" class="btn btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="add_dokter_ruangan()">Tambah Dokter Ruangan</button>      
                                        </div>        
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />   
                                       <table id="table_dokter_ruangan_list" class="table table-striped dataTable" cellspacing="0">      
                                       <thead>
                                            <tr> 
                                                <th>No</th>
                                                <th>Nama Dokter</th>
                                                <th>Poli Ruangan</th>
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Poli Ruangan</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- modal -->    
<div id="modal_add_dokter_ruangan" class="modal fade"> 
    <div class="modal-dialog modal-lg">  
       <?php echo form_open('#',array('id' => 'fmCreateKelas_poli_ruangan'))?> 
       <div class="modal-content" style="margin-top: 100px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah Dokter Ruangan</h2> 
            </div>     
            <div class="modal-body">
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Dokter<span style="color: red">*</span></label> 
                        <select name="" class="form-control"> 
                            <option value="">Pilih Dokter</option>
                        </select>
                    </div> 
                    <div class="form-group col-md-12">
                        <label>Ruangan<span style="color: red">*</span></label> 
                        <select name="" class="form-control">
                            <option value="">Pilih Ruangan</option>
                        </select>       
                    </div>   
                </div> 

                <div class="row"> 
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="saveKelurahan"><i class="fa fa-floppy-o p-r-10"></i>Simpan</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
 
<!-- modal --> 
<div id="modal_update_dokter_ruangan" class="modal fade bs-example-modal-lg modal-add" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog modal-lg">   
       <?php echo form_open('#',array('id' => 'fmUpdateKelas_pelayanan'))?> 
       <div class="modal-content" style="margin-top: 100px"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">update Kelas Pelayanan</h2> 
            </div>     
            <div class="modal-body">
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Nama Kelas Pelayanan<span style="color: red">*</span></label>
                        <input type="text" id="upd_nama_kelas_pelayanan" name="upd_nama_kelas_pelayanan" class="form-control" value=""> 
                    </div>  
                </div> 

                <div class="row"> 
                    <div class="col-md-12">     
                    <input type="hidden" id="upd_id_kelurahan" name="upd_id_kelurahan" class="validate">
                        <button type="button" class="btn btn-success pull-right" id="updateKelurahan"><i class="fa fa-floppy-o p-r-10"></i>Simpan</button> 
                    </div> 
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
 


<?php $this->load->view('footer');?>
      