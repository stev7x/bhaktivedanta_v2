<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">User Management </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Configuration</a></li>
                            <li class="active">User Management</li>
                        </ol>
                    </div> 
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12"> 
                    <div class="panel panel-info1">        
                                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />  
                            <div class="panel-heading"> User Management 
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                    <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_user" data-backdrop="static" data-keyboard="false">Tambah User</button>
                                     
                                </div>
                                    <div class="row">       
                                    <div class="table-responsive">

                                           <table id="table_user_list" class="responsive-table display" cellspacing="1">
                                                    <thead>
                                                    <tr>
                                                    <th>No</th>
                                                    <th>Nama Pegawai</th>
                                                    <th>Alamat</th>
                                                    <th>Email</th>
                                                    <th>Username</th>
                                                    <th>Action</th>
                                                </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="13">No Data to Display</td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                        <tr>
                                                        <th>No</th>
                                                        <th>Nama Pegawai</th>
                                                        <th>Alamat</th>
                                                        <th>Email</th>
                                                        <th>Username</th>
                                                        <th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                            </table> 
                                      </div>

                                </div>
                            </div>  
                        </div>
                        
                    </div>
                </div>
            </div>
                <!-- Modal  --> 
<div id="modal_add_user" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <?php echo form_open('#',array('id' => 'fmCreateUser' , 'data-toggle="validator"'))?>
       <div class="modal-content"> 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah User</h2> 
            </div>
            <div class="modal-body"> 
                <form data-toggle="validator"> 
                    <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                        <div class="form-group col-md-12">
                            <label for="nama" class="control-label">Nama Lengkap<span style="color: red">*</span></label>
                            <input type="text" id="nama_lengkap" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap" data-error="Please fill out this field." >
                                <div class="help-block with-errors"></div>
                        </div>    
                        <div class="form-group col-md-12">
                            <label>Alamat<span style="color: red">*</span></label>
                            <input type="text" id="alamat" name="alamat" class="form-control" placeholder="Alamat Lengkap" data-error="Please fill out this field." >
                                 <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">  
                                <div class="form-group col-md-6">
                                    <label>Username<span style="color: red">*</span></label>
                                    <input type="text" id="username" name="username" name="" class="form-control" placeholder="Username" data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Email<span style="color: red">*</span></label>
                                    <input type="email" id="email" name="email" class="form-control" placeholder="ex : user@mail.com" data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">  
                                <div class="form-group col-md-6">
                                    <label>Password<span style="color: red">*</span></label>
                                    <input type="password" id="password" name="password" class="form-control" placeholder="Password"data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Confirm Password<span style="color: red">*</span></label>
                                    <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="Confirm Password"data-error="Please fill out this field." >
                                        <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label>User Access Permission <span style="color: red">*</span></label>
                            <div id="box-access" style="border: 1px solid #e4e7ea; padding: 10px 10px 10px 20px;">
                                <div style="margin-bottom: 5px">
                                    <input type="checkbox" class="form-check-input" id="checkAll" onchange="checkAllChange(this);">
                                    <label style='margin-left: 4px;' class="form-check-label" for="checkAll"> Check All</label>
                                </div>

                                <?php
                                    foreach ($all_menu as $key => $value) {
                                        if ($value['parent'] == 0) {
                                            echo "<input type='checkbox' class='form-check-input check' id='".$value['role_id']."' value='".$value['role_id']."' name='user_access_permission[]' onchange='checkT1(this);'/>";
                                            echo "<label style='margin-left: 4px;' class='form-check-label' for='".$value['role_id']."'>".$value['role_name']."</label><br>";

                                            foreach ($all_menu as $keyChild => $valueChild) {
                                                if ($valueChild['parent'] == $value['role_id']) {
                                                    echo "<div style='margin-left: 16px;'>";
                                                    echo "<input type='checkbox' class='form-check-input check' parent='".$value['role_id']."' id='".$valueChild['role_id']."' value='".$valueChild['role_id']."' name='user_access_permission[]' onchange='checkT2(this);'/>";
                                                    echo "<label style='margin-left: 4px;' class='form-check-label' for='".$valueChild['role_id']."'>".$valueChild['role_name']."</label><br>";
                                                    echo "</div>";

                                                    foreach ($all_menu as $keyChild2 => $valueChild2) {
                                                        if ($valueChild2['parent'] == $valueChild['role_id']) {
                                                            echo "<div style='margin-left: 32px;'>";
                                                            echo "<input type='checkbox' class='form-check-input check' parent='".$valueChild['role_id']."' id='".$valueChild2['role_id']."' value='".$valueChild2['role_id']."' name='user_access_permission[]' onchange='checkT3(this);'/>";
                                                            echo "<label style='margin-left: 4px;' class='form-check-label' for='".$valueChild2['role_id']."'>".$valueChild2['role_name']."</label><br>";
                                                            echo "</div>";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                ?>
                            </div>
<!--                            <select class="form-control selectpicker" id="select_profile" name="select_profile">-->
<!--                                    <option value="" disable selected>Pilih Profile</option>-->
<!--                                        --><?php
//                                            $list_profile = $this->User_management_model->get_group_list();
//                                                foreach($list_profile as $list){
//                                                echo "<option value=".$list->id.">".$list->name."</option>";
//                                                    }
//                                        ?>
<!--                            </select> -->
                        </div> 
                </div>          
                <div class="row">
                    <div class="col-md-12"> 
                        <button type="button" class="btn btn-success pull-right" id="saveUser"><i class="fa fa-floppy-o p-r-10"></i>Simpan</button>
                    </div>
                </div> 
                </form> 
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog --> 
</div>
 <!-- /.modal -->

                <!--/row -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class="checkbox checkbox-info">
                                        <input id="checkbox1" type="checkbox" class="fxhdr">
                                        <label for="checkbox1"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox2" type="checkbox" class="fxsdr">
                                        <label for="checkbox2"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme working">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a> 
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url() ?>assets/plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->

           

<?php $this->load->view('footer');?>
      