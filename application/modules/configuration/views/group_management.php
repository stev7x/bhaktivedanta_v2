<?php $this->load->view('header');?>
    
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid"> 
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Group Management</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Configuration</a></li>
                            <li class="active">Group Management</li>
                        </ol>   
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Group Management  
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                            <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                <button  type="button" class="btn btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="add_group()" >Tambah User Group</button>
                                            </div>
                                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />   
                                           <table id="table_group_list" class="table table-striped dataTable" cellspacing="0"> 
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Group</th>
                                                        <th>Definisi</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Group</th>
                                                        <th>Definisi</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>  
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->

<!-- modal -->
<div id="modal_add_group" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog modal-lg">  
       <?php echo form_open('#',array('id' => 'fmCreateGroup'))?>
       <div class="modal-content" style="margin-top:150px;">    
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah User Group</h2> 
            </div>    
            <div class="modal-body" style="height:400px;overflow: auto;">    
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Nama Group<span style="color: red">*</span></label>
                        <input type="text" id="group_name" name="group_name" class="form-control" placeholder="Nama Group">
                    </div>  
                    <div class="form-group col-md-12">
                        <label>Definisi<span style="color: red">*</span></label>
                        <input type="text" id="definition" name="definition" class="form-control" placeholder="Definisi">
                    </div>  
                    <div class="col-md-12">
                        <fieldset style="margin-top: 45px;">
                            <div id="role_list">  
                            </div>
                        </fieldset> 
                    </div>  
                </div> 

                <div class="row">
                    <div class="col-md-12">  
                        <button type="button" class="btn btn-success pull-right" id="saveGroup"><i class="fa fa-floppy-o p-r-10"></i>Simpan</button>
                    </div>
                </div> 
            </div>
         </div>
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
  
 <div id="modal_update_group" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog modal-lg">  
       <?php echo form_open('#',array('id' => 'fmUpdateGroup'))?>
       <div class="modal-content" style="margin-top:150px;">    
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Tambah User Group</h2> 
            </div>    
            <div class="modal-body" style="height:400px;overflow: auto;">    
                <div class="alert alert-success alert-dismissable hide" id="modal_notif">     
                    <button type="button" class="close" data-dismiss="alert" >&times;</button> Data Berhasil di Update. 
                </div>    
                <span style="color: red;">* Wajib diisi</span><br>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Nama Group<span style="color: red">*</span></label>
                        <input type="hidden" name="upd_group_id" id="upd_group_id" readonly>
                        <input type="text" id="upd_group_name" name="upd_group_name" class="form-control" placeholder="Nama Group">
                    </div>    
                    <div class="form-group col-md-12">
                        <label>Definisi<span style="color: red">*</span></label>
                        <input type="text" id="upd_definition" name="upd_definition" class="form-control" placeholder="Definisi">
                    </div>  
                    <div class="col-md-12">
                        <fieldset style="margin-top: 45px;">
                            <div id="upd_role_list">
                            </div>
                        </fieldset>
                    </div>  
                </div> 

                <div class="row">
                    <div class="col-md-12">   
                        <button type="button" class="btn btn-success pull-right" id="updateGroup"><i class="fa fa-floppy-o p-r-10"></i>Simpan</button> 
                    </div>
                </div> 
            </div>
         </div> 
         <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
     <!-- /.modal-dialog -->
</div>
 <!-- /.modal -->  
 
<?php $this->load->view('footer');?>
      