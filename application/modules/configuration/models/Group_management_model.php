<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_management_model extends CI_Model {
    var $column = array('name','name','definition');
    var $order = array('name' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
	function get_groups_list(){
        $this->db->from('aauth_groups');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function count_groups_all(){
        $this->db->from('aauth_groups');

        return $this->db->count_all_results();
    }
	
	function count_groups_filtered(){
        $this->db->from('aauth_groups');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
	
	function get_list_role($parent = 0){
        $this->db->from('aauth_role');
        $this->db->where('parent',$parent);
        $query = $this->db->get();
        
        return $query->result();
    }
	
	function get_perm_id_by_role_id($role_id){
		$this->db->select('id');
		$this->db->from('aauth_perms');
		$this->db->where('role_id',$role_id);
		$query = $this->db->get();
        
        return $query->result();
	}

	function get_group_by_id($id){
        $query = $this->db->get_where('aauth_groups', array('id' => $id), 1, 0);
        return $query->row();
    }
	
	function get_perm_by_group($group_id){
        $query = $this->db->get_where('aauth_perm_to_group', array('group_id' => $group_id));
        return $query->result();
    }
	
	function get_role_by_perm($arr_perm_id){
		$this->db->select('role_id');
		$this->db->from('aauth_perms');
		$this->db->where_in('id', $arr_perm_id);
		$this->db->group_by('role_id');
        $query = $this->db->get();
		
        return $query->result();
    }
	
	function delete_perm_by_group($group_id){
        $delete = $this->db->delete('aauth_perm_to_group', array('group_id' => $group_id));
        
        return $delete;
    }
		
}
/* End of file Group_management_model.php */