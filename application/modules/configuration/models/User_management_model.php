<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_management_model extends CI_Model {
    var $column = array('id','nama_lengkap','alamat','email','username');
    var $order = array('id' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_user_list(){
        $this->db->from('aauth_users');
        $i = 0;
        $search_value = $this->input->get('search');

        if($search_value){
//            $this->db->group_start();
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
//            $this->db->group_end();
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_user_all(){
        $this->db->from('aauth_users');

        return $this->db->count_all_results();
    }
    
    public function count_user_filtered(){
        $this->db->from('aauth_users');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
//            $this->db->group_start();
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
//            $this->db->group_end();
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function get_group_list(){
         return $this->db->get('aauth_groups')->result();
    }
    
    public function add_user_to_group($data){
        $insert = $this->db->insert('aauth_user_to_group',$data);
        return $insert;
    }
}