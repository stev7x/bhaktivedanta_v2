<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Models extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function formatingData($plain_data)
    {
        foreach ($plain_data as $data) {
            $result[$data['id']] = $data;
        }

        return $result;
    }

    public function all($table)
    {
        return $this->formatingData($this->db->get($table)->result_array());
    }

    public function show($table, $id)
    {
        return $this->db->get_where($table, array('id' => $id))->result_array()[0];
    }

    public function where($table, $wheres=array())
    {
        return $this->db->get_where($table, $wheres)->result_array();
    }

    public function query($query)
    {
        return $this->db->query($query)->result_array();
    }

    public function insert($table, $data)
    {
        $data['created_by'] = 0;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = 0;
        $data['updated_at'] = date('Y-m-d H:i:s');

        return $this->db->insert($table, $data);
    }

    public function update($table, $data, $id)
    {
        $data['updated_by'] = 0;
        $data['updated_at'] = date('Y-m-d H:i:s');

        return $this->db->update($table, $data, array('id' => $id));
    }

    public function update2($table, $data, $id)
    {
        return $this->db->update($table, $data, array('id' => $id));
    }

    public function delete($table, $id)
    {
        return $this->db->delete($table, array('id' => $id));
    }
}