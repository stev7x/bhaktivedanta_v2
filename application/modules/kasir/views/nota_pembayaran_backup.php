
<!DOCTYPE html>
 <html>
<head> 
<title>Nota Pembayaran</title>
<!-- http://localhost/bhaktivedanta/kasir/kasirrj/print_kwitansi/70 -->
<style type="text/css" rel="stylesheet">
      
      body{
        padding-right: 10px;

      }
			table {
				border-collapse: collapse;
				/*text-align: center center;*/
			}
      p{
        line-height: 5%;
        font-size: 16px;
      }
      .judul-invoice {
        font-size:30px;
        font-style:bold;
      }
      .body-invoice {
        font-size:24px;
        font-style:bold;
      }
  
	</style> 
</head> 
   
<body onload="window.print()">
<!-- <body > -->
<table  width="900" border="1" cellpadding="2" align="center">
<tr>
  <td colspan="7"><img src="<?php echo base_url() ?>assets/plugins/images/header.png" alt=""></td>
  <td height="30" colspan="9"><h2>NOTA PEMBAYARAN</h2></td>
  </tr>
<tr>
  <td width="37">&nbsp;</td>
  <td width="113">&nbsp;</td>
  <td width="3">&nbsp;</td>
  <td width="340">&nbsp;</td>
  <td colspan="3" rowspan="3">&nbsp;</td>
  <td width="73">&nbsp;</td>
  <td colspan="3">&nbsp;</td>
</tr>
<tr>
  <td></td>
  <td>Nama Pasien </td>
  <td>:</td>
  <td><?php echo $pasienrj->pasien_nama; ?></td>
  <td>Tanggal </td>
  <td width="3">:</td>
  <td width="242"><?php echo date('d M Y'); ?></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>Alamat </td>
  <td>:</td>
  <td><?php echo $pasienrj->pasien_alamat; ?></td>
  <td>No. Nota</td>
  <td>:</td>
  <td><?php echo $total_bayar->no_pembayaran ?></td>
</tr>
</table>
<br>

<table width="900"  cellpadding="2" align="center" style="border:1px solid black;"> 
    <tr>  
      <td width="264" style="border:1px solid black"><div align="center">Item Description</div></td>
      <td width="33" style="border:1px solid black"><div align="center">Qyt</div></td>
      <td width="200" style="border:1px solid black"><div align="center">Unit Price</div></td> 
      <td width="200" style="border:1px solid black"><div align="center">Disc </div></td> 
      <td width="136" style="border:1px solid black"><div align="center">Amount</div></td>
    </tr>
    <tr>  
       
      <tr>  
      <td><b>Biaya Dokter :</b><br></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;text-align: right;">&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
    <?php
    
      foreach($tindakan_list as $list){       
        if($list->kelompoktindakan_id == 1){
      ?>  
      <td><?php echo $list->daftartindakan_nama. " Oleh ".$pasienrj->NAME_DOKTER ; ?></td>
      <!-- <td><?php echo $list->daftartindakan_nama ; ?></td> -->
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->jml_tindakan; ?></td>
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_tindakan)?></td> 
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->diskon)?></td>  
      <td style="text-align: right;"><?php echo "Rp. ".number_format($list->total_harga) ?></td>
    </tr>
          
    <?php  
       }
    }
     ?>

    <!-- type == tindakan khusus -->

      <tr>   
      <td><b>Tindakan:</b><br></td> 
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;text-align: right;">&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
      <?php  
        $no = 0;
      foreach($tindakan_list as $list){ 
        if($list->kelompoktindakan_id != 1){
        $no++;
      ?>   
      <td><?php echo $list->daftartindakan_nama ?></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->jml_tindakan; ?></td>
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_tindakan)?></td> 
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->diskon)?></td>  
      <td style="text-align: right;"><?php echo "Rp. ".number_format($list->total_harga) ?></td>
    </tr>  
    <?php  
        }
      }
     ?> 

      <tr>  
      <td><b>Obat:</b><br></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;text-align: right;">&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
      <?php  
        $no = 0;
      foreach($obat as $list){
        $no++;
      ?>   
      <td><?php echo $list->nama_barang; ?></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->qty; ?></td>
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_jual)?></td> 
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->diskon)?></td> 
      <td style="text-align: right;"><?php echo "Rp. ".number_format($list->total_harga) ?></td>
    </tr>  
    <?php  
      }
     ?> 
    </table>  
 

  <table style="margin-top: 8px" width="900" border="0" align="center">
    <tr> 
      <td rowspan="2" valign="top">
          <table width="300" height="150" border="0" align="center" style="margin-top: 16px"> 
            <tr>  
              <td valign="top" height="90" align="center"><span align="center">KASIR</span></td>
            </tr>
            <tr>  
              <td valign="top" align="center"><hr style="height: 1px;background-color: #000;"></td>
            </tr>

          </table>
      </td>

      <td>
           <table  width="300" height="110" border="1"> 
              <tr> 
                <td ><p>Terbilang : </p> <p><?php echo ucwords(Terbilang($total_bayar->total_bayar)). "RUPIAH"; ?></p></td>
              </tr>  
            </table>
      </td>

      <td>
            <table width="290" height="110" border="1">
              <tr>  
                <td ><p>Sub Total : <?= "Rp. ".number_format($total_bayar->total_keseluruhan)  ?> </p><hr>
                  <p>Discount &nbsp;:  <?= $total_bayar->total_discount ?></p></td>
              </tr>   
            </table>
        
      </td>
    </tr>
    <tr>
      <td>
          <table  width="300" height="100" border="1">
          <tr> 
            <td width="270" height="61" valign="top"><p>Description :</p>
              <p>&nbsp;</p></td>
          </tr>
        </table>
    </td>
    <td>
          <table width="290"  height="100" border="1">
            <tr>
              <td width="334" height="60" align="left" valign="top" >      <p>Retur : </p>
                <p><strong>Total Tagihan : 
                  <?= "Rp. ".number_format($total_bayar->total_bayar) ?>
              </strong></p></td>
            </tr>
          </table>  
    </td>
    
    </tr>

  </table>
  <br>
  <br> 

  <table  width="900" border="0" align="center">
  <tr>
      <td align="center"><img src="<?php echo base_url() ?>assets/plugins/images/header_kwitansi.png" alt=""></td>
  </tr>
  <tr>
      <td align="center">
          <p><small>Jl. Raya Petitenget No.1X-seminyak, kerobokan, badung, Kerobokan Kelod, Kec. Kuta Utara, Kabupaten Badung, Bali 80361 </small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
          <p><small>TELP (0361) 9343811</small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
          <p><small>Website:www.bvmedicalbali.com E-mail:bvmedicalbali@gmail.com</small></p>
          <!-- <p><small>Website://puribundamalang.com E-mail:puribunda.malang@yahoo.com</small></p> CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
          <!-- <p>BALI</p> CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
      </td>
  </tr>
  <tr> 
      <td align="center" valign="top"><hr style="height: 1px;background-color: #000;"></td>
  </tr> 
  </table>
  
  <table class="judul-invoice" width="900" border="0" align="center">
  <tr>
      <td align="right" style="width:50%"> INVOICE </td> <td align="center"> No :</td> <td> <?php echo $total_bayar->no_pembayaran ?></td> 
  </tr>
  
  </table>

  <table class="body-invoice" width="900" border="1" align="center" style="margin-top:20px;">
  <tr>
      <td align="left" style="width:18%"> Receive From </td> <td align="center"> : </td> <td> Indra Hadi S</td> 
  </tr>
  <tr>
      <td align="left" style="width:18%"> The Sum of </td> <td align="center"> : </td> <td> <?= "Rp. ".number_format($total_bayar->total_bayar) ?></td> 
  </tr>

  <tr>
      <td align="left" style="width:18%"> In Payment of </td> <td align="center"> : </td> 
  </tr>

  </table>
  
  <table width="900"  cellpadding="2" align="center" style="border:1px solid black;"> 
    <tr>  
      <td width="264" style="border:1px solid black"><div align="center">Item Description</div></td>
      <td width="33" style="border:1px solid black"><div align="center">Qyt</div></td>
      <td width="200" style="border:1px solid black"><div align="center">Unit Price</div></td> 
      <td width="200" style="border:1px solid black"><div align="center">Disc </div></td> 
      <td width="136" style="border:1px solid black"><div align="center">Amount</div></td>
    </tr>
    <tr>  
       
      <tr>  
      <td><b>Biaya Dokter :</b><br></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;text-align: right;">&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
    <?php
    
      foreach($tindakan_list as $list){       
        if($list->kelompoktindakan_id == 1){
      ?>  
      <td><?php echo $list->daftartindakan_nama. " Oleh ".$pasienrj->NAME_DOKTER ; ?></td>
      <!-- <td><?php echo $list->daftartindakan_nama ; ?></td> -->
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->jml_tindakan; ?></td>
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_tindakan)?></td> 
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->diskon)?></td>  
      <td style="text-align: right;"><?php echo "Rp. ".number_format($list->total_harga) ?></td>
    </tr>
          
    <?php  
       }
    }
     ?>

    <!-- type == tindakan khusus -->

      <tr>   
      <td><b>Tindakan:</b><br></td> 
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;text-align: right;">&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
      <?php  
        $no = 0;
      foreach($tindakan_list as $list){ 
        if($list->kelompoktindakan_id != 1){
        $no++;
      ?>   
      <td><?php echo $list->daftartindakan_nama ?></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->jml_tindakan; ?></td>
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_tindakan)?></td> 
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->diskon)?></td>  
      <td style="text-align: right;"><?php echo "Rp. ".number_format($list->total_harga) ?></td>
    </tr>  
    <?php  
        }
      }
     ?> 

      <tr>  
      <td><b>Obat:</b><br></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
      <td style="border-right: 1px solid black;text-align: right;">&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
      <?php  
        $no = 0;
      foreach($obat as $list){
        $no++;
      ?>   
      <td><?php echo $list->nama_barang; ?></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->qty; ?></td>
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_jual)?></td> 
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->diskon)?></td> 
      <td style="text-align: right;"><?php echo "Rp. ".number_format($list->total_harga) ?></td>
    </tr>  
    <?php  
      }
     ?> 
    </table>

<p>&nbsp;</p>
</body>

</html>


<?php

function Terbilang($x){
      $abil = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
      if ($x < 12)
        return " " . $abil[$x];
      elseif ($x < 20)
        return Terbilang($x - 10) . "BELAS";
      elseif ($x < 100)
        return Terbilang($x / 10) . " PULUH" . Terbilang($x % 10);
      elseif ($x < 200)
        return " SERATUS" . Terbilang($x - 100);
      elseif ($x < 1000)
        return Terbilang($x / 100) . " RATUS" . Terbilang($x % 100);
      elseif ($x < 2000)
        return " SERIBU" . Terbilang($x - 1000);
      elseif ($x < 1000000)
        return Terbilang($x / 1000) . " RIBU" . Terbilang($x % 1000);
      elseif ($x < 1000000000)
        return Terbilang($x / 1000000) . " JUTA" . Terbilang($x % 1000000);
    }
?>