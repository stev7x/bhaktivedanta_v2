<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>

<!-- MAIN CONTENT HERE -->
<div id="page-wrapper">
    <div class="container-fluid">
        
        <!-- TITLE -->
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Transaksi BPJS</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                <ol class="breadcrumb">
                    <li><a href="index.html">Kasir</a></li>
                    <li class="active">Transaksi BPJS</li>
                </ol>
            </div>
        </div>

        <!-- MAIN CONTENT -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info1">

                    <div class="panel-heading">Transaksi BPJS
                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                    </div> 

                    <div class="panel-wrapper collapse in" aria-expanded="true"> 
                        <div class="panel-body"> 
                            <ul class="nav customtab nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#BELUMBAYAR" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> BELUM BAYAR</span></a></li>
                                <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#SUDAHBAYAR" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">SUDAH BAYAR</span></a></li>  
                            </ul> 

                            <!-- Tab panes --> 
                            <div class="tab-content">
                                
                                <div role="tabpanel" class="tab-pane fade active in" id="BELUMBAYAR">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <label for="inputName1" class="control-label"></label>
                                            <dl class="text-right">  
                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                            </dl> 
                                        </div>  
                                        <div class="col-md-10 col-sm-10">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group col-md-4 col-sm-12"> 
                                                        <label for="inputName1" class="control-label"><b>Poli :</b></label>
                                                        <b>
                                                        <select name="poliruangan" id="poliruangan" class="form-control select" style="margin-bottom: 7px" onchange="getDokter()">
                                                            <option value="" disabled selected>Pilih Berdasarkan</option>
                                                            <?php
                                                                // $list_poli = $this->Pasien_rj_model->get_poli();
                                                                // foreach ($list_poli as $list) {
                                                                    // echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                                                // }
                                                            ?>
                                                        </select></b>                              
                                                    </div>    
                                                    <div class="form-group col-md-4 col-sm-12"> 
                                                        <label for="inputName1" class="control-label"><b>Dokter :</b></label>
                                                        <b>
                                                        <select name="dokter_poli" id="dokter_poli" class="form-control select" style="margin-bottom: 7px">
                                                            <option value="" >Pilih Berdasarkan</option>
                                                        </select></b>   
                                                    </div>    
                                                    <div class="form-group col-md-4 col-sm-12"> 
                                                        <label for="inputName1" class="control-label"><b>Pemanggilan :</b></label>
                                                        <b>
                                                        <select name="urutan" id="urutan" class="form-control select" style="margin-bottom: 7px">
                                                            <option value="" selected disabled>Pilih Berdasarkan</option>
                                                            <option value="no_pendaftaran" >Reservasi</option>
                                                            <option value="tgl_pendaftaran" >Datang</option>
                                                        </select></b>   
                                                    </div>    
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <label>Tanggal Kunjungan,Dari</label>
                                                        <div class="input-group">   
                                                            <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">  
                                                        <label>Sampai</label>    
                                                        <div class="input-group">   
                                                            <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2"> 
                                                        <label>&nbsp;</label><br>
                                                        <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                                    </div> 
                                                </div>
                                                <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="no_pendaftaran">No. Pendaftaran</option>
                                                        <option value="no_rekam_medis">No. Rekam Medis</option>
                                                        <option value="no_bpjs">No. BPJS</option>
                                                        <option value="pasien_nama">Nama Pasien</option>   
                                                        <option value="pasien_alamat">Alamat Pasien</option>   
                                                        <option value="NAME_DOKTER">Dokter PJ</option>   
                                                    </select></b>
                                                            
                                                </div>    
                                                <div class="form-group col-md-9" style="margin-top: 7px">   
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                    <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasien()" >       
                                                </div> 
                                            </div>
                                        </div> 
                                    </div><br><hr style="margin-top: -27px">
                                    <table id="table_list_kasirrj_belumbayar" class="table table-striped dataTable table-responsive" cellspacing="0">     
                                        <thead>  
                                            <tr>
                                                <th>No</th>
                                                <th>Poliklinik</th>
                                                <th>No. Pendaftaran</th>
                                                <th>Tanggal Pendaftaran</th>
                                                <th>No. Rekam Medis</th>
                                                <th>Nama Pasien</th> 
                                                <th>Jenis Kelamin</th>
                                                <th>Umur Pasien</th>
                                                <th>Alamat Pasien</th>
                                                <th>Pembayaran</th>           
                                                <th>Status Periksa</th>
                                                <th>Bayar Tagihan</th>   
                                            </tr>
                                        </thead>  
                                        <tbody>
                                            <tr>
                                                <td colspan="12">No Data to Display</td>
                                            </tr>    
                                        </tbody> 
                                    </table> 
                                </div> 

                                <div role="tabpanel" class="tab-pane fade in" id="SUDAHBAYAR"> 
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                        <label for="inputName1" class="control-label"></label>
                                            <dl class="text-right">  
                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                            </dl> 
                                        </div> 
                                        <div class="col-md-10 col-sm-10">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group col-md-4 col-sm-12"> 
                                                        <label for="inputName1" class="control-label"><b>Poli :</b></label>
                                                        <b>
                                                        <select name="poliruangan_byr" id="poliruangan_byr" class="form-control select" style="margin-bottom: 7px" onchange="getDokter()">  
                                                            <option value="" disabled selected>Pilih Berdasarkan</option>
                                                            <?php
                                                                // $list_poli = $this->Pasien_rj_model->get_poli();
                                                                // foreach ($list_poli as $list) {
                                                                //     echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                                                // }
                                                            ?>
                                                        </select></b>
                                                            
                                                    </div>    
                                                    <div class="form-group col-md-4 col-sm-12"> 
                                                        <label for="inputName1" class="control-label"><b>Dokter :</b></label>
                                                        <b>
                                                        <select name="dokter_poli_byr" id="dokter_poli_byr" class="form-control select" style="margin-bottom: 7px">
                                                            <option value="" >Pilih Berdasarkan</option>
                                                        </select></b>
                                                            
                                                    </div>    
                                                    <div class="form-group col-md-4 col-sm-12"> 
                                                        <label for="inputName1" class="control-label"><b>Pemanggilan :</b></label>
                                                        <b>
                                                        <select name="urutan_byr" id="urutan_byr" class="form-control select" style="margin-bottom: 7px">
                                                            <option value="" selected disabled>Pilih Berdasarkan</option>
                                                            <option value="no_pendaftaran_byr" >Reservasi</option>
                                                            <option value="tgl_pendaftaran_byr" >Datang</option>
                                                        </select></b>
                                                            
                                                    </div>    
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <label>Tanggal Kunjungan,Dari</label>
                                                        <div class="input-group">   
                                                            <input onkeydown="return false" name="tgl_awal_byr" id="tgl_awal_byr" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">  
                                                        <label>Sampai</label>    
                                                        <div class="input-group">   
                                                            <input onkeydown="return false" name="tgl_akhir_byr" id="tgl_akhir_byr" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-2"> 
                                                        <label>&nbsp;</label><br> 
                                                        <button type="button" class="btn btn-success col-md-12" onclick="cariPasienByr()">Cari</button>        
                                                    </div> 
                                                </div>
                                                <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option_byr" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="no_pendaftaran_byr">No. Pendaftaran</option>
                                                        <option value="no_rekam_medis_byr">No. Rekam Medis</option>
                                                        <option value="no_bpjs_byr">No. BPJS</option>
                                                        <option value="pasien_nama_byr">Nama Pasien</option>         
                                                        <option value="pasien_alamat_byr">Alamat Pasien</option>   
                                                        <option value="NAME_DOKTER_byr">Dokter PJ</option>   
                                                    </select></b>
                                                            
                                                </div>    
                                                <div class="form-group col-md-9" style="margin-top: 7px">       
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label> 
                                                    <input type="Text" class="form-control pilihbyr" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasienByr()" >       
                                                </div> 
                                            </div>
                                        </div> 
                                    </div><br><hr style="margin-top: -27px">
                                    <table id="table_list_kasirrj_sudahbayar" class="table table-striped dataTable table-responsive" cellspacing="0">     
                                        <thead>      
                                            <tr> 
                                                <th>No</th>
                                                <th>Poliklinik</th>
                                                <th>No. Pendaftaran</th>
                                                <th>Tanggal Pendaftaran</th>
                                                <th>No. Rekam Medis</th>
                                                <th>Nama Pasien</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur Pasien</th>
                                                <th>Alamat Pasien</th>
                                                <th>Pembayaran</th>           
                                                <th>Status Periksa</th>
                                                <th>Generate Invoice</th>
                                                <th>Bayar Tagihan</th>   
                                                <!-- <th>Pulangkan</th>        -->
                                            </tr>
                                        </thead>  
                                        <tbody>
                                            <tr>
                                                <td colspan="12">No Data to Display</td>
                                            </tr>    
                                        </tbody> 
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>  

                </div>
            </div>
        </div>

    </div>
</div>
<!-- MAIN CONTENT HERE -->

<?php $this->load->view('footer');?>