<!DOCTYPE html>
<html>
    <head>
        <title>Kwitansi Pembayaran</title>
        <style>
            .payment{
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            .payment th {
                border: 1px solid #000000;
                text-align: center;
                padding: 8px;
            }  
            
            .payment td {
                border: 1px solid #000000;
                text-align: left;
                padding: 8px;
            } 
             
            .fott{
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }
            
        </style>
    </head> 
    <body onload="window.print()">
        <table>
            <tr>
            <td rowspan="3" class="brand-logo"><img src="<?php echo base_url()?>assets/plugins/images/logo-puribunda.png" alt="RSU logo" width="200px"> &nbsp; &nbsp;</td>
                <td colspan="5" style="text-align: center;"><h2>RSU. MITRA DELIMA</h2></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center;">Jl. Raya Bulupayung 1B Desa Krebet, Kec. Bululawang, Kab. Malang</td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center;">Phone (0341)805183, 081217442444, E-Mail rsumitradelima@y_mail.com</td>
            </tr>
            <tr>
                <td colspan="6"><hr></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;">KWITANSI PEMBAYARAN</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>&nbsp;</td>
                <td>Pasien</td><td>:</td><td colspan="2"><?php echo $pasienrj->pasien_nama; ?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>No. RM</td><td>:</td><td colspan="2"><?php echo $pasienrj->no_rekam_medis; ?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>Alamat</td><td>:</td><td colspan="2"><?php echo $pasienrj->pasien_alamat; ?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>Poliklinik</td><td>:</td><td colspan="2"><?php echo $pasienrj->nama_poliruangan; ?></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br>
        <?php
        $total_obat = isset($tot_obat->total) ? $tot_obat->total : '0';
        $total_rad = isset($tot_rad->total_harga) ? $tot_rad->total_harga : '0';
        $total_lab = isset($tot_lab->total_harga) ? $tot_lab->total_harga : '0';
        $total_tind = isset($tot_tind->total_harga) ? $tot_tind->total_harga : '0';
        $total_all = $total_obat+$total_rad+$total_lab+$total_tind;
        ?>
        <table class="payment">
            <thead> 
                <th>No</th>
                <th>Transaksi</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Total</th>
                <th>Keterangan</th>
            </thead>   
            <tbody>
                <?php
                foreach($tindakan_list as $list){
                    if($list->komponentarif_id == 4){
                ?>
                <tr>
                    <td>1</td>
                    <td>Pendaftaran</td> 
                    <td><?php echo number_format($list->harga_tindakan); ?></td>
                    <td><?php echo $list->jml_tindakan; ?></td>
                    <td><?php echo number_format($list->total_harga); ?></td>
                    <td>-</td>
                </tr>
                <?php
                    }
                }
                ?> 

                <?php
                foreach($tindakan_list as $list){
                    if($list->komponentarif_id == 8){
                ?>
                <tr>
                    <td>2</td>
                    <td>Pemeriksaan Dokter</td>
                    <td><?php echo number_format($list->harga_tindakan); ?></td>
                    <td><?php echo $list->jml_tindakan; ?></td>
                    <td><?php echo number_format($list->total_harga); ?></td>
                    <td>-</td>
                </tr>
                <?php 
                    }
                }
                ?>
                <tr>
                    <td>3</td>
                    <td>Obat</td>
                    <td><?php echo number_format($total_obat); ?></td>
                    <td>1</td>
                    <td><?php echo number_format($total_obat); ?></td>
                    <td>-</td>
                </tr> 
                <tr>
                    <td>4</td>
                    <td>Pemeriksaan Lainnya :</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Laboratorium</td>
                    <td><?php echo number_format($total_lab); ?></td>
                    <td>-</td>
                    <td><?php echo number_format($total_lab); ?></td>
                    <td>-</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Radiologi</td>
                    <td><?php echo number_format($total_rad); ?></td>
                    <td>-</td>
                    <td><?php echo number_format($total_rad); ?></td>
                    <td>-</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tindakan Khusus :</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                $no = 0;
                foreach($tindakan_list as $list){
                    if($list->tariftindakan_id != '3' && $list->tariftindakan_id != '2'){
                        $no++;
                ?>
                        <tr>
                            <td></td>
                            <td><?php echo $no.". &nbsp;".$list->daftartindakan_nama; ?></td>
                            <td><?php echo number_format($list->harga_tindakan); ?></td>
                            <td><?php echo $list->jml_tindakan; ?></td>
                            <td><?php echo number_format($list->total_harga); ?></td>
                            <td>-</td>
                        </tr>
                <?php
                    }
                }
                ?>
            </tbody>
            <tfoot>
            <th colspan="2"> Total</th>
            <th></th>
            <th></th>
            <th><?php echo $total_bayar->total_bayar; ?></th>
            <!-- <th><?php //echo number_format($total_all); ?></th> -->
            <th></th>
            </tfoot>
        </table>
        <br>
        <table class="fott">
            <tr>
                <td colspan="4" style="width:370px">&nbsp;</td>
                <td style="text-align: right;">Malang,&nbsp;</td>
                <td style="text-align: left;"> <?php echo date('d M Y'); ?></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td style="text-align: center;" colspan="2">Kasir</td>
            </tr>
            <tr><td colspan="6" style="text-align: center;">&nbsp;</td></tr>
            <tr><td colspan="6" style="text-align: center;">&nbsp;</td></tr>
            <tr><td colspan="6" style="text-align: center;">MENGUTAMAKAN KESELAMATAN DAN KENYAMANAN PASIEN</td></tr>
        </table>
    </body>
</html>