<!DOCTYPE html>
<html>
    <head>
        <title>Kwitansi Pembayaran</title>
        <style>
            .payment{
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            .payment th {
                border: 1px solid #000000;
                text-align: left;
                padding: 8px;
            }  
            
            .payment td {
                border: 1px solid #000000;
                text-align: left;
                padding: 8px;
            } 
            
            .fott{
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }
            
        </style>
    </head>
    <body onload="window.print()">
        <table>
            <tr>
            <td rowspan="3" class="brand-logo"><img src="<?php echo base_url()?>assets/plugins/images/logo-puribunda.png" alt="RSU logo" width="200px"> &nbsp; &nbsp;</td>
                <td colspan="6" style="text-align: center;">RSU. MITRA DELIMA</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;">Jl. RAYA BULUPAYUNG 1B DESA KREBET, KEC. BULULAWANG, KAB. MALANG</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;">PHONE ( 0341 ) 805183, 081 217 442 444, E-Mail rsumitradelima@y_mail.com</td>
            </tr>
            <tr>
                <td colspan="7"><hr></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: center;">KWITANSI</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td colspan="7" align="right"> Malang, <?php echo date('d F Y'); ?></td>
            </tr>
            <tr>
                <td>No. Kwitansi</td><td>:</td><td>
                    <?php echo $pasienri->no_rekam_medis . "/" . $pasienri->type_pembayaran . "/" . date("m/Y"); ?>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Telah terima dari</td><td>:</td><td><?php echo $pasienri->pasien_nama; ?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <?php
                    $total_obat = isset($tot_obat->total) ? $tot_obat->total : '0';
                    $total_bhp = isset($tot_bhp->total) ? $tot_bhp->total : '0';
                    $total_alkes = isset($tot_alkes->total) ? $tot_alkes->total : '0';
                    $total_rad = isset($tot_rad->total_harga) ? $tot_rad->total_harga : '0';
                    $total_lab = isset($tot_lab->total_harga) ? $tot_lab->total_harga : '0';
                    $total_tind = isset($tot_tind->total_harga) ? $tot_tind->total_harga : '0';
                    $total_jasadokter = isset($tot_jasadokter->total_harga) ? $tot_jasadokter->total_harga : '0';
                    $total_jasaperawatan = isset($tot_jasaperawatan->total_harga) ? $tot_jasaperawatan->total_harga : '0';
                    $total_all = $total_obat+$total_bhp+$total_alkes+$total_rad+$total_lab+$total_tind+$total_jasadokter+$total_jasaperawatan;
                    $total_obatalkes = $total_obat+$total_bhp+$total_alkes;
                    $total_penunjang = $total_rad+$total_lab;
                ?>
                <td>Uang Sebanyak</td><td>:</td><td><?php echo "Rp.". number_format($total_all); ?></td>
            </tr>
            <tr>
                <td>Untuk Pembayaran</td><td>:</td><td>
                    <?php
                    $dataCome = date_create(date('Y-m-d', strtotime($pasienri->tgl_masukadmisi)));
                    $dateOut = date_create(date('Y-m-d', strtotime($pasienri->tgl_pulang)));
                    $diffDate = date_diff($dataCome, $dateOut);
                    echo "Pelayanan Kesehatan Rawat Inap di RS SIMRS PURIBUNDA a/n" . $pasienri->pasien_nama . " (" . $pasienri->umur . "), " . $pasienri->no_rekam_medis . ", " .
                    date('d-M-Y H:i:s', strtotime($pasienri->tgl_masukadmisi)) . " WIB s/d " . date('d-M-Y H:i:s', strtotime($pasienri->tgl_pulang)) . 
                    " (" . 
                        $diffDate->format('%R%a Hari')
                    . ")"; ?>
                </td>
                <td>&nbsp;</td>
            </tr>
<!--            </tfoot>-->        
        </table>
        <br>
        <table class="fott">
            <tr>
                <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td style="text-align: center;" colspan="1">Pasien/Keluarga Pasien</td>
            </tr>
            <tr><td colspan="7" style="text-align: center;">&nbsp;</td></tr>
            <tr><td colspan="7" style="text-align: center;">&nbsp;</td></tr>
            <tr>
                <td style="text-align: center;" colspan="2"><?php echo "Terbilang : <br>" . Terbilang($total_all) . " Rupiah"; ?></td>
                <td colspan="4">&nbsp;</td>
                <td style="text-align: center;" colspan="1">(Kasir Rawat Inap)</td>
            </tr>
            <tr><td colspan="7" style="text-align: center;">&nbsp;</td></tr>
            <tr><td colspan="7" style="text-align: center;">MENGUTAMAKAN KESELAMATAN DAN KENYAMANAN PASIEN</td></tr>
        </table>
    </body>
</html>

<?php 

function Terbilang($x){
      $abil = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
      if ($x < 12)
        return " " . $abil[$x];
      elseif ($x < 20)
        return Terbilang($x - 10) . "BELAS";
      elseif ($x < 100)
        return Terbilang($x / 10) . " PULUH" . Terbilang($x % 10);
      elseif ($x < 200)
        return " SERATUS" . Terbilang($x - 100);
      elseif ($x < 1000)
        return Terbilang($x / 100) . " RATUS" . Terbilang($x % 100);
      elseif ($x < 2000)
        return " SERIBU" . Terbilang($x - 1000);
      elseif ($x < 1000000)
        return Terbilang($x / 1000) . " RIBU" . Terbilang($x % 1000);
      elseif ($x < 1000000000)
        return Terbilang($x / 1000000) . " JUTA" . Terbilang($x % 1000000);
    }

?>