<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Kasir Rawat Inap</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="index.html">Kasir</a></li>
                            <li class="active">Kasir Rawat Inap</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info1">
                            <div class="panel-heading"> Data Kasir Rawat Inap     
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <!-- <div class="table-responsive"> -->
                                   <table id="table_list_kasirri" class="table table-striped dataTable table-responsive" cellspacing="0">
                                        <thead> 
                                            <tr>
                                                <th>No</th>
                                                <th>Ruangan</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>No. Pendaftaran</th>
                                                <th>Tanggal Pendaftaran</th>
                                                <th>No. Rekam Medis</th>
                                                <th>Nama Pasien</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur Pasien</th>
                                                <th>Alamat Pasien</th>
                                                <th>Pembayaran</th>
                                                <th>Status Rawat</th>
                                                <th>Bayar Tagihan</th>
                                                <th>Generate Invoice</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="13">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <!--/row -->
               
            </div>
            <!-- /.container-fluid -->

<div id="modal_bayar_kasirri" class="modal fade bs-example-modal-lg" onclick="reloadTableKasir()" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">      
        <div class="modal-content">       
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Bayar Tagihan</b></h2>  
             </div>  
            <div class="modal-body" style="background: #fafafa">        
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>    
                </div> 
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal_generate" class="modal fade bs-example-modal-lg" onclick="()" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;">
    <div class="modal-dialog modal-large">         
        <div class="modal-content" style="margin-top: 150px"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Generate</b></h2>  
             </div>       
            <div class="modal-body" style="background: #fafafa;height: auto; max-height: 500px; overflow: auto;">        
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>    
                </div> 
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->           

           

<?php $this->load->view('footer');?>
      