<?php $this->load->view('header_iframe');?>
<?php
$totaltindakan = !empty($total_tindakan->total_harga) ? $total_tindakan->total_harga : 0;
$totalrad = !empty($total_rad->total_harga) ? $total_rad->total_harga : 0;
$totallab = !empty($total_lab->total_harga) ? $total_lab->total_harga : 0;
$totalobat = !empty($total_obat->total_harga) ? $total_obat->total_harga : 0;
$subtotal = $totaltindakan+$totalrad+$totallab+$totalobat;
?> 
<body>
<div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
    <button type="button" class="close" data-dismiss="alert" >&times;</button> 
    <div> 
        <p id="modal_card_message"></p> 
    </div>
</div>

<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row">  
        <div class="col-md-12">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">   
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr> 
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr> 
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Poliklinik</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">  
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->umur; ?></td>
                                    </tr> 
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr> 
                                    <tr> 
                                        <td><b>Kelas Pelayanan</b></td> 
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr> 
                                    <tr> 
                                        <td><b>Type Pembayaran</b></td> 
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr>
                                     <tr>
                                        <td><b>Jenis Pasien</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_pasien; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Call</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->type_call; ?></td>
                                    </tr>  
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  
    </div>    

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info1">
                <div class="panel-heading" align="center" style="background-color: #0bb58b;"> Detail List Tindakan 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        

                        <ul class="nav customtab nav-tabs" role="tablist">
                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#umum" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> UMUM</span></a></li>
                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#rad" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">RADIOLOGI</span></a></li>  
                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#lab" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">LABORATORIUM</span></a></li>
                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#obat" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">OBAT</span></a></li>

                        </ul>    
              
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="umum">
                                <?php echo form_open('#',array('id' => 'fmTindakanUmum'))?> 
                                <div class="row"> 
                                    <div class="col-md-12"> 
                                        <h4 align="center">Tindakan Umum</h4>           
                                        <!-- <button type="button" class="btn btn-success pull-right" onclick="reloadTableTindakan()"><i class="fa fa-refresh"></i></button>  -->
                                        <table id="table_tindakan_kasir" class="table table-striped table-hover" cellspacing="0">
                                            <thead>   
                                                <tr>    
                                                    <th></th>
                                                    <th>Tgl. Tindakan</th>
                                                    <th>Nama Tindakan</th>
                                                    <th>Harga</th>
                                                    <th>Jumlah</th>
                                                    <th>Diskon</th> 
                                                    <th>Total</th>          
                                                    <th>Cara Bayar</th>     
                                                    <th>Aksi</th> 
                                                </tr>   
                                            </thead>
                                            <tbody>     
                                                <tr>     
                                                    <td colspan="9">No Data to Display</td>
                                                </tr>    
                                               <!--  <?php 
                                                foreach ($tindakan_umum as $tindakan){  
                                                $tgl_tindakan = date_create($tindakan->tgl_tindakan);
                                                
                                                ?> 
                                                <tr>      
                                                    <td><?= date_format($tgl_tindakan, 'd-M-Y') ?> </td>  
                                                    <td><?= $tindakan->daftartindakan_nama ?></td>
                                                    <td><?= $tindakan->harga_tindakan ?></td>  
                                                    <td><?= $tindakan->jml_tindakan ?></td>   
                                                    <td><?= $tindakan->total_harga_tindakan ?></td>  
                                                    <td><?= $tindakan->is_cyto == 1 ? 'YA' : 'Tidak' ?></td>  
                                                    <td><span id="total_harga"><?= $tindakan->total_harga ?></span></td>                  
                                                    <td><a href="#" class="discount" id="val_discount" data-url="<?= base_url() ?>kasir/kasirrj/do_live_edit" data-pk="<?= $tindakan->tindakanpasien_id ?>"  data-title="Discount"><?= !empty($tindakan->val_discount ) ? $tindakan->val_discount : 0 ?></a></td>            
                                               </tr> 
                                                <?php  } ?>  -->
                                            </tbody>  
                                            <tfoot>  
                                                <tr>    
                                                    <td colspan="9" style="text-align: right;"><b>Total Tindakan Umum : <?= number_format($totaltindakan) ?></b></td> 
                                                </tr>
                                            </tfoot> 
                                        </table>
                                    </div>      
                                     
                                    <div class="col-md-12">  
                                        <div class="form-group col-md-3"> 
                                            <label>Pilih Pembayaran</label> 
                                            <input type="hidden" name="pembayaran_id" id="pembayaran_id" value="<?= $list_pasien->pembayaran_id ?>">      
                                            <select name="cara_pembayaran_umum" id="cara_pembayaran_umum" class="form-control" onchange="showCaraBayar('umum')">     
                                                <option disabled selected>PILIH PEMBAYARAN</option> 
                                                <option value="PRIBADI">PRIBADI</option>
                                                <option value="ASURANSI1">ASURANSI 1</option> 
                                                <option value="ASURANSI2">ASURANSI 2</option> 
                                            </select>        
                                        </div>  
                                        <div class="pembayaran1 col-md-7" style="display: none;" >
                                            <div class="form-group col-md-6">   
                                                <label id="lblNamaPembayaran_umum">Nama Asuransi</label> 
                                                <input type="text" name="nama_asuransi_umum" id="nama_asuransi_umum" class="form-control" placeholder="nama asuransi">
                                            </div>
                                            <div class="form-group col-md-6">   
                                                <label id="lblNomorPembayaran_umum">Nomor Asuransi</label>  
                                                <input type="number" name="nomor_asuransi_umum" id="nomor_asuransi_umum" class="form-control" placeholder="nomor asuransi"> 
                                            </div> 
                                            
                                        </div>     
                                        <div class="form-group col-md-2">
                                            <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">       
                                            <input type="hidden" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">              
                                            <input type="hidden" name="tanggal_pembayaran" value="<?= date('d-M-Y') ?>">  
                                            <div id="detail_id">
                                                <input type="hidden" id="detail_pembayaran_id" name="detailpembayaran_id" value="<?php echo !empty($list_pasien->det_tindakan_id) ? "$list_pasien->det_tindakan_id" : ""  ?>" />         
                                            </div>      


                                            <!-- <button hidden type="button" class="btn btn-success" onclick="saveCaraBayar()" ><i class="fa fa-floppy-o"></i> SIMPAN</button>   -->
                                            <label>&nbsp;</label>   
                                            <button type="button" class="btn btn-success col-md-12" id="saveTindakanUmum"><i class="fa fa-floppy-o"></i> Konfirmasi</button>    
                                        </div>
                                    </div>   
                                    <div class="col-md-6 totalumum" >        
                                        <label>Table Tindakan Cover</label>     
                                        <table style="max-height: 250px;overflow:auto;" id="table_tindakan_cover" class="table   table-striped table-hover" cellspacing="0"> 
                                            <thead>       
                                                <tr>       
                                                    <th>Nama Tindakan</th>
                                                    <th>Cover By</th>     
                                                    <th>Aksi</th>  
                                                </tr>   
                                            </thead> 
                                            <tbody>     
                                                <tr>     
                                                    <td colspan="3" align="center">No Data to Display</td>
                                                </tr>    
                                            </tbody>    
                                        </table>   
                                    </div>
                                </div>      
                                <input type="hidden" name="action" id="action" value="<?= $action?>">              
                                <?= form_close() ?>        
                               <div class="clearfix"></div>
                            </div> 
 
                            <div role="tabpanel" class="tab-pane fade in" id="rad">
                                <div class="row">  
                                    <div class="col-md-12">   
                                        <h4 align="center">Tindakan Radiologi</h4>
                                        
                                        <table id="table_tindakanrad_kasir" class="table table-striped table-hover" cellspacing="0">
                                            <thead>  
                                                <tr>
                                                    <th>Tgl. Tindakan</th>
                                                    <th>Nama Tindakan</th>
                                                    <th>Harga</th>
                                                    <th>Jumlah</th>
                                                    <th>Total</th>
                                                    <th>Cyto</th> 
                                                    <th>Total + Cyto</th>
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="7">No Data to Display</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="7" style="text-align: right;"><b>Total Tindakan Radiologi : <?= number_format($totalrad) ?> </b></td> 
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="col-md-6">  
                                        <div class="form-group col-md-12"> 
                                            <label>Pilih Pembayaran</label> 
                                            <input type="hidden" name="pembayaran_id" id="pembayaran_id" value="<?= $list_pasien->pembayaran_id ?>">          
                                            <select name="cara_pembayaran_rad" id="cara_pembayaran_rad" class="form-control" onchange="showCaraBayar('rad')">      
                                                <option disabled selected>PILIH PEMBAYARAN</option> 
                                                <option value="PRIBADI">PRIBADI</option>
                                                <option value="ASURANSI1">ASURANSI 1</option> 
                                                <option value="ASURANSI2">ASURANSI 2</option> 
                                            </select>         
                                        </div>
                                        <div class="pembayaran2 col-md-12" style="display: none;" >
                                            <div class="form-group">   
                                                <label id="lblNamaPembayaran_rad">Nama Asuransi</label> 
                                                <input type="text" name="nama_asuransi_rad" id="nama_asuransi_rad" class="form-control" placeholder="nama asuransi">
                                            </div>
                                            <div class="form-group">   
                                                <label id="lblNomorPembayaran_rad">Nomor Asuransi</label>  
                                                <input type="number" name="nomor_asuransi_rad" id="nomor_asuransi_rad" class="form-control" placeholder="nomor asuransi">
                                            </div>
                                        </div>   
                                    </div>
                                    <div class="col-md-6 totalrad" style="display: none;">
                                        <div class="form-group"> 
                                            <label for="total_rad" class="active">Jumlah yang dibayar</label>
                                            <input id="val_total_rad" class="form-control" type="number" name="total_rad_val" value="<?php echo $totalrad; ?>" >          
                                            <!-- <input id="total_rad" type="text" name="total_rad" class="form-control" value="<?php echo number_format($totalrad); ?>" readonly> -->
                                        </div>
                                    </div>
                                </div>
                               <div class="clearfix"></div>
                            </div>


                            <div role="tabpanel" class="tab-pane fade in" id="lab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 align="center">Tindakan Laboratorium</h4>
                                        <table id="table_tindakanlab_kasir" class="table table-striped table-hover" cellspacing="0">
                                            <thead>  
                                                <tr>
                                                    <th>Tgl. Tindakan</th>
                                                    <th>Nama Tindakan</th>
                                                    <th>Harga</th>
                                                    <th>Jumlah</th>
                                                    <th>Total</th>
                                                    <th>Cyto</th>
                                                    <th>Total + Cyto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr> 
                                                    <td colspan="7">No Data to Display</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr> 
                                                    <td colspan="7" style="text-align: right;"><b>Total Laboratorium : <?= number_format($totallab) ?></b></td>  
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="col-md-6">  
                                        <div class="form-group col-md-12"> 
                                            <label>Pilih Pembayaran</label> 
                                            <input type="hidden" name="pembayaran_id" id="pembayaran_id" value="<?= $list_pasien->pembayaran_id ?>">      
                                            <select name="cara_pembayaran_lab" id="cara_pembayaran_lab" class="form-control" onchange="showCaraBayar('lab')">      
                                                <option disabled selected>PILIH PEMBAYARAN</option> 
                                                <option value="PRIBADI">PRIBADI</option>
                                                <option value="ASURANSI1">ASURANSI 1</option> 
                                                <option value="ASURANSI2">ASURANSI 2</option> 
                                            </select>        
                                        </div>  
                                        <div class="pembayaran3 col-md-12" style="display: none;" >
                                            <div class="form-group">   
                                                <label id="lblNamaPembayaran_lab">Nama Asuransi</label> 
                                                <input type="text" name="nama_asuransi_lab" id="nama_asuransi_lab" class="form-control" placeholder="nama asuransi">
                                            </div>
                                            <div class="form-group">   
                                                <label id="lblNomorPembayaran">Nomor Asuransi</label>  
                                                <input type="number" name="nomor_asuransi_lab" id="nomor_asuransi_lab" class="form-control" placeholder="nomor asuransi">
                                            </div>
                                            
                                        </div>   
                                    </div>
                                    <div class="col-md-6 totallab" style="display: none;">
                                        <div class="form-group">
                                            <label for="total_lab">Jumlah yang dibayar</label>
                                            <input id="val_total_lab" class="form-control" type="number" name="total_lab_val" value="<?php echo $totallab; ?>" >   
                                            <!-- <input id="total_lab" type="text" name="total_lab" class="form-control" value="<?php echo number_format($totallab); ?>" readonly> -->
                                        </div>
                                    </div>
                                </div>
                               <div class="clearfix"></div> 
                            </div> 



                            <div role="tabpanel" class="tab-pane fade in" id="obat"> 
                                <?php echo form_open('#',array('id' => 'fmObat'))?>  
                                    <div class="row">  
                                        <div class="col-md-12">   
                                            <h4 align="center">Obat Asuransi</h4> 
                                            <table id="table_obat_kasir" class="table table-striped table-hover" cellspacing="0">
                                                <thead>  
                                                    <tr>  
                                                        <th></th>  
                                                        <th>Tanggal</th> 
                                                        <th>Nama Obat</th>  
                                                        <th>Harga</th> 
                                                        <th>Jumlah</th>   
                                                        <th>Total</th>
                                                        <th>Diskon</th>   
                                                        <th>Cara Bayar</th>  
                                                        <th>Aksi</th>          
                                                        <!-- <th>Cara Bayar</th> -->
                                                    </tr>   
                                                </thead> 
                                                <tbody>    
                                                    <tr> 
                                                        <td colspan="9">No Data to Display</td> 
                                                    </tr> 
                                                </tbody>     
                                                <tfoot>    
                                                    <tr>   
                                                        <td colspan="9" style="text-align: right;"><b>Total Obat : <?= number_format($totalobat) ?></b></td> 
                                                    </tr>
                                                </tfoot> 
                                            </table>   
                                        </div> 
                                       <!--  <div class="col-md-6">         
                                            <div class="form-group">     
                                                <label for="total_obat" >Jumlah yang dibayar</label>     
                                                <input id="val_total_obat" class="form-control" type="number" name="total_obat_val" value="<?php echo $totalobat; ?>" >  
                                            </div>    
                                            <input type="hidden" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>"> 
                                            <input type="hidden" name="pembayaranobat_id" id="pembayaranobat_id" > 
                                            <button type="button" title="klik untuk simpan pembayaran asuransi" class="btn btn-success" id="saveObat"><i class="fa fa-floppy-o"></i> SIMPAN</button> 
                                        </div> -->
                                        <div class="col-md-12">   
                                            <div class="form-group col-md-3"> 
                                                <label>Pilih Pembayaran</label>   
                                                <input type="hidden" name="pembayaran_id" id="pembayaran_id" value="<?= $list_pasien->pembayaran_id ?>">        
                                                <select name="cara_pembayaran_obat" id="cara_pembayaran_obat" class="form-control" onchange="showCaraBayar('obat')">      
                                                    <option disabled selected>PILIH PEMBAYARAN</option> 
                                                    <option value="PRIBADI">PRIBADI</option>
                                                    <option value="ASURANSI1">ASURANSI 1</option> 
                                                    <option value="ASURANSI2">ASURANSI 2</option> 
                                                </select>         
                                            </div>   
                                            <div class="pembayaran4 col-md-7" style="display: none;" >
                                                <div class="form-group col-md-6">     
                                                    <label id="lblNamaPembayaran_obat">Nama Asuransi</label>  
                                                    <input type="text" name="nama_asuransi_obat" id="nama_asuransi_obat" class="form-control" placeholder="nama asuransi">
                                                </div>
                                                <div class="form-group col-md-6">   
                                                    <label id="lblNomorPembayaran_obat">Nomor Asuransi</label>  
                                                    <input type="number" name="nomor_asuransi_obat" id="nomor_asuransi_obat" class="form-control" placeholder="nomor asuransi">     
                                                </div> 
                                                 
                                            </div>     
                                            <div class="form-group col-md-2">
                                                <!-- <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">        -->  
                                                <div id="bayarobat_id"> 
                                                <input type="hidden" name="pembayaran_obat_id" id="pembayaran_obat_id" value="<?php echo !empty($list_pasien->det_obat_id) ? "$list_pasien->det_obat_id" : ""  ?>" />       
                                                </div>   

                                                <input type="hidden" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">              
                                                <input type="hidden" name="tanggal_pembayaran" value="<?= date('d-M-Y') ?>">    


                                                <button hidden type="button" class="btn btn-success" onclick="saveCaraBayar()" ><i class="fa fa-floppy-o"></i> SIMPAN</button>  
                                                <label>&nbsp;</label>   
                                                <button type="button" class="btn btn-success col-md-12" id="saveObat"><i class="fa fa-floppy-o"></i> SIMPAN</button>   
                                            </div>     
                                        </div>     
                                        <div class="col-md-6 totalobat">           
                                            <label>Table Obat Cover</label>     
                                            <table style="max-height: 250px;overflow:auto;" id="table_obat_cover" class="table   table-striped table-hover" cellspacing="0"> 
                                                <thead>        
                                                    <tr>        
                                                        <th>Nama Obat</th>
                                                        <th>Cover By</th>      
                                                        <th>Aksi</th>  
                                                    </tr>    
                                                </thead> 
                                                <tbody>      
                                                    <tr>     
                                                        <td colspan="3" align="center">No Data to Display</td>
                                                    </tr>    
                                                </tbody>    
                                            </table>   
                                        </div> 
                                    </div>    
                                <?php echo form_close() ?>    
                                
                               <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div><BR><BR> 

    <div class="row">  
        <div class="col-md-12">   
        <h4 align="center">Input Pembayaran</h4>     
             <?php echo form_open('#',array('id' => 'fmPembayaran'))?>
            <div class="row"> 
                <div class="col-md-6">         
                <input type="hidden" name="aksi" id="aksi" value="<?= $action?>">           
                <input type="hidden" name="pembayarankasir_id" id="pembayarankasir_id" value="<?php $temp = $list_pasien->pendaftaran_id; $this->Kasirrj_model->get_pembayarankasir($temp); ?>"> 
                <input id="total_tindakan_val" type="hidden" name="total_tindakan_val" value="<?php echo $totaltindakan ?>"  > 
                <input id="total_rad_val"  type="hidden" name="total_rad_val" value="<?php echo $totalrad; ?>" > 
                <input id="total_lab_val"  type="hidden" name="total_lab_val" value="<?php echo $totallab; ?>" >
                <input id="total_obat_val" type="hidden" name="total_obat_val" value="<?php echo $totalobat; ?>" > 
                    <div class="form-group"> 
                        <label for="tgl_pembayaran" class="active">Tgl. Pembayaran</label>
                        <input id="tgl_pembayaran" type="text" name="tgl_pembayaran" class="form-control" value="<?php echo  date('d-M-Y'); ?>" readonly>
                    </div>
                    <div class="form-group">  
                        <label>Total Tindakan Umum</label>   
                        <input type="text" name="totaltindakanumum" class="form-control" value="<?= number_format($totaltindakan)?>" readonly>  
                    </div>
                    <div class="form-group">
                        <label>Total Tindakan Radiologi</label>
                        <input type="text" name="totaltindakanrad" class="form-control" value="<?= number_format($totalrad)?>" readonly>  
                    </div>
                    <div class="form-group">
                        <label>Total Tindakan Laboratorium</label>
                        <input type="text" name="totaltindakanlab" class="form-control" value="<?= number_format($totallab)?>" readonly>  
                    </div>
                    <div class="form-group">
                        <label>Total Obat</label>
                        <input type="text" name="totaltindakanumum" class="form-control" value="<?= number_format($totalobat)?>" readonly>  
                    </div>  

                    

                     
                </div>

                <div class="col-md-6"> 
                    <div class="form-group">
                        <label for="subtotal" class="active">Sub Total<span style="color: red;"> *</span></label>
                        <input id="subtotal" type="text" name="subtotal" class="form-control" value="<?php echo number_format($subtotal); ?>" readonly>
                        <input id="subtotal_val" type="hidden" name="subtotal_val" value="<?php echo $subtotal; ?>" readonly>
                        <input id="total_value" type="hidden" name="total_value" value="<?php echo $subtotal; ?>" readonly>    
                    </div>
                    <div class="form-group">
                        <label for="discount" class="active">Discount (%)</label> 
                        <input id="discount" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" name="discount" class="form-control no" maxlength="2" placeholder="0" onkeyup="hitungHarga()"> 
                    </div>
                    <div class="form-group">
                        <label for="total_bayar" class="active">Total Bayar<span style="color: red;"> *</span></label>
                        <input id="total_bayar" type="text" name="total_bayar" class="form-control" value="<?php echo number_format($subtotal); ?>" readonly>
                        <input id="total_bayar_val" type="hidden" name="total_bayar_val" value="<?php echo $subtotal; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="cara_bayar" class="active">Cara Bayar<span style="color: red;"> *</span></label>
                        <input id="cara_bayar" type="text" name="cara_bayar" class="form-control" value="<?php echo $list_pasien->type_pembayaran; ?>" readonly> 
                    </div> 

                    <input type="hidden" id="byr_pendaftaran_id" name="byr_pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>" readonly> 
                    <input type="hidden" name="byr_pasien_id" id="byr_pasien_id" value="<?php echo $list_pasien->pasien_id; ?>" readonly> 
                </div> 
            </div> 
            <div class="row">    
                        <button  type="button" class="btn btn-success " id="savePembayaran"><i class="fa fa-floppy-o"></i> Draft </button> 
                        <button style="margin-left: 12px" type="button" class="btn btn-default " id="print"><i class="fa fa-print"></i> PRINT KWITANSI</button>  


                        <!-- KONDISI JIKA ADA DATA ASURANSI 1 : MAKA TAMPILKAN -->
                        <?php if(!empty($data_asuransi->nama_asuransi)) { ?> 
                        
                        <!-- DEKLARASI VARIABLE UNTUK PARAMETER Printkwitansi_asuransi -->
                        <?php    
                            $pasien_id      = !empty($list_pasien->pasien_id)       ? $list_pasien->pasien_id :'null' ; 
                            $pendaftaran_id = !empty($list_pasien->pendaftaran_id)  ? $list_pasien->pendaftaran_id : 'null'; 
                            $detail_id      = !empty($list_pasien->det_tindakan_id) ? $list_pasien->det_tindakan_id :'null';              
                            // $detail_obat_id = !empty($list_pasien->det_obat_id)     ? $list_pasien->det_obat_id :'';       
                        ?>  
                        <button style="margin-left: 12px" type="button" class="btn btn-default " id="printAsuransi1" onclick="printkwitansi_asuransi(<?= $pasien_id?>,<?= $pendaftaran_id ?>,<?= $detail_id ?>,'<?= $data_asuransi->nama_asuransi?>')" disabled><i class="fa fa-print p-r-10"></i><?= $data_asuransi->nama_asuransi ?></button>                
                        <?php } ?>   
                        <!-- END KONDISI ASURANSI 1 -->

                        <!-- KONDISI JIKA ADA DATA ASURANSI 2 : MAKA TAMPILKAN -->
                        <?php if(!empty($data_asuransi->nama_asuransi2)){ ?>    
                        <button style="margin-left: 12px" type="button" class="btn btn-default " id="printAsuransi2" onclick="printkwitansi_asuransi(<?= $pasien_id?>,<?= $pendaftaran_id ?>,<?= $detail_id ?>,'<?= $data_asuransi->nama_asuransi2 ?>')" disabled><i class="fa fa-print p-r-10"></i><?= $data_asuransi->nama_asuransi2 ?></button>       
                        <?php } ?>
                        <!-- END KONDISI ASURANSI 2 -->       
                        <button style="margin-left: 12px" type="button" class="btn btn-default " id="printNota" disabled><i class="fa fa-print" ></i> PRINT NOTA</button>    
                        <!-- <button type="button" class="btn btn-success" id="printNota" onclick="printDetail_Kwitansi()">PRINT NOTA</button>     -->

                </div>  
            </div>
        </div>      
    </div>

    <?= form_close()?>
   

</div>
<!-- end whitebox -->
<?php $this->load->view('footer_iframe');?>        
<script src="<?php echo base_url()?>assets/dist/js/pages/kasir/kasirrj/generaterj.js"></script>
 

<div id="modal_diskon" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">     
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Diskon</b></h2> </div>  
            <div class="modal-body" style="background: #fafafa">      
                <div class="white-box col-md-12">   
                  <ul class="nav customtab nav-tabs" role="tablist">
                        <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#persen" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Percent (%)</span></a></li>
                        <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#nominal" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Nominal</span></a></li>
                    </ul>
                     <div class="tab-content">
                        <!-- <input type="hidden" name="tindakanpasien_id" id="tindakanpasien_id" value="<?= $tindakan->tindakanpasien_id ?>">       -->
                        <!-- <input type="hidden" name="reseptur_id" id="reseptur_id" value="<?= $tindakan->tindakanpasien_id ?>">         -->
                        <input type="hidden" name="tindakanpasien_id" id="tindakanpasien_id">       
                        <input type="hidden" name="reseptur_id" id="reseptur_id">                 
                        <div role="tabpanel" class="tab-pane fade active in" id="persen">  
                            <div class="col-md-12" align="center"> 
                                <div class="col-md-1"></div>
                                <div class="form-group col-md-8">       
                                    <input id="diskon_persen" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" name="diskon_persen" class="form-control no" maxlength="2" placeholder="0" style="float: right;">
                                </div>    
                                <div class="col-md-2">  
                                    <button type="button" class="btn btn-success" id="btnDiskonPersen" onclick="hitungdiskon('persen')"><i class="fa fa-plus"></i> TAMBAH</button>      
                                </div>  
                            </div>   
                        </div>           
                        <div role="tabpanel" class="tab-pane fade in" id="nominal">
                            <div class="col-md-12" align="center">
                                <div class="col-md-1"></div>                                                    
                                <div class="form-group col-md-8">     
                                    <input type="number" id="diskon_nominal" name="diskon_nominal" class="form-control" placeholder="Nominal" style="float: right;" />      
                                </div> 
                                <div class="col-md-2">     
                                    <button type="button" class="btn btn-success" id="btnDiskonNominal" onclick="hitungdiskon('nominal')"><i class="fa fa-plus"></i> TAMBAH</button>    
                                </div>     
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


</body> 
</html> 



