<?php
//    if ($tot_obat == 0) {
//        $surcharge = 0;
//    } else if ($cara_bayar == "Tanggungan Hotel") {
//		$surcharge = 0;
//	} else {
//		$surcharge = (($tot_lab->total_harga + $tot_rad->total_harga + $tot_tind->total_harga + $tot_obat + $feeDokter['total_harga']) * 0.03);
//	}

	$surcharge = $get_pembayar->surcharge;;
	
//	echo $surcharge;
//	exit;
?>

<!DOCTYPE html>
 <html>
<head> 
<title>Nota Pembayaran</title>
<!-- http://localhost/bhaktivedanta/kasir/kasirrj/print_kwitansi/70 -->
<style type="text/css" rel="stylesheet">
      
      body{
        padding-right: 10px;

      }
			table {
				border-collapse: collapse;
				/*text-align: center center;*/
			}
      p{
        line-height: 5%;
        font-size: 16px;
      }
      .judul-invoice {
        font-size:30px;
        font-style:bold;
      }
      .body-invoice {
        font-size:24px;
        font-style:bold;
      }
      .rounded-box-invoice {
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
      }

    .table-invoice {
    border-collapse:separate;
    border:solid black 1px;
    border-radius:6px;
    -moz-border-radius:6px;
    }
    .table-invoice td, th {
        border-left:solid black 1px;
        border-top:solid black 1px;
    }

    .table-invoice th {
        background-color: blue;
        border-top: none;
    }

    .table-invoice td:first-child, th:first-child {
        border-left: none;
    }
      
    label {
      display: block;
      padding-left: 15px;
      text-indent: -15px;
    }
    input {
      width: 30px;
      height: 30px;
      padding: 0;
      margin-left:10px;
      margin-right:10px;
      margin-top:0;
      margin-bottom:0;
      vertical-align: bottom;
      position: relative;
      top: -1px;
      *overflow: hidden;
    }
    
  

    
  
	</style> 
</head> 
   
<body onload="window.print()">
<!--<body>-->

  <table  width="900" border="0" align="center">
  <tr>
      <td align="center"><img src="<?php echo base_url() ?>assets/plugins/images/header_kwitansi.png" alt=""></td>
  </tr>
  <tr>
      <td align="center">
      <?php if ($this->session->tempdata('data_session')[1] == 'Bhaktivedanta Medical Seminyak') { ?>
          <p><small>Jl. Raya Petitenget No.1X-seminyak, kerobokan, badung, Kerobokan Kelod, Kec. Kuta Utara, Kabupaten Badung, Bali 80361 </small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
          <p><small>TELP (0361) 9343811</small></p> <!-- CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
          <p><small>Website:www.bvmedicalbali.com E-mail:bvmedicalbali@gmail.com</small></p>
          <!-- <p><small>Website://puribundamalang.com E-mail:puribunda.malang@yahoo.com</small></p> CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
          <!-- <p>BALI</p> CASEMIX INA-CBG JAMINAN KESEHATAN NASIONAL -->
      <?php } ?>
      <?php if ($this->session->tempdata('data_session')[1] == 'Bhaktivedanta Medical Petitenget') { ?>
          
      <?php } ?>
      </td>
  </tr>
  <tr> 
      <td align="center" valign="top"><hr style="height: 1px;background-color: #000;"></td>
  </tr> 
  </table>
  
  <table class="judul-invoice" width="900" border="0" align="center">
  <tr>
      <td align="right" style="width:50%"> INVOICE </td> <td align="center"> No :</td> <td> <?php echo $total_bayar->no_pembayaran ?></td> 
  </tr>
  
  </table>

  <table class="body-invoice" width="900" border="0" align="center" style="margin-top:20px;" cellpadding="5">
  <tr>
      <td align="left" style="width:18%" > Receive From </td> <td align="center"> : </td> <td> <?php echo $pasienrj->pasien_nama ?></td> 
  </tr>
  <tr>
      <td align="left" style="width:18%"> The total of </td> <td align="center"> : </td>
      <td><?= $total_bayar->total_bayar != 0 ? "Rp. ". number_format($total_bayar->total_bayar) : "" ?></td>
  </tr>

  <tr>
      <td align="left" style="width:18%"> In Payment of </td> <td align="center"> : </td> 
  </tr>

  </table>
  
  <table width="900"  cellpadding="2" align="center" style="border:1px solid black;margin-top:10px"> 
    <tr>  
      <td width="264" style="border:1px solid black"><div align="center">Item Description</div></td>
      <td width="33" style="border:1px solid black"><div align="center">Qty</div></td>
      <td width="200" style="border:1px solid black"><div align="center">Unit Price</div></td> 
      <td width="200" style="border:1px solid black"><div align="center">Disc </div></td> 
      <td width="136" style="border:1px solid black"><div align="center">Amount</div></td>
    </tr>

      <?php
      $umum = 0;
      foreach($tindakan_list as $list) { if ($list->type_tindakan=="TINDAKAN") $umum++; }
      ?>
      <tr>
          <td><b>Medical Action :</b><br></td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?= $umum < 1 ? '-' : '&nbsp;' ?></td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: right;"><?= $umum < 1 ? '-' : '&nbsp;' ?></td>
          <td style="border-right: 1px solid black;text-align: right;"><?= $umum < 1 ? '-' : '&nbsp;' ?></td>
          <td style="border-right: 1px solid black;text-align: right;"><?= $umum < 1 ? '-' : '&nbsp;' ?></td>
      </tr>
    

    <!-- type == tindakan khusus -->

      
      <?php  
        $no = 0;
      foreach($tindakan_list as $list){ 
        $no++;
        if ($list->type_tindakan=="TINDAKAN") {
      ?> 
      <tr>  
      <td><?php echo $list->daftartindakan_nama ?></td>
      <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->jml_tindakan; ?></td>
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_baru)?></td> 
      <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->diskon)?></td>  
      <td style="text-align: right;"><?php echo "Rp. ".number_format($list->total_harga) ?></td>
    </tr>  
    <?php  
        }
      }
     ?>

        <?php
            $lab = 0;
            foreach($tindakan_list as $list) {
                if ($list->type_tindakan=="LABORATORIUM") {
                    $lab++;
                }
            }
        ?>

      <tr>  
        <td><b>Laboratorium :</b><br></td>
        <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?= $lab < 1 ? '-' : '&nbsp;' ?></td>
        <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: right;"><?= $lab < 1 ? '-' : '&nbsp;' ?></td>
        <td style="border-right: 1px solid black;text-align: right;"><?= $lab < 1 ? '-' : '&nbsp;' ?></td>
        <td style="border-right: 1px solid black;text-align: right;"><?= $lab < 1 ? '-' : '&nbsp;' ?></td>
      </tr>
    

    <!-- type == tindakan khusus -->


      <?php
      $no = 0;
          foreach($tindakan_list as $list){
            $no++;
            if ($list->type_tindakan=="LABORATORIUM") {
          ?>
          <tr>
          <td><?php echo $list->daftartindakan_nama ?></td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->jml_tindakan; ?></td>
          <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_tindakan)?></td>
          <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->diskon)?></td>
          <td style="text-align: right;"><?php echo "Rp. ".number_format($list->jml_tindakan*$list->harga_tindakan) ?></td>
        </tr>
        <?php
            }
          }
     ?>

      <?php
      $no = $total_harga_satuan = $total_diskon_obat = $total_harga_obat = $qty_obat = 0;
      foreach($obat as $key => $value) {
          $total_harga_satuan = $total_harga_satuan + ($value['harga_satuan'] * $value['jumlah_obat']);
          $total_diskon_obat = $total_diskon_obat + $value['diskon'];
          $total_harga_obat = $total_harga_obat + $value['harga_total'];
          $qty_obat = $qty_obat + 1;
      }

//      if ($get_pembayar->cara_bayar != 0 && $get_pembayar->cara_bayar != '') {
//
//      }
      ?>

      <tr>
          <td><b>Medicine :</b><br></td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?= $total_harga_satuan != 0 ? $qty_obat : "-" ?></td>
          <td style="border-right: 1px solid black;text-align: right;"><?= $total_harga_satuan != 0 ? "Rp. ".number_format($total_harga_satuan + $surcharge) : "-" ?></td>
          <td style="border-right: 1px solid black;text-align: right;"><?= $total_harga_satuan != 0 ? "Rp. ".number_format($total_diskon_obat) : "-" ?></td>
          <td style="text-align: right;"><?= $total_harga_satuan != 0 ? "Rp. ".number_format($total_harga_obat + $surcharge) : "-" ?></td>
      </tr>

      <tr>
          <td><b>Doctor Fee :</b><br></td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"> <?= $feeDokter != 0 ? 1 : "" ?> </td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: right;"><?= $feeDokter != 0 ? "Rp. ". number_format($feeDokter['harga']) : "-" ?></td>
          <td style="border-right: 1px solid black;text-align: right;"><?= $feeDokter != 0 ? "Rp. ". number_format($feeDokter['diskon']) : "-" ?></td>
          <td style="text-align: right;"><?= $feeDokter != 0 ? "Rp. ". number_format($feeDokter['total_harga']) : "-" ?></td>
      </tr>

    </table>
    <table class="body-invoice" width="900" border="0" align="center" cellpadding="2">
      <tr>
          <td class="rounded-box-invoice" align="left" style="width:60%" >

            <table class="table-invoice" width="300" border="1" align="left" cellpadding="2" height="75">
            </table>

          </td>

          <?php
            if (($cara_bayar == "Pribadi") || ($cara_bayar == "Asuransi") || ($cara_bayar == "Bill Hotel") || ($cara_bayar == "Tanggungan Hotel")) {
                $get_pembayar->cara_bayar = "";
            }
          ?>

          <td>
            <div class="checkbox" style="padding:5px;margin-top:10px">
              <label><input type="checkbox" value="" <?= $cara_bayar == "Pribadi" ? "checked" : ($get_pembayar->cara_bayar == "Pribadi" ? 'checked' : '') ?>>Pribadi</label>
            </div>
            <div class="checkbox" style="padding:5px">
              <label><input type="checkbox" value="" <?= $cara_bayar == "Asuransi" ? "checked" : ($get_pembayar->cara_bayar == "Asuransi" ? 'checked' : '') ?>>Asuransi</label>
            </div>
            <div class="checkbox" style="padding:5px">
              <label><input type="checkbox" value="" <?= $cara_bayar == "Bill Hotel" ? "checked" : ($get_pembayar->cara_bayar == "Bill Hotel" ? 'checked' : '') ?>>Bill Hotel</label>
            </div>
<!--              <div class="checkbox" style="padding:5px">-->
<!--                  <label><input type="checkbox" value="" --><?//= $cara_bayar == "Tanggungan Hotel" ? "checked" : ($get_pembayar->cara_bayar == "Tanggungan" ? 'checked' : '') ?><!--Tanggungan Hotel</label>-->
<!--              </div>-->
          </td> 
      </tr>

      <tr>
          <td class="rounded-box-invoice" align="left" style="width:60%;" >
              Badung, <?php echo date('d M Y'); ?>
          </td>  
      </tr>

      <tr>
          <td class="rounded-box-invoice" align="left" style="width:50%;margin-top:20px;margin-bottom:20px">
              Doctor on Duty,<br><br><br>
              (<?php echo $pasienrj->NAME_DOKTER; ?>)
          </td>

          <td class="rounded-box-invoice" align="center" style="width:50%;" >
              Patient,<br><br><br>
              (<?php echo $pasienrj->pasien_nama ?>)
          </td>  
      </tr>
      
  </table>
<p>&nbsp;</p>
</body>

</html>


<?php

function Terbilang($x){
      $abil = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
      if ($x < 12)
        return " " . $abil[$x];
      elseif ($x < 20)
        return Terbilang($x - 10) . "BELAS";
      elseif ($x < 100)
        return Terbilang($x / 10) . " PULUH" . Terbilang($x % 10);
      elseif ($x < 200)
        return " SERATUS" . Terbilang($x - 100);
      elseif ($x < 1000)
        return Terbilang($x / 100) . " RATUS" . Terbilang($x % 100);
      elseif ($x < 2000)
        return " SERIBU" . Terbilang($x - 1000);
      elseif ($x < 1000000)
        return Terbilang($x / 1000) . " RIBU" . Terbilang($x % 1000);
      elseif ($x < 1000000000)
        return Terbilang($x / 1000000) . " JUTA" . Terbilang($x % 1000000);
    }
?>