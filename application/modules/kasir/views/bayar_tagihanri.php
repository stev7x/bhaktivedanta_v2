<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row">  
        <div class="col-md-12">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">   
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr> 
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->poli_ruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    

                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td>  
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->umur; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr> 
                                        <td><b>Lama Pemeriksaan</b></td>  
                                        <td>:</td>           
                                        <td style="padding-left: 15px"><?= $list_pasien->lama_rawat_hari; ?> Hari</td>
                                    </tr> 

                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->poli_ruangan; ?>" readonly>  
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>    
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4 align="center">Jasa Dokter</h4>
            <table id="table_jasadokter_kasir" class="table table-striped table-hover" cellspacing="0">
                <thead>
                    <tr>
                        <th>Tgl. Tindakan</th>
                        <th>Nama Tindakan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Cyto</th>
                        <th>Total + Cyto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5">No Data to Display</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-12">
            <h4 align="center">Jasa Perawatan</h4>
            <table id="table_jasaperawatan_kasir" class="table table-striped table-hover" cellspacing="0">
               <thead>
                    <tr>
                        <th>Tgl. Tindakan</th>
                        <th>Nama Tindakan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Cyto</th>
                        <th>Total + Cyto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5">No Data to Display</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h4 align="center">Tindakan Khusus</h4>
            <table id="table_tindakan_kasir" class="table table-striped table-hover" cellspacing="0">
                <thead>
                    <tr>
                        <th>Tgl. Tindakan</th>
                        <th>Nama Tindakan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Cyto</th>
                        <th>Total + Cyto</th> 
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5">No Data to Display</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row"> 
        <div class="col-md-12">
            <h4 align="center">Tindakan Radiologi</h4>
            <table id="table_tindakanrad_kasir" class="table table-striped table-hover" cellspacing="0">
                <thead>
                    <tr>
                        <th>Tgl. Tindakan</th>
                        <th>Nama Tindakan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Cyto</th>
                        <th>Total + Cyto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5">No Data to Display</td>
                    </tr>
                </tbody> 
            </table>
        </div>
    </div>

    <div class="row">  
        <div class="col-md-12">
            <h4 align="center">Tindakan Laboratorium</h4>
            <table id="table_tindakanlab_kasir" class="table table-striped table-hover" cellspacing="0">
                <thead>
                    <tr>
                        <th>Tgl. Tindakan</th>
                        <th>Nama Tindakan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Cyto</th>
                        <th>Total + Cyto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5">No Data to Display</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> 

    <div class="row">  
        <div class="col-md-12">
            <h4 align="center">Obat</h4>
            <table id="table_obat_kasir" class="table table-striped table-hover" cellspacing="0">
                 <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Nama Obat</th>
                        <th>Satuan</th>
                        <th>Signa</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="7">No Data to Display</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> 

    <div class="row">  
        <div class="col-md-12">
            <h4 align="center">BHP</h4>
            <table id="table_bhp_kasir" class="table table-striped table-hover" cellspacing="0">
                 <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Nama BHP</th>
                        <th>Satuan</th>  
                        <th>Signa</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>

                    </tr>
                </thead>
                <tbody> 
                    <tr>
                        <td colspan="7">No Data to Display</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div> 

    <div class="row">  
        <div class="col-md-12">
            <h4 align="center">Alkes</h4>
            <table id="table_alkes_kasir" class="table table-striped table-hover" cellspacing="0">
                 <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Nama Alkes</th>
                        <th>Satuan</th>
                        <th>Signa</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>

                    </tr>
                </thead>
                <tbody>
                    <tr> 
                        <td colspan="7">No Data to Display</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>  

    <div class="row">  
        <div class="col-md-12">
        <h4 align="center">Input Pembayaran</h4>
            <?php
                            $totaljasadokter = !empty($total_jasadokter->total_harga) ? $total_jasadokter->total_harga : 0;
                            $totaljasaperawatan = !empty($total_pelayananri->total_harga) ? $total_pelayananri->total_harga : 0;
                            $totaltindakainlain = !empty($total_tindakanlain->total_harga) ? $total_tindakanlain->total_harga : 0;
                            $totalrad = !empty($total_rad->total_harga) ? $total_rad->total_harga : 0;
                            $totallab = !empty($total_lab->total_harga) ? $total_lab->total_harga : 0;
                            $totalobat = !empty($total_obat->total_harga) ? $total_obat->total_harga : 0;
                            $totalbhp = !empty($total_bhp->total_harga) ? $total_bhp->total_harga : 0;
                            $totalalkes = !empty($total_alkes->total_harga) ? $total_alkes->total_harga : 0;
                            $subtotal = $totaljasadokter+$totaljasaperawatan+$totaltindakainlain+$totalrad+$totallab+$totalobat+$totalbhp+$totalalkes;
                            ?>   
            <?php echo form_open('#',array('id' => 'fmPembayaran'))?> 
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="tgl_pembayaran" class="active">Tgl. Pembayaran</label>
                        <input id="tgl_pembayaran" type="text" name="tgl_pembayaran" class="form-control" value="<?php echo  date('d-M-Y'); ?>" readonly>
                    </div>

                    <div class="form-group">   
                        <label for="total_jasadokter" class="active">Total Jasa Dokter</label>
                       <input id="total_jasadokter" type="text" name="total_jasadokter" class="form-control" value="<?php echo number_format($totaljasadokter); ?>" readonly>
                            <input id="total_jasadokter_val" type="hidden" name="total_jasadokter_val" value="<?php echo $totaljasadokter; ?>" readonly>
                    </div>

                    <div class="form-group"> 
                        <label for="total_jasaperawatan" class="active">Total Jasa Perawatan</label>
                        <input id="total_jasaperawatan" type="text" class="form-control" name="total_jasaperawatan" value="<?php echo number_format($totaljasaperawatan); ?>" readonly>
                        <input id="total_jasaperawatan_val" type="hidden" name="total_jasaperawatan_val" value="<?php echo $totaljasaperawatan; ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label for="total_tindakanlain" class="active">Total Tindakan Khusus</label>
                        <input id="total_tindakanlain" type="text" class="form-control" name="total_tindakanlain" value="<?php echo number_format($totaltindakainlain); ?>" readonly>
                        <input id="total_tindakanlain_val" type="hidden" name="total_tindakanlain_val" value="<?php echo $totaltindakainlain; ?>" readonly>  
                    </div>

                    <div class="form-group">
                        <label for="total_rad" class="active">Total Tindakan Radiologi</label>
                        <input id="total_rad" type="text" class="form-control" name="total_rad" value="<?php echo number_format($totalrad); ?>" readonly>
                        <input id="total_rad_val" type="hidden" name="total_rad_val" value="<?php echo $totalrad; ?>" readonly>
                    </div> 
                    <div class="form-group">
                        <label for="total_lab" class="active">Total Tindakan Laboratorium</label>
                        <input id="total_lab" type="text" class="form-control" name="total_lab" value="<?php echo number_format($totallab); ?>" readonly>
                        <input id="total_lab_val" type="hidden" name="total_lab_val" value="<?php echo $totallab; ?>" readonly> 
                    </div>
                </div>

                <div class="col-md-6">  
                    <div class="form-group">
                        <label for="total_obat" class="active">Total Obat</label>
                        <input id="total_obat" type="text" class="form-control" name="total_obat" value="<?php echo number_format($totalobat); ?>" readonly>
                        <input id="total_obat_val" type="hidden" name="total_obat_val" value="<?php echo $totalobat; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="total_bhp" class="active">Total BHP</label>
                        <input id="total_bhp" type="text" class="form-control" name="total_bhp" value="<?php echo number_format($totalbhp); ?>" readonly>
                        <input id="total_bhp_val" type="hidden" name="total_bhp_val" value="<?php echo $totalbhp; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="total_alkes" class="active">Total Alkes</label>
                        <input id="total_alkes" type="text" class="form-control" name="total_alkes" value="<?php echo number_format($totalalkes); ?>" readonly>
                        <input id="total_alkes_val" type="hidden" name="total_alkes_val" value="<?php echo $totalalkes; ?>" readonly>   
                    </div> 
                    <div class="form-group">
                        <label for="subtotal" class="active">Sub Total<span style="color: red;"> *</span></label>
                        <input id="subtotal" type="text" name="subtotal" class="form-control" value="<?php echo number_format($subtotal); ?>" readonly>
                        <input id="subtotal_val" type="hidden" name="subtotal_val" value="<?php echo $subtotal; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="discount" class="active">Discount (%)</label>
                        <input id="discount" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" name="discount" class="form-control no" maxlength="2" placeholder="0" onkeyup="hitungHarga()">
                    </div>
                    <div class="form-group">
                        <label for="total_bayar" class="active">Total Bayar<span style="color: red;"> *</span></label>
                        <input id="total_bayar" type="text" name="total_bayar" class="form-control" value="<?php echo number_format($subtotal); ?>" readonly>
                        <input id="total_bayar_val" type="hidden" name="total_bayar_val" value="<?php echo $subtotal; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="cara_bayar" class="active">Cara Bayar<span style="color: red;"> *</span></label>
                        <input id="cara_bayar" type="text" name="cara_bayar" class="form-control" value="<?php echo $list_pasien->type_pembayaran; ?>" readonly>
                    </div>

                    <input type="hidden" id="byr_pendaftaran_id" name="byr_pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>" readonly> 
                    <input type="hidden" name="byr_pasien_id" id="byr_pasien_id" value="<?php echo $list_pasien->pasien_id; ?>" readonly> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="col-md-3">   
                        <button type="button" class="btn btn-success col-sm-12" id="savePembayaran"><i class="fa fa-floppy-o"></i> SIMPAN</button>
                    </div>
                    <div class="col-md-3">      
                        <button type="button" class="btn btn-default col-sm-12" id="print" disabled><i class="fa fa-print"></i> PRINT KWITANSI</button> 
                    </div>
                    <div class="col-md-3">      
                        <button type="button" class="btn btn-default col-sm-12" id="printNota" disabled><i class="fa fa-print"></i> PRINT NOTA</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>


   

</div>
<!-- end whitebox -->


<?php $this->load->view('footer_iframe');?>        
<script src="<?php echo base_url()?>assets/dist/js/pages/kasir/kasirri/bayar_tagihanri.js"></script>
</body>
 
</html> 



