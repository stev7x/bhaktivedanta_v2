<!DOCTYPE html >
<head>
<title>Detail Pemeriksaan Pasien</title>
<style type="text/css" rel="stylesheet">
			body{
			padding:1;
			}
			table {
	border-collapse: collapse;
	white-space: normal;				/*text-align: center center;*/
			}

	</style>
</head>
<body onLoad="window.print()">
<table width="100%" border="0" cellpadding="1">
  <tr>
    <td width="18%" rowspan="5" align="center">LOGO</td>
    <td width="65%" align="center">RUMAH SAKIT IBU DAN ANAK</td>
    <td width="17%" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&quot; PURI BUNDA &quot;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">Jl. Simpang Sulfat Utara No. 60 A Malang</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">Phone ( 0341) 480047</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
</table>
<hr width="100%" color="#000000">
<table width="100%" border="0" cellpadding="1" cellspacing="5">
  <tr>
    <td colspan="3" align="right">09 September 2017</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><strong>PERINCIAN BIAYA PERAWATAN</strong></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td width="17%">Nama</td>
    <td width="1%">:</td>
    <td width="82%">&nbsp;</td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Kelas Pelayanan </td>
    <td>:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>No. Reg </td>
    <td>:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Tgl. Perawatan</td>
    <td>:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<hr width="100%" color="#000000">
<table width="100%" border="0" cellpadding="1">
  <tr>
    <td colspan="8"><em><strong>Honor Dokter</strong></em></td>
  </tr>
  <tr>
    <td width="10%">&nbsp;</td>
    <td colspan="2">Dr Sp. OG / Opr. Bedah</td>
    <td width="1%">:</td>
    <td width="2%">Rp.</td>
    <td width="15%" align="right">&nbsp;</td>
    <td width="3%">&nbsp;</td>
    <td width="18%" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Assisten Opr. Bedah</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Dr Anasthesia</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Dr Spesialis Anak</td>
    <td>:</td>
    <td >Rp.</td>
    <td style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><em><strong>Total Honor Dokter</strong></em></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Rp .</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><strong><em>Persiapan Pre Operasi</em></strong></td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Obat dan BHP Pre Operasi</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Jasa Paramedis</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><strong><em>Total Biaya Persiapan Pre Operasi</em></strong></td>
    <td width="27%">&nbsp;</td>
    <td>:</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><strong><em>Operasi</em></strong></td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="24%">Sewa OK</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa Paramedis Instrument</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa On Loop</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Obat dan BHP OK (Anasthesia)</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa Perawat Resusitasi Bayi</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right" >&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Oksigen</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><strong><em>Total Biaya Operasi</em></strong></td>
    <td>:</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td height="22" colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><em><strong>Post Operasi</strong></em></td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa Perawat Ruangan Ibu </td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa Perawat Ruangan Bayi</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Perawatan Ibu</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Perawatan Bayi</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa Visit Dr. Sp. OG</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa Konsul Dr. Sp. OG</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa Visit Dr. Sp. A</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Jasa Konsul Dr. Sp. A</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Obat dan BHP Pasca Operasi Ibu</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Obat dan BHP Pasca Operasi Bayi</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya NST</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Lab</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Ambulance </td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Biaya Pemeriksaan UGD</td>
    <td>&nbsp;</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><strong><em>Total Biaya Perawatan Post Operasi</em></strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><em><strong>Jasa Operasional Puri Bunda</strong></em></td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><em><strong>TOTAL</strong></em></td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><em><strong>TOTAL BIAYA PERAWATAN</strong></em></td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td><em><strong>Terbilang : </strong></em></td>
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8"><em>Nota ini sebagai lampiran dari Kwitansi</em></td>
  </tr>
</table>

</body>
</html>
