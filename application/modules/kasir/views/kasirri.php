<?php $this->load->view('header');?>
<title>SIMRS | Kasir Rawat Inap</title>
<?php $this->load->view('sidebar');?>
      <!-- START CONTENT -->
<section id="content">
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Kasir Rawat Inap</h5>
            <ol class="breadcrumbs">
                <li><a href="#">Kasir</a></li>
                <li class="active">Kasir Rawat Inap</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->


    <!--start container-->
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header">Pasien Belum Bayar</h4>
                        <!--<input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />-->
                        <div id="card-alert" class="card green notif" style="display:none;">
                            <div class="card-content white-text">
                                <p id="card_message">SUCCESS : The page has been added.</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
<!--                        <div class="col s4">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="text" class="datepicker" name="tgl_awal" id="tgl_awal" placeholder="Tanggal Awal">
                                    <label for="tgl_awal">Dari</label>
                                </div>
                                <div class="input-field col s12">
                                    <input type="text" class="datepicker" name="tgl_akhir" id="tgl_awal" placeholder="Tanggal Akhir">
                                    <label for="tgl_akhir">Ke</label>
                                </div>
                            </div>
                        </div>-->
                        <div id="table-datatables">
                            <div class="row">
                                <div class="col s12 m4 l12">
                                    <table id="table_list_kasirri" class="responsive-table display" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Ruangan</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>No. Pendaftaran</th>
                                                <th>Tanggal Pendaftaran</th>
                                                <th>No. Rekam Medis</th>
                                                <th>Nama Pasien</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur Pasien</th>
                                                <th>Alamat Pasien</th>
                                                <th>Pembayaran</th>
                                                <th>Status Rawat</th>
                                                <th>Bayar Tagihan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="13">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<!-- Start Modal Add Paket -->
<div id="modal_bayar_kasirri" class="modal" style="width: 80% !important ; max-height: 90% !important ;">
    <!--<div class="modal-dialog" role="document">-->
    <div class="modal-content">
        <h1>Bayar Tagihan</h1>
        <div class="divider"></div>
        <div id="card-alert" class="card green modal_notif" style="display:none;">
            <div class="card-content white-text">
                <p id="modal_card_message"></p>
            </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="col s12 formValidate">
<!--            <div class="row">
                <div class="input-field col s12">
                    <input type="text" id="nama_paket" name="nama_paket" class="validate">
                    <label for="nama_lengkap">Nama Paket Operasi<span style="color: red;"> *</span></label>
                </div>
            </div>-->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
            </div>
        </div>
    </div>
    <!--</div>-->
    <div class="modal-footer">
        <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close" onclick="reloadTableKasir()">Close<i class="mdi-navigation-close left"></i></button>
    </div>
</div>
    <!-- End Modal Add Paket -->
<?php $this->load->view('footer');?>