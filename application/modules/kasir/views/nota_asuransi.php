<!DOCTYPE html>
 <html>
<head> 
<title>Nota Pembayaran</title>
<style type="text/css" rel="stylesheet">
      body{
        padding-right: 10px;

      }
      table {
        border-collapse: collapse;
        /*text-align: center center;*/ 
      }
  
  </style>
</head> 
     
<body onload="window.print()">
<!-- <body > -->
<table  width="900" border="0" cellpadding="2" align="center">
  <tr>
    <td colspan="7"><img src="<?php echo base_url() ?>assets/plugins/images/header.png" alt=""></td>        
    <td height="30" colspan="9"><h2 style="text-align: center;">NOTA PEMBAYARAN <br><?= $nama_asuransi ?></h2></td> 
    <!-- <td width="305"><p><b>RSIA Puri Bunda</b>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          Jl. Simpang Sulfat Utara No.60A     
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         Malang</p></td>  -->
    </tr>
  <tr>
    <td width="37">&nbsp;</td>
    <td width="113">&nbsp;</td>
    <td width="3">&nbsp;</td>
    <td width="340">&nbsp;</td>
    <td colspan="3" rowspan="3">&nbsp;</td>
    <td width="73">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>  
  <tr>
    <td>&nbsp;</td>
    <td>Nama Pasien </td>
    <td>:</td>
    <td><?php echo $pasienrj->pasien_nama; ?></td>
    <td>Tanggal </td>
    <td width="3">:</td>
    <td width="242"><?php echo date('d M Y'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Alamat </td>
    <td>:</td>
    <td><?php echo $pasienrj->pasien_alamat; ?></td>
    <td>No. Nota</td>
    <td>:</td>
    <td></td>
  </tr>
</table>
 <br>  
<table width="900"  cellpadding="2" align="center" style="border:1px solid black;"> 
    <tr>   
      <td width="264" style="border:1px solid black"><div align="center">Item Description</div></td>
      <td width="33" style="border:1px solid black"><div align="center">Qyt</div></td>
      <td width="200" style="border:1px solid black"><div align="center">Unit Price</div></td>
      <td width="136" style="border:1px solid black"><div align="center">Amount</div></td>
    </tr> 
    <tr>  
        <tr>  
          <td><b>Tindakan Umum:</b><br></td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
          <td style="border-right: 1px solid black;text-align: right;">&nbsp;</td>
          <td>&nbsp;</td> 
        </tr>
         <?php 
         $total_tindakan_umum = 0;
         foreach($tindakan_umum as $list){

          ?>
          <tr>
          <td><?php echo $list->daftartindakan_nama?></td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $list->jml_tindakan; ?></td>
          <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($list->harga_tindakan)?></td> 
          <td style="text-align: right;"><?php echo "Rp. ".number_format($list->total_harga_tindakan) ?></td>
          <input type="hidden" name="tindakan[]" class="tindakan" value="<?= $list->total_harga_tindakan ?>" />
          </tr>
          <?php 
            $total_tindakan_umum += $list->total_harga_tindakan;
           } ?>     
        <tr>     
          <td><b>Obat:</b><br></td>
          <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;">&nbsp;</td>
          <td style="border-right: 1px solid black;text-align: right;">&nbsp;</td>
          <td>&nbsp;</td> 
        </tr> 
        <?php
        $total_obat =0;
         foreach($obat as $obat){ ?> 
        <tr>
        <td><?php echo $obat->nama_barang?></td>
        <td style="border-right: 1px solid black;border-left: 1px solid black;text-align: center;"><?php echo $obat->jml_obat; ?></td>
        <td style="border-right: 1px solid black;text-align: right;"><?php echo "Rp. ".number_format($obat->harga_jual)?></td>       h 
        <td style="text-align: right;"><?php echo "Rp. ".number_format($obat->total_harga) ?></td>
        <input type="hidden" name="obat[]" class="obat" value="<?= $obat->total_harga ?>" />    
        </tr>
        <?php $total_obat += $obat->total_harga;
          } ?>      
    </tr> 
    <tr>   
        <th style="text-align: left;border-right:1px solid black;border-top : 1px solid black" colspan="3"><b>Total Tindakan Umum  </b></th>   
        <th style="border-top: 1px solid black;text-align: right;"><span style="float: left;">Rp.</span><span align="right"> <?= number_format($total_tindakan_umum) ?></span></th> 
    </tr>     
    <tr>      
        <th style="text-align: left;border-right:1px solid black;border-top : 1px solid black" colspan="3"><b>Total Obat  </b></th>   
        <th style="border-top: 1px solid black;text-align: right;"><span style="float: left;">Rp.</span><span><?= number_format($total_obat) ?></span></th>  
    </tr>    
    <tr>      
        <?php $total_keseluruhan = $total_obat + $total_tindakan_umum ?>
        <th style="text-align: left;border-right:1px solid black;border-top : 1px solid black" colspan="3"><b>Total Keseluruhan  </b></th>   
        <th style="border-top: 1px solid black;text-align: right;"><span style="float: left;">Rp.</span><span><?= number_format($total_keseluruhan) ?></span></th> 
    </tr>      

   
    </table>

 



    <br>  
<!--     <table  width="900" border="1" cellpadding="2" align="center">
      <tr> 
        <th style="text-align: left;"><b>Total Tindakan Umum  </b></th>
        <th><?= "Rp. ".number_format($total_tindakan_umum) ?></th> 
      </tr> 
      <tr>
        <th style="text-align: left;"><b>Total Obat   </b></th>
        <th><?= "Rp. ".number_format($total_obat) ?></th> 
      </tr>
    </table> -->
 



 

<p>&nbsp;</p> 
</body>

  
</html>  


<?php

function Terbilang($x){
      $abil = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
      if ($x < 12)
        return " " . $abil[$x];
      elseif ($x < 20)
        return Terbilang($x - 10) . "BELAS";
      elseif ($x < 100)
        return Terbilang($x / 10) . " PULUH" . Terbilang($x % 10);
      elseif ($x < 200)
        return " SERATUS" . Terbilang($x - 100);
      elseif ($x < 1000)
        return Terbilang($x / 100) . " RATUS" . Terbilang($x % 100);
      elseif ($x < 2000)
        return " SERIBU" . Terbilang($x - 1000);
      elseif ($x < 1000000)
        return Terbilang($x / 1000) . " RIBU" . Terbilang($x % 1000);
      elseif ($x < 1000000000)
        return Terbilang($x / 1000000) . " JUTA" . Terbilang($x % 1000000);
    }
?>