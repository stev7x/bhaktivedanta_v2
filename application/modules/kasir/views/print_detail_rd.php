<!DOCTYPE html >
<head>
<title>Detail Pemeriksaan Pasien</title>
<style type="text/css" rel="stylesheet">
      body{
      padding:1;
      }
      table {
  border-collapse: collapse;
  white-space: normal;        /*text-align: center center;*/
      }

  </style>
</head>
<body onLoad="window.print()">
<table width="100%" border="0" cellpadding="1">
  <tr>      
    <td width="18%" rowspan="5" align="center">       
    <img src="<?= base_url() ?>assets/plugins/images/logo-puribunda.png" >
    </td> 
    <td width="65%" align="center">RUMAH SAKIT IBU DAN ANAK</td>
    <td width="17%" align="center">&nbsp;</td>
  </tr> 
  <tr>
    <td align="center">&quot; PURI BUNDA &quot;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">Jl. Simpang Sulfat Utara No. 60 A Malang</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">Phone ( 0341) 480047</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
</table>
<hr width="100%" color="#000000">
<table width="100%" border="0" cellpadding="1" cellspacing="5">
  <tr>
    <td colspan="3" align="right">09 September 2017</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><strong>PERINCIAN BIAYA PERAWATAN</strong></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td width="17%">Nama</td>
    <td width="1%">:</td>
    <td width="82%"><?php echo $pasienrj->pasien_nama; ?></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><?php echo $pasienrj->pasien_alamat; ?></td>
  </tr>
  <tr>
    <td>Kelas Pelayanan </td>
    <td>:</td> 
    <td><?php echo $pasienrj->kelaspelayanan_nama; ?></td>
  </tr>
  <tr>
    <td>No. Reg </td>
    <td>:</td>
    <td><?php echo $pasienrj->no_pendaftaran; ?></td>
  </tr>
  <tr>
    <td>Tgl. Perawatan</td>
    <td>:</td> 
    <td><?php echo $pasienrj->tgl_pendaftaran ?></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
<hr width="100%" color="#000000">
<table width="100%" border="0" cellpadding="1">
  <tr>
    <td colspan="8"><em><strong>Honor Dokter</strong></em></td>
  </tr>  
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="51" colspan="2">Jasa Visite Dokter Sp. OG</td>
    <td width="1%">:</td>
    <td width="2%">Rp.</td>
    <td width="15%" align="right">&nbsp;</td>
    <td width="3%">&nbsp;</td>
    <td width="18%" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Jasa Konsul Dokter S. OG</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Jasa Visite Dokter Sp. PD</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Jasa Konsul Doter Sp. PD</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Jasa Visite Dokter Sp. KG</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Jasa Visite Dokter Umum</td>
    <td>:</td>
    <td >Rp.</td>
    <td style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><em><strong>Total Honor Dokter</strong></em></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Rp .</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8"><em><strong>Perawatan</strong></em></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Kamar </td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Jasa Paramedis</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Obat dan BHP</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Makan</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Pasang Infus</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td width="10%">&nbsp;</td>
    <td width="51%" colspan="2">Biaya Pasang NGT</td>
    <td width="1%">:</td>
    <td width="2%">Rp.</td>
    <td width="15%" align="right">&nbsp;</td>
    <td width="3%">&nbsp;</td>
    <td width="18%" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Pasang Catheter</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Oksigen</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Lab</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya VT</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya USG</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Nebulizer</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya NST</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya EKG </td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Pasang IUD</td>
    <td>:</td>
    <td>Rp.</td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Biaya Pemeriksaan UGD</td>
    <td>:</td>
    <td >Rp.</td>
    <td style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><strong><em>Total Biaya</em></strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><em><strong>Jasa Operasional Puri Bunda</strong></em></td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><em><strong>TOTAL</strong></em></td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6"><em><strong>TOTAL BIAYA PERAWATAN</strong></em></td>
    <td>Rp.</td>
    <td align="right" style="border-bottom: 1px solid black;text-align: right;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td><em><strong>Terbilang : </strong></em></td>
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8"><em>Nota ini sebagai lampiran dari Kwitansi</em></td>
  </tr>
</table>

</body>
</html>
