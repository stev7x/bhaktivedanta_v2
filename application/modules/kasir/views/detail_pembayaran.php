
<!DOCTYPE html>
 <html>
<head> 
<title>Nota Pembayaran</title>
<style type="text/css" rel="stylesheet">
      
      body{
        padding-right: 10px; 

      }
			table {
				border-collapse: collapse;
			}
      .info_header {
        max-width: 300px; 
      }
      .margin-center {
        max-width: 90%;
        margin: auto;
        padding: 10px; 
        font-family: sans-serif;   
      }
      .margin-center2 {
        max-width: 70%;  
        margin: auto;
        padding: 10px;   
        font-family: sans-serif;   
      }

	</style> 
</head> 
<body onload="window.print()">
  <div class="margin-center">
    <table width="100%" >
      <tr> 
        <td width="20%"><img src="<?php echo base_url() ?>assets/plugins/images/header.png" alt="" width="100%"></td>
        <td width="80%" valign="bottom" style="text-align: center;" align="right">
              <?php if ($this->session->tempdata('data_session')[1] == 'Bhaktivedanta Medical Seminyak') { ?>
                  <Br><h2 style="margin-bottom: 7px">Bhaktivedanta Medical Seminyak</h2>
                  <p style="margin-top: -10px">Jl. Camplung Tanduk, Seminyak, Kuta, Kabupaten Badung, Bali 80361
                  TELP (0361) 9343911</p> 
                  <br><h3>Nota Pembayaran</h3>
              <?php } ?>
              <?php if ($this->session->tempdata('data_session')[1] == 'Bhaktivedanta Medical Petitenget') { ?>
                  <Br><h2 style="margin-bottom: 7px">Bhaktivedanta Medical Bali</h2>
                  <p style="margin-top: -10px">Jl. Raya Petitenget No.1X-seminyak, kerobokan, badung, Kerobokan Kelod, Kec. Kuta Utara, Kabupaten Badung, Bali 80361
                  TELP (0361) 9343811</p> 
                  <br><h3>Nota Pembayaran</h3>
              <?php } ?>
              <?php if ($this->session->tempdata('data_session')[1] == 'Bhaktivedanta Medical Canggu') { ?>
                  <Br><h2 style="margin-bottom: 7px">Bhaktivedanta Medical Canggu</h2>
                  <p style="margin-top: -10px">Jl. Pantai Batu Bolong No.11 B, Canggu, Kec. Kuta Utara, Kabupaten Badung, Bali 80351
                  TELP 0811-3926-699</p> 
                  <br><h3>Nota Pembayaran</h3>
              <?php } ?>
        </td>
      </tr>
    </table>

    <table  width="100%" >
      <tr>
        <td width="30%">No.Nota</td>
        <td width="1%">:</td>
        <td width="79%"><?= $total_bayar->no_pembayaran ?></td>
      </tr>
      <tr>
        <td valign="top">Unit/Instansi</td>
        <td valign="top">:</td>
        <td valign="top"><?= $pasienrj->instalasi_nama ?></td>
      </tr>
      <tr>
        <td valign="top">Tanggal Perawatan</td>
        <td valign="top">:</td>  
        <td valign="top"><?php echo date('d M Y'); ?></td>
      </tr>
      <tr>
        <td valign="top">No.R.M</td>
        <td valign="top">:</td>
        <td valign="top"> <?= $pasienrj->no_rekam_medis ?></td>
      </tr>
      <tr>
        <td valign="top">Nama Pasien</td>
        <td valign="top">:</td>
        <td valign="top"> <?= $pasienrj->pasien_nama ?> </td>
      </tr>
      <tr>
        <td valign="top">Alamat Pasien</td>
        <td valign="top">:</td>
        <td valign="top"> <?= $pasienrj->pasien_alamat ?> </td>
      </tr>
      <tr>
        <td valign="top">Dokter</td>
        <td valign="top">:</td>
        <td valign="top"> <?= $pasienrj->NAME_DOKTER ?>  </td>
      </tr>
      <tr>  
        <td valign="top">Administrasi Rekam Medis</td>
        <td valign="top">:</td>
        <td align="right">-</span></td>
      </tr>
      <tr> 
        <td valign="top">Tindakan</td>
        <td valign="top">:</td>
        <td valign="top" align="right">
            <table width="100%" >
              <?php 
                foreach($tindakan_list as $list){
              ?>
              <tr>
                <td width="70%"><?= $list->daftartindakan_nama ?></td>
                <td width="10%" align="right"><?= $list->jml_tindakan ?></td>
                <td width="20%" align="right"><?= $list->total_harga ?></td>
              </tr>    
              <?php } ?>
            </table>
        </td>
      </tr>
      <tr>
        <td valign="top">Obat & BHP</td>
        <td valign="top">:</td>
        <td valign="top">
            <table width="100%" >
            <?php
              foreach($obat as $key => $value) {
            ?>
              <tr>
                <td width="70%"><?php echo $value->nama_barang; ?></td>
                <td width="10%" align="right"><?php echo $value->qty; ?></td>
                <td width="20%" align="right"><?= "Rp. ".number_format(($value->harga_jual * $value->qty));  ?></td>
              </tr>
            <?php
              }
            ?>
            </table>
        </td> 
      </tr>
      <tr>
        <td valign="top">TOTAL TAGIHAN</td>
        <td valign="top">:</td>
        <td valign="top" align="right"><B><?= "Rp. ".number_format($total_bayar->total_keseluruhan)  ?></B></td>
      </tr> 
      <tr>
        <td valign="top">TOTAL BAYAR</td>
        <td valign="top"></td>
        <td valign="top">
          <table width="100%">
            <tr>  
              <td><?php echo ucwords(Terbilang($total_bayar->total_bayar)) ?> </td>    
              <td align="right"><span style="font-size: 18px"><b><?= "Rp. ".number_format($total_bayar->total_bayar) ?></b></span></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <br><br>
    <div class="margin-center2">
        <table width="100%" >
            <tr>
              <td width="30%" align="center">
              <span>Mengetahui,</span><br>a/n Direktur<br>Kabid Umum & Keuangan<br><br><br><br>
              (......................)
              </td>
              <td width="30%"></td>
              <td width="40%" align="center">
              <span>Malang , <?php
                echo date("d-M-Y H:i");
              ?> <br>Kasir</span><br><br><br><br><br>
              (.......................)
              </td>
            </tr>
        </table>
    </div>
  </div> 
</body>
</html>   

  
<?php 

function Terbilang($x){
      $abil = array("", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS");
      if ($x < 12)
        return " " . $abil[$x];
      elseif ($x < 20)
        return Terbilang($x - 10) . "BELAS";
      elseif ($x < 100)
        return Terbilang($x / 10) . " PULUH" . Terbilang($x % 10);
      elseif ($x < 200)
        return " SERATUS" . Terbilang($x - 100);
      elseif ($x < 1000)
        return Terbilang($x / 100) . " RATUS" . Terbilang($x % 100);
      elseif ($x < 2000)
        return " SERIBU" . Terbilang($x - 1000);
      elseif ($x < 1000000)
        return Terbilang($x / 1000) . " RIBU" . Terbilang($x % 1000);
      elseif ($x < 1000000000)
        return Terbilang($x / 1000000) . " JUTA" . Terbilang($x % 1000000);
    }

?>