<!DOCTYPE html >
<head>
<title>Detail Pemeriksaan Pasien</title>
<style type="text/css" rel="stylesheet">
			body{
			padding:1;
			}
			table {
	border-collapse: collapse;
	white-space: normal;				/*text-align: center center;*/
			}
	</style>
</head>
<!-- <body onLoad="window.print()"> -->
<body>
<table width="100%" border="0" cellpadding="1"> 
  <tr>
    <td width="18%" rowspan="5" align="center">  
      <img src="<?= base_url() ?>assets/plugins/images/logo-puribunda.png" width="200px" >  
    </td>
    <td width="65%" align="right">Nomor :</td>
    <td width="17%"><?= $total_bayar->no_pembayaran ?></td>
  </tr>
  <tr>
    <td align="right">Tanggal :</td> 
    <td><?= $total_bayar->tgl_pembayaran ?></td>
  </tr>
  <tr> 
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td> 
  </tr>
  <tr>
    <td align="center"><strong>SLIP PEMBAYARAN IGD </strong></td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><strong>TINDAKAN &amp; PEMERIKSAAN DOKTER</strong></td>
    <td align="center">&nbsp;</td> 
  </tr>
</table>
<table width="100%" border="0" cellpadding="1" cellspacing="5">
  <tr> 
    <td colspan="7">&nbsp;</td>
  </tr>
  <tr>
    <td width="13%">No. Registrasi</td>
    <td width="1%">:</td>
    <td colspan="2"><?php echo $pasienrj->no_pendaftaran; ?></td>
    <td width="14%">Nama Pasien </td>
    <td width="1%">:</td> 
    <td width="31%"><?php echo $pasienrj->pasien_nama; ?></td>
  </tr>
  <tr> 
    <td>No. Rekam Medis</td>
    <td>:</td>
    <td colspan="2"><?php echo $pasienrj->no_rekam_medis; ?></td>
    <td>Nama Dokter</td>
    <td>:</td>
    <td><?php echo $pasienrj->NAME_DOKTER; ?></td>
  </tr>
  <tr> 
    <td colspan="7">&nbsp;</td>
  </tr>
</table>
<hr width="100%" color="#000000">
<table width="100%" border="0" cellpadding="1" cellspacing="5">
  <tr>
    <td>Jasa Konsultasi Dokter Umum</td>
  </tr>
  <tr>
    <td width="83%">&nbsp;</td>
  </tr>
</table>
 <?php
        $total_obat = isset($tot_obat->total) ? $tot_obat->total : '0';
        $total_rad = isset($tot_rad->total_harga) ? $tot_rad->total_harga : '0';
        $total_lab = isset($tot_lab->total_harga) ? $tot_lab->total_harga : '0';
        $total_tind = isset($tot_tind->total_harga) ? $tot_tind->total_harga : '0';
        $total_all = $total_obat+$total_rad+$total_lab+$total_tind;
        ?>
<table width="100%" border="1" cellpadding="1">
  <tr>
    <th width="10%" align="center" valign="middle"><strong>NO</strong></th>
    <th width="31%" align="center" valign="middle"><strong>Nama Tindakan</strong></th>
    <th width="17%" align="center" valign="middle"><strong>Tarif Tindakan</strong></th>
    <th width="16%" align="center" valign="middle"><strong>Jumlah Tindakan</strong></th>
    <th width="18%" align="center" valign="middle"><strong>Jumlah Tarif Tindakan</strong></th>
  </tr>  
  <tr>
      <tr>
          <td>1</td>
          <td>Administrasi :</td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
     <?php
        $no = 0;
      foreach($tindakan_list as $list){
          if($list->komponentarif_id == 4){
          $no++;
      ?>
       <tr>
            <td></td>
            <td><?php echo $no.". &nbsp;".$list->daftartindakan_nama; ?></td>
            <td><?php echo number_format($list->harga_tindakan); ?></td>
            <td><?php echo $list->jml_tindakan; ?></td>
            <td><?php echo number_format($list->total_harga); ?></td>
        </tr>
      <?php
          }
      }
      ?>

       <tr>
          <td>2</td>
          <td>Pemeriksaan Dokter : </td>
          <td></td>
          <td></td>
          <td></td>
      </tr>

       <?php
        $no = 0;
        foreach($tindakan_list as $list){
            if($list->komponentarif_id == 8){
          $no++;
              
        ?>
        <tr>
            <td></td> 
            <td><?php echo $no.". &nbsp;".$list->daftartindakan_nama; ?></td>
            <td><?php echo number_format($list->harga_tindakan); ?></td>
            <td><?php echo $list->jml_tindakan; ?></td>
            <td><?php echo number_format($list->total_harga); ?></td>
        </tr>
        <?php
            }
        }
        ?>
      <tr>
          <td>3</td>
          <td>Tindakan Khusus :</td>
          <td></td>
          <td></td>
          <td></td>
      </tr>


        <?php
          $no = 0;
          foreach($tindakan_list as $list){
              // if($list->daftartindakan_id != '3' && $list->daftartindakan_id != '2'){
              if($list->komponentarif_id == 1){
                  $no++;
          ?>
                  <tr>
                      <td></td>
                      <td><?php echo $no.". &nbsp;".$list->daftartindakan_nama; ?></td>
                      <td><?php echo number_format($list->harga_tindakan); ?></td>
                      <td><?php echo $list->jml_tindakan; ?></td>
                      <td><?php echo number_format($list->total_harga); ?></td>
                  </tr>
          <?php
              }
          } 
          ?>

  
  <tr>
    <td colspan="4">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<p>&nbsp;</p>
<table width="100%" border="0" cellpadding="1">
<tr>
  <td colspan="4" rowspan="5">&nbsp;</td>
  <td align="center">&nbsp;</td>
</tr>
<tr>
    <td width="21%" align="center">Paraf Dokter</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>(.....................................................)</td>
  </tr>
</table>
</body>
</html>
