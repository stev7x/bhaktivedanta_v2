<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasirrj_model extends CI_Model {
    var $column = array('pendaftaran_id','nama_poliruangan','no_pendaftaran','tgl_pendaftaran','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa');
    var $order = array('tgl_pendaftaran' => 'ASC');

    public function __construct(){
        parent::__construct();
    }

    public function kasir() {
        $this->db->select('*');
        $this->db->from('v_belumbayar_pasien_rj');
        $this->db->or_where_not_in("status_periksa", array("SUDAH PULANG", "SELESAI"));
        $this->db->order_by('tgl_pendaftaran','DESC');

        $query = $this->db->get();

        return $query->result_array();
    }

    public function pembayaran() {
        $this->db->select('*');
        $this->db->from('t_pembayarankasir');

        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_kasirrj_list($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan){  
        $this->db->select('*');     
        $this->db->from('v_belumbayar_pasien_rj');    
        $i = 0;       

        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $this->db->or_where_not_in("status_periksa", array("SUDAH PULANG", "SELESAI"));
        // $this->db->order_by('tgl_pendaftaran', 'ASC');
        // $this->db->where('status_periksa != "DIPULANGKAN"');   
        // $this->db->where('no_rekam_medis != ""');    
         $this->db->order_by('tgl_pendaftaran','DESC');

        if(!empty($poliruangan)){
            $this->db->where('poliruangan_id',$poliruangan);
        }
        if(!empty($dokter_poli)){
            $this->db->where('id_m_dokter',$dokter_poli);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_nama)){ 
            $this->db->like('pasien_nama', $pasien_nama);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($name_dokter)){
            $this->db->like('NAME_DOKTER', $name_dokter);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }
        if(!empty($urutan)){
            $this->db->order_by($urutan, "ASC");
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');

        $query = $this->db->get();

        return $query->result();
    } 

    public function get_kasirrj_sudahbayar_list($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan){ 
        $this->db->select('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');    
        $this->db->from('v_sudahbayar_pasien_rj');

        $this->db->group_by('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');   

        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');  
 
        // $this->db->where('status_periksa != "DIPULANGKAN"'); 
        $this->db->order_by('tgl_pendaftaran','DESC');

        if(!empty($poliruangan)){
            $this->db->where('poliruangan_id',$poliruangan);
        }
        if(!empty($dokter_poli)){
            $this->db->where('id_m_dokter',$dokter_poli);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_nama)){ 
            $this->db->like('pasien_nama', $pasien_nama);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($name_dokter)){
            $this->db->like('NAME_DOKTER', $name_dokter);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }
        if(!empty($urutan)){
            $this->db->order_by($urutan, "ASC");
        }

        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');

        $query = $this->db->get();

        return $query->result();
    }

    public function get_alldata_kasir($tgl){ 
        $sql = 'SELECT * FROM v_belumbayar_pasien_rj WHERE tgl_pendaftaran LIKE "%'.$tgl.'%"';         
        $query = $this->db->query($sql);  
  
        return $query->result();         
    }

    // public function count_kasirrj_all(){
    //     $this->db->select('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');

    //     $this->db->from('v_belumbayar_pasien_rj');

    //     $this->db->group_by('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
    //     $query = $this->db->get();
    //     return $query->num_rows();
    // }

    public function count_kasirrj_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter){
        $this->db->select('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');

        $this->db->from('v_belumbayar_pasien_rj');

        // $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');  
        
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){ 
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_nama)){ 
            $this->db->like('pasien_nama', $pasien_nama);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($name_dokter)){
            $this->db->like('NAME_DOKTER', $name_dokter);
        }

        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');
        
        return $this->db->count_all_results();
    }

    public function count_kasirrj_sudahbayar_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter){
        $this->db->select('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');

        $this->db->from('v_sudahbayar_pasien_rj');       
        
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"'); 
 
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){ 
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_nama)){ 
            $this->db->like('pasien_nama', $pasien_nama);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($name_dokter)){
            $this->db->like('NAME_DOKTER', $name_dokter);
        }

        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');
        
        return $this->db->count_all_results();
    }


    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_rm_pasien_rj', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();
    }

    public function get_data_pasien($pasien_id){
        $query = $this->db->get_where('m_pasien', array('pasien_id' => $pasien_id), 1, 0);
        return $query->row();
    }

    public function get_dataPendaftaran_by_id($pendaftaran_id){
        $sql = 'SELECT * FROM t_pendaftaran WHERE pendaftaran_id = "'.$pendaftaran_id.'" ';
        $query = $this->db->query($sql);
        return $query->row();                  
    }   

    public function sum_harga_tindakan($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_tindakan_sudahbayar($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.pembayarankasir_id !=', '');
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_discount_tindakan($pendaftaran_id){
        $this->db->select_sum('diskon');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_discount_tindakan_sudahbayar($pendaftaran_id){
        $this->db->select_sum('diskon');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.pembayarankasir_id !=', '');
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_rad($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanradbelumbayar');
        $this->db->where('v_tindakanradbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();
        return $query->row();
    }

//    public function sum_discount_rad($pendaftaran_id){
//        $this->db->select_sum('discount');
//        $this->db->from('v_tindakanradbelumbayar');
//        $this->db->where('v_tindakanradbelumbayar.pendaftaran_id',$pendaftaran_id);
//        $query = $this->db->get();
//        return $query->row();
//    }

    public function sum_harga_lab($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanlabbelumbayar');
        $this->db->where('v_tindakanlabbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }

//    public function sum_discount_lab($pendaftaran_id){
//        $this->db->select_sum('discount');
//        $this->db->from('v_tindakanlabbelumbayar');
//        $this->db->where('v_tindakanlabbelumbayar.pendaftaran_id',$pendaftaran_id);
//        $query = $this->db->get();
//
//        return $query->row();
//    }

    public function sum_harga_obat($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function obat ($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_tindakan_umum_all($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');

        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_umum($pendaftaran_id){ 
        $this->db->select('*');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.pembayarankasir_id', NULL);
        $this->db->or_where('v_tindakanbelumbayar.pembayarankasir_id', "");
        $length = $this->input->get('length');

        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

//        echo $this->db->last_query();
//        exit;

        return $query->result();
    }
 
    public function get_tindakan_umum_sudahbayar($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.pembayarankasir_id !=', '');
        $length = $this->input->get('length');

        if($length !== false){  
            if($length != -1) {  
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }  
        }
 
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_rad($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanradbelumbayar');
        $this->db->where('v_tindakanradbelumbayar.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_lab($pendaftaran_id){
        $this->db->select('a.*, b.daftartindakan_nama');
        $this->db->from('t_tindakanpasien a');
        $this->db->join("m_tindakan b", "b.daftartindakan_id=a.daftartindakan_id");
        $this->db->where("a.pendaftaran_id", $pendaftaran_id);

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
 
        return $query->result();
    }
    
    // Check Pembayaran Kasir ID
    public function checkPembayaranKasirTindakanObat($pendaftaran_id) {
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        $this->db->limit(1);
        $pembayarankasir_id = $this->db->get("t_resepturpasien")->result();
        return $pembayarankasir_id;
    }
    
    
    public function get_obat_pasien($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_obatbelumbayar'); 
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);  
        // $this->db->where('v_obatbelumbayar.nama_asuransi',"");    
        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) { 
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            } 
        }

        $query = $this->db->get();
        return $query->result();   
    }   
 
    public function get_obat_pasien_sudahbayar($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_obatsudahbayar'); 
        $this->db->where('v_obatsudahbayar.pendaftaran_id',$pendaftaran_id);  
        // $this->db->where('v_obatbelumbayar.nama_asuransi',"");    
        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) {        
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            } 
        }

        $query = $this->db->get();
        return $query->result();   
    }


    public function get_obat_cover($pembayaran_obat_id){      
        $this->db->select('*');    
        $this->db->from('t_detailbayar_obat');   
        $this->db->join('t_detail_pembayaran_obat','t_detail_pembayaran_obat.pembayaran_obat_id =   t_detailbayar_obat.pembayaran_obat_id','left'); 
        $this->db->where('t_detailbayar_obat.pembayaran_obat_id', $pembayaran_obat_id);                
        $this->db->where('t_detailbayar_obat.cara_bayar != "PRIBADI"');       
        // $this->db->where('v_obatbelumbayar.type_pembayaran != "PRIBADI" ');     
        $length = $this->input->get('length');     
        if($length !== false){ 
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            } 
        }

        $query = $this->db->get();
        return $query->result();   
    }

    public function get_obat_cover2($pembayaran_obat_id,$reseptur_id){      
        $this->db->select('*');    
        $this->db->from('t_detailbayar_obat');   
        $this->db->join('t_detail_pembayaran_obat','t_detail_pembayaran_obat.pembayaran_obat_id =   t_detailbayar_obat.pembayaran_obat_id','left'); 
        $this->db->where('t_detailbayar_obat.pembayaran_obat_id', $pembayaran_obat_id);                    
        $this->db->where('t_detailbayar_obat.cara_bayar != "PRIBADI"');      
        $this->db->where('t_detailbayar_obat.reseptur_id', $reseptur_id);                
        
        $query = $this->db->get();
        return $query->num_rows();                 
    }


    public function get_obat_pasien_pribadi($pendaftaran_id){  
        $this->db->select('*');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id); 
        $this->db->where('v_obatbelumbayar.nama_asuransi',"");    
        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            } 
        }

        $query = $this->db->get();
        return $query->result(); 
    }

    public function insert_pembayarankasir($data){
        $insert = $this->db->insert('t_pembayarankasir',$data);
        // $error = $this->db->error();
        // var_dump($error); die();
        if ($insert) {
            $this->db->select("pembayarankasir_id");
            $this->db->from("t_pembayarankasir");
            $this->db->order_by("pembayarankasir_id", "desc");
            $this->db->limit(1);
            return $this->db->get()->row()->pembayarankasir_id;
        }
        return 0;
    }

    function getJenisPembayaranTindakanObat($pembayarankasir_id) {
        $this->db->where("pembayarankasir_id", $pembayarankasir_id);
        return $this->db->get("t_pembayarankasir")->result();
    }

    // Check Pembayaran Kasir ID
    public function checkPembayaranKasirTindakan($pendaftaran_id) {
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        $this->db->limit(1);
        $pembayarankasir_id = $this->db->get("t_tindakanpasien")->result();
        return $pembayarankasir_id;
    }

    public function checkPembayaranKasirID($pendaftaran_id) {
        $this->db->select("pembayarankasir_id");
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        $this->db->limit(1);
        return $this->db->get("t_pembayarankasir")->row();
    }

    // Update Pembayaran kasir ID on Obat
    public function update_obat($data=array(), $id) {
        $this->db->where("reseptur_id", $id);
        $this->db->update("t_resepturpasien", $data);
    }

    // Update Pembayaran kasir ID on t_tindakanpasien
    public function update_tindakanpasien($data=array(), $id) {
        $this->db->where("tindakanpasien_id", $id);
        $this->db->update("t_tindakanpasien", $data);
    }

    // Insert Detail Pembayaran Kasir
    public function insert_detail_pembayarankasir($data=array()){
        $insert = $this->db->insert('t_detail_pembayaran',$data);
        if ($insert) {
            $this->db->select("detail_pembayaran_id");
            $this->db->from("t_detail_pembayaran");
            $this->db->order_by("detail_pembayaran_id", "desc");
            $this->db->limit(1);
            return $this->db->get()->row()->detail_pembayaran_id;
        }
        return 0;
    }

    // Insert Detail Cara Bayar kasir
    public function insert_detail_carabayar($data=array()) {
        $this->db->insert("t_detail_pembayaran_cara_bayar", $data);
    }

    public function update_pembayarankasir($data, $id){  
        $this->db->where("pembayarankasir_id", $id);
        $update = $this->db->update('t_pembayarankasir',$data);   
        if ($update) return $id;
        else return 0;
    }

    public function insert_bayartindakan($data){
        $this->db->insert('t_detail_pembayaran',$data);
        $insert = $this->db->insert_id(); 
        return $insert; 
    }   
 
    public function insert_bayarobat($data){
       $this->db->insert('t_detail_pembayaran_obat',$data);
        $insert = $this->db->insert_id();  
        return $insert;   
    }   
    


    public function edit_bayartindakan($data,$id){    
        $query = $this->db->update('t_detail_pembayaran',$data,array('detail_pembayaran_id' =>$id )); 
        return $query;         
    }  
     
    public function get_bayartindakan2($pasien_id,$detail_id,$jenis_asuransi){       
        $this->db->from('t_detailbayar_tindakan');
        $this->db->join('t_detail_pembayaran','t_detail_pembayaran.detail_pembayaran_id = t_detailbayar_tindakan.detail_pembayaran_id','left');    
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_detailbayar_tindakan.daftartindakan_id','left');
        $this->db->where('t_detail_pembayaran.pasien_id',$pasien_id);  
        $this->db->where('t_detailbayar_tindakan.detail_pembayaran_id',$detail_id);
        $this->db->where('t_detailbayar_tindakan.nama_asuransi',$jenis_asuransi);
        $query = $this->db->get();                
        return $query->result();                 
        // $this->db->where('t_detail_pembayaran.tanggal_pembayaran',$tanggal);  
    }   
    public function get_bayartindakan($pasien_id,$detail_id){     
        $this->db->from('t_detailbayar_tindakan');  
        $this->db->join('t_detail_pembayaran','t_detail_pembayaran.detail_pembayaran_id = t_detailbayar_tindakan.detail_pembayaran_id','left');   
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_detailbayar_tindakan.daftartindakan_id','left');
        $this->db->where('t_detail_pembayaran.pasien_id',$pasien_id);  
        // $this->db->where('t_detail_pembayaran.tanggal_pembayaran',$tanggal);   
        $this->db->where('t_detailbayar_tindakan.detail_pembayaran_id',$detail_id);
        $query = $this->db->get();       
        return $query->result();              
    } 
 
    public function get_bayartindakan_check($pasien_id,$tanggal,$detail_id,$tindakanpasien_id){      
        $this->db->from('t_detailbayar_tindakan');  
        $this->db->join('t_detail_pembayaran','t_detail_pembayaran.detail_pembayaran_id = t_detailbayar_tindakan.detail_pembayaran_id','left');   
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_detailbayar_tindakan.daftartindakan_id','left');
        $this->db->where('t_detail_pembayaran.pasien_id',$pasien_id);  
        $this->db->where('t_detail_pembayaran.tanggal_pembayaran',$tanggal);   
        $this->db->where('t_detailbayar_tindakan.detail_pembayaran_id',$detail_id);        
        $this->db->where('t_detailbayar_tindakan.tindakanpasien_id',$tindakanpasien_id);
        $query = $this->db->get();        
        return $query->num_rows();                  
    } 

 

    public function get_bayarobat($pendaftaran_id,$detailobat_id,$jenis_asuransi){
        $sql = '
    SELECT * FROM t_detailbayar_obat AS t_do INNER JOIN t_detail_pembayaran_obat AS t_dp ON t_do.pembayaran_obat_id = t_dp.pembayaran_obat_id
    WHERE t_dp.pendaftaran_id = "'.$pendaftaran_id.'" AND t_dp.pembayaran_obat_id = "'.$detailobat_id.'" AND t_do.cara_bayar LIKE "%'.$jenis_asuransi.'%" ';
        $query = $this->db->query($sql);
             
        return $query->result();

    }

    // public function insert_tindakan($data){
    //     $this->db->insert('t_detailbayar_tindakan',$data);
    //     $insert = $this->db->insert_id(); 
    //     return $insert; 
    // }
    public function get_detailbayar_byid($id){    
        $this->db->from('t_detailbayar_tindakan');
        $this->db->where('detailbayar_tindakan_id',$id);    
        $query = $this->db->get(); 
  
        return $query->row();   
    }
    public function get_detailbayarObat_byid($id){     
        $this->db->from('t_detailbayar_obat'); 
        $this->db->where('detailbayar_obat_id',$id);    
        $query = $this->db->get(); 
  
        return $query->row();   
    }   

    public function getCaraPembayaran($id){
        $query = $this->db->get_where('t_pembayaran', array('pembayaran_id' => $id), 1, 0);
        return $query->row(); 
    }

    // menampilkan asuransi
    function get_asuransi($type){
        $this->db->from('m_asuransi');
        if (!empty($type)) {
            $this->db->where('tipe_asuransi',$type);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function delete_tindakan_cover($id){        
        $delete = $this->db->delete('t_detailbayar_tindakan', array('detailbayar_tindakan_id' => $id));      

        return $delete;
    }
    public function delete_obat_cover($id){         
        $delete = $this->db->delete('t_detailbayar_obat', array('detailbayar_obat_id' => $id));      

        return $delete;
    }

    function get_no_pembayaran(){
        $default = "00001";
        $date = date('Ymd'); 
        $prefix = "INV".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pembayaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pembayarankasir
                        WHERE no_pembayaran LIKE ('INV".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pembayaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pembayaran;
    }

    public function count_tindakan_umum($pendaftaran_id){
        $this->db->from('t_tindakanpasien');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tindakan_rad($pendaftaran_id){
        $this->db->from('t_tindakanradiologi');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tindakan_lab($pendaftaran_id){
        $this->db->from('t_tindakanlab');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_obat($pendaftaran_id){
        $this->db->from('t_resepturpasien');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_penunjang($pendaftaran_id){
        $this->db->from('t_pasienmasukpenunjang');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function update_tindakan($table, $data, $idfield, $idval){
        $this->db->where($idfield, $idval);
        $update = $this->db->update($table, $data);
        return $update;
    }

    public function update_pendaftaran($data, $id){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));
 
        return $update;  
    }
    public function set_status_pulang($hari,$bulan,$tahun,$petugas_id){
        $sql = 'UPDATE t_pendaftaran
                SET status_periksa = "DIPULANGKAN" , status_pasien = 0 , petugas_id = "'.$petugas_id.'"         
                WHERE instalasi_id = "1" AND DAY(tgl_pendaftaran) = "'.$hari.'" AND MONTH(tgl_pendaftaran) = "'.$bulan.'" AND YEAR(tgl_pendaftaran) ="'.$tahun.'"'; 
        $query = $this->db->query($sql); 
        return $query;     
    }

    function update_live($where,$data){
        $this->db->where($where); 
        $this->db->update("t_sub_detail",$data);
    }


 
    function live_edit($table,$where,$id,$data){
        // $this->db->where($where);  
        // $this->db->update("t_tindakanpasien",$data);
              
        $update = $this->db->update($table, $data, array($where => $id));
        return $update;        
    }

    public function get_tindakan_by_id($id){  
        $query = $this->db->get_where('v_tindakanbelumbayar',array('tindakanpasien_id' => $id));
        return $query->row();  
    }      
    public function get_tindakansudahbayar_by_id($id){         
        $query = $this->db->get_where('v_tindakanbelumbayar',array('tindakanpasien_id' => $id));
        return $query->row();  
    }      

    public function get_tindakan_by_id2($id){  
        // $query = $this->db->get_where('t_tindakanpasien',array('tindakanpasien_id' => $id));
        $this->db->select('*');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id','left');     
        $this->db->where('t_tindakanpasien.tindakanpasien_id',$id);
    
        $query = $this->db->get();
        return $query->row();               
    }

    public function get_obat_by_id($id){  
        $query = $this->db->get_where('v_obatbelumbayar',array('reseptur_id' => $id));  
        return $query->row();  
    }

    public function get_obatsudahbayar_by_id($id){             
        $query = $this->db->get_where('v_obatsudahbayar',array('reseptur_id' => $id));  
        return $query->row();  
    }


    public function get_pembayarankasir($id){
        $this->db->where("pembayarankasir_id", $id);
        $query = $this->db->get('t_pembayarankasir');
        return $query->row();
    }

    public function sum_harga_rad_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanradiologi');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_lab_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanlab');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();     
    }

    public function sum_harga_obat_sudah_bayar($pembayarankasir_id){   
        $this->db->select('SUM(qty*harga_jual) AS total_harga');
        $this->db->from('t_resepturpasien');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_tindakan_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanpasien');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_print_bayar($pembayarankasir_id){
        $this->db->select('*');
        $this->db->from('t_pembayarankasir');
        $this->db->join('aauth_users','aauth_users.id = t_pembayarankasir.create_by','left');      
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);

        $query = $this->db->get();

        return $query->row();
    } 

    public function get_tindakan_pasien($pembayarankasir_id){           
        $this->db->select('t_tindakanpasien.jml_tindakan, t_tindakanpasien.total_harga,t_tindakanpasien.diskon,t_tindakanpasien.harga_tindakan as harga_baru,m_tindakan.*');
        $this->db->from('t_tindakanpasien');          
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_tindakanpasien.dokter_id','left'); 
        $this->db->where('t_tindakanpasien.pembayarankasir_id',$pembayarankasir_id);    
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_pasien_all($pendaftaran_id){
        $this->db->select('t_tindakanpasien.pendaftaran_id, t_tindakanpasien.jml_tindakan, t_tindakanpasien.total_harga,t_tindakanpasien.diskon,t_tindakanpasien.harga_tindakan as harga_baru,m_tindakan.*');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_tindakanpasien.dokter_id','left');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_obat($pembayarankasir_id){
        $this->db->select('*');
        $this->db->from('t_resepturpasien');
        $this->db->where('t_resepturpasien.pembayarankasir_id', $pembayarankasir_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_fee_dokter($pasien_id, $dokter_id) {
        $this->db->select("SUM(harga) harga");
        $this->db->where("dokter_id", $dokter_id);
        $this->db->where("pasien_id", $pasien_id);
        return $this->db->get("t_fee_dokter_perawat")->row()->harga;
    }

    // START : model untuk mengambil data jenis pasien sesuai id
    function get_jenis_pasien_by_id($id) {
        $this->db->select('*');
        $this->db->from('m_jenis_pasien');
        $this->db->where("id_jenis_pasien", $id);
        $query = $this->db->get();

        return $query->row();
    }
    // END

    // START : model untuk mengambil data jenis pasien sesuai id
    function get_layanan_by_id($id) {
        $this->db->select('*');
        $this->db->from('m_layanan');
        $this->db->where("id_layanan", $id);
        $query = $this->db->get();

        return $query->row();
    }
    // END

    public function get_perawat_pasien($pasien_id, $pendaftaran_id) {
        $this->db->select('*');
        $this->db->from('t_fee_dokter_perawat');
        $this->db->where("pasien_id", $pasien_id);
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        $this->db->order_by('id_fee', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_tindakan_pasien_list($pendaftaran_id){
        $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama,m_tindakan.type_tindakan');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);
        $this->db->where('m_tindakan.type_tindakan',"TINDAKAN");
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_reservasi_by_no_pendaftaran($no_pendaftaran) {
        $this->db->select('*');
        $this->db->from('t_reservasi');
        $this->db->where("no_pendaftaran", $no_pendaftaran);
        $this->db->order_by('reservasi_id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }
}
