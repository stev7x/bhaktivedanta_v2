<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasirrd_model extends CI_Model {
    var $column = array('pendaftaran_id','nama_poliruangan','no_pendaftaran','tgl_pendaftaran','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa');
    var $order = array('tgl_pendaftaran' => 'ASC');

    public function __construct(){
        parent::__construct();
    }

    public function get_kasirrd_list(){
        $this->db->select('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $this->db->from('v_belumbayar_pasien_rd');
        $i = 0; 
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_kasirrd_all(){
        $this->db->select('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $this->db->from('v_belumbayar_pasien_rd');
        $this->db->group_by('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_kasirrd_filtered(){
        $this->db->select('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $this->db->from('v_belumbayar_pasien_rd');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by('pendaftaran_id, nama_poliruangan, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_alldata_kasir($tgl){ 
      
        $sql = 'SELECT * FROM v_belumbayar_pasien_rd WHERE tgl_pendaftaran LIKE "%'.$tgl.'%"';          
        $query = $this->db->query($sql);  
  
        return $query->result();           
    }

    public function set_status_pulang($hari,$bulan,$tahun,$petugas_id){
        $sql = 'UPDATE t_pendaftaran
                SET status_periksa = "DIPULANGKAN" , status_pasien = 0 , petugas_id = "'.$petugas_id.'"              
                WHERE instalasi_id = "2" AND DAY(tgl_pendaftaran) = "'.$hari.'" AND MONTH(tgl_pendaftaran) = "'.$bulan.'" AND YEAR(tgl_pendaftaran) ="'.$tahun.'"'; 
        $query = $this->db->query($sql); 
        return $query;     
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_rm_pasien_rd', array('pendaftaran_id' => $pendaftaran_id), 1, 0);

        return $query->row();
    }

    public function sum_harga_tindakan($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_rad($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanradbelumbayar');
        $this->db->where('v_tindakanradbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_lab($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanlabbelumbayar');
        $this->db->where('v_tindakanlabbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_obat($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_tindakan_umum($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_rad($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanradbelumbayar');
        $this->db->where('v_tindakanradbelumbayar.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_lab($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanlabbelumbayar');
        $this->db->where('v_tindakanlabbelumbayar.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_obat_pasien($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function insert_pembayarankasir($data){
        $this->db->insert('t_pembayarankasir',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }

    function get_no_pembayaran(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "INV".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pembayaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pembayarankasir
                        WHERE no_pembayaran LIKE ('INV".$date."%')";
        $no_current =  $this->db->query($sql)->result();

        $next_pembayaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);

        return $next_pembayaran;
    }

    public function count_tindakan_umum($pendaftaran_id){
        $this->db->from('t_tindakanpasien');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tindakan_rad($pendaftaran_id){
        $this->db->from('t_tindakanradiologi');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tindakan_lab($pendaftaran_id){
        $this->db->from('t_tindakanlab');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_obat($pendaftaran_id){
        $this->db->from('t_resepturpasien');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_penunjang($pendaftaran_id){
        $this->db->from('t_pasienmasukpenunjang');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function update_tindakan($table, $data, $idfield, $idval){
        $update = $this->db->update($table, $data, array($idfield => $idval));

        return $update;
    }

    public function get_pembayarankasir($id){
        $query = $this->db->get_where('t_pembayarankasir', array('pembayarankasir_id' => $id), 1, 0);

        return $query->row();
    }

    public function sum_harga_rad_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanradiologi');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_lab_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanlab');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_obat_sudah_bayar($pembayarankasir_id){
        $this->db->select('SUM(qty*harga_jual) AS total');
        $this->db->from('t_resepturpasien');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_tindakan_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanpasien');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_print_bayar($pembayarankasir_id){
        $this->db->select('*');
        $this->db->from('t_pembayarankasir');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);

        $query = $this->db->get();

        return $query->row();


    }

    public function get_tindakan_pasien($pembayarankasir_id){
        $this->db->select('t_tindakanpasien.jml_tindakan, t_tindakanpasien.komponentarif_id, t_tindakanpasien.total_harga, m_tindakan.daftartindakan_id, m_tindakan.harga_tindakan, m_tindakan.daftartindakan_nama');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->result();
    }
}
