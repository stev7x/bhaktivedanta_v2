<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasirri_model extends CI_Model {
    var $column = array('pendaftaran_id','nama_poliruangan','kelaspelayanan_nama','no_pendaftaran','tgl_pendaftaran','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa');
    var $order = array('tgl_pendaftaran' => 'ASC');
    
    public function __construct(){
        parent::__construct();
    }
    
    public function get_kasirri_list(){
        $this->db->select('pendaftaran_id, nama_poliruangan, kelaspelayanan_nama, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $this->db->from('v_belumbayar_pasien_ri');
        $i = 0; 
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by('pendaftaran_id, nama_poliruangan, kelaspelayanan_nama, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_kasirri_all(){
        $this->db->select('pendaftaran_id, nama_poliruangan, kelaspelayanan_nama, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $this->db->from('v_belumbayar_pasien_ri');
        $this->db->group_by('pendaftaran_id, nama_poliruangan, kelaspelayanan_nama, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_kasirri_filtered(){
        $this->db->select('pendaftaran_id, nama_poliruangan, kelaspelayanan_nama, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $this->db->from('v_belumbayar_pasien_ri'); 
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by('pendaftaran_id, nama_poliruangan, kelaspelayanan_nama, no_pendaftaran, tgl_pendaftaran, no_rekam_medis, pasien_nama, jenis_kelamin, umur, pasien_alamat, type_pembayaran, status_periksa');
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_rm_pasien_ri', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        

        return $query->row();    
    }
    
    public function sum_harga_jasadokter($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.kelompoktindakan_id',KELOMPOKTINDAKAN_JASADOKTER);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_jasaperawatan($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.kelompoktindakan_id',KELOMPOKTINDAKAN_PELAYANANRI);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_tindakan($pendaftaran_id){
        $notin = array(KELOMPOKTINDAKAN_JASADOKTER, KELOMPOKTINDAKAN_PELAYANANRI);
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where_not_in('v_tindakanbelumbayar.kelompoktindakan_id',$notin);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_rad($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanradbelumbayar');
        $this->db->where('v_tindakanradbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_lab($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_tindakanlabbelumbayar');
        $this->db->where('v_tindakanlabbelumbayar.pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_obat($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        // $this->db->where('v_obatbelumbayar.id_jenis_barang',JENISOA_OBAT); //current comment by darfat
        // $this->db->where('v_obatbelumbayar.jenisoa_id',JENISOA_OBAT);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_bhp($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        //$this->db->where('v_obatbelumbayar.id_jenis_barang',JENISOA_BHP); //current comment by darfat
        // $this->db->where('v_obatbelumbayar.jenisoa_id',JENISOA_BHP);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_alkes($pendaftaran_id){
        $this->db->select_sum('total_harga');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        // $this->db->where('v_obatbelumbayar.id_jenis_barang',JENISOA_ALKES); //current comment by darfat
        // $this->db->where('v_obatbelumbayar.jenisoa_id',JENISOA_ALKES);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function get_jasadokter($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.kelompoktindakan_id',KELOMPOKTINDAKAN_JASADOKTER);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_jasaperawatan($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.kelompoktindakan_id',KELOMPOKTINDAKAN_PELAYANANRI);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tindakanlain($pendaftaran_id){
        $notin = array(KELOMPOKTINDAKAN_JASADOKTER, KELOMPOKTINDAKAN_PELAYANANRI);
        $this->db->select('*');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where_not_in('v_tindakanbelumbayar.kelompoktindakan_id',$notin);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tindakan_rad($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanradbelumbayar');
        $this->db->where('v_tindakanradbelumbayar.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_tindakan_lab($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanlabbelumbayar');
        $this->db->where('v_tindakanlabbelumbayar.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_obat_pasien($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        // $this->db->where('v_obatbelumbayar.id_jenis_barang',JENISOA_OBAT);//current comment by darfat
        // $this->db->where('v_obatbelumbayar.jenisoa_id',JENISOA_OBAT);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_bhp_pasien($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        // $this->db->where('v_obatbelumbayar.id_jenis_barang',JENISOA_BHP); //current comment by darfat
        // $this->db->where('v_obatbelumbayar.jenisoa_id',JENISOA_BHP);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_alkes_pasien($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_obatbelumbayar');
        $this->db->where('v_obatbelumbayar.pendaftaran_id',$pendaftaran_id);
        // $this->db->where('v_obatbelumbayar.id_jenis_barang',JENISOA_ALKES); //current comment by darfat
        // $this->db->where('v_obatbelumbayar.jenisoa_id',JENISOA_ALKES);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    function get_no_pembayaran(){
        $default = "00001";
        $date = date('Ymd');
        $prefix = "INV".$date;
        $sql = "SELECT CAST(MAX(SUBSTR(no_pembayaran,".(strlen($prefix)+1).",".(strlen($default)).")) AS UNSIGNED) nomaksimal
                        FROM t_pembayarankasir 
                        WHERE no_pembayaran LIKE ('INV".$date."%')";
        $no_current =  $this->db->query($sql)->result();
        
        $next_pembayaran = $prefix.(isset($no_current[0]->nomaksimal) ? (str_pad($no_current[0]->nomaksimal+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        
        return $next_pembayaran;
    }
    
    public function insert_pembayarankasir($data){
        $this->db->insert('t_pembayarankasir',$data);
        $insert = $this->db->insert_id();
        return $insert;
    }
    
    public function update_common($table, $data, $idfield, $idval){
        $update = $this->db->update($table, $data, array($idfield => $idval));
        
        return $update;
    }
    
    public function count_tindakan_umum($pendaftaran_id){
        $this->db->from('t_tindakanpasien');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function count_tindakan_rad($pendaftaran_id){
        $this->db->from('t_tindakanradiologi');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function count_tindakan_lab($pendaftaran_id){
        $this->db->from('t_tindakanlab');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function count_obat($pendaftaran_id){
        $this->db->from('t_resepturpasien');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function count_penunjang($pendaftaran_id){
        $this->db->from('t_pasienmasukpenunjang');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function get_pembayarankasir($id){
        $query = $this->db->get_where('t_pembayarankasir', array('pembayarankasir_id' => $id), 1, 0);

        return $query->row();
    }
    
    public function sum_harga_rad_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanradiologi');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_lab_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanlab');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_obat_sudah_bayar($pembayarankasir_id){
        $this->db->select('SUM(t_resepturpasien.qty*t_resepturpasien.harga_jual) AS total');
        $this->db->from('t_resepturpasien');
        $this->db->join('m_obat','m_obat.obat_id = t_resepturpasien.obat_id');
        $this->db->where('t_resepturpasien.pembayarankasir_id',$pembayarankasir_id);
        $this->db->where('m_obat.jenis_obat',JENISOA_OBAT);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_bhp_sudah_bayar($pembayarankasir_id){
        $this->db->select('SUM(t_resepturpasien.qty*t_resepturpasien.harga_jual) AS total');
        $this->db->from('t_resepturpasien');
        $this->db->join('m_obat','m_obat.obat_id = t_resepturpasien.obat_id');
        $this->db->where('t_resepturpasien.pembayarankasir_id',$pembayarankasir_id);
        $this->db->where('m_obat.jenis_obat',JENISOA_BHP);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_harga_alkes_sudah_bayar($pembayarankasir_id){
        $this->db->select('SUM(t_resepturpasien.qty*t_resepturpasien.harga_jual) AS total');
        $this->db->from('t_resepturpasien');
        $this->db->join('m_obat','m_obat.obat_id = t_resepturpasien.obat_id');
        $this->db->where('t_resepturpasien.pembayarankasir_id',$pembayarankasir_id);
        $this->db->where('m_obat.jenis_obat',JENISOA_ALKES);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_tindakan_sudah_bayar($pembayarankasir_id){
        $notin = array(KELOMPOKTINDAKAN_JASADOKTER, KELOMPOKTINDAKAN_PELAYANANRI);
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan', 'm_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id', 'left');
        $this->db->join('m_tindakan_komponen_tarif', 'm_tindakan_komponen_tarif.daftar_tindakan_id = t_tindakanpasien.daftartindakan_id', 'left');
        $this->db->where('t_tindakanpasien.pembayarankasir_id',$pembayarankasir_id);
        $this->db->where_not_in('m_tindakan.kelompoktindakan_id',$notin);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_jasadokter_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan', 'm_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id', 'left');
        $this->db->join('m_tindakan_komponen_tarif', 'm_tindakan_komponen_tarif.daftar_tindakan_id = t_tindakanpasien.daftartindakan_id', 'left');
        $this->db->where('t_tindakanpasien.pembayarankasir_id',$pembayarankasir_id);
        $this->db->where('m_tindakan.kelompoktindakan_id',KELOMPOKTINDAKAN_JASADOKTER);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function sum_jasaperawatan_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan', 'm_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id', 'left');
        $this->db->join('m_tindakan_komponen_tarif', 'm_tindakan_komponen_tarif.daftar_tindakan_id = t_tindakanpasien.daftartindakan_id', 'left');
        $this->db->where('t_tindakanpasien.pembayarankasir_id',$pembayarankasir_id);
        $this->db->where('m_tindakan.kelompoktindakan_id',KELOMPOKTINDAKAN_PELAYANANRI);
        $query = $this->db->get();

        return $query->row();
    }
    
    public function get_tindakan_pasien($pembayarankasir_id){
        $this->db->select('t_tindakanpasien.jml_tindakan, t_tindakanpasien.total_harga, m_tindakan.harga_tindakan, m_tindakan.daftartindakan_nama, m_tindakan.kelompoktindakan_id');
        $this->db->from('t_tindakanpasien');
        $this->db->join('m_tindakan', 'm_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id', 'left');
        $this->db->join('m_tindakan_komponen_tarif', 'm_tindakan_komponen_tarif.daftar_tindakan_id = t_tindakanpasien.daftartindakan_id', 'left');
        $this->db->where('t_tindakanpasien.pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->result();
    }
    
    public function kataTerbilang($input_number){
        $input_number = abs($input_number);
        $number = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if ($input_number < 12) {
            $temp = " " . $number[$input_number];
        } else if ($input_number < 20) {
            $temp = $this->kataTerbilang($input_number - 10) . " belas";
        } else if ($input_number < 100) {
            $temp = $this->kataTerbilang($input_number / 10) . " puluh" . $this->kataTerbilang($input_number % 10);
        } else if ($input_number < 200) {
            $temp = " seratus" . $this->kataTerbilang($input_number - 100);
        } else if ($input_number < 1000) {
            $temp = $this->kataTerbilang($input_number / 100) . " ratus" . $this->kataTerbilang($input_number % 100);
        } else if ($input_number < 2000) {
            $temp = " seribu" . $this->kataTerbilang($input_number - 1000);
        } else if ($input_number < 1000000) {
            $temp = $this->kataTerbilang($input_number / 1000) . " ribu" . $this->kataTerbilang($input_number % 1000);
        } else if ($input_number < 1000000000) {
            $temp = $this->kataTerbilang($input_number / 1000000) . " juta" . $this->kataTerbilang($input_number % 1000000);
        } else if ($input_number < 1000000000000) {
            $temp = $this->kataTerbilang($input_number / 1000000000) . " milyar" . $this->kataTerbilang(fmod($input_number, 1000000000));
        } else if ($input_number < 1000000000000000) {
            $temp = $this->kataTerbilang($input_number / 1000000000000) . " trilyun" . $this->kataTerbilang(fmod($input_number, 1000000000000));
        }
        return $temp;
    }
    
    public function formatNumberTerbilang($input_number){
        if ($input_number < 0){
            $return = "minus " . trim($this->kataTerbilang($input_number));
        } else {
            $arrnum = explode('.', $input_number);
            $arrcount = count($arrnum);
            if ($arrcount == 1) {
                $return = trim($this->kataTerbilang($input_number));
            } else if ($arrcount > 1) {
                $return = trim($this->kataTerbilang($arrnum[0])) . " koma " . trim($this->kataTerbilang($arrnum[1]));
            }
        }
        $return = strtoupper($return." RUPIAH");
       
        return $return;
    }

    public function get_obat($pembayarankasir_id){
        $this->db->select('*');
        $this->db->from('t_resepturpasien');
        $this->db->where('t_resepturpasien.pembayarankasir_id', $pembayarankasir_id);
        $query = $this->db->get();
        return $query->result();
    }
}