<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_bpjs_model extends CI_Model {
    var $column = array('pendaftaran_id','nama_poliruangan','no_pendaftaran','tgl_pendaftaran','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa');
    var $order = array('tgl_pendaftaran' => 'ASC'); 

    public function __construct(){
        parent::__construct();
    }
}