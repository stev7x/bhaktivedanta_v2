<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vaksinrj extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('Kasirrj_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    // Index
    public function index(){
        $is_permit = $this->aauth->control_no_redirect('kasir_vaksin_rawat_jalan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "kasir_vaksin_rawat_jalan_view";
        $comments = "List Vaksin Kasir Rawat Jalan";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('vaksin_kasirrj', $this->data);
    }

    // Get List Pasien Belum Bayar
    public function ajax_list_kasirrj(){
        $tgl_awal       = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir      = $this->input->get('tgl_akhir',TRUE);
        $poliruangan    = $this->input->get('poliruangan',TRUE);
        $dokter_poli    = $this->input->get('dokter_poli',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs        = $this->input->get('no_bpjs',TRUE);
        $pasien_nama    = $this->input->get('pasien_nama',TRUE);
        $pasien_alamat  = $this->input->get('pasien_alamat',TRUE);
        $name_dokter    = $this->input->get('name_dokter',TRUE);
        $urutan         = $this->input->get('urutan',TRUE);
        $list = $this->Kasirrj_model->get_kasirrj_list($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan);

        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrj->nama_poliruangan;
            $row[] = $pasienrj->no_pendaftaran;
            $row[] = date('d-M-Y H:i:s', strtotime($pasienrj->tgl_pendaftaran));
            $row[] = $pasienrj->no_rekam_medis;
            $row[] = $pasienrj->pasien_nama;
            $row[] = $pasienrj->jenis_kelamin;
            $row[] = $pasienrj->umur;
            $row[] = $pasienrj->pasien_alamat;
            $row[] = $pasienrj->type_pembayaran;
            $row[] = $pasienrj->status_periksa;
            $row[] = '
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_bayar_kasirrj" data-backdrop="static" data-keyboard="false"  title="Klik untuk pemeriksaan pasien" onclick="bayarTagihan('. "'".$pasienrj->pendaftaran_id. "'" .','. "'Bayar'" .' )">Bayar</button>
            ';      
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kasirrj_model->count_kasirrj_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter),
                    "recordsFiltered" => $this->Kasirrj_model->count_kasirrj_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
}