<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasirrj extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kasirrj_model');
        $this->load->model('Models');

        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
            // print_r($value);
            $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_jalan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "kasir_rawat_jalan_view";
        $comments = "List Kasir Rawat Jalan";
        $this->aauth->logit($perms, current_url(), $comments);
        $this->load->view('v_kasirrj', $this->data);
    }

    public function migration() {
        $data_kasir = (array) $this->Kasirrj_model->kasir();
        $data_pembayaran = (array) $this->Kasirrj_model->pembayaran();
        $update = $h2h = array();

//        echo "<pre>";
//        print_r($data_kasir);
//        exit;

        $i = 1;
        foreach ($data_kasir as $key => $value) {
            $pembayarankasir_id = @$this->Kasirrj_model->get_tindakan_umum_sudahbayar($value['pendaftaran_id'])[0]->pembayarankasir_id ? $this->Kasirrj_model->get_tindakan_umum_sudahbayar($value['pendaftaran_id'])[0]->pembayarankasir_id : 0;
            if ($pembayarankasir_id == 0) {
                $con = array(
                    'pasien_id' => $value['pasien_id'],
                    'pendaftaran_id' => $value['pendaftaran_id']
                );

                if (@$data_tindakan_dokter = $this->Models->where("rawatjalan_penjualan_obat", $con)[0]) {
                    if ($data_tindakan_dokter['pembayaran_id'] != 0) {
                        $pembayarankasir_id = $data_tindakan_dokter['pembayaran_id'];
                    }
                }
            }

            foreach ($data_pembayaran as $key1 => $value1) {
                if ($value1['pembayarankasir_id'] == $pembayarankasir_id) {
                    $data['total_tindakan_umum'] = $data['total_tindakan_lab'] = $data['total_tindakan_escort'] = $data['total_tindakan_obat'] = $data['total_diskon_obat'] = $data['total_harga_satuan_obat'] = $data['total_hp_obat'] = 0;

                    $data['tindakan']   = $this->Kasirrj_model->get_tindakan_pasien_all($value['pendaftaran_id']);
                    $data['tindakan_obat']   = $this->Models->where("rawatjalan_penjualan_obat", array('pendaftaran_id' => $value['pendaftaran_id']));
                    $data['tindakan_dokter'] = @$this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $value['pendaftaran_id']))[0];

                    $diskon_tindakan = 0;
                    foreach($data['tindakan'] as $list){
                        $total_harga  = $list->jml_tindakan * $list->harga_tindakan;
                        $diskon_tindakan = $diskon_tindakan + $list->diskon;

                        if ($list->escort == 1) {
                            $data['total_tindakan_escort'] = $data['total_tindakan_escort'] + $total_harga;
                        } else if ($list->type_tindakan == "TINDAKAN") {
                            $data['total_tindakan_umum'] = $data['total_tindakan_umum'] + $total_harga;
                        } else if ($list->type_tindakan == "LABORATORIUM") {
                            $data['total_tindakan_lab'] = $data['total_tindakan_lab'] + $total_harga;
                        }
                    }

                    foreach ($data['tindakan_obat'] as $key => $value) {
//                        $obat = $this->Models->query("SELECT MAX(harga_persediaan_3) AS result FROM farmasi_obat WHERE id = " . $value['obat_id'])[0];
                        $obat = $this->Models->query("SELECT het_persediaan_3 AS result FROM farmasi_obat WHERE id = " . $value['obat_id'])[0];

                        $data['total_hp_obat'] = $data['total_hp_obat'] + $obat['result'];
                        $data['total_diskon_obat'] = $data['total_diskon_obat'] + $value['diskon'];
                        $data['total_harga_satuan_obat'] = $data['total_harga_satuan_obat'] + $value['harga_satuan'];
                        $data['total_tindakan_obat'] = $data['total_tindakan_obat'] + ($value['harga_satuan'] * $value['jumlah_obat']);
                    }

                    $total = $data['total_tindakan_umum'] + $data['total_tindakan_lab'] + $data['total_tindakan_escort'] + $data['total_tindakan_obat'] + $data['tindakan_dokter']['harga'];
                    $data['surcharge']   = $total * 0.03;
                    $data['total_keseluruhan'] = $total + $data['surcharge'];
                    $data['total_discount'] = $diskon_tindakan + $data['total_diskon_obat'] + $data['tindakan_dokter']['diskon'];
                    $data['total_bayar'] = $data['total_keseluruhan'] - $data['total_discount'];

                    $update = [
//                        'pendaftaran_id' => $value['pendaftaran_id'],
//                        'pembayarankasir_id' => $pembayarankasir_id,
//                        'totalbiayaobat' => $data['total_tindakan_obat'],
//                        'totalbiayatindakan' => $data['total_tindakan_umum'],
//                        'totalbiayalab' => $data['total_tindakan_lab'],
//                        'totalbiayadokter' => $data['tindakan_dokter']['total_harga'],
//                        'totalbiayaescort' => $data['total_tindakan_escort'],
//                        'total_keseluruhan' => $data['total_keseluruhan'],
//                        'total_discount' => $data['total_discount'],
//                        'total_bayar' => $data['total_bayar'],
//                        'surcharge' => $data['surcharge'],
                        'total_hp_obat' => $data['total_hp_obat']
                    ];

                    if($this->Kasirrj_model->update_pembayarankasir($update, $pembayarankasir_id)) {
                        echo $i . ". " . $pembayarankasir_id . " - OK" . "<br>";
                    } else {
                        echo $i . ". " . $pembayarankasir_id . " - NOK"  . "<br>";
                    }

//                    $h2h = [
//                        'pendaftaran_id' => $value['pendaftaran_id'],
//                        'pembayarankasir_id' => $pembayarankasir_id,
//                        'totalbiayaobat' => $value1['totalbiayaobat'],
//                        'totalbiayatindakan' => $value1['totalbiayatindakan'],
//                        'totalbiayalab' => $value1['totalbiayalab'],
//                        'totalbiayadokter' => $value1['totalbiayadokter'],
//                        'totalbiayaescort' => $value1['totalbiayaescort'],
//                        'total_keseluruhan' => $value1['total_keseluruhan'],
//                        'total_discount' => $value1['total_discount'],
//                        'total_bayar' => $value1['total_bayar'],
//                        'surcharge' => $value1['surcharge']
//                    ];

//                    echo "<pre>";
//                    print_r($update);
//                    print_r($h2h);
                    echo "-------\n";
                }
            }

            $i++;
        }
    }

    public function set_status_pulang(){
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
        $user_id = $this->data['users']->id;
        // $status_pulang = $this->input->post('status_pulang', TRUE);
        // $nama_rs = $this->input->post('nama_rs', TRUE);
        $data = array(
            'status_periksa'    => 'DIPULANGKAN',
            'petugas_id'        => $user_id
            // 'status_pasien'     => 0,
            // 'carapulang' => $status_pulang,
            // 'rs_rujukan' => $nama_rs,
        );

        $update_pendaftaran = $this->Kasirrj_model->update_pendaftaran($data, $pendaftaran_id);

        if($update_pendaftaran){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Berhasil set status pulang'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Berhasil set status pulang";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal set status pulang, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_darurat_pasienrd_view";
            $comments = "Gagal set status pulang";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }

    public function set_status_pulang_semua(){
        $tgl_kunjungan   = $this->input->get('tgl_kunjungan');
        $hari            = date('d',strtotime($tgl_kunjungan));
        $bulan           = date('m',strtotime($tgl_kunjungan));
        $tahun           = date('Y',strtotime($tgl_kunjungan));

        $tgl_kunjungan2 =  date('Y-m-d',strtotime($tgl_kunjungan));

        $list = $this->Kasirrj_model->get_alldata_kasir($tgl_kunjungan2);
        $count_data = count($list);
        $user_id = $this->data['users']->id;
        $data = array();

        if($count_data <= 0 ){
            $res=array(
                'success'   => FALSE,
                'messages'  => "Tidak ada pasien yang bisa dipulangkan"
            );
        }else {
            $update = $this->Kasirrj_model->set_status_pulang($hari,$bulan,$tahun,$user_id);
            if($update){
                $res = array(
                    'success'  => TRUE,
                    'messages' => "Semua pasien telah dipulangkan"
                );
            }else{
                $res = array(
                    'success' => FALSE,
                    'messages' => "Semua pasien gagal dipulangkan"
                );
            }
        }

        echo json_encode($res);
    }


    public function ajax_list_kasirrj(){
        $tgl_awal       = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir      = $this->input->get('tgl_akhir',TRUE);
        $poliruangan    = $this->input->get('poliruangan',TRUE);
        $dokter_poli    = $this->input->get('dokter_poli',TRUE);
        $no_pendaftaran = $this->input->get('nolist_pendaftaran',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs        = $this->input->get('no_bpjs',TRUE);
        $pasien_nama    = $this->input->get('pasien_nama',TRUE);
        $pasien_alamat  = $this->input->get('pasien_alamat',TRUE);
        $name_dokter    = $this->input->get('name_dokter',TRUE);
        $urutan         = $this->input->get('urutan',TRUE);
        $list = $this->Kasirrj_model->get_kasirrj_list($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan);

//        echo "<pre>";
//        print_r($list);
//        exit;

        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrj){
            if (strtolower($pasienrj->status_periksa) == "selesai" || strtolower($pasienrj->status_periksa) == "sudah pulang")
                continue;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrj->nama_poliruangan;
            $row[] = $pasienrj->no_pendaftaran;
            $row[] = date('d-M-Y H:i:s', strtotime($pasienrj->tgl_pendaftaran));
            $row[] = $pasienrj->no_rekam_medis;
            $row[] = $pasienrj->pasien_nama;
            $row[] = $pasienrj->jenis_kelamin;
            $row[] = $pasienrj->umur;
            $row[] = $pasienrj->pasien_alamat;
            $row[] = $pasienrj->type_pembayaran;
            $row[] = $pasienrj->status_periksa;
            $status_pulang = "disabled";
            $dis = "";

            if (strtolower($pasienrj->status_periksa) != "sudah bayar") {
                $status_pulang = "";
            }

            $pembayarankasir_id = @$this->Kasirrj_model->get_tindakan_umum_sudahbayar($pasienrj->pendaftaran_id)[0]->pembayarankasir_id ? $this->Kasirrj_model->get_tindakan_umum_sudahbayar($pasienrj->pendaftaran_id)[0]->pembayarankasir_id : 0;
            if ($pembayarankasir_id == 0) {
                $con = array(
                    'pasien_id' => $pasienrj->pasien_id,
                    'pendaftaran_id' => $pasienrj->pendaftaran_id
                );

                if (@$data_tindakan_dokter = $this->Models->where("rawatjalan_penjualan_obat", $con)[0]) {
                    if ($data_tindakan_dokter['pembayaran_id'] != 0) {
                        $pembayarankasir_id = $data_tindakan_dokter['pembayaran_id'];
                    }
                }
            }

            $row[] = '<button type="button" class="btn btn-success" title="Klik untuk view invoice" onclick="viewInvoice('. $pembayarankasir_id .','.$pasienrj->pendaftaran_id.')">View Invoice</button>';
            $row[] = '<button type="button" '.$status_pulang.' class="btn btn-success" data-toggle="modal" data-target="#modal_bayar_kasirrj" title="Klik untuk pemeriksaan pasien" onclick="bayarTagihan('. "'".$pasienrj->pendaftaran_id. "'" .','. "'Bayar'" .' )">Bayar</button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Kasirrj_model->count_kasirrj_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter),
            "recordsFiltered" => $this->Kasirrj_model->count_kasirrj_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_kasirrj_sudahbayar(){
        $tgl_awal       = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir      = $this->input->get('tgl_akhir',TRUE);
        $poliruangan    = $this->input->get('poliruangan',TRUE);
        $dokter_poli    = $this->input->get('dokter_poli',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs        = $this->input->get('no_bpjs',TRUE);
        $pasien_nama    = $this->input->get('pasien_nama',TRUE);
        $pasien_alamat  = $this->input->get('pasien_alamat',TRUE);
        $name_dokter    = $this->input->get('name_dokter',TRUE);
        $urutan         = $this->input->get('urutan',TRUE);
        $list = $this->Kasirrj_model->get_kasirrj_sudahbayar_list($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrj->nama_poliruangan;
            $row[] = $pasienrj->no_pendaftaran;
            $row[] = date('d-M-Y H:i:s', strtotime($pasienrj->tgl_pendaftaran));
            $row[] = $pasienrj->no_rekam_medis;
            $row[] = $pasienrj->pasien_nama;
            $row[] = $pasienrj->jenis_kelamin;
            $row[] = $pasienrj->umur;
            $row[] = $pasienrj->pasien_alamat;
            $row[] = $pasienrj->type_pembayaran;
            $row[] = $pasienrj->status_periksa;
            $row[] = '
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_bayar_kasirrj" data-backdrop="static" data-keyboard="false"  title="Klik untuk pemeriksaan pasien" onclick="bayarTagihan('. "'".$pasienrj->pendaftaran_id. "'" .','. "'Bayar'" .' )">Bayar</button>
        ';
            $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_bayar_kasirrj"  title="Klik untuk pemeriksaan pasien" onclick="bayarTagihan('. "'".$pasienrj->pendaftaran_id. "'" .','. "'Bayar'" .' )">Bayar</button>';
            $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_generate"  title="Klik untuk pemeriksaan pasien" onclick="generate('. "'".$pasienrj->pendaftaran_id. "'" .','. "'Bayar'" .' )">Generate</button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Kasirrj_model->count_kasirrj_sudahbayar_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter),
            "recordsFiltered" => $this->Kasirrj_model->count_kasirrj_sudahbayar_all($tgl_awal, $tgl_akhir, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function bayar_tagihan($pendaftaran_id, $action){
        $pasienrj['action']         = $action;
        $pasienrj['list_pasien']    = $this->Kasirrj_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienrj['tindakan_umum']  = $action == 'Bayar' ? $this->Kasirrj_model->get_tindakan_umum($pendaftaran_id) : $this->Kasirrj_model->get_tindakan_umum_sudahbayar($pendaftaran_id);
        $pasienrj['data_reservasi'] = $this->Models->where("t_reservasi", array("no_pendaftaran" => $pasienrj['list_pasien']->no_pendaftaran))[0];
        $pasienrj['total_obat']     = 0;
        $pasienrj['diskon_obat']    = 0;
        $pasienrj['total_dokter']   = 0;
        $pasienrj['diskon_dokter']  = 0;
        $pembayarankasir_id         = @$this->Kasirrj_model->get_tindakan_umum_sudahbayar($pendaftaran_id)[0]->pembayarankasir_id ? $this->Kasirrj_model->get_tindakan_umum_sudahbayar($pendaftaran_id)[0]->pembayarankasir_id : 0;

        if ($action == 'Edit') {
            $pasienrj['total_tindakan']    = $this->Kasirrj_model->sum_harga_tindakan_sudahbayar($pendaftaran_id);
            $pasienrj['discount_tindakan'] = $this->Kasirrj_model->sum_discount_tindakan_sudahbayar($pendaftaran_id);

            $penjualan_obat = $this->Models->where("rawatjalan_penjualan_obat", array('pembayaran_id' => $pembayarankasir_id));
            foreach ($penjualan_obat as $key => $value) {
                $pasienrj['total_obat']  = $pasienrj['total_obat'] + ($value['harga_total'] + $value['diskon']);
                $pasienrj['diskon_obat'] = $pasienrj['diskon_obat'] + $value['diskon'];
            }

            $data_fee_dokter = $this->Models->where("rawatjalan_feedokter", array('pembayaran_id' => $pembayarankasir_id))[0];
            $pasienrj['total_dokter']  = $data_fee_dokter['total_harga'];
            $pasienrj['diskon_dokter'] = $data_fee_dokter['diskon'];

            if (count($pasienrj['tindakan_umum']) < 1) {
                $pasienrj['cara_bayar'] = @$this->Models->where("rawatjalan_feedokter", array('pembayaran_id' => $pembayarankasir_id))[0]['cara_bayar'];
            } else {
                $pasienrj['cara_bayar'] = @$pasienrj['tindakan_umum'][0]->cara_bayar;
            }
        } else {
            $pasienrj['total_tindakan']    = $this->Kasirrj_model->sum_harga_tindakan($pendaftaran_id);
            $pasienrj['discount_tindakan'] = $this->Kasirrj_model->sum_discount_tindakan($pendaftaran_id);
            $data_tindakan                 = $this->Kasirrj_model->get_tindakan_umum_all($pendaftaran_id);
            $cur_pembayaran                = "";

            foreach ($data_tindakan as $list) {
                if ($list->cara_bayar != "") {
                    $cur_pembayaran = $list->cara_bayar;
                }
            }

            $condition = array(
                'pendaftaran_id' => $pendaftaran_id
            );

            $penjualan_obat = $this->Models->where("rawatjalan_penjualan_obat", $condition);
            foreach ($penjualan_obat as $key => $value) {
                $pasienrj['total_obat']  = $pasienrj['total_obat'] + ($value['harga_total'] + $value['diskon']);
                $pasienrj['diskon_obat'] = $pasienrj['diskon_obat'] + $value['diskon'];

                if ($cur_pembayaran == "") {
                    if ($value['cara_bayar'] != "") {
                        $cur_pembayaran = $value['cara_bayar'];
                    }
                }
            }

            if (count($this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $pendaftaran_id))) > 0) {
                $data_fee_dokter = $this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $pendaftaran_id))[0];

                if ($cur_pembayaran == "") {
                    if ($data_fee_dokter['cara_bayar'] != "") {
                        $cur_pembayaran = $data_fee_dokter['cara_bayar'];
                    }
                }
            } else {
                $data_fee_dokter['total_harga'] = 0;
                $data_fee_dokter['diskon'] = 0;
            }

            $pasienrj['total_dokter']  = $data_fee_dokter['total_harga'];
            $pasienrj['diskon_dokter'] = $data_fee_dokter['diskon'];

            if (count($pasienrj['tindakan_umum']) < 1) {
                $pasienrj['cara_bayar'] = @$this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $pendaftaran_id))[0]['cara_bayar'];
            } else {
                $pasienrj['cara_bayar'] = @$pasienrj['tindakan_umum'][0]->cara_bayar;
            }
        }

        $pasienrj['cur_pembayaran'] = $cur_pembayaran;
        $pasienrj['total_rad']       = $this->Kasirrj_model->sum_harga_rad($pendaftaran_id);
//        $pasienrj['discount_rad']    = $this->Kasirrj_model->sum_discount_rad($pendaftaran_id);
        $pasienrj['total_lab']       = $this->Kasirrj_model->sum_harga_lab($pendaftaran_id);
//        $pasienrj['discount_lab']    = $this->Kasirrj_model->sum_discount_lab($pendaftaran_id);
//        $pasienrj['obat'] = $this->Kasirrj_model->get_obat_pasien($pendaftaran_id);
//        $pasienrj['obat']            = $this->Models->where("rawatjalan_penjualan_obat", array('pendaftaran_id' => $pendaftaran_id));
        $pembayaran_id               = $pasienrj['list_pasien']->pembayaran_id;
        $pasienrj['data_asuransi']   = $this->Kasirrj_model->getCaraPembayaran($pembayaran_id);
        $pasienrj['list_type_bayar'] = array('1'=>'Pribadi','2'=>'Asuransi', '3'=>'Bill Hotel', '4'=> 'Tanggungan Hotel');
        $pasienrj['pembayarankasir_id'] = $pembayarankasir_id;

//        echo "<pre>";
//        print_r($pasienrj);
//        exit;

        $this->load->view('bayar_tagihan', $pasienrj);
    }

    public function print_kwitansi($pembayarankasir_id){
        $print['pembayarankasir_id'] = $pembayarankasir_id;
//        $get_pembayarankasir = $this->Kasirrj_model->get_pembayarankasir($pembayarankasir_id);
        $print['get_pembayar'] = $this->Kasirrj_model->get_pembayarankasir($pembayarankasir_id);
        $print['pasienrj'] = $this->Kasirrj_model->get_pendaftaran_pasien($print['get_pembayar']->pendaftaran_id);
        $print['data_reservasi'] = $this->Models->where("t_reservasi", array("no_pendaftaran" => $print['pasienrj']->no_pendaftaran))[0];
        $print['tindakan_umum'] = $this->Kasirrj_model->get_tindakan_umum($print['get_pembayar']->pendaftaran_id);
        if (count($print['tindakan_umum']) < 1) {
            $print['cara_bayar'] = @$this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $print['get_pembayar']->pendaftaran_id))[0]['cara_bayar'];
        } else {
            $print['cara_bayar'] = @$pasienrj['tindakan_umum'][0]->cara_bayar;
        }
        $print['tot_obat'] = 0;
        $print['tot_rad'] = $this->Kasirrj_model->sum_harga_rad_sudah_bayar($pembayarankasir_id);
        $print['tot_lab'] = $this->Kasirrj_model->sum_harga_lab_sudah_bayar($pembayarankasir_id);
        $print['tot_tind'] = $this->Kasirrj_model->sum_tindakan_sudah_bayar($pembayarankasir_id);
        $print['tindakan_list'] = $this->Kasirrj_model->get_tindakan_pasien($pembayarankasir_id);
        $print['total_bayar'] = $this->Kasirrj_model->get_print_bayar($pembayarankasir_id);
        $print['obat'] = $this->Models->where("rawatjalan_penjualan_obat", array('pembayaran_id' => $pembayarankasir_id));
        foreach ($print['obat'] as $key => $value) {
            $print['tot_obat'] = $print['tot_obat'] + $value['harga_total'];
        }

        $print['data_obat'] = $this->Models->all("farmasi_obat");

        $total = $this->Kasirrj_model->get_print_bayar($pembayarankasir_id);
        $print['terbilang'] = "" ;
        if (count($this->Models->where("rawatjalan_feedokter", array("pembayaran_id" => $pembayarankasir_id))) > 0) {
            $print['feeDokter'] = $this->Models->where("rawatjalan_feedokter", array("pembayaran_id" => $pembayarankasir_id))[0];
        } else {
            $print['feeDokter'] = 0;
        }


//        echo "<pre>";
//        print_r($print);
//        exit;

        $this->load->view('kasir/nota_pembayaran', $print);
    }

    public function view_nota($pendaftaran_id) {
        $data['pendaftaran']     = $this->Kasirrj_model->get_pendaftaran_pasien($pendaftaran_id);
        $data['pasien']          = $this->Kasirrj_model->get_data_pasien($data['pendaftaran']->pasien_id);
        $data['reservasi']       = @$this->Models->where("t_reservasi", array("no_pendaftaran" => $data['pendaftaran']->no_pendaftaran))[0];
        $data['no_invoice']      = $this->Kasirrj_model->get_no_pembayaran();

        $data['total_tindakan_umum'] = $data['total_tindakan_lab'] = $data['total_tindakan_obat'] = $data['total_diskon_obat'] = $data['total_harga_satuan_obat'] = 0;

        $data['tindakan']   = $this->Kasirrj_model->get_tindakan_pasien_all($pendaftaran_id);
        $data['tindakan_obat']   = $this->Models->where("rawatjalan_penjualan_obat", array('pendaftaran_id' => $pendaftaran_id));
        $data['tindakan_dokter'] = @$this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $pendaftaran_id))[0];

        $diskon_tindakan = 0;
        foreach($data['tindakan'] as $list){
            $total_harga  = $list->jml_tindakan * $list->harga_tindakan;
            $diskon_tindakan = $diskon_tindakan + $list->diskon;

            if ($list->type_tindakan=="TINDAKAN") {
                $data['total_tindakan_umum'] = $data['total_tindakan_umum'] + $total_harga;
            }

            if ($list->type_tindakan=="LABORATORIUM") {
                $data['total_tindakan_lab'] = $data['total_tindakan_lab'] + $total_harga;
            }
        }

        foreach ($data['tindakan_obat'] as $key => $value) {
            $data['total_diskon_obat'] = $data['total_diskon_obat'] + $value['diskon'];
            $data['total_harga_satuan_obat'] = $data['total_harga_satuan_obat'] + $value['harga_satuan'];
            $data['total_tindakan_obat'] = $data['total_tindakan_obat'] + ($value['harga_satuan'] * $value['jumlah_obat']);
        }

        $total = $data['total_tindakan_umum'] + $data['total_tindakan_lab'] + $data['total_tindakan_obat'] + $data['tindakan_dokter']['harga'];
        $data['surcharge']   = $total * 0.03;
        $data['total_bayar'] = ($total + $data['surcharge']) - ($diskon_tindakan + $data['total_diskon_obat'] + $data['tindakan_dokter']['diskon']);

        $this->load->view('view_nota', $data);
    }

    public function getCaraPembayaran($id){
        $data = $this->Kasirrj_model->getCaraPembayaran($id);
        echo json_encode($data);

    }

    public function generate($pendaftaran_id,$action){
        $pasienrj['action'] = $action;
        $pasienrj['list_pasien'] = $this->Kasirrj_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienrj['tindakan_umum'] = $this->Kasirrj_model->get_tindakan_umum($pendaftaran_id);

        $pembayarankasir_id['list_pasien'] = $this->Kasirrj_model->get_pembayarankasir($pendaftaran_id);

        if($action == 'Edit'){
            $pasienrj['total_tindakan'] = $this->Kasirrj_model->sum_harga_tindakan_sudahbayar($pendaftaran_id);
            $pasienrj['total_obat'] = $this->Kasirrj_model->sum_harga_obat_sudah_bayar($pembayarankasir_id);
        }else{
            $pasienrj['total_tindakan'] = $this->Kasirrj_model->sum_harga_tindakan($pendaftaran_id);
            $pasienrj['total_obat'] = $this->Kasirrj_model->sum_harga_obat($pendaftaran_id);
        }

        $pasienrj['total_rad'] = $this->Kasirrj_model->sum_harga_rad($pendaftaran_id);
        $pasienrj['total_lab'] = $this->Kasirrj_model->sum_harga_lab($pendaftaran_id);
        $pasienrj['obat'] = $this->Kasirrj_model->get_obat_pasien($pendaftaran_id);
        $pembayaran_id = $pasienrj['list_pasien']->pembayaran_id;
        $pasienrj['data_asuransi'] = $this->Kasirrj_model->getCaraPembayaran($pembayaran_id);

        $this->load->view('generaterj', $pasienrj);
    }

    public function get_items_by_id() {
        $id    = $this->input->get('id', TRUE);
        $jenis = $this->input->get('jenis', TRUE);

        if ($jenis == 0) {
            $data  = $this->Models->where("v_tindakanbelumbayar", array('tindakanpasien_id' => $id))[0];
        } else if ($jenis == 1) {
            $data  = $this->Models->show("rawatjalan_penjualan_obat", $id);
        } else {
            $data  = $this->Models->show("rawatjalan_feedokter", $id);
        }

        $message = array(
            'success' => true,
            'data' => $data
        );

        echo json_encode($message);
    }

    public function ajax_list_tindakan_umum(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $action         = $this->input->get('action', TRUE);
        $result         = "";

        if ($action == "Bayar") {
            $list_tindakan = $this->Kasirrj_model->get_tindakan_umum($pendaftaran_id);
            $data_fee_dokter = @$this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $pendaftaran_id, 'status_bayar' => 0))[0];
        } else {
            $list_tindakan = $this->Kasirrj_model->get_tindakan_umum_sudahbayar($pendaftaran_id);
            $data_fee_dokter = @$this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $pendaftaran_id, 'status_bayar' => 1))[0];
        }

        if ($data_fee_dokter == null) {
            $data_pendaftaran = $this->Models->where("t_pendaftaran", array('pendaftaran_id' => $pendaftaran_id))[0];
            $data_pasien = $this->Models->where("m_pasien", array('pasien_id' => $data_pendaftaran['pasien_id']))[0];
            $data_reservasi = $this->Models->where("t_reservasi", array('no_pendaftaran' => $data_pendaftaran['no_pendaftaran']))[0];
            $hotel_id = $data_reservasi['hotel_id'] != null ? $data_reservasi['hotel_id'] : 0;
            $fee_sokter = $this->get_fee_dokter_params($hotel_id, $data_pasien['jenis_pasien'], $data_pasien['type_call'], $data_reservasi['pemanggilan']);

            $data_fee_dokter = array(
                'pasien_id'      => $data_pasien['pasien_id'],
                'pendaftaran_id' => $pendaftaran_id,
                'harga'          => $fee_sokter,
                'diskon'         => 0,
                'total_harga'    => $fee_sokter,
                'cara_bayar'     => '',
                'pembayaran_id'  => 0,
                'status_bayar'   => 0
            );

            $result = $this->Models->insert("rawatjalan_feedokter", $data_fee_dokter);
        }

        $data = array();

        foreach($list_tindakan as $list){
            if ($list->cara_bayar == "") {
                $tgl_tindakan = date_create($list->tgl_tindakan);
                $row = array();
                $row[] = isset($list->cara_bayar) ? "" : '<input type="checkbox" onchange="cekListTindakan(this);" id="tindakan_list" name="id[]" value="'.$list->tindakanpasien_id.'" checked/>';
                $row[] = date_format($tgl_tindakan, 'd-M-Y');
                $row[] = $list->daftartindakan_nama;
                $row[] = "Rp. ". number_format($list->harga_tindakan);
                $row[] = $list->jml_tindakan;
                $row[] = "<span class='diskon'>Rp. ". number_format($list->diskon)."</span>";
                $row[] = "<span class='total'>Rp. ". number_format($list->total_harga)."</span>";
                $row[] = $list->cara_bayar;
                $row[] = '<button type="button" class="btn btn-success" onclick="open_diskon('.$list->tindakanpasien_id.')" >Diskon</button>'.
                    '<button type="button" class="btn btn-info" onclick="reset_diskon('.$list->tindakanpasien_id.')" >Reset</button>';

                $data[] = $row;
            }
        }

        $count_dokter = 0;

        if ($result == true) {
            if ($result == "Bayar") {
                $data_fee_dokter = @$this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $pendaftaran_id, 'status_bayar' => 0))[0];
            } else {
                $data_fee_dokter = @$this->Models->where("rawatjalan_feedokter", array('pendaftaran_id' => $pendaftaran_id, 'status_bayar' => 1))[0];
            }
        }

        if ($data_fee_dokter) {
            if ($data_fee_dokter["cara_bayar"] == "") {
                $tgl_tindakan = date_create($data_fee_dokter['created_at']);

                $row = array();
                $row[] = $data_fee_dokter["cara_bayar"] != "" ? "" : '<input type="checkbox" onchange="cekListDokter(this);" id="dokter_list" name="dokter[]" value="'.$data_fee_dokter['id'].'" checked/>';
                $row[] = date_format($tgl_tindakan, 'd-M-Y');
                $row[] = "Dokter Fee";
                $row[] = "Rp. ". number_format($data_fee_dokter['harga']);
                $row[] = "1";
                $row[] = "<span class='diskon'>Rp. ". number_format($data_fee_dokter['diskon'])."</span>";
                $row[] = "<span class='total'>Rp. ". number_format($data_fee_dokter['total_harga'])."</span>";
                $row[] = $data_fee_dokter['cara_bayar'];
                $row[] = '<button type="button" class="btn btn-success"   onclick="open_diskon_dokter('.$data_fee_dokter['id'].')" >Diskon</button>'.
                    '<button type="button" class="btn btn-info" onclick="reset_diskon_dokter('.$data_fee_dokter['id'].')" >Reset</button>';

                $data[] = $row;
            }

            $count_dokter = 1;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list_tindakan) + $count_dokter,
            "recordsFiltered" => count($list_tindakan) + $count_dokter,
            "data" => $data,
        );
        // output to json format

        echo json_encode($output);
    }

    public function ajax_list_tindakan_cover(){
        $tanggal = date('Y-m-d');
        $detail_id = $this->input->get('detail_id',TRUE);
        $pasien_id = $this->input->get('pasien_id',TRUE);
        // print_r($pasien_id);die();
        $list_tindakan = $this->Kasirrj_model->get_bayartindakan($pasien_id,$detail_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_bayar = date_create($list->tanggal_pembayaran);
            $row = array();
            $row[] = $list->daftartindakan_nama;
            $row[] = $list->nama_asuransi;
            $row[] = '
                    <button onclick="hapusTindakanCover('.$list->detailbayar_tindakan_id.')" type="button" class="btn btn-danger"><i class="fa fa-trash p-r-10"></i> Hapus</button>
                ';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list_tindakan),
            "recordsFiltered" => count($list_tindakan),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);

    }

    public function ajax_list_obat_cover(){
        $tanggal = date('Y-m-d');
        $pembayaran_id = $this->input->get('pembayaran_id',TRUE);
        $list_tindakan = $this->Kasirrj_model->get_obat_cover($pembayaran_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_bayar = date_create($list->tanggal_pembayaran);
            $row = array();
            $row[] = $list->nama_barang;
            $row[] = $list->cara_bayar;
            $row[] = '
                    <button onclick="hapusObatCover('.$list->detailbayar_obat_id.')" type="button" class="btn btn-danger"><i class="fa fa-trash p-r-10"></i> Hapus</button>
                ';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list_tindakan),
            "recordsFiltered" => count($list_tindakan),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);

    }

    public function hapusTindakanCover($id){
        $pasien_id = $this->input->get('pasien_id', TRUE);
        $detail_id = $this->input->get('detail_id', TRUE);
        $tanggal   = date('Y-m-d');
        $data = $this->Kasirrj_model->get_detailbayar_byid($id);
        // print_r($data);die();
        $push = $this->Kasirrj_model->delete_tindakan_cover($id);
        $data2['cara_bayar'] = 'PRIBADI';
        $tindakanpasien_id = $data->tindakanpasien_id;
        $push2 = $this->Kasirrj_model->live_edit('t_tindakanpasien','tindakanpasien_id',$tindakanpasien_id,$data2);

        if($push){
            $res = array(
                'success'   => TRUE
            );
        }else {
            $res = array(
                'success' => false
            );
        }

        echo json_encode($res);
    }

    public function hapusObatCover($id){
        $tanggal   = date('Y-m-d');
        $data = $this->Kasirrj_model->get_detailbayarObat_byid($id);
        $push = $this->Kasirrj_model->delete_obat_cover($id);


        $data2['type_pembayaran'] = 'PRIBADI';
        $reseptur_id = $data->reseptur_id;
        $push2 = $this->Kasirrj_model->live_edit('t_resepturpasien','reseptur_id',$reseptur_id,$data2);

        if($push){
            $res = array(
                'success'   => TRUE
            );
        }else {
            $res = array(
                'success' => false
            );
        }

        echo json_encode($res);
    }

    public function ajax_list_tindakan_rad(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_tindakan = $this->Kasirrj_model->get_tindakan_rad($pendaftaran_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_tindakan = date_create($list->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $list->daftartindakan_nama;
            $row[] = "Rp. ". number_format($list->harga_tindakan);
            $row[] = $list->jml_tindakan;
            $row[] = "Rp. ". number_format($list->total_harga_tindakan);
            $row[] = $list->is_cyto == 1 ? "Ya" : "Tidak";
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list_tindakan),
            "recordsFiltered" => count($list_tindakan),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_tindakan_lab(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_tindakan = $this->Kasirrj_model->get_tindakan_lab($pendaftaran_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_tindakan = date_create($list->tgl_tindakan);

            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $list->daftartindakan_nama;
            $row[] = "Rp. ". number_format($list->harga_tindakan);
            $row[] = $list->jml_tindakan;
            $row[] = "Rp. ". number_format($list->total_harga_tindakan);
            $row[] = $list->is_cyto == 1 ? "Ya" : "Tidak";
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list_tindakan),
            "recordsFiltered" => count($list_tindakan),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_list_obat_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id', TRUE);
        $action         = $this->input->get('action', TRUE);
        $status_bayar   = $action == "Bayar" ? 0 : 1;

        $condition = array(
            'pendaftaran_id' => $pendaftaran_id,
            'cara_bayar' => "",
            'status_bayar' => $status_bayar
        );

        $list = $this->Models->where("rawatjalan_penjualan_obat", $condition);
        $obat = $this->Models->all("farmasi_obat");
        $data = array();

        foreach ($list as $key => $value) {

//            $getPembayaranKasir = $this->Kasirrj_model->getJenisPembayaranTindakanObat($list->cara_bayar);
//            $getPembayaranKasir = $this->Models->where("t_pembayarankasir", array('pembayarankasir_id' => $value['pembayaran_id']));
//            $tipePembayaran = "";
//
//            if (count($getPembayaranKasir) > 0) {
//                @$tipePembayaran = $getPembayaranKasir[0]->cara_pembayaran;
//            }

//            $tipeObat = ($list->type_pembayaran == 0) ? "Umum";
            $statusObat = $value['cara_bayar'] != "" ? "" : '<input id="tindakan_list" type="checkbox" onchange="cekListObat(this);" name="id_obat[]" value="'.$value['id'].'" checked>';

            $row = array();
            $row[] = '<input id="tindakan_list" type="checkbox" onchange="cekListObat(this);" name="id_obat[]" value="'.$value['id'].'" checked>';
            $row[] = date( 'd-M-Y', strtotime($value['created_at']));
            $row[] = $obat[$value['obat_id']]['nama']  . " (Umum)";
            $row[] = "Rp. ". number_format($value['harga_satuan']);
            $row[] = $value['jumlah_obat'];
            $row[] = "<span class='diskon_obat'>Rp. ". number_format($value['diskon'])."</span>";
            $row[] = "<span class='total_obat'>Rp. ". number_format($value['harga_total'])."</span>";
            $row[] = $value['cara_bayar'];
            $row[] = '<button type="button"   class="btn btn-success" onclick="open_diskon_obat('.$value['id'].')" >Diskon</button>'.
                '<button type="button" class="btn btn-info" onclick="reset_diskon_obat('.$value['id'].')" >Reset</button>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_obat_pribadi_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_obat = $this->Kasirrj_model->get_obat_pasien_pribadi($pendaftaran_id);
        // print_r($list_obat);die();
        $data = array();

        foreach($list_obat as $list){
            $tgl_resep = date_create($list->tgl_reseptur);
            $row   = array();
            $row[] = date_format($tgl_resep, 'd-M-Y');
            $row[] = $list->nama_barang;
            $row[] = "Rp. ". number_format($list->harga_jual);
            $row[] = $list->qty;
            $row[] = "Rp. ". number_format($list->total_harga);
            $row[] = "Rp. ". number_format($list->diskon);
            $row[] = 'PRIBADI';
            $row[] = '
                    <button type="button" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modal_diskon" class="btn btn-success" onclick="open_diskon_obat('.$list->reseptur_id.')" > DISKON </button> ';
            $data[] = $row;
        }
        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list_obat),
            "recordsFiltered" => count($list_obat),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_pembayaran(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_jalan_action');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subtotal_val', 'Sub Total', 'required');
        $this->form_validation->set_rules('total_bayar_val', 'Total', 'required');
        $this->form_validation->set_rules('cara_bayar', 'Cara Bayar', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "kasir_rawat_jalan_action";
            $comments = "Gagal melakukan pembayaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('byr_pendaftaran_id',TRUE);
            $databayar['pendaftaran_id'] = $pendaftaran_id;
            $databayar['pasien_id'] = $this->input->post('byr_pasien_id',TRUE);
            $databayar['no_pembayaran'] = $this->Kasirrj_model->get_no_pembayaran();
            $databayar['tgl_pembayaran'] = date('Y-m-d');
            $databayar['totalbiayaobat'] = $this->input->post('total_obat_val',TRUE);
            $databayar['totalbiayatindakan'] = $this->input->post('total_tindakan_val',TRUE);
            $databayar['totalbiayalab'] = $this->input->post('total_lab_val',TRUE);
            $databayar['totalbiayarad'] = $this->input->post('total_rad_val',TRUE);
            $databayar['total_keseluruhan'] = $this->input->post('subtotal_val',TRUE);
            $databayar['total_discount'] = $this->input->post('discount',TRUE);
            $databayar['surcharge'] = str_replace(",","", $this->input->post('surcharge',TRUE));

            $data_pendaftaran = $this->Kasirrj_model->get_pendaftaran_pasien($pendaftaran_id);
            $data_tindakan = $this->Kasirrj_model->get_tindakan_pasien_list($pendaftaran_id);
            $data_reservasi = $this->Kasirrj_model->get_reservasi_by_no_pendaftaran($data_pendaftaran->no_pendaftaran);

//            echo "<pre>";
//            print_r($data_reservasi);
//            exit;

            if ($data_reservasi->driver_id == null) $data_reservasi->driver_id = 0;
            if ($data_reservasi->driver_id_2 == null) $data_reservasi->driver_id_2 = 0;

//            echo $data_reservasi->driver_id . " - " . $data_reservasi->driver_id_2;
//            exit;

            if ($data_reservasi->driver_id == 0 && $data_reservasi->driver_id_2 == 0) {
//                echo "<pre>";
//                print_r($data_tindakan);
//                exit;

                foreach ($data_tindakan as $list) {
                    if ($list->daftartindakan_id > 92 && $list->daftartindakan_id < 98) {
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'messages' => 'Driver wajib diisi jika ada tindakan dengan Ambulance'
                        );

                        // if permitted, do logit
                        $perms = "kasir_rawat_jalan_action";
                        $comments = "Driver wajib diisi jika ada tindakan dengan Ambulance";
                        $this->aauth->logit($perms, current_url(), $comments);

                        echo json_encode($res);
                        exit;
                    }

                    if ($list->daftartindakan_id > 112 && $list->daftartindakan_id < 125) {
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'messages' => 'Driver wajib diisi jika ada tindakan dengan Ambulance'
                        );

                        // if permitted, do logit
                        $perms = "kasir_rawat_jalan_action";
                        $comments = "Driver wajib diisi jika ada tindakan dengan Ambulance";
                        $this->aauth->logit($perms, current_url(), $comments);

                        echo json_encode($res);
                        exit;
                    }

                    if ($list->daftartindakan_id == 464) {
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'messages' => 'Driver wajib diisi jika ada tindakan dengan Ambulance'
                        );

                        // if permitted, do logit
                        $perms = "kasir_rawat_jalan_action";
                        $comments = "Driver wajib diisi jika ada tindakan dengan Ambulance";
                        $this->aauth->logit($perms, current_url(), $comments);

                        echo json_encode($res);
                        exit;
                    }

                    if ($list->daftartindakan_id > 588 && $list->daftartindakan_id < 593) {
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'messages' => 'Driver wajib diisi jika ada tindakan dengan Ambulance'
                        );

                        // if permitted, do logit
                        $perms = "kasir_rawat_jalan_action";
                        $comments = "Driver wajib diisi jika ada tindakan dengan Ambulance";
                        $this->aauth->logit($perms, current_url(), $comments);

                        echo json_encode($res);
                        exit;
                    }
                }
            }

            $max_perawat = 0;
            $data_perawat = $this->Kasirrj_model->get_perawat_pasien($databayar['pasien_id'], $pendaftaran_id);

            if (@$data_perawat->dokter_id_1 != 0) $max_perawat = $max_perawat + 1;
            if (@$data_perawat->dokter_id_2 != 0) $max_perawat = $max_perawat + 1;
            if (@$data_perawat->dokter_id_3 != 0) $max_perawat = $max_perawat + 1;

            if ($max_perawat < 2 || $data_perawat == null) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Perawat pasien wajib terisi minimal 2'
                );

                // if permitted, do logit
                $perms = "kasir_rawat_jalan_action";
                $comments = "Perawat pasien wajib terisi minimal 2";
                $this->aauth->logit($perms, current_url(), $comments);

                echo json_encode($res);
                exit;
            }

            if ($databayar['total_discount']==null) {
                $databayar['total_discount'] = 0;
            }

            $total = $this->input->post('total_bayar_val',TRUE);
            $databayar['total_bayar'] = $total;
            $databayar['cara_bayar'] = $this->input->post('cara_bayar',TRUE);
            $databayar['create_time'] = date('Y-m-d H:i:s');
            $databayar['create_by'] = $this->data['users']->id;
            $action   = $this->input->post('aksi',TRUE);

            //start transaction pendaftaran
            $this->db->trans_begin();
            // CHECK ACTION , APAKAH TAMBAH DATA ATAU EDIT DATA

            if($action == 'Bayar'){
                $insert_pembayaran = $this->Kasirrj_model->insert_pembayarankasir($databayar);
                $pembayaran_id = $this->db->insert_id();
            }else{
                $pembayarankasir_id = $this->input->post('pembayarankasir_id',TRUE);
                $insert_pembayaran = $this->Kasirrj_model->update_pembayarankasir($databayar,$pembayarankasir_id);
                $insert_pembayaran = $pembayarankasir_id;
            }

            if($insert_pembayaran){
                $status['status_periksa'] = "SUDAH BAYAR";
                // $status['status_pasien'] = 0;
                $this->Kasirrj_model->update_tindakan('t_pendaftaran' , $status, 'pendaftaran_id', $pendaftaran_id);

                if($this->Kasirrj_model->count_penunjang($pendaftaran_id) > 0){
                    $datapen['statusperiksa'] = "SUDAH PULANG";
                    $this->Kasirrj_model->update_tindakan('t_pasienmasukpenunjang' , $datapen, 'pendaftaran_id', $pendaftaran_id);
                }

                if($this->Kasirrj_model->count_tindakan_umum($pendaftaran_id) > 0){
                    $data['pembayarankasir_id'] = $insert_pembayaran;
                    $this->Kasirrj_model->update_tindakan('t_tindakanpasien' , $data, 'pendaftaran_id', $pendaftaran_id);
                }

                if($this->Kasirrj_model->count_tindakan_rad($pendaftaran_id) > 0){
                    $data['pembayarankasir_id'] = $insert_pembayaran;
                    $this->Kasirrj_model->update_tindakan('t_tindakanradiologi' , $data, 'pendaftaran_id', $pendaftaran_id);
                }

                if($this->Kasirrj_model->count_tindakan_lab($pendaftaran_id) > 0){
                    $data['pembayarankasir_id'] = $insert_pembayaran;
                    $this->Kasirrj_model->update_tindakan('t_tindakanlab' , $data, 'pendaftaran_id', $pendaftaran_id);
                }

                if($this->Kasirrj_model->count_obat($pendaftaran_id) > 0){
                    $data['pembayarankasir_id'] = $insert_pembayaran;
                    $this->Kasirrj_model->update_tindakan('t_resepturpasien' , $data, 'pendaftaran_id', $pendaftaran_id);
                }

                $data_update_penjualan = array(
                    'status_bayar' => 1,
                    'pembayaran_id' => $insert_pembayaran
                );

                $con = array(
                    'pasien_id' => $databayar['pasien_id'],
                    'pendaftaran_id' => $databayar['pendaftaran_id']
                );

                $data_penjualan = $this->Models->where("rawatjalan_penjualan_obat", $con);
                $data_fee_dokter = $this->Models->where("rawatjalan_feedokter", $con);

                foreach ($data_penjualan as $key => $value) {
                    $this->Models->update("rawatjalan_penjualan_obat", $data_update_penjualan, $value['id']);
                }

                foreach ($data_fee_dokter as $key => $value) {
                    $this->Models->update("rawatjalan_feedokter", $data_update_penjualan, $value['id']);
                }

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'pembayarankasir_id' => $insert_pembayaran,
                    'messages' => 'Payment has been saved to database'
                );

                // if permitted, do logit
                $perms = "kasir_rawat_jalan_action";
                $comments = "Success to Create a new payment";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Failed insert payment to database, please contact web administrator.'
                );

                // if permitted, do logit
                $perms = "kasir_rawat_jalan_action";
                $comments = "Failed to insert payment when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }

        echo json_encode($res);
    }

    public function do_create_pembayaranrj(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_jalan_action');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subtotal_val', 'Sub Total', 'required');
        $this->form_validation->set_rules('total_bayar_val', 'Total', 'required');
        $this->form_validation->set_rules('cara_bayar', 'Cara Bayar', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "kasir_rawat_jalan_action";
            $comments = "Gagal melakukan pembayaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('byr_pendaftaran_id',TRUE);
            $databayar['pendaftaran_id'] = $pendaftaran_id;
            $databayar['pasien_id'] = $this->input->post('byr_pasien_id',TRUE);
            $databayar['no_pembayaran'] = $this->Kasirrj_model->get_no_pembayaran();
            $databayar['tgl_pembayaran'] = date('Y-m-d');
            $databayar['totalbiayaobat'] = $this->input->post('total_obat_val',TRUE);
            $databayar['totalbiayatindakan'] = $this->input->post('total_tindakan_val',TRUE);
            $databayar['totalbiayalab'] = $this->input->post('total_lab_val',TRUE);
            $databayar['totalbiayarad'] = $this->input->post('total_rad_val',TRUE);
            $databayar['total_keseluruhan'] = $this->input->post('subtotal_val',TRUE);
            $databayar['total_discount'] = $this->input->post('discount',TRUE);

            $total = $this->input->post('total_bayar_val',TRUE);
            $pembulatan_angka = $this->pembulatan($total);

            $databayar['total_bayar'] = $pembulatan_angka;
            $databayar['cara_pembayaran'] = $this->input->post('cara_bayar',TRUE);
            $databayar['create_time'] = date('Y-m-d H:i:s');
            $databayar['create_by'] = $this->data['users']->id;
            $databayar['statuss'] = 'draft';
            $action   = $this->input->post('aksi',TRUE);

            //start transaction pendaftaran
            $this->db->trans_begin();
            // CHECK ACTION , APAKAH TAMBAH DATA ATAU EDIT DATA
            if($action == 'Bayar'){
                $insert_pembayaran = $this->Kasirrj_model->insert_pembayarankasir($databayar);
            }else{
                $pembayarankasir_id = $this->input->post('pembayarankasir_id',TRUE);
                $insert_pembayaran = $this->Kasirrj_model->update_pembayarankasir($databayar,$pembayarankasir_id);
                $insert_pembayaran = $pembayarankasir_id;
            }

            if($insert_pembayaran){
                // $status['status_periksa']           = "SUDAH BAYAR";
                // if($action == "Bayar"){
                //     $status['pembayarankasir_id']   = $insert_pembayaran;
                // }
                // // $status['status_pasien'] = 0;
                // $this->Kasirrj_model->update_tindakan('t_pendaftaran' , $status, 'pendaftaran_id', $pendaftaran_id);

                // if($this->Kasirrj_model->count_penunjang($pendaftaran_id) > 0){
                //     $datapen['statusperiksa'] = "SUDAH PULANG";
                //     $this->Kasirrj_model->update_tindakan('t_pasienmasukpenunjang' , $datapen, 'pendaftaran_id', $pendaftaran_id);
                // }

                // if($this->Kasirrj_model->count_tindakan_umum($pendaftaran_id) > 0){
                //     $data['pembayarankasir_id'] = $insert_pembayaran;
                //     $this->Kasirrj_model->update_tindakan('t_tindakanpasien' , $data, 'pendaftaran_id', $pendaftaran_id);
                // }

                // if($this->Kasirrj_model->count_tindakan_rad($pendaftaran_id) > 0){
                //     $data['pembayarankasir_id'] = $insert_pembayaran;
                //     $this->Kasirrj_model->update_tindakan('t_tindakanradiologi' , $data, 'pendaftaran_id', $pendaftaran_id);
                // }

                // if($this->Kasirrj_model->count_tindakan_lab($pendaftaran_id) > 0){
                //     $data['pembayarankasir_id'] = $insert_pembayaran;
                //     $this->Kasirrj_model->update_tindakan('t_tindakanlab' , $data, 'pendaftaran_id', $pendaftaran_id);
                // }

                // if($this->Kasirrj_model->count_obat($pendaftaran_id) > 0){
                //     $data['pembayarankasir_id'] = $insert_pembayaran;
                //     $this->Kasirrj_model->update_tindakan('t_resepturpasien' , $data, 'pendaftaran_id', $pendaftaran_id);
                // }

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'pembayarankasir_id' => $insert_pembayaran,
                    'messages' => 'Payment has been saved to database'
                );

                // if permitted, do logit
                $perms = "kasir_rawat_jalan_action";
                $comments = "Success to Create a new payment";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Failed insert payment to database, please contact web administrator.'
                );

                // if permitted, do logit
                $perms = "kasir_rawat_jalan_action";
                $comments = "Failed to insert payment when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }

        echo json_encode($res);
    }

    public function do_save_obat(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_jalan_action');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('cara_pembayaran_obat', 'Jumlah Bayar Obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "kasir_rawat_jalan_action";
            $comments = "Gagal melakukan pembayaran with errors = '".  validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $obatid     = $this->input->post('obatid[]',true);
            $bdata      = count($obatid);

            $nama_asuransi  = $this->input->post('nama_asuransi_obat',true);
            $bayarobat_id   = $this->input->post('pembayaran_obat_id',true);
            $pendaftaran_id = $this->input->post('pendaftaran_id',true);
            $bayarobat['pendaftaran_id']        = $pendaftaran_id;
            $bayarobat['type_pembayaran']       = "ASURANSI";
            $tanggal = date('Y-m-d');
            $bayarobat['tanggal_pembayaran']    = $tanggal;

            if(!empty($obatid)){
                // SECOND CONDITION, KONDISI SETELAH MEMPUNYAI PEMBAYARAN_OBAT_ID
                if(!empty($bayarobat_id) || $bayarobat_id != null) {
                    $messages = '';

                    for($i=0; $i < $bdata ; $i++ ){
                        $check = FALSE;
                        $last_data = $this->Kasirrj_model->get_obat_cover2($bayarobat_id,$obatid[$i] );
                        if($last_data > 0 ){
                            $val = $this->Kasirrj_model->get_obat_by_id($obatid[$i]);
                            $messages .="- ".$val->nama_barang." (SUDAH DICOVER), <BR>";
                            $check    = TRUE;
                            $success  = FALSE;
                        }

                        if($check == FALSE){
                            $val = $this->Kasirrj_model->get_obat_by_id($obatid[$i]);

                            $obat['pembayaran_obat_id']   = $bayarobat_id;
                            $obat['reseptur_id']          = $val->reseptur_id;
                            $obat['nama_barang']          = $val->nama_barang;
                            $obat['jml_obat']             = $val->qty;
                            $obat['harga_jual']           = $val->harga_jual;
                            $obat['total_harga']          = $val->total_harga;
                            $obat['cara_bayar']           = $nama_asuransi;
                            $this->db->insert('t_detailbayar_obat',$obat);

                            $data2['type_pembayaran'] = $nama_asuransi;
                            $push2 = $this->Kasirrj_model->live_edit('t_resepturpasien','reseptur_id',$obatid[$i],$data2);
                            $success = TRUE;
                        }
                    }
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => $success,
                        'pembayaran_obat_id' => $bayarobat_id,
                        'messages' => $messages
                    );

                }else{  // KONDISI SAAT PERTAMA KALI INSERT DATABASE DAN BELUM MEMPUNYAI PEMBAYARAN_OBAT_ID
                    $insert_obat = $this->Kasirrj_model->insert_bayarobat($bayarobat);
                    $data_tpendaftaran['det_obat_id'] = $insert_obat;
                    $this->db->update('t_pendaftaran',$data_tpendaftaran,array('pendaftaran_id' =>$pendaftaran_id ));

                    if($insert_obat){

                        for($i=0; $i < $bdata ; $i++ ){
                            $val = $this->Kasirrj_model->get_obat_by_id($obatid[$i]);

                            $obat['pembayaran_obat_id']   = $insert_obat;
                            $obat['reseptur_id']          = $val->reseptur_id;
                            $obat['nama_barang']          = $val->nama_barang;
                            $obat['jml_obat']             = $val->qty;
                            $obat['harga_jual']           = $val->harga_jual;
                            $obat['total_harga']          = $val->total_harga;
                            $obat['cara_bayar']           = $nama_asuransi;
                            $this->db->insert('t_detailbayar_obat',$obat);

                            $data2['type_pembayaran'] = $nama_asuransi;
                            $push2 = $this->Kasirrj_model->live_edit('t_resepturpasien','reseptur_id',$obatid[$i],$data2);
                        }

                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => true,
                            'pembayaran_obat_id' => $insert_obat
                        );

                    }else{
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'pembayaran_obat_id' => $insert_obat
                        );
                    }
                }
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'pembayaran_obat_id' => $bayarobat_id,
                    'messages' => "CENTANG OBAT TERLEBIH DAHULU"
                );
            }
        }

        echo json_encode($res);
    }

    public function do_save_obatrj(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_jalan_action');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }


        $this->load->library('form_validation');
        $this->form_validation->set_rules('cara_pembayaran_obat', 'Jumlah Bayar Obat', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);


            // if permitted, do logit
            $perms = "kasir_rawat_jalan_action";
            $comments = "Gagal melakukan pembayaran with errors = '".  validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $obatid     = $this->input->post('obatid[]',true);
            $bdata      = count($obatid);

            $nama_asuransi  = $this->input->post('nama_asuransi_obat',true);
            $bayarobat_id   = $this->input->post('pembayaran_obat_id',true);
            $pendaftaran_id = $this->input->post('pendaftaran_id',true);
            $bayarobat['pendaftaran_id']        = $pendaftaran_id;
            $bayarobat['type_pembayaran']       = "ASURANSI";
            $tanggal = date('Y-m-d');
            $bayarobat['tanggal_pembayaran']    = $tanggal;



            if(!empty($obatid)){
                // SECOND CONDITION, KONDISI SETELAH MEMPUNYAI PEMBAYARAN_OBAT_ID
                if(!empty($bayarobat_id) || $bayarobat_id != null) {
                    $messages = '';

                    for($i=0; $i < $bdata ; $i++ ){
                        $check = FALSE;
                        $last_data = $this->Kasirrj_model->get_obat_cover2($bayarobat_id,$obatid[$i] );
                        if($last_data > 0 ){
                            $val = $this->Kasirrj_model->get_obat_by_id($obatid[$i]);
                            $messages .="- ".$val->nama_barang." (SUDAH DICOVER), <BR>";
                            $check    = TRUE;
                            $success  = FALSE;
                        }

                        if($check == FALSE){
                            $val = $this->Kasirrj_model->get_obat_by_id($obatid[$i]);

                            $obat['pembayaran_obat_id']   = $bayarobat_id;
                            $obat['reseptur_id']          = $val->reseptur_id;
                            $obat['nama_barang']          = $val->nama_barang;
                            $obat['jml_obat']             = $val->qty;
                            $obat['harga_jual']           = $val->harga_jual;
                            $obat['total_harga']          = $val->total_harga;
                            $obat['cara_bayar']           = $nama_asuransi;
                            $this->db->insert('t_detailbayar_obat',$obat);

                            $data2['type_pembayaran'] = $nama_asuransi;
                            $push2 = $this->Kasirrj_model->live_edit('t_resepturpasien','reseptur_id',$obatid[$i],$data2);
                            $success = TRUE;
                        }
                    }
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => $success,
                        'pembayaran_obat_id' => $bayarobat_id,
                        'messages' => $messages
                    );

                }else{  // KONDISI SAAT PERTAMA KALI INSERT DATABASE DAN BELUM MEMPUNYAI PEMBAYARAN_OBAT_ID
                    $insert_obat = $this->Kasirrj_model->insert_bayarobat($bayarobat);
                    $data_tpendaftaran['det_obat_id'] = $insert_obat;
                    $this->db->update('t_pendaftaran',$data_tpendaftaran,array('pendaftaran_id' =>$pendaftaran_id ));

                    if($insert_obat){

                        for($i=0; $i < $bdata ; $i++ ){
                            $val = $this->Kasirrj_model->get_obat_by_id($obatid[$i]);

                            $obat['pembayaran_obat_id']   = $insert_obat;
                            $obat['reseptur_id']          = $val->reseptur_id;
                            $obat['nama_barang']          = $val->nama_barang;
                            $obat['jml_obat']             = $val->qty;
                            $obat['harga_jual']           = $val->harga_jual;
                            $obat['total_harga']          = $val->total_harga;
                            $obat['cara_bayar']           = $nama_asuransi;
                            $this->db->insert('t_detailbayar_obat',$obat);

                            $data2['type_pembayaran'] = $nama_asuransi;
                            $push2 = $this->Kasirrj_model->live_edit('t_resepturpasien','reseptur_id',$obatid[$i],$data2);
                        }

                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => true,
                            'pembayaran_obat_id' => $insert_obat
                        );

                    }else{
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'pembayaran_obat_id' => $insert_obat
                        );
                    }
                }
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'pembayaran_obat_id' => $bayarobat_id,
                    'messages' => "CENTANG OBAT TERLEBIH DAHULU"
                );
            }



        }

        echo json_encode($res);


    }



    public function do_save_tindakanumum(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipe_pembayaran_umum', 'Pembayaran', 'required');

        if ($this->form_validation->run() === FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);
            echo json_encode($res);
        }
        else {
            $pendaftaran_id = $this->input->post("pendaftaran_id", TRUE);
            $pasien_id = $this->input->post("pasien_id", TRUE);
            $pembayaran_id = $this->input->post("pembayaran_id", TRUE);
            $asuransi1 = $this->input->post("asuransi1", TRUE);
            $no_asuransi1 = $this->input->post("no_asuransi1", TRUE);
            $asuransi2 = $this->input->post("asuransi2", TRUE);
            $no_asuransi2 = $this->input->post("no_asuransi2", TRUE);
            $total_bayar_pribadi = $this->input->post("total_bayar_pribadi", TRUE);
            $total_bayar_bill_hotel = $this->input->post("total_bayar_bill_hotel", TRUE);
            $total_bayar_tanggung_hotel = $this->input->post("total_bayar_tanggung_hotel", TRUE);
            $total_bayar_asuransi = $this->input->post("total_bayar_asuransi", TRUE);
            $cara_pembayaran_umum = $this->input->post("cara_pembayaran_umum", TRUE) == "" ? "Cash" : $this->input->post("cara_pembayaran_umum", TRUE);
            $tipe_pembayaran_umum = $this->input->post("tipe_pembayaran_umum", TRUE);
            $action = $this->input->post("action", TRUE);
            $list_id_tindakan = $this->input->post("id[]", TRUE);
            $list_id_feedokter = $this->input->post("dokter[]", TRUE);

            $edc_bank  = $this->input->post("bank_edc_umum", TRUE);
            $edc_nomor = $this->input->post("nomor_edc_kartu_umum", TRUE);
            $edc_batch = $this->input->post("nomor_edc_batch_umum", TRUE);
            $cc_bank   = $this->input->post("bank_cc_umum", TRUE);
            $cc_nomor  = $this->input->post("nomor_cc_kartu_umum", TRUE);
            $cc_batch  = $this->input->post("nomor_cc_batch_umum", TRUE);
            $tf_bank   = $this->input->post("bank_tf_umum", TRUE);
            $tf_nomor  = $this->input->post("nomor_tf_kartu_umum", TRUE);
            $tf_batch  = $this->input->post("nomor_tf_batch_umum", TRUE);

            $checkAndGetIDPembayaranKasir = $this->Kasirrj_model->checkPembayaranKasirTindakan($pendaftaran_id);

            $total_harga_tindakan = 0;
            $status_tindakan = true;
            foreach ($checkAndGetIDPembayaranKasir as $item) {
                if ($list_id_tindakan != 0) {
                    foreach ($list_id_tindakan as $item_id_tindakan) {
                        if ($item->tindakanpasien_id == $item_id_tindakan) {
                            $total_harga_tindakan += $item->total_harga_tindakan;
                            if (!empty($item->pembayarankasir_id)) $status_tindakan = false;
                        }
                    }
                }
            }

            $pembayarankasir_id = $this->Kasirrj_model->checkPembayaranKasirID($pendaftaran_id);
            // echo $status_tindakan . " | " . $pembayarankasir_id;

            // Jika pembayaran kasir id yang kita select null, maka lakukan
            // maka buat pembayaran kasir baru

            if ($status_tindakan && empty($pembayarankasir_id->pembayarankasir_id)) {
                $dataInsert = array(
                    "pendaftaran_id"       => $pendaftaran_id,
                    "pasien_id"            => $pasien_id,
                    "tgl_pembayaran"       => date("Y-m-d H:i:s"),
                    "totalbiayatindakan"   => $total_harga_tindakan,
                    "cara_bayar"           => $cara_pembayaran_umum,
                    "create_time"          => date("Y-m-d H:i:s"),
                    "cara_pembayaran"      => $cara_pembayaran_umum,
                    "nama_asuransi_1"      => $asuransi1,
                    "no_asuransi_1"        => $no_asuransi1,
                    "nama_asuransi_2"      => $asuransi2,
                    "no_asuransi_2"        => $no_asuransi2,
                    "no_pembayaran"        => date("YmdHis"),
                    "create_by"            => $this->data['users']->id,
                    "bank_edc_umum"        => $edc_bank,
                    "nomor_edc_kartu_umum" => $edc_nomor,
                    "nomor_edc_batch_umum" => $edc_batch,
                    "bank_cc_umum"         => $cc_bank,
                    "nomor_cc_kartu_umum"  => $cc_nomor,
                    "nomor_cc_batch_umum"  => $cc_batch,
                    "bank_tf_umum"         => $tf_bank,
                    "nomor_tf_kartu_umum"  => $tf_nomor,
                    "nomor_tf_batch_umum"  => $tf_batch
                );

                $insertPembayaranKasir = $this->Kasirrj_model->insert_pembayarankasir($dataInsert);

                // Update Pembayaran Kasir Id On tindakanpasien
                if ($insertPembayaranKasir != 0) {
                    if ($list_id_tindakan != null) {
                        foreach ($list_id_tindakan as $item_id_tindakan) {
                            $this->Kasirrj_model->update_tindakanpasien(array(
                                "pembayarankasir_id" => $insertPembayaranKasir,
                                "cara_bayar" => $cara_pembayaran_umum == null ? $tipe_pembayaran_umum : $cara_pembayaran_umum
                            ), $item_id_tindakan);

                            $dataInsert = array(
                                "tindakan_poli" => "Tindakan Umum",
                                "pendaftaran_id" => $pendaftaran_id,
                                "pasien_id" => $pasien_id,
                                "type_pembayaran" => $cara_pembayaran_umum == null ? $tipe_pembayaran_umum : $cara_pembayaran_umum,
                                "tanggal_pembayaran" => date("Y-m-d H:i:s"),
                                "pembayarankasir_id" => $insertPembayaranKasir
                            );

                            $insertDetailPembayaranKasir = $this->Kasirrj_model->insert_detail_pembayarankasir($dataInsert);

                            // Cek jika detail pembayaran kasir terinser.
                            if ($insertDetailPembayaranKasir != 0) {

                                if (strtolower($tipe_pembayaran_umum) == "pribadi") {
                                    $total_harga_tindakan = $total_bayar_pribadi;
                                }
                                else if (strtolower($tipe_pembayaran_umum) == "bill hotel") {
                                    $total_harga_tindakan = $total_bayar_bill_hotel;
                                }
                                else if (strtolower($tipe_pembayaran_umum) == "tanggungan hotel") {
                                    $total_harga_tindakan = $total_bayar_tanggung_hotel;
                                }
                                else if (strtolower($tipe_pembayaran_umum) == "asuransi") {
                                    $total_harga_tindakan = $total_bayar_asuransi;
                                }

                                $dataInsert = array(
                                    "jumlah_pembayaran" => $total_harga_tindakan,
                                    "detail_pembayaran_id" => $insertDetailPembayaranKasir
                                );

                                $insertDetailCaraBayar = $this->Kasirrj_model->insert_detail_carabayar($dataInsert);
                            }
                        }
                    }

                    $this->Models->update("rawatjalan_feedokter", array('cara_bayar' => ($cara_pembayaran_umum == null ? $tipe_pembayaran_umum : $cara_pembayaran_umum)), $list_id_feedokter[0]);

                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => "Berhasil mengkonfirmasi pembayaran");
                    echo json_encode($res);
                }
                else {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => "Gagal mengkonfirmasi pembayaran 1");
                    echo json_encode($res);
                }
            }
            else if(($status_tindakan && !empty($pembayarankasir_id->pembayarankasir_id)) || (!$status_tindakan && !empty($pembayarankasir_id->pembayarankasir_id)) ) {
                $dataInsert = array(
                    "pendaftaran_id" => $pendaftaran_id,
                    "pasien_id" => $pasien_id,
                    "tgl_pembayaran" => date("Y-m-d H:i:s"),
                    "totalbiayatindakan" => $total_harga_tindakan,
                    "cara_bayar" => $cara_pembayaran_umum,
                    "create_time" => date("Y-m-d H:i:s"),
                    "cara_pembayaran" => $cara_pembayaran_umum,
                    "nama_asuransi_1" => $asuransi1,
                    "no_asuransi_1" => $no_asuransi1,
                    "nama_asuransi_2" => $asuransi2,
                    "no_asuransi_2" => $no_asuransi2,
                    "no_pembayaran" => date("YmdHis"),
                    "create_by" => $this->data['users']->id
                );

                $insertPembayaranKasir = $this->Kasirrj_model->update_pembayarankasir($dataInsert, $pembayarankasir_id->pembayarankasir_id);
                // Update Pembayaran Kasir Id On tindakanpasien
                if ($insertPembayaranKasir) {
                    if (count($list_id_tindakan) > 0) {
                        foreach ($list_id_tindakan as $item_id_tindakan) {
                            $this->Kasirrj_model->update_tindakanpasien(array(
                                "pembayarankasir_id" => $insertPembayaranKasir,
                                "cara_bayar" => $cara_pembayaran_umum == null ? $tipe_pembayaran_umum : $cara_pembayaran_umum
                            ), $item_id_tindakan);

                            $dataInsert = array(
                                "tindakan_poli" => "Tindakan Umum",
                                "pendaftaran_id" => $pendaftaran_id,
                                "pasien_id" => $pasien_id,
                                "type_pembayaran" => $cara_pembayaran_umum,
                                "tanggal_pembayaran" => date("Y-m-d H:i:s"),
                                "pembayarankasir_id" => $insertPembayaranKasir
                            );

                            $insertDetailPembayaranKasir = $this->Kasirrj_model->insert_detail_pembayarankasir($dataInsert);

                            // Cek jika detail pembayaran kasir terinser.
                            if ($insertDetailPembayaranKasir != 0) {

                                if (strtolower($cara_pembayaran_umum) == "pribadi") {
                                    $total_harga_tindakan = $total_bayar_pribadi;
                                }
                                else if (strtolower($cara_pembayaran_umum) == "bill hotel") {
                                    $total_harga_tindakan = $total_bayar_bill_hotel;
                                }
                                else if (strtolower($cara_pembayaran_umum) == "tanggungan hotel") {
                                    $total_harga_tindakan = $total_bayar_tanggung_hotel;
                                }

                                $dataInsert = array(
                                    "jumlah_pembayaran" => $total_harga_tindakan,
                                    "detail_pembayaran_id" => $insertDetailPembayaranKasir
                                );

                                $insertDetailCaraBayar = $this->Kasirrj_model->insert_detail_carabayar($dataInsert);
                            }
                        }
                    }

                    $this->Models->update("rawatjalan_feedokter", array('cara_bayar' => ($cara_pembayaran_umum == null ? $tipe_pembayaran_umum : $cara_pembayaran_umum)), $list_id_feedokter[0]);

                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => "Berhasil mengkonfirmasi pembayaran");
                    echo json_encode($res);
                }
                else {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => "Gagal mengkonfirmasi pembayaran 2");
                    echo json_encode($res);
                }
            }
            else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => "Gagal mengkonfirmasi pembayaran 3");
                echo json_encode($res);
            }
        }
    }

    public function do_save_tindakanumumrj(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_jalan_action');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('cara_pembayaran_umum', 'Cara Pembayaran', 'required');
        // $this->form_validation->set_rules('total_bayar_val', 'Total', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "kasir_rawat_jalan_action";
            $comments = "Gagal melakukan pembayaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            //jika ada variable detail_id = EDIT
            $row_select        = $this->input->post('id[]',TRUE);
            $action            = $this->input->post('action',TRUE);
            $bdata             = count($row_select);
            $total  = 0;
            for($i=0; $i < $bdata ; $i++ ){
                if($action == 'Edit'){
                    $result = $this->Kasirrj_model->get_tindakansudahbayar_by_id($row_select[$i]);
                }else{
                    $result = $this->Kasirrj_model->get_tindakan_by_id($row_select[$i]);
                }
                $harga  = $result->total_harga;
                $total += $harga;
            }

            $detail_id      = $this->input->post('detailpembayaran_id',TRUE);

            $no_asuransi_umum       = $this->input->post('nomor_asuransi_umum',TRUE);
            $nama_asuransi_umum     = $this->input->post('nama_asuransi_umum',TRUE);

            $bayartindakan['tindakan_poli']     = "Tindakan Umum";
            $bayartindakan['pasien_id']         = $this->input->post('pasien_id',TRUE);
            $bayartindakan['pendaftaran_id']    = $this->input->post('pendaftaran_id',TRUE);
            $bayartindakan['type_pembayaran']   = $this->input->post('cara_pembayaran_umum',TRUE);
            $tgl_bayar  = date_create($this->input->post('tanggal_pembayaran',TRUE));
            $tgl_bayar2 = date_format($tgl_bayar,'Y-m-d');
            $bayartindakan['tanggal_pembayaran']   = $tgl_bayar2;

            // DEKLARASI VARIABLE UNTUK KEPERLUAN FUNCTION Printkwitansi_asurannsi(bayar_tagihan.js)
            $list_pasien    = $this->Kasirrj_model->get_pendaftaran_pasien($bayartindakan['pendaftaran_id']);
            $pembayaran_id  = $list_pasien->pembayaran_id;
            $data_asuransi  = $this->Kasirrj_model->getCaraPembayaran($pembayaran_id);

            $nama_asuransi  = $data_asuransi->nama_asuransi;
            $nama_asuransi2 = $data_asuransi->nama_asuransi2;
            $det_obat_id    = $list_pasien->det_obat_id;
            // END DEKLARASI VARIABLE


            if(!empty($row_select)){
                // KONDISI SECOND INSERT
                if(!empty($detail_id)){
                    $tanggal = date('Y-m-d');
                    $messages = '';


                    for($i=0; $i < $bdata ; $i++ ){
                        $check = FALSE ;
                        $last_data = $this->Kasirrj_model->get_bayartindakan_check($bayartindakan['pasien_id'],$tanggal,$detail_id,$row_select[$i]);

                        if($last_data > 0 ){
                            $val = $this->Kasirrj_model->get_tindakan_by_id2($row_select[$i]);
                            $messages .= "- ".$val->daftartindakan_nama." (SUDAH DICOVER), <br>";
                            $check      = TRUE;
                            $success    = FALSE;
                        }

                        if($check == FALSE) {
                            if($action == 'Edit'){
                                $val = $this->Kasirrj_model->get_tindakansudahbayar_by_id($row_select[$i]);
                            }else{
                                $val = $this->Kasirrj_model->get_tindakan_by_id($row_select[$i]);
                            }
                            // print_r($val);die();
                            $tindakan['detail_pembayaran_id']   = $detail_id;
                            $tindakan['daftartindakan_id']      = $val->daftartindakan_id;
                            $tindakan['tindakanpasien_id']      = $row_select[$i];
                            $tindakan['jml_tindakan']           = $val->jml_tindakan;
                            $tindakan['total_harga_tindakan']   = $val->total_harga_tindakan;
                            $tindakan['nama_asuransi']          = $nama_asuransi_umum;
                            $tindakan['no_asuransi']            = $no_asuransi_umum;
                            $this->db->insert('t_detailbayar_tindakan',$tindakan);
                            $data['cara_bayar'] = $nama_asuransi;
                            $push = $this->Kasirrj_model->live_edit('t_tindakanpasien','tindakanpasien_id',$row_select[$i],$data);

                            $success = TRUE;
                        }

                    }


                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => $success,
                        'detail_pembayaran_id' => $detail_id,
                        'det_obat_id' => $det_obat_id,
                        'pasien_id' => $bayartindakan['pasien_id'],
                        'total' => $total,
                        'nama_asuransi' => $nama_asuransi,
                        'nama_asuransi2' => $nama_asuransi2,
                        'pendaftaran_id' => $bayartindakan['pendaftaran_id'],
                        'messages' => $messages
                    );
                    // if permitted, do logit
                    $perms = "kasir_rawat_jalan_action";
                    $comments = "Success to Create a new payment";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
                // KONDISI INSERT
                else{
                    $insert_bayartindakan = $this->Kasirrj_model->insert_bayartindakan($bayartindakan);
                    $pendaftaran_id = $bayartindakan['pendaftaran_id'];

                    $data_tpendaftaran['det_tindakan_id'] = $insert_bayartindakan;
                    $this->db->update('t_pendaftaran',$data_tpendaftaran,array('pendaftaran_id' =>$pendaftaran_id ));


                    if($insert_bayartindakan){

                        for($i=0; $i < $bdata ; $i++ ){
                            if($action == 'Edit'){
                                $val = $this->Kasirrj_model->get_tindakansudahbayar_by_id($row_select[$i]);
                            }else{
                                $val = $this->Kasirrj_model->get_tindakan_by_id($row_select[$i]);
                            }
                            $tindakan['detail_pembayaran_id']   = $insert_bayartindakan;
                            $tindakan['tindakanpasien_id']      = $row_select[$i];
                            $tindakan['daftartindakan_id']      = $val->daftartindakan_id;
                            $tindakan['jml_tindakan']           = $val->jml_tindakan;
                            $tindakan['total_harga_tindakan']   = $val->total_harga_tindakan;
                            $tindakan['nama_asuransi']          = $nama_asuransi_umum;
                            $tindakan['no_asuransi']            = $no_asuransi_umum;
                            $this->db->insert('t_detailbayar_tindakan',$tindakan);

                            $data['cara_bayar'] = $nama_asuransi;
                            $push = $this->Kasirrj_model->live_edit('t_tindakanpasien','tindakanpasien_id',$row_select[$i],$data);
                        }

                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => true,
                            'detail_pembayaran_id' => $insert_bayartindakan,
                            'det_obat_id' => $det_obat_id,
                            'pasien_id' => $bayartindakan['pasien_id'],
                            'nama_asuransi' => $nama_asuransi,
                            'nama_asuransi2' => $nama_asuransi2,
                            'total' => $total,
                            'pendaftaran_id' => $bayartindakan['pendaftaran_id'],
                            'messages' => 'Data berhasil diinput'
                        );
                        // if permitted, do logit
                        $perms = "kasir_rawat_jalan_action";
                        $comments = "Success to Create a new payment";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }else{
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'messages' => 'Data gagal diinput ke database.'
                        );

                        // if permitted, do logit
                        $perms = "kasir_rawat_jalan_action";
                        $comments = "Failed to insert payment when saving to database";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }
                }
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => "CENTANG TINDAKAN TERLEBIH DAHULU"
                );
            }
        }
        echo json_encode($res);
    }

    public function do_save_tindakanobat() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id_obat', 'Pendaftaran ID', 'required');
        $this->form_validation->set_rules('pasien_id_obat', 'Pasien ID', 'required');

        if ($this->form_validation->run() === FALSE) {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);
            echo json_encode($res);
        }
        else {
            $pendaftaran_id = $this->input->post("pendaftaran_id_obat", TRUE);
            $pasien_id = $this->input->post("pasien_id_obat", TRUE);
            $pembayaran_id = $this->input->post("pembayaran_id", TRUE);
            $asuransi1 = $this->input->post("asuransi1", TRUE);
            $no_asuransi1 = $this->input->post("no_asuransi1", TRUE);
            $asuransi2 = $this->input->post("asuransi2", TRUE);
            $no_asuransi2 = $this->input->post("no_asuransi2", TRUE);
            $total_bayar_pribadi = $this->input->post("total_bayar_pribadi", TRUE);
            $total_bayar_bill_hotel = $this->input->post("total_bayar_bill_hotel", TRUE);
            $total_bayar_tanggung_hotel = $this->input->post("total_bayar_tanggung_hotel", TRUE);
            $total_bayar_asuransi = $this->input->post("total_bayar_asuransi", TRUE);
            $cara_pembayaran_obat = $this->input->post("cara_pembayaran_umum", TRUE) == "" ? "Cash" : $this->input->post("cara_pembayaran_umum", TRUE);
            $tipe_pembayaran_obat = $this->input->post("tipe_pembayaran_umum", TRUE);
            $action = $this->input->post("action", TRUE);

            $edc_bank  = $this->input->post("bank_edc_umum", TRUE);
            $edc_nomor = $this->input->post("nomor_edc_kartu_umum", TRUE);
            $edc_batch = $this->input->post("nomor_edc_batch_umum", TRUE);
            $cc_bank   = $this->input->post("bank_cc_umum", TRUE);
            $cc_nomor  = $this->input->post("nomor_cc_kartu_umum", TRUE);
            $cc_batch  = $this->input->post("nomor_cc_batch_umum", TRUE);
            $tf_bank   = $this->input->post("bank_tf_umum", TRUE);
            $tf_nomor  = $this->input->post("nomor_tf_kartu_umum", TRUE);
            $tf_batch  = $this->input->post("nomor_tf_batch_umum", TRUE);
            $list_id_tindakan = $this->input->post("id_obat[]", TRUE);

            $data_tindakan = $this->Kasirrj_model->get_tindakan_umum_all($pendaftaran_id);

            foreach ($data_tindakan as $list) {
                if ($list->cara_bayar != "") {
                    $cara_pembayaran_obat = $list->cara_bayar;
                }
            }

            $data_update_penjualan = array(
                'cara_bayar' => $cara_pembayaran_obat,
            );

            $i = 0;
            for ($i = 0; $i<sizeof($list_id_tindakan); $i++) {
                $this->Models->update("rawatjalan_penjualan_obat", $data_update_penjualan, $list_id_tindakan[$i]);
            }

//            echo "<pre>";
//            print_r($_POST);
//            exit;

//            $checkAndGetIDPembayaranKasir = $this->Kasirrj_model->checkPembayaranKasirTindakanObat($pendaftaran_id);
            $q = "SELECT * FROM rawatjalan_penjualan_obat WHERE pendaftaran_id = " . $pendaftaran_id . " LIMIT 1";
            $checkAndGetIDPembayaranKasir = $this->Models->query($q);

            $total_harga_tindakan = 0;
            $status_tindakan = true;
            foreach ($checkAndGetIDPembayaranKasir as $key => $value) {
                if ($list_id_tindakan != null) {
                    foreach ($list_id_tindakan as $item_id_tindakan) {
                        if ($value['id'] == $item_id_tindakan) {
                            $total_harga_tindakan += $value['harga_total'];
                            if ($value['pembayaran_id'] != 0) $status_tindakan = false;
                        }
                    }
                }
            }

            $pembayarankasir_id = $this->Kasirrj_model->checkPembayaranKasirID($pendaftaran_id);
//            $pembayarankasir_id = $this->Models->query($q);

//             echo $status_tindakan . " | " . $pembayarankasir_id;
//             exit;

            // Jika pembayaran kasir id yang kita select null, maka lakukan
            // maka buat pembayaran kasir baru
            if ($status_tindakan && $pembayarankasir_id->pembayarankasir_id) {
                $dataInsert = array(
                    "pendaftaran_id" => $pendaftaran_id,
                    "pasien_id" => $pasien_id,
                    "tgl_pembayaran" => date("Y-m-d H:i:s"),
                    "totalbiayaobat" => $total_harga_tindakan,
                    "cara_bayar" => $cara_pembayaran_obat,
                    "create_time" => date("Y-m-d H:i:s"),
                    "cara_pembayaran" => $cara_pembayaran_obat,
                    "nama_asuransi_1" => $asuransi1,
                    "no_asuransi_1" => $no_asuransi1,
                    "nama_asuransi_2" => $asuransi2,
                    "no_asuransi_2" => $no_asuransi2,
                    "no_pembayaran" => date("YmdHis"),
                    "create_by" => $this->data['users']->id,
                    "bank_edc_umum"        => $edc_bank,
                    "nomor_edc_kartu_umum" => $edc_nomor,
                    "nomor_edc_batch_umum" => $edc_batch,
                    "bank_cc_umum"         => $cc_bank,
                    "nomor_cc_kartu_umum"  => $cc_nomor,
                    "nomor_cc_batch_umum"  => $cc_batch,
                    "bank_tf_umum"         => $tf_bank,
                    "nomor_tf_kartu_umum"  => $tf_nomor,
                    "nomor_tf_batch_umum"  => $tf_batch
                );

//                echo "<pre>";
//                print_r($dataInsert);
//                exit;

                $insertPembayaranKasir = $this->Kasirrj_model->insert_pembayarankasir($dataInsert);
                $id_pembayaran = $this->db->insert_id();

                // Update Pembayaran Kasir Id On tindakanpasien
                if ($insertPembayaranKasir != 0) {
                    foreach ($list_id_tindakan as $item_id_tindakan) {
                        $this->Kasirrj_model->update_obat(array(
                            "pembayarankasir_id" => $insertPembayaranKasir,
                            "type_pembayaran" => $cara_pembayaran_obat
                        ), $item_id_tindakan);

                        $dataInsert = array(
                            "tindakan_poli" => "Tindakan Obat",
                            "pendaftaran_id" => $pendaftaran_id,
                            "pasien_id" => $pasien_id,
                            "type_pembayaran" => $cara_pembayaran_obat,
                            "tanggal_pembayaran" => date("Y-m-d H:i:s"),
                            "pembayarankasir_id" => $insertPembayaranKasir
                        );

                        $insertDetailPembayaranKasir = $this->Kasirrj_model->insert_detail_pembayarankasir($dataInsert);

                        // Cek jika detail pembayaran kasir terinser.
                        if ($insertDetailPembayaranKasir != 0) {

                            if (strtolower($tipe_pembayaran_obat) == "pribadi") {
                                $total_harga_tindakan = $total_bayar_pribadi;
                            }
                            else if (strtolower($tipe_pembayaran_obat) == "bill hotel") {
                                $total_harga_tindakan = $total_bayar_bill_hotel;
                            }
                            else if (strtolower($tipe_pembayaran_obat) == "tanggungan hotel") {
                                $total_harga_tindakan = $total_bayar_tanggung_hotel;
                            }
                            else if (strtolower($tipe_pembayaran_obat) == "asuransi") {
                                $total_harga_tindakan = $total_bayar_asuransi;
                            }

                            $dataInsert = array(
                                "jumlah_pembayaran" => $total_harga_tindakan,
                                "detail_pembayaran_id" => $insertDetailPembayaranKasir
                            );

                            $insertDetailCaraBayar = $this->Kasirrj_model->insert_detail_carabayar($dataInsert);

                            $data_update_penjualan = array(
                                'cara_bayar' => $cara_pembayaran_obat,
                            );

                            $this->Models->update("rawatjalan_penjualan_obat", $data_update_penjualan, $item_id_tindakan);
                        }
                    }

//                    echo "<pre>";
//                    print_r($cara_pembayaran_obat);
//                    print_r($list_id_tindakan);
//                    exit;

                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => "Berhasil mengkonfirmasi pembayaran");
                    echo json_encode($res);
                }
                else {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => "Gagal mengkonfirmasi pembayaran 1");
                    echo json_encode($res);
                }
            }
            else if(($status_tindakan && !empty($pembayarankasir_id->pembayarankasir_id)) || (!$status_tindakan && !empty($pembayarankasir_id->pembayarankasir_id)) ) {
                $dataInsert = array(
                    "pendaftaran_id" => $pendaftaran_id,
                    "pasien_id" => $pasien_id,
                    "tgl_pembayaran" => date("Y-m-d H:i:s"),
                    "totalbiayaobat" => $total_harga_tindakan,
                    "cara_bayar" => $cara_pembayaran_obat,
                    "create_time" => date("Y-m-d H:i:s"),
                    "cara_pembayaran" => $cara_pembayaran_obat,
                    "nama_asuransi_1" => $asuransi1,
                    "no_asuransi_1" => $no_asuransi1,
                    "nama_asuransi_2" => $asuransi2,
                    "no_asuransi_2" => $no_asuransi2,
                    "no_pembayaran" => date("YmdHis"),
                    "create_by" => $this->data['users']->id,
                    "bank_edc_umum"        => $edc_bank,
                    "nomor_edc_kartu_umum" => $edc_nomor,
                    "nomor_edc_batch_umum" => $edc_batch,
                    "bank_cc_umum"         => $cc_bank,
                    "nomor_cc_kartu_umum"  => $cc_nomor,
                    "nomor_cc_batch_umum"  => $cc_batch,
                    "bank_tf_umum"         => $tf_bank,
                    "nomor_tf_kartu_umum"  => $tf_nomor,
                    "nomor_tf_batch_umum"  => $tf_batch
                );

                $insertPembayaranKasir = $this->Kasirrj_model->update_pembayarankasir($dataInsert, $pembayarankasir_id->pembayarankasir_id);
                // Update Pembayaran Kasir Id On tindakanpasien
                if ($insertPembayaranKasir) {
                    if ($list_id_tindakan != null) {
                        foreach ($list_id_tindakan as $item_id_tindakan) {
                            $this->Kasirrj_model->update_obat(array(
                                "pembayarankasir_id" => $insertPembayaranKasir,
                                "type_pembayaran" => $cara_pembayaran_obat
                            ), $item_id_tindakan);

                            $dataInsert = array(
                                "tindakan_poli" => "Tindakan Obat",
                                "pendaftaran_id" => $pendaftaran_id,
                                "pasien_id" => $pasien_id,
                                "type_pembayaran" => $cara_pembayaran_obat,
                                "tanggal_pembayaran" => date("Y-m-d H:i:s"),
                                "pembayarankasir_id" => $pembayarankasir_id->pembayarankasir_id
                            );

                            $insertDetailPembayaranKasir = $this->Kasirrj_model->insert_detail_pembayarankasir($dataInsert);

                            // Cek jika detail pembayaran kasir terinser.
                            if ($insertDetailPembayaranKasir != 0) {

                                if (strtolower($tipe_pembayaran_obat) == "pribadi") {
                                    $total_harga_tindakan = $total_bayar_pribadi;
                                }
                                else if (strtolower($tipe_pembayaran_obat) == "bill hotel") {
                                    $total_harga_tindakan = $total_bayar_bill_hotel;
                                }
                                else if (strtolower($tipe_pembayaran_obat) == "tanggungan hotel") {
                                    $total_harga_tindakan = $total_bayar_tanggung_hotel;
                                }
                                else if (strtolower($tipe_pembayaran_obat) == "asuransi") {
                                    $total_harga_tindakan = $total_bayar_asuransi;
                                }

                                $dataInsert = array(
                                    "jumlah_pembayaran" => $total_harga_tindakan,
                                    "detail_pembayaran_id" => $insertDetailPembayaranKasir
                                );

                                $insertDetailCaraBayar = $this->Kasirrj_model->insert_detail_carabayar($dataInsert);
                            }

                            $data_update_penjualan = array(
                                'cara_bayar' => $cara_pembayaran_obat,
                            );
                            $this->Models->update("rawatjalan_penjualan_obat", $data_update_penjualan, $item_id_tindakan);
                        }
                    }

                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => "Berhasil mengkonfirmasi pembayaran");
                    echo json_encode($res);
                }
                else {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => "Gagal mengkonfirmasi pembayaran 2");
                    echo json_encode($res);
                }
            }
            else {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => "Gagal mengkonfirmasi pembayaran 3");
                echo json_encode($res);
            }
        }
    }

    function check_detailtindakan_id(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $data = $this->Kasirrj_model->get_dataPendaftaran_by_id($pendaftaran_id);

        // print_r($data);die();

        $detail_tindakan = $data->det_tindakan_id;
        $detail_obat     = $data->det_obat_id;

        $res = array(
            'detail_tindakan' => $detail_tindakan,
            'detail_obat'     => $detail_obat
        );

        echo json_encode($res);
    }


    function saveCarabayar(){
        $nama_asuransi     = $this->input->post('nama_asuransi',TRUE);
        $row_select        = $this->input->post('id',TRUE);
        $bdata             = count($row_select);
        $total  = 0;
        for($i=0; $i < $bdata ; $i++ ){
            $result = $this->Kasirrj_model->get_tindakan_by_id($row_select[$i]);
            $harga  = $result->total_harga;
            $total += $harga;
        }
        if(!empty($row_select)){
            // INSERT DB
            for($i=0; $i < $bdata ; $i++ ){
                $data['cara_bayar'] = $nama_asuransi;
                $push = $this->Kasirrj_model->live_edit('t_tindakanpasien','tindakanpasien_id',$row_select[$i],$data);
            }

            if($push){
                $res = array(
                    'success' => true,
                    'total'   => $total
                );
            }else{
                $res = array(
                    'success' => false
                );
            }
        }else{
            $res = array(
                'success' => false
            );
        }
        echo json_encode($res);

    }

    function hitungdiskon(){
        $persen             = $this->input->post('persen',TRUE);
        $nominal            = $this->input->post('nominal',TRUE);
        $tindakanpasien_id  = $this->input->post('tindakanpasien_id',TRUE);
        $reseptur_id        = $this->input->post('reseptur_id',TRUE);
        $feedokter_id       = $this->input->post('fee_dokter_id',TRUE);
        $jenis              = $this->input->post('jenis',TRUE);
        $action             = $this->input->post('action',TRUE);

        // KONDISI JIKA DISKON TINDAKAN
        if($jenis == "tindakan"){
            // KONDISI JIKA DISKON MENGGUNAKAN PERSENTASE
            if(!empty($persen) || $persen != null) {
                if($action == "Edit"){
                    $data = $this->Kasirrj_model->get_tindakansudahbayar_by_id($tindakanpasien_id);
                }else{
                    $data = $this->Kasirrj_model->get_tindakan_by_id($tindakanpasien_id);
                }
                $hargatindakan = $data->harga_tindakan;
                $jml_tindakan = $data->jml_tindakan;
                $totaltindakan = $hargatindakan * $jml_tindakan;
                $diskon = ($totaltindakan * $persen) / 100;
                $total = $totaltindakan - $diskon;

                $push = array(
                    'total_harga' => $total,
                    'diskon' => $diskon
                );

                $db = $this->Kasirrj_model->live_edit('t_tindakanpasien','tindakanpasien_id', $tindakanpasien_id, $push);

                if($db){
                    $res = array(
                        'status'=> 'success',
                        'diskon' => $diskon
                    );
                }else{
                    $res = array(
                        'status'=> 'Gagal memasukan diskon',
                    );
                }
            }
            // KONDISI JIKA MENGGUNAKAN DISKON NOMINAL
            if(!empty($nominal) || $nominal != null ){
                if($action == "Edit"){
                    $data = $this->Kasirrj_model->get_tindakansudahbayar_by_id($tindakanpasien_id);
                }else{
                    $data = $this->Kasirrj_model->get_tindakan_by_id($tindakanpasien_id);
                }

                if ($data->total_harga < $nominal) {
                    echo json_encode(array('status'=> 'Nominal terlalu besar!'));
                    exit;
                }

                // print_r($data);die();

                $push = array(
                    'total_harga' => (($data->harga_tindakan * $data->jml_tindakan) - $nominal),
                    'diskon' => $nominal
                );

                $db = $this->Kasirrj_model->live_edit('t_tindakanpasien','tindakanpasien_id',$tindakanpasien_id,$push);

                if($db){
                    $res = array(
                        'status'=> 'success',
                        'diskon' => $nominal
                    );
                }else{
                    $res = array(
                        'status'=> 'Gagal memasukan diskon',
                    );
                }
            }
        } else if ($jenis == "obat") { // KONDISI JIKA DISKON OBAT
            $table_penjualan = "rawatjalan_penjualan_obat";

            // KONDISI JIKA MENGGUNAKAN DISKON PERSENTASE
            if(!empty($persen) || $persen != null) {
                if ($action == "Edit") {
                    $data = $this->Models->where($table_penjualan, array('id' => $reseptur_id, 'status_bayar' => 1))[0];
                } else {
                    $data = $this->Models->where($table_penjualan, array('id' => $reseptur_id, 'status_bayar' => 0))[0];
                }

                $diskon = (($data['harga_satuan'] * $data['jumlah_obat']) * $persen ) / 100;

                $push = array(
                    'harga_total' => ($data['harga_satuan'] * $data['jumlah_obat']) - $diskon,
                    'diskon' => $diskon
                );

                if($this->Models->update($table_penjualan, $push, $reseptur_id)) {
                    $res = array(
                        'status'=> 'success',
                        'diskon' => $diskon
                    );
                }else{
                    $res = array(
                        'status'=> 'Gagal memasukan diskon',
                    );
                }
            }

            // KONDISI JIKA MENGGUNAKAN DISKON NOMINAL
            if(!empty($nominal) || $nominal != null ){
                if ($action == "Edit") {
                    $data = $this->Models->where($table_penjualan, array('id' => $reseptur_id, 'status_bayar' => 1))[0];
                } else {
                    $data = $this->Models->where($table_penjualan, array('id' => $reseptur_id, 'status_bayar' => 0))[0];
                }

                $push = array(
                    'harga_total' => ($data['harga_satuan'] * $data['jumlah_obat']) - $nominal,
                    'diskon' => $nominal
                );

                if($this->Models->update($table_penjualan, $push, $reseptur_id)) {
                    $res = array(
                        'status'=> 'success',
                        'diskon' => $nominal
                    );
                }else{
                    $res = array(
                        'status'=> 'Gagal memasukan diskon',
                    );
                }
            }
        } else {  // KONDISI JIKA DISKON FEE DOKTER
            $table_feedokter = "rawatjalan_feedokter";

            // KONDISI JIKA MENGGUNAKAN DISKON PERSENTASE
            if(!empty($persen) || $persen != null) {
                if ($action == "Edit") {
                    $data = $this->Models->where($table_feedokter, array('id' => $feedokter_id, 'status_bayar' => 1))[0];
                } else {
                    $data = $this->Models->where($table_feedokter, array('id' => $feedokter_id, 'status_bayar' => 0))[0];
                }

                $diskon = ($data['harga'] * $persen ) / 100;

                $push = array(
                    'total_harga' => ($data['harga'] - $diskon),
                    'diskon'      => $diskon
                );

                if($this->Models->update($table_feedokter, $push, $feedokter_id)) {
                    $res = array(
                        'status'=> 'success',
                        'diskon' => $diskon
                    );
                }else{
                    $res = array(
                        'status'=> 'Gagal memasukan diskon',
                    );
                }
            }

            // KONDISI JIKA MENGGUNAKAN DISKON NOMINAL
            if(!empty($nominal) || $nominal != null ){
                if ($action == "Edit") {
                    $data = $this->Models->where($table_feedokter, array('id' => $feedokter_id, 'status_bayar' => 1))[0];
                } else {
                    $data = $this->Models->where($table_feedokter, array('id' => $feedokter_id, 'status_bayar' => 0))[0];
                }

                $push = array(
                    'total_harga' => ($data['harga'] - $nominal),
                    'diskon'      => $nominal
                );

                if($this->Models->update($table_feedokter, $push, $feedokter_id)) {
                    $res = array(
                        'status'=> 'success',
                        'diskon' => $nominal
                    );
                }else{
                    $res = array(
                        'status'=> 'Gagal memasukan diskon',
                    );
                }
            }
        }

        echo json_encode($res);
    }

    function hitungdiskonDokter(){
        $persen             = $this->input->post('persen',TRUE);
        $nominal            = $this->input->post('nominal',TRUE);
        $tindakanpasien_id  = $this->input->post('tindakanpasien_id',TRUE);
        $reseptur_id        = $this->input->post('reseptur_id',TRUE);
        $fee_dokter_id      = $this->input->post('fee_dokter_id',TRUE);
        $jenis              = $this->input->post('jenis',TRUE);
        $action             = $this->input->post('action',TRUE);

        $table_feedokter = "rawatjalan_feedokter";

        if(!empty($persen) || $persen != null) {
            if ($action == "Edit") {
                $data = $this->Models->where($table_feedokter, array('id' => $fee_dokter_id, 'status_bayar' => 1))[0];
            } else {
                $data = $this->Models->where($table_feedokter, array('id' => $fee_dokter_id, 'status_bayar' => 0))[0];
            }

            $harga     = $data['harga'];
            $total     = $data['total_harga'];

            // $total1 = $data->total_harga;
            $diskon = ($total * $persen) / 100;

            $total = $total - $diskon;

            $push = array(
                'total_harga' => $total,
                'diskon' => $data['diskon'] + $diskon
            );

            if($this->Models->update($table_feedokter, $push, $fee_dokter_id)) {
                $res = array(
                    'status'=> 'success',
                    'diskon' => $diskon
                );
            }else{
                $res = array(
                    'status'=> 'Error',
                );
            }
        }

        // KONDISI JIKA MENGGUNAKAN DISKON NOMINAL
        if(!empty($nominal) || $nominal != null ){
            if ($action == "Edit") {
                $data = $this->Models->where($table_feedokter, array('id' => $fee_dokter_id, 'status_bayar' => 1))[0];
            } else {
                $data = $this->Models->where($table_feedokter, array('id' => $fee_dokter_id, 'status_bayar' => 0))[0];
            }

            $harga     = $data['harga'];
            $total     = $data['total_harga'];

            $total = $total - $nominal;

            $push = array(
                'total_harga' => $total,
                'diskon' => $data['diskon'] + $nominal
            );

            if($this->Models->update($table_feedokter, $push, $fee_dokter_id)) {
                $res = array(
                    'status'=> 'success',
                    'diskon' => $nominal
                );
            }else{
                $res = array(
                    'status'=> 'Error',
                );
            }
        }

        echo json_encode($res);
    }

    function do_live_edit(){
        $col = $_POST['name'];
        $id_sub = $_POST['pk'];
        $diskon = $_POST['value'];
        $data = $this->Kasirrj_model->get_tindakan_by_id($id_sub);
        $hargatindakan = $data->harga_tindakan;
        $jml_tindakan = $data->jml_tindakan;
        $totaltindakan = $hargatindakan * $jml_tindakan;
        $hitung_diskon = ($totaltindakan * $diskon)/100;
        $total = $totaltindakan - $hitung_diskon;
        $push = array(
            'total_harga_tindakan' => $total,
            'total_harga'  => $total,
            $col => $diskon
        );
        $update = $this->Kasirrj_model->live_edit($id_sub,$push);
        if($update){
            $res = array(
                'status' => 'success'
            );
        }else{
            $res = array(
                'status' => 'error'
            );
        }

        echo json_encode($res);
    }


    function pembulatan($uang)
    {
        $ratusan = substr($uang, -3);
        if($ratusan<500)
            $akhir = $uang - $ratusan;
        else
            $akhir = $uang + (1000-$ratusan);
        return $akhir;
    }

    public function printNota($pembayarankasir_id){
        $print['pembayarankasir_id'] = $pembayarankasir_id;
        $get_pembayarankasir = $this->Kasirrj_model->get_pembayarankasir($pembayarankasir_id);
        $print['get_pembayar'] = $this->Kasirrj_model->get_pembayarankasir($pembayarankasir_id);
        $print['pasienrj'] = $this->Kasirrj_model->get_pendaftaran_pasien($get_pembayarankasir->pendaftaran_id);
        $print['tot_obat'] = $this->Kasirrj_model->sum_harga_obat_sudah_bayar($pembayarankasir_id);
        $print['tot_rad'] = $this->Kasirrj_model->sum_harga_rad_sudah_bayar($pembayarankasir_id);
        $print['tot_lab'] = $this->Kasirrj_model->sum_harga_lab_sudah_bayar($pembayarankasir_id);
        $print['tot_tind'] = $this->Kasirrj_model->sum_tindakan_sudah_bayar($pembayarankasir_id);
        $print['tindakan_list'] = $this->Kasirrj_model->get_tindakan_pasien($pembayarankasir_id);
        $print['total_bayar'] = $this->Kasirrj_model->get_print_bayar($pembayarankasir_id);
        $print['obat'] = $this->Kasirrj_model->get_obat($pembayarankasir_id);
        $total = $this->Kasirrj_model->get_print_bayar($pembayarankasir_id);
        $this->load->view('detail_pembayaran',$print);
    }

    public function print_kwitansi_2($pasien_id,$pendaftaran_id,$detail_id,$detailobat_id,$jenis_asuransi){
        $tanggal = date('Y-m-d');
        $jenis_asuransi = str_replace("%20"," ",$jenis_asuransi);
        $print['nama_asuransi'] = $jenis_asuransi;
        $print['tindakan_umum'] = $this->Kasirrj_model->get_bayartindakan2($pasien_id,$detail_id,$jenis_asuransi);
        $print['obat'] = $this->Kasirrj_model->get_bayarobat($pendaftaran_id,$detailobat_id,$jenis_asuransi);
        $print['pasienrj'] = $this->Kasirrj_model->get_pendaftaran_pasien($pendaftaran_id);
        $this->load->view('kasir/nota_asuransi', $print);
    }

    function numtowords($num){
        $decones = array(
            '01' => "One",
            '02' => "Two",
            '03' => "Three",
            '04' => "Four",
            '05' => "Five",
            '06' => "Six",
            '07' => "Seven",
            '08' => "Eight",
            '09' => "Nine",
            10 => "Ten",
            11 => "Eleven",
            12 => "Twelve",
            13 => "Thirteen",
            14 => "Fourteen",
            15 => "Fifteen",
            16 => "Sixteen",
            17 => "Seventeen",
            18 => "Eighteen",
            19 => "Nineteen"
        );
        $ones = array(
            0 => " ",
            1 => "One",
            2 => "Two",
            3 => "Three",
            4 => "Four",
            5 => "Five",
            6 => "Six",
            7 => "Seven",
            8 => "Eight",
            9 => "Nine",
            10 => "Ten",
            11 => "Eleven",
            12 => "Twelve",
            13 => "Thirteen",
            14 => "Fourteen",
            15 => "Fifteen",
            16 => "Sixteen",
            17 => "Seventeen",
            18 => "Eighteen",
            19 => "Nineteen"
        );
        $tens = array(
            0 => "",
            2 => "Twenty",
            3 => "Thirty",
            4 => "Forty",
            5 => "Fifty",
            6 => "Sixty",
            7 => "Seventy",
            8 => "Eighty",
            9 => "Ninety"
        );
        $hundreds = array(
            "Hundred",
            "Thousand",
            "Million",
            "Billion",
            "Trillion",
            "Quadrillion"
        ); //limit t quadrillion
        $num = number_format($num,2,".",",");
        $num_arr = explode(".",$num);
        $wholenum = $num_arr[0];
        $decnum = $num_arr[1];
        $whole_arr = array_reverse(explode(",",$wholenum));
        krsort($whole_arr);
        $rettxt = "";
        foreach($whole_arr as $key => $i){
            if($i < 20){
                $rettxt .= $ones[$i];
            }
            elseif($i < 100){
                $rettxt .= $tens[substr($i,0,1)];
                $rettxt .= " ".$ones[substr($i,1,1)];
            }
            else{
                $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
                $rettxt .= " ".$tens[substr($i,1,1)];
                $rettxt .= " ".$ones[substr($i,2,1)];
            }
            if($key > 0){
                $rettxt .= " ".$hundreds[$key]." ";
            }

        }
        $rettxt = $rettxt." peso/s";

        if($decnum > 0){
            $rettxt .= " and ";
            if($decnum < 20){
                $rettxt .= $decones[$decnum];
            }
            elseif($decnum < 100){
                $rettxt .= $tens[substr($decnum,0,1)];
                $rettxt .= " ".$ones[substr($decnum,1,1)];
            }
            $rettxt = $rettxt." centavo/s";
        }
        return $rettxt;
    }

    function rp_terbilang($angka,$debug){

        $angka = str_replace(".",",",$angka);

        list($angka, $desimal) = explode(",",$angka);
        $panjang=strlen($angka);
        for ($b=0;$b<$panjang;$b++)
        {
            $myindex=$panjang-$b-1;
            $a_bil[$b]=substr($angka,$myindex,1);
        }
        if ($panjang>9)
        {
            $bil=$a_bil[9];
            if ($panjang>10)
            {
                $bil=$a_bil[10].$bil;
            }

            if ($panjang>11)
            {
                $bil=$a_bil[11].$bil;
            }
            if ($bil!="" && $bil!="000")
            {
                $terbilang .= $this->rp_satuan($bil,$debug)." milyar";
            }

        }

        if ($panjang>6)
        {
            $bil=$a_bil[6];
            if ($panjang>7)
            {
                $bil=$a_bil[7].$bil;
            }

            if ($panjang>8)
            {
                $bil=$a_bil[8].$bil;
            }
            if ($bil!="" && $bil!="000")
            {
                $terbilang .= $this->rp_satuan($bil,$debug)." juta";
            }

        }

        if ($panjang>3)
        {
            $bil=$a_bil[3];
            if ($panjang>4)
            {
                $bil=$a_bil[4].$bil;
            }

            if ($panjang>5)
            {
                $bil=$a_bil[5].$bil;
            }
            if ($bil!="" && $bil!="000")
            {
                $terbilang .= $this->rp_satuan($bil,$debug)." ribu";
            }

        }

        $bil=$a_bil[0];
        if ($panjang>1)
        {
            $bil=$a_bil[1].$bil;
        }

        if ($panjang>2)
        {
            $bil=$a_bil[2].$bil;
        }
        //die($bil);
        if ($bil!="" && $bil!="000")
        {
            $terbilang .= $this->rp_satuan($bil,$debug);
        }
        return trim($terbilang);
    }

    function rp_satuan($angka,$debug){
        $a_str['1']="satu";
        $a_str['2']="dua";
        $a_str['3']="tiga";
        $a_str['4']="empat";
        $a_str['5']="lima";
        $a_str['6']="enam";
        $a_str['7']="tujuh";
        $a_str['8']="delapan";
        $a_str['9']="sembilan";


        $panjang=strlen($angka);
        for ($b=0;$b<$panjang;$b++)
        {
            $a_bil[$b]=substr($angka,$panjang-$b-1,1);
        }

        if ($panjang>2)
        {
            if ($a_bil[2]=="1")
            {
                $terbilang=" seratus";
            }
            else if ($a_bil[2]!="0")
            {
                $terbilang= " ".$a_str[$a_bil[2]]. " ratus";
            }
        }

        if ($panjang>1)
        {
            if ($a_bil[1]=="1")
            {
                if ($a_bil[0]=="0")
                {
                    $terbilang .=" sepuluh";
                }
                else if ($a_bil[0]=="1")
                {
                    $terbilang .=" sebelas";
                }
                else
                {
                    $terbilang .=" ".$a_str[$a_bil[0]]." belas";
                }
                return $terbilang;
            }
            else if ($a_bil[1]!="0")
            {
                $terbilang .=" ".$a_str[$a_bil[1]]." puluh";
            }
        }

        if ($a_bil[0]!="0")
        {
            $terbilang .=" ".$a_str[$a_bil[0]];
        }
        return $terbilang;

    }

    public function get_fee_dokter($hotel_id = false, $jenis_pasien, $type_call, $shift) {
        $shift = ($shift == "PAGI") ? 1 : 2;
        $result = 0;
        $feeDokterShift = $this->Models->where('fee_dokter_by_shift', array('shift' => $shift, 'branch' => $this->session->tempdata('data_session')[2]))[0];

        if ($type_call < 4) {
            if ($type_call == 3) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['vs_local'] = 0;
                    $feeDokterHotel['vs_domestik'] = 0;
                    $feeDokterHotel['vs_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['vs_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['vs_domestik'];
                } else {
                    $result = $feeDokterHotel['vs_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['vs_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['vs_dome'];
                } else {
                    $result = $feeDokterShift['vs_asing'];
                }
            }
        } else {
            if ($type_call == 5) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['oc_local'] = 0;
                    $feeDokterHotel['oc_domestik'] = 0;
                    $feeDokterHotel['oc_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['oc_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['oc_domestik'];
                } else {
                    $result = $feeDokterHotel['oc_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['oc_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['oc_dome'];
                } else {
                    $result = $feeDokterShift['oc_asing'];
                }
            }
        }

        return $result;
    }

    public function get_fee_dokter_params($hotel_id = false, $jenis_pasien, $type_call, $shift) {
        $shift = ($shift == "PAGI") ? 1 : 2;
        $result = 0;
        $feeDokterShift = $this->Models->where('fee_dokter_by_shift', array('shift' => $shift, 'branch' => $this->session->tempdata('data_session')[2]))[0];

        if ($type_call < 4) {
            if ($type_call == 3) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['vs_local'] = 0;
                    $feeDokterHotel['vs_domestik'] = 0;
                    $feeDokterHotel['vs_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['vs_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['vs_domestik'];
                } else {
                    $result = $feeDokterHotel['vs_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['vs_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['vs_dome'];
                } else {
                    $result = $feeDokterShift['vs_asing'];
                }
            }
        } else {
            if ($type_call == 5) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['oc_local'] = 0;
                    $feeDokterHotel['oc_domestik'] = 0;
                    $feeDokterHotel['oc_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['oc_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['oc_domestik'];
                } else {
                    $result = $feeDokterHotel['oc_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['oc_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['oc_dome'];
                } else {
                    $result = $feeDokterShift['oc_asing'];
                }
            }
        }

        return $result;
    }
}