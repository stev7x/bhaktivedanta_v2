<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasirri extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Kasirri_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_inap_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "kasir_rawat_inap_view";
        $comments = "List Kasir Rawat Inap";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_kasirri', $this->data);
    }

    public function ajax_list_kasirri(){
//        $tgl_awal = $this->input->get('tgl_awal',TRUE);
//        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $list = $this->Kasirri_model->get_kasirri_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienri){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienri->nama_poliruangan;
            $row[] = $pasienri->kelaspelayanan_nama;
            $row[] = $pasienri->no_pendaftaran;
            $row[] = $pasienri->tgl_pendaftaran;
            $row[] = $pasienri->no_rekam_medis;
            $row[] = $pasienri->pasien_nama;
            $row[] = $pasienri->jenis_kelamin;
            $row[] = $pasienri->umur;
            $row[] = $pasienri->pasien_alamat;
            $row[] = $pasienri->type_pembayaran;
            $row[] = $pasienri->status_periksa;
            if(trim(strtoupper($pasienri->status_periksa)) == "DIPULANGKAN"){
                $row[] = '<button disabled type="button" class="btn btn-default" title="Klik untuk bayar tagihan" onclick="bayarTagihan('."'".$pasienri->pendaftaran_id."'".')">Bayar</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="generate('."'".$pasienri->pendaftaran_id."'".')">Generate</button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_bayar_kasirri" data-backdrop="static" data-keyboard="false" title="Klik untuk bayar tagihan" onclick="bayarTagihan('."'".$pasienri->pendaftaran_id."'".')" >Bayar</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_bayar_kasirri" title="Klik untuk bayar tagihan" onclick="bayarTagihan('."'".$pasienri->pendaftaran_id."'".')" >Bayar</button>';
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="generate('."'".$pasienri->pendaftaran_id."'".')">Generate</button>';

            }

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kasirri_model->count_kasirri_all(),
                    "recordsFiltered" => $this->Kasirri_model->count_kasirri_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function bayar_tagihan($pendaftaran_id){
        $pasienri['list_pasien'] = $this->Kasirri_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienri['total_jasadokter'] = $this->Kasirri_model->sum_harga_jasadokter($pendaftaran_id);
        $pasienri['total_pelayananri'] = $this->Kasirri_model->sum_harga_jasaperawatan($pendaftaran_id);
        $pasienri['total_tindakanlain'] = $this->Kasirri_model->sum_harga_tindakan($pendaftaran_id);
        $pasienri['total_rad'] = $this->Kasirri_model->sum_harga_rad($pendaftaran_id);
        $pasienri['total_lab'] = $this->Kasirri_model->sum_harga_lab($pendaftaran_id);
        $pasienri['total_obat'] = $this->Kasirri_model->sum_harga_obat($pendaftaran_id);
        $pasienri['total_bhp'] = $this->Kasirri_model->sum_harga_bhp($pendaftaran_id);
        $pasienri['total_alkes'] = $this->Kasirri_model->sum_harga_alkes($pendaftaran_id);

        // echo "<pre>";
        // print_r ($pasienri);die();
        // echo "</pre>";

        $this->load->view('bayar_tagihanri', $pasienri);
    }

    public function ajax_list_tindakanlain(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_tindakan = $this->Kasirri_model->get_tindakanlain($pendaftaran_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_tindakan = date_create($list->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $list->daftartindakan_nama;
            $row[] = "Rp. ". number_format($list->harga_tindakan);
            $row[] = $list->jml_tindakan;
            $row[] = "Rp. ". number_format($list->total_harga_tindakan);
            $row[] = $list->is_cyto == 1 ? "Ya" : "Tidak";
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list_tindakan),
                    "recordsFiltered" => count($list_tindakan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_jasadokter(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_tindakan = $this->Kasirri_model->get_jasadokter($pendaftaran_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_tindakan = date_create($list->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $list->daftartindakan_nama;
            $row[] = "Rp. ". number_format($list->harga_tindakan);
            $row[] = $list->jml_tindakan;
            $row[] = "Rp. ". number_format($list->total_harga_tindakan);
            $row[] = $list->is_cyto == 1 ? "Ya" : "Tidak";
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list_tindakan),
                    "recordsFiltered" => count($list_tindakan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_jasaperawatan(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_tindakan = $this->Kasirri_model->get_jasaperawatan($pendaftaran_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_tindakan = date_create($list->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $list->daftartindakan_nama;
            $row[] = "Rp. ". number_format($list->harga_tindakan);
            $row[] = $list->jml_tindakan;
            $row[] = "Rp. ". number_format($list->total_harga_tindakan);
            $row[] = $list->is_cyto == 1 ? "Ya" : "Tidak";
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list_tindakan),
                    "recordsFiltered" => count($list_tindakan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_tindakan_rad(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_tindakan = $this->Kasirri_model->get_tindakan_rad($pendaftaran_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_tindakan = date_create($list->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $list->daftartindakan_nama;
            $row[] = "Rp. ". number_format($list->harga_tindakan);
            $row[] = $list->jml_tindakan;
            $row[] = "Rp. ". number_format($list->total_harga_tindakan);
            $row[] = $list->is_cyto == 1 ? "Ya" : "Tidak";
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list_tindakan),
                    "recordsFiltered" => count($list_tindakan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_tindakan_lab(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_tindakan = $this->Kasirri_model->get_tindakan_lab($pendaftaran_id);
        $data = array();

        foreach($list_tindakan as $list){
            $tgl_tindakan = date_create($list->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $list->daftartindakan_nama;
            $row[] = "Rp. ". number_format($list->harga_tindakan);
            $row[] = $list->jml_tindakan;
            $row[] = "Rp. ". number_format($list->total_harga_tindakan);
            $row[] = $list->is_cyto == 1 ? "Ya" : "Tidak";
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list_tindakan),
                    "recordsFiltered" => count($list_tindakan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_obat_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_obat = $this->Kasirri_model->get_obat_pasien($pendaftaran_id);
        $data = array();

        foreach($list_obat as $list){
            $tgl_resep = date_create($list->tgl_reseptur);
            $row = array();
            $row[] = date_format($tgl_resep, 'd-M-Y');
            $row[] = $list->nama_obat;
            $row[] = $list->satuan;
            $row[] = $list->signa;
            $row[] = "Rp. ". number_format($list->harga_jual);
            $row[] = $list->qty;
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list_obat),
                    "recordsFiltered" => count($list_obat),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_bhp_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_obat = $this->Kasirri_model->get_bhp_pasien($pendaftaran_id);
        $data = array();

        foreach($list_obat as $list){
            $tgl_resep = date_create($list->tgl_reseptur);
            $row = array();
            $row[] = date_format($tgl_resep, 'd-M-Y');
            $row[] = $list->nama_obat;
            $row[] = $list->satuan;
            $row[] = $list->signa;
            $row[] = "Rp. ". number_format($list->harga_jual);
            $row[] = $list->qty;
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list_obat),
                    "recordsFiltered" => count($list_obat),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_alkes_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list_obat = $this->Kasirri_model->get_alkes_pasien($pendaftaran_id);
        $data = array();

        foreach($list_obat as $list){
            $tgl_resep = date_create($list->tgl_reseptur);
            $row = array();
            $row[] = date_format($tgl_resep, 'd-M-Y');
            $row[] = $list->nama_obat;
            $row[] = $list->satuan;
            $row[] = $list->signa;
            $row[] = "Rp. ". number_format($list->harga_jual);
            $row[] = $list->qty;
            $row[] = "Rp. ". number_format($list->total_harga);
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list_obat),
                    "recordsFiltered" => count($list_obat),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

     function pembulatan($uang){
     $ratusan = substr($uang, -3);
     if($ratusan<500)
     $akhir = $uang - $ratusan;
     else
     $akhir = $uang + (1000-$ratusan);
     return $akhir;
    }

    public function generate($pendaftaran_id){
        $pasienrj['list_pasien'] = $this->Kasirri_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienrj['total_tindakan'] = $this->Kasirri_model->sum_harga_tindakan($pendaftaran_id);
        $pasienrj['total_rad'] = $this->Kasirri_model->sum_harga_rad($pendaftaran_id);
        $pasienrj['total_lab'] = $this->Kasirri_model->sum_harga_lab($pendaftaran_id);
        $pasienrj['total_obat'] = $this->Kasirri_model->sum_harga_obat($pendaftaran_id);

        $this->load->view('generaterri', $pasienrj);
    }

    public function do_create_generate(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_jalan_action');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subtotal_val', 'Sub Total', 'required');
        $this->form_validation->set_rules('total_bayar_val', 'Total', 'required');
        $this->form_validation->set_rules('cara_bayar', 'Cara Bayar', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "kasir_rawat_jalan_action";
            $comments = "Gagal melakukan pembayaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('byr_pendaftaran_id',TRUE);
            $databayar['pendaftaran_id'] = $pendaftaran_id;
            $databayar['pasien_id'] = $this->input->post('byr_pasien_id',TRUE);
            $databayar['no_pembayaran'] = $this->Kasirri_model->get_no_pembayaran();
            $databayar['tgl_pembayaran'] = date('Y-m-d');
            $databayar['totalbiayaobat'] = $this->input->post('total_obat_val',TRUE);
            $databayar['totalbiayatindakan'] = $this->input->post('total_tindakan_val',TRUE);
            $databayar['totalbiayalab'] = $this->input->post('total_lab_val',TRUE);
            $databayar['totalbiayarad'] = $this->input->post('total_rad_val',TRUE);
            $databayar['total_keseluruhan'] = $this->input->post('subtotal_val',TRUE);
            $databayar['total_discount'] = $this->input->post('discount',TRUE);

            $total = $this->input->post('total_bayar_val',TRUE);
            $pembulatan_angka = $this->pembulatan($total);
            // print_r($pembulatan_angka);die();
            $draft = "draft";
            $databayar['total_bayar'] = $pembulatan_angka;
            $databayar['cara_bayar'] = $this->input->post('cara_bayar',TRUE);
            $databayar['create_time'] = date('Y-m-d H:i:s');
            $databayar['create_by'] = $this->data['users']->id;
            $databayar['statuss'] = $draft;

            //start transaction pendaftaran
            $this->db->trans_begin();

            $insert_pembayaran = $this->Kasirri_model->insert_pembayarankasir($databayar);

            if($insert_pembayaran){
                // $status['status_periksa'] = "SUDAH PULANG";
                // $status['status_pasien'] = 0;
                // $this->Kasirri_model->update_tindakan('t_pendaftaran' , $status, 'pendaftaran_id', $pendaftaran_id);

                // if($this->Kasirri_model->count_penunjang($pendaftaran_id) > 0){
                //     $datapen['statusperiksa'] = "SUDAH PULANG";
                //     $this->Kasirri_model->update_tindakan('t_pasienmasukpenunjang' , $datapen, 'pendaftaran_id', $pendaftaran_id);
                // }

                // if($this->Kasirri_model->count_tindakan_umum($pendaftaran_id) > 0){
                //     $data['pembayarankasir_id'] = $insert_pembayaran;
                //     $this->Kasirri_model->update_tindakan('t_tindakanpasien' , $data, 'pendaftaran_id', $pendaftaran_id);
                // }

                // if($this->Kasirri_model->count_tindakan_rad($pendaftaran_id) > 0){
                //     $data['pembayarankasir_id'] = $insert_pembayaran;
                //     $this->Kasirri_model->update_tindakan('t_tindakanradiologi' , $data, 'pendaftaran_id', $pendaftaran_id);
                // }

                // if($this->Kasirri_model->count_tindakan_lab($pendaftaran_id) > 0){
                //     $data['pembayarankasir_id'] = $insert_pembayaran;
                //     $this->Kasirri_model->update_tindakan('t_tindakanlab' , $data, 'pendaftaran_id', $pendaftaran_id);
                // }

                // if($this->Kasirri_model->count_obat($pendaftaran_id) > 0){
                //     $data['pembayarankasir_id'] = $insert_pembayaran;
                //     $this->Kasirri_model->update_tindakan('t_resepturpasien' , $data, 'pendaftaran_id', $pendaftaran_id);
                // }

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'pembayarankasir_id' => $insert_pembayaran,
                    'messages' => 'Payment has been saved to database'
                );

                // if permitted, do logit
                $perms = "kasir_rawat_jalan_action";
                $comments = "Success to Create a new payment";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Failed insert payment to database, please contact web administrator.'
                );

                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Failed to insert payment when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }
        echo json_encode($res);
    }

    public function do_create_pembayaran(){
        $is_permit = $this->aauth->control_no_redirect('kasir_rawat_inap_action');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subtotal_val', 'Sub Total', 'required');
        $this->form_validation->set_rules('total_bayar_val', 'Total', 'required');
        $this->form_validation->set_rules('cara_bayar', 'Cara Bayar', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "kasir_rawat_inap_action";
            $comments = "Gagal melakukan pembayaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('byr_pendaftaran_id',TRUE);
            $tot_obat = $this->input->post('total_obat_val',TRUE);
            $tot_bhp = $this->input->post('total_bhp_val',TRUE);
            $tot_alkes = $this->input->post('total_alkes_val',TRUE);
            $tot_obatalkes = intval($tot_obat)+intval($tot_bhp)+intval($tot_alkes);

            $tot_jasadokter = $this->input->post('total_jasadokter_val',TRUE);
            $tot_jasaperawatan = $this->input->post('total_jasaperawatan_val',TRUE);
            $tot_tindakanlain = $this->input->post('total_tindakanlain_val',TRUE);
            $tot_tindakan = intval($tot_jasadokter)+intval($tot_jasaperawatan)+intval($tot_tindakanlain);

            $databayar['pendaftaran_id'] = $pendaftaran_id;
            $databayar['pasien_id'] = $this->input->post('byr_pasien_id',TRUE);
            $databayar['no_pembayaran'] = $this->Kasirri_model->get_no_pembayaran();
            $databayar['tgl_pembayaran'] = date('Y-m-d');
            $databayar['totalbiayaobat'] = $tot_obatalkes;
            $databayar['totalbiayatindakan'] = $tot_tindakan;
            $databayar['totalbiayalab'] = $this->input->post('total_lab_val',TRUE);
            $databayar['totalbiayarad'] = $this->input->post('total_rad_val',TRUE);
            $databayar['total_keseluruhan'] = $this->input->post('subtotal_val',TRUE);
            $databayar['total_discount'] = $this->input->post('discount',TRUE);

            $total = $this->input->post('total_bayar_val',TRUE);
            $pembulatan_angka = $this->pembulatan($total);
            // print_r($pembulatan_angka);die();

            $databayar['total_bayar'] = $pembulatan_angka;
            $databayar['cara_bayar'] = $this->input->post('cara_bayar',TRUE);
            $databayar['create_time'] = date('Y-m-d H:i:s');
            $databayar['create_by'] = $this->data['users']->id;

            //start transaction pendaftaran
            $this->db->trans_begin();

            $insert_pembayaran = $this->Kasirri_model->insert_pembayarankasir($databayar);

            if($insert_pembayaran){
                $status['status_periksa'] = "SUDAH PULANG";
                $status['status_pasien'] = 0;
                $this->Kasirri_model->update_common('t_pendaftaran' , $status, 'pendaftaran_id', $pendaftaran_id);

                $statusadmisi['statusrawat'] = "SUDAH PULANG";
                $this->Kasirri_model->update_common('t_pasienadmisi' , $statusadmisi, 'pendaftaran_id', $pendaftaran_id);

                if($this->Kasirri_model->count_penunjang($pendaftaran_id) > 0){
                    $datapen['statusperiksa'] = "SUDAH PULANG";
                    $this->Kasirri_model->update_common('t_pasienmasukpenunjang' , $datapen, 'pendaftaran_id', $pendaftaran_id);
                }

                if($this->Kasirri_model->count_tindakan_umum($pendaftaran_id) > 0){
                    $data['pembayarankasir_id'] = $insert_pembayaran;
                    $this->Kasirri_model->update_common('t_tindakanpasien' , $data, 'pendaftaran_id', $pendaftaran_id);
                }

                if($this->Kasirri_model->count_tindakan_rad($pendaftaran_id) > 0){
                    $data['pembayarankasir_id'] = $insert_pembayaran;
                    $this->Kasirri_model->update_common('t_tindakanradiologi' , $data, 'pendaftaran_id', $pendaftaran_id);
                }

                if($this->Kasirri_model->count_tindakan_lab($pendaftaran_id) > 0){
                    $data['pembayarankasir_id'] = $insert_pembayaran;
                    $this->Kasirri_model->update_common('t_tindakanlab' , $data, 'pendaftaran_id', $pendaftaran_id);
                }

                if($this->Kasirri_model->count_obat($pendaftaran_id) > 0){
                    $data['pembayarankasir_id'] = $insert_pembayaran;
                    $this->Kasirri_model->update_common('t_resepturpasien' , $data, 'pendaftaran_id', $pendaftaran_id);
                }

                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'pembayarankasir_id' => $insert_pembayaran,
                    'messages' => 'Payment has been saved to database'
                );

                // if permitted, do logit
                $perms = "kasir_rawat_inap_action";
                $comments = "Success to Create a new payment";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Failed insert payment to database, please contact web administrator.'
                );

                // if permitted, do logit
                $perms = "kasir_rawat_inap_action";
                $comments = "Failed to insert payment when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }

            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }

        echo json_encode($res);
    }

    public function print_kwitansi($pembayarankasir_id){
        $print['pembayarankasir_id'] = $pembayarankasir_id;
        $get_pembayarankasir = $this->Kasirri_model->get_pembayarankasir($pembayarankasir_id);
        $print['pembayarankasir'] = $get_pembayarankasir;
        $print['terbilang'] = $this->Kasirri_model->formatNumberTerbilang($get_pembayarankasir->total_bayar);
        $print['pasienri'] = $this->Kasirri_model->get_pendaftaran_pasien($get_pembayarankasir->pendaftaran_id);
        $print['tot_obat'] = $this->Kasirri_model->sum_harga_obat_sudah_bayar($pembayarankasir_id);
        $print['tot_bhp'] = $this->Kasirri_model->sum_harga_bhp_sudah_bayar($pembayarankasir_id);
        $print['tot_alkes'] = $this->Kasirri_model->sum_harga_alkes_sudah_bayar($pembayarankasir_id);
        $print['tot_rad'] = $this->Kasirri_model->sum_harga_rad_sudah_bayar($pembayarankasir_id);
        $print['tot_lab'] = $this->Kasirri_model->sum_harga_lab_sudah_bayar($pembayarankasir_id);
        $print['tot_tind'] = $this->Kasirri_model->sum_tindakan_sudah_bayar($pembayarankasir_id);
        $print['tot_jasadokter'] = $this->Kasirri_model->sum_jasadokter_sudah_bayar($pembayarankasir_id);
        $print['tot_jasaperawatan'] = $this->Kasirri_model->sum_jasaperawatan_sudah_bayar($pembayarankasir_id);
        $print['tindakan_list'] = $this->Kasirri_model->get_tindakan_pasien($pembayarankasir_id);
        $print['kasir'] = $this->data['users']->username;
        $this->load->view('kasir/print_kwitansiri', $print);
    }
}
