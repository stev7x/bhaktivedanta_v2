<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vaksinrd extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('Kasirrd_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    // Index
    public function index(){
        $is_permit = $this->aauth->control_no_redirect('kasir_vaksin_rawat_darurat_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "kasir_vaksin_rawat_darurat_view";
        $comments = "List Vaksin Kasir Rawat Darurat";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('vaksin_kasirrd', $this->data);
    }

    // Get List Pasien Belum Bayar
    public function ajax_list_kasirrd(){
        $list = $this->Kasirrd_model->get_kasirrd_list(); 
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienrj->nama_poliruangan;
            $row[] = $pasienrj->no_pendaftaran;
            $row[] = $pasienrj->tgl_pendaftaran;
            $row[] = $pasienrj->no_rekam_medis;
            $row[] = $pasienrj->pasien_nama;
            $row[] = $pasienrj->jenis_kelamin;
            $row[] = $pasienrj->umur;
            $row[] = $pasienrj->pasien_alamat;
            $row[] = $pasienrj->type_pembayaran;
            $row[] = $pasienrj->status_periksa;
            $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="bayarTagihan('."'".$pasienrj->pendaftaran_id."'".')">Bayar</button>';
            // $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="generate('."'".$pasienrj->pendaftaran_id."'".')">Generate</button>';

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kasirrd_model->count_kasirrd_all(),
                    "recordsFiltered" => $this->Kasirrd_model->count_kasirrd_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
}