<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vaksinri extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('Kasirri_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    // Index
    public function index(){
        $is_permit = $this->aauth->control_no_redirect('kasir_vaksin_rawat_inap_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "kasir_vaksin_rawat_inap_view";
        $comments = "List Vaksin Kasir Rawat Inap";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('vaksin_kasirri', $this->data);
    }

    // Get List Pasien Belum Bayar
    public function ajax_list_kasirri(){
        $list = $this->Kasirri_model->get_kasirri_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienri){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasienri->nama_poliruangan;
            $row[] = $pasienri->kelaspelayanan_nama;
            $row[] = $pasienri->no_pendaftaran;
            $row[] = $pasienri->tgl_pendaftaran;
            $row[] = $pasienri->no_rekam_medis;
            $row[] = $pasienri->pasien_nama;
            $row[] = $pasienri->jenis_kelamin;
            $row[] = $pasienri->umur;
            $row[] = $pasienri->pasien_alamat;
            $row[] = $pasienri->type_pembayaran;
            $row[] = $pasienri->status_periksa;
            if(trim(strtoupper($pasienri->status_periksa)) == "DIPULANGKAN"){
                $row[] = '<button disabled type="button" class="btn btn-default" title="Klik untuk bayar tagihan" onclick="bayarTagihan('."'".$pasienri->pendaftaran_id."'".')">Bayar</button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_bayar_kasirri" data-backdrop="static" data-keyboard="false" title="Klik untuk bayar tagihan" onclick="bayarTagihan('."'".$pasienri->pendaftaran_id."'".')" >Bayar</button>';
            }

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Kasirri_model->count_kasirri_all(),
                    "recordsFiltered" => $this->Kasirri_model->count_kasirri_filtered(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
}