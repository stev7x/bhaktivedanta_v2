<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class list_vk extends CI_Controller{
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Please login first.');
            redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('list_vk_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('vk_list_View');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "vk_list_View";
        $comments = "List Pasien Rawat Darurat";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_vk', $this->data);
    }

    public function ajax_list_pasienvk(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $list = $this->list_vk_model->get_pasienvk_list($tgl_awal, $tgl_akhir, $no_rekam_medis, $no_bpjs, $pasien_alamat, $nama_pasien, $no_pendaftaran);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrd){
            $tgl_lahir = $pasienrd->tanggal_lahir;
            $umur  = new Datetime($tgl_lahir);
            $today = new Datetime();
            $diff  = $today->diff($umur);

            $no++;
            $row = array();
            $row[] = $pasienrd->no_pendaftaran;
            $row[] = $pasienrd->no_rekam_medis;
            $row[] = $pasienrd->no_bpjs ;
            $row[] = $pasienrd->pasien_nama;
            $row[] = $pasienrd->tgl_pendaftaran;
            $row[] = $pasienrd->jenis_kelamin;
            // $row[] = $pasienrd->umur;
            $row[] = $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
            $row[] = $pasienrd->pasien_alamat;
            $row[] = $pasienrd->type_pembayaran;
            $row[] = $pasienrd->kelaspelayanan_nama;
            $row[] = $pasienrd->status_periksa;
            $row[] = !empty($pasienrd->carapulang) ? $pasienrd->carapulang : 'Not Set';
            //Action
            $row[] = '<div class="dropdown-action" onclick="$(this).children().toggleClass('."'d-block'".')">
                        <span class="title-dropdown">Pilih Proses</span><span class="arrow">▼</span>
                        <div class="dropdown-menu-action">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_pasienrd" title="Klik untuk pemeriksaan pasien" >PERIKSA</button> 
                        <button type="button" class="btn btn-success"  title="K.K OK">K.K OK</button>
                        <button type="button" class="btn btn-success"  title="K.K VK">K.K VK</button>
                        <button type="button" class="btn btn-success"  title="K.K HCU">K.K HCU</button>
                        <button type="button" class="btn btn-success"  title="K.K Pathologi Anak">K.K Pathologi Anak</button>
                        <button type="button" class="btn btn-success"  title="Rujuk Rs Lain">Rujuk Rs Lain</button>
                        <button type="button" class="btn btn-success"  title="Batal">Batal</button>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_kirim_penunjang" title="Klik untuk kirim ke penunjang">PENUNJANG</button>
                        </div>
                    </div>
                ';

            $data[] = $row;
        }
        
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->list_vk_model->count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "recordsFiltered" => $this->list_vk_model->count_all_list_filtered($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }
}
?>