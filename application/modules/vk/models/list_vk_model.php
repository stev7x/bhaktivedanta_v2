<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class list_vk_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }

    public function get_pasienvk_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs,$no_pendaftaran){
        $this->db->from('v_list_vk');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function count_all_list($tgl_awal, $tgl_akhir, $nama_pasien, $no_rekam_medis, $pasien_alamat,$no_bpjs, $no_pendaftaran){
        $this->db->from('v_list_vk');
         $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        
        return $this->db->count_all_results();
    }


    public function count_all_list_filtered($nama_pasien,  $no_rekam_medis, $pasien_alamat, $no_bpjs, $no_pendaftaran){
        $this->db->from('v_list_vk');
        
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
}
?>