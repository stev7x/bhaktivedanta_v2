<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Inventori  -  Barang </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Inventori</a></li>
                            <li class="active">Barang</li> 
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">  
                            <div class="panel-heading"> Data Barang
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">  
                                <div class="row">
                                <div class="col-md-2">
                                <label for="inputName1" class="control-label"></label>
                                    <dl class="text-right">  
                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                        <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                    </dl> 
                                </div> 
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label>Tanggal Kunjungan,Dari</label>
                                            <div class="input-group">   
                                                <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                             </div>
                                        </div>
                                        <div class="col-md-5">  
                                            <label>Sampai</label>    
                                            <div class="input-group">   
                                                <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                             </div>
                                        </div>
                                        <div class="col-md-2"> 
                                            <label>&nbsp;</label><br>
                                            <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                        </div> 
                                        <div class="form-group col-md-3" style="margin-top: 7px"> 
                                            <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                            <b>
                                            <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                <option value="" disabled selected>Pilih Berdasarkan</option>
                                                <option value="no_masukpenunjang">No. Pendaftaran</option>
                                                <option value="no_rekam_medis">No. Rekam Medis</option>
                                                <option value="no_bpjs">No. BPJS</option>
                                                <option value="nama_pasien">Nama Pasien</option>   
                                                <option value="pasien_alamat">Alamat Pasien</option>   
                                                <!-- <option value="status">Status</option>    -->
                                            </select></b>
                                                 
                                        </div>    
                                        <div class="form-group col-md-9" style="margin-top: 7px">   
                                            <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                            <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasien()" >       
                                        </div> 
                                    </div>
                                </div> 
                            </div><br><hr style="margin-top: -27px">
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_gizi" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus p-r-10"></i>Tambah Barang</button>
                                <div class="table-responsive">   
                                   <table id="table_list_gizi" class="table table-striped dataTable" cellspacing="0">
                                            <thead>  
                                                <tr> 
                                                    <th>No</th>
                                                    <th>Kode Barang</th>
                                                    <th>Nama Barang</th>
                                                    <th>Jumlah Barang</th>
                                                    <th>Kondisi Barang</th>
                                                    <th>Action</th>
                                                </tr>  
                                            </thead>
                                            <tbody> 
                                                <tr>   
                                                    <td colspan="4">No Data to Display</td>
                                                </tr>
                                            </tbody>
                                        </table>  
                                    </div>
                                </div>
                            </div>  
                        </div> 
                        
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->

<div id="modal_add_gizi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-lg">         
        <div class="modal-content">   
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Tambah Pemesanan</b></h2>     
             </div>  
            <div class="modal-body">
                <div class="row">     
                    <div class="col-md-6">     
                        <div class="form-group">
                            <label>Nama Pasien<span style="color: red;">*</span></label>
                            <input type="text" name="#" class="form-control" placeholder="Nama Pasien">
                        </div>
                        <div class="form-group">
                            <label>Jenis Terapi diet<span style="color: red;">*</span></label>
                            <select class="form-control">
                                <option>Nasi</option>
                                <option>Bubur Halus</option>
                                <option>Bubur Kasar</option>
                            </select>
                        </div>  
                        <div class="form-group">
                            <label>Keterangan</label>  
                            <textarea class="form-control" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Ruangan<span style="color: red;">*</span></label>
                            <select class="form-control">
                                <option>TEST</option>
                                <option>TEST</option>
                            </select>
                        </div> 
                        <div class="form-group">
                            <label>Kelas Pelayanan<span style="color: red;">*</span></label>
                            <select class="form-control">
                                <option>TEST</option>
                                <option>TEST</option>  
                            </select>
                        </div> 
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-success"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>  
                    </div>
                </div>  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

  
<div id="modal_edit_gizi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-lg">         
        <div class="modal-content">   
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Edit Pemesanan</b></h2>     
             </div>  
            <div class="modal-body">
                <div class="row">     
                    <div class="col-md-6">     
                        <div class="form-group">
                            <label>Nama Pasien<span style="color: red;">*</span></label>
                            <input type="text" name="#" class="form-control" placeholder="Nama Pasien">
                        </div>
                        <div class="form-group">
                            <label>Jenis Terapi diet<span style="color: red;">*</span></label>
                            <select class="form-control">
                                <option>Nasi</option>
                                <option>Bubur Halus</option>
                                <option>Bubur Kasar</option>
                            </select>
                        </div>  
                        <div class="form-group">
                            <label>Keterangan</label>  
                            <textarea class="form-control" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Ruangan<span style="color: red;">*</span></label>
                            <select class="form-control">
                                <option>TEST</option>
                                <option>TEST</option>
                            </select>
                        </div> 
                        <div class="form-group">
                            <label>Kelas Pelayanan<span style="color: red;">*</span></label>
                            <select class="form-control">
                                <option>TEST</option>
                                <option>TEST</option>  
                            </select>
                        </div> 
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-success"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>  
                    </div>
                </div>  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
           

<?php $this->load->view('footer');?>
      