<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Radiologi_model extends CI_Model {

    var $columnListDokter = array('daftartindakan_nama');
    var $orderListDokter = array('daftartindakan_nama' => 'ASC');

    var $columnListUser = array('id');
    var $orderListUser = array('id' => 'ASC');

    public function __construct(){
        parent::__construct();
    }

    public function get_radiologi_list($tgl_awal, $tgl_akhir, $status, $no_masukpenunjang, $no_rekam_medis,$no_bpjs, $nama_pasien, $pasien_alamat){
        $this->db->from('v_pasien_rad');
        $this->db->where('tgl_masukpenunjang BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        
        if(!empty($status)){
            $this->db->like('statusperiksa', $status);
        }
        if(!empty($no_masukpenunjang)){
            $this->db->like('no_masukpenunjang', $no_masukpenunjang);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien); 
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_radiologi_all($tgl_awal, $tgl_akhir, $status, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $nama_pasien, $pasien_alamat){
        $this->db->from('v_pasien_rad');
        $this->db->where('tgl_masukpenunjang BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        
        if(!empty($status)){
            $this->db->like('statusperiksa', $status);
        }
        if(!empty($no_masukpenunjang)){
            $this->db->like('no_masukpenunjang', $no_masukpenunjang);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }

        return $this->db->count_all_results(); 
    }

    public function get_pendaftaran_pasien($pasienmasukpenungjang_id){
        $query = $this->db->get_where('v_pasien_rad', array('pasienmasukpenunjang_id' => $pasienmasukpenungjang_id), 1, 0);

        return $query->row();
    }

    public function get_dokter_list(){
        $this->db->from('m_dokter'); 
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_dokter_rad(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('poliruangan_id', POLIRUANGAN_RADIOLOGI); //radiologi
        $this->db->group_by('id_M_DOKTER');
        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_dokter_anastesi(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }


    public function get_pendaftran_resep_pasien($pendaftaran_id){
        $this->db->select('t_pendaftaran.*,  m_pasien.*,  m_instalasi.*,  m_dokter.*');
        $this->db->from('t_pendaftaran');
        $this->db->join('m_pasien',' t_pendaftaran.pasien_id = m_pasien.pasien_id');
        $this->db->join('m_instalasi','m_instalasi.instalasi_id = t_pendaftaran.instalasi_id');
         $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_pendaftaran.dokter_id');
        $this->db->where('t_pendaftaran.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan_hasil($pendaftaran_id){
        $this->db->select('t_tindakanradiologi.*,  m_tindakan.*');
        $this->db->from('t_tindakanradiologi');
        $this->db->join('m_tindakan',' t_tindakanradiologi.daftartindakan_id = m_tindakan.daftartindakan_id');
        $this->db->where('t_tindakanradiologi.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_perawat(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_PERAWAT); //Perawat
        $query = $this->db->get();

        return $query->result(); 
    }

    public function update_to_sudahperiksa($id, $id_penunjang){
        $data = array(
            'status_periksa' => 'SUDAH PERIKSA'
        );
        $update1 = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));
        $data2 = array( 
            'statusperiksa' => 'SUDAH PERIKSA'
        );
        $update_penunjang = $this->db->update('t_pasienmasukpenunjang', $data2, array('pasienmasukpenunjang_id' => $id_penunjang));
        
        return array($update1,$update_penunjang);
    }   

    public function get_tindakan_pasien_list($pendaftaran_id){
        $this->db->select('t_tindakanradiologi.*, m_tindakan.daftartindakan_nama');
        $this->db->from('t_tindakanradiologi');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanradiologi.daftartindakan_id');

        $this->db->where('t_tindakanradiologi.pendaftaran_id',$pendaftaran_id);  
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

       
    //    $test =  $query->result();

    //     echo "<pre>";
    //     print_r($test);
    //     die();
    //     echo "</pre>";

        // echo "<pre>";
        // print_r ($radiologi);die();
        // echo "</pre>";
        return $query->result();
    }

    function get_tindakan_by_id($id){
            $this->db->select('*');
            $this->db->from('m_tindakan');
            $this->db->where('daftartindakan_id =', $id);
            $query = $this->db->get();
            return $query->result();
    }

     function get_user_by_id($iduser){
            $this->db->select('*');
            $this->db->from('aauth_users');
            $this->db->where('id =', $iduser);
            $query = $this->db->get();
            return $query->result();
    }
     public function count_list_tindakan(){
        $this->db->from('m_tindakan');
        return $this->db->count_all_results();
    }
      public function count_list_user(){
        $this->db->from('aauth_users');
        return $this->db->count_all_results();
    }


     function count_list_filtered_tindakan(){
          $this->db->from('m_tindakan');
          $i = 0;
          $search_value = $this->input->get('search');
          if($search_value){
              foreach ($this->columnListDokter as $item){
                  ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                  $i++;
              }
          }
          $order_column = $this->input->get('order');
          if($order_column !== false){
              $this->db->order_by($this->columnListDokter[$order_column['0']['column']], $order_column['0']['dir']);
          }
          else if(isset($this->orderListDokter)){
              $orderListDokter = $this->orderListDokter;
              $this->db->order_by(key($orderListDokter), $orderListDokter[key($orderListDokter)]);
          }
          $query = $this->db->get();
          return $query->num_rows();
    }



     function count_list_filtered_user(){
          $this->db->from('aauth_users');
          $i = 0;
          $search_value = $this->input->get('search');
          if($search_value){
              foreach ($this->columnListUser as $item){
                  ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                  $i++;
              }
          }
          $order_column = $this->input->get('order');
          if($order_column !== false){
              $this->db->order_by($this->columnListUser[$order_column['0']['column']], $order_column['0']['dir']);
          }
          else if(isset($this->orderListUser)){
              $orderListUser = $this->orderListUser;
              $this->db->order_by(key($orderListUser), $orderListUser[key($orderListUser)]);
          }
          $query = $this->db->get();
          return $query->num_rows();
    }

    function get_list_tindakan(){
    // $this->db->select('*');
        //$this->db->from('v_stokobatalkes');
        // $this->db->where('qtystok !=', 0);
        //SELECT name_dokter, kelompokdokter_nama FROM `m_dokter` JOIN m_kelompokdokter ON m_dokter.kelompokdokter_id = m_kelompokdokter.kelompokdokter_id
        $this->db->select('*');
        $this->db->from('m_tindakan');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->columnListDokter as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnListDokter[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->orderListDokter)){
            $orderListDokter = $this->orderListDokter;
            $this->db->order_by(key($orderListDokter), $orderListDokter[key($orderListDokter)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
  }

     function get_list_user(){
    // $this->db->select('*');
        //$this->db->from('v_stokobatalkes');
        // $this->db->where('qtystok !=', 0);
        //SELECT name_dokter, kelompokdokter_nama FROM `m_dokter` JOIN m_kelompokdokter ON m_dokter.kelompokdokter_id = m_kelompokdokter.kelompokdokter_id
        $this->db->select('*');
        $this->db->from('aauth_users');

        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->columnListUser as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->columnListUser[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->orderListUser)){
            $orderListUser = $this->orderListUser;
            $this->db->order_by(key($orderListUser), $orderListUser[key($orderListDokter)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
  }


    public function get_tindakan_pasien($pasien_id){
         //$this->db->select('*');
    $this->db->select('t_tindakanpasien.*, m_tindakan.*, t_tindakan_pasien_bhp_detail.jumlah as jumlahbhp,t_tindakan_pasien_obat_detail.jumlah as jumlahobat,m_dokter.*');
    $this->db->from('t_tindakanpasien');
    $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
    $this->db->join('t_tindakan_pasien_bhp_detail','t_tindakan_pasien_bhp_detail.tindakan_pasien_id = t_tindakanpasien.tindakanpasien_id');
    $this->db->join('t_tindakan_pasien_obat_detail','t_tindakan_pasien_obat_detail.tindakan_pasien_id = t_tindakanpasien.tindakanpasien_id');
    $this->db->join('m_dokter','m_dokter.id_M_dokter = t_tindakanpasien.dokter_id');
    $this->db->where('t_tindakanpasien.pasien_id',$pasien_id);  
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan($kelaspelayanan_id, $is_bpjs){
        $this->db->select('m_tindakan.*');
        $this->db->from('m_tindakan');
        // $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('m_tindakan.kelaspelayanan_id',$kelaspelayanan_id);
        $this->db->where('m_tindakan.kelompoktindakan_id','6'); //tindakan radiologi
        // $this->db->where('m_tindakan.komponentarif_id','1');
        if(strtoupper($is_bpjs) == strtoupper("BPJS")){
            $this->db->where('m_tindakan.bpjs_non_bpjs','BPJS');
        }else{
            $this->db->where('m_tindakan.bpjs_non_bpjs','Non BPJS');
        }
        $query = $this->db->get();

        return $query->result();
    }

    
    public function get_dokter_umum(){
        $this->db->from('m_dokter');
        //$this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tarif_tindakan($daftartindakan_id){
        $query = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $daftartindakan_id), 1, 0);

        return $query->row();
    }

    public function insert_tindakanpasien($pendaftaran_id, $penunjang_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id, $penunjang_id);
        $insert = $this->db->insert('t_tindakanradiologi',$data);

        return $insert;
    }
    public function delete_periksa($pasienmasukpenunjang_id,$pendaftaran_id){
        $datas = array(
            'statusperiksa' => 'Batal Periksa'
        );
        $update1 = $this->db->update('t_pasienmasukpenunjang', $datas, array('pasienmasukpenunjang_id' => $pasienmasukpenunjang_id));

        $data = array(
              'status_periksa' => 'Sudah Periksa'
          );
        $update2 = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $pendaftaran_id));

        return $update1 && $update2;
    }

    public function delete_tindakan($id){
        $delete = $this->db->delete('t_tindakanradiologi', array('tindakanradiologi_id' => $id));

        return $delete;
    }
    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_obat');
        $this->db->like('nama_obat', $val);
        $this->db->or_like('jenis_obat', $val);
        $query = $this->db->get();

        return $query->result();
    }
}
