<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium_model extends CI_Model {
    public function __construct(){
        parent::__construct();
    }

    public function get_laboratorium_list($tgl_awal, $tgl_akhir,$no_masukpenunjang, $no_rekam_medis, $no_bpjs, $nama_pasien, $pasien_alamat, $status){
        $this->db->from('v_pasien_lab');
        $this->db->where('tgl_masukpenunjang BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
         
        if(!empty($no_masukpenunjang)){
            $this->db->like('no_masukpenunjang', $no_masukpenunjang);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($status)){
            $this->db->like('statusperiksa', $status);
        }
       
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function count_laboratorium_all($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $nama_pasien, $pasien_alamat, $status){
        $this->db->from('v_pasien_lab');
        $this->db->where('tgl_masukpenunjang BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        
        if(!empty($no_masukpenunjang)){
            $this->db->like('no_masukpenunjang', $no_masukpenunjang);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($nama_pasien)){
            $this->db->like('pasien_nama', $nama_pasien);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($status)){
            $this->db->like('statusperiksa', $status);
        }

        return $this->db->count_all_results();
    }

    public function get_pendaftaran_pasien($pasienmasukpenunjang_id){
        $query = $this->db->get_where('v_pasien_lab', array('pasienmasukpenunjang_id' => $pasienmasukpenunjang_id), 1, 0);

        return $query->row();
    }

    public function get_pendaftaran_pasien_2($pasien_id){
        $query = $this->db->get_where('v_pasien_lab', array('pasien_id' => $pasien_id), 1, 0);

        return $query->row();
    }

    public function get_dokter_list(){
        $this->db->from('m_dokter'); 
        $query = $this->db->get();

        return $query->result();
    }
    

    public function get_data_hasil_lab($pasien_id, $id){
        $sql = 'SELECT *
FROM t_tindakan_lab, t_det_tindakan_lab, m_lab
WHERE t_det_tindakan_lab.`jenis_tindakan` = "'.$id.'" AND t_det_tindakan_lab.lab_id = m_lab.lab_id AND t_det_tindakan_lab.`tindakan_lab_id` = t_tindakan_lab.`tindakan_lab_id` AND t_tindakan_lab.pasien_id = "'.$pasien_id.'"';
        $query = $this->db->query($sql);
        return $query->result();

    }

    public function get_data_hasil_lab_2($pasien_id, $id){
        $sql = 'SELECT *
FROM t_tindakan_lab, t_det_tindakan_lab, m_lab
WHERE t_det_tindakan_lab.`jenis_tindakan` = "'.$id.'" AND t_det_tindakan_lab.lab_id = m_lab.lab_id AND t_det_tindakan_lab.`tindakan_lab_id` = t_tindakan_lab.`tindakan_lab_id` AND t_tindakan_lab.pasien_id = "'.$pasien_id.'"';
        $query = $this->db->query($sql);
        return $query->row(); 

    }

    public function get_data_pasien_lab($pendaftaran_id, $type_pembayaran){
        $sql = 'SELECT t_tindakan_lab.*, t_det_tindakan_lab.*, m_lab.`satuan` 
                FROM m_lab, t_tindakan_lab, t_det_tindakan_lab 
                WHERE t_det_tindakan_lab.`lab_id` = m_lab.`lab_id` 
                AND t_det_tindakan_lab.`tindakan_lab_id` = t_tindakan_lab.`tindakan_lab_id` 
                AND t_tindakan_lab.`pendaftaran_id` = "'.$pendaftaran_id.'" AND t_det_tindakan_lab.type_pembayaran = "'.$type_pembayaran.'"';
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function get_data_pasien_lab_all($pendaftaran_id){
        $sql = 'SELECT t_tindakan_lab.*, t_det_tindakan_lab.*, m_lab.`satuan` 
                FROM m_lab, t_tindakan_lab, t_det_tindakan_lab 
                WHERE t_det_tindakan_lab.`lab_id` = m_lab.`lab_id` 
                AND t_det_tindakan_lab.`tindakan_lab_id` = t_tindakan_lab.`tindakan_lab_id` 
                AND t_tindakan_lab.`pendaftaran_id` = "'.$pendaftaran_id.'"';
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function get_dokter_lab(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('poliruangan_id', POLIRUANGAN_LABORATORIUM); //laboratorium
        $this->db->group_by('id_M_DOKTER'); 
        $query = $this->db->get();

        // return $query->result();
        return $query->row();
    }
    
    public function get_dokter_anastesi(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_DOKTER_ANASTESI); //dokter anastesi
        $query = $this->db->get();

        return $query->result(); 
    }
    
    public function get_perawat(){
        $this->db->from('m_dokter');
        $this->db->where('kelompokdokter_id', KELOMPOK_PERAWAT); //Perawat
        $query = $this->db->get();

        return $query->result();
    }

    public function update_to_sudahperiksa($id, $pasienmasukpenunjang_id){
        $data = array(
            'status_periksa' => 'Sudah Periksa'
        );
        $update1 = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));
        $datas = array(
            'statusperiksa' => 'Sudah Periksa'
        );
        $update2 = $this->db->update('t_pasienmasukpenunjang', $datas, array('pasienmasukpenunjang_id' => $pasienmasukpenunjang_id));
        return $update1 && $update2;
    }

    public function get_tindakan_pasien_list($pendaftaran_id){
        $this->db->select('t_tindakanlab.*, m_daftartindakan.daftartindakan_nama');
        $this->db->from('t_tindakanlab');
        $this->db->join('m_tariftindakan','m_tariftindakan.tariftindakan_id = t_tindakanlab.tariftindakan_id');
        $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('t_tindakanlab.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tindakan($kelaspelayanan_id, $is_bpjs){
        $this->db->select('m_tariftindakan.*, m_daftartindakan.daftartindakan_nama');
        $this->db->from('m_tariftindakan');
        $this->db->join('m_daftartindakan','m_daftartindakan.daftartindakan_id = m_tariftindakan.daftartindakan_id');
        $this->db->where('m_tariftindakan.kelaspelayanan_id',$kelaspelayanan_id);
        $this->db->where('m_daftartindakan.kelompoktindakan_id','7'); //tindakan laboratorium
        // $this->db->where('m_tariftindakan.komponentarif_id','1');
        if(strtoupper($is_bpjs) == "BPJS"){
            $this->db->where('m_tariftindakan.bpjs_non_bpjs','BPJS');
        }else{
            $this->db->where('m_tariftindakan.bpjs_non_bpjs','Non BPJS');
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tarif_tindakan($tariftindakan_id){
        $query = $this->db->get_where('m_tariftindakan', array('tariftindakan_id' => $tariftindakan_id), 1, 0);

       
        return $query->row();
    }

    public function get_range_lab($lab_id){
        $query = $this->db->get_where('m_lab', array('lab_id' => $lab_id), 1, 0);

       
        return $query->row();
    }

    public function insert_tindakanpasien($pendaftaran_id,$pasienmasukpenunjang_id, $data=array()){
        $this->update_to_sudahperiksa($pendaftaran_id, $pasienmasukpenunjang_id);
        $insert = $this->db->insert('t_tindakan_lab',$data);
        $id     = $this->db->insert_id();
        return $id;

        return $id;
    }
    public function delete_periksa($pasienmasukpenunjang_id, $pendaftaran_id){
        $datas = array(
            'statusperiksa' => 'Batal Periksa'
        );
        $update1 = $this->db->update('t_pasienmasukpenunjang', $datas, array('pasienmasukpenunjang_id' => $pasienmasukpenunjang_id));

        $data = array(
              'status_periksa' => 'Sudah Periksa'
          );
        $update2 = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $pendaftaran_id));
      
        return $update1 && $update2;
    }

    public function delete_hasil_tindakan($tindakan_lab_id, $lab_id){
        $array = array('tindakan_lab_id' => $tindakan_lab_id, 'lab_id' => $lab_id);
        $delete = $this->db->delete('t_det_tindakan_lab', $array);
        return $delete;
    }

    public function delete_tindakan($tindakan_lab_id, $lab_id){
        // $this->db->where('tindakan_lab_id', $tindakan_lab_id);
        // $this->db->where('lab_id', $lab_id);
        // $this->db->delete('t_det_tindakan_lab');

        // $query = $this->db->get();
        // return $query->row();
        // $delete = $this->db->delete('t_det_tindakan_lab', array('tindakan_lab_id' => $tindakan_lab_id, 'lab_id' => $lab_id));
        $array = array('tindakan_lab_id' => $tindakan_lab_id, 'lab_id' => $lab_id);
        $delete = $this->db->delete('t_det_tindakan_lab', $array);
        return $delete;
    }

    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('m_obat');
        $this->db->like('nama_obat', $val);
        $this->db->or_like('jenis_obat', $val);
        $query = $this->db->get();

        return $query->result();
    }

    public function getCaraPembayaran($id){
        $query = $this->db->get_where('t_pembayaran', array('pembayaran_id' => $id), 1, 0);
        return $query->row(); 
    }
}
