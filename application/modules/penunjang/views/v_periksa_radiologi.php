<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box"  style="height:440px; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">     
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
         
    <div class="row">    
        <div class="col-md-12">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">   
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr> 
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_masukpenunjang; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Poliklinik</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->poli_ruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pengirim</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->dokterpengirim_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Nama Asuransi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_asuransi; ?></td>
                                    </tr> 
                                    <tr>
                                        <td><b>No Asuransi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->no_asuransi; ?></td>
                                    </tr>   
                                    <tr>
                                        <td><b>Instansi Pekerjaan</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->instansi_pekerjaan; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Alamat</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_alamat_domisili; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kasus Polisi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->kasus_polisi; ?></td>
                                    </tr> 
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                </table>
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->dokterpengirim_nama; ?>">  
                                <input id="pendaftaran_id" type="hidden" name="pendaftaran_id" class="validate" value="<?php echo $list_pasien->pendaftaran_id; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->poli_ruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">

                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  
    </div>

   <div class="col-md-12" >
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Tindakan</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-hasil-tindakan" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs" onclick="reloadTablePasien()"> Hasil Tindakan</span></a></li> 
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">  
                <div role="tabpanel" class="tab-pane active" id="tabs-tindakan"> 

                    <table id="table_tindakan_radiologi_pasien" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>jml Jenis Obat</th>
                                <th>jml Jenis Alkes</th>
                                <th>Dokter</th>
                                <th>Batal/Hapus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>jml Jenis Obat</th>
                                <th>jml Jenis Alkes</th>
                                <th>Dokter</th>
                                <th>Batal/Hapus</th>
                            </tr>
                        </tfoot>
                    </table><br>

                   
                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <input type="hidden" id="pasienmasukpenunjang_id" name="penunjang_id" value="<?php echo $list_pasien->pasienmasukpenunjang_id; ?>">


                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_tindakan" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message_tindakan"></p>
                        </div>
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="id_pasien" id="id_pasien" value="<?php // echo $list_pasien->pasien_id; ?>">
                    <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php //echo $list_pasien->kelaspelayanan_id; ?>">
                    <input type="hidden" id="dokter_id_tindakan" name="dokter_id_tindakan" value="<?php // echo $list_pasien->id_M_DOKTER; ?>">

                    <?php
                        //if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                        //else{
                      ?>
                      <div class="col-md-12">
                        <div class="form-group col-md-4">
                           <label class="control-label">Kode Tindakan<span style="color: red;"> *</span></label>
                              <div class="input-group">
                                  <input type="hidden" name="tindakan_id1" id="tindakan_id1" value="">
                                  <input type="text" class="form-control" id="kode_tindakan1" name="kode_tindakan1" placeholder="Pilih Tindakan">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" title="Lihat List Tindakan" data-toggle="modal" data-target="#modal_tindakan" onclick="tes()"><i class="fa fa-list"></i></button>
                                  </span>
                              </div>
                            <!-- <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly> -->
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Nama Tindakan<span style="color: red;"> *</span></label>
                            <input id="nama_tindakan2" name="nama_tindakan2" class="form-control" placeholder="Nama Tindakan" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Jumlah Tindakan<span style="color: red;"> *</span></label>
                            <input id="jml_tindakan" name="jml_tindakan" class="form-control" min="1" value="" type="number" onkeyup="hitungHarga()" min="1" placeholder="Jumlah Tindakan" required>
                            <!-- <input type="hidden" id="harga_tindakan_satuan" name="harga_tindakan_satuan" value="0" readonly>
                            <input type="hidden" id="subtotal" name="subtotal" value="0" readonly>
                            <input type="hidden" id="totalharga" name="totalharga" value="0" readonly>
                            <input type="hidden" id="is_cyto" name="is_cyto" value="0" readonly> -->
                        </div>
                      </div>
                      <div class="col-md-12">
                          <div class="form-group col-md-4" style="margin-top:-25px;">
                              <label>&nbsp;</label>
                              <button style="width: 100%" type="button" class="btn btn-success" id="mapICD9keTindakan">
                                  <!-- <i class="fa fa-floppy-o"></i> -->
                                  Map ICD 9 Ke Tindakan
                              </button>
                          </div>
                      </div>
              <div class="col-md-12">
                        <div class="form-group col-md-4">
                            <label class="control-label">Kode Tindakan ICD 9<span style="color: red;"> *</span></label>
                            <input id="kode_tindakan_icd9" name="kode_tindakan_icd9" class="form-control" placeholder="Kode Tindakan ICD 9" required>
                            <!-- <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly> -->
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Nama Tindakan ICD 9<span style="color: red;"> *</span></label>
                            <input id="nama_tindakan_icd9" name="nama_tindakan_icd9" class="form-control" placeholder="Nama Tindakan ICD 9" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label">Jumlah Tindakan ICD 9<span style="color: red;"> *</span></label>
                            <input id="jml_tindakan_icd9" name="jml_tindakan_icd9" class="form-control" min="1" value="" type="number" onkeyup="hitungHarga()" min="1" placeholder="Jumlah Tindakan ICD 9" required>
                            <!-- <input type="hidden" id="harga_tindakan_satuan" name="harga_tindakan_satuan" value="0" readonly>
                            <input type="hidden" id="subtotal" name="subtotal" value="0" readonly>
                            <input type="hidden" id="totalharga" name="totalharga" value="0" readonly>
                            <input type="hidden" id="is_cyto" name="is_cyto" value="0" readonly> -->
                        </div>
                      

                         <div class="form-group col-md-3">
                           <input type="radio" name="time" id="ya" >   Cyto
                           <input type="radio" name="time" id="non" >   Non Cyto
                        </div>

                      </div>
                      <style>
                      .tbody-input{
                        margin: 0px;
                        padding: 3px 4px;
                        text-align: center;
                        width: 100%;
                      }

                      .tbody-input-left{
                        margin: 0px;
                        padding: 3px 4px;
                        text-align: left;
                        width: 100%;
                      }
                      </style>
                      <table id="table_tindakan_obat" class="table table-striped dataTable" cellspacing="0">
                          <thead>
                              <tr>
                                  <th width="70%">Obat</th>
                                  <th width="10%">Jumlah</th>
                                  <th width="10%">Satuan</th>
                                  <th width="10%">Hapus</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <td><input class="tbody-input-left" type="text" value=""></td>
                                <td><input class="tbody-input" type="number" value=""></td>
                                <td><input class="tbody-input" type="text" value=""></td>
                                <td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus obat" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td>
                              </tr>
                          </tbody>
                      </table>
                      <div class="form-group col-md-1" style="margin-top: -20px;padding: 0px; float:right;">
                          <button type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" id="addTindakanObat" onclick="addobat()"><i class="fa fa-plus"></i> OBAT</button>
                      </div>
                      <br>
                        <table id="table_tindakan_bhp" class="table table-striped dataTable" cellspacing="0">
                          <thead>
                              <tr>
                                  <th width="70%">BHP</th>
                                  <th width="10%">Jumlah</th>
                                  <th width="10%">Satuan</th>
                                  <th width="10%">Hapus</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><input class="tbody-input-left" type="text" value=""></td>
                                  <td><input class="tbody-input" type="number" value=""></td>
                                  <td><input class="tbody-input"type="text" value=""></td>
                                  <td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus BHP" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td>
                              </tr>
                          </tbody>
                      </table>
                      <div class="form-group col-md-1" style="margin-top: -20px;padding: 0px; float:right;">
                          <button type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" id="addTindakanBHP" onclick="addbhp()"><i class="fa fa-plus"></i> BHP</button>
                      </div>
                      <br>
                      <div class="row">
                          <div class="form-group col-md-6">
                              <label class="control-label">Dokter <span style="color: red;"> *</span></label>
                              <select id="dokter_id" name="dokter_id" class="form-control select2"> 
                                <option value=""  selected>Pilih Dokter</option>
                                <?php
                                $list_umum = $this->Radiologi_model->get_dokter_umum();
                                foreach($list_umum as $list){
                                    echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                }
                                ?>
                            </select>
                          </div>
                              
                        
                        <div class="form-group col-md-9">
                             <label class="control-label">Hasil Pemeriksaan<span style="color: red;"> *</span></label>
                            <textarea class="form-control" rows="5" id="hasil"></textarea>
                        </div> 

                          <div class="form-group col-md-12">
                             <label class="control-label">Detail Kesimpulan<span style="color: red;"> *</span></label>
                            <textarea class="form-control" rows="5" id="hasil"></textarea>
                        </div>

                         <div class="form-group col-md-12">
                             <label class="control-label">Upload Gambar<span style="color: red;"> *</span></label>
                            <button rows="5" type="button">Upload</button>
                        </div>

                        <div class="form-group col-md-6">
                           <label class="control-label">User<span style="color: red;"> *</span></label>
                              <div class="input-group">
                                  <input type="hidden" name="user1" id="user1" value="">
                                  <input type="text" class="form-control" id="username1" name="username1" placeholder="Pilih User">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" title="Lihat List User" data-toggle="modal" data-target="#modal_user" onclick="cariuser()"><i class="fa fa-list"></i></button>
                                  </span>
                              </div>
                            <!-- <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                            <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly> -->
                        </div>




                          
                      </div>


                        <div class="form-group col-md-3 float-right">
                            <button style="width: 100%" type="button" class="btn btn-success" id="saveResep"><i class="fa fa-plus"></i> TAMBAH</button>
                        </div>
                    <?php// } ?>


                    <?php echo form_close();?>
                    <div class="clearfix"></div>




                                
<!-- 
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
   -->
                <div class="col-md-6 float-left">
                <button style="width: 100%" type="button" class="btn btn-success" id="printResep"><i class="fa fa-floppy-o"></i> SIMPAN HASIL PEMERIKSAAN</button>
                </div>

                <div class="col-md-6 float-right">
                    <button style="width: 100%" type="button" class="btn btn-primary" id="printreseptur" onclick="printHasil()"><i class="fa fa-print"></i> PRINT</button>
                    </div>

                    <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div>

                <div role="tabpanel" class="tab-pane" id="tabs-hasil-tindakan"> 

                    <table id="table_hasil_tindakan_radiologi" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <!-- <th>Cyto</th> -->
                                <th>Aksi</th>
                            </tr> 
                        </thead> 
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot> 
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot> 
                    </table><br>        
                    
                    <div class="clearfix"></div>
                </div>

               <div id="modal_hasil_tindakan" class="modal fade bs-example-modal-lg" onclick="reloadTablePasien()" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="margin-top: -25px;">
                    <div class="modal-dialog modal-lg">          
                        <div class="modal-content" >     
                            <div class="modal-header" style="background: #fafafa;"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h2 class="modal-title" id="myLargeModalLabel"><b>Periksa Hasil Radiologi</b></h2>     
                             </div>  
                            <div class="modal-body" style="background: #fafafa;height: 350px;overflow: auto;">        
                               <textarea id="mymce" name="area"></textarea>  
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            </div>  
        </div>

</div>

<div id="modal_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: 365px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Tindakan</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="table-responsive">
                   <table id="table_list_tindakan" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nama Tindakan</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="3">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>Nama Tindakan</th>
                              <th>Pilih</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_user" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="overflow: auto;height: 365px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List User</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="table-responsive">
                   <table id="table_list_user" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td colspan="3">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                              <th>Username</th>
                              <th>Pilih</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- end whitebox --> 


<?php $this->load->view('footer_iframe');?>        
<script src="<?php echo base_url()?>assets/dist/js/pages/penunjang/radiologi/periksa_radiologi.js"></script>
</body>
 
</html> 



