<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<style>
    #utama{
        width:100%;
        border-collapse : collapse;
        border-style: solid;
    }

    #hidden{
        border-style :hidden;
    }

    #space{
        border-left : hidden;
        border-right : hidden;
        
    }

    #center{
        text-align:center;
    }

    #periksa{
        text-align:center;
        border-style :hidden;        
    }
</style>
<body onload="window.print()">
    <table border="1px" id="utama">
        <tr id="periksa">
            <td colspan="5"><img src="<?php echo base_url()?>/assets/plugins/images/header.png"></td>
        </tr>
        <tr id="periksa">
            <td colspan="5">
                <?php foreach ($list_hasil_lab as $list) {
                    if($list->jenis_tindakan == 1){
                        $indeks = "HEMATOLOGI";
                    }else if($list->jenis_tindakan == 2){
                        $indeks = "URINE";
                    }else if($list->jenis_tindakan == 3){
                        $indeks = "KIMIA KLINIK";
                    }else if($list->jenis_tindakan == 4){
                        $indeks = "IMUNO SERULOGI";
                    }else if($list->jenis_tindakan == 5){
                        $indeks = "LAIN LAIN";
                    }else{
                        $indeks = "TIDAK VALID";
                    }
                }?>
                HASIL PEMERIKSAAN LABORATORIUM <?php echo $indeks ?>                 
            </td>
        </tr>
        <tr id="space">
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr id="hidden">
            <td colspan="5">
                <table>
                    <tr>
                        <td>Nama / No. RM</td>
                        <td>:</td>
                        <td><?php echo $list_pasien->pasien_nama ." / ". $list_pasien->no_rekam_medis?></td>
                    </tr>
                    <tr>
                        <td>Tgl. Lahir / Umur</td>
                        <td>:</td>
                        <td>
                            <?php 
                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                $umur  = new Datetime($tgl_lahir);
                                $today = new Datetime();
                                $diff  = $today->diff($umur);
                                echo date('d/m/Y', strtotime($list_pasien->tanggal_lahir)) ." / ". $diff->y." Th ".$diff->m." Bln ".$diff->d." Hr" ?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td valign="top">:</td>
                        <td valign="top"><?php echo $list_pasien->pasien_alamat_domisili ?></td>
                    </tr>
                    <tr>
                        <td>Dokter Pengirim</td>
                        <td>:</td>
                        <td><?php echo $list_pasien->dokterpengirim_nama ?></td>
                    </tr>
                    <tr>
                        <td>Tgl / Jam Periksa</td>
                        <td>:</td>
                        <td><?php echo date('d M Y', strtotime($hasil_lab->tanggal_periksa)) ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;". date('H:i', strtotime($hasil_lab->tanggal_periksa))?></td>
                    </tr>
                    <tr>
                        <td>Penanggung Jawab</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="space">
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <th>No</th>
            <th>Nama Pemeriksaan</th>
            <th>Hasil Pemeriksaan</th>
            <th>Nilai Rujukan</th>
            <th>Metode</th>
        </tr>
            <?php
                $i = 0;
                foreach ($list_hasil_lab as $list) { 
                $i++; ?>
        <tr id="center">
            <td><?php echo $i ?></td>
            <td><?php echo $list->nama_pemeriksaan ?></td>
            <td><?php echo $list->hasil ." ". $list->satuan?></td>
            <td><?php echo $list->tampil_laki ." ". $list->satuan?></td>
            <td><?php echo $list->metode?></td>
        </tr>
                <?php  } ?>
    </table>
</body>
</html>