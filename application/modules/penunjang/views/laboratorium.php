<?php $this->load->view('header');?>
<title>SIMRS | Pasien Laboratorium</title>
<?php $this->load->view('sidebar');?>

      <!-- START CONTENT -->
<section id="content">
    <!--breadcrumbs start-->
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey hide-on-large-only">
            <i class="mdi-action-search active"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
        </div>
      <div class="container">
        <div class="row">
          <div class="col s12 m12 l12">
            <h5 class="breadcrumbs-title">Pasien Laboratorium</h5>
            <ol class="breadcrumbs">
                <li><a href="#">Laboratorium</a></li>
                <li class="active">Pasien Laboratorium</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->


    <!--start container-->
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header">List Pasien Laboratorium</h4>
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div id="card-alert" class="card green notif" style="display:none;">
                            <div class="card-content white-text">
                                <p id="card_message">SUCCESS : The page has been added.</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col s4">
                                    <i class="mdi-action-event prefix"></i>
                                    <input type="text" class="datepicker" name="tgl_awal" id="tgl_awal" placeholder="Tanggal Awal" value="<?php echo date('d F Y'); ?>">
                                    <label for="tgl_awal">Tanggal Kunjungan, Dari</label>
                                </div>
                                <div class="input-field col s4">
                                    <i class="mdi-action-event prefix"></i>
                                    <input type="text" class="datepicker" name="tgl_akhir" id="tgl_akhir" placeholder="Tanggal Akhir" value="<?php echo date('d F Y'); ?>">
                                    <label for="tgl_akhir">Sampai</label>
                                </div>
                                <div class="input-field col s4">
                                    <select id="status" name="status" class="validate">
                                        <option value="" disabled selected>Pilih Status</option>
                                        <option value="ANTRIAN">ANTRIAN</option>
                                        <option value="SEDANG PERIKSA">SEDANG PERIKSA</option>
                                        <option value="SUDAH PERIKSA">SUDAH PERIKSA</option>
                                        <option value="SUDAH PULANG">SUDAH PULANG</option>
                                        <option value="BATAL PERIKSA">BATAL PERIKSA</option>
                                    </select>
                                    <label for="status">Status Periksa</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col s4">
                                    <input type="text" name="nama_pasien" id="nama_pasien" placeholder="Ketik Nama Pasien" value="">
                                    <label for="nama_pasien">Nama Pasien</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s12">
                            <div class="row">
                                <div class="input-field col s12">
                                    <button type="button" class="btn light-green waves-effect waves-light darken-4" title="Cari Pasien" id="searchPasien">Search</button>
                                </div>
                            </div>
                        </div>
                        <div class="col s12">
                            &nbsp;
                        </div>
                        <div id="table-datatables"> 
                            <div class="row">
                                <div class="col s12 m4 l12">
                                      <table id="table_list_laboratorium" class="responsive-table display" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>No. Pendaftaran</th>
                                                <th>Tgl. Pendaftaran</th>
                                                <th>No. Rekam Medis</th>
                                                <th>Nama Pasien</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur</th>
                                                <th>Alamat</th>
                                                <th>Pembayaran</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Status Pasien</th>
                                                <th>Periksa Pasien</th>
                                                <th>Batal Periksa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="13">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<!-- Start Modal Add Paket -->
<div id="modal_periksa_laboratorium" class="modal" style="width: 80% !important ; max-height: 90% !important ;">
    <!--<div class="modal-dialog" role="document">-->
    <div class="modal-content">
        <h1>Pemeriksaan Pasien</h1>
        <div class="divider"></div>
        <div id="card-alert" class="card green modal_notif" style="display:none;">
            <div class="card-content white-text">
                <p id="modal_card_message"></p>
            </div>
            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="col s12 formValidate">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
            </div>
        </div>
    </div>
    <!--</div>-->
    <div class="modal-footer">
        <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close" onclick="reloadTablePasien()">Close<i class="mdi-navigation-close left"></i></button>
    </div>
</div>
    <!-- End Modal Add Paket -->
<?php $this->load->view('footer');?>
