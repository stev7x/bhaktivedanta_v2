<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" id="scroll" style="height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->
    
    <div class="row">    
        <div class="col-md-12">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">   
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr> 
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_masukpenunjang; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Poliklinik</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->poli_ruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pengirim</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->dokterpengirim_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Nama Asuransi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_asuransi; ?></td>
                                    </tr> 
                                    <tr>
                                        <td><b>No Asuransi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->no_asuransi; ?></td>
                                    </tr>   
                                    <tr>
                                        <td><b>Instansi Pekerjaan</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->instansi_pekerjaan; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                     <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                 $tgl_lahir = $list_pasien->tanggal_lahir;
                                                 $umur  = new Datetime($tgl_lahir);
                                                 $today = new Datetime();
                                                 $diff  = $today->diff($umur);
                                                 echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Alamat</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_alamat_domisili; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kasus Polisi</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->kasus_polisi; ?></td>
                                    </tr> 
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr>
                                  
                                </table>
                                <!-- <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">   -->
                                <!-- <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly> -->
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->poli_ruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input id="tgl_lahir" type="hidden" name="tgl_lahir" class="validate" value="<?php echo $list_pasien->tanggal_lahir; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">

                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  
    </div>

    

   <div class="col-md-12" >
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> TINDAKAN</span></a></li> 
                <li role="presentation" class="nav-item"><a href="#tabs-hasil-tindakan" class="nav-link" aria-controls="hasil_diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> HASIL TINDAKAN</span></a></li> 
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">  
                <div role="tabpanel" class="tab-pane active" id="tabs-tindakan"> 

                <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <div class="col-md-12"> 
                        <div class="row">
                        <div class="col-md-12">
                        <table id="table_hasil_tindakan_lab_all" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Periksa</th>
                                    <th>Nama Pemeriksaan</th>
                                    <th>Jenis Pemeriksaan</th>
                                    <th>Hasil</th>
                                    <th>Batal/Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="3">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Periksa</th>
                                    <th>Nama Pemeriksaan</th>
                                    <th>Jenis Pemeriksaan</th>
                                    <th>Hasil</th>
                                    <th>Batal/Hapus</th>
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                        
                        <div class="alert alert-success alert-dismissable col-md-12 modal_notif" id="modal_notif" style="display:none;">
                            <button type="button" class="close " data-dismiss="alert" >&times;</button> 
                            <div >
                                <p id="modal_card_message" class="card_message"></p>
                            </div>
                        </div>

                        <input type="hidden" id="pasienmasukpenunjang_id" name="pasienmasukpenunjang_id" value="<?php echo $list_pasien->pasienmasukpenunjang_id; ?>">
                        <h3 style="width:100%;border-bottom: 1px solid #eeeeee;margin-bottom: 30px;margin-left: 10px;margin-right: 10px;padding-left: 5px">Pilih Dokter</h3> 
                            <div class="form-group col-md-6">
                                <label for="type_pembayaran">Type Pembayaran</label>
                                <input type="hidden" name="pembayaran_id" id="pembayaran_id" value="<?= $list_pasien->pembayaran_id ?>">      
                                <select name="type_pembayaran" id="type_pembayaran" class="form-control" onchange="showCaraBayar('umum')">     
                                    <option disabled selected>PILIH PEMBAYARAN</option> 
                                    <option value="PRIBADI">PRIBADI</option>
                                    <option value="ASURANSI1">ASURANSI 1</option> 
                                    <option value="ASURANSI2">ASURANSI 2</option> 
                                </select>   
                            </div>
                            <div class="pembayaran1 col-md-8" style="display: none;" >
                                <div class="form-group">   
                                    <label id="lblNamaPembayaran_umum">Nama Asuransi</label> 
                                    <input type="text" name="nama_asuransi" id="nama_asuransi" class="form-control" placeholder="nama asuransi">
                                </div>
                                <div class="form-group">   
                                    <label id="lblNomorPembayaran_umum">Nomor Asuransi</label>  
                                    <input type="number" name="no_asuransi" id="no_asuransi" class="form-control" placeholder="nomor asuransi"> 
                                </div> 
                            </div>  
                            <div class="form-group col-md-6">
                                <label>Dokter Laboratorium<span style="color: red;"> *</span></label>
                                <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id;?>">
                                <input type="hidden" name="pasien_id" id="pasien_id"  value="<?php echo $list_pasien->pasien_id;?>">
                                <?php
                                 $dokter_lab_list = $this->Laboratorium_model->get_dokter_lab(); 
                                 // foreach($dokter_lab_list as $list){
                                 //     echo "<option value='".$list->id_M_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                 // }
                                ?>
                                <input type="text" name="dokter_lab" id="dokter_lab" class="form-control" placeholder="Dokter Lab" value="<?php echo $dokter_lab_list->NAME_DOKTER; ?>">
                                <!-- <select id="dokter_lab" name="dokter_lab" class="form-control select2">
                                    <option value=""  selected>Pilih Dokter Laboratorium</option>
                                </select> -->
                            </div>
                            <div class="form-group col-md-4">
                                <label>Pilih Menu Tindakan<span style="color: red;"> *</span></label>
                                <select id="menu_tindakan" name="menu_tindakan" class="form-control" onchange="pilihMenuTindakan()">
                                    <option value=""  selected>Pilih Menu Tindakan</option>
                                    <option value="1">Per Paket</option>
                                    <option value="2">Per Satuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4" style="display:none;" id="pilih_paket">
                                <label>Pilih Paket Tindakan<span style="color: red;"> *</span></label>
                                <select id="menu_paket" name="menu_paket" class="form-control" onchange="pilihPaket()">
                                    <option value=""  selected>Pilih Paket Tindakan</option>
                                    <option value="1">Hematologi</option>
                                    <option value="2">Urine</option>
                                    <option value="3">Kimia Klinik</option>
                                    <option value="4">Imuno Serulogi</option>
                                    <option value="5">Lain lain</option>
                                </select>
                            </div>

                        </div> 
                   </div> 
                   
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <div class="col-md-12"> 
                        <h3 style="width:100%;border-bottom: 1px solid #eeeeee;margin-bottom: 30px;margin-left: 10px;margin-right: 10px;padding-left: 5px">Input Tindakan</h3> 
                        <div class="col-md-6">
                            <div style="display:none;" id="menu_hematologi">
                                <div class="panel panel-info1">  
                                    <div class="panel-heading no-padding">HEMATOLOGI     
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a></div>
                                    </div>        
                                    <div class="panel-wrapper collapse" aria-expanded="false">  
                                        <div class="panel-body">
                                            <div class="row" style="margin-top: -21px; margin-left: -24px;">
                                                <div class="col-md-12">
                                                    <div class="checkbox checkbox-primary pull-left p-t-0">
                                                        <input id="cek_darah_lengkap" type="checkbox"> 
                                                        <label for="cek_darah_lengkap">Darah Lengkap </label> 
                                                    </div>
                                                </div>
                                            </div>                                       
                                            <div id="darah_lengkap" style="display: none; margin-left: 8px; margin-top: -16px;">
                                                <div class="row" style="margin-top: -10px;"> 
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_hb"></div>
                                                        <div id="tarif_hb"></div>
                                                        <div id="periksa_hb"></div>
                                                        <div id="id_hb"></div>
                                                        <input type="number" id="hb" name="hb" class="form-control" placeholder="Hb" onkeyup="cekRange('6','hb')" >
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_hitung_jenis"></div>
                                                        <div id="tarif_hitung_jenis"></div>
                                                        <div id="id_hitung_jenis"></div>
                                                        <input type="number" id="hitung_jenis" name="hitung_jenis" class="form-control" placeholder="Hitung Jenis" onkeyup="cekRange('8','hitung_jenis')">
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: -41px;">
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_leukosit"></div>
                                                        <div id="tarif_leukosit"></div>
                                                        <div id="periksa_leukosit"></div>
                                                        <div id="id_leukosit"></div>
                                                        <input type="number" id="leukosit" name="leukosit" class="form-control" placeholder="Leukosit" onkeyup="cekRange('8','leukosit')" >
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_eritrosit"></div>
                                                        <div id="tarif_eritrosit"></div>
                                                        <div id="id_eritrosit"></div>
                                                        <input type="number" name="eritrosit" id="eritrosit" class="form-control" placeholder="Eritrosit" onkeyup="cekRange('4','eritrosit')" >
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: -41px;">
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_trombosit"></div>
                                                        <div id="tarif_trombosit"></div>
                                                        <div id="periksa_trombosit"></div>
                                                        <div id="id_trombosit"></div>
                                                        <input type="number" id="trombosit" name="trombosit" class="form-control" placeholder="Trombosit" onkeyup="cekRange('9','trombosit')">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_mcv"></div>
                                                        <div id="tarif_mcv"></div>
                                                        <div id="periksa_mcv"></div>
                                                        <div id="id_mcv"></div>
                                                        <input type="number" name="mcv" id="mcv" class="form-control" placeholder="MCV" onkeyup="cekRange('18','mcv')">
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: -41px;">
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_hematrokrit"></div>
                                                        <div id="tarif_hematrokrit"></div>
                                                        <div id="periksa_hematrokrit"></div>
                                                        <div id="id_hematrokrit"></div>
                                                        <input type="number" name="hematokrit" id="hematokrit" class="form-control" placeholder="Hematokrit" onkeyup="cekRange('5','hematokrit')">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_mch"></div>
                                                        <div id="tarif_mch"></div>
                                                        <div id="periksa_mch"></div>
                                                        <div id="id_mch"></div>
                                                        <input type="number" name="mch" id="mch" class="form-control" placeholder="MCH" onkeyup="cekRange('16','mch')">
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: -41px;">

                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_led"></div>
                                                        <div id="tarif_led"></div>
                                                        <div id="periksa_led"></div>
                                                        <div id="id_led"></div>
                                                        <input type="number" name="led" id="led" class="form-control" placeholder="LED" onkeyup="cekRange('7','led')">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>&nbsp;</label>
                                                        <div id="notification_mchc"></div>
                                                        <div id="tarif_mchc"></div>
                                                        <div id="periksa_mchc"></div>
                                                        <div id="id_mchc"></div>
                                                        <input type="number" name="mchc" id="mchc" class="form-control" placeholder="MCHC" onkeyup="cekRange('17','mchc')">
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="row" style="margin-top: -16px; margin-left: -24px;">
                                                <div class="col-md-6"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0">
                                                        <input id="cek_pendarahan" type="checkbox"> 
                                                        <label for="cek_pendarahan">Waktu Pendarahan (BT)</label>
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_darah" style="display: none;">
                                                        <div id="notification_waktu_pendarahan"></div>                                                         
                                                        <div id="tarif_waktu_pendarahan"></div>                                                         
                                                        <div id="periksa_waktu_pendarahan"></div>                                                         
                                                        <div id="id_waktu_pendarahan"></div>                                                         
                                                        <input type="text" id="waktu_pendarahan" name="waktu_pendarahan" class="form-control" placeholder="Waktu Pendarahan (BT)" style="margin-left:-6px" onkeyup="cekRange('36','waktu_pendarahan')"> 
                                                    </div> 
                                                </div>  
                                                <div class="col-md-6"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0">
                                                        <input id="cek_pembekuan" type="checkbox"> 
                                                        <label for="cek_pembekuan">Waktu Pembekuan (CT)</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_beku" style="display: none;">
                                                        <div id="notification_waktu_pembekuan"></div>                                                        
                                                        <div id="tarif_waktu_pembekuan"></div>                                                        
                                                        <div id="periksa_waktu_pembekuan"></div>                                                        
                                                        <div id="id_waktu_pembekuan"></div>                                                        
                                                        <input type="text" id="waktu_pembekuan" name="waktu_pembekuan" class="form-control" placeholder="Waktu Pembekuan" style="margin-left:-6px" onkeyup="cekRange('37','waktu_pembekuan')"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6" style="margin-top: -16px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0">
                                                        <input id="cek_goldar" type="checkbox"> 
                                                        <label for="cek_goldar">Gol. Darah ABO</label> 
                                                    </div> 
                                                    <div class="form-group col-md-12" id="div_goldar" style="display: none;" >
                                                        <div id="notification_gol_darah"></div>                                                        
                                                        <div id="tarif_gol_darah"></div>  
                                                        <div id="periksa_gol_darah"></div>  
                                                        <div id="id_gol_darah"></div>  
                                                        <input type="text" name="gol_darah" id="gol_darah" class="form-control" placeholder="Gol. Darah ABO" style="margin-left:-6px"> 
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>      
                                </div>
                            </div>
                        <!-- </div> 
                        <div class="col-md-6"  style="display:none;" id="menu_urine"> -->
                            <div style="display:none;" id="menu_urine">
                                <div class="panel panel-info1">  
                                    <div class="panel-heading no-padding">URINE     
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a></div>
                                    </div>        
                                    <div class="panel-wrapper collapse" aria-expanded="false">  
                                        <div class="panel-body">
                                            <div class="row" style="margin-top: -21px; margin-left: -24px;">
                                                <div class="col-md-12"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_urine_lengkap" type="checkbox"> 
                                                        <label for="cek_urine_lengkap">Urine Lengkap</label> 
                                                    </div>
                                                </div>
                                                <div id="div_urin_lengkap" style="display: none; margin-left: 32px; margin-top: -8px; ">
                                                    <div class="row" style="margin-top: -10px;"> 
                                                        <div class="form-group col-md-6" >
                                                            <label>&nbsp;</label>
                                                            <div id="notification_protein"></div>                                                                                                                    
                                                            <div id="tarif_protein"></div>                                                                                                                    
                                                            <div id="periksa_protein"></div>                                                                                                                    
                                                            <div id="id_protein"></div>                                                                                                                    
                                                            <input type="number" id="protein" name="protein" class="form-control" placeholder="Protein" onkeyup="cekRange('49','protein')" >
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>&nbsp;</label>
                                                            <input type="number" id="bilirubin" name="bilirubin" class="form-control" placeholder="Bilirubin" onkeyup="cekRange('52','bilirubin')">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: -41px;">
                                                        <div class="form-group col-md-6">
                                                            <label>&nbsp;</label>
                                                            <div id="notification_glukosa"></div>                                                                                                                    
                                                            <div id="tarif_glukosa"></div>                                                                                                                    
                                                            <div id="periksa_glukosa"></div>                                                                                                                    
                                                            <div id="id_glukosa"></div>                                                                                                                    
                                                            <input type="number" id="glukosa" name="glukosa" class="form-control" placeholder="Glukosa" onkeyup="cekRange('50','glukosa')">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>&nbsp;</label>
                                                            <div id="notification_sedimen"></div>                                                                                                                    
                                                            <div id="tarif_sedimen"></div>                                                                                                                    
                                                            <div id="periksa_sedimen"></div>                                                                                                                    
                                                            <div id="id_sedimen"></div>                                                                                                                    
                                                            <input type="number" id="sedimen" name="sedimen" class="form-control" placeholder="Sedimen" onkeyup="cekRange('','sedimen')" >
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: -41px;">
                                                        <div class="form-group col-md-6">
                                                            <label>&nbsp;</label>
                                                            <div id="notification_urobilin"></div>                                                                                                                    
                                                            <div id="tarif_urobilin"></div>                                                                                                                    
                                                            <div id="periksa_urobilin"></div>                                                                                                                    
                                                            <div id="id_urobilin"></div>                                                                                                                    
                                                            <input type="number" id="urobilin" name="urobilin" class="form-control" placeholder="Urobilin" onkeyup="cekRange('51','urobilin')">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>&nbsp;</label>
                                                            <div id="notification_ph"></div>                                                                                                                    
                                                            <div id="tarif_ph"></div>                                                                                                                    
                                                            <div id="periksa_ph"></div>                                                                                                                    
                                                            <div id="id_ph"></div>                                                                                                                    
                                                            <input type="number" id="ph" name="ph" class="form-control" placeholder="pH" onkeyup="cekRange('48','ph')" >
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            <!--  <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_protein" type="checkbox"> 
                                                        <label for="cek_protein">Protein</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_protein" style="display: none; margin-left: 16px">
                                                        <input type="text" id="protein" name="protein" class="form-control" placeholder="Protein" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_bilirubin" type="checkbox"> 
                                                        <label for="cek_bilirubin">Bilirubin</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_bilirubin" style="display: none; margin-left: 16px">
                                                        <input type="text" id="bilirubin" name="bilirubin" class="form-control" placeholder="Bilirubin" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_glukosa" type="checkbox"> 
                                                        <label for="cek_glukosa">Glukosa</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_glukosa" style="display: none; margin-left: 16px">
                                                        <input type="text" id="glukosa" name="glukosa" class="form-control" placeholder="Glukosa" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_sedimen" type="checkbox"> 
                                                        <label for="cek_sedimen">Sedimen</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_sedimen" style="display: none; margin-left: 16px">
                                                        <input type="text" id="sedimen" name="sedimen" class="form-control" placeholder="Sedimen" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_urobilin" type="checkbox"> 
                                                        <label for="cek_urobilin">Urobilin</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_urobilin" style="display: none; margin-left: 16px">
                                                        <input type="text" id="urobilin" name="urobilin" class="form-control" placeholder="Urobilin" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_ph" type="checkbox"> 
                                                        <label for="cek_ph">pH</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_ph" style="display: none; margin-left: 16px">
                                                        <input type="text" id="ph" name="ph" class="form-control" placeholder="pH" > 
                                                    </div> 
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>      
                                </div>
                            </div>
                        <!-- </div>
                        <div class="col-md-6"  style="display:none;" id="menu_usg"> -->
                            <!-- <div style="display:none;" id="menu_usg">
                                <div class="panel panel-info1">  
                                    <div class="panel-heading no-padding">USG     
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a></div>
                                    </div>        
                                    <div class="panel-wrapper collapse" aria-expanded="false">  
                                        <div class="panel-body">
                                            <div class="row" style="margin-top: -21px; margin-left: -24px;">
                                                <div class="col-md-12"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_usg_kebidanan" type="checkbox"> 
                                                        <label for="cek_usg_kebidanan">USG Kebidanan</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_usg_kebidanan" style="display: none; margin-left: 16px">
                                                        <div id="notification_usg_kebidanan"></div>                                                                                                                    
                                                        <div id="tarif_usg_kebidanan"></div>                                                                                                                    
                                                        <div id="periksa_usg_kebidanan"></div>        
                                                        <input type="text" id="usg_kebidanan" name="usg_kebidanan" class="form-control" placeholder="USG Kebidanan"  > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_tvs" type="checkbox"> 
                                                        <label for="cek_tvs">TVS (Transvaginal)</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_tvs" style="display: none; margin-left: 16px">
                                                        <div id="notification_tvs"></div>                                                                                                                    
                                                        <div id="tarif_tvs"></div>                                                                                                                    
                                                        <div id="periksa_tvs"></div> 
                                                        <input type="text" id="tvs" name="tvs" class="form-control" placeholder="TVS (Transvaginal)" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_usg_4d" type="checkbox"> 
                                                        <label for="cek_usg_4d">USG 4D</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_usg_4d" style="display: none; margin-left: 16px">
                                                        <div id="notification_usg_4d"></div>                                                                                                                    
                                                        <div id="tarif_tvs_usg_4d"></div>                                                                                                                    
                                                        <div id="periksa_tvs_usg_4d"></div> 
                                                        <input type="text" id="usg_4d" name="usg_4d" class="form-control" placeholder="USG 4D" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_ecg" type="checkbox"> 
                                                        <label for="cek_ecg">ECG</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_ecg" style="display: none; margin-left: 16px">
                                                        <div id="notification_ecg"></div>                                                                                                                    
                                                        <div id="tarif_ecg"></div>                                                                                                                    
                                                        <div id="periksa_ecg"></div> 
                                                        <input type="text" id="ecg" name="ecg" class="form-control" placeholder="ECG" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_nst" type="checkbox"> 
                                                        <label for="cek_nst">NST</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_nst" style="display: none; margin-left: 16px">
                                                        <div id="notification_nst"></div>                                                                                                                    
                                                        <div id="tarif_nst"></div>                                                                                                                    
                                                        <div id="periksa_nst"></div> 
                                                        <input type="text" id="nst" name="nst" class="form-control" placeholder="NST"  > 
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>      
                                </div>
                            </div> -->
                        </div>
                        <div class="col-md-6">
                            <div style="display:none;" id="menu_kimia_klinik">
                                <div class="panel panel-info1"> 
                                    <div class="panel-heading no-padding">KIMIA KLINIK     
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a></div>
                                    </div>    
                                    <div class="panel-wrapper collapse" aria-expanded="false">
                                        <div class="panel-body">
                                            <div class="row" style="margin-top: -21px; margin-left: -24px;">
                                                <div class="col-md-6"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_sgot" type="checkbox"> 
                                                        <label for="cek_sgot">SGOT</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_sgot" style="display: none; margin-left: 24px;">
                                                        <div id="notification_sgot"></div>
                                                        <div id="tarif_sgot"></div>
                                                        <div id="periksa_sgot"></div>
                                                        <div id="id_sgot"></div>
                                                        <input type="number" id="sgot" name="sgot" class="form-control" placeholder="SGOT" style="margin-left:-6px" onkeyup="cekRange('31','sgot')"> 
                                                    </div> 
                                                </div>  
                                                <div class="col-md-6" > 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_bilirubin_total" type="checkbox"> 
                                                        <label for="cek_bilirubin_total">Bilirubin (Total/Dir/Ind)</label> 
                                                    </div> 
                                                    <div class="form-group col-md-12" id="div_bilirubin_total" style="display: none; margin-left: 24px;">
                                                        <div id="notification_bilirubin_total"></div>                                                        
                                                        <div id="tarif_bilirubin_total"></div>                                                        
                                                        <div id="periksa_bilirubin_total"></div>                                                        
                                                        <div id="id_bilirubin_total"></div>                                                        
                                                        <input type="number" id="bilirubin_total" name="bilirubin_total" class="form-control" placeholder="Bilirubin (Total/Dir/Ind)" style="margin-left:-6px" onkeyup="cekRange('33','bilirubin_total')"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6" style="margin-top: -16px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_sgpt" type="checkbox"> 
                                                        <label for="cek_sgpt">SGPT</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_sgpt" style="display: none; margin-left: 24px">
                                                        <div id="notification_sgpt"></div>                                                                                                                
                                                        <div id="periksa_sgpt"></div>                                                                                                                
                                                        <div id="tarif_sgpt"></div>                                                                                                                
                                                        <div id="id_sgpt"></div>                                                                                                                
                                                        <input type="number" id="sgpt" name="sgpt" class="form-control" placeholder="SGPT" style="margin-left:-6px" onkeyup="cekRange('32','sgpt')" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6" style="margin-top: -16px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_glukosa_puasa" type="checkbox"> 
                                                        <label for="cek_glukosa_puasa">Glukosa Puasa</label> 
                                                    </div> 
                                                    <div class="form-group col-md-12" id="div_glukosa_puasa" style="display: none; margin-left: 24px">
                                                        <div id="notification_glukosa_puasa"></div>                                                                                                                                                                    
                                                        <div id="periksa_glukosa_puasa"></div>                                                                                                                                                                    
                                                        <div id="id_glukosa_puasa"></div>                                                                                                                                                                    
                                                        <div id="tarif_glukosa_puasa"></div>                                                                                                                                                                    
                                                        <input type="text" id="glukosa_puasa" name="glukosa_puasa" class="form-control" placeholder="Glukosa Puasa" style="margin-left:-6px" onkeyup="cekRange('1','glukosa_puasa')"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6" style="margin-top: -16px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_ureum" type="checkbox"> 
                                                        <label for="cek_ureum">Ureum</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_ureum" style="display: none; margin-left: 24px;">
                                                        <div id="notification_ureum"></div>                                                                                                                                                                                                                        
                                                        <div id="tarif_ureum"></div>                                                                                                                                                                                                                        
                                                        <div id="periksa_ureum"></div>                                                                                                                                                                                                                        
                                                        <div id="id_ureum"></div>                                                                                                                                                                                                                        
                                                        <input type="text" name="ureum" id="ureum" class="form-control" placeholder="Ureum" style="margin-left:-6px" onkeyup="cekRange('28','ureum')"> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6" style="margin-top: -16px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_glukosa_2jam_pp" type="checkbox"> 
                                                        <label for="cek_glukosa_2jam_pp">Glukosa 2 Jam PP</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_glukosa_2jam_pp" style="display: none; margin-left: 24px">
                                                        <div id="notification_glukosa_2jam_pp"></div>                                                                                                                                                                                                                                                                            
                                                        <div id="tarif_glukosa_2jam_pp"></div>                                                                                                                                                                                                                                                                            
                                                        <div id="periksa_glukosa_2jam_pp"></div>                                                                                                                                                                                                                                                                            
                                                        <div id="id_glukosa_2jam_pp"></div>                                                                                                                                                                                                                                                                            
                                                        <input type="text" id="glukosa_2jam_pp" name="glukosa_2jam_pp" class="form-control" placeholder="Glukosa 2 Jam PP" style="margin-left:-6px" onkeyup="cekRange('2','glukosa_2jam_pp')"> 
                                                    </div> 
                                                </div>  
                                                <div class="col-md-6" style="margin-top: -16px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_kreatinin" type="checkbox"> 
                                                        <label for="cek_kreatinin">Kreatinin</label> 
                                                    </div> 
                                                    <div class="form-group col-md-12" id="div_kreatinin" style="display: none; margin-left: 24px;">
                                                        <div id="notification_kreatinin"></div>                                                                                                                                                                                                                                                                                                                                                                                           
                                                        <div id="periksa_kreatinin"></div>                                                                                                                                                                                                                                                                                                                                                                                           
                                                        <div id="tarif_kreatinin"></div>                                                                                                                                                                                                                                                                                                                                                                                           
                                                        <div id="id_kreatinin"></div>                                                                                                                                                                                                                                                                                                                                                                                           
                                                        <input type="text" id="kreatinin" name="kreatinin" class="form-control" placeholder="Kreatinin" style="margin-left:-6px" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-6" style="margin-top: -16px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_glukosa_sewaktu" type="checkbox"> 
                                                        <label for="cek_glukosa_sewaktu">Glukosa Sewaktu</label> 
                                                    </div> 
                                                    <div class="form-group col-md-12" id="div_glukosa_sewaktu" style="display: none; margin-left: 24px">
                                                        <div id="notification_glukosa_sewaktu"></div>                                                                                                                                                                                                                                                                                                                                    
                                                        <div id="periksa_glukosa_sewaktu"></div>                                                                                                                                                                                                                                                                                                                                    
                                                        <div id="id_glukosa_sewaktu"></div>                                                                                                                                                                                                                                                                                                                                    
                                                        <div id="tarif_glukosa_sewaktu"></div>                                                                                                                                                                                                                                                                                                                                    
                                                        <input type="text" id="glukosa_sewaktu" name="glukosa_sewaktu" class="form-control" placeholder="Glukosa Sewaktu" style="margin-left:-6px" onkeyup="cekRange('3','glukosa_sewaktu')"> 
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>      
                                </div>
                            </div>
                        <!-- </div>
                        <div class="col-md-6"  style="display:none;" id="menu_dll"> -->
                            <div style="display:none;" id="menu_dll">
                                <div class="panel panel-info1">  
                                    <div class="panel-heading no-padding">LAIN-LAIN     
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a></div>
                                    </div>        
                                    <div class="panel-wrapper collapse" aria-expanded="false">  
                                        <div class="panel-body">
                                            <div class="row" style="margin-top: -21px; margin-left: -24px;">
                                                <div class="col-md-12"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_tespack" type="checkbox"> 
                                                        <label for="cek_tespack">Tes Pack (HCG EIA)</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_testpack" style="display: none; margin-left: 16px">
                                                        <input type="text" id="tespack" name="tespack" class="form-control" placeholder="Test Pack(HCG EIA)" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_papsmear" type="checkbox"> 
                                                        <label for="cek_papsmear">Pap Smear</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_papsmear" style="display: none; margin-left: 16px">
                                                        <input type="text" id="papsmear" name="papsmear" class="form-control" placeholder="Pap Smear" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_sperma" type="checkbox"> 
                                                        <label for="cek_sperma">Analisa Sperma</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_sperma" style="display: none; margin-left: 16px">
                                                        <input type="text" id="analisa_sperma" name="analisa_sperma" class="form-control" placeholder="Analisa Sperma" > 
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>      
                                </div>
                            </div>
                        <!-- </div>
                        <div class="col-md-6"  style="display:none;" id="menu_serologi"> -->
                            <div style="display:none;" id="menu_serologi">
                                <div class="panel panel-info1">  
                                    <div class="panel-heading no-padding">IMUNO SEROLOGI
                                        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a></div>
                                    </div>        
                                    <div class="panel-wrapper collapse" aria-expanded="false">  
                                        <div class="panel-body">
                                            <div class="row" style="margin-top: -20px; margin-left: -24px;">
                                                <div class="col-md-12"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_widal" type="checkbox"> 
                                                        <label for="cek_widal">Widal</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_widal" style="display: none; margin-left: 16px">
                                                        <input type="widal" id="widal" name="tespack" class="form-control" placeholder="Widal" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_igm" type="checkbox"> 
                                                        <label for="cek_igm">IgM Anti Dengue</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_igm" style="display: none; margin-left: 16px">
                                                        <input type="text" id="igm" name="igm" class="form-control" placeholder="IgM Anti Dengue" > 
                                                    </div> 
                                                </div>
                                                <div class="col-md-12" style="margin-top: -14px"> 
                                                    <div class="checkbox checkbox-primary pull-left p-t-0 col-md-12">
                                                        <input id="cek_igg" type="checkbox"> 
                                                        <label for="cek_igg">IgG Anti Dengue</label> 
                                                    </div>
                                                    <div class="form-group col-md-12" id="div_igg" style="display: none; margin-left: 16px">
                                                        <input type="text" id="igg" name="igg" class="form-control" placeholder="IgG Anti Dengue" > 
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    

                      <!--   <div class="row">
                            <div class="form-group col-md-6"> 
                                <input type="hidden" id="dokter_pengirim" name="dokter_pengirim" value="<?php echo $list_pasien->dokterpengirim_id?>">
                                <label>Tindakan<span style="color: red;"> *</span></label>
                                <select id="tindakan_lab" name="tindakan_lab" class="form-control select2" onchange="getTarifTindakanLab()"> 
                                    <option value=""  selected>Pilih Tindakan</option>
                                </select>
                                <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                                <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="jml_tindakan" class="active">Jumlah Tindakan<span style="color: red;"> *</span></label>
                                <input readonly id="jml_tindakan" type="number" min="1" name="jml_tindakan" class="form-control" value="" onkeyup="hitungHarga()" onchange="hitungHarga()"> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                 <label for="subtotal" class="active">Sub Total</label>
                                <input id="subtotal" type="text" name="subtotal" class="form-control" value="" placeholder="Sub Total" readonly> 
                            </div>
                            <div class="form-group col-md-4">
                                <label>Cyto<span style="color: red;"> *</span></label>
                                <select id="is_cyto" name="is_cyto" class="form-control" onchange="hitungHarga()">
                                    <option value="" disabled selected>Pilih Cyto</option> 
                                    <option value="1">Ya</option>
                                    <option value="0">Tidak</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="totalharga" class="active">Total Harga</label>
                                <input id="totalharga" type="text" name="totalharga" class="form-control" value="" placeholder="Sub Total" readonly> 
                            </div>  
                        </div> -->
                    </div> 
                    <div class="form-group col-md-12" style="margin-left: 8px; margin-top: 24px;">   
                        <button type="button" class="btn btn-success" id="saveTindakan"><i class="fa fa-floppy-o"></i> SIMPAN</button>   
                    </div>
                    <?php echo form_close();?> 
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tabs-hasil-tindakan">
                    <div class="col-md-12">
                        <ul class="nav customtab nav-tabs" role="tablist">
                            <li id="bpjs_tabs" role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#bpjs" id="bpjs_a" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">BPJS</span></a></li>
                            <li id="non_bpjs_tabs" role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#non_bpjs" id="non_bpjs_a" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">NON BPJS</span></a></li>  
                        </ul>
                        <!-- tabs sub kebidanan  -->
                        <div class="tab-content">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-info" id="print" onclick="printData('<?php echo $list_pasien->pasien_id; ?>','v_print_lab','1')">Print Hematologi</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-info" id="print" onclick="printData('<?php echo $list_pasien->pasien_id; ?>','v_print_lab','2')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Print Urine&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-info" id="print" onclick="printData('<?php echo $list_pasien->pasien_id; ?>','v_print_lab','3')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Print Klinik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade active in" id="bpjs">
                                <div class="col-md-12">
                                    <table id="table_hasil_tindakan_lab" class="table table-striped dataTable" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal Periksa</th>
                                                <th>Nama Pemeriksaan</th>
                                                <th>Jenis Pemeriksaan</th>
                                                <th>Hasil</th>
                                                <th>Batal/Hapus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="3">No data to display</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal Periksa</th>
                                                <th>Nama Pemeriksaan</th>
                                                <th>Jenis Pemeriksaan</th>
                                                <th>Hasil</th>
                                                <th>Batal/Hapus</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in" id="non_bpjs">
                            <div class="col-md-12">
                                    <table id="table_hasil_tindakan_lab_non_bpjs" class="table table-striped dataTable" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal Periksa</th>
                                                <th>Nama Pemeriksaan</th>
                                                <th>Jenis Pemeriksaan</th>
                                                <th>Hasil</th>
                                                <th>Batal/Hapus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="3">No data to display</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal Periksa</th>
                                                <th>Nama Pemeriksaan</th>
                                                <th>Jenis Pemeriksaan</th>
                                                <th>Hasil</th>
                                                <th>Batal/Hapus</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>


   

</div>
<!-- end whitebox --> 


<?php $this->load->view('footer_iframe');?>        
<script src="<?php echo base_url()?>assets/dist/js/pages/penunjang/laboratorium/periksa_laboratorium.js"></script>

</body>
 
</html>  



