<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gizi extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Laboratorium_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_gizi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "penunjang_gizi_view";
        $comments = "List Gizi";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_list_gizi', $this->data);
    }

    public function ajax_list_laboratorium(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $no_masukpenunjang = $this->input->get('no_masukpenunjang',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $status = $this->input->get('status',TRUE);
        $list = $this->Laboratorium_model->get_laboratorium_list($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $nama_pasien, $pasien_alamat, $status);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $laboratorium){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $laboratorium->no_masukpenunjang;
            $row[] = date('d-M-Y H:i:s', strtotime($laboratorium->tgl_masukpenunjang));
            $row[] = $laboratorium->no_rekam_medis;
            $row[] = $laboratorium->no_bpjs;
            $row[] = $laboratorium->pasien_nama;
            $row[] = $laboratorium->jenis_kelamin;
            $row[] = $laboratorium->umur;
            $row[] = $laboratorium->pasien_alamat;
            $row[] = $laboratorium->type_pembayaran;
            $row[] = $laboratorium->kelaspelayanan_nama;
            $row[] = $laboratorium->statusperiksa;
            if(trim(strtolower($laboratorium->statusperiksa)) == "sudah pulang" || trim(strtolower($laboratorium->statusperiksa)) == "batal periksa"){
                $row[] = '<button type="button" class="btn btn-default" title="Klik untuk pemeriksaan pasien" disabled>PERIKSA</button>';
                $row[] = '<button type="button" class="btn btn-default btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" disabled><i class="fa fa-times"></i></button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="periksalaboratorium('."'".$laboratorium->pasienmasukpenunjang_id."'".')">PERIKSA</button>';
                $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$laboratorium->pasienmasukpenunjang_id."','".$laboratorium->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>';
            }
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laboratorium_model->count_laboratorium_all($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis,$no_bpjs, $nama_pasien, $pasien_alamat, $status),
                    "recordsFiltered" => $this->Laboratorium_model->count_laboratorium_all($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs,$nama_pasien, $pasien_alamat, $status),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function periksa_laboratorium($pasienmasukpenunjang_id){
        $laboratorium['list_pasien'] = $this->Laboratorium_model->get_pendaftaran_pasien($pasienmasukpenunjang_id);

        $this->load->view('v_periksa_laboratorium', $laboratorium);
    }


    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Laboratorium_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            //$row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            $row[] = '<button type="button" class="btn btn-circle btn-danger" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanlab_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_tindakan_list(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $is_bpjs = $this->input->get('type_pembayaran',TRUE);
        $list_tindakan = $this->Laboratorium_model->get_tindakan($kelaspelayanan_id, $is_bpjs);
        $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->tariftindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_tarif_tindakan(){
        $tariftindakan_id = $this->input->get('tariftindakan_id',TRUE);
        $tarif_tindakan = $this->Laboratorium_model->get_tarif_tindakan($tariftindakan_id);

        $res = array(
            "list" => $tarif_tindakan,
            "success" => true
        );

        echo json_encode($res);
    }

    public function do_create_tindakan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_laboratorium_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tindakan_lab','Nama Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jumlah Tindakan', 'required|trim');
        $this->form_validation->set_rules('dokter_lab','Dokter Laboratorium', 'required|trim');
        $this->form_validation->set_rules('dokter_anastesi','Dokter Anastesi', 'required|trim');
        $this->form_validation->set_rules('perawat','Perawat', 'required|trim');
        $this->form_validation->set_rules('is_cyto','Cyto', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasienmasukpenunjang_id = $this->input->post('pasienmasukpenunjang_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter_pengirim = $this->input->post('dokter_pengirim', TRUE);
            $dokter_lab = $this->input->post('dokter_lab', TRUE);
            $dokter_anastesi = $this->input->post('dokter_anastesi', TRUE);
            $perawat = $this->input->post('perawat', TRUE);
            $tindakan = $this->input->post('tindakan_lab', TRUE);
            $jml_tindakan = $this->input->post('jml_tindakan', TRUE);
            $subtotal = $this->input->post('subtotal', TRUE);
            $is_cyto = $this->input->post('is_cyto', TRUE);
            $totalharga = $this->input->post('totalharga', TRUE);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'tariftindakan_id' => $tindakan,
                'jml_tindakan' => $jml_tindakan,
                'total_harga_tindakan' => $subtotal,
                'is_cyto' => $is_cyto == '1' ? '1' : '0',
                'total_harga' => $totalharga,
                'dokter_id' => $dokter_lab,
                'tgl_tindakan' => date('Y-m-d'),
                'dokterpengirim_id' => $dokter_pengirim,
                'dokteranastesi_id' => $dokter_anastesi,
                'perawat_id' => $perawat,
                'petugas_id' => $this->data['users']->id
            );

            $ins = $this->Laboratorium_model->insert_tindakanpasien($pendaftaran_id, $pasienmasukpenunjang_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_laboratorium_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakanlab_id = $this->input->post('tindakanlab_id', TRUE);

        $delete = $this->Laboratorium_model->delete_tindakan($tindakanlab_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakanlab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakanlab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function do_delete_laboratorium(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_laboratorium_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pasienmasukpenunjang_id = $this->input->post('pasienmasukpenunjang_id', TRUE);
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $delete = $this->Laboratorium_model->delete_periksa($pasienmasukpenunjang_id, $pendaftaran_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $pasienmasukpenunjang_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pasienmasukpenunjang_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function autocomplete_obat(){
        $val_obat = $this->input->get('val_obat');
        if(empty($val_obat)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }
        $pasien_list = $this->Laboratorium_model->get_obat_autocomplete($val_obat);
        if(count($pasien_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($pasien_list); $i++){
                $list[] = array(
                    "id"                => $pasien_list[$i]->obat_id,
                    "value"             => $pasien_list[$i]->nama_obat,
                    "deskripsi"         => $pasien_list[$i]->jenis_obat,
                    "satuan"            => $pasien_list[$i]->satuan,
                    "harga_netto"       => $pasien_list[$i]->harga_netto,
                    "harga_jual"        => $pasien_list[$i]->harga_jual,
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Pasien ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
        }
        echo json_encode($res);
    }

}
