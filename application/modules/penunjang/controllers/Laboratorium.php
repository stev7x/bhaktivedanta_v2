<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Laboratorium_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_laboratorium_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "penunjang_laboratorium_view";
        $comments = "List Pasien Laboratorium";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_laboratorium', $this->data);
    }

    public function ajax_list_laboratorium(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $no_masukpenunjang = $this->input->get('no_masukpenunjang',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $status = $this->input->get('status',TRUE);
        $list = $this->Laboratorium_model->get_laboratorium_list($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $nama_pasien, $pasien_alamat, $status);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $laboratorium){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $laboratorium->no_masukpenunjang;
            $row[] = date('d-M-Y H:i:s', strtotime($laboratorium->tgl_masukpenunjang));
            $row[] = $laboratorium->no_rekam_medis;
            $row[] = $laboratorium->no_bpjs;
            $row[] = $laboratorium->pasien_nama;
            $row[] = $laboratorium->jenis_kelamin;
            $row[] = $laboratorium->umur;
            $row[] = $laboratorium->pasien_alamat;
            $row[] = $laboratorium->type_pembayaran;
            $row[] = $laboratorium->kelaspelayanan_nama;
            $row[] = $laboratorium->statusperiksa;
            if(trim(strtolower($laboratorium->statusperiksa)) == "sudah pulang" || trim(strtolower($laboratorium->statusperiksa)) == "batal periksa"){
                $row[] = '<button type="button" class="btn btn-default" title="Klik untuk pemeriksaan pasien" disabled>PERIKSA</button>';
                $row[] = '<button type="button" class="btn btn-default btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" disabled><i class="fa fa-times"></i></button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="periksalaboratorium('."'".$laboratorium->pasienmasukpenunjang_id."'".')">PERIKSA</button>';
                $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$laboratorium->pasienmasukpenunjang_id."','".$laboratorium->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>';
            }
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laboratorium_model->count_laboratorium_all($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis,$no_bpjs, $nama_pasien, $pasien_alamat, $status),
                    "recordsFiltered" => $this->Laboratorium_model->count_laboratorium_all($tgl_awal, $tgl_akhir, $no_masukpenunjang, $no_rekam_medis, $no_bpjs,$nama_pasien, $pasien_alamat, $status),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function ajax_list_hasil_pasien_lab(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $type_pembayaran = $this->input->get('type_pembayaran',TRUE);
        $list = $this->Laboratorium_model->get_data_pasien_lab($pendaftaran_id, $type_pembayaran);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $laboratorium){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d-M-Y H:i:s', strtotime($laboratorium->tanggal_periksa));
            $row[] = $laboratorium->nama_pemeriksaan;
            if($laboratorium->jenis_tindakan == 1 ){
                $row[] = "Hematologi";
            }else if($laboratorium->jenis_tindakan == 2){
                $row[] = "Urine";
            }else if($laboratorium->jenis_tindakan == 3){
                $row[] = "Kimia Klinik";
            }else if($laboratorium->jenis_tindakan == 4){
                $row[] = "Imuno Serulogi";
            }else if($laboratorium->jenis_tindakan == 5){
                $row[] = "Lain - lain";
            }else{
                $row[] = "Tidak Valid";
            }
            $row[] = $laboratorium->hasil ." ". $laboratorium->satuan;
            $row[] = '
                        <button type="button" class="btn btn-circle btn-danger" title="Klik untuk pemeriksaan pasien" onclick="hapusHasilTindakan('."'".$laboratorium->tindakan_lab_id."','".$laboratorium->lab_id."'".')" ><i class="fa fa-times"></i></button>
                    ';
            // onclick="hapusDong"("'.$laboratorium->tindakan_lab_id.'","'.$laboratorium->lab_id.'")

                    //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function ajax_list_hasil_pasien_lab_all(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Laboratorium_model->get_data_pasien_lab_all($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $laboratorium){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d-M-Y H:i:s', strtotime($laboratorium->tanggal_periksa));
            $row[] = $laboratorium->nama_pemeriksaan;
            if($laboratorium->jenis_tindakan == 1 ){
                $row[] = "Hematologi";
            }else if($laboratorium->jenis_tindakan == 2){
                $row[] = "Urine";
            }else if($laboratorium->jenis_tindakan == 3){
                $row[] = "Kimia Klinik";
            }else if($laboratorium->jenis_tindakan == 4){
                $row[] = "Imuno Serulogi";
            }else if($laboratorium->jenis_tindakan == 5){
                $row[] = "Lain - lain";
            }else{
                $row[] = "Tidak Valid";
            }
            $row[] = $laboratorium->hasil ." ". $laboratorium->satuan;
            $row[] = '
                        <button type="button" class="btn btn-circle btn-danger" title="Klik untuk pemeriksaan pasien" onclick="hapusHasilTindakan('."'".$laboratorium->tindakan_lab_id."','".$laboratorium->lab_id."'".')" ><i class="fa fa-times"></i></button>
                    ';
            // onclick="hapusDong"("'.$laboratorium->tindakan_lab_id.'","'.$laboratorium->lab_id.'")

                    //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function periksa_laboratorium($pasienmasukpenunjang_id){

        $laboratorium['list_pasien'] = $this->Laboratorium_model->get_pendaftaran_pasien($pasienmasukpenunjang_id);

        $this->load->view('v_periksa_laboratorium', $laboratorium);
    }


    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Laboratorium_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            //$row[] = $tindakan->total_harga_tindakan;
            $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            $row[] = '<button type="button" class="btn btn-circle btn-danger" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanlab_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_tindakan_list(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $is_bpjs = $this->input->get('type_pembayaran',TRUE);
        $list_tindakan = $this->Laboratorium_model->get_tindakan($kelaspelayanan_id, $is_bpjs);
        $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->tariftindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_tarif_tindakan(){
        $tariftindakan_id = $this->input->get('tariftindakan_id',TRUE);
        $tarif_tindakan = $this->Laboratorium_model->get_tarif_tindakan($tariftindakan_id);

        $res = array(
            "list" => $tarif_tindakan,
            "success" => true
        );

        echo json_encode($res);
    }

    function get_range_lab(){
        $lab_id = $this->input->get('lab_id',TRUE);
        $lab_range = $this->Laboratorium_model->get_range_lab($lab_id);

        $res = array(
            "list" => $lab_range,
            "success" => true
        );

        echo json_encode($res);
    }

    public function do_create_tindakan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_laboratorium_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('dokter_lab','Dokter Laboratorium', 'required|trim');
        // $this->form_validation->set_rules('hb','Dokter Laboratorium', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id          = $this->input->post('pendaftaran_id', TRUE);
            $pasienmasukpenunjang_id = $this->input->post('pasienmasukpenunjang_id', TRUE);
            $pasien_id               = $this->input->post('pasien_id', TRUE);
            $dokter_pengirim         = $this->input->post('dokter_pengirim', TRUE);
            $dokter_lab              = $this->input->post('dokter_lab', TRUE);
            $dokter_anastesi         = $this->input->post('dokter_anastesi', TRUE);
            $perawat                 = $this->input->post('perawat', TRUE);
            $tindakan                = $this->input->post('tindakan_lab', TRUE);
            $jml_tindakan            = $this->input->post('jml_tindakan', TRUE);
            $subtotal                = $this->input->post('subtotal', TRUE);
            $is_cyto                 = $this->input->post('is_cyto', TRUE);
            $totalharga              = $this->input->post('totalharga', TRUE);
            $type_pembayaran         = $this->input->post('type_pembayaran', TRUE);
            $nama_asuransi           = $this->input->post('nama_asuransi', TRUE);
            $no_asuransi             = $this->input->post('no_asuransi', TRUE);

            $hematologi_hasil_fix   =  $this->insert_lab_hasil('hb','hitung_jenis','leukosit','eritrosit','trombosit','mcv','hematrokrit','mch','led','mchc','waktu_pendarahan','waktu_pembekuan','gol_darah',"");
            $hematologi_nama_fix    =  $this->insert_lab_nama('hb_periksa','hitung_jenis_periksa','leukosit_periksa','eritrosit_periksa','trombosit_periksa','mcv_periksa','hematrokrit_periksa','mch_periksa','led_periksa','mchc_periksa','waktu_pendarahan_periksa','waktu_pembekuan_periksa','gol_darah_periksa',"");
            $hematologi_tarif_fix   =  $this->insert_lab_tarif('hb_tarif','hitung_jenis_tarif','leukosit_tarif','eritrosit_tarif','trombosit_tarif','mcv_tarif','hematrokrit_tarif','mch_tarif','led_tarif','mchc_tarif','waktu_pendarahan_tarif','waktu_pembekuan_tarif','gol_darah_tarif',"");
            $hematologi_id_fix      =  $this->insert_lab_id('hb_id','hitung_jenis_id','leukosit_id','eritrosit_id','trombosit_id','mcv_id','hematrokrit_id','mch_id','led_id','mchc_id','waktu_pendarahan_id','waktu_pembekuan_id','gol_darah_id',"");

            $urine_hasil_fix        =  $this->insert_lab_hasil('protein','glukosa','sedimen','urobilin','ph','','','','','','','','','');
            $urine_nama_fix         =  $this->insert_lab_nama('protein_periksa','glukosa_periksa','sedimen_periksa','urobilin_periksa','ph_periksa','','','','','','','','','');
            $urine_tarif_fix        =  $this->insert_lab_tarif('protein_tarif','glukosa_tarif','sedimen_tarif','urobilin_tarif','ph_tarif','','','','','','','','','');
            $urine_id_fix           =  $this->insert_lab_id('protein_id','glukosa_id','sedimen_id','urobilin_id','ph_id','','','','','','','','','');

            $klinik_hasil_fix       =  $this->insert_lab_hasil('sgot','bilirubin','sgpt','glukosa_puasa','ureum','glukosa_2jam_pp','kreatinin','glukosa_sewaktu','','','','','','');
            $klinik_nama_fix        =  $this->insert_lab_nama('sgot_periksa','bilirubin_periksa','sgpt_periksa','glukosa_puasa_periksa','ureum_periksa','glukosa_2jam_pp_periksa','kreatinin_periksa','glukosa_sewaktu_periksa','','','','','','');
            $klinik_tarif_fix       =  $this->insert_lab_tarif('sgot_tarif','bilirubin_tarif','sgpt_tarif','glukosa_puasa_tarif','ureum_tarif','glukosa_2jam_pp_tarif','kreatinin_tarif','glukosa_sewaktu_tarif','','','','','','');
            $klinik_id_fix          =  $this->insert_lab_id('sgot_id','bilirubin_id','sgpt_id','glukosa_puasa_id','ureum_id','glukosa_2jam_pp_id','kreatinin_id','glukosa_sewaktu_id','','','','','','');

            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id'      => $pasien_id,
                'dokter_id'      => $dokter_lab,
                'tgl_tindakan'   => date('Y-m-d'),
                'dokterpengirim_id' => $dokter_pengirim,
                'type_pembayaran'   => $type_pembayaran,
                'nama_asuransi'     => $nama_asuransi,
                'no_asuransi'       => $no_asuransi,
            );



            $ins = $this->Laboratorium_model->insert_tindakanpasien($pendaftaran_id, $pasienmasukpenunjang_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                if(!empty($hematologi_nama_fix)){
                    foreach ($hematologi_nama_fix as $index => $list) {
                        $data = array(
                            'tindakan_lab_id'   => $ins,
                            'lab_id'            => $hematologi_id_fix[$index],
                            'nama_pemeriksaan'  => $list,
                            'hasil'             => $hematologi_hasil_fix[$index],
                            'harga_satuan'      => $hematologi_tarif_fix[$index],
                            'jenis_tindakan'    => 1,
                            'tanggal_periksa'   => date('Y-m-d H:i:s'),
                            'type_pembayaran'   => $type_pembayaran,
                        );
                        $this->db->insert('t_det_tindakan_lab', $data);
                        // var_dump($data);
                    }
                }

                if(!empty($urine_nama_fix)){
                    foreach ($urine_nama_fix as $index => $list) {
                        $data = array(
                            'tindakan_lab_id'   => $ins,
                            'lab_id'            => $urine_id_fix[$index],
                            'nama_pemeriksaan'  => $list,
                            'hasil'             => $urine_hasil_fix[$index],
                            'harga_satuan'      => $urine_tarif_fix[$index],
                            'jenis_tindakan'    => 2,
                            'tanggal_periksa'   => date('Y-m-d H:i:s'),
                            'type_pembayaran'   => $type_pembayaran,


                        );
                        $this->db->insert('t_det_tindakan_lab', $data);
                        // var_dump($data);
                    }
                }

                if(!empty($klinik_nama_fix)){
                    foreach ($klinik_nama_fix as $index => $list) {
                        $data = array(
                            'tindakan_lab_id'   => $ins,
                            'lab_id'            => $klinik_id_fix[$index],
                            'nama_pemeriksaan'  => $list,
                            'hasil'             => $klinik_hasil_fix[$index],
                            'harga_satuan'      => $klinik_tarif_fix[$index],
                            'jenis_tindakan'    => 3,
                            'tanggal_periksa'   => date('Y-m-d H:i:s'),
                            'type_pembayaran'   => $type_pembayaran,

                        );
                        $this->db->insert('t_det_tindakan_lab', $data);
                        // var_dump($data);
                    }
                }
                // die();


                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_laboratorium_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakan_lab_id = $this->input->post('tindakan_lab_id', TRUE);
        $lab_id = $this->input->post('tindakanlab_id', TRUE);

        $delete = $this->Laboratorium_model->delete_tindakan($tindakan_lab_id, $lab_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakanlab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakanlab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_hasil_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_laboratorium_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakan_lab_id = $this->input->post('tindakan_lab_id', TRUE);
        $lab_id = $this->input->post('lab_id', TRUE);

        // var_dump($tindakan_lab_id);
        // var_dump($lab_id);
        // die();

        $delete = $this->Laboratorium_model->delete_hasil_tindakan($tindakan_lab_id, $lab_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakan_lab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakan_lab_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_laboratorium(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_laboratorium_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pasienmasukpenunjang_id = $this->input->post('pasienmasukpenunjang_id', TRUE);
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $delete = $this->Laboratorium_model->delete_periksa($pasienmasukpenunjang_id, $pendaftaran_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $pasienmasukpenunjang_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "penunjang_laboratorium_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pasienmasukpenunjang_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    function autocomplete_obat(){
        $val_obat = $this->input->get('val_obat');
        if(empty($val_obat)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }
        $pasien_list = $this->Laboratorium_model->get_obat_autocomplete($val_obat);
        if(count($pasien_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($pasien_list); $i++){
                $list[] = array(
                    "id"                => $pasien_list[$i]->obat_id,
                    "value"             => $pasien_list[$i]->nama_obat,
                    "deskripsi"         => $pasien_list[$i]->jenis_obat,
                    "satuan"            => $pasien_list[$i]->satuan,
                    "harga_netto"       => $pasien_list[$i]->harga_netto,
                    "harga_jual"        => $pasien_list[$i]->harga_jual,
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Pasien ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
        }
        echo json_encode($res);
    }

    function insert_lab_tarif($harga1,$harga2,$harga3, $harga4, $harga5, $harga6, $harga7, $harga8, $harga9, $harga10, $harga11, $harga12, $harga13, $harga14){
            $col_tarif1                 = $this->input->post($harga1, TRUE);
            $col_tarif2                 = $this->input->post($harga2, TRUE);
            $col_tarif3                 = $this->input->post($harga3, TRUE);
            $col_tarif4                 = $this->input->post($harga4, TRUE);
            $col_tarif5                 = $this->input->post($harga5, TRUE);
            $col_tarif6                 = $this->input->post($harga6, TRUE);
            $col_tarif7                 = $this->input->post($harga7, TRUE);
            $col_tarif8                 = $this->input->post($harga8, TRUE);
            $col_tarif9                 = $this->input->post($harga9, TRUE);
            $col_tarif10                 = $this->input->post($harga10, TRUE);
            $col_tarif11                 = $this->input->post($harga11, TRUE);
            $col_tarif12                 = $this->input->post($harga12, TRUE);
            $col_tarif13                 = $this->input->post($harga13, TRUE);
            $col_tarif14                 = $this->input->post($harga14, TRUE);


            $array_tarif = array(
                $col_tarif1,
                $col_tarif2,
                $col_tarif3,
                $col_tarif4,
                $col_tarif5,
                $col_tarif6,
                $col_tarif7,
                $col_tarif8,
                $col_tarif9,
                $col_tarif10,
                $col_tarif11,
                $col_tarif12,
                $col_tarif13,
                $col_tarif14,
            );


            $array_tarif_fix = array();
            $array_tarif_not_fix = array();

            foreach($array_tarif as $list){
                if (!empty($list)){
                    $array_tarif_fix[] = $list;
                }else{
                    $array_tarif_not_fix[] = $list;
                }
            }

            return $array_tarif_fix;
    }

    function insert_lab_nama($nama1,$nama2,$nama3,$nama4,$nama5,$nama6,$nama7,$nama8,$nama9,$nama10,$nama11,$nama12,$nama13,$nama14){
            $col_nama1                 = $this->input->post($nama1, TRUE);
            $col_nama2                 = $this->input->post($nama2, TRUE);
            $col_nama3                 = $this->input->post($nama3, TRUE);
            $col_nama4                 = $this->input->post($nama4, TRUE);
            $col_nama5                 = $this->input->post($nama5, TRUE);
            $col_nama6                 = $this->input->post($nama6, TRUE);
            $col_nama7                 = $this->input->post($nama7, TRUE);
            $col_nama8                 = $this->input->post($nama8, TRUE);
            $col_nama9                 = $this->input->post($nama9, TRUE);
            $col_nama10                 = $this->input->post($nama10, TRUE);
            $col_nama11                = $this->input->post($nama11, TRUE);
            $col_nama12                 = $this->input->post($nama12, TRUE);
            $col_nama13                 = $this->input->post($nama13, TRUE);
            $col_nama14                 = $this->input->post($nama14, TRUE);


            $array_nama = array(
                $col_nama1,
                $col_nama2,
                $col_nama3,
                $col_nama4,
                $col_nama5,
                $col_nama6,
                $col_nama7,
                $col_nama8,
                $col_nama9,
                $col_nama10,
                $col_nama11,
                $col_nama12,
                $col_nama13,
                $col_nama14,
            );

            $array_nama_fix = array();
            $array_nama_not_fix = array();


            foreach($array_nama as $list){
                if (!empty($list)){
                    $array_nama_fix[] = $list;
                }else{
                    $array_nama_not_fix[] = $list;
                }
            }

            return $array_nama_fix;
    }


    function insert_lab_hasil($hasil1,$hasil2,$hasil3,$hasil4,$hasil5,$hasil6,$hasil7,$hasil8,$hasil9,$hasil10,$hasil11,$hasil12,$hasil13,$hasil14){
            $col_hasil1                 = $this->input->post($hasil1, TRUE);
            $col_hasil2                 = $this->input->post($hasil2, TRUE);
            $col_hasil3                 = $this->input->post($hasil3, TRUE);
            $col_hasil4                 = $this->input->post($hasil4, TRUE);
            $col_hasil5                 = $this->input->post($hasil5, TRUE);
            $col_hasil6                 = $this->input->post($hasil6, TRUE);
            $col_hasil7                 = $this->input->post($hasil7, TRUE);
            $col_hasil8                 = $this->input->post($hasil8, TRUE);
            $col_hasil9                 = $this->input->post($hasil9, TRUE);
            $col_hasil10                 = $this->input->post($hasil10, TRUE);
            $col_hasil11                 = $this->input->post($hasil11, TRUE);
            $col_hasil12                 = $this->input->post($hasil12, TRUE);
            $col_hasil13                 = $this->input->post($hasil13, TRUE);
            $col_hasil14                 = $this->input->post($hasil14, TRUE);


            $array_hasil = array(
                $col_hasil1,
                $col_hasil2,
                $col_hasil3,
                $col_hasil4,
                $col_hasil5,
                $col_hasil6,
                $col_hasil7,
                $col_hasil8,
                $col_hasil9,
                $col_hasil10,
                $col_hasil11,
                $col_hasil12,
                $col_hasil13,
                $col_hasil14,
            );
            $array_hasil_fix = array();
            $array_hasil_not_fix = array();

            foreach($array_hasil as $list){
                if (!empty($list)){
                    $array_hasil_fix[] = $list;
                }else{
                    $array_hasil_not_fix[] = $list;
                }
            }
            return $array_hasil_fix;
    }


    function insert_lab_id($id1,$id2,$id3,$id4,$id5,$id6,$id7,$id8,$id9,$id10,$id11,$id12,$id13,$id14){
            $col_id1                 = $this->input->post($id1, TRUE);
            $col_id2                 = $this->input->post($id2, TRUE);
            $col_id3                 = $this->input->post($id3, TRUE);
            $col_id4                 = $this->input->post($id4, TRUE);
            $col_id5                 = $this->input->post($id5, TRUE);
            $col_id6                 = $this->input->post($id6, TRUE);
            $col_id7                 = $this->input->post($id7, TRUE);
            $col_id8                 = $this->input->post($id8, TRUE);
            $col_id9                 = $this->input->post($id9, TRUE);
            $col_id10                = $this->input->post($id10, TRUE);
            $col_id11                = $this->input->post($id11, TRUE);
            $col_id12                = $this->input->post($id12, TRUE);
            $col_id13                = $this->input->post($id13, TRUE);
            $col_id14                = $this->input->post($id14, TRUE);


            $array_id = array(
                $col_id1,
                $col_id2,
                $col_id3,
                $col_id4,
                $col_id5,
                $col_id6,
                $col_id7,
                $col_id8,
                $col_id9,
                $col_id10,
                $col_id11,
                $col_id12,
                $col_id13,
                $col_id14,
            );
            $array_id_fix = array();
            $array_id_not_fix = array();

            foreach($array_id as $list){
                if (!empty($list)){
                    $array_id_fix[] = $list;
                }else{
                    $array_id_not_fix[] = $list;
                }
            }
            return $array_id_fix;
    }




    public function hasil_lab($pasien_id, $view, $id){
        $tindakan_list['list_hasil_lab'] = $this->Laboratorium_model->get_data_hasil_lab($pasien_id, $id);
        $tindakan_list['hasil_lab'] = $this->Laboratorium_model->get_data_hasil_lab_2($pasien_id, $id);
        $tindakan_list['list_pasien'] = $this->Laboratorium_model->get_pendaftaran_pasien_2($pasien_id);

        $this->load->view('penunjang/'.$view,$tindakan_list);
        // print_r($tindakan_list);die();
    }

    public function getCaraPembayaran($id){
        $data = $this->Laboratorium_model->getCaraPembayaran($id);
        echo json_encode($data);

    }

}
