<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Radiologi extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Radiologi_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_radiologi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "penunjang_radiologi_view";
        $comments = "List Pasien Radiologi";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_radiologi', $this->data);
    }

    public function ajax_list_radiologi(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $status = $this->input->get('status',TRUE);
        $no_masukpenunjang = $this->input->get('no_masukpenunjang',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs = $this->input->get('no_bpjs',TRUE);
        $nama_pasien = $this->input->get('nama_pasien',TRUE);
        $pasien_alamat = $this->input->get('pasien_alamat',TRUE);
        $list = $this->Radiologi_model->get_radiologi_list($tgl_awal, $tgl_akhir, $status, $no_masukpenunjang, $no_rekam_medis, $no_bpjs, $nama_pasien, $pasien_alamat);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $radiologi){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $radiologi->no_masukpenunjang;
            $row[] = date('d-M-Y H:i:s', strtotime($radiologi->tgl_masukpenunjang));
            $row[] = $radiologi->no_rekam_medis;
            $row[] = $radiologi->no_bpjs;
            $row[] = $radiologi->pasien_nama;
            $row[] = $radiologi->jenis_kelamin;
            $row[] = $radiologi->umur;
            $row[] = $radiologi->pasien_alamat;
            $row[] = $radiologi->type_pembayaran;
            $row[] = $radiologi->kelaspelayanan_nama;
            $row[] = $radiologi->statusperiksa;
            if(trim(strtolower($radiologi->statusperiksa)) == "sudah pulang" || trim(strtolower($radiologi->statusperiksa)) == "batal periksa"){
                $row[] = '<button type="button" class="btn btn-default" title="Klik untuk pemeriksaan pasien" disabled>PERIKSA</button>';
                $row[] = '<button type="button" class="btn btn-default btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" disabled><i class="fa fa-times"></i></button>';
            }else{
                $row[] = '<button type="button" class="btn btn-success" title="Klik untuk pemeriksaan pasien" onclick="periksaradiologi('."'".$radiologi->pasienmasukpenunjang_id."','".$radiologi->pendaftaran_id."'".')">PERIKSA</button>';
                $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$radiologi->pasienmasukpenunjang_id."','".$radiologi->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>';
            }
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Radiologi_model->count_radiologi_all($tgl_awal, $tgl_akhir, $status, $no_masukpenunjang, $no_rekam_medis,$no_bpjs, $nama_pasien, $pasien_alamat),
                    "recordsFiltered" => $this->Radiologi_model->count_radiologi_all($tgl_awal, $tgl_akhir, $status, $no_masukpenunjang, $no_rekam_medis,$no_bpjs, $nama_pasien, $pasien_alamat),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function periksa_radiologi($pasienmasukpenunjang_id){
        $radiologi['list_pasien'] = $this->Radiologi_model->get_pendaftaran_pasien($pasienmasukpenunjang_id);
        // echo "<pre>";
        // print_r ($radiologi);die();
        // echo "</pre>";

        $this->load->view('v_periksa_radiologi', $radiologi);
    }


    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Radiologi_model->get_tindakan_pasien($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            $row[] = count($tindakan->jumlahobat);
            $row[] = count($tindakan->jumlahbhp);
             $row[] = $tindakan->NAME_DOKTER;
            // if($tindakan->type_pembayaran == 1){
            //     $row[] = "BPJS";
            // }else{
            //     $row[] = "NON BPJS";
            // }
            //$row[] = $tindakan->total_harga_tindakan;
            // $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanradiologi_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }

        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


  public function ajax_get_tindakan_by_id(){
		$tindakan_id = $this->input->get('daftartindakan_id',TRUE);
		$data_tindakan = $this->Radiologi_model->get_tindakan_by_id($tindakan_id);
		if (sizeof($data_tindakan)>0){
			$tindakan = $data_tindakan[0];

			$res = array(
				"success" => true,
				"messages" => "Data Tindakan ditemukan",
				"data" => $tindakan
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data Tindakan tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
	}


  public function ajax_get_user_by_id(){
		$id = $this->input->get('id',TRUE);
		$data_user = $this->Radiologi_model->get_user_by_id($id);
		if (sizeof($data_user)>0){
			$id = $data_user[0];

			$res = array(
				"success" => true,
				"messages" => "Data Tindakan ditemukan",
				"data" => $id
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data Tindakan tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
	}


  public function ajax_list_tindakan(){
		$data_list_tindakan = $this->Radiologi_model->get_list_tindakan();
        $id = 1;
		$data = array();
		foreach($data_list_tindakan as $tindakan){
			$row = array();
			$row[] = $tindakan->daftartindakan_nama;
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihListTindakan('."'".$tindakan->daftartindakan_id."'".','."'".$id."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" =>   $this->Radiologi_model->count_list_tindakan(),
			"recordsFiltered" =>   $this->Radiologi_model->count_list_filtered_tindakan(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}


  public function ajax_list_user(){
		$data_list_user = $this->Radiologi_model->get_list_user();
        $idr = 1;
		$data = array();
		foreach($data_list_user as $user){
			$row = array();
			$row[] = $user->username;
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihListUser('."'".$user->id."'".','."'".$idr."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" =>   $this->Radiologi_model->count_list_user(),
			"recordsFiltered" =>   $this->Radiologi_model->count_list_filtered_user(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}

    public function ajax_list_hasil_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Radiologi_model->get_tindakan_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $tindakan){
            $tgl_tindakan = date_create($tindakan->tgl_tindakan);
            $row = array();
            $row[] = date_format($tgl_tindakan, 'd-M-Y');
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            //$row[] = $tindakan->total_harga_tindakan;
            // $row[] = $tindakan->is_cyto == '1' ? "Ya" : "Tidak";
            //$row[] = $tindakan->total_harga;
            $row[] = '
            <button type="button" class="btn btn-warning btn-circle" data-toggle="modal" data-target="#modal_hasil_tindakan" data-backdrop="static" data-keyboard="false" title="Klik untuk memberikan hasil tindakan" onclick="editTindakan('."'".$tindakan->tindakanradiologi_id."'".')"><i class="fa fa-edit"></i></button>

            <button type="button" class="btn btn-success btn-circle" title="Klik untuk print tindakan" onclick="printTindakan('."'".$tindakan->tindakanradiologi_id."'".')"><i class="fa fa-print"></i></button>

            ';
            //add html for action
            $data[] = $row;
        }

        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_tindakan_list(){
        $kelaspelayanan_id = $this->input->get('kelaspelayanan_id',TRUE);
        $is_bpjs = $this->input->get('type_pembayaran',TRUE);
        $list_tindakan = $this->Radiologi_model->get_tindakan($kelaspelayanan_id, $is_bpjs);
        $list = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }


        public function print_resep($pendaftaran_id){
        $print['pendaftaran_id'] = $pendaftaran_id;
     //   $get_resep = $this->Pasien_rd_model->get_detail_resep_pasien($pendaftaran_id);
        //$print['reseptur'] = $this->Pasien_rd_model->get_pendaftran_resep_pasien($get_resep->pendaftaran_id);
         // $print['reseprd'] = $this->Pasien_rd_model->get_pendaftran_resep_pasien($pendaftaran_id);
        // $print['pembayarankasir_id'] = $pembayarankasir_id;
        // $print['tot_obat'] = $this->Kasirrd_model->sum_harga_obat_sudah_bayar($pembayarankasir_id);
        // $print['tot_rad'] = $this->Pasien_rd_model->sum_total_resep($pendaftaran_id);
        // $print['tot_lab'] = $this->Kasirrd_model->sum_harga_lab_sudah_bayar($pembayarankasir_id);
        // $print['tot_tind'] = $this->Kasirrd_model->sum_tindakan_sudah_bayar($pembayarankasir_id);
        // $print['tindakan_list'] = $this->Kasirrd_model->get_tindakan_pasien($pembayarankasir_id);
        // $print['total_bayar'] = $this->Kasirrd_model->get_print_bayar($pembayarankasir_id);

        // e
        // echo "<pre>";
        // print_r($print);die();
        // echo "</pre>";

        //$this->load->view('rawatdarurat/print_reseptur', $print);

        $this->load->view('penunjang/print_hasil', $print);
    }


    function get_tarif_tindakan(){
        $daftartindakan_id = $this->input->get('daftartindakan_id',TRUE);
        $tarif_tindakan = $this->Radiologi_model->get_tarif_tindakan($daftartindakan_id);

        $res = array(
            "list" => $tarif_tindakan,
            "success" => true
        );

        echo json_encode($res);
    }

    public function do_create_tindakan_pasien(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_radiologi_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tindakan','Nama Tindakan', 'required|trim');
        $this->form_validation->set_rules('dokter_rad','Dokter Radiologi', 'required|trim');
        $this->form_validation->set_rules('dokter_anastesi','Dokter Anastesi', 'required|trim');
        $this->form_validation->set_rules('perawat','Perawat', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jumlah Tindakan', 'required|trim');
        $this->form_validation->set_rules('is_cyto','Cyto', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "penunjang_radiologi_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $penunjang_id = $this->input->post('penunjang_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter_pengirim = $this->input->post('dokter_pengirim', TRUE);
            $dokter_rad = $this->input->post('dokter_rad', TRUE);
            $dokter_anastesi = $this->input->post('dokter_anastesi', TRUE);
            $perawat = $this->input->post('perawat', TRUE);
            $tindakan = $this->input->post('tindakan', TRUE);
            $jml_tindakan = $this->input->post('jml_tindakan', TRUE);
            $subtotal = $this->input->post('subtotal', TRUE);
            $is_cyto = $this->input->post('is_cyto', TRUE);
            $type_pembayaran = $this->input->post('type_pembayaran', TRUE);
            $totalharga = $this->input->post('totalharga', TRUE);
            $data_tindakanpasien = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id' => $pasien_id,
                'daftartindakan_id' => $tindakan,
                'jml_tindakan' => $jml_tindakan,
                'total_harga_tindakan' => $subtotal,
                'is_cyto' => $is_cyto == '1' ? '1' : '0',
                'total_harga' => $totalharga,
                'dokter_id' => $dokter_rad,
                'tgl_tindakan' => date('Y-m-d'),
                'dokterpengirim_id' => $dokter_pengirim,
                'dokteranastesi_id' => $dokter_anastesi,
                'type_pembayaran' => $type_pembayaran,
                'perawat_id' => $perawat,
                'petugas_id' => $this->data['users']->id,
            );


            $ins = $this->Radiologi_model->insert_tindakanpasien($pendaftaran_id, $penunjang_id, $data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "penunjang_radiologi_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "penunjang_radiologi_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_radiologi_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakanradiologi_id = $this->input->post('tindakanradiologi_id', TRUE);

        $delete = $this->Radiologi_model->delete_tindakan($tindakanradiologi_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "penunjang_radiologi_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakanradiologi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "penunjang_radiologi_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakanradiologi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    public function do_delete_radiologi(){
        $is_permit = $this->aauth->control_no_redirect('penunjang_radiologi_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pasienmasukpenunjang_id = $this->input->post('pasienmasukpenunjang_id', TRUE);
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $delete = $this->Radiologi_model->delete_periksa($pasienmasukpenunjang_id,$pendaftaran_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "penunjang_radiologi_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "penunjang_radiologi_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }
    function autocomplete_obat(){
        $val_obat = $this->input->get('val_obat');
        if(empty($val_obat)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Pasien Berikut"
                );
        }
        $pasien_list = $this->Radiologi_model->get_obat_autocomplete($val_obat);
        if(count($pasien_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($pasien_list); $i++){
                $list[] = array(
                    "id"                => $pasien_list[$i]->obat_id,
                    "value"             => $pasien_list[$i]->nama_obat,
                    "deskripsi"         => $pasien_list[$i]->jenis_obat,
                    "satuan"            => $pasien_list[$i]->satuan,
                    "harga_netto"       => $pasien_list[$i]->harga_netto,
                    "harga_jual"        => $pasien_list[$i]->harga_jual,
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Pasien ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Pasien Not Found"
                );
        }
        echo json_encode($res);
    }

}
