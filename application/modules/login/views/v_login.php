<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/plugins/images/favicon.png">
    <title>Bhaktivedanta Medical</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url() ?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet"> 
    <!-- animation CSS -->
    <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
                                
    <!-- color CSS -->
    <link href="<?php echo base_url() ?>assets/css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <style type="text/css" media="screen">
        .hide{
            display: none;
        }      
        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: black !important;
            text-decoration: bold !important;
        }
        input,
        input::-webkit-input-placeholder {
            font-size: 15px;
            line-height: 3;
        }  
        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
           color:    black !important;
           opacity:  1 !important;
        }
        ::-moz-placeholder { /* Mozilla Firefox 19+ */
           color:    black !important;
           opacity:  1 !important;
        }
        :-ms-input-placeholder { /* Internet Explorer 10-11 */
           color:    black !important;  
        }
        ::-ms-input-placeholder { /* Microsoft Edge */
           color:    black !important ;
        }      
 

    </style>  
</head>

<body background="<?php echo base_url() ?>assets/plugins/images/login-register.jpg">  
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>  
    <section id="wrapper" class="login-register" style="background-size: cover; background-repeat: no-repeat; background-position: center center;">                           
        <div class="login-box login-sidebar" style="background:rgba(255,255,255, 0.70);">           
            <div class="white-box" style="margin-top: 100px;background:rgba(255,255,255, 0.0);">  
                <!-- <form class="form-horizontal form-material" id="loginform" action="index.html"> -->
                    <?php echo form_open('login/do_login','class="form-horizontal form-material"'); ?>   

                    <a href="javascript:void(0)" class="text-center db">
                        <img src="<?php echo base_url() ?>assets/plugins/images/new_logo.png" alt="Home" />
                    </a>   

                    <?php if(!empty($error)){ ?> 
                    <div class="alert alert-danger alert-dismissable" id="notif">  
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?= $error ?>   
                    </div>  
                    <?php } ?> 
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">   
                            <input onfocus="this.removeAttribute('readonly');" autocomplete="off" class="form-control" type="text" required="" placeholder="Username" name="username" style="background-image: linear-gradient(green, green), linear-gradient(rgba(120, 130, 140, 0.13), rgba(120, 130, 140, 0.13)) !important;color: black"> </div>    
                    </div>   
                    <div class="form-group">    
                        <div class="col-xs-12">  
                            <input onfocus="this.removeAttribute('readonly');" autocomplete="off" class="form-control" type="password" required="" placeholder="Password" name="password" style="background-image: linear-gradient(green, green), linear-gradient(rgba(120, 130, 140, 0.13), rgba(120, 130, 140, 0.13)) !important;"> </div>
                    </div>

                    <div class="form-group">    
                        <div class="col-xs-12">  
                            <select class="form-control" id="branch" name="branch">
                                <option value="" disabled selected>Pilih Branch</option>
                                <?php
                                $list_kelompoktindakan = $this->Login_model->getBranchList();
                                foreach($list_kelompoktindakan as $list){
                                    echo "<option value=".$list->id_branch.">".$list->nama."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox"> 
                                <label for="checkbox-signup" style="color: black !important"> Ingatkan Saya </label> 
                            </div>  
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">     
                            <button class="btn btn-success btn-lg btn-block text-uppercase" type="submit">Masuk</button>  
                        </div>
                    </div>
                </form>
                    <?php echo form_close();?>
                <!-- </form>  -->
            </div>
        </div>
    </section>
    <!-- jQuery --> 
    <script src="<?php echo base_url() ?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() ?>assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="<?php echo base_url() ?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script> 
    $(document).ready(function(){        
        $('.login-register').css('background-image','url("<?php echo base_url()?>assets/plugins/images/ibu_anak.png")'); 
        $("#notif").fadeTo(1100, 500).slideUp(1000, function(){
            $("#notif").hide(); 
        });       
    });          
    </script>
</body>

</html>