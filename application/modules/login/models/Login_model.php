<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
    public function _constract(){
        parent::__construct();
    }

    public function getBranchList() {
        $this->db->select('id_branch,nama');
        $this->db->from('m_branch');
        $query = $this->db->get();
        return $query->result();
    }

    public function getNameBranch($id) {
        $this->db->select('id_branch,nama');
        $this->db->from('m_branch');
        $this->db->where('id_branch',$id);
        $query = $this->db->get();
        $ret = $query->row();
        return $ret->nama;
    }

    public function getPrefixBranch($id) {
        $this->db->select('id_branch,prefix');
        $this->db->from('m_branch');
        $this->db->where('id_branch',$id);
        $query = $this->db->get();
        $ret = $query->row();
        return $ret->prefix;
    }
    
    public function getStatusCheckin($id,$tanggal) {
        $this->db->select('status');
        $this->db->from('t_absen');
        $this->db->where('id_user',$id);
        $this->db->like('absen_date',$tanggal);
        $this->db->order_by("absen_date", "desc");
        $query = $this->db->get();
        $ret = $query->row();
        if ($ret) {
            return $ret->status;
        }
        else {
            return "BELUM CHECKIN";
        }
        
    }
}

