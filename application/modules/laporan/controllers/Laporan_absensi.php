<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_absensi extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Laporan_absensi_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('laporan_absensi_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "laporan_absensi_view";
        $comments = "Listing User";
        $this->aauth->logit($perms, current_url(), $comments);
        // $this->load->view('v_laporan_rm', $this->data);
        $daftar_klinik = $this->Laporan_absensi_model->get_klinik();
        $this->data['daftar_klinik'] = $daftar_klinik;


//    die();

         $this->load->view('v_laporan_absensi', $this->data);
    }

    public function ajax_list_laporan_absensi(){
        $tgl_awal         = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir',TRUE);

        $list = $this->Laporan_absensi_model->get_absensi_list($tgl_awal, $tgl_akhir);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $rj->nama_lengkap;
            $row[] = $rj->nama;
            $row[] = $rj->to_do;
            $row[] = $rj->absen_date;
            $row[] = $rj->status;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_absensi_model->count_absensi_all($tgl_awal, $tgl_akhir),
                    "recordsFiltered" => $this->Laporan_absensi_model->count_absensi_all($tgl_awal, $tgl_akhir),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_list_laporan_absensi_klinik(){
        $tgl_awal         = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir',TRUE);
        $nama_klinik = $this->input->get('nama_klinik',TRUE);
        $list = $this->Laporan_absensi_model->get_absensi_list($tgl_awal, $tgl_akhir);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $rj->nama_lengkap;
            $row[] = $rj->nama;
            $row[] = $rj->to_do;
            $row[] = $rj->absen_date;
            $row[] = $rj->status;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_absensi_model->count_absensi_all($tgl_awal, $tgl_akhir),
                    "recordsFiltered" => $this->Laporan_absensi_model->count_absensi_all($tgl_awal, $tgl_akhir),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function autocomplete_users(){
        $request = $this->input->post('keyword');
        $list = $this->Laporan_absensi_model->get_users_autocomplete($request);
        $users = array();
        $l = array();
        foreach($list as $rj){
            $l["value"] = $rj->nama_lengkap;
            $l["id_user"] = $rj->id;
            $users[] = $l;
        }
        $res = array(
                    'success' => true,
                    'messages' => "users ditemukan",
                    'data' => $users
                    );
        echo json_encode($res);
    }
    public function ajax_get_list_absen_user(){
        $id_user = $this->input->get('id_user');
        $tgl_awal         = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir',TRUE);

        $list = $this->Laporan_absensi_model->get_list_absen_user($tgl_awal, $tgl_akhir,$id_user);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $rj->nama_lengkap;
            $row[] = $rj->nama;
            $row[] = $rj->to_do;
            $row[] = $rj->absen_date;
            $row[] = $rj->status;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_absensi_model->count_absensi_all($tgl_awal, $tgl_akhir),
                    "recordsFiltered" => $this->Laporan_absensi_model->count_absensi_all($tgl_awal, $tgl_akhir),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    function export_absensi_laporan_perorangan($tgl_awal, $tgl_akhir){
        $tgl_awal = str_replace("%20"," ",$tgl_awal);
        $tgl_akhir = str_replace("%20"," ",$tgl_akhir);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Laporan Absensi Perorangan");
         $object->getActiveSheet()
                        ->getStyle("A3:F3")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A3:F3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $object->getActiveSheet()
                                ->mergeCells('A1:F1')
                                ->setCellValueByColumnAndRow(0,1,"Laporan absensi perorangan dari tanggal $tgl_awal - $tgl_akhir");

        $table_columns = array("NO", "NAMA","BRANCH","TODO","TANGGAL","STATUS");
        $column = 0;
        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }

        $absensi_perorangan = $this->Laporan_absensi_model->get_absensi_list($tgl_awal, $tgl_akhir);
        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($absensi_perorangan as $row){
            $no++;
            $object->getActiveSheet()
                                    ->setCellValueByColumnAndRow(0, $excel_row, $no)
                                    ->setCellValueByColumnAndRow(1, $excel_row, $row->nama_lengkap)
                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->nama)
                                    ->setCellValueByColumnAndRow(3, $excel_row, $row->to_do)
                                    ->setCellValueByColumnAndRow(4, $excel_row, $row->absen_date)
                                    ->setCellValueByColumnAndRow(5, $excel_row, $row->status);
            $excel_row++;
        }
        foreach(range('A','F') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $datenow = date('d/m/Y');
        $object->getActiveSheet()->getStyle('A3:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Absensi Perorangan '.$tgl_awal.' - '.$tgl_akhir.'.xlsx"');
        $object_writer->save('php://output');
    }
}
