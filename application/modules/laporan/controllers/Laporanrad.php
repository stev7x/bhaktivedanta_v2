<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanrad extends CI_Controller{
	public $data = array();

	public function __construct(){
		parent::__construct();

        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Laporanrad_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
				// data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
	}

	public function index(){
        $is_permit = $this->aauth->control_no_redirect('laporan_rad_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "laporan_rad_view";
        $comments = "Laporan Radiologi.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_laporan_rad', $this->data);
	}

	public function ajax_list_radiologi(){
		$tgl_awal 	= $this->input->get('tgl_awal');
		$tgl_akhir 	= $this->input->get('tgl_akhir');
		$list 		= $this->Laporanrad_model->get_radiologi_datatables($tgl_awal,$tgl_akhir);
		$data 		= array();
        $no 		= isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $radiologi){
        	$no++;
        	$row = array();
        	$row[] = date("d-m-Y", strtotime($radiologi->tgl_tindakan));
        	$row[] = $no;
        	$row[] = $radiologi->pasien_nama;
        	$row[] = $radiologi->no_rekam_medis;
        	$row[] = $radiologi->ruanganrad;
        	$row[] = date("d-m-Y", strtotime($radiologi->tgl_tindakan));
        	$row[] = $radiologi->daftartindakan_nama;
        	$row[] = $radiologi->dokterpengirim_nama;
        	$row[] = $radiologi->is_cyto == 0 ? "Biasa" : "Cito";
        	$row[] = $radiologi->petugas;
        	$row[] = ""; #Hasil Tanggal
        	$row[] = $radiologi->dokterrad_nama;
        	$row[] = number_format($radiologi->total_harga);
        	$row[] = ""; #Ket.
        	$data[] = $row;
        }

        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporanrad_model->count_radiologi_all($tgl_awal, $tgl_akhir),
                    "recordsFiltered" => $this->Laporanrad_model->count_radiologi_all($tgl_awal, $tgl_akhir),
                    "data" => $data,
                    );

        echo json_encode($output);
	}

    function export_excel_radiologi($tgl_awal,$tgl_akhir){
        if ($tgl_awal == "undefined"){ $tgl_awal = ""; } else { $tgl_awal = str_replace("%20"," ",$tgl_awal); }
        if ($tgl_akhir == "undefined"){ $tgl_akhir = ""; } else { $tgl_akhir = str_replace("%20"," ",$tgl_akhir); }

        $listradexport = $this->Laporanrad_model->get_export_radiologi($tgl_awal, $tgl_akhir);

        $fname="Laporan_Radiologi_".date('d_M_Y_H:i:s').".xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$fname");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo'<table border="0" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr><td colspan=5><font size=4><b>Laporan Radiologi</b></font></td></tr>';
        echo'<tr><td>Periode </td><td colspan=4> : '.$tgl_awal." - ".$tgl_akhir.'</td></tr>';
        echo'<tr><td></td></tr>';
        echo '</table>';

        echo'<table border="1" bordercolor="#333333" cellspacing="0" cellpadding="0">';
		echo'<tr>';
		echo'<th rowspan="2">Tanggal</th>';
		echo'<th rowspan="2">No</th>';
		echo'<th rowspan="2">Nama Pasien</th>';
		echo'<th rowspan="2">RM</th>';
		echo'<th rowspan="2">Ruang</th>';
		echo'<th colspan="2">Permintaan</th>';
		echo'<th rowspan="2">Pengirim</th>';
		echo'<th colspan="3">Pengerjaan</th>';
		echo'<th colspan="2">Hasil</th>';
		echo'<th rowspan="2">Ket.</th>';
		echo'</tr>';
		echo'<tr>';
		echo'<th>Tanggal</th>';
		echo'<th>Tindakan</th>';
		echo'<th>Cito/Biasa</th>';
		echo'<th>Petugas</th>';
		echo'<th>Tanggal</th>';
		echo'<th>Dokter Sp.RAD</th>';
		echo'<th>Tarif</th>';
		echo'</tr>';

        if(count($listradexport)>0){
            $i=0;
            $total_harga = 0;
            while($i<count($listradexport)){
                $no = $i+1;
                $tgl_tindakan = !empty($listradexport[$i]->tgl_tindakan) ? date('d-M-Y', strtotime($listradexport[$i]->tgl_tindakan)) : '-';
                $iscyto = $listradexport[$i]->is_cyto == 0 ? "Biasa" : "Cito";
                $harga = number_format($listradexport[$i]->total_harga_tindakan);
                echo'<tr>';
                echo "<td align='left' style='text-align:left;'>".$tgl_tindakan."</td>";
                echo "<td align='left' style='text-align:left;'>".$no."</td>";
                echo "<td align='left' style='text-align:left;'>".$listradexport[$i]->pasien_nama."</td>";
                echo "<td align='left' style='text-align:left;'>".$listradexport[$i]->no_rekam_medis."</td>";
                echo "<td align='left' style='text-align:left;'>".$listradexport[$i]->ruanganrad."</td>";
                echo "<td align='left' style='text-align:left;'>".$listradexport[$i]->tgl_tindakan."</td>";
                echo "<td align='left' style='text-align:left;'>".$listradexport[$i]->daftartindakan_nama."</td>";
                echo "<td align='left' style='text-align:left;'>".$listradexport[$i]->dokterpengirim_nama."</td>";
                echo "<td align='left' style='text-align:left;'>".$iscyto."</td>";
                echo "<td align='left' style='text-align:left;'>".$listradexport[$i]->petugas."</td>";
                echo "<td align='left' style='text-align:left;'>ini belum ada tgl di dbnya</td>";
                echo "<td align='left' style='text-align:left;'>".$listradexport[$i]->dokterrad_nama."</td>";
                echo "<td align='right' style='text-align:right;'>"."Rp 	".$listradexport[$i]->total_harga.""."</td>";
                echo'</tr>';
                $total = $listradexport[$i]->total_harga++;
                $i++;
            }
            echo "<tr bgcolor=#ffff00>";
            echo "<td align='center' style='text-align:center;' colspan='12'><b>TOTAL</b></td>";
            echo "<td align='right' style='text-align:right;'><b>"."Rp 	".number_format($total).""."</b></td>";
            echo "<td></td>";
            echo "</tr>";
        }else{
            echo'<tr>';
            echo "<td align='center' style='text-align:center;' colspan='16'>No Data</td>";
            echo'</tr>';
        }

		echo'</table><br>';
        echo 'print date : '.date('d-M-Y H:i:s');
    }
}

?>
