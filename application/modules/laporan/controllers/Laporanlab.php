<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanlab extends CI_Controller{

	public $data = array();

	public function __construct(){
		parent::__construct();

        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Laporanlab_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
				// data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
	}

	public function index(){
        $is_permit = $this->aauth->control_no_redirect('laporan_lab_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }
        // if permitted, do logit
        $perms = "laporan_lab_view";
        $comments = "Laporan Laboratorium.";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_laporan_lab', $this->data);
	}

	public function ajax_list_laboratorium(){
		$tgl_awal 	= $this->input->get('tgl_awal');
		$tgl_akhir 	= $this->input->get('tgl_akhir');
		$list_lab = $this->Laporanlab_model->get_data_laboratorium($tgl_awal, $tgl_akhir);
		$count_list_lab = $this->Laporanlab_model->count_get_data_laboratorium($tgl_awal, $tgl_akhir);

		$row = array();
		$no = $this->input->get('start',TRUE);
		foreach($list_lab as $list){
			$no++;

			$row[] = array(
				$no,
				date('d/m/Y', strtotime($list->tgl_tindakan)),
				$list->pasien_nama,
				$list->no_rekam_medis,
				$list->dokterpengirim_nama,
				$list->daftartindakan_nama,
				number_format($list->harga_tindakan, 2, ',', '.')
			);
		}

		$output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $count_list_lab,
            "recordsFiltered" => $count_list_lab,
            "data" => $row,
        );

        echo json_encode($output);
	}

	public function export_excel_laboratorium($tgl_awal, $tgl_akhir){
		if ($tgl_awal == "undefined"){ $tgl_awal = ""; } else { $tgl_awal = str_replace("%20"," ",$tgl_awal); }
        if ($tgl_akhir == "undefined"){ $tgl_akhir = ""; } else { $tgl_akhir = str_replace("%20"," ",$tgl_akhir); }

        $listlabexport = $this->Laporanlab_model->get_export_laboratorium($tgl_awal, $tgl_akhir);

		$fname="ReportLaboratorium_".date('d_M_Y_H:i:s').".xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$fname");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo'<table border="0" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr><td colspan=5><font size=4><b>Report Laboratorium</b></font></td></tr>';
        if($start != "" && $end != ""){
            echo'<tr><td>Periode </td><td colspan=4> : '.date('d/m/Y',strtotime($start)).' - '.date('d/m/Y',strtotime($end)).'</td></tr>';
        }
        echo '</table><br><br>';

        echo'<table border="1" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr bgcolor=#b7b7b7>';
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Tgl Sample</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Nama</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>RM</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Pengirim</td>";
        echo "<td style='font-weight:bolder;text-align:center;' colspan='2'>Pemeriksaan</td>";
        echo'</tr>';
        echo'<tr bgcolor=#b7b7b7>';
        echo "<td style='font-weight:bolder;text-align:center;'>Tarif</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Tindakan</td>";
        echo'</tr>';

        if(count($list_lab)>0){
            $i=0;
            while($i<count($list_lab)){
                echo'<tr>';

				$tgl_tindakan = date('d/m/Y', strtotime($list_lab[$i]->tgl_tindakan));
                echo "<td style='text-align:left;'>".$tgl_tindakan."</td>";
                echo "<td style='text-align:left;'>".$list_lab[$i]->pasien_nama."</td>";
                echo "<td style='text-align:left;'>'".$list_lab[$i]->no_rekam_medis."'</td>";
                echo "<td style='text-align:left;'>".$list_lab[$i]->dokterpengirim_nama."</td>";
                echo "<td style='text-align:left;'>".$list_lab[$i]->daftartindakan_nama."</td>";

				$harga_tindakan = number_format($list_lab[$i]->harga_tindakan, 2, ',', '.');
                echo "<td style='text-align:left;'>".$harga_tindakan."</td>";

                echo'</tr>';
                $i++;
            }
        }else{
            echo'<tr>';
            echo "<td style='text-align:center;' colspan='6'>No Data</td>";
            echo'</tr>';
        }

        echo'</table><br>';
        echo 'print date : '.date('d-M-Y H:i:s');
	}

}
/* End of file Laporanlab.php */
