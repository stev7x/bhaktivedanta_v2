<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_igd extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Laporan_igd_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('laporan_igd_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "laporan_igd_view";
        $comments = "Laporan IGD";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_laporan_igd', $this->data);
    }

    public function ajax_list_laporan_igd(){
        $tahun = $this->input->get('tahun',TRUE);
        $bulan = $this->input->get('bulan',TRUE);

        $list = $this->Laporan_igd_model->get_laporan($tahun, $bulan);
        // $list = $this->Laporan_igd_model->get_laporan();
        $data = array();
        $no = 0;
        foreach($list as $list_igd){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list_igd->priode_bulan;
            $row[] = $list_igd->diterima;
            $row[] = $list_igd->ditolak;

            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_igd_model->count_all_list($tahun, $bulan),
                    "recordsFiltered" => $this->Laporan_igd_model->count_all_list_filtered($tahun, $bulan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function exportToExcel($tgl_awal,$tgl_akhir){
        if ($tgl_awal == "undefined"){ $tgl_awal = ""; } else { $tgl_awal = str_replace("%20"," ",$tgl_awal); }
        if ($tgl_akhir == "undefined"){ $tgl_akhir = ""; } else { $tgl_akhir = str_replace("%20"," ",$tgl_akhir); }
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Laporan IGD");
        $object->getActiveSheet()
                        ->getStyle("A1:D1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:D1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $table_columns = array("No", "Periode Bulan", "Diterima", "Ditolak");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Laporan_igd_model->get_data($tgl_awal,$tgl_akhir);

        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $no);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->priode_bulan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->diterima);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->ditolak);
            $excel_row++;
        }

        foreach(range('A','D') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan IGD.xlsx"');
        $object_writer->save('php://output');
    }

}


?>