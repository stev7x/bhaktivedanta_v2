<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_rm extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Laporan_rm_model');
        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('laporan_rm_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "laporan_rm_view";
        $comments = "Listing User";
        $this->aauth->logit($perms, current_url(), $comments);
        // $this->load->view('v_laporan_rm', $this->data);



//    die();

         $this->load->view('v_laporan_rm', $this->data);
    }

    public function ajax_list_rawat_jalan(){
        $tgl_awal         = $this->input->get('tgl_awal_rj',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir_rj',TRUE);
        $status_kunjungan = $this->input->get('status_kunjungan',TRUE);
        $rm_awal = $this->input->get('rm_awal',TRUE);
        $rm_akhir = $this->input->get('rm_akhir',TRUE);
        // print_r($status_kunjungan);die();
        $list = $this->Laporan_rm_model->get_rawat_jalan_list($tgl_awal, $tgl_akhir, $status_kunjungan, $rm_awal, $rm_akhir);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date("d-m-Y",strtotime($rj->tgl_pendaftaran));
            $row[] = $rj->pasien_nama;
            $row[] = $rj->no_rekam_medis;
            $row[] = $rj->type_pembayaran;
            $row[] = (($rj->nama_perujuk !='') ? 'Ada': 'Tidak');
            $row[] = $rj->nama_poliruangan;
            $row[] = $rj->NAME_DOKTER;
            $row[] = $rj->status_kunjungan;
            $row[] = $rj->nama_diagnosa != '' ? $rj->nama_diagnosa : $rj->status_periksa;
            $row[] = $rj->kode_diagnosa;
            $row[] = $rj->carapulang;
            $row[] = $rj->petugas_rm;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_rm_model->count_rawat_jalan_all($tgl_awal, $tgl_akhir, $status_kunjungan),
                    "recordsFiltered" => $this->Laporan_rm_model->count_rawat_jalan_all($tgl_awal, $tgl_akhir, $status_kunjungan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_poli_rinci(){
        $tgl_awal         = $this->input->get('tgl_awal_rj',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir_rj',TRUE);
        $filterBy           = $this->input->get('filterBy',TRUE);
        $list = $this->Laporan_rm_model->getDataPoliUmumRinci($tgl_awal, $tgl_akhir, $filterBy);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date("d-m-Y",strtotime($rj->TANGGAL));
            $row[] = $rj->POLI;
            $row[] = $rj->DOKTER_SPESIALIS;
            $row[] = $rj->NAMA_PASIEN;
            $row[] = $rj->DIAGNOSA;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_rm_model->count_poli_rinci_list($tgl_awal, $tgl_akhir, $filterBy),
                    "recordsFiltered" => $this->Laporan_rm_model->count_poli_rinci_list($tgl_awal, $tgl_akhir, $filterBy),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_poli_keseluruhan(){
        $tgl_awal         = $this->input->get('tgl_awal_rj',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir_rj',TRUE);
        $filterBy           = $this->input->get('filterBy',TRUE);
        // print_r($status_kunjungan);die();
        $list = $this->Laporan_rm_model->get_poli_keseluruhan_list($tgl_awal, $tgl_akhir, $filterBy);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date("d-m-Y",strtotime($rj->TANGGAL));
            $row[] = $rj->POLI;
            $row[] = $rj->NAMA_DOKTER;
            $row[] = $rj->JUMLAH;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_rm_model->count_poli_keseluruhan_list($tgl_awal, $tgl_akhir, $filterBy),
                    "recordsFiltered" => $this->Laporan_rm_model->count_poli_keseluruhan_list($tgl_awal, $tgl_akhir, $filterBy),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_rm_rj(){
        $tgl_awal         = $this->input->get('tgl_awal_rj',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir_rj',TRUE);
        // $filterBy           = $this->input->get('filterBy',TRUE);
        // print_r($status_kunjungan);die();
        $list = $this->Laporan_rm_model->get_rm_rj_list($tgl_awal, $tgl_akhir);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rj){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date("d-m-Y",strtotime($rj->tgl_pendaftaran));
            $row[] = $rj->no_rekam_medis;
            $row[] = $rj->pasien_nama;
            $row[] = $rj->nama_poliruangan;
            $row[] = $rj->NAME_DOKTER;
            $row[] = $rj->type_pembayaran;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_rm_model->count_rm_rj_list($tgl_awal, $tgl_akhir),
                    "recordsFiltered" => $this->Laporan_rm_model->count_rm_rj_list($tgl_awal, $tgl_akhir),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_list_pendaftaran_poli(){
        $tgl_awal         = $this->input->get('tgl_awal_rj',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir_rj',TRUE);
        // $filterBy           = $this->input->get('filterBy',TRUE);
        // print_r($status_kunjungan);die();
        $list = $this->Laporan_rm_model->getDataPendaftaranPoli($tgl_awal, $tgl_akhir);
        // echo "<pre>";
        // print_r($list);
        // echo "</pre>";
        // die();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rj){
            $cek = $rj->no_rekam_medis;
            if($cek!= null){

                $no++;
                $row = array();
                $row[] = $no;
                $row[] = date("d-m-Y",strtotime($rj->tgl_pendaftaran));
                $row[] = $rj->no_rekam_medis;
                $row[] = $rj->pasien_nama;
                $row[] = $rj->umur;
                $row[] = $rj->jenis_kelamin;
                $row[] = $rj->jenis_kelamin;
                $row[] = $rj->NAME_DOKTER;
                $data[] = $row;
            }
        }
        $output = array(
                "draw" => $this->input->get('draw'),
                "recordsTotal" => $this->Laporan_rm_model->countDataPendaftaranPoliAll($tgl_awal, $tgl_akhir),
                "recordsFiltered" => $this->Laporan_rm_model->countDataPendaftaranPoliAll($tgl_awal, $tgl_akhir),
                "data" => $data,
            );
            //output to json format
        echo json_encode($output);
    }


    public function ajax_list_laporan_dinkes(){
        $tgl_awal         = $this->input->get('tgl_awal_rj',TRUE);
        $tgl_akhir        = $this->input->get('tgl_akhir_rj',TRUE);
        // $filterBy           = $this->input->get('filterBy',TRUE);
        // print_r($status_kunjungan);die();
        $list = $this->Laporan_rm_model->getDataLapDinkes($tgl_awal, $tgl_akhir);
        // echo "<pre>";
        // print_r($list);
        // echo "</pre>";
        // die();
            $data = array();
            $no = isset($_GET['start']) ? $_GET['start'] : 0;
            foreach($list as $rj){
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = date("d-m-Y",strtotime($rj->tgl_pendaftaran));
                $row[] = $rj->no_pendaftaran;
                $row[] = $rj->pasien_nama;
                $row[] = $rj->umur;
                $row[] = $rj->askep;
                $row[] = $rj->pasien_alamat;
                $row[] = $rj->pasien_alamat;
                $data[] = $row;
            }
            $output = array(
                        "draw" => $this->input->get('draw'),
                        "recordsTotal" => $this->Laporan_rm_model->countDataLapDinkes($tgl_awal, $tgl_akhir),
                        "recordsFiltered" => $this->Laporan_rm_model->countDataLapDinkes($tgl_awal, $tgl_akhir),
                        "data" => $data,
                        );
            //output to json format
            echo json_encode($output);
    }

    public function ajax_list_rawat_darurat(){
        $tgl_awal = $this->input->get('tgl_awal_rd',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir_rd',TRUE);
        $status_kunjungan = $this->input->get('status_kunjungan',TRUE);
        $list = $this->Laporan_rm_model->get_rawat_darurat_list($tgl_awal, $tgl_akhir,$status_kunjungan);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $rd){
            $no++;
            $row = array();
            $row[] = date("m",strtotime($rd->tgl_pendaftaran));
            $row[] = date("d",strtotime($rd->tgl_pendaftaran));
            $row[] = date("d-m-Y",strtotime($rd->tgl_pendaftaran));
            $row[] = $rd->no_rekam_medis;
            $row[] = $rd->pasien_nama;
            $row[] = $rd->pasien_alamat;
            $row[] = $rd->jenis_kelamin;
            $row[] = $rd->umur;
            $row[] = $rd->NAME_DOKTER;
            $row[] = $rd->nama_diagnosa != '' ? $rd->nama_diagnosa : $rd->status_periksa;
            $row[] = $rd->kode_diagnosa;
            $row[] = $rd->status_kunjungan;
            $row[] = $rd->carapulang;
            $row[] = $rd->type_pembayaran;
            $row[] = $rd->petugas_rm;
             //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_rm_model->count_rawat_darurat_all($tgl_awal, $tgl_akhir,$status_kunjungan),
                    "recordsFiltered" => $this->Laporan_rm_model->count_rawat_darurat_all($tgl_awal, $tgl_akhir,$status_kunjungan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_list_rawat_inap(){
        $tgl_awal = $this->input->get('tgl_awal_ri',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir_ri',TRUE);
        $dokter_pjri = $this->input->get('dokter_pj_ri',TRUE);
        $pasien = $this->input->get('pasien_ri',TRUE);
        $list = $this->Laporan_rm_model->get_rawat_inap_list($tgl_awal, $tgl_akhir,$dokter_pjri,$pasien);
        $data = array();
        $i = 0;
        foreach($list as $ri){
            $no = $i++;
            $row = array();
            $row[] = $no;
            $row[] = $ri->no_rekam_medis;
            $row[] = $ri->pasien_nama;
            $row[] = $ri->jenis_kelamin;
            $row[] = $ri->tanggal_lahir;
            $row[] = $ri->umur;
            $row[] = $ri->pasien_alamat;
            $row[] = $ri->poli_ruangan;
            $row[] = $ri->no_kamar." / ".$ri->no_bed;
            $row[] = $ri->nama_diagnosa;
            $row[] = $ri->kode_diagnosa;
            $row[] = $ri->dokter_igd;
            $row[] = $ri->dokter_pj;
            $row[] = $ri->dokter_anastesi;
            $row[] = $ri->perawat;
            $row[] = $ri->tgl_masukadmisi;
            $row[] = $ri->tgl_pulang;
            $row[] = $ri->lama_rawat_hari;
            $row[] = $ri->carapulang;
            $row[] = $ri->type_pembayaran;
            $row[] = $ri->petugas_rm;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_rm_model->count_rawat_inap_all($tgl_awal, $tgl_akhir, $dokter_pjri,$pasien),
                    "recordsFiltered" => $this->Laporan_rm_model->count_rawat_inap_all($tgl_awal, $tgl_akhir, $dokter_pjri,$pasien),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }
    // public function ajax_list_rawat_darurat(){
    //     $tgl_awal = $this->input->get('tgl_awal_rd',TRUE);
    //     $tgl_akhir = $this->input->get('tgl_akhir_rd',TRUE);
    //     $status_kunjungan = $this->input->get('status_kunjungan',TRUE);
    //     $list = $this->Laporan_rm_model->get_rawat_darurat_list($tgl_awal, $tgl_akhir,$status_kunjungan);
    //     $data = array();
    //     $no = isset($_GET['start']) ? $_GET['start'] : 0;
    //     foreach($list as $rd){
    //         $no++;
    //         $row = array();
    //         $row[] = date("m",strtotime($rd->tgl_pendaftaran));
    //         $row[] = date("d",strtotime($rd->tgl_pendaftaran));
    //         $row[] = date("d-m-Y",strtotime($rd->tgl_pendaftaran));
    //         $row[] = $rd->no_rekam_medis;
    //         $row[] = $rd->pasien_nama;
    //         $row[] = $rd->pasien_alamat;
    //         $row[] = $rd->jenis_kelamin;
    //         $row[] = $rd->umur;
    //         $row[] = $rd->NAME_DOKTER;
    //         $row[] = $rd->nama_diagnosa != '' ? $rd->nama_diagnosa : $rd->status_periksa;
    //         $row[] = $rd->kode_diagnosa;
    //         $row[] = $rd->status_kunjungan;
    //         $row[] = $rd->carapulang;
    //         $row[] = $rd->type_pembayaran;
    //         $row[] = $rd->petugas_rm;
    //          //add html for action
    //         $data[] = $row;
    //     }
    //     $output = array(
    //                 "draw" => $this->input->get('draw'),
    //                 "recordsTotal" => $this->Laporan_rm_model->count_rawat_darurat_all($tgl_awal, $tgl_akhir,$status_kunjungan),
    //                 "recordsFiltered" => $this->Laporan_rm_model->count_rawat_darurat_all($tgl_awal, $tgl_akhir,$status_kunjungan),
    //                 "data" => $data,
    //                 );
    //     //output to json format
    //     echo json_encode($output);
    // }

    public function ajax_list_poli_obgyn(){
        $tgl_awal = $this->input->get('tgl_awal_obgyn',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir_obgyn',TRUE);
        $list = $this->Laporan_rm_model->getDataBidan($tgl_awal, $tgl_akhir);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $ri){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ri->tgl_pendaftaran;
            $row[] = $ri->no_rekam_medis;
            $row[] = $ri->nama_poliruangan;
            $row[] = $ri->pasien_nama;
            $row[] = $ri->jenis_kelamin;
            $row[] = date('d F Y', strtotime($ri->tanggal_lahir));
            $row[] = $ri->umur;
            $row[] = $ri->pasien_alamat;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_rm_model->countDataBidanAll($tgl_awal, $tgl_akhir),
                    "recordsFiltered" => $this->Laporan_rm_model->countDataBidanAll($tgl_awal, $tgl_akhir),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_hasil_vaksinasi(){
        $tgl_awal = $this->input->get('tgl_awal_rj',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir_rj',TRUE);
        $tipe = 1;
        $list = $this->Laporan_rm_model->getHasilVaksinasi($tgl_awal, $tgl_akhir, $tipe);
        // echo "<pre>";
        // print_r($list);
        // echo "</pre>";
        // die();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $ri){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d M Y', strtotime($ri->tgl_pendaftaran));
            $row[] = $ri->pasien_nama;
            $row[] = date('d F Y', strtotime($ri->tanggal_lahir));
            $row[] = $ri->pasien_alamat;
            $row[] = $ri->kelurahan_nama;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Laporan_rm_model->countHasilVaksinasi($tgl_awal, $tgl_akhir, $tipe),
                    "recordsFiltered" => $this->Laporan_rm_model->countHasilVaksinasi($tgl_awal, $tgl_akhir, $tipe),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function export_excel_rj($tgl_awal,$tgl_akhir,$poliklinik, $rm_awal, $rm_akhir){
        if ($tgl_awal == "undefined"){ $tgl_awal = ""; } else { $tgl_awal = str_replace("%20"," ",$tgl_awal); }
        if ($tgl_akhir == "undefined"){ $tgl_akhir = ""; } else { $tgl_akhir = str_replace("%20"," ",$tgl_akhir); }
        if ($poliklinik == "undefined"){ $poliklinik = ""; }

        $listrjexport = $this->Laporan_rm_model->get_export_rj($tgl_awal, $tgl_akhir, $poliklinik, $rm_awal, $rm_akhir);

        $fname="LaporanRM_RawatJalan_".date('d_M_Y_H:i:s').".xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$fname");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo'<table border="0" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr><td colspan=5><font size=4><b>Rekapitulasi Kunjungan Poliklinik (Rawat Jalan)</b></font></td></tr>';
        echo'<tr><td>Periode </td><td colspan=4> : '.$tgl_awal." - ".$tgl_akhir.'</td></tr>';
        if($poliklinik!=""){
            echo'<tr><td>Poliklinik </td><td colspan=4> : '.$poliklinik.'</td></tr>';
        }
        echo '</table><br><br>';

        echo'<table border="1" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr bgcolor=#b7b7b7>';
        echo "<td style='font-weight:bolder;text-align:center;'>No</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Tanggal</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Nama Pasien</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>No. RM</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Pembiayaan</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Rujukan</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Poliklinik</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Dokter</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Status Kunjungan</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Diagnosa</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Kode ICD 10</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Terapi</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Petugas RM</td>";
        echo'</tr>';

        if(count($listrjexport)>0){
            $i=0;
            while($i<count($listrjexport)){
                $no = $i+1;
                echo'<tr>';
                echo "<td style='text-align:left;'>".$no."</td>";
                $tgl_pendaftaran = !empty($listrjexport[$i]->tgl_pendaftaran) ? date('d-M-Y', strtotime($listrjexport[$i]->tgl_pendaftaran)) : '-';
                echo "<td style='text-align:left;'>".$tgl_pendaftaran."</td>";
                echo "<td style='text-align:left;'>".$listrjexport[$i]->pasien_nama."</td>";
                echo "<td style='text-align:left;'>"."'".$listrjexport[$i]->no_rekam_medis."'"."</td>";
                echo "<td style='text-align:left;'>".$listrjexport[$i]->type_pembayaran."</td>";
                $perujuk = !empty($listrjexport[$i]->nama_perujuk) ? 'Ada' : 'Tidak';
                echo "<td style='text-align:left;'>".$perujuk."</td>";
                echo "<td style='text-align:left;'>".$listrjexport[$i]->nama_poliruangan."</td>";
                echo "<td style='text-align:right;'>".$listrjexport[$i]->NAME_DOKTER."</td>";
                $diagnosa = !empty($listrjexport[$i]->nama_diagnosa) ? $listrjexport[$i]->nama_diagnosa : $listrjexport[$i]->status_periksa;
                echo "<td style='text-align:left;'>".$listrjexport[$i]->status_kunjungan."</td>";
                echo "<td style='text-align:left;'>".$diagnosa."</td>";
                echo "<td style='text-align:left;'>".$listrjexport[$i]->kode_diagnosa."</td>";
                echo "<td style='text-align:left;'>".$listrjexport[$i]->carapulang."</td>";
                echo "<td style='text-align:left;'>".$listrjexport[$i]->petugas_rm."</td>";
                echo'</tr>';
                $i++;
            }
        }else{
            echo'<tr>';
            echo "<td style='text-align:center;' colspan='12'>No Data</td>";
            echo'</tr>';
        }

        echo'</table><br>';
        echo 'print date : '.date('d-M-Y H:i:s');
    }
    function export_excel_rd($tgl_awal,$tgl_akhir){
        if ($tgl_awal == "undefined"){ $tgl_awal = ""; } else { $tgl_awal = str_replace("%20"," ",$tgl_awal); }
        if ($tgl_akhir == "undefined"){ $tgl_akhir = ""; } else { $tgl_akhir = str_replace("%20"," ",$tgl_akhir); }

        $listrdexport = $this->Laporan_rm_model->get_export_rd($tgl_awal, $tgl_akhir);

        $fname="LaporanRM_RawatDarurat_".date('d_M_Y_H:i:s').".xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$fname");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo'<table border="0" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr><td colspan=5><font size=4><b>Rekapitulasi Kunjungan Poliklinik (Rawat Darurat)</b></font></td></tr>';
        echo'<tr><td>Periode </td><td colspan=4> : '.$tgl_awal." - ".$tgl_akhir.'</td></tr>';
        echo '</table><br><br>';

        echo'<table border="1" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr bgcolor=#b7b7b7>';
        echo "<td style='font-weight:bolder;text-align:center;'>Nomor</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Nomor</td>";
        echo "<td style='font-weight:bolder;text-align:center:' rowspan='2'>Tgl Masuk</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>No RM</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Nama Pasien</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Alamat</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Gender</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Umur</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Dokter</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Diagnosa</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Kode ICD 10</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Kunjungan</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Keterangan</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Status Pembayaran</td>";
        echo "<td style='font-weight:bolder;text-align:center;' rowspan='2'>Petugas</td>";
        echo'</tr>';
        echo'<tr bgcolor=#b7b7b7>';
        echo "<td style='font-weight:bolder;text-align:center;'>Bulan</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Hari</td>";
        echo'</tr>';

        if(count($listrdexport)>0){
            $i=0;
            while($i<count($listrdexport)){
                $no = $i+1;
                echo'<tr>';
                $tgl_pendaftaran = !empty($listrdexport[$i]->tgl_pendaftaran) ? date('d-M-Y', strtotime($listrdexport[$i]->tgl_pendaftaran)) : '-';
                $bulan  = !empty($listrdexport[$i]->tgl_pendaftaran) ? date('m', strtotime($listrdexport[$i]->tgl_pendaftaran)) : '-';
                $hari   = !empty($listrdexport[$i]->tgl_pendaftaran) ? date('d', strtotime($listrdexport[$i]->tgl_pendaftaran)) : '-';
                echo "<td style='text-align:left;'>".$bulan."</td>";
                echo "<td style='text-align:left;'>".$hari."</td>";
                echo "<td style='text-align:left;'>".$tgl_pendaftaran."</td>";
                echo "<td style='text-align:left;'>"."'".$listrdexport[$i]->no_rekam_medis."'"."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->pasien_nama."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->pasien_alamat."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->jenis_kelamin."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->umur."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->NAME_DOKTER."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->nama_diagnosa."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->kode_diagnosa."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->status_kunjungan."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->carapulang."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->type_pembayaran."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->petugas_rm."</td>";
                echo'</tr>';
                $i++;
            }
        }else{
            echo'<tr>';
            echo "<td style='text-align:center;' colspan='15'>No Data</td>";
            echo'</tr>';
        }

        echo'</table><br>';
        echo 'print date : '.date('d-M-Y H:i:s');
    }

    function export_excel_ri($tgl_awal,$tgl_akhir,$dokter_pjri,$pasien){
        if ($tgl_awal == "undefined"){ $tgl_awal = ""; } else { $tgl_awal = str_replace("%20"," ",$tgl_awal); }
        if ($tgl_akhir == "undefined"){ $tgl_akhir = ""; } else { $tgl_akhir = str_replace("%20"," ",$tgl_akhir); }
        if ($dokter_pjri == "undefined"){ $dokter_pjri = ""; } else { $dokter_pjri = str_replace("%20"," ",$dokter); }
        if ($pasien == "undefined"){ $pasien = ""; } else { $pasien = str_replace("%20"," ",$pasien); }

        $listrdexport = $this->Laporan_rm_model->get_export_ri($tgl_awal, $tgl_akhir, $dokter_pjri,$pasien);

        $fname="LaporanRM_RawatInap_".date('d_M_Y_H:i:s').".xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$fname");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo'<table border="0" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr><td colspan=5><font size=4><b>Rekapitulasi Kunjungan Poliklinik (Rawat Inap)</b></font></td></tr>';
        echo'<tr><td>Tanggal Masuk </td><td colspan=4> : '.$tgl_awal.'</td></tr>';
        echo'<tr><td>Tanggal Keluar </td><td colspan=4> : '.$tgl_akhir.'</td></tr>';
        echo '</table><br><br>';

        echo'<table border="1" bordercolor="#333333" cellpadding="0" cellspacing="0">';
        echo'<tr bgcolor=#b7b7b7>';
        echo "<td style='font-weight:bolder;text-align:center;'>No</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>No. RM</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Nama Pasien</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Jenis Kelamin</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Tanggal Lahir</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Usia</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Alamat</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Ruang</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Kamar</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Diagnosa Keluar</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>ICD 10</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Dokter IGD</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Dokter Penanggungjawab</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Dokter Anastesi</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Perawat / Bidan</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Tanggal Masuk</td>";
        echo "<td style='font-weight:bolder;text-align:center:'>Tanggal Keluar</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Hari Perawatan<td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Cara Pulang</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Pembayaan Pasien</td>";
        echo "<td style='font-weight:bolder;text-align:center;'>Keterangan</td>";
        echo'</tr>';

        if(count($listrdexport)>0){
            $i=0;
            while($i<count($listrdexport)){
                $no = $i+1;
                echo'<tr>';
                $tgl_masuk = !empty($listrdexport[$i]->tgl_masukadmisi) ? date('d-M-Y', strtotime($listrdexport[$i]->tgl_masukadmisi)) : '-';
                $tgl_keluar = !empty($listrdexport[$i]->tgl_pulang) ? date('d-M-Y', strtotime($listrdexport[$i]->tgl_pulang)) : '-';
                echo "<td style='text-align:left;'>".$no."</td>";
                echo "<td style='text-align:left;'>"."'".$listrdexport[$i]->no_rekam_medis."'"."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->pasien_nama."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->jenis_kelamin."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->tanggal_lahir."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->umur."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->pasien_alamat."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->poli_ruangan."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->no_kamar."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->nama_diagnosa."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->kode_diagnosa."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->dokter_igd."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->dokter_pj."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->dokter_anastesi."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->perawat."</td>";
                echo "<td style='text-align:left;'>".$tgl_masuk."</td>";
                echo "<td style='text-align:left;'>".$tgl_keluar."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->lama_rawat_hari."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->carapulang."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->type_pembayaran."</td>";
                echo "<td style='text-align:left;'>".$listrdexport[$i]->petugas_rm."</td>";
                echo'</tr>';
                $i++;
            }
        }else{
            echo'<tr>';
            echo "<td style='text-align:center;' colspan='8'>No Data</td>";
            echo'</tr>';
        }

        echo'</table><br>';
        echo 'print date : '.date('d-M-Y H:i:s');
    }


//START EXPORT POLI RAJAL/obgyn
    function exportToExcelBidan($tgl_awal_obgyn, $tgl_akhir_obgyn){
        $tgl_awal_obgyn = str_replace("%20"," ",$tgl_awal_obgyn);
        $tgl_akhir_obgyn = str_replace("%20"," ",$tgl_akhir_obgyn);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("RAJAL");
         $object->getActiveSheet()
                        ->getStyle("A1:AS1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:AS1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("NO REG", "NO RM","NAMA","JK","NAMA ORTU","TGL LAHIR","UMUR","ALAMAT","KELURAHAN","TANGGAL PERIKSA","KATEGORI PX","BB (KG)","TB (CM)","SISTOLE","DIASTOLE","VAKSINASI NON HAMIL","VAKSINASI HAMIL","IMUN 1","IMUN 2","IMUN 3","IMUN 4","IMUN 5","KASUS","PRENATAL","PASCA PERSALIAN","PASCA KEGUGURAN","Gravida","Partus","Prematur","imatur","abortus","hidup","HPHT","HPL","USIA KEHAMILAN","TINDAKAN 1","TINDAKAN 2","TINDAKAN 3","DIRUJUK MRS","ASURANSI", "DOKTER PJ","ASISTEN","KETERANGAN (FAKTOR RESIKO YANG DITEMUKAN)","SKOR KSPR","DIAGNOSA");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $poli_obgyn = $this->Laporan_rm_model->getDataBidan($tgl_awal_obgyn, $tgl_akhir_obgyn);
    //    echo "<pre>";
    //    print_r($poli_obgyn);
    //    echo "</pre>";
        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($poli_obgyn as $row){
            $hariini = date('Y');
            $lahir = date('Y', strtotime($row->tanggal_lahir));
            $hitung = $hariini - $lahir;
            if($hitung <= 5){
                $kategori_px = "Balita";
            }else if($hitung >= 6 && $hitung <= 12){
                $kategori_px = "Anak - anak";
            }else if($hitung >= 13 && $hitung <= 17){
                $kategori_px = "Remaja";
            }else if($hitung >= 18 || $row->status_kawin == "Kawin"){
                $kategori_px = "Dewasa";
            }else{
                $kategori_px = " ";
            }

            if($row->jenis_kelamin == "Laki-laki"){
                $jk = "L";
            }else if($row->jenis_kelamin == "Perempuan"){
                $jk = "P";
            }else{
                $jk = "Tidak Valid";
            }



            // var_dump($row->create_by);die();
            $no++;
            $object->getActiveSheet()
                                    ->setCellValueByColumnAndRow(0, $excel_row, $row->no_pendaftaran)
                                    ->setCellValueByColumnAndRow(1, $excel_row, $row->no_rekam_medis)
                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->pasien_nama)
                                    ->setCellValueByColumnAndRow(3, $excel_row, $jk)
                                    ->setCellValueByColumnAndRow(4, $excel_row, $row->nama_orangtua)
                                    ->setCellValueByColumnAndRow(5, $excel_row, ($row->tanggal_lahir === null) ? '' : date('d/m/Y', strtotime($row->tanggal_lahir)))
                                    ->setCellValueByColumnAndRow(6, $excel_row, $row->umur)
                                    ->setCellValueByColumnAndRow(7, $excel_row, $row->pasien_alamat)
                                    ->setCellValueByColumnAndRow(8, $excel_row, $row->kelurahan_nama)
                                    ->setCellValueByColumnAndRow(9, $excel_row, ($row->tgl_pendaftaran === null) ? '' :date('d/m/Y', strtotime($row->tgl_pendaftaran)))
                                    ->setCellValueByColumnAndRow(10, $excel_row, $kategori_px) // px
                                    ->setCellValueByColumnAndRow(11, $excel_row, $row->bb)
                                    ->setCellValueByColumnAndRow(12, $excel_row, $row->tb)
                                    ->setCellValueByColumnAndRow(13, $excel_row, $row->sistole)
                                    ->setCellValueByColumnAndRow(14, $excel_row, $row->diastole)
                                    ->setCellValueByColumnAndRow(15, $excel_row, $row->vaksinasi_nonhamil)
                                    ->setCellValueByColumnAndRow(16, $excel_row, $row->vaksinasi_hamil)
                                    ->setCellValueByColumnAndRow(17, $excel_row, $row->imun_1)
                                    ->setCellValueByColumnAndRow(18, $excel_row, $row->imun_2)
                                    ->setCellValueByColumnAndRow(19, $excel_row, $row->imun_3)
                                    ->setCellValueByColumnAndRow(20, $excel_row, $row->imun_4)
                                    ->setCellValueByColumnAndRow(21, $excel_row, $row->imun_5)
                                    ->setCellValueByColumnAndRow(22, $excel_row, "") //KASUS
                                    ->setCellValueByColumnAndRow(23, $excel_row, $row->prenatal)
                                    ->setCellValueByColumnAndRow(24, $excel_row, $row->pasca_persalinan)
                                    ->setCellValueByColumnAndRow(25, $excel_row, $row->pasca_keguguran)
                                    ->setCellValueByColumnAndRow(26, $excel_row, $row->G)
                                    ->setCellValueByColumnAndRow(27, $excel_row, $row->Pa)
                                    ->setCellValueByColumnAndRow(28, $excel_row, $row->P)
                                    ->setCellValueByColumnAndRow(29, $excel_row, $row->i)
                                    ->setCellValueByColumnAndRow(30, $excel_row, $row->a)
                                    ->setCellValueByColumnAndRow(31, $excel_row, $row->h);
                                    if($row->HPHT === null){
                $object->getActiveSheet()
                                        ->setCellValueByColumnAndRow(32, $excel_row, ($row->HPHT === null) ? '' : date('d/m/Y', strtotime($row->HPHT)))
                                        ->setCellValueByColumnAndRow(33, $excel_row, ($row->HPL === null) ? '' : 'USG : '.date('d/m/Y', strtotime($row->HPL)));
                                    }else{
                $object->getActiveSheet()
                                        ->setCellValueByColumnAndRow(32, $excel_row, ($row->HPHT === null) ? '' : date('d/m/Y', strtotime($row->HPHT)))
                                        ->setCellValueByColumnAndRow(33, $excel_row, ($row->HPL === null) ? '' : date('d/m/Y', strtotime($row->HPL)));
                                    }
            $object->getActiveSheet()
                                    ->setCellValueByColumnAndRow(34, $excel_row, $row->usia_kehamilan)
                                    ->setCellValueByColumnAndRow(35, $excel_row, $row->tindakan_satu)
                                    ->setCellValueByColumnAndRow(36, $excel_row, $row->tindakan_dua)
                                    ->setCellValueByColumnAndRow(37, $excel_row, $row->tindakan_tiga)
                                    ->setCellValueByColumnAndRow(38, $excel_row, "") //DIRUJUK MRS
                                    ->setCellValueByColumnAndRow(39, $excel_row, $row->asuransi_1) //ASURANSI
                                    ->setCellValueByColumnAndRow(40, $excel_row, $row->nama_dokter)
                                    ->setCellValueByColumnAndRow(41, $excel_row, $row->create_by) //asisten
                                    ->setCellValueByColumnAndRow(42, $excel_row, $row->faktor_resiko)
                                    ->setCellValueByColumnAndRow(43, $excel_row, $row->skor_kspr)
                                    ->setCellValueByColumnAndRow(44, $excel_row, $row->diagnosa_nama); //DIAGNOSA
            $excel_row++;
            // var_dump($no);
        }
        // die();
         foreach(range('A','Z') as $columnID) {
               $object->getActiveSheet()
                                    ->getColumnDimension($columnID)
                                    ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan RAJAL '.$tgl_awal_obgyn.' - '.$tgl_akhir_obgyn.'.xlsx"');
        $object_writer->save('php://output');
    }
//END EXPORT POLI OBGYN/BIDAN



//START EXPORT POLI RINCI
    function exportToExcelPoliUmumRinci($tgl_awal_rj, $tgl_akhir_rj, $filterBy){
        $tgl_awal_rj = str_replace("%20"," ",$tgl_awal_rj);
        $tgl_akhir_rj = str_replace("%20"," ",$tgl_akhir_rj);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Report Poli Rinci");
         $object->getActiveSheet()
                        ->getStyle("A3:F3")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A3:F3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $object->getActiveSheet()
                                ->mergeCells('A1:F1')
                                ->setCellValueByColumnAndRow(0,1,"Report dari tanggal $tgl_awal_rj - $tgl_akhir_rj");
        $table_columns = array("NO", "TANGGAL","POLI","DOKTER SPESIALIS","NAMA PASIEN","DIAGNOSA");
        $column = 0;
        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }

        $poli_rinci = $this->Laporan_rm_model->getDataPoliUmumRinci($tgl_awal_rj, $tgl_akhir_rj, $filterBy);
        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($poli_rinci as $row){
            $no++;
            $object->getActiveSheet()
                                    ->setCellValueByColumnAndRow(0, $excel_row, $no)
                                    ->setCellValueByColumnAndRow(1, $excel_row, date('d F Y', strtotime($row->TANGGAL)))
                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->POLI)
                                    ->setCellValueByColumnAndRow(3, $excel_row, $row->DOKTER_SPESIALIS)
                                    ->setCellValueByColumnAndRow(4, $excel_row, $row->NAMA_PASIEN)
                                    ->setCellValueByColumnAndRow(5, $excel_row, $row->DIAGNOSA);
            $excel_row++;
        }

        foreach(range('A','F') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $datenow = date('d/m/Y');
        $object->getActiveSheet()->getStyle('A3:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Rinci Poliklinik '.$tgl_awal_rj.' - '.$tgl_akhir_rj.'.xlsx"');
        $object_writer->save('php://output');
    }
//END EXPORT POLI RINCI


//START EXPOERT POLI KESELURUHAN
    function exportToExcelPoliUmumKeseluruhan($tgl_awal_rj, $tgl_akhir_rj, $filterBy){
        $tgl_awal_rj = str_replace("%20"," ",$tgl_awal_rj);
        $tgl_akhir_rj = str_replace("%20"," ",$tgl_akhir_rj);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Poliklinik Keseluruhan");
         $object->getActiveSheet()
                        ->getStyle("A3:E3")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A3:E3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $object->getActiveSheet()
                                ->mergeCells('A1:E1')
                                ->setCellValueByColumnAndRow(0,1,"Report dari tanggal $tgl_awal_rj - $tgl_akhir_rj");

        $table_columns = array("NO", "TANGGAL","POLI","DOKTER SPESIALIS","JUMLAH");
        $column = 0;
        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }

        $poli_keseluruhan = $this->Laporan_rm_model->getDataPoliUmumKeseluruhan($tgl_awal_rj, $tgl_akhir_rj, $filterBy);
        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($poli_keseluruhan as $row){
            $no++;
            $object->getActiveSheet()
                                    ->setCellValueByColumnAndRow(0, $excel_row, $no)
                                    ->setCellValueByColumnAndRow(1, $excel_row, date('d F Y', strtotime($row->TANGGAL)))
                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->POLI)
                                    ->setCellValueByColumnAndRow(3, $excel_row, $row->NAMA_DOKTER)
                                    ->setCellValueByColumnAndRow(4, $excel_row, $row->JUMLAH);
            $excel_row++;
    }
        foreach(range('A','E') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $datenow = date('d/m/Y');
        $object->getActiveSheet()->getStyle('A3:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Jumlah Kunjungan Dokter Poli '.$tgl_awal_rj.' - '.$tgl_akhir_rj.'.xlsx"');
        $object_writer->save('php://output');
    }
//END EXPORT POLI KESELURUHAN

// TAMBAHAN
    function testing_chart($tgl_awal_rj, $tgl_akhir_rj){
        $tgl_awal_rj = str_replace("%20"," ",$tgl_awal_rj);
        $tgl_akhir_rj = str_replace("%20"," ",$tgl_akhir_rj);
        // var_dump($tgl_awal_obgyn, $tgl_akhir_obgyn);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("testing");

        $data = $this->Laporan_rm_model->getKunjunganData($tgl_awal_rj, $tgl_akhir_rj);

        $object->getActiveSheet()
                ->mergeCells('B2:C2')
                ->mergeCells('A2:A3')
                ->mergeCells('A1:F1');
        $object->getActiveSheet()
                ->setCellValueByColumnAndRow(1,2,"RJ")
                ->setCellValueByColumnAndRow(0,2,"JENIS KELAMIN")
                ->setCellValueByColumnAndRow(0,1,"Report dari tanggal $tgl_awal_rj - $tgl_akhir_rj");

        $table_columns4 = array("JENIS KELAMIN","RJ BARU","RJ LAMA");
        $column = 0;
        foreach($table_columns4 as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }
        // var_dump($object);
        // die();

        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data as $row){
            $no++;
            $object->getActiveSheet()
                            ->setCellValueByColumnAndRow(0, $excel_row, $row->jk)
                            ->setCellValueByColumnAndRow(1, $excel_row, $row->baru)
                            ->setCellValueByColumnAndRow(2, $excel_row, $row->lama);
            $excel_row++;
        }

        // $row = 'A';
        // $column = 0;
        // foreach ($data as $labels) {
        //     $dataseriesLabels[] = new PHPExcel_Chart_DataSeriesValues('String', 'testing!$'.$row.'$1', NULL, $labels->pendidikan_nama);
        //     $row++;
        //     $column++;
        // }


        // echo "<pre>";
        // echo print_r($dataseriesLabels);
        // echo "</pre>";

        $dataseriesLabels = array(
            new PHPExcel_Chart_DataSeriesValues('String', 'testing!$B$3', NULL, 1),
            new PHPExcel_Chart_DataSeriesValues('String', 'testing!$C$3', NULL, 1)
        );

        $xAxisTickValues = array(
            new PHPExcel_Chart_DataSeriesValues('Number', 'testing!$A$4:$A$'.$object->getActiveSheet()->getHighestRow(), NULL, 3),
        );
        $dataseriesValues = array(
            // new PHPExcel_Chart_DataSeriesValues('String', 'testing!$A$2:$A$'.$object->getActiveSheet()->getHighestRow(), NULL, 2),
            new PHPExcel_Chart_DataSeriesValues('String', 'testing!$B$4:$B$'.$object->getActiveSheet()->getHighestRow(), NULL, 3),
            new PHPExcel_Chart_DataSeriesValues('String', 'testing!$C$4:$C$'.$object->getActiveSheet()->getHighestRow(), NULL, 3)
        );


        $series1 = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
            PHPExcel_Chart_DataSeries::GROUPING_STANDARD,       // plotType
            range(0, count($dataseriesValues) - 1),          // plotOrder
            $dataseriesLabels,                             // plotLabel
            $xAxisTickValues,                               // plotCategory
            $dataseriesValues                              // plotValues
        );

        $plotarea = new PHPExcel_Chart_PlotArea(NULL, array($series1));
        $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
        $title = new PHPExcel_Chart_Title('CHART Pendidikan');


        $chart = new PHPExcel_Chart(
            'chart1',       // name
            $title,         // title
            $legend,        // legend
            $plotarea,      // plotArea
            TRUE,           // plotVisibleOnly
            0,              // displayBlanksAs
            new PHPExcel_Chart_Title('Nama Pendidikan'),           // xAxisLabel
            new PHPExcel_Chart_Title('Jumlah')            // yAxisLabel
        );
        // echo "<pre>";
        // echo print_r($chart);
        // echo "</pre>";
        // die();
        // $objWorksheet = $object->getActiveSheet();
        $chart->setTopLeftPosition('G1');
        $chart->setBottomRightPosition('Y10');
        $object->getActiveSheet()->addChart($chart);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $object_writer->setIncludeCharts(TRUE);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Poliklinik Keseluruhan '.$tgl_awal_rj.' - '.$tgl_akhir_rj.'.xlsx"');
        $object_writer->save('php://output');
    }

// END TAMBAHAN


//START REPORT RM DULU
    function exportToExcelRM($tgl_awal_rj, $tgl_akhir_rj){
        $tgl_awal_rj = str_replace("%20"," ",$tgl_awal_rj);
        $tgl_akhir_rj = str_replace("%20"," ",$tgl_akhir_rj);
        // var_dump($tgl_awal_obgyn, $tgl_akhir_obgyn);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Report Pasien");
        $object->getActiveSheet()
                        ->getStyle("A1:AG1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:AG1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("NO REG", "NO RM","NAMA PASIEN","NAMA PANGGILAN","JENIS KELAMIN","UMUR","ALAMAT","KELAS","RUANG","CATATAN KHUSUS","KASUS POLISI","TGL MASUK MRS","JAM MASUK MRS","TGL PULANG MRS","JAM PULANG","LAMA MRS","KODE PERUJUK","NAMA PERUJUK",
        "KODE PENANGGUNG JAWAB","NAMA PENANGGUNG JAWAB","UMUR","HUBUNGAN DENGAN PASIEN","ASURANSI","JAMINAN","KODE DOKTER PJ","NAMA DOKTER PJ","DOKTER LAIN","ASAL MASUK","DIRAWAT KARENA","PETUGAS PENDAFTAR","ANAMNESE","DIAGNOSA","MACAM PERAWATAN");
        $column = 0;
        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Laporan_rm_model->getDataRmRj($tgl_awal_rj, $tgl_akhir_rj);
        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->no_pendaftaran);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->no_rekam_medis);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->pasien_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->nama_panggilan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->jenis_kelamin);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->umur);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->pasien_alamat);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->kelaspelayanan_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->nama_poliruangan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->catatan_khusus); //catatan khusus
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->kasus_polisi); //kasusu polisi
            $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, date('d F Y', strtotime($row->tgl_pendaftaran))); //tgl masuk mrs
            $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, date('H:i', strtotime($row->tgl_pendaftaran))); //jam masuk mrs
            $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, ""); //tgl pulang mrs
            $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, ""); //jam pulang mrs
            $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, ""); //lama rs
            $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, ""); //kode perujuk
            $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->nama_perujuk); //nama perujuk
            $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, ""); //kode pj
            if($row->status_kawin == "Kawin"){
                $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row->nama_suami_istri); //nama pj
                $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $row->umur_suami_istri); //
                $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, "Suami / Istri"); //asuransi
            }else{
                $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row->nama_orangtua); //nama pj
                $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $row->umur_ortu); //
                $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, "Orangtua"); //asuransi
            }
            $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $row->nama_asuransi); // asuransi
            $object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, ""); //jaminan
            $object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, ""); //kd dokter pj
            $object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, $row->NAME_DOKTER); //nama dokter pj
            $object->getActiveSheet()->setCellValueByColumnAndRow(26, $excel_row, ""); //dokter lain
            $object->getActiveSheet()->setCellValueByColumnAndRow(27, $excel_row, $row->nama_poliruangan); //asal masuk
            $object->getActiveSheet()->setCellValueByColumnAndRow(28, $excel_row, ""); //dirawat akrena
            $object->getActiveSheet()->setCellValueByColumnAndRow(39, $excel_row, ""); //petugas
            $object->getActiveSheet()->setCellValueByColumnAndRow(30, $excel_row, ""); //anamse
            $object->getActiveSheet()->setCellValueByColumnAndRow(32, $excel_row, ""); //diagnosa
            $object->getActiveSheet()->setCellValueByColumnAndRow(33, $excel_row, ""); //macam perawatan
            $excel_row++;
    }

        $letters = range ('A1','Z1');
            foreach($letters as $columnID) {
                $object->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $datenow = date('d/m/Y');
        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Report Pasien per '.$datenow.'.xlsx"');
        $object_writer->save('php://output');
    }



//START EXPORT HASIL VAKSINASI
    function exportToExcelHasilvaksinasi($tgl_awal_rj, $tgl_akhir_rj, $tipe){
        $tgl_awal_rj = str_replace("%20"," ",$tgl_awal_rj);
        $tgl_akhir_rj = str_replace("%20"," ",$tgl_akhir_rj);
        $today_month =  date('m');
        $today_year =  date('Y');
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Hasil Vaksinasi");
        $object->getActiveSheet()
                        ->getStyle("A4:O4")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A4:O4")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $object->getActiveSheet()
                        ->getStyle("D5:O5")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("D5:O5")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $object->getActiveSheet()
                                ->mergeCells('A4:A5')
                                ->mergeCells('B4:B5')
                                ->mergeCells('C4:C5')
                                ->mergeCells('D4:E4')
                                ->mergeCells('F4:O4')
                                ->mergeCells('A2:C2') //UPK
                                ->mergeCells('A3:C3') //ALAMAT
                                ->mergeCells('D2:F2') //NAMA RS
                                ->mergeCells('D3:F3') //JL RS
                                ->mergeCells('H3:J3') //TAHUN
                                ->mergeCells('H2:J2') //LAL BULAN
                                ->mergeCells('K2:M2') //isian bulan
                                ->mergeCells('K3:M3') //isian tahun
                                ->mergeCells('A1:O1') //HEADER
                                // ->mergeCells('F5:O5') //vkasinasi 2
                                ->setCellValueByColumnAndRow(0,1,"LAPORAN HASIL VAKSINASI UPK SWASTA")
                                ->setCellValueByColumnAndRow(0,4,"NO")
                                ->setCellValueByColumnAndRow(1,4,"NAMA")
                                ->setCellValueByColumnAndRow(2,4,"TANGGAL LAHIR")
                                ->setCellValueByColumnAndRow(3,4,"ALAMAT")
                                ->setCellValueByColumnAndRow(5,4,"HASIL VAKSINASI")
                                ->setCellValueByColumnAndRow(0,2,"UPK SWASTA")
                                ->setCellValueByColumnAndRow(3,2,": PURI BUNDA")
                                ->setCellValueByColumnAndRow(0,3,"ALAMAT")
                                ->setCellValueByColumnAndRow(3,3,": JL SIMPANG SULFAT")
                                ->setCellValueByColumnAndRow(7,2,"LAPORAN BULAN")
                                ->setCellValueByColumnAndRow(7,3,"TAHUN")
                                ->setCellValueByColumnAndRow(10,2,": ".$today_month)
                                ->setCellValueByColumnAndRow(10,3,": ".$today_year);
                                // ->setCellValueByColumnAndRow(5,5,"JENIS VAKSINASI");
        if($tipe == 1){
            $table_columns = array("NO", "NAMA","TANGGAL LAHIR","JALAN RT/RW","KELURAHAN","HB 0","BCG","Polio 1","Pentabio 1","Polio 2","Pentabio 2","Polio 3","Pentabio 3","Polio 4","CAMPAK");
            $column = 0;
            foreach($table_columns as $field){
                $object->getActiveSheet()->setCellValueByColumnAndRow($column, 5, $field);
                $column++;
            }
        }else if($tipe == 2){
            $table_columns = array("NO", "NAMA","TANGGAL LAHIR","JALAN RT/RW","KELURAHAN","           ","           ","           ","           ","           ","           ","           ","           ","           ","           ");
            $column = 0;
            foreach($table_columns as $field){
                $object->getActiveSheet()->setCellValueByColumnAndRow($column, 5, $field);
                $column++;
            }
        }


        $employee_data = $this->Laporan_rm_model->getHasilVaksinasi($tgl_awal_rj, $tgl_akhir_rj, $tipe);
        if($tipe == 1){
            $excel_row = 6;
            $no = isset($_GET['start']) ? $_GET['start'] : 0;
            foreach($employee_data as $row){
                $no++;
                $object->getActiveSheet()
                                        ->setCellValueByColumnAndRow(0, $excel_row, $no)
                                        ->setCellValueByColumnAndRow(1, $excel_row, $row->pasien_nama)
                                        ->setCellValueByColumnAndRow(2, $excel_row, (!$row->tanggal_lahir) ? '' : date('d/m/Y', strtotime($row->tanggal_lahir)))
                                        ->setCellValueByColumnAndRow(3, $excel_row, $row->pasien_alamat)
                                        ->setCellValueByColumnAndRow(4, $excel_row, $row->kelurahan_nama)
                                        ->setCellValueByColumnAndRow(5, $excel_row, (!$row->imun_1) ? '' : date('d M Y', strtotime($row->imun_1)))
                                        ->setCellValueByColumnAndRow(6, $excel_row, (!$row->imun_2) ? '' : date('d M Y', strtotime($row->imun_2)))
                                        ->setCellValueByColumnAndRow(7, $excel_row, (!$row->imun_3) ? '' : date('d M Y', strtotime($row->imun_3)))
                                        ->setCellValueByColumnAndRow(8, $excel_row, (!$row->imun_4) ? '' : date('d M Y', strtotime($row->imun_4)))
                                        ->setCellValueByColumnAndRow(9, $excel_row, (!$row->imun_5) ? '' : date('d M Y', strtotime($row->imun_5)))
                                        ->setCellValueByColumnAndRow(10, $excel_row, (!$row->imun_6) ? '' : date('d M Y', strtotime($row->imun_6)))
                                        ->setCellValueByColumnAndRow(11, $excel_row, (!$row->imun_7) ? '' : date('d M Y', strtotime($row->imun_7)))
                                        ->setCellValueByColumnAndRow(12, $excel_row, (!$row->imun_8) ? '' : date('d M Y', strtotime($row->imun_8)))
                                        ->setCellValueByColumnAndRow(13, $excel_row, (!$row->imun_9) ? '' : date('d M Y', strtotime($row->imun_9)))
                                        ->setCellValueByColumnAndRow(14, $excel_row, (!$row->imun_10) ? '' : date('d M Y', strtotime($row->imun_10)));
                $excel_row++;
            }
        }else if($tipe == 2){
            $excel_row = 6;
            $no = isset($_GET['start']) ? $_GET['start'] : 0;
            foreach($employee_data as $row){
                $no++;
                $object->getActiveSheet()
                ->setCellValueByColumnAndRow(0, $excel_row, $no)
                ->setCellValueByColumnAndRow(1, $excel_row, $row->pasien_nama)
                ->setCellValueByColumnAndRow(2, $excel_row, date('d M Y', strtotime($row->tanggal_lahir)))
                ->setCellValueByColumnAndRow(3, $excel_row, $row->pasien_alamat)
                ->setCellValueByColumnAndRow(4, $excel_row, $row->kelurahan_nama)
                ->setCellValueByColumnAndRow(5, $excel_row, $row->imun_1)
                ->setCellValueByColumnAndRow(6, $excel_row, $row->imun_2)
                ->setCellValueByColumnAndRow(7, $excel_row, $row->imun_3)
                ->setCellValueByColumnAndRow(8, $excel_row, $row->imun_4)
                ->setCellValueByColumnAndRow(9, $excel_row, $row->imun_5)
                ->setCellValueByColumnAndRow(10, $excel_row, $row->imun_6)
                ->setCellValueByColumnAndRow(11, $excel_row, $row->imun_7)
                ->setCellValueByColumnAndRow(12, $excel_row, $row->imun_8)
                ->setCellValueByColumnAndRow(13, $excel_row, $row->imun_9)
                ->setCellValueByColumnAndRow(14, $excel_row, $row->imun_10);
                $excel_row++;
            }
        }
        $letters = range ('A4','O4');
        foreach($letters as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $object->getActiveSheet()->getStyle("A1:O1")->applyFromArray($style);

        $datenow = date('d/m/Y');
        $object->getActiveSheet()->getStyle('A4:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Hasil Vaksinasi dari '.$tgl_awal_rj.' - '.$tgl_akhir_rj.'.xlsx"');
        $object_writer->save('php://output');
    }
//END REPORT HASIL VAKSINASI



//START REPORT RM BARU
    function exportToExcelRMBaru($tgl_awal_rj, $tgl_akhir_rj){
        $tgl_awal_rj = str_replace("%20"," ",$tgl_awal_rj);
        $tgl_akhir_rj = str_replace("%20"," ",$tgl_akhir_rj);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $excel = new PHPEXCEL();


// sheet 1 REPORT RM
        $excel->createSheet(0);
        $sheet1 = $excel->getSheet(0)->setTitle('Report Pasien RM');
        $excel->getSheetByName('Report Pasien RM')
                                    ->getStyle("A3:AG3")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('Report Pasien RM')
                        ->getStyle("A3:AG3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $excel->getSheetByName('Report Pasien RM')
                                ->mergeCells('A1:AG1')
                                ->setCellValueByColumnAndRow(0,1,"Report dari tanggal $tgl_awal_rj - $tgl_akhir_rj");

        $table_columns1 = array("NO REG", "NO RM","NAMA PASIEN","NAMA PANGGILAN","JENIS KELAMIN","UMUR","ALAMAT","KELAS","RUANG","CATATAN KHUSUS","KASUS POLISI","TGL MASUK MRS","JAM MASUK MRS","TGL PULANG MRS","JAM PULANG","LAMA MRS","KODE PERUJUK","NAMA PERUJUK",
        "KODE PENANGGUNG JAWAB","NAMA PENANGGUNG JAWAB","UMUR","HUBUNGAN DENGAN PASIEN","ASURANSI","JAMINAN","KODE DOKTER PJ","NAMA DOKTER PJ","DOKTER LAIN","ASAL MASUK","DIRAWAT KARENA","PETUGAS PENDAFTAR","ANAMNESE","DIAGNOSA","MACAM PERAWATAN");
        $column = 0;
        foreach($table_columns1 as $field){
            $excel->getSheetByName('Report Pasien RM')->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }
        $data_rm = $this->Laporan_rm_model->getDataRmRj($tgl_awal_rj, $tgl_akhir_rj);
        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data_rm as $row){
            $no++;
            $excel->getSheetByName('Report Pasien RM')
                                                    ->setCellValueByColumnAndRow(0, $excel_row, $row->no_pendaftaran)
                                                    ->setCellValueByColumnAndRow(1, $excel_row, $row->no_rekam_medis)
                                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->pasien_nama)
                                                    ->setCellValueByColumnAndRow(3, $excel_row, $row->nama_panggilan)
                                                    ->setCellValueByColumnAndRow(4, $excel_row, $row->jenis_kelamin)
                                                    ->setCellValueByColumnAndRow(5, $excel_row, $row->umur)
                                                    ->setCellValueByColumnAndRow(6, $excel_row, $row->pasien_alamat)
                                                    ->setCellValueByColumnAndRow(7, $excel_row, $row->kelaspelayanan_nama)
                                                    ->setCellValueByColumnAndRow(8, $excel_row, $row->nama_poliruangan)
                                                    ->setCellValueByColumnAndRow(9, $excel_row, $row->catatan_khusus) //catatan khusus
                                                    ->setCellValueByColumnAndRow(10, $excel_row, $row->kasus_polisi) //kasusu polisi
                                                    ->setCellValueByColumnAndRow(11, $excel_row, date('d F Y', strtotime($row->tgl_pendaftaran))) //tgl masuk mrs
                                                    ->setCellValueByColumnAndRow(12, $excel_row, date('H:i', strtotime($row->tgl_pendaftaran))) //jam masuk mrs
                                                    ->setCellValueByColumnAndRow(13, $excel_row, "") //tgl pulang mrs
                                                    ->setCellValueByColumnAndRow(14, $excel_row, "") //jam pulang mrs
                                                    ->setCellValueByColumnAndRow(15, $excel_row, "") //lama rs
                                                    ->setCellValueByColumnAndRow(16, $excel_row, "") //kode perujuk
                                                    ->setCellValueByColumnAndRow(17, $excel_row, $row->nama_perujuk) //nama perujuk
                                                    ->setCellValueByColumnAndRow(18, $excel_row, "") //kode pj
                                                    ->setCellValueByColumnAndRow(19, $excel_row, $row->nama_pj) //nama pj
                                                    ->setCellValueByColumnAndRow(20, $excel_row, "") //umur pj
                                                    ->setCellValueByColumnAndRow(21, $excel_row, $row->status_hubungan_pj) //hubungan
                                                    ->setCellValueByColumnAndRow(22, $excel_row, $row->nama_asuransi) // asuransi
                                                    ->setCellValueByColumnAndRow(23, $excel_row, "") //jaminan
                                                    ->setCellValueByColumnAndRow(24, $excel_row, "") //kd dokter pj
                                                    ->setCellValueByColumnAndRow(25, $excel_row, $row->NAME_DOKTER) //nama dokter pj
                                                    ->setCellValueByColumnAndRow(26, $excel_row, "") //dokter lain
                                                    ->setCellValueByColumnAndRow(27, $excel_row, $row->nama_poliruangan) //asal masuk
                                                    ->setCellValueByColumnAndRow(28, $excel_row, "") //dirawat akrena
                                                    ->setCellValueByColumnAndRow(29, $excel_row, "") //petugas
                                                    ->setCellValueByColumnAndRow(30, $excel_row, "") //anamse
                                                    ->setCellValueByColumnAndRow(31, $excel_row, $row->diagnosa) //diagnosa
                                                    ->setCellValueByColumnAndRow(32, $excel_row, ""); //diagnosa
            $excel_row++;
        }

        $letters1 = range ('A3','Z3');
            foreach($letters1 as $columnID) {
                $excel->getSheetByName('Report Pasien RM')->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
//END SHEET 1

//START SHEET 2 DATA KUNJUNGAN
        $excel->createSheet(1);
        $sheet2 = $excel->getSheet(1)->setTitle('JumlahKunjungan');
        $excel->getSheetByName('JumlahKunjungan')
                                    ->getStyle("A2:C2")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('JumlahKunjungan')
                        ->getStyle("A2:C2")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $excel->getSheetByName('JumlahKunjungan')
                                    ->getStyle("B3:C3")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('JumlahKunjungan')
                        ->getStyle("B3:C3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $data_kunjungan = $this->Laporan_rm_model->getKunjunganData($tgl_awal_rj, $tgl_akhir_rj);


        $excel->getSheetByName('JumlahKunjungan')
                ->mergeCells('B2:C2')
                ->mergeCells('A2:A3')
                ->mergeCells('A1:F1');
        $excel->getSheetByName('JumlahKunjungan')
                ->setCellValueByColumnAndRow(1,2,"RJ")
                ->setCellValueByColumnAndRow(0,2,"JENIS KELAMIN")
                ->setCellValueByColumnAndRow(1,3,"RJ BARU")
                ->setCellValueByColumnAndRow(2,3,"RJ LAMA")
                ->setCellValueByColumnAndRow(0,1,"Report dari tanggal $tgl_awal_rj - $tgl_akhir_rj");

        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data_kunjungan as $row){
            $no++;
            $excel->getSheetByName('JumlahKunjungan')
                            ->setCellValueByColumnAndRow(0, $excel_row, $row->jk)
                            ->setCellValueByColumnAndRow(1, $excel_row, $row->baru)
                            ->setCellValueByColumnAndRow(2, $excel_row, $row->lama);
            $excel_row++;
        }

        $letters2 = range ('A2','C2');
            foreach($letters2 as $columnID) {
                $excel->getSheetByName('JumlahKunjungan')->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );


        //CHART DATA KUNJUNGAN
        $dataseriesLabels_datakunjungan = array(
            new PHPExcel_Chart_DataSeriesValues('String', 'JumlahKunjungan!$B$3', NULL, 1),
            new PHPExcel_Chart_DataSeriesValues('String', 'JumlahKunjungan!$C$3', NULL, 1)
        );

        $xAxisTickValues_datakunjungan = array(
            new PHPExcel_Chart_DataSeriesValues('Number', 'JumlahKunjungan!$A$4:$A$'.$excel->getSheetByName('JumlahKunjungan')->getHighestRow(), NULL, 3),
        );
        $dataseriesValues_datakunjungan = array(
            new PHPExcel_Chart_DataSeriesValues('String', 'JumlahKunjungan!$B$4:$B$'.$excel->getSheetByName('JumlahKunjungan')->getHighestRow(), NULL, 3),
            new PHPExcel_Chart_DataSeriesValues('String', 'JumlahKunjungan!$C$4:$C$'.$excel->getSheetByName('JumlahKunjungan')->getHighestRow(), NULL, 3)
        );


        $series_datakunjungan = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,                   // plotType
            PHPExcel_Chart_DataSeries::GROUPING_STANDARD,               // plotType
            range(0, count($dataseriesValues_datakunjungan) - 1),       // plotOrder
            $dataseriesLabels_datakunjungan,                            // plotLabel
            $xAxisTickValues_datakunjungan,                             // plotCategory
            $dataseriesValues_datakunjungan                             // plotValues
        );

        $plotarea_datakunjungan = new PHPExcel_Chart_PlotArea(NULL, array($series_datakunjungan));
        $legend_datakunjungan = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
        $title_datakunjungan = new PHPExcel_Chart_Title('Chart Data Kunjungan Pasien RJ');

        $chart_datakunjungan = new PHPExcel_Chart(
            'chart1',                                   // name
            $title_datakunjungan,                       // title
            $legend_datakunjungan,                      // legend
            $plotarea_datakunjungan,                    // plotArea
            TRUE,                                       // plotVisibleOnly
            0,                                          // displayBlanksAs
            new PHPExcel_Chart_Title('Jenis Kelamin'),  // xAxisLabel
            new PHPExcel_Chart_Title('Jumlah')          // yAxisLabel
        );

        $chart_datakunjungan->setTopLeftPosition('G1');
        $chart_datakunjungan->setBottomRightPosition('Y10');
        $excel->getSheetByName('JumlahKunjungan')->addChart($chart_datakunjungan);
        // END CHART DATA KUNJUNGAN

        $excel->getSheetByName('JumlahKunjungan')->getStyle('A2:' . $excel->getSheetByName('JumlahKunjungan')->getHighestColumn() . $excel->getSheetByName('JumlahKunjungan')->getHighestRow())->applyFromArray($styleArray);
//END SHEET 2

//START SHEET 3 DATA DEMOGRAFI
        $excel->createSheet(2);
        $sheet3 = $excel->getSheet(2)->setTitle('DataDemografi');
        $excel->getSheetByName('DataDemografi')
                                    ->getStyle("A3:C3")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('DataDemografi')
                        ->getStyle("A3:C3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $excel->getSheetByName('DataDemografi')
                                    ->getStyle("E3:G3")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('DataDemografi')
                        ->getStyle("E3:G3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $excel->getSheetByName('DataDemografi')
                                    ->getStyle("I3:K3")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('DataDemografi')
                                                ->getStyle("I3:K3")
                                                ->getFill()
                                                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                                                ->getStartColor()
                                                ->setRGB('01c0c8');

        $excel->getSheetByName('DataDemografi')->setCellValueByColumnAndRow(5,1, "Report dari tanggal $tgl_awal_rj - $tgl_akhir_rj");

        //START DEMOGRAFI PEKERJAAN PASIEN
        $table_columns3 = array("No", "Pekerjaan Pasien","Jumlah");
        $column = 0;
        foreach($table_columns3 as $field){
            $excel->getSheetByName('DataDemografi')->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }
        $data_demografi = $this->Laporan_rm_model->getDemografiPekerjaanPasien($tgl_awal_rj, $tgl_akhir_rj);

        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data_demografi as $row){
            $no++;
            $excel->getSheetByName('DataDemografi')
                                                    ->setCellValueByColumnAndRow(0, $excel_row, $no)
                                                    ->setCellValueByColumnAndRow(1, $excel_row, $row->nama_pekerjaan)
                                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->jum_pekerjaan);
            $excel_row++;
        }

        $letters3 = range ('A3','C3');
            foreach($letters3 as $columnID) {
                $excel->getSheetByName('DataDemografi')->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

        //CHART DEMOGRAFI PEKERJAAN PASIEN
        $dataseriesLabels   = array(
            new PHPExcel_Chart_DataSeriesValues('String','DataDemografi!$B$3',NULL,1));
        $xAxisTickValues    = array(
            new PHPExcel_Chart_DataSeriesValues('Number','DataDemografi!$B$4:$B$'.$excel->getSheetByName('DataDemografi')->getHighestRow(),NULL, 10));
        $dataseriesValues   = array(
            new PHPExcel_Chart_DataSeriesValues('String','DataDemografi!$C$4:$C$'.$excel->getSheetByName('DataDemografi')->getHighestRow(), NUll, 10));

        $series_pekerjaan_pasien = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,        // plotType
            PHPExcel_Chart_DataSeries::GROUPING_STANDARD,    // plotType
            range(0, count($dataseriesValues) - 1),          // plotOrder
            $dataseriesLabels,                               // plotLabel
            $xAxisTickValues,                                // plotCategory
            $dataseriesValues                                // plotValues
        );

        $plotarea1 = new PHPExcel_Chart_PlotArea(NULL, array($series_pekerjaan_pasien));
        $legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
        $title1 = new PHPExcel_Chart_Title('Chart Pendidikan');

        $chart_pekerjaan_pasien = new PHPExcel_Chart(
            'chart1',        // name
            $title1,         // title
            $legend1,        // legend
            $plotarea1,      // plotArea
            TRUE,            // plotVisibleOnly
            0,               // displayBlanksAs
            new PHPExcel_Chart_Title('Nama Pekerjaan'),             // xAxisLabel
            new PHPExcel_Chart_Title('Jumlah')                      // yAxisLabel
        );

        $chart_pekerjaan_pasien->setTopLeftPosition('A20');
        $chart_pekerjaan_pasien->setBottomRightPosition('L30');
        $excel->getSheetByName('DataDemografi')->addChart($chart_pekerjaan_pasien);
        //END DATA DEMOGRAFI PEKERJAAN PASIEN


        //START DATA DEMOGRAFI PENDIDIKAN PASIEN
        $table_columns4 = array("No", "Pendidikan Pasien","Jumlah");
        $column = 4;
        foreach($table_columns4 as $field){
            $excel->getSheetByName('DataDemografi')->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }
        $data_demografi2 = $this->Laporan_rm_model->getDemografiPendidikanPasien($tgl_awal_rj, $tgl_akhir_rj);

        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data_demografi2 as $row){
            $no++;
            $excel->getSheetByName('DataDemografi')
                                                    ->setCellValueByColumnAndRow(4, $excel_row, $no)
                                                    ->setCellValueByColumnAndRow(5, $excel_row, $row->pendidikan_nama)
                                                    ->setCellValueByColumnAndRow(6, $excel_row, $row->jum_pendidikan);
            $excel_row++;
        }

        $letters4 = range ('E3','G3');
            foreach($letters4 as $columnID) {
                $excel->getSheetByName('DataDemografi')->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
        //CHART DATA DEMOGRAFI PASIEN
        $dataseriesLabels2   = array(
            new PHPExcel_Chart_DataSeriesValues('String','DataDemografi!$F$3',NULL,1));
        $xAxisTickValues2    = array(
            new PHPExcel_Chart_DataSeriesValues('Number','DataDemografi!$F$4:$F$'.$excel->getSheetByName('DataDemografi')->getHighestRow(),NULL, 10));
        $dataseriesValues2   = array(
            new PHPExcel_Chart_DataSeriesValues('String','DataDemografi!$G$4:$G$'.$excel->getSheetByName('DataDemografi')->getHighestRow(), NUll, 10));

        $series_pendidikan_pasien = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,        // plotType
            PHPExcel_Chart_DataSeries::GROUPING_STANDARD,    // plotType
            range(0, count($dataseriesValues) - 1),          // plotOrder
            $dataseriesLabels2,                              // plotLabel
            $xAxisTickValues2,                               // plotCategory
            $dataseriesValues2                               // plotValues
        );

        $plotarea2 = new PHPExcel_Chart_PlotArea(NULL, array($series_pendidikan_pasien));
        $legend2 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
        $title2 = new PHPExcel_Chart_Title('Chart Pendidikan');

        $chart_pendidikan_pasien = new PHPExcel_Chart(
            'chart1',        // name
            $title2,         // title
            $legend2,        // legend
            $plotarea2,      // plotArea
            TRUE,            // plotVisibleOnly
            0,               // displayBlanksAs
            new PHPExcel_Chart_Title('Nama Pendidikan'),            // xAxisLabel
            new PHPExcel_Chart_Title('Jumlah')                      // yAxisLabel
        );

        $chart_pendidikan_pasien->setTopLeftPosition('A40');
        $chart_pendidikan_pasien->setBottomRightPosition('L50');
        $excel->getSheetByName('DataDemografi')->addChart($chart_pendidikan_pasien);
        //END DATA DEMOGRAFI PENDIDIKAN PASIEN

        //START DATA DEMOGRAFI PEKERJAAN PJ
        $table_columns5 = array("No", "Pekerjaan Penanggung Jawab","Jumlah");
        $column = 8;
        foreach($table_columns5 as $field){
            $excel->getSheetByName('DataDemografi')->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }

        $data_demografi3 = $this->Laporan_rm_model->getDemografiPekrjaanPJ($tgl_awal_rj, $tgl_akhir_rj);
        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data_demografi3 as $row){
            $no++;
            $excel->getSheetByName('DataDemografi')
                                                    ->setCellValueByColumnAndRow(8, $excel_row, $no)
                                                    ->setCellValueByColumnAndRow(9, $excel_row, $row->nama_pekerjaan)
                                                    ->setCellValueByColumnAndRow(10, $excel_row, $row->jum_pekerjaan_PJ);
            $excel_row++;
        }
        $letters5 = range ('I3','K3');
            foreach($letters5 as $columnID) {
                $excel->getSheetByName('DataDemografi')->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
         $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        //CHART DATA DEMOGRAFI PEKERJAAN PJ
        $dataseriesLabels3   = array(
            new PHPExcel_Chart_DataSeriesValues('String','DataDemografi!$J$3',NULL,1));
        $xAxisTickValues3    = array(
            new PHPExcel_Chart_DataSeriesValues('Number','DataDemografi!$J$4:$J$'.$excel->getSheetByName('DataDemografi')->getHighestRow(),NULL, 10));
        $dataseriesValues3   = array(
            new PHPExcel_Chart_DataSeriesValues('String','DataDemografi!$K$4:$K$'.$excel->getSheetByName('DataDemografi')->getHighestRow(), NUll, 10));

        $series_pekerjaan_PJ = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,        // plotType
            PHPExcel_Chart_DataSeries::GROUPING_STANDARD,    // plotType
            range(0, count($dataseriesValues3) - 1),         // plotOrder
            $dataseriesLabels3,                              // plotLabel
            $xAxisTickValues3,                               // plotCategory
            $dataseriesValues3                               // plotValues
        );

        $plotarea3 = new PHPExcel_Chart_PlotArea(NULL, array($series_pekerjaan_PJ));
        $legend3 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
        $title3 = new PHPExcel_Chart_Title('Chart Pekerjaan');

        $chart_pekerjaan_PJ = new PHPExcel_Chart(
            'chart3',        // name
            $title3,         // title
            $legend3,        // legend
            $plotarea3,      // plotArea
            TRUE,            // plotVisibleOnly
            0,               // displayBlanksAs
            new PHPExcel_Chart_Title('Nama Pekerjaan PJ'),           // xAxisLabel
            new PHPExcel_Chart_Title('Jumlah')                       // yAxisLabel
        );

        $chart_pekerjaan_PJ->setTopLeftPosition('A60');
        $chart_pekerjaan_PJ->setBottomRightPosition('L70');
        $excel->getSheetByName('DataDemografi')->addChart($chart_pekerjaan_PJ);
        $excel->getSheetByName('DataDemografi')->getStyle('A3:' . $excel->getSheetByName('DataDemografi')->getHighestColumn() . $excel->getSheetByName('DataDemografi')->getHighestRow())->applyFromArray($styleArray);
        // END DATA DEMOGRAFI PEKERJAAN PJ
//END SHEET 3

//START SHEET 4 DATA PENYAKIT
        $excel->createSheet(3);
        $sheet4 = $excel->getSheet(3)->setTitle('DataPenyakit');
        $excel->getSheetByName('DataPenyakit')
                                    ->getStyle("A3:D3")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('DataPenyakit')
                        ->getStyle("A3:D3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $excel->getSheetByName('DataPenyakit')
                                            ->mergeCells('A1:D1')
                                            ->setCellValueByColumnAndRow(0, 1, "Report Dari Tanggal $tgl_awal_rj - $tgl_akhir_rj");
        $table_columns6 = array("NO", "NAMA PENYAKIT","KODE","JUMLAH");
        $column = 0;
        foreach($table_columns6 as $field){
            $excel->getSheetByName('DataPenyakit')->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }
        $data_penyakit = $this->Laporan_rm_model->getDataPenyakit($tgl_awal_rj, $tgl_akhir_rj);
        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data_penyakit as $row){
            $no++;
            $excel->getSheetByName('DataPenyakit')
                                                    ->setCellValueByColumnAndRow(0, $excel_row, $no)
                                                    ->setCellValueByColumnAndRow(1, $excel_row, $row->nama_icd10)
                                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->kode_icd10)
                                                    ->setCellValueByColumnAndRow(3, $excel_row, $row->jum);
            $excel_row++;
        }

        $letters6 = range ('A1','D1');
            foreach($letters6 as $columnID) {
                $excel->getSheetByName('DataPenyakit')->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );


        //CHART DATA PENYAKIT
        $dataseriesLabels4   = array(
            new PHPExcel_Chart_DataSeriesValues('String','DataPenyakit!$B$3',NULL,1));
        $xAxisTickValues4    = array(
            new PHPExcel_Chart_DataSeriesValues('Number','DataPenyakit!$B$4:$B$'.$excel->getSheetByName('DataPenyakit')->getHighestRow(),NULL, 10));
        $dataseriesValues4   = array(
            new PHPExcel_Chart_DataSeriesValues('String','DataPenyakit!$D$4:$D$'.$excel->getSheetByName('DataPenyakit')->getHighestRow(), NUll, 10));

        $series_data_penyakit = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,       // plotType
            PHPExcel_Chart_DataSeries::GROUPING_STANDARD,    // plotType
            range(0, count($dataseriesValues4) - 1),          // plotOrder
            $dataseriesLabels4,                               // plotLabel
            $xAxisTickValues4,                                // plotCategory
            $dataseriesValues4                                // plotValues
        );

        $plotarea4 = new PHPExcel_Chart_PlotArea(NULL, array($series_data_penyakit));
        $legend4 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
        $title4 = new PHPExcel_Chart_Title('Chart Data Penyakit');

        $chart_data_penyakit = new PHPExcel_Chart(
            'chart3',       // name
            $title4,         // title
            $legend4,        // legend
            $plotarea4,      // plotArea
            TRUE,           // plotVisibleOnly
            0,              // displayBlanksAs
            new PHPExcel_Chart_Title('Nama Penyakit'),           // xAxisLabel
            new PHPExcel_Chart_Title('Jumlah')                      // yAxisLabel
        );

        $chart_data_penyakit->setTopLeftPosition('G3');
        $chart_data_penyakit->setBottomRightPosition('Q15');
        $excel->getSheetByName('DataPenyakit')->addChart($chart_data_penyakit);
        $excel->getSheetByName('DataPenyakit')->getStyle('A3:' . $excel->getSheetByName('DataPenyakit')->getHighestColumn() . $excel->getSheetByName('DataPenyakit')->getHighestRow())->applyFromArray($styleArray);
        //END CHART DATA PENYAKIT
//END SHEET 4

//START SHEET 5 DATA PEMBAYARAN
        $excel->createSheet(4);
        $sheet5 = $excel->getSheet(4)->setTitle('DataPembayaran');
        $excel->getSheetByName('DataPembayaran')
                                    ->getStyle("A2:D2")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('DataPembayaran')
                        ->getStyle("A2:D2")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $excel->getSheetByName('DataPembayaran')
                                    ->getStyle("B3:D3")
                                    ->getFont()
                                    ->setSize(14)
                                    ->setBold(true)
                                    ->getColor()
                                    ->setRGB('FFFFFF');
        $excel->getSheetByName('DataPembayaran')
                        ->getStyle("B3:D3")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $data_pembayaran = $this->Laporan_rm_model->getDataPembayaran($tgl_awal_rj, $tgl_akhir_rj);

        $excel->getSheetByName('DataPembayaran')
                ->mergeCells('B2:D2')
                ->mergeCells('A2:A3')
                ->mergeCells('A1:d1');
        $excel->getSheetByName('DataPembayaran')
                ->setCellValueByColumnAndRow(0,1,"Report tahun 2018")
                ->setCellValueByColumnAndRow(0,2,"BULAN")
                ->setCellValueByColumnAndRow(1,2,"RAWAT JALAN")
                ->setCellValueByColumnAndRow(1,3,"PRIBADI")
                ->setCellValueByColumnAndRow(2,3,"ASURANSI")
                ->setCellValueByColumnAndRow(3,3,"BPJS");

        $excel_row = 4;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($data_pembayaran as $row){
            $no++;
            $excel->getSheetByName('DataPembayaran')
                            ->setCellValueByColumnAndRow(0, $excel_row, $row->bulan)
                            ->setCellValueByColumnAndRow(1, $excel_row, $row->pribadi)
                            ->setCellValueByColumnAndRow(2, $excel_row, $row->asuransi)
                            ->setCellValueByColumnAndRow(3, $excel_row, $row->bpjs);
            $excel_row++;
        }

        $letters7 = range ('A2','D2');
            foreach($letters7 as $columnID) {
                $excel->getSheetByName('DataPembayaran')->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        //CHART DATA PEMBAYARAN RAWAT JALAN
        $dataseriesLabels_datapembayaran = array(
            new PHPExcel_Chart_DataSeriesValues('String', 'DataPembayaran!$B$3', NULL, 1),
            new PHPExcel_Chart_DataSeriesValues('String', 'DataPembayaran!$C$3', NULL, 1),
            new PHPExcel_Chart_DataSeriesValues('String', 'DataPembayaran!$D$3', NULL, 1)
        );

        $xAxisTickValues_datapembayaran = array(
            new PHPExcel_Chart_DataSeriesValues('Number', 'DataPembayaran!$A$4:$A$'.$excel->getSheetByName('DataPembayaran')->getHighestRow(), NULL, 3),
        );
        $dataseriesValues_datapembayaran = array(
            new PHPExcel_Chart_DataSeriesValues('String', 'DataPembayaran!$B$4:$B$'.$excel->getSheetByName('DataPembayaran')->getHighestRow(), NULL, 3),
            new PHPExcel_Chart_DataSeriesValues('String', 'DataPembayaran!$C$4:$C$'.$excel->getSheetByName('DataPembayaran')->getHighestRow(), NULL, 3),
            new PHPExcel_Chart_DataSeriesValues('String', 'DataPembayaran!$D$4:$D$'.$excel->getSheetByName('DataPembayaran')->getHighestRow(), NULL, 3)
        );

        $series_datapembayaran = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,               // plotType
            PHPExcel_Chart_DataSeries::GROUPING_STANDARD,           // plotType
            range(0, count($dataseriesValues_datapembayaran) - 1),  // plotOrder
            $dataseriesLabels_datapembayaran,                       // plotLabel
            $xAxisTickValues_datapembayaran,                        // plotCategory
            $dataseriesValues_datapembayaran                        // plotValues
        );

        $plotarea_datapembayaran = new PHPExcel_Chart_PlotArea(NULL, array($series_datapembayaran));
        $legend_datapembayaran = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, FALSE);
        $title_datapembayaran = new PHPExcel_Chart_Title('Chart Data Pembayaran Pasien RJ');

        $chart_datapembayaran = new PHPExcel_Chart(
            'chart1',                   // name
            $title_datapembayaran,      // title
            $legend_datapembayaran,     // legend
            $plotarea_datapembayaran,   // plotArea
            TRUE,                       // plotVisibleOnly
            0,                          // displayBlanksAs
            new PHPExcel_Chart_Title('BULAN'),           // xAxisLabel
            new PHPExcel_Chart_Title('Jumlah')            // yAxisLabel
        );

        $chart_datapembayaran->setTopLeftPosition('G1');
        $chart_datapembayaran->setBottomRightPosition('Y10');
        $excel->getSheetByName('DataPembayaran')->addChart($chart_datapembayaran);

        $excel->getSheetByName('DataPembayaran')->getStyle('A2:' . $excel->getSheetByName('DataPembayaran')->getHighestColumn() . $excel->getSheetByName('DataPembayaran')->getHighestRow())->applyFromArray($styleArray);
//END SHEET 5

        $excel->setActiveSheetIndex(0);
        $datenow = date('d/m/Y');
        $excel->getActiveSheet()->getStyle('A3:' . $excel->getActiveSheet()->getHighestColumn() . $excel->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $object_writer->setIncludeCharts(TRUE);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Report Pasien per '.$tgl_awal_rj.' - '.$tgl_akhir_rj.'.xlsx"');
        $object_writer->save('php://output');
    }
//END PRINT REPORT RM BARU


//START GET DATA CHART
    function getDataChart($pilih_data ,$charts_perbandingan1, $pilih_data, $tahun_charts, $bulan_charts, $tgl_awal_charts, $tgl_akhir_charts){
        $tahun_charts = str_replace("%20"," ",$tahun_charts);
        $bulan_charts = str_replace("%20"," ",$bulan_charts);
        $tgl_awal_charts = str_replace("%20"," ",$tgl_awal_charts);
        $tgl_akhir_charts = str_replace("%20"," ",$tgl_akhir_charts);
        $data1 = $this->Laporan_rm_model->getDataPenyakitRAW($pilih_data, $charts_perbandingan1, $tahun_charts, $bulan_charts, $tgl_awal_charts, $tgl_akhir_charts);

        echo json_encode($data1);
    }
//END GET DATA CHART







     function exportToExcelDinkes($tgl_awal_rj, $tgl_akhir_rj){
        $tgl_awal_rj = str_replace("%20"," ",$tgl_awal_rj);
        $tgl_akhir_rj = str_replace("%20"," ",$tgl_akhir_rj);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("LAPORAN DINKES KB");
        $object->getActiveSheet()
                        ->getStyle("A6:L6")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A6:L6")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $object->getActiveSheet()
                        ->getStyle("C7:K7")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("C7:K7")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $object->getActiveSheet() //kiri
                                ->mergeCells('A3:B3')
                                ->setCellValueByColumnAndRow(0,3,"BULAN / TAHUN")
                                ->mergeCells('C3:D3') //
                                ->setCellValueByColumnAndRow(2,3,": ". date('F Y', strtotime($tgl_awal_rj)))
                                ->mergeCells('A4:B4')
                                ->setCellValueByColumnAndRow(0,4,"KELURUHAN")
                                ->mergeCells('C4:D4') //
                                ->setCellValueByColumnAndRow(2,4,": PANDANWANGI");
        $object->getActiveSheet() //kanan
                                ->mergeCells('I3:J3')
                                ->setCellValueByColumnAndRow(8,3,"NAMA RS")
                                ->mergeCells('K3:L3') //
                                ->setCellValueByColumnAndRow(10,3,": RSIA PURI BUNDA")
                                ->mergeCells('I4:J4')
                                ->setCellValueByColumnAndRow(8,4,"ALAMAT")
                                ->mergeCells('K4:L4') //
                                ->setCellValueByColumnAndRow(10,4,": JL. SIMPANG SULFAT");



        $object->getActiveSheet() //judul
                                ->mergeCells('A1:L1')
                                ->mergeCells('A2:L2')
                                ->setCellValueByColumnAndRow(0,1,"BKPM KOTA MALANG TAHUN ". date('Y', strtotime($tgl_awal_rj)))
                                ->setCellValueByColumnAndRow(0,2,"KB KONDOM");
        $object->getActiveSheet() //NO
                                ->mergeCells('A6:A7')
                                ->setCellValueByColumnAndRow(0,6,"NO");
        $object->getActiveSheet() // NO REG
                                ->mergeCells('B6:B7')
                                ->setCellValueByColumnAndRow(1,6,"NO REG");
        $object->getActiveSheet() // NAMA
                                ->mergeCells('C6:D6')
                                ->setCellValueByColumnAndRow(2,6,"NAMA");
        $object->getActiveSheet() // NAMA AKSEP & SUAMI
                                ->setCellValueByColumnAndRow(2,7,"AKSEP")
                                ->setCellValueByColumnAndRow(3,7,"SUAMI");
        $object->getActiveSheet() // UMUR
                                ->mergeCells('E6:F6')
                                ->setCellValueByColumnAndRow(4,6,"UMUR");
        $object->getActiveSheet() // UMUR AKSEP & SUAMI
                                ->setCellValueByColumnAndRow(4,7,"AKSEP")
                                ->setCellValueByColumnAndRow(5,7,"SUAMI");
        $object->getActiveSheet() // ALKON
                                ->mergeCells('G6:G7')
                                ->setCellValueByColumnAndRow(6,6,"ALKON");
        $object->getActiveSheet() // PEKERJAAN
                                ->mergeCells('H6:I6')
                                ->setCellValueByColumnAndRow(7,6,"PEKERJAAN");
        $object->getActiveSheet() // PEKERJAAN AKSEP & SUAMI
                                ->setCellValueByColumnAndRow(7,7,"AKSEP")
                                ->setCellValueByColumnAndRow(8,7,"SUAMI");
        $object->getActiveSheet() // PELAYANAN
                                ->mergeCells('J6:K6')
                                ->setCellValueByColumnAndRow(9,6,"PELAYANAN");
        $object->getActiveSheet() // PELAYANAN AKSEP & SUAMI
                                ->setCellValueByColumnAndRow(9,7,"AKSEP")
                                ->setCellValueByColumnAndRow(10,7,"SUAMI");
        $object->getActiveSheet() // ALAMAT
                                ->mergeCells('L6:L7')
                                ->setCellValueByColumnAndRow(11,6,"ALAMAT");
        $center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $object->getActiveSheet()->getStyle("A1:L1")->applyFromArray($center);
        $object->getActiveSheet()->getStyle("A2:L2")->applyFromArray($center);
        // $sheet->getStyle("A1:L1")->applyFromArray($center);



        $dinkes = $this->Laporan_rm_model->getDataLapDinkes($tgl_awal_rj, $tgl_akhir_rj);
        // echo "<pre>";
        // print_r($dinkes);
        // echo "</pre>";
        // die();
        $excel_row = 8;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($dinkes as $row){
            $no++;
            $object->getActiveSheet()
                                    ->setCellValueByColumnAndRow(0, $excel_row, $no)
                                    ->setCellValueByColumnAndRow(1, $excel_row, $row->no_pendaftaran)
                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->pasien_nama)
                                    ->setCellValueByColumnAndRow(3, $excel_row, $row->nama_suami_istri)
                                    ->setCellValueByColumnAndRow(4, $excel_row, $row->umur)
                                    ->setCellValueByColumnAndRow(5, $excel_row, $row->umur_suami_istri)
                                    ->setCellValueByColumnAndRow(6, $excel_row, $row->daftartindakan_nama)
                                    ->setCellValueByColumnAndRow(7, $excel_row, $row->askep)
                                    ->setCellValueByColumnAndRow(8, $excel_row, $row->suami)
                                    ->setCellValueByColumnAndRow(9, $excel_row, "")
                                    ->setCellValueByColumnAndRow(10, $excel_row, "")
                                    ->setCellValueByColumnAndRow(11, $excel_row, $row->pasien_alamat);
            $excel_row++;
        }

        foreach(range('A','L') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $datenow = date('d/m/Y');
        $object->getActiveSheet()->getStyle('A6:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="LAPORAN DINKES KB '.$tgl_awal_rj.' - '.$tgl_akhir_rj.'.xlsx"');
        $object_writer->save('php://output');
    }





        function exportToExcelPendaftaran($tgl_awal_rj, $tgl_akhir_rj){
        $tgl_awal_rj = str_replace("%20"," ",$tgl_awal_rj);
        $tgl_akhir_rj = str_replace("%20"," ",$tgl_akhir_rj);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Report Pendaftaran");
         $object->getActiveSheet()
                        ->getStyle("A4:H4")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A4:H4")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');
        $object->getActiveSheet()
                                ->mergeCells('A1:H1')
                                ->setCellValueByColumnAndRow(0,1,"Report dari tanggal $tgl_awal_rj - $tgl_akhir_rj");
        $table_columns = array("RM", "NAMA","UMUR","JK","TANGGAL","KATEGORI","URUTAN","DOKTER PJ");
        $column = 0;
        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 4, $field);
            $column++;
        }

        $pendaftaran = $this->Laporan_rm_model->getDataPendaftaranPoli($tgl_awal_rj, $tgl_akhir_rj);
        // var_dump($pendaftaran); die();
        $excel_row = 5;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($pendaftaran as $row){
            $no++;
            $hariini = date('Y');
            $lahir = date('Y', strtotime($row->tanggal_lahir));
            $hitung = $hariini - $lahir;
            if($hitung <= 5){
                $kategori_px = "Balita";
            }else if($hitung >= 6 && $hitung <= 12){
                $kategori_px = "Anak - anak";
            }else if($hitung >= 13 && $hitung <= 17){
                $kategori_px = "Remaja";
            }else if($hitung >= 18 || $row->status_kawin == "Kawin"){
                $kategori_px = "Dewasa";
            }else{
                $kategori_px = " ";
            }

            if($row->jenis_kelamin == "Laki-laki"){
                $jk = "L";
            }else if($row->jenis_kelamin == "Perempuan"){
                $jk = "P";
            }else{
                $jk = "";
            }
            $object->getActiveSheet()
                                    ->setCellValueByColumnAndRow(0, $excel_row, $row->no_rekam_medis)
                                    ->setCellValueByColumnAndRow(1, $excel_row, $row->pasien_nama)
                                    ->setCellValueByColumnAndRow(2, $excel_row, $row->umur)
                                    ->setCellValueByColumnAndRow(3, $excel_row, $jk)
                                    ->setCellValueByColumnAndRow(4, $excel_row, date('d/m/Y', strtotime($row->tanggal_lahir)))
                                    ->setCellValueByColumnAndRow(5, $excel_row, $kategori_px)
                                    ->setCellValueByColumnAndRow(6, $excel_row, "")
                                    ->setCellValueByColumnAndRow(7, $excel_row, $row->NAME_DOKTER);
                                    // ->setCellValueByColumnAndRow(8, $excel_row, );
            $excel_row++;
            // var_dump($excel_row);
        }
        // die();

        foreach(range('A','H') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $datenow = date('d/m/Y');
        $object->getActiveSheet()->getStyle('A4:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Pendaftaran Poli '.$tgl_awal_rj.' - '.$tgl_akhir_rj.'.xlsx"');
        $object_writer->save('php://output');
    }



}
