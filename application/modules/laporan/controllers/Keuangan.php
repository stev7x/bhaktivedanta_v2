<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller{
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Keuangan_model');
        $this->load->model('Models');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('laporan_keuangan_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms = "laporan_keuangan_view";
        $comments = "Laporan Keuangan";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_laporan_keuangan', $this->data);
    }

    public function ajax_list_pemasukan () {
        $tgl_awal = $this->input->get('tgl_awal');
        $tgl_akhir = $this->input->get('tgl_akhir');
        $data_kasir = (array) $this->Keuangan_model->kasir();
        $data_pembayaran = (array) $this->Keuangan_model->pembayaran();

//        $pasien = $this->Keuangan_model->getPasien_by_id('115');
//        echo "<pre>";
//        print_r($pasien);
//        exit;

        $data = array();

        foreach ($data_kasir as $key => $value) {
            $pembayarankasir_id = @$this->Keuangan_model->get_tindakan_umum_sudahbayar($value['pendaftaran_id'])[0]->pembayarankasir_id ? $this->Keuangan_model->get_tindakan_umum_sudahbayar($value['pendaftaran_id'])[0]->pembayarankasir_id : 0;
            if ($pembayarankasir_id == 0) {
                $con = array(
                    'pasien_id' => $value['pasien_id'],
                    'pendaftaran_id' => $value['pendaftaran_id']
                );

                if (@$data_tindakan_dokter = $this->Models->where("rawatjalan_penjualan_obat", $con)[0]) {
                    if ($data_tindakan_dokter['pembayaran_id'] != 0) {
                        $pembayarankasir_id = $data_tindakan_dokter['pembayaran_id'];
                    }
                }
            }

            foreach ($data_pembayaran as $key1 => $value1) {
                if ($value1['pembayarankasir_id'] == $pembayarankasir_id) {
                    $pendaftaran = $this->Keuangan_model->get_pendaftaran_pasien($value1['pendaftaran_id']);
                    @$pasien = $this->Keuangan_model->getPasien_by_id($value1['pasien_id'])[0];

                    $nod1 = $nod2 = $nod3 = $bank = $batch = $no_rek = $nama_rek = $nama_asuransi_1 = $no_asuransi_1 = $nama_asuransi_2 = $no_asuransi_2 ='-';
                    @$perawat = $this->Models->where("t_fee_dokter_perawat", [
                        'pasien_id' => $value['pasien_id'],
                        'pendaftaran_id' => $value['pendaftaran_id']
                    ])[0];

                    if ($perawat['dokter_id_1'] != 0)
                        $nod1 = $this->Models->where("masterdata_perawat", ['id' => $perawat['dokter_id_1']])[0]['nama'];
                    if ($perawat['dokter_id_2'] != 0)
                        $nod2 = $this->Models->where("masterdata_perawat", ['id' => $perawat['dokter_id_2']])[0]['nama'];
                    if ($perawat['dokter_id_3'] != 0)
                        $nod3 = $this->Models->where("masterdata_perawat", ['id' => $perawat['dokter_id_3']])[0]['nama'];

                    @$reservasi = $this->Keuangan_model->getReservasi($pasien->no_rekam_medis);
                    @$hotel = $this->Keuangan_model->getHotel($pasien->id_hotel);
                    @$layanan = $this->Keuangan_model->get_layanan($reservasi->jenis_pelayanan_id);
                    @$typePembayaran = $this->Keuangan_model->getTypePembayaran($pendaftaran->pembayaran_id)[0];
                    $caraPembayaran = $value1['cara_bayar'] == 'Pribadi' ? 'Cash' : $value1['cara_bayar'];
                    $data_bank = $this->Models->all("rawatjalan_bank");

//                    echo "<pre>";
//                    print_r($bank);
//                    exit;

                    if ($caraPembayaran != 'Cash') {
                        if ($caraPembayaran == 'CC') {
                            $bank = $data_bank[$value1['bank_cc_umum']]['nama'];
                            $no_rek = $value1['nomor_cc_kartu_umum'];
                            $batch = $value1['nomor_cc_batch_umum'];
                            $nama_rek = '-';
                        }

                        if ($caraPembayaran == 'EDC') {
                            $bank = $data_bank[$value1['bank_edc_umum']]['nama'];
                            $no_rek = $value1['nomor_edc_kartu_umum'];
                            $batch = $value1['nomor_edc_batch_umum'];
                            $nama_rek = '-';
                        }

                        if ($caraPembayaran == 'TF') {
                            $bank = $data_bank[$value1['bank_tf_umum']]['nama'];
                            $no_rek = $value1['nomor_tf_kartu_umum'];
                            $batch = $value1['nomor_tf_batch_umum'];
                            $nama_rek = '-';
                        }
                    }

                    $nama_asuransi_1 = $value1['nama_asuransi_1'] != '' ? $value1['nama_asuransi_1'] : '-';
                    $no_asuransi_1 = $value1['no_asuransi_1'] != '' ? $value1['no_asuransi_1'] : '-';
                    $nama_asuransi_2 = $value1['nama_asuransi_2'] != '' ? $value1['nama_asuransi_2'] : '-';
                    $no_asuransi_2 = $value1['no_asuransi_2'] != '' ? $value1['no_asuransi_2'] : '-';

                    $transport = "";
                    if (((@$reservasi->driver_id != NULL) && (@$reservasi->driver_id != 0)) || ((@$reservasi->driver_id_2 != NULL) && (@$reservasi->driver_id_2 != 0))) {
                        $transport = "Driver :";
                        if ((@$reservasi->driver_id != NULL) && (@$reservasi->driver_id != 0)) {
                            $transport = $transport . "<br>- " . $this->Models->where("masterdata_perawat", ['id' => $reservasi->driver_id])[0]['nama'];
                        }

                        if ((@$reservasi->driver_id_2 != NULL) && (@$reservasi->driver_id_2 != 0)) {
                            $transport = $transport . "<br>- " . $this->Models->where("masterdata_perawat", ['id' => $reservasi->driver_id_2])[0]['nama'];
                        }

                        $transport = $transport . "<br>Jenis Layanan : " . $layanan->nama_layanan . "<br>Waktu : " . @$reservasi->pemanggilan;
                    } else {
                        $transport = "-";
                    }

                    $row = array();
                    $row[] = $value1['tgl_pembayaran'];
                    $row[] = $this->session->tempdata('data_session')[2];
                    $row[] = $pendaftaran->no_pendaftaran;
                    $row[] = @$pasien->pasien_nama;
                    $row[] = @$pasien->no_rekam_medis;
                    $row[] = "Rp. " . number_format($value1['totalbiayadokter'] == NULL ? 0 : $value1['totalbiayadokter']);
                    $row[] = "Rp. " . number_format($value1['totalbiayaobat'] == NULL ? 0 : $value1['totalbiayaobat']);
                    $row[] = "Rp. " . number_format($value1['totalbiayatindakan'] == NULL ? 0 : $value1['totalbiayatindakan']);
                    $row[] = "Rp. " . number_format($value1['totalbiayaescort'] == NULL ? 0 : $value1['totalbiayaescort']);
                    $row[] = "Rp. " . number_format($value1['totalbiayalab'] == NULL ? 0 : $value1['totalbiayalab']);
                    $row[] = "Rp. " . number_format($value1['total_discount'] == NULL ? 0 : $value1['total_discount']);
                    $row[] = "Rp. " . number_format($value1['total_bayar'] == NULL ? 0 : ($value1['total_bayar'] - $value1['surcharge']));
                    $row[] = $typePembayaran->type_pembayaran;
                    $row[] = $bank;
                    $row[] = $batch;
                    $row[] = $no_rek;
                    $row[] = $nama_rek;
                    $row[] = $caraPembayaran;
                    $row[] = $nama_asuransi_1;
                    $row[] = $no_asuransi_1;
                    $row[] = $nama_asuransi_2;
                    $row[] = $no_asuransi_2;
                    $row[] = "Rp. " . number_format($value1['surcharge'] == NULL ? 0 : $value1['surcharge']);
                    $row[] = $pendaftaran->NAME_DOKTER;
                    $row[] = $nod1;
                    $row[] = $nod2;
                    $row[] = $nod3;
                    $row[] = $transport;
                    $row[] = "Rp. " . number_format($value1['total_hp_obat'] == NULL ? 0 : $value1['total_hp_obat']);
                    $row[] = @$hotel->nama_hotel;
                    $row[] = @$reservasi->pic_hotel;
                    $row[] = @$layanan->nama_layanan;
                    $data[] = $row;
                }
            }
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function ajax_list_pengeluaran(){
        $tgl_awal = $this->input->get('tgl_awal');
        $tgl_akhir = $this->input->get('tgl_akhir');

        $data = array(

        );

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function get_fee_dokter($hotel_id = false, $jenis_pasien, $type_call, $shift) {
        $shift = ($shift == "PAGI") ? 1 : 2;
        $result = 0;
        $feeDokterShift = $this->Models->where('fee_dokter_by_shift', array('shift' => $shift, 'branch' => $this->session->tempdata('data_session')[2]))[0];

        if ($type_call < 4) {
            if ($type_call == 3) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['vs_local'] = 0;
                    $feeDokterHotel['vs_domestik'] = 0;
                    $feeDokterHotel['vs_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['vs_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['vs_domestik'];
                } else {
                    $result = $feeDokterHotel['vs_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['vs_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['vs_dome'];
                } else {
                    $result = $feeDokterShift['vs_asing'];
                }
            }
        } else {
            if ($type_call == 5) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['oc_local'] = 0;
                    $feeDokterHotel['oc_domestik'] = 0;
                    $feeDokterHotel['oc_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['oc_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['oc_domestik'];
                } else {
                    $result = $feeDokterHotel['oc_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['oc_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['oc_dome'];
                } else {
                    $result = $feeDokterShift['oc_asing'];
                }
            }
        }

        return $result;
    }

    public function getFeeDokter($hotel_id, $jenis_pasien, $type_call, $hour) {
        $shift = ($hour >= 8) && ($hour < 22) ? 1 : 2;
        $result = 0;

        $feeDokterShift = $this->Keuangan_model->get_fee_dokter_by_shift($shift)[0];
        //print_r($feeDokterHotel[0]->oc_domestic);

        if (($hotel_id != 0) || ($hotel_id != NULL)) {
            $feeDokterHotel = $this->Keuangan_model->get_fee_dokter_by_hotel($hotel_id)[0];

            if ($type_call == 3) {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel->vs_domestic;
                } else {
                    $result = $feeDokterHotel->vs_guest;
                }
            } else if ($type_call == 5) {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel->oc_domestic;
                } else {
                    $result = $feeDokterHotel->oc_guest;
                }
            } else if (($type_call == 2) || ($type_call == 3)) {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift->vs_lokal;
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift->vs_dome;
                } else {
                    $result = $feeDokterShift->vs_asing;
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift->oc_lokal;
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift->oc_dome;
                } else {
                    $result = $feeDokterShift->oc_asing;
                }
            }
        } else {
            if ($type_call < 4) {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift->vs_lokal;
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift->vs_dome;
                } else {
                    $result = $feeDokterShift->vs_asing;
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift->oc_lokal;
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift->oc_dome;
                } else {
                    $result = $feeDokterShift->oc_asing;
                }
            }

        }

        return $result;
    }

    public function data_laporan() {
        $data = array();

        $getPasien = $this->Keuangan_model->getPasien();
        foreach ($getPasien as $item) {
            $get_pembayarankasir = $this->Keuangan_model->get_pembayarankasir($item->pendaftaran_id, $item->pasien_id);
            $feeDokter = $this->Keuangan_model->get_fee_dokter($item->pasien_id, $item->dokter_id);
            $obat = $this->Keuangan_model->get_obat($get_pembayarankasir->pembayarankasir_id);
            $tindakan = $this->Keuangan_model->get_tindakan_pasien($get_pembayarankasir->pembayarankasir_id);
            $total_bayar = $this->Keuangan_model->get_print_bayar($get_pembayarankasir->pembayarankasir_id);
            $pasienrj = $this->Keuangan_model->get_pendaftaran_pasien($item->pendaftaran_id);
            $feeDokterAll = $this->Keuangan_model->getFeeDokter($item->pasien_id);
            $hotel = $this->Keuangan_model->getHotel($item->id_hotel);
            $reservasi = $this->Keuangan_model->getReservasi($item->no_rekam_medis);
            $layanan = $this->Keuangan_model->get_layanan($reservasi->jenis_pelayanan_id);

            $feeDokter = $this->get_fee_dokter($reservasi->hotel_id, $layanan->id_layanan, $pasienrj->type_call, $reservasi->pemanggilan);
            $feeObat = $this->Keuangan_model->get_fee_obat_by_pasien($pasienrj->pasien_id)[0]->total;
            $feeTindakan = $this->Keuangan_model->get_fee_tindakan_by_pasien($pasienrj->pasien_id)[0]->total;
            $feeEscort = $this->Keuangan_model->get_fee_escort_by_pasien($pasienrj->pasien_id)[0]->total;
            $feeLab = $this->Keuangan_model->get_fee_lab_by_pasien($pasienrj->pasien_id)[0]->total;
            $typePembayaran = $this->Keuangan_model->get_fee_surcharge_by_pembayaran($reservasi->pembayaran_id)[0]->type_pembayaran;
            $feeSurcharge = $typePembayaran == "Tanggungan" ? 0 : (0.03 * $feeObat);
            $transport = "-";
            if ($reservasi->jenis_pelayanan_id > 3) {
                $transport = $this->Keuangan_model->get_driver_name($reservasi->driver_id)[0]->nama . " - " . date('H:i:s', strtotime($reservasi->tgl_created));
            }
            $hpObat = $this->Keuangan_model->get_hp_obat($pasienrj->pasien_id);

            $namaPerawat = "";
            foreach ($feeDokterAll as $itemFeeDokter) {
                $dataPerawat = $this->Keuangan_model->getDataPerawat($itemFeeDokter->dokter_id);
                if (!empty($dataPerawat)) {
                    $namaPerawat .= $dataPerawat->NAME_DOKTER . ", ";
                }
            }

            $surcharge = 0;
            foreach($obat as $list){
                $surcharge += $list->total_harga;
            }
            $surcharge = $surcharge * 0.3;

            if (empty($feeDokter)) {
                $feeDokter = 0;
            }

            // print_r($feeDokter); die();
            $dataObat = "";
            foreach ($obat as $itemObat) {
                $dataObat .= $itemObat->nama_barang . ", ";
            }

            $dataTindakan = "";
            foreach ($tindakan as $itemTindakan) {
                if ($itemTindakan->type_tindakan == "TINDAKAN")
                    $dataTindakan .= $itemTindakan->daftartindakan_nama . ", ";
            }

            $dataLab = "";
            foreach ($tindakan as $itemTindakan) {
                if ($itemTindakan->type_tindakan == "LABORATORIUM")
                    $dataLab .= $itemTindakan->daftartindakan_nama . ", ";
            }

            $row = array();
            $row[] = $get_pembayarankasir->tgl_pembayaran;
            $row[] = $item->no_pendaftaran;
            $row[] = $item->pasien_nama;
            $row[] = $item->no_rekam_medis;
            $row[] = "Rp. ".number_format($feeDokter == NULL ? 0 : $feeDokter);
            $row[] = "Rp. ".number_format($feeObat == NULL ? 0 : $feeObat);
            $row[] = "Rp. ".number_format($feeTindakan == NULL ? 0 : $feeTindakan);
            $row[] = "Rp. ".number_format($feeEscort == NULL ? 0 : $feeEscort);
            $row[] = "Rp. ".number_format($feeLab == NULL ? 0 : $feeLab);
            $row[] = "Rp. ".number_format($feeDokter + $feeObat + $feeTindakan + $feeEscort + $feeLab);
            $row[] = "Rp. ". number_format($feeSurcharge == NULL ? 0 : $feeSurcharge);
            $row[] = $pasienrj->NAME_DOKTER;
            $row[] = $namaPerawat;
            $row[] = $transport;
            $row[] = "Rp. ". number_format($hpObat == NULL ? 0 : $hpObat);
            $row[] = $hotel->nama_hotel;
            $row[] = $reservasi->pic_hotel;
            $row[] = $layanan->nama_layanan;
            $data[]= $row;
        }

        return $data;
    }

    function export_laporan_keuangan() {
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Laporan Keuangan");
        $object->getActiveSheet()
            ->getStyle("A3:R3")
            ->getFont()
            ->setSize(14)
            ->setBold(true)
            ->getColor()
            ->setRGB('FFFFFF');
        $object->getActiveSheet()
            ->getStyle("A3:R3")
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('01c0c8');

        $object->getActiveSheet()
            ->mergeCells('A1:R1')
            ->setCellValueByColumnAndRow(0,1,"Laporan keuangan");

        $table_columns = array("TGL", "No Bill","Nama Pasien","Kode","Doctor Fee","Medicine","Other","Escort","Lab","Total","Surcharge","DOD","NOD","Transport","Hp Obat","Nama Hotel","JP","Keterangan");
        $column = 0;
        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 3, $field);
            $column++;
        }

        $data = $this->data_laporan();

//        echo "<pre>";
//        print_r($data);
//        exit;
        $excel_row = 4;
        foreach($data as $row){
            $object->getActiveSheet()
                ->setCellValueByColumnAndRow(0, $excel_row, $row[0])
                ->setCellValueByColumnAndRow(1, $excel_row, $row[1])
                ->setCellValueByColumnAndRow(2, $excel_row, $row[2])
                ->setCellValueByColumnAndRow(3, $excel_row, $row[3])
                ->setCellValueByColumnAndRow(4, $excel_row, $row[4])
                ->setCellValueByColumnAndRow(5, $excel_row, $row[5])
                ->setCellValueByColumnAndRow(6, $excel_row, $row[6])
                ->setCellValueByColumnAndRow(7, $excel_row, $row[7])
                ->setCellValueByColumnAndRow(8, $excel_row, $row[8])
                ->setCellValueByColumnAndRow(9, $excel_row, $row[9])
                ->setCellValueByColumnAndRow(10, $excel_row, $row[10])
                ->setCellValueByColumnAndRow(11, $excel_row, $row[11])
                ->setCellValueByColumnAndRow(12, $excel_row, $row[12])
                ->setCellValueByColumnAndRow(13, $excel_row, $row[13])
                ->setCellValueByColumnAndRow(14, $excel_row, $row[14])
                ->setCellValueByColumnAndRow(15, $excel_row, $row[15])
                ->setCellValueByColumnAndRow(16, $excel_row, $row[16])
                ->setCellValueByColumnAndRow(17, $excel_row, $row[17]);
            $excel_row++;
        }
        foreach(range('A','R') as $columnID) {
            $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $datenow = date('d/m/Y');
        $object->getActiveSheet()->getStyle('A3:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Laporan Keuangan.xlsx"');
        $object_writer->save('php://output');
    }
}
