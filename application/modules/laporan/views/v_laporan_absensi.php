<?php $this->load->view('header');?>
<!-- <title>SIMRS | Dashboard</title> -->
<?php $this->load->view('sidebar');?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"> Laporan Absensi </h4> </div>
                <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Laporan</a></li>
                        <li class="active">Laporan Absensi</li> 
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!--row --> 
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-info1"> 
                        <div class="panel-heading">Data Laporan Absensi
                            <!-- <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div> -->
                        </div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">

                                <ul class="nav customtab nav-tabs" role="tablist">
                                    <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#home1" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Absensi Perorangan</span></a></li>
                                    <li id="tab_klinik" role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#profile1" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Absensi Perklinik</span></a></li>  

                                </ul>
                                <!-- Tab panes --> 
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="home1">
                                       <div class="row">
                                        <div class="form-group col-md-2">
                                            <label for="inputName1" class="control-label"></label>
                                            <!-- <input type="number" class="form-control" id="inputName1" placeholder="No.Rekam Medis" required> -->

                                            <dl class="text-right">  
                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                <dd>Ketik / pilih untuk mencari atau filter data </dd>  
                                            </dl>

                                        </div>
                                        <div class="col-md-10"> 
                                            <div class="col-md-3">
                                                <label>Tanggal Absensi,dari</label>
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                </div>
                                            </div> 
                                            <div class="col-md-3">   
                                                <label>Sampai</label>    
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">  
                                                <label for="inputName1" class="control-label">Nama :</label>
                                                <input type="text" id="nama_users" maxlength="8" name="nama_users" class="form-control autocomplete" placeholder=" Cari Users....." >
                                                <input type="hidden" id="id_user" name="id_user">

                                            </div>   
                                            <div class="col-md-3"> 
                                                <label>&nbsp;</label><br>    
                                                <button type="button" class="btn btn-success col-md-12" id="searchLaporan" >Cari</button>        
                                            </div>

                                        </div> 
                                        </div><hr >
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-warning" onclick="exportToExcelAbsensiPerorangan()">EXPORT TO EXCEL</button>
                                            </div>
                                        </div><br> 

                                        <table id="table_absensi_perorangan" class="table table-stripped table-hover  table-responsive" cellspacing="0">
                                            <thead> 
                                              <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Branch</th>
                                                <th>Todo</th>
                                                <th>Absen Data</th>
                                                <th>Status</th>

                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="15">No Data to Display</td>
                                                </tr>
                                            </tbody>
                                        </table>      


                                        <div class="clearfix"></div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="profile1">
                                       <div class="row">
                                        <div class="form-group col-md-2">
                                            <label for="inputName1" class="control-label"></label>
                                            <!-- <input type="number" class="form-control" id="inputName1" placeholder="No.Rekam Medis" required> -->

                                            <dl class="text-right">  
                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                <dd>Ketik / pilih untuk mencari atau filter data </dd>  
                                            </dl>

                                        </div>
                                        <div class="col-md-10"> 
                                            <div class="col-md-3">
                                                <label>Tanggal Absensi,Dari</label>
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_awal" id="tgl_awal_rd" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                </div>
                                            </div> 
                                            <div class="col-md-3">   
                                                <label>Sampai</label>    
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir_rd" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">  
                                                <label for="inputName1" class="control-label">Nama Klinik :</label>
                                                <select name="" class="form-control select" style="margin-bottom: 7px" id="nama_klinik" >          
                                                    <option value="" disabled selected>Pilih</option>
                                                    <?php foreach ($daftar_klinik as $val){
                                                            echo '<option value="'.$val->id_branch.'">'.$val->nama.'</option>';
                                                    }

                                                    ?>
                                                          
                                                </select>
                                            </div>   
                                            <div class="col-md-3"> 
                                                <label>&nbsp;</label><br>    
                                                <button type="button" class="btn btn-success col-md-12" id="searchLaporanRd" >Cari</button>        
                                            </div>

                                        </div> 
                                        </div><hr >
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-warning" onclick="printToExcelRd()">EXPORT TO EXCEL</button>
                                            </div>
                                        </div><br> 

                                        <table id="table_absensi_klinik" class="table table-stripped table-hover  table-responsive" cellspacing="0">
                                            <thead> 
                                              <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Branch</th>
                                                <th>Todo</th>
                                                <th>Absen Data</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="15">No Data to Display</td>
                                                </tr>
                                            </tbody>
                                        </table>      


                                        <div class="clearfix"></div>
                                    </div>

                        </div>        
                    </div>
                </div>  
            </div>

        </div>
    </div>
    <!--/row -->
</div>
<!-- /.container-fluid -->

<?php $this->load->view('footer');?>
