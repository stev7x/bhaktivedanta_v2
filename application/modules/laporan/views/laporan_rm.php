<?php $this->load->view('header');?>
<title>SIMRS | Laporan Rekam Medis</title>
<?php $this->load->view('sidebar');?>
      <!-- START CONTENT -->
      <section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Laporan Rekam Medis</h5>
                <ol class="breadcrumbs">
                    <li><a href="#"></a></li>
                    <li class="active">Laporan Rekam Medis</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->
        <!--start container-->
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <h4 class="header">Laporan Rekam Medis</h4>
                            <div class="row">
                                <div class="col s12">
                                    <ul class="tabs tab-demo z-depth-1">
                                        <li class="tab col s4"><a class="active" href="#rawat_jelan_tab">Rawat Jalan</a>
                                        </li>
                                        <li class="tab col s4"><a href="#igd_tab">IGD</a>
                                        </li>
                                        <li class="tab col s4"><a href="#rawat_inap_tab">Rawat Inap</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col s12">
                                    <div id="rawat_jelan_tab" class="col s12">
                                        <div>&nbsp;</div>
                                        <div id="table-datatables">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="row">
                                                        <div class="input-field col s6">
                                                            <i class="mdi-action-event prefix"></i>
                                                            <input type="text" class="datepicker_rj_awal" name="tgl_awal_rj" id="tgl_awal_rj" placeholder="Tanggal Awal" value="<?php echo date('d F Y'); ?>">
                                                            <label for="tgl_awal">Tanggal Pendaftaran, Dari</label>
                                                        </div>
                                                        <div class="input-field col s6">
                                                            <i class="mdi-action-event prefix"></i>
                                                            <input type="text" class="datepicker_rj_akhir" name="tgl_akhir_rj" id="tgl_akhir_rj" placeholder="Tanggal Akhir" value="<?php echo date('d F Y'); ?>">
                                                            <label for="tgl_akhir">Sampai</label>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <div class="row">
                                                        <div class="input-field col s6">
                                                            <select id="poliklinik_rj" name="poliklinik_rj" class="validate">
                                                                <option value="" disabled selected>Poliklinik</option>
                                                                <?php
                                                                $list_poli = $this->Laporan_rm_model->get_ruangan();
                                                                foreach($list_poli as $list){
                                                                    echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                            <label for="poliklinik_rj">Ruangan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <div class="row">
                                                        <div class="input-field col s12">
                                                            <button type="button" class="btn light-green waves-effect waves-light darken-4" title="Cari Pasien" id="searchLaporanRj">Search</button>
                                                            <button type="button" class="btn yellow waves-effect waves-light darken-4" title="Cari Pasien" onclick="printToExcelRj()">Export To Excel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    &nbsp;
                                                </div>
                                                <div class="col s12 m4 l12">
                                                    <div id="dvData">
                                                    <table id="table_rawat_jalan" class="responsive-table display" cellspacing="0">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Tgl</th>
                                                                <th>Nama</th>
                                                                <th>No RM</th>
                                                                <th>Pembiayaan</th>
                                                                <th>Rujukan</th>
                                                                <th>Poli Klinik</th>
                                                                <th>Dokter</th>
                                                                <th>Diagnosa</th>
                                                                <th>Kode ICD 10</th>
                                                                <th>Terapi</th>
                                                                <th>Petugas RM</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="4">No Data to Display</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="igd_tab" class="col s12">
                                        <div>&nbsp;</div>
                                        <div id="table-datatables">
                                            <div class="row">
                                                <div class="col s12 m4 l12">
                                                    <div class="col s12">
                                                        <div class="row">
                                                            <div class="input-field col s6">
                                                                <i class="mdi-action-event prefix"></i>
                                                                <input type="text" class="datepicker_rd_awal" name="tgl_awal_rd" id="tgl_awal_rd" placeholder="Tanggal Awal" value="<?php echo date('d F Y'); ?>">
                                                                <label for="tgl_awal_rd">Tanggal Pendaftaran, Dari</label>
                                                            </div>
                                                            <div class="input-field col s6">
                                                                <i class="mdi-action-event prefix"></i>
                                                                <input type="text" class="datepicker_rd_akhir" name="tgl_akhir_rd" id="tgl_akhir_rd" placeholder="Tanggal Akhir" value="<?php echo date('d F Y'); ?>">
                                                                <label for="tgl_akhir_rd">Sampai</label>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col s12">
                                                        <div class="row">
                                                            <div class="input-field col s12">
                                                                <button type="button" class="btn light-green waves-effect waves-light darken-4" title="Cari Pasien" id="searchLaporanRd">Search</button>
                                                                <button type="button" class="btn yellow waves-effect waves-light darken-4" title="Cari Pasien" onclick="printToExcelRd()">Export To Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col s12">
                                                        &nbsp;
                                                    </div>
                                                    
                                                    <div class="col s12">
                                                        <table id="table_rawat_darurat" class="responsive-table display" cellspacing="0">
                                                            <thead>
                                                                <tr>
                                                                    <th>Nomor (Bulan)</th>
                                                                    <th>Nomor (Hari)</th>
                                                                    <th>Tgl Masuk</th>
                                                                    <th>No RM</th>
                                                                    <th>Nama Pasien</th>
                                                                    <th>Alamat</th>
                                                                    <th>Gender</th>
                                                                    <th>Umur</th>
                                                                    <th>Dokter</th>
                                                                    <th>Diagnosa</th>
                                                                    <th>Kode ICD 10</th>
                                                                    <th>Kunjungan</th>
                                                                    <th>Keterangan</th>
                                                                    <th>Status Pembayaran</th>
                                                                    <th>Petugas</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="15">No Data to Display</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="rawat_inap_tab" class="col s12">
                                        <div>&nbsp;</div>
                                        <div id="table-datatables">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="row">
                                                        <div class="input-field col s6">
                                                            <i class="mdi-action-event prefix"></i>
                                                            <input type="text" class="datepicker_ri_awal" name="tgl_awal_ri" id="tgl_awal_ri" placeholder="Tanggal Awal" value="<?php echo date('d F Y'); ?>">
                                                            <label for="tgl_awal">Tanggal Masuk, Dari</label>
                                                        </div>
                                                        <div class="input-field col s6">
                                                            <i class="mdi-action-event prefix"></i>
                                                            <input type="text" class="datepicker_ri_akhir" name="tgl_akhir_ri" id="tgl_akhir_ri" placeholder="Tanggal Akhir" value="<?php echo date('d F Y'); ?>">
                                                            <label for="tgl_akhir">Tanggal Pulang</label>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <div class="row">
                                                        <div class="input-field col s6">
                                                            <input type="text" name="nama_pasien_ri" id="nama_pasien_ri" placeholder="Ketik Nama Pasien" value="">
                                                            <label for="nama_pasien_ri">Nama Pasien</label>
                                                        </div>
                                                        <div class="input-field col s6">
                                                            <select id="dokter_pj_ri" name="dokter_pj_ri" class="validate">
                                                                <option value="" disabled selected>Pilih Dokter</option>
                                                                <?php
                                                                $list_dokter = $this->Laporan_rm_model->get_dokter_list();
                                                                foreach($list_dokter as $list){
                                                                    echo "<option value='".$list->NAME_DOKTER."'>".$list->NAME_DOKTER."</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                            <label for="dokter_pj">Dokter Penanggung Jawab</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <div class="row">
                                                        <div class="input-field col s12">
                                                            <button type="button" class="btn light-green waves-effect waves-light darken-4" title="Cari Pasien" id="searchLaporanRi">Search</button>
                                                            <button type="button" class="btn yellow waves-effect waves-light darken-4" title="Cari Pasien" onclick="printToExcelRi()">Export To Excel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    &nbsp;
                                                </div>
                                                <div class="col s12 m4 l12">
                                                    <table id="table_rawat_inap" class="responsive-table display" cellspacing="0">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>No. RM</th>
                                                                <th>Nama Pasien</th>
                                                                <th>Jenis Kelamin</th>
                                                                <th>Tanggal Lahir</th>
                                                                <th>Usia</th>
                                                                <th>Alamat</th>
                                                                <th>Ruang</th>
                                                                <th>Kamar</th>
                                                                <th>Diagnosa Keluar</th>
                                                                <th>Kode Diagnosa</th>
                                                                <th>Dokter IGD</th>
                                                                <th>Dokter Penanggungjawab</th>
                                                                <th>Dokter Anastesi</th>
                                                                <th>Perawat / Bidan</th>
                                                                <th>Tanggal Masuk</th>
                                                                <th>Tanggal Keluar</th>
                                                                <th>Hari Perawatan</th>
                                                                <th>Cara Pulang</th>
                                                                <th>Pembiayaan Pasien</th>
                                                                <th>Keterangan</th>
                                                            </tr>
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="21">No Data to Display</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
    <!-- END CONTENT -->
    

<?php $this->load->view('footer');?>
      