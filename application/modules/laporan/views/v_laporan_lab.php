<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Laporan Laboratorium </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Laporan</a></li>
                            <li class="active">Laporan Laboratorium</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Data Laporan Laboratorium 
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-2">
                                                <label for="inputName1" class="control-label"></label>
                                                    <dl class="text-right">  
                                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                        <dd>type/ select to find or filter data</dd>
                                                    </dl> 
                                                
                                        </div> 
                                        <div class="col-md-10"> 
                                            <div class="col-md-5">
                                                <label>Tanggal Kunjungan,Dari</label>
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                </div>
                                            </div> 
                                            <div class="col-md-5">   
                                                <label>Sampai</label>    
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                </div>
                                            </div>
                                            <div class="col-md-2"> 
                                                <label>&nbsp;</label><br>
                                                <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                            </div>        
                                        </div>
                                       
                                    </div><hr style="margin-top: -27px">
                                    <div class="row">
                                        <div class="col-md-6">  
                                            <button type="button" class="btn btn-warning" onclick="printToExcelLab()"> EXPORT TO EXCEL </button>  
                                        </div>
                                    </div><br>

                                   <table id="table_laporan_laboratorium" class="table table-striped dataTable" cellspacing="0" style="width: 100%;">  
                                        <thead>
                                                <tr>
                                                <th rowspan="2">No</th>
                                                <th rowspan="2">Tgl Sample</th>
                                                <th rowspan="2">Nama</th>
                                                <th rowspan="2">RM</th>
                                                <th rowspan="2">Pengirim</th>
                                                <th colspan="2"><center>Pemeriksaan</center></th>
                                            </tr>
                                            <tr>
                                                <th>Tindakan</th>
                                                <th>Tarif</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="4">No Data to Display</td>
                                            </tr>
                                        </tbody>    
                                    </table>   
                                </div>
                            </div>  
                        </div>
                        
                    </div>
                    
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->  

                
<?php $this->load->view('footer');?>
      