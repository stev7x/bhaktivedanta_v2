<?php $this->load->view('header');?>
<!-- <title>SIMRS | Dashboard</title> -->
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Laporan Rekam Medis </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Laporan</a></li>
                            <li class="active">Laporan Rekam Medis</li> 
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1"> 
                            <div class="panel-heading">Data Laporan Rekam Medis 
                                <!-- <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div> -->
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">

                                    <ul class="nav customtab nav-tabs" role="tablist">
                                        <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#home1" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> RAWAT JALAN</span></a></li>
                                        <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#profile1" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">IGD</span></a></li>  
                                        <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#messages1" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">RAWAT INAP</span></a></li>
                                        <!-- <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#bidan" class="nav-link" aria-controls="bidan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">POLI OBGYN</span></a></li> -->
                                        <li role="presentation" onclick="" class="nav-item" style="width: 100%;text-align: center;"><a href="#charts" class="nav-link" aria-controls="charts" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">CHARTS</span></a></li>
                                    </ul>
                                    <!-- Tab panes --> 
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="home1">
                                        <div class="row">
                                            <div class="form-group col-md-2">
                                                    <label for="inputName1" class="control-label"></label>
                                                    <!-- <input type="number" class="form-control" id="inputName1" placeholder="No.Rekam Medis" required> -->
                                                    <dl class="text-right">  
                                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                        <dd>Ketik / pilih untuk mencari atau filter data</dd>  
                                                    
                                                    </dl>
                                            </div>
                                            <div class="col-md-10"> 
                                                <div class="col-md-3">
                                                    <label>Tanggal Kunjungan,Dari</label>
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_awal" id="tgl_awal_rj" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                    </div>
                                                </div> 
                                                <div class="col-md-3">   
                                                    <label>Sampai</label>    
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir_rj" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">  
                                                    <label for="inputName1" class="control-label">Filter berdasarkan :</label>
                                                    <select name="status_kunjungan" class="form-control select" style="margin-bottom: 7px" id="status_kunjungan" >     
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="PASIEN LAMA">Pasien Lama</option>   
                                                        <option value="PASIEN BARU">Pasien Baru</option>       
                                                    </select>
                                                </div>   
                                                <div class="form-group col-md-3">  
                                                    <label for="inputName1" class="control-label">Export report berdasarkan :</label>
                                                    <select name="berdasarkan" class="form-control select" style="margin-bottom: 7px" id="berdasarkan">     
                                                        <!-- <option value="" disabled selected>Pilih Berdasarkan</option> -->
                                                        <option value="1">Hari</option>   
                                                        <!-- <option value="2">Bulan</option>       
                                                        <option value="3">Tahun</option>        -->
                                                    </select>
                                                </div>   
                                                <div class="col-md-3"> 
                                                    <label>&nbsp;</label><br>
                                                    <!-- <button type="button" style="margin-bottom: 50px;" class="btn btn-success col-md-12" id="searchLaporanPoliKlinikRinci" >Cari</button>         -->
                                                </div>
                                                    
                                            </div> 
                                        </div><hr style="margin-top: -22px">

                                           <ul class="nav nav-tabs" role="tablist">
                                                    <!-- <li role="presentation" class="active nav-item"><a href="#tabs-a" class="nav-link" aria-controls="tabs-a" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Report Pasien RJ</span></a></li> -->
                                                    <li role="presentation" class="active nav-item"><a href="#tabs-b" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-user-md"></i></span><span class="hidden-xs">Report Poli Klinik Rinci</span></a></li>
                                                    <li role="presentation" class="nav-item"><a href="#tabs-c" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Report Jumlah Kunjungan Dokter Poli</span></a></li>  
                                                    <li role="presentation" class="nav-item"><a href="#tabs-d" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Report RM</span></a></li>  
                                                    <li role="presentation" class="nav-item"><a href="#tabs-e" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Report RAJAL</span></a></li>  
                                                    <li role="presentation" class="nav-item"><a href="#tabs-f" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Report Hasil Vaksinasi</span></a></li>  
                                                    <li role="presentation" class="nav-item"><a href="#tabs-g" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Report Pendaftaran</span></a></li>  
                                                    <li role="presentation" class="nav-item"><a href="#tabs-h" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Report DINKES KB</span></a></li>  
                                            </ul>
                                             <div class="tab-content"> 
                                                <div role="tabpanel" class="tab-pane active" id="tabs-b">
                                                    <div class="row">
                                                        <div class="col-md-12">  
                                                            <button type="button" class="btn btn-success col-md-6" id="searchLaporanPoliKlinikRinci" >Cari</button>        
                                                            <button type="button" class="btn btn-warning col-md-6" onclick="exportToExcelPoliUmumRinci()">EXPORT TO EXCEL RINCI</button>
                                                        </div>
                                                    </div> <BR> 
                                                    <div class="table-responsive">
                                                    <table id="table_poli_rinci" class="table table-stripped table-hover" cellspacing="0"> 
                                                        <thead>
                                                                <tr>  
                                                                    <th>No</th>
                                                                    <th>Tgl</th>
                                                                    <th>Poli Klinik</th>
                                                                    <th>Dokter</th> 
                                                                    <th>Nama Pasien</th>
                                                                    <th>Diagnosa</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <tr>   
                                                                    <td colspan="6">No Data to Display</td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="tabs-c">
                                                    <div class="row">
                                                        <div class="col-md-12">  
                                                            <button type="button" class="btn btn-success col-md-6" id="searchLaporanPoliKlinikKeseluruhan" >Cari</button>        
                                                            <button type="button" class="btn btn-warning col-md-6" onclick="exportToExcelPoliUmumKeseluruhan()">EXPORT TO EXCEL KESELURUHAN</button>
                                                        </div>
                                                    </div> <BR> 
                                                    <div class="table-responsive">
                                                    <table id="table_poli_keseluruhan" class="table table-stripped table-hover" cellspacing="0"> 
                                                        <thead>
                                                                <tr>  
                                                                    <th>No</th>
                                                                    <th>Tgl</th>
                                                                    <th>Poli Klinik</th>
                                                                    <th>Dokter</th> 
                                                                    <th>Jumlah Pasien</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <tr>   
                                                                    <td colspan="5">No Data to Display</td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="tabs-d">
                                                    <div class="row">
                                                        <div class="col-md-12">  
                                                            <button type="button" class="btn btn-success col-md-6" id="searchLaporanRmRj" >Cari</button>        
                                                            <button type="button" class="btn btn-warning col-md-6" onclick="exportToExcelRM()">EXPORT TO EXCEL RM</button>
                                                        </div>
                                                    </div> <BR> 
                                                    <div class="table-responsive">
                                                    <table id="table_rm_rj" class="table table-stripped table-hover" cellspacing="0"> 
                                                        <thead>
                                                                <tr>  
                                                                    <th>No</th>
                                                                    <th>Tgl</th>
                                                                    <th>No RM</th>
                                                                    <th>Nama Pasien</th> 
                                                                    <th>Poli Klinik</th>
                                                                    <th>Dokter</th>
                                                                    <th>Pembayaran</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <tr>   
                                                                    <td colspan="7">No Data to Display</td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="tabs-e">
                                                    <div class="row">
                                                        <div class="col-md-12">  
                                                            <button type="button" class="btn btn-success col-md-6" id="searchLaporanPoliObgyn" >Cari</button>        
                                                            <button type="button" class="btn btn-warning col-md-6" onclick="exportToExcelBidan()">EXPORT TO EXCEL RAJAL</button>
                                                        </div>
                                                    </div> <BR> 
                                                    <div class="table-responsive">
                                                    <table id="table_poli_obgyn" class="table table-stripped table-hover" cellspacing="0"> 
                                                        <thead>
                                                                <tr>  
                                                                    <th>No</th>
                                                                    <th>Tgl Pendaftaran</th>
                                                                    <th>No. RM</th>
                                                                    <th>Poli Ruangan</th>
                                                                    <th>Nama Pasien</th>
                                                                    <th>Jenis Kelamin</th>
                                                                    <th>Tanggal Lahir</th>
                                                                    <th>Usia</th>
                                                                    <th>Alamat</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <tr>   
                                                                    <td colspan="7">No Data to Display</td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="tabs-f">
                                                    <div class="row">
                                                        <div class="col-md-12">  
                                                            <button type="button" class="btn btn-success col-md-4" id="searchLaporanHasilVaksinasi" >Cari</button>        
                                                            <button type="button" class="btn btn-warning col-md-4" onclick="exportToExcelHasilvaksinasi(1)">Export Hasil Vaksinasi </button>
                                                            <button type="button" class="btn btn-info col-md-4" onclick="exportToExcelHasilvaksinasi(2)">Export Hasil Vaksinasi 2</button>
                                                        </div>
                                                    </div> <BR> 
                                                    <div class="table-responsive">
                                                    <table id="table_hasil_vaksinasi" class="table table-stripped table-hover" cellspacing="0"> 
                                                        <thead>
                                                                <tr>  
                                                                    <th>No</th>
                                                                    <th>Tanggal Pendaftaran</th>
                                                                    <th>Nama Pasien</th>
                                                                    <th>Tanggal Lahir</th>
                                                                    <th>Pasien Alamat</th>
                                                                    <th>Kelurahan</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <tr>   
                                                                    <td colspan="7">No Data to Display</td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="tabs-g">
                                                    <div class="row">
                                                        <div class="col-md-12">  
                                                            <button type="button" class="btn btn-success col-md-6" id="searchLaporanPendaftaranPoli" >Cari</button>        
                                                            <button type="button" class="btn btn-warning col-md-6" onclick="exportToExcelPendaftaran()">EXPORT TO EXCEL PENDAFTARAN</button>
                                                        </div>
                                                    </div> <BR> 
                                                    <div class="table-responsive">
                                                        <table id="table_pendaftaran_poli" class="table table-stripped table-hover" cellspacing="0"> 
                                                            <thead>
                                                                    <tr>  
                                                                        <th>No</th>
                                                                        <th>Tanggal Pendaftaran</th>
                                                                        <th>NO RM</th>
                                                                        <th>Nama Pasien</th>
                                                                        <th>Umur</th>
                                                                        <th>Jenis Kelamin</th>
                                                                        <th>kategori px</th>
                                                                        <th>Dokter PJ</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody> 
                                                                    <tr>   
                                                                        <td colspan="8">No Data to Display</td>
                                                                    </tr>
                                                                </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="tabs-h">
                                                    <div class="row">
                                                        <div class="col-md-12">  
                                                            <button type="button" class="btn btn-success col-md-6" id="searchLaporanDinkes" >Cari</button>        
                                                            <button type="button" class="btn btn-warning col-md-6" onclick="exportToExcelDinkes()">EXPORT TO EXCEL DINKES</button>
                                                        </div>
                                                    </div> <BR> 
                                                    <div class="table-responsive">
                                                         <table id="table_dinkes" class="table table-stripped table-hover" cellspacing="0"> 
                                                            <thead>
                                                                    <tr>  
                                                                        <th>No</th>
                                                                        <th>Tanggal Pendaftaran</th>
                                                                        <th>NO Pendaftaran</th>
                                                                        <th>Nama Pasien</th>
                                                                        <th>Umur</th>
                                                                        <th>aksep</th>
                                                                        <th>alamat</th>
                                                                        <th>alamat</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody> 
                                                                    <tr>   
                                                                        <td colspan="8">No Data to Display</td>
                                                                    </tr>
                                                                </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="profile1">
                                             <div class="row">
                                                <div class="form-group col-md-2">
                                                        <label for="inputName1" class="control-label"></label>
                                                        <!-- <input type="number" class="form-control" id="inputName1" placeholder="No.Rekam Medis" required> -->
                                                        
                                                           <dl class="text-right">  
                                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                                <dd>Ketik / pilih untuk mencari atau filter data </dd>  
                                                            </dl>
                                                        
                                                </div>
                                                <div class="col-md-10"> 
                                                    <div class="col-md-3">
                                                        <label>Tanggal Kunjungan,Dari</label>
                                                        <div class="input-group">   
                                                            <input onkeydown="return false" name="tgl_awal" id="tgl_awal_rd" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-3">   
                                                        <label>Sampai</label>    
                                                        <div class="input-group">   
                                                            <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir_rd" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">  
                                                        <label for="inputName1" class="control-label">Filter berdasarkan :</label>
                                                        <select name="status_kunjungan" class="form-control select" style="margin-bottom: 7px" id="status_kunjungan_rd" >          
                                                            <option value="" disabled selected>Pilih Berdasarkan</option>
                                                            <option value="PASIEN LAMA">Pasien Lama</option>   
                                                            <option value="PASIEN BARU">Pasien Baru</option>       
                                                        </select>
                                                    </div>   
                                                    <div class="col-md-3"> 
                                                        <label>&nbsp;</label><br>    
                                                        <button type="button" class="btn btn-success col-md-12" id="searchLaporanRd" >Cari</button>        
                                                    </div>
                                                        
                                                </div> 
                                            </div><hr style="margin-top: -27px">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-warning" onclick="printToExcelRd()">EXPORT TO EXCEL</button>
                                                </div>
                                            </div><br> 
 
                                            <table id="table_rawat_darurat" class="table table-stripped table-hover  table-responsive" cellspacing="0">
                                                <thead> 
                                                      <tr>
                                                            <th>Nomor (Bulan)</th>
                                                            <th>Nomor (Hari)</th>
                                                            <th>Tgl Masuk</th>
                                                            <th>No RM</th>
                                                            <th>Nama Pasien</th>
                                                            <th>Alamat</th>
                                                            <th>Gender</th>
                                                            <th>Umur</th>
                                                            <th>Dokter</th>
                                                            <th>Diagnosa</th>
                                                            <th>Kode ICD 10</th>
                                                            <th>Kunjungan</th>
                                                            <th>Keterangan</th>
                                                            <th>Status Pembayaran</th>
                                                            <th>Petugas</th>
                                                        </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="15">No Data to Display</td>
                                                    </tr>
                                                </tbody>
                                            </table>      


                                            <div class="clearfix"></div>
                                        </div>
                                        
                                        <div role="tabpanel" class="tab-pane fade" id="messages1">
                                             <div class="row">
                                                <div class="form-group col-md-2">
                                                        <label for="inputName1" class="control-label"></label>
                                                        <!-- <input type="number" class="form-control" id="inputName1" placeholder="No.Rekam Medis" required> -->
                                                        
                                                            <dl class="text-right">  
                                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                                <dd>Ketik / pilih untuk mencari atau filter data </dd>    
                                                            </dl>
                                                        
                                                </div>
                                                <div class="col-md-10"> 
                                                    <div class="col-md-3">
                                                        <label>Tanggal Kunjungan,Dari</label>
                                                        <div class="input-group">   
                                                            <input onkeydown="return false" name="tgl_awal" id="tgl_awal_ri" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-3">   
                                                        <label>Sampai</label>    
                                                        <div class="input-group">    
                                                            <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir_ri" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">  
                                                        <label for="inputName1" class="control-label">Filter berdasarkan :</label>
                                                        <select name="status_kunjungan" class="form-control select" style="margin-bottom: 7px" id="status_kunjungan_ri" >     
                                                            <option value="" disabled selected>Pilih Berdasarkan</option>
                                                            <option value="PASIEN LAMA">Pasien Lama</option>   
                                                            <option value="PASIEN BARU">Pasien Baru</option>       
                                                        </select>
                                                    </div>   
                                                    <div class="col-md-3"> 
                                                        <label>&nbsp;</label><br>
                                                        <button type="button" class="btn btn-success col-md-12" id="searchLaporanRi" >Cari</button>        
                                                    </div>
                                                        
                                                </div>
                                            </div><hr style="margin-top: -27px">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-warning" onclick="printToExcelRi()">EXPORT TO EXCEL</button>
                                                </div>
                                            </div><br> 
    
                                            <table id="table_rawat_inap" class="table table-stripped table-hover  table-responsive" cellspacing="0">
                                                    <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>No. RM</th>
                                                                <th>Nama Pasien</th>
                                                                <th>Jenis Kelamin</th>
                                                                <th>Tanggal Lahir</th>
                                                                <th>Usia</th>
                                                                <th>Alamat</th>
                                                                <th>Ruang</th>
                                                                <th>Kamar</th>
                                                                <th>Diagnosa Keluar</th>
                                                                <th>Kode Diagnosa</th>
                                                                <th>Dokter IGD</th>
                                                                <th>Dokter Penanggungjawab</th>
                                                                <th>Dokter Anastesi</th>
                                                                <th>Perawat / Bidan</th>
                                                                <th>Tanggal Masuk</th>
                                                                <th>Tanggal Keluar</th>
                                                                <th>Hari Perawatan</th>
                                                                <th>Cara Pulang</th>
                                                                <th>Pembiayaan Pasien</th>
                                                                <th>Keterangan</th>
                                                            </tr> 
                                                    </thead>
                                                    <tbody>
                                                            <tr>
                                                                <td colspan="21">No Data to Display</td>
                                                            </tr>
                                                    </tbody>
                                            </table>       
                                            <div class="clearfix"></div>
                                        </div>

                                        

                                        <div role="tabpanel" class="tab-pane fade" id="charts">
                                            <div class="row">
                                                <div class="col-md-12"> 
                                                    <div class="col-md-12">
                                                        <div class="col-md-12">
                                                                <label for="" style="font-size:8pt;color:red;">*Catatan : Grafik Data Pembayaran Pasien RJ, Sementara Hanya Bisa Per Tahun </label><BR>                                                                

                                                            <div class="form-group col-md-4">
                                                                <label for="">Pilih Per :</label>
                                                                
                                                                <select class="form-control" onchange="pilihBerdasarkanCharts()" name="charts_perbandingan1" id="charts_perbandingan1">
                                                                    <option value="" disabled selected>Pilih Berdasarkan</option>
                                                                    <option value="1">Tahun</option>
                                                                    <option value="2">Bulan</option>
                                                                    <option value="3">Hari</option>
                                                                </select>
                                                             </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                             <div id="berdasarkan_data" class="form-group col-md-4" style="display:none;">
                                                                <label for="">Data Charts :</label>
                                                                <select class="form-control" name="" id="pilih_data">
                                                                    <option value="" disabled selected>Pilih Data Charts</option>
                                                                    <option value="1">Data Penyakit Pasien RJ</option>
                                                                    <option value="2">Data Pekerjaan Pasien RJ</option>
                                                                    <option value="3">Data Pendidikan Pasien RJ</option>
                                                                    <option value="4">Data Pekerjaan PJ RJ</option>
                                                                    <option value="5" >Data Kunjungan Pasien RJ</option>
                                                                    <option value="6"  title="hanya bisa per tahun">Data Pembayaran Pasien RJ</option>
                                                                </select>
                                                            </div>
                                                            <div id="berdasarkan_tahun"  class="form-group col-md-4" style="display:none;">
                                                                <label for="">Pilih Tahun :</label>
                                                                <div class="input-group">   
                                                                    <input onkeydown="return false" name="tahun_charts" id="tahun_charts" type="text" class="form-control mydatepicker" value="<?php echo date('Y'); ?>" placeholder="yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                                </div>
                                                            </div> 
                                                            <div id="berdasarkan_bulan"  class="form-group col-md-4" style="display:none;">
                                                                <label for="">Pilih Bulan:</label>
                                                                <div class="input-group">   
                                                                    <input onkeydown="return false" name="bulan_charts" id="bulan_charts" type="text" class="form-control mydatepicker" value="<?php echo date('m'); ?>" placeholder="mm"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                                </div>
                                                            </div> 
                                                            <div id="berdasarkan_tgl_awal"  class="form-group col-md-4" style="display:none;">
                                                                <label for="">Dari :</label>
                                                                <div class="input-group">   
                                                                    <input onkeydown="return false" name="tgl_awal" id="tgl_awal_charts" type="text" class="form-control mydatepicker" value="<?php echo date('d M Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                                </div>
                                                            </div> 
                                                            <div id="berdasarkan_tgl_akhir" class="form-group col-md-4" style="display:none;">   
                                                                <label for="">Sampai :</label>
                                                                <div class="input-group">    
                                                                    <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir_charts" type="text" class="form-control mydatepicker" value="<?php echo date('d M Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                            <button class="btn btn-info" type="button" onclick="testing()"><i class="fa fa-eye"></i> LIHAT GRAFIK</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="white-box">
                                                                <h3 id ="judul" class="text-center"></h3>
                                                                <div id="morris-area-chart1" style="height: 370px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- perbandingan kedua -->
                                                    <!-- <div class="col-md-6">
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-4">
                                                                <select class="form-control" title="comming soon" onchange="pilihBerdasarkanCharts()" name="charts_perbandingan1" id="charts_perbandingan1">
                                                                    <option value="" disabled selected>Pilih Berdasarkan</option>
                                                                    <option value="1" disabled>Tahun</option>
                                                                    <option value="2" disabled>Bulan</option>
                                                                    <option value="3" disabled>Hari</option>
                                                                </select>
                                                             </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                             <div id="berdasarkan_data" class="form-group col-md-4" style="display:none;">
                                                                <select class="form-control" name="" id="pilih_data">
                                                                    <option value="" disabled selected>Pilih Data Charts</option>
                                                                    <option value="1" >Data Penyakit</option>
                                                                    <option value="2">Data Pekerjaan Pasien </option>
                                                                    <option value="3">Data Pendidikan Pasien</option>
                                                                    <option value="4">Data Pekerjaan PJ</option>
                                                                    <option value="5">Data Kunjungan Pasien</option>
                                                                    <option value="6">Data Pembayaran</option>
                                                                </select>
                                                            </div>
                                                            <div id="berdasarkan_tahun"  class="form-group col-md-4" style="display:none;">
                                                                <div class="input-group">   
                                                                    <input onkeydown="return false" name="tahun_charts" id="tahun_charts" type="text" class="form-control mydatepicker" value="<?php echo date('Y'); ?>" placeholder="yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                                </div>
                                                            </div> 
                                                            <div id="berdasarkan_bulan"  class="form-group col-md-4" style="display:none;">
                                                                <div class="input-group">   
                                                                    <input onkeydown="return false" name="bulan_charts" id="bulan_charts" type="text" class="form-control mydatepicker" value="<?php echo date('F'); ?>" placeholder="MM"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                                </div>
                                                            </div> 
                                                            <div id="berdasarkan_tgl_awal"  class="form-group col-md-4" style="display:none;">
                                                                <div class="input-group">   
                                                                    <input onkeydown="return false" name="tgl_awal" id="tgl_awal_charts" type="text" class="form-control mydatepicker" value="<?php echo date('d M Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                                </div>
                                                            </div> 
                                                            <div id="berdasarkan_tgl_akhir" class="form-group col-md-4" style="display:none;">   
                                                                <div class="input-group">    
                                                                    <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir_charts" type="text" class="form-control mydatepicker" value="<?php echo date('d M Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                            <button class="btn btn-info" title="comming soon" disabled type="button" onclick="testing()">CLICK BROHHH</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="white-box">
                                                                <div id="morris-area-chart2" style="height: 370px;"></div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>        
                                </div>
                            </div>  
                        </div>
                        
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->
 
<?php $this->load->view('footer');?>
