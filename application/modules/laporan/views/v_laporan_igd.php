<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Laporan IGD </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Laporan</a></li>
                            <li class="active">Laporan IGD</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Laporan IGD
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body"> 
                                     <div class="row">
                                        <div class="form-group col-md-2">
                                                <label for="inputName1" class="control-label"></label>
                                                <!-- <input type="number" class="form-control" id="inputName1" placeholder="No.Rekam Medis" required> -->
                                                    <dl class="text-right">  
                                                        <dt><h4 style="color: gray"><b>Periode</b></h4></dt>
                                                    </dl> 
                                        </div>
                                        <div class="col-md-10"> 
                                            <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                <label for="inputName1" class="control-label"><b>Tahun :</b></label>
                                                <b>
                                                <select name="tahun" id="tahun" class="form-control select" style="margin-bottom: 7px">
                                                    <option value="">Pilih Tahun</option>
                                                    <?php 
                                                        $thisYear = (int)date('Y');
                                                        $maxYear = $thisYear - 25;
                                                        for($i=$thisYear;$i>=$maxYear;$i--)
                                                        {
                                                            echo '<option value='.$i.'>'.$i.'</option>';
                                                        }
                                                    ?>
                                                    <!-- <option value="" disabled="" selected="">Tahun</option>
                                                    <option value="Janurari">Janurari</option>
                                                    <option value="Februari">Februari</option>
                                                    <option value="Maret">Maret</option>
                                                    <option value="April">April</option>
                                                    <option value="Mei">Mei</option>
                                                    <option value="Juni">Juni</option>
                                                    <option value="Juli">Juli</option>
                                                    <option value="Agustus">Agustus</option>
                                                    <option value="Septemebr">Septemebr</option>
                                                    <option value="Oktober">Oktober</option>
                                                    <option value="November">November</option>
                                                    <option value="Desember">Desember</option> -->
                                                </select></b>
                                                     
                                            </div>
                                            <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                <label for="inputName1" class="control-label"><b>Bulan : </b></label>
                                                <b>
                                                <select name="bulan" id="bulan" class="form-control select" style="margin-bottom: 7px">
                                                    <option value="">Pilih Bulan</option>
                                                    <option value="January">January</option>
                                                    <option value="February">February</option>
                                                    <option value="March">March</option>
                                                    <option value="April">April</option>
                                                    <option value="May">May</option>
                                                    <option value="June">June</option>
                                                    <option value="July">July</option>
                                                    <option value="August">August</option>
                                                    <option value="September">September</option>
                                                    <option value="October">October</option>
                                                    <option value="November">November</option>
                                                    <option value="December">December</option>
                                                </select></b>
                                                     
                                            </div>
                                            <div class="col-md-2"> 
                                                <label>&nbsp;</label><br>
                                                <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                            </div> 
                                        </div> 
                                    </div>

                                    <h3>Laporan IGD</h3>
                                    <button type="button" class="btn btn-success" style="margin-bottom: 15px;" onclick="exportToExcel()">Export Excel</button>
                                        <table id="table_laporan_igd" class="table table-striped dataTable" cellspacing="0" style="width: 100%;">  
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Periode Bulan</th>
                                                <th>Diterima</th>
                                                <th>Ditolak</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="4">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>  
                                    <br>
                                </div>
                            </div>  
                        </div>
                        
                    </div>
                    
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->  

                
<?php $this->load->view('footer');?>
      