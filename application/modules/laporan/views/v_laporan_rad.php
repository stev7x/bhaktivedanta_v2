<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Laporan Radiologi </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Laporan</a></li>
                            <li class="active">Laporan Radiologi</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
            </div>  
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Data Laporan Radiologi 
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                <div class="row">
                                <div class="col-md-2">
                                <label for="inputName1" class="control-label"></label>
                                    <dl class="text-right">  
                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                        <dd>Ketik / pilih untuk mencari atau filter data</dd>  
                                    </dl> 
                                </div>       
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label>Tanggal Kunjungan,Dari</label>
                                            <div class="input-group">   
                                                <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                             </div>
                                        </div>
                                        <div class="col-md-5">  
                                            <label>Sampai</label>    
                                            <div class="input-group">   
                                                <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                             </div>
                                        </div>
                                        <div class="col-md-2"> 
                                            <label>&nbsp;</label><br>
                                            <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                        </div> 
                                    <!-- ini export -->
                                    </div>
                                    
                                </div> 
                            </div> 
                            <br><hr style="margin-top: -27px">
                                    <div class="row">
                                        <div class="col-md-6">  
                                            <button type="button" class="btn btn-warning" onclick="printToExcelRadiologi()"> EXPORT TO EXCEL </button>  
                                        </div>
                                    </div><br>

                                
                                           <table id="table_laporan_radiologi" class="table table-striped dataTable table-responsive" cellspacing="0">
                                                    <thead>  
                                                        <tr>
                                                            <th rowspan="2">Tanggal</th>
                                                            <th rowspan="2">No</th>
                                                            <th rowspan="2">Nama Pasien</th>
                                                            <th rowspan="2">RM</th>
                                                            <th rowspan="2">Ruang</th>  
                                                            <th colspan="2" style="text-align: center;">Permintaan</th>
                                                            <th rowspan="2">Pengirim</th> 
                                                            <th colspan="5" style="text-align: center">Hasil</th>
                                                            <th rowspan="2">Ket.</th> 
                                                        </tr>
                                                        <tr>
                                                            <th>Tanggal</th>
                                                            <th>Tindakan</th>
                                                            <th>Cito/Biasa</th>  
                                                            <th>Petugas</th>
                                                            <th>Tanggal</th>
                                                            <th>Dokter Sp.RAD</th>
                                                            <th>Tarif</th>
                                                        </tr> 
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="12" style="text-align:right">Total Tarif:</th>
                                                            <th style="text-align: right;"></th>
                                                            <th></th> 
                                                        </tr>
                                                    </tfoot>
                                                    <tbody> 
                                                        <tr>
                                                            <td colspan="14">No Data to Display</td>
                                                        </tr>
                                                    </tbody>
                                                </table>  

                                </div>
                            </div>  
                        </div>
                        
                    </div>
                    
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->  

                    
           

<?php $this->load->view('footer');?>
      