<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
<style type="text/css">
    #table_laporan_keuangan_wrapper{
        overflow-x:auto;
    }
    table{
  table-layout: fixed; // ***********add this
  word-wrap:break-word; // ***********and this
}
</style>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Laporan Keuangan </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Laporan</a></li>
                            <li class="active">Laporan Laboratorium</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Data Laporan Keuangan 
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body"> 
                                     <div class="row">
                                        <div class="form-group col-md-2">
                                                <label for="inputName1" class="control-label"></label>
                                                <!-- <input type="number" class="form-control" id="inputName1" placeholder="No.Rekam Medis" required> -->
                                                
                                                    <dl class="text-right">  
                                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                        <dd>type/ select to find or filter data</dd>
                                                    </dl> 
                                                
                                        </div>
                                        <div class="col-md-10"> 
                                            <div class="col-md-5">
                                                <label>Tanggal Kunjungan,Dari</label>
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                </div>
                                            </div> 
                                            <div class="col-md-5">   
                                                <label>Sampai</label>    
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                </div>
                                            </div>  
                                            <div class="col-md-2"> 
                                                <label>&nbsp;</label><br>
                                                <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                            </div>
                                        </div>
                                    </div><hr style="margin-top: -27px">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-warning" onclick="exportToExcelLaporanKeuangan()">EXPORT TO EXCEL</button>
                                        </div>
                                    </div><br>

                                    <h3>Laporan Keuangan</h3>   
                                   <table id="table_laporan_keuangan" class="table table-striped dataTable" cellspacing="0" style="width: 100%; ">
                                        <thead>
                                            <tr>
                                                <th>Tgl</th>
                                                <th>Branch</th>
                                                <th>No Bill</th>
                                                <th>Nama Pasien</th>
                                                <th>Kode</th>
                                                <th>Doctor fee </th>
                                                <th>Medicine </th>
                                                <th>Other </th>
                                                <th>Escort </th>
                                                <th>Lab </th>
                                                <th>Diskon</th>
                                                <th>Total </th>
                                                <th>Jenis Pembayaran </th>
                                                <th>Nama Bank </th>
                                                <th>Batch Number </th>
                                                <th>No Rekening </th>
                                                <th>Nama Rekening </th>
                                                <th>Cara Pelunasan </th>
                                                <th>Nama Asuransi 1 </th>
                                                <th>No Asuransi 1 </th>
                                                <th>Nama Asuransi 2 </th>
                                                <th>No Asuransi 2 </th>
                                                <th>Surcharge </th>
                                                <th>DOD </th>
                                                <th>NOD 1 </th>
                                                <th>NOD 2 </th>
                                                <th>NOD 3 </th>
                                                <th>Transport </th>
                                                <th>hp. Obat </th>
                                                <th>Nama Hotel</th>
                                                <th>JP</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="3">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <th colspan="2" style="text-align:right">Total Tarif:</th>
                                                <th style="text-align:left"></th>
                                            </tr>
                                        </tfoot> -->
                                    </table>  
                                    <br>
 
                                    <h3>Laporan Pengeluaran</h3>  
                                   <table id="table_laporan_pengeluaran" class="table table-striped dataTable" cellspacing="0" style="width: 100%;">  
                                        <thead>
                                            <tr>
                                                <th>Kelompok / Kategori</th>
                                                <th>Poliklinik / Tindakan</th>
                                                <th>Total Biaya</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="3">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="2" style="text-align:right">Total Tarif:</th>
                                                <th style="text-align:left"></th>
                                            </tr>
                                        </tfoot>
                                    </table>      

                                </div>
                            </div>  
                        </div>
                        
                    </div>
                    
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->  

                
<?php $this->load->view('footer');?>
      