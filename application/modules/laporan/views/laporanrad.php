<?php $this->load->view('header');?>
	<title>SIMRS | Laporan Radiologi</title>
<?php $this->load->view('sidebar');?>
	<!-- START CONTENT -->
	<section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Laporan Radiologi</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Laporan</a></li>
                    <li class="active">Laporan Radiologi</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->

        <!-- Start Container -->
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col m12">
						<div class="card-panel">
							<h4 class="header">Laporan Radiologi</h4>
							<div class="row">
								<div class="col m12">
									<div id="table-datatables">
										<div class="row">
                                            <div class="col s12">
                                                &nbsp;
                                            </div>
                                            <div class="col s12">
                                                <div class="row">
                                                    <div class="input-field col s6">
                                                        <i class="mdi-action-event prefix"></i>
                                                        <input type="text" class="datepicker_awal" name="tgl_awal" id="tgl_awal" placeholder="Tanggal Awal" value="<?php echo date('d F Y'); ?>">
                                                        <label for="tgl_awal">Tanggal Tindakan, Dari</label>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <i class="mdi-action-event prefix"></i>
                                                        <input type="text" class="datepicker_akhir" name="tgl_akhir" id="tgl_akhir" placeholder="Tanggal Akhir" value="<?php echo date('d F Y'); ?>">
                                                        <label for="tgl_akhir">Sampai</label>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col s12">
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <button type="button" class="btn light-green waves-effect waves-light darken-4" title="Cari Pasien" id="searchLaporan">Search</button>
                                                        <button type="button" class="btn yellow waves-effect waves-light darken-4" title="Cari Pasien" onclick="printToExcelRadiologi()">Export To Excel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s12">
                                                &nbsp;
                                            </div>
                                            <div class="col s12 m4 l12">
                                                <div id="dvData">
                                                <table id="table_laporan_radiologi" class="responsive-table display" cellspacing="0">
                                                    <thead>
                                                    	<tr>
                                                            <th rowspan="2">Tanggal</th>
                                                            <th rowspan="2">No</th>
                                                            <th rowspan="2">Nama Pasien</th>
                                                            <th rowspan="2">RM</th>
                                                            <th rowspan="2">Ruang</th>
                                                            <th rowspan="2">P/L</th>
                                                    		<th colspan="2">Permintaan</th>
                                                            <th rowspan="2">Pengirim</th>
                                                    		<th colspan="3">Pengerjaan</th>
                                                    		<th colspan="3">Hasil</th>
                                                            <th rowspan="2">Ket.</th>
                                                    	</tr>
                                                        <tr>
                                                            <th>Jam</th>
                                                            <th>Tindakan</th>
                                                            <th>Jam</th>
                                                            <th>Cito/Biasa</th>
                                                            <th>Petugas</th>
                                                            <th>Tanggal</th>
                                                            <th>Dokter Sp.RAD</th>
                                                            <th>Tarif</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                    	<tr>
                                                    		<th colspan="14" style="text-align:right">Total Tarif:</th>
                                                    		<th style="text-align: right;"></th>
                                                    		<th></th>
                                                    	</tr>
                                                    </tfoot>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4">No Data to Display</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Container -->
	</section>
	<!-- END CONTENT -->
<?php $this->load->view('footer');?>