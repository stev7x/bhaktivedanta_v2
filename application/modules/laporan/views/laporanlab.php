<?php $this->load->view('header');?>
	<title>SIMRS | Laporan Laboratorium</title>
<?php $this->load->view('sidebar');?>
	<!-- START CONTENT -->
	<section id="content">
        
        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">Laporan Laboratorium</h5>
                <ol class="breadcrumbs">
                    <li><a href="#">Laporan</a></li>
                    <li class="active">Laporan Laboratorium</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->

        <!-- Start Container -->
		<div class="container">
			<div class="section">
				<div class="row">
					<div class="col m12">
						<div class="card-panel">
							<h4 class="header">Laporan Laboratorium</h4>
							<div class="row">
								<div class="col m12">
									<div id="table-datatables">
										<div class="row">
                                            <div class="col s12">
                                                &nbsp;
                                            </div>
                                            <div class="col s12">
                                                <div class="row">
                                                    <div class="input-field col s6">
                                                        <i class="mdi-action-event prefix"></i>
                                                        <input type="text" class="datepicker_awal" name="tgl_awal" id="tgl_awal" placeholder="Tanggal Awal" value="<?php echo date('d F Y'); ?>">
                                                        <label for="tgl_awal">Tanggal Tindakan, Dari</label>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <i class="mdi-action-event prefix"></i>
                                                        <input type="text" class="datepicker_akhir" name="tgl_akhir" id="tgl_akhir" placeholder="Tanggal Akhir" value="<?php echo date('d F Y'); ?>">
                                                        <label for="tgl_akhir">Sampai</label>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col s12">
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <button type="button" class="btn light-green waves-effect waves-light darken-4" title="Cari Pasien" id="searchLaporan">Search</button>
                                                        <button type="button" class="btn yellow waves-effect waves-light darken-4" title="Export Excel" onclick="printToExcelLab()">Export To Excel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s12">
                                                &nbsp;
                                            </div>
                                            <div class="col s12 m4 l12">
                                                <div id="dvData">
                                                <table id="table_laporan_laboratorium" class="responsive-table display" cellspacing="0">
                                                    <thead>
														<tr>
															<th rowspan="2">No</th>
															<th rowspan="2">Tgl Sample</th>
															<th rowspan="2">Nama</th>
															<th rowspan="2">RM</th>
															<th rowspan="2">Pengirim</th>
															<th colspan="2"><center>Pemeriksaan</center></th>
														</tr>
														<tr>
															<th>Tindakan</th>
															<th>Tarif</th>
														</tr>
													</thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4">No Data to Display</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Container -->
	</section>
	<!-- END CONTENT -->
<?php $this->load->view('footer');?>