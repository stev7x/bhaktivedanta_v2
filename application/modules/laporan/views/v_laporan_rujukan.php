<?php $this->load->view('header');?>
 
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Laporan Rujukan </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb"> 
                            <li><a href="index.html">Laporan</a></li>
                            <li class="active">Laporan Rujukan</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Laporan Rujukan
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body"> 
                                     <div class="row">
                                        <div class="form-group col-md-2">
                                                <label for="inputName1" class="control-label"></label>
                                                <!-- <input type="number" class="form-control" id="inputName1" placeholder="No.Rekam Medis" required> -->
                                                    <dl class="text-right">  
                                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                    </dl> 
                                        </div>
                                        <div class="col-md-10"> 
                                            <div class="col-md-5">
                                                <label>Penggunaan,Dari</label>
                                                <div class="input-group">   
                                                    <!-- <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php //echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> -->
                                                    <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F 2016'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                </div>
                                            </div> 
                                            <div class="col-md-5">   
                                                <label>Sampai</label>    
                                                <div class="input-group">   
                                                    <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                </div>
                                            </div>  
                                            <div class="col-md-2"> 
                                                <label>&nbsp;</label><br>
                                                <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>        
                                            </div> 
                                        </div> 
                                    </div>

                                    <h3>Laporan Rujukan</h3>
                                    <button type="button" class="btn btn-success" style="margin-bottom: 15px;" onclick="exportToExcel()">Export Excel</button>
                                        <table id="table_laporan_rujukan" class="table table-striped dataTable" cellspacing="0" style="width: 100%;">  
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal Pendaftaran</th>
                                                <th>Nama Pasien</th>
                                                <th>Nama Perujuk</th>
                                                <th>Alamat Perujuk</th>
                                                <th>Nomor Telpon</th>
                                                <th>Catatan Rujukan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="7">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>  
                                    <br>
                                </div>
                            </div>  
                        </div>
                        
                    </div>
                    
                </div>
                <!--/row -->
                
            </div>
            <!-- /.container-fluid -->  

                
<?php $this->load->view('footer');?>
      