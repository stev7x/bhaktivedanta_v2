<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function kasir() {
        $this->db->select('*');
        $this->db->from('v_belumbayar_pasien_rj');
        $this->db->or_where_not_in("status_periksa", array("SUDAH PULANG", "SELESAI"));
        $this->db->order_by('tgl_pendaftaran','DESC');
        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');

        $query = $this->db->get();

        return $query->result_array();
    }

    public function pembayaran() {
        $this->db->select('*');
        $this->db->from('t_pembayarankasir');

        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_tindakan_umum_sudahbayar($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('v_tindakanbelumbayar');
        $this->db->where('v_tindakanbelumbayar.pendaftaran_id',$pendaftaran_id);
        $this->db->where('v_tindakanbelumbayar.pembayarankasir_id !=', '');
        $length = $this->input->get('length');

        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    function get_ruangan_rj(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_JALAN));
        
        return $query->result();
    }
    
    function sum_jumlah_tindakan($tgl_awal, $tgl_akhir, $poliruangan_id){
        $this->db->select('SUM(total_harga) as jml');
    	$this->db->from('t_tindakanpasien');
        $this->db->where('ruangan_id',$poliruangan_id);
        $this->db->where('pembayarankasir_id IS NOT NULL');
    	$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');

    	$query = $this->db->get();
        $res = $query->row();
        return $res->jml;
    }
    
    function sum_jumlah_rad($tgl_awal, $tgl_akhir){
        $this->db->select('SUM(total_harga) as jml');
    	$this->db->from('t_tindakanradiologi');
        $this->db->where('pembayarankasir_id IS NOT NULL');
    	$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');

    	$query = $this->db->get();
        $res = $query->row();
        return $res->jml;
    }
    
    function sum_jumlah_lab($tgl_awal, $tgl_akhir){
        $this->db->select('SUM(total_harga) as jml');
    	$this->db->from('t_tindakanlab');
        $this->db->where('pembayarankasir_id IS NOT NULL');
    	$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');

    	$query = $this->db->get();
        $res = $query->row();
        return $res->jml;
    }
    
    function sum_jumlah_obat($tgl_awal, $tgl_akhir){
        $this->db->select('SUM(harga_jual*qty) as jml');
    	$this->db->from('t_resepturpasien');
        $this->db->where('pembayarankasir_id IS NOT NULL');
    	$this->db->where('tgl_reseptur BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');

    	$query = $this->db->get();
        $res = $query->row();
        return $res->jml;
    }

    public function get_pembayarankasir($pendaftaran_id, $pasien_id){
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        $this->db->where("pasien_id", $pasien_id);
        $query = $this->db->get('t_pembayarankasir');
        return $query->row();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        $query = $this->db->get_where('v_rm_pasien_rj', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();
    }   

    public function sum_harga_obat_sudah_bayar($pembayarankasir_id){   
        $this->db->select('SUM(qty*harga_jual) AS total_harga');
        $this->db->from('t_resepturpasien');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function sum_harga_lab_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanlab');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();     
    }

    public function sum_tindakan_sudah_bayar($pembayarankasir_id){
        $this->db->select_sum('total_harga');
        $this->db->from('t_tindakanpasien');
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_tindakan_pasien($pembayarankasir_id){           
        $this->db->select('t_tindakanpasien.jml_tindakan, t_tindakanpasien.total_harga,t_tindakanpasien.diskon,t_tindakanpasien.harga_tindakan as harga_baru,m_tindakan.*');
        $this->db->from('t_tindakanpasien');          
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_tindakanpasien.dokter_id','left'); 
        $this->db->where('t_tindakanpasien.pembayarankasir_id',$pembayarankasir_id);    
        $query = $this->db->get();


        return $query->result();
    }

    public function get_print_bayar($pembayarankasir_id){
        $this->db->select('*');
        $this->db->from('t_pembayarankasir');
        $this->db->join('aauth_users','aauth_users.id = t_pembayarankasir.create_by','left');      
        $this->db->where('pembayarankasir_id',$pembayarankasir_id);

        $query = $this->db->get();

        return $query->row(); 
    } 

    public function get_obat($pembayarankasir_id){
        $this->db->select('*');
        $this->db->from('t_resepturpasien');
        $this->db->where('t_resepturpasien.pembayarankasir_id', $pembayarankasir_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_fee_dokter($pasien_id, $dokter_id) {
        $this->db->select("SUM(harga) harga");
        $this->db->from("t_fee_dokter_perawat");
        $this->db->where("dokter_id", $dokter_id);
        $this->db->where("pasien_id", $pasien_id);
        $query = $this->db->get();

        return $query;
    }

    public function getFeeDokter($pasien_id) {
        $this->db->where("pasien_id", $pasien_id);
        return $this->db->get("t_fee_dokter_perawat")->result();
    }

    public function getPasien() {
        $this->db->where("status_periksa", "SUDAH BAYAR");
        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');
        $this->db->from("t_pendaftaran a");
        $this->db->join("m_pasien b", "b.pasien_id = a.pasien_id");

        return $this->db->get()->result();
    }

    public function getPasien_by_id($pasien_id) {
//        $this->db->where("status_periksa", "SUDAH BAYAR");
        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');
        $this->db->where('b.pasien_id', $pasien_id);
        $this->db->from("t_pendaftaran a");
        $this->db->join("m_pasien b", "b.pasien_id = a.pasien_id");

        return $this->db->get()->result();
    }

    public function getDataPerawat($dokter_id) {
        $this->db->where("id_M_DOKTER", $dokter_id);
        $this->db->where("kelompokdokter_id", "16");
        return $this->db->get("m_dokter")->row();
    }

    public function getHotel($hotel_id) {
        $this->db->where("id_hotel", $hotel_id);
        return $this->db->get("m_hotel")->row();
    }

    public function getReservasi($no_rekam_medis) {
        $this->db->where("no_rekam_medis", $no_rekam_medis);
        return $this->db->get("t_reservasi")->row();
    }

    // NEW FOR KLINIK
     function get_layanan($layanan_id){
         $this->db->where("id_layanan", $layanan_id);
         $this->db->select('*');
         $this->db->from('m_layanan');
         $query = $this->db->get();
         return $query->row();
     }

    // START : Model buat ambil harga dokter
    public function get_fee_dokter_by_hotel($id_hotel) {
        $this->db->select('*');
        $this->db->from('fee_dokter_by_hotel');
        $this->db->where('id_hotel', $id_hotel);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_fee_dokter_by_shift($shift) {
        $this->db->select('*');
        $this->db->from('fee_dokter_by_shift');
        $this->db->where('shift', $shift);
        $query = $this->db->get();

        return $query->result();
    }
    // END

    // START : Model buat ambil harga obat
    public function get_fee_obat_by_pasien($pasien_id) {
        $this->db->select('SUM(harga_jual) AS total');
        $this->db->from('t_resepturpasien');
        $this->db->where('pasien_id', $pasien_id);
        $query = $this->db->get();

        return $query->result();
    }
    // END

    // START : Model buat ambil harga tindakan (sementara)
    public function get_fee_tindakan_by_pasien($pasien_id) {
        $this->db->select('SUM(harga_tindakan) AS total');
        $this->db->from('t_tindakanpasien');
        $this->db->where('pasien_id', $pasien_id);
        $this->db->where("daftartindakan_id > 97");
        $query = $this->db->get();

        return $query->result();
    }
    // END

    // START : Model buat ambil harga escort (sementara)
    public function get_fee_escort_by_pasien($pasien_id) {
        $this->db->select('SUM(harga_tindakan) AS total');
        $this->db->from('t_tindakanpasien');
        $this->db->where('pasien_id', $pasien_id);
        $this->db->where("daftartindakan_id > 92 AND daftartindakan_id < 98");
        $query = $this->db->get();

        return $query->result();
    }
    // END

    // START : Model buat ambil harga lab (sementara)
    public function get_fee_lab_by_pasien($pasien_id) {
        $this->db->select('SUM(harga_tindakan) AS total');
        $this->db->from('t_tindakanpasien');
        $this->db->where('pasien_id', $pasien_id);
        $this->db->where("daftartindakan_id > 6 AND daftartindakan_id < 93");
        $query = $this->db->get();

        return $query->result();
    }
    // END

    // START : Model buat ambil harga lab (sementara)
    public function getTypePembayaran($pembayaran_id) {
        $this->db->select('type_pembayaran');
        $this->db->from('t_pembayaran');
        $this->db->where('pembayaran_id', $pembayaran_id);
        $query = $this->db->get();

        return $query->result();
    }
    // END

    // START : Model buat ambil harga lab (sementara)
    public function get_driver_name($driver_id) {
        $this->db->select('nama');
        $this->db->from('m_driver');
        $this->db->where('id', $driver_id);
        $query = $this->db->get();

        return $query->result();
    }
    // END

    // START : Model buat ambil harga pokok obat (sementara)
    public function get_hp($kode_barang) {
        $this->db->select('harga_pokok');
        $this->db->from('t_stok_farmasi');
        $this->db->where('kode_barang', $kode_barang);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_hp_obat($pasien_id) {
        $this->db->select('kode_barang');
        $this->db->from('t_resepturpasien');
        $this->db->where('pasien_id', $pasien_id);
        $data_kode = $this->db->get()->result();
        $total_hp = "";

        foreach ($data_kode as $data => $value) {
            $total_hp += $this->get_hp($value->kode_barang)[0]->harga_pokok;
        }

        return $total_hp;
    }
    // END
}