<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_igd_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_laporan($tahun, $bulan){
    // public function get_laporan(){
        $priode_user = $tahun . " " . $bulan;

        $this->db->select('priode_bulan');
        $this->db->distinct();
        $this->db->from('v_report_igd');  
        $this->db->where('priode_bulan != ""');
        $this->db->like('priode_bulan', $priode_user);

        // $this->db->where('priode_bulan BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get()->result();
        $dataIGD = array();
        foreach ($query as $value) {

            $this->db->select('total, status');
            $this->db->from('v_report_igd');
            $this->db->where('priode_bulan', $value->priode_bulan);
            $dataDi = $this->db->get()->result();

            $diterima = 0;
            $ditolak = 0;
            foreach ($dataDi as $value1) {
                if ($value1->status == 'HADIR') {
                    $diterima = $value1->total;
                }
                if ($value1->status == 'TOLAK') {
                    $ditolak = $value1->total;
                }
            }

            $dataIGD[] = (object) array(
                "priode_bulan"  => $value->priode_bulan,
                'diterima'      => $diterima,
                'ditolak'       => $ditolak
            );
        }

        return $dataIGD;
    }

    public function count_all_list($tahun, $bulan){
        $priode_user = $tahun . " " . $bulan;

        $this->db->select('priode_bulan');
        $this->db->distinct();
        $this->db->from('v_report_igd');  
        $this->db->where('priode_bulan != ""');
        $this->db->like('priode_bulan', $priode_user);

        $query = $this->db->get()->result();
        $dataIGD = array();
        foreach ($query as $value) {

            $this->db->select('total, status');
            $this->db->from('v_report_igd');
            $this->db->where('priode_bulan', $value->priode_bulan);
            $dataDi = $this->db->get()->result();

            $diterima = 0;
            $ditolak = 0;
            foreach ($dataDi as $value1) {
                if ($value1->status == 'HADIR') {
                    $diterima = $value1->total;
                }
                if ($value1->status == 'TOLAK') {
                    $ditolak = $value1->total;
                }
            }

            $dataIGD[] = (object) array(
                "priode_bulan"  => $value->priode_bulan,
                'diterima'      => $diterima,
                'ditolak'       => $ditolak
            );
        }

        return count($dataIGD);
    }

    public function count_all_list_filtered($tahun, $bulan){
        $priode_user = $tahun . " " . $bulan;

        $this->db->select('priode_bulan');
        $this->db->distinct();
        $this->db->from('v_report_igd');  
        $this->db->where('priode_bulan != ""');
        $this->db->like('priode_bulan', $priode_user);

        $query = $this->db->get()->result();
        $dataIGD = array();
        foreach ($query as $value) {

            $this->db->select('total, status');
            $this->db->from('v_report_igd');
            $this->db->where('priode_bulan', $value->priode_bulan);
            $dataDi = $this->db->get()->result();

            $diterima = 0;
            $ditolak = 0;
            foreach ($dataDi as $value1) {
                if ($value1->status == 'HADIR') {
                    $diterima = $value1->total;
                }
                if ($value1->status == 'TOLAK') {
                    $ditolak = $value1->total;
                }
            }

            $dataIGD[] = (object) array(
                "priode_bulan"  => $value->priode_bulan,
                'diterima'      => $diterima,
                'ditolak'       => $ditolak
            );
        }

        return count($dataIGD);
    }
    
    public function get_data($tahun, $bulan){
        $priode_user = $tahun . " " . $bulan;

        $this->db->select('priode_bulan');
        $this->db->distinct();
        $this->db->from('v_report_igd');  
        $this->db->where('priode_bulan != ""');
        $this->db->like('priode_bulan', $priode_user);

        $query = $this->db->get()->result();
        $dataIGD = array();
        foreach ($query as $value) {

            $this->db->select('total, status');
            $this->db->from('v_report_igd');
            $this->db->where('priode_bulan', $value->priode_bulan);
            $dataDi = $this->db->get()->result();

            $diterima = 0;
            $ditolak = 0;
            foreach ($dataDi as $value1) {
                if ($value1->status == 'HADIR') {
                    $diterima = $value1->total;
                }
                if ($value1->status == 'TOLAK') {
                    $ditolak = $value1->total;
                }
            }

            $dataIGD[] = (object) array(
                "priode_bulan"  => $value->priode_bulan,
                'diterima'      => $diterima,
                'ditolak'       => $ditolak
            );
        }

        return $dataIGD;
    }
}
?>