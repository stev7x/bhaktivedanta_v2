<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_rm_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_rawat_jalan_list($tgl_awal, $tgl_akhir, $status_kunjungan, $rm_awal, $rm_akhir){
        if($rm_awal == "" && $rm_akhir == ""){
            $this->db->from('v_report_rm_pasien_rj');  
            $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        }else{
            $this->db->from('v_report_rm_pasien_rj');  
            // $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
            $this->db->where('no_rekam_medis BETWEEN "'. $rm_awal.'" and "'.$rm_akhir.'"');
            $this->db->order_by('no_rekam_medis','ASC');
        }
        
        
        if(!empty($status_kunjungan)){ 
            $this->db->where('status_kunjungan', $status_kunjungan);
        }    
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result(); 
    }

    
    public function get_poli_keseluruhan_list($tgl_awal, $tgl_akhir, $filterBy){
         if($filterBy == 1 ){
            $filterBy = 'DAY';
        }else if($filterBy == 2){
            $filterBy = "MONTH";
        }else if($filterBy == 3){
            $filterBy = "YEAR";
        }else{

        }
        // $sql = 'select * from v_report_keseluruhan_rj where tanggal between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"';
               $this->db->from('v_report_keseluruhan_rj')
                 ->where('TANGGAL BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"') 
                //  ->group_by('DOKTER_ID , '.$filterBy.'(TANGGAL)')
                 ->order_by('TANGGAL','ASC');

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        return $query->result(); 
    }

    public function count_poli_keseluruhan_list($tgl_awal, $tgl_akhir, $filterBy){
        if($filterBy == 1 ){
            $filterBy = 'DAY';
        }else if($filterBy == 2){
            $filterBy = "MONTH";
        }else if($filterBy == 3){
            $filterBy = "YEAR";
        }else{

        }
        // $sql = 'select * from v_report_keseluruhan_rj where tanggal between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" group by DOKTER_ID , '.$filterBy.'(TANGGAL)';
         $this->db->from('v_report_keseluruhan_rj')
                 ->where('TANGGAL BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"') 
                //  ->group_by('DOKTER_ID , '.$filterBy.'(TANGGAL)')
                 ->order_by('TANGGAL','ASC');
                 
        
        return $this->db->count_all_results();
    }

    public function get_rm_rj_list($tgl_awal, $tgl_akhir){

        // $sql = 'select * from v_report_rm_pasien_rj where tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"';
        $this->db->from('v_report_rm_pasien_rj')
                 ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"')
                 ->order_by('tgl_pendaftaran','ASC');

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
        
        return $query->result(); 
    }

     public function count_rm_rj_list($tgl_awal, $tgl_akhir){
        // $sql = 'select * from v_report_rm_pasien_rj where tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"';
        $this->db->from('v_report_rm_pasien_rj')
                 ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"')
                 ->order_by('tgl_pendaftaran','ASC');

        // $query = $this->db->get();
        
        return $this->db->count_all_results();
    }


    public function get_rawat_inap_list($tgl_awal, $tgl_akhir, $dokter_pjri,$pasien){
        $this->db->from('v_report_rm_pasien_ri'); 
        $this->db->where('tgl_masukadmisi BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" AND "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($pasien)){   
            $this->db->like('pasien_nama', $pasien); 
        } 
        if(!empty($dokter_pjri)){
            $this->db->where('dokter_pj', $dokter_pjri);
        }
         
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
      
    function get_export_ri($tgl_awal, $tgl_akhir, $dokter_pjri,$pasien){
        $this->db->from('v_report_rm_pasien_ri');
        $this->db->where('tgl_masukadmisi >="'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and tgl_pulang <="'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($pasien)){
            $this->db->like('pasien_nama', $pasien);
        }
        if(!empty($dokter_pjri)){
            $this->db->where('dokter_pj', $dokter_pjri);
        }
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_rawat_inap_all($tgl_awal, $tgl_akhir, $dokter_pjri,$pasien){
        $this->db->from('v_report_rm_pasien_ri');
        $this->db->where('tgl_masukadmisi >="'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and tgl_pulang <="'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($pasien)){
            $this->db->like('pasien_nama', $pasien);
        }
        if(!empty($dokter_pjri)){
            $this->db->where('dokter_pj', $dokter_pjri);
        }

        return $this->db->count_all_results();
    }
    
    public function get_export_rj($tgl_awal, $tgl_akhir, $poliklinik, $rm_awal, $rm_akhir){
        if($rm_awal == "" && $rm_akhir == ""){
            $this->db->from('v_report_rm_pasien_rj');
            $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        }else{
            $this->db->from('v_report_rm_pasien_rj');
            $this->db->where('no_rekam_medis BETWEEN "'. $rm_awal.'" and "'.$rm_akhir.'"');
        }
        
        
        if(!empty($poliklinik)){
            $this->db->where('poliruangan_id', $poliklinik);
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function get_export_rd($tgl_awal, $tgl_akhir){
        $this->db->from('v_report_rm_pasien_rd');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_rawat_jalan_all($tgl_awal, $tgl_akhir, $status_kunjungan){
        $this->db->from('v_report_rm_pasien_rj');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"'); 
        if(!empty($status_kunjungan)){   
            $this->db->where('status_kunjungan', $status_kunjungan);
        }
        return $this->db->count_all_results();
    }
    
    public function get_rawat_darurat_list($tgl_awal, $tgl_akhir, $status_kunjungan){
        $this->db->from('v_report_rm_pasien_rd');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" AND "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($status_kunjungan)){         
            $this->db->where('status_kunjungan', $status_kunjungan);
        }
          
        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();

        
        return $query->result();
    }
     
    public function count_rawat_darurat_all($tgl_awal, $tgl_akhir,$status_kunjungan){
        $this->db->from('v_report_rm_pasien_rd');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"'); 
        if(!empty($status_kunjungan)){   
            $this->db->where('status_kunjungan', $status_kunjungan);
        }

        return $this->db->count_all_results();
    }

    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_JALAN));
        
        return $query->result();
    }   
    
    public function get_dokter_list(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('instalasi_id', 3); //rawat inap
        $this->db->group_by('id_M_DOKTER');
        $query = $this->db->get();

        return $query->result();
    }   
    
    public function getDataBidan($tgl_awal, $tgl_akhir){
        $this->db->from('v_pasien_kebidanan')
                 ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
      
        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        
        return $query->result(); 
    
    }

    public function getDataPoliUmumKeseluruhan($tgl_awal, $tgl_akhir, $filterBy){
        if($filterBy == 1 ){
            $filterBy = 'DAY';
        }else if($filterBy == 2){
            $filterBy = "MONTH";
        }else if($filterBy == 3){
            $filterBy = "YEAR";
        }else{

        }
        // $sql = 'select * from v_report_keseluruhan_rj where tanggal between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"';
        $this->db
                ->from('v_report_keseluruhan_rj')
                ->where('tanggal BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"'); 
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        
        return $query->result(); 
    
    }

    public function getDataPoliUmumRinci($tgl_awal, $tgl_akhir, $filterBy){
        if($filterBy == 1 ){
            $filterBy = 'DAY';
        }else if($filterBy == 2){
            $filterBy = "MONTH";
        }else if($filterBy == 3){
            $filterBy = "YEAR";
        }else{

        }
        $this->db
                ->from('v_report_rinci_rj') 
                ->where('TANGGAL BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"') 
                ->order_by('TANGGAL','ASC');

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        return $query->result();  
    
    }

    public function count_poli_rinci_list($tgl_awal, $tgl_akhir, $filterBy){
        $this->db->from('v_report_rinci_rj')
                 ->where('TANGGAL BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"') 
                 ->order_by('TANGGAL','ASC');
                 
        
        return $this->db->count_all_results();
    }



    public function getDataRmRj($tgl_awal, $tgl_akhir){
        $sql = 'select * from v_report_rm_pasien_rj where tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" ';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDemografiPekerjaanPasien($tgl_awal, $tgl_akhir){
        $sql = '
        SELECT nama_pekerjaan, COUNT(nama_pekerjaan) AS jum_pekerjaan FROM v_demografi_pekerjaan_pasien 
WHERE tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" GROUP BY nama_pekerjaan order by jum_pekerjaan desc
        ';
        $query = $this->db->query($sql);
        
        return $query->result(); 
    
    }

    public function getDemografiPendidikanPasien($tgl_awal, $tgl_akhir){
        $sql = '
     SELECT pendidikan_nama, COUNT(pendidikan_nama) AS jum_pendidikan FROM v_demografi_pendidikan_pasien
WHERE tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" GROUP BY pendidikan_nama order by jum_pendidikan desc
        ';
        $query = $this->db->query($sql);
        
        return $query->result(); 
    
    }

    public function getDemografiPekrjaanPJ($tgl_awal, $tgl_akhir){
        $sql = '
SELECT nama_pekerjaan, COUNT(nama_pekerjaan) AS jum_pekerjaan_PJ FROM v_demografi_pekerjaan_PJ	
WHERE tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" GROUP BY nama_pekerjaan order by jum_pekerjaan_PJ desc
        ';
        $query = $this->db->query($sql);
        
        return $query->result(); 
    
    }

    public function getKunjunganData($tgl_awal, $tgl_akhir){
    //    $this->db->from('v_report_kunjungan_jk');
//         $sql = '
//                 SELECT 
// (SELECT COUNT(v_report_jk_1.`pasien_lama_laki`) FROM v_report_jk_1 WHERE (v_report_jk_1.tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'")) AS PASIEN_LAMA_L_RJ,
// (SELECT COUNT(v_report_jk_2.`pasien_baru_laki`) FROM v_report_jk_2 WHERE (v_report_jk_2.tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'")) AS PASIEN_BARU_L_RJ,
// (SELECT COUNT(v_report_jk_3.`pasien_lama_perem`) FROM v_report_jk_3 WHERE (v_report_jk_3.tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'")) AS PASIEN_LAMA_P_RJ,
// (SELECT COUNT(v_report_jk_4.`pasien_baru_perem`) FROM v_report_jk_4 WHERE (v_report_jk_4.tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'")) AS PASIEN_BARU_P_RJ

//                 ';
                // $this->db->select('CONCAT(v_report_jk_1.`status_kunjungan`, " Laki") AS `status`, COUNT(v_report_jk_1.`pasien_lama_laki`) AS jum FROM v_report_jk_1 WHERE v_report_jk_1.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"  
                // UNION SELECT CONCAT(v_report_jk_2.`status_kunjungan`, " Laki") AS `status`, COUNT(v_report_jk_2.`pasien_baru_laki`) AS jum FROM v_report_jk_2 WHERE v_report_jk_2.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" 
                // UNION SELECT CONCAT(v_report_jk_3.`status_kunjungan`, " Perempuan") AS `status`, COUNT(v_report_jk_3.`pasien_lama_perem`) AS jum FROM v_report_jk_3 WHERE v_report_jk_3.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" 
                // UNION SELECT CONCAT(v_report_jk_4.`status_kunjungan`, " Perempuan")AS `status`, COUNT(v_report_jk_4.`pasien_baru_perem`) AS jum FROM v_report_jk_4 WHERE v_report_jk_4.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $this->db->select('
        CONCAT("Laki") AS jk,
(SELECT COUNT(v_report_jk_2.`pasien_baru_laki`) FROM v_report_jk_2 WHERE v_report_jk_2.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'") AS baru,
(SELECT COUNT(v_report_jk_1.`pasien_lama_laki`) FROM v_report_jk_1 WHERE v_report_jk_1.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'") AS lama
UNION
SELECT CONCAT("perempuan") AS jk,
(SELECT COUNT(v_report_jk_4.`pasien_baru_perem`) FROM v_report_jk_4 WHERE v_report_jk_4.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" ) AS baru,
(SELECT COUNT(v_report_jk_3.`pasien_lama_perem`) FROM v_report_jk_3 WHERE v_report_jk_3.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'") AS lama
        
        ');

        $query = $this->db->get();

        // $query = $this->db->get();
        
        return $query->result(); 
    
    }

   
    public function getDataPenyakit($tgl_awal, $tgl_akhir){
       $this->db
                ->select('t_diagnosapasien.`nama_icd10`,  t_diagnosapasien.`kode_icd10`, COUNT(nama_icd10) AS jum')
                ->from('t_diagnosapasien')
                ->join('t_pendaftaran','t_pendaftaran.pendaftaran_id = t_diagnosapasien.pendaftaran_id','left')
                ->where('t_pendaftaran.status_pasien',0)
                ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"')
                ->group_by('nama_icd10')
                ->order_by('jum','DESC')
                ->limit(10);
    //    $this->db->from('v_report_kunjungan_jk');
        $query = $this->db->get();
        
        return $query->result(); 
    
    }

    public function getDataPenyakitRAW($pilih_data, $charts_perbandingan1, $tahun_charts, $bulan_charts, $tgl_awal_charts, $tgl_akhir_charts){
        $tgl_diagnosa = 'tgl_diagnosa BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'"';
        $tgl_pendaftaran = 'tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'"';
        if($pilih_data == 1){ //data_penyakit
            if($charts_perbandingan1 == 1){ 
                $this->db
                        ->select('nama_icd10,  COUNT(nama_icd10) AS jum')
                        ->from('v_data_penyakit')
                        ->where('(YEAR(tgl_diagnosa) = "'.$tahun_charts.'")')
                        ->group_by('nama_icd10 , (YEAR(tgl_diagnosa) = "'.$tahun_charts.'")');
                        
                    }else if($charts_perbandingan1 == 2){
                $this->db
                        ->select('nama_icd10,  COUNT(nama_icd10) AS jum')
                        ->from('v_data_penyakit')
                        ->where('(YEAR(tgl_diagnosa) = "'.$tahun_charts.'") and  (MONTH(tgl_diagnosa) = "'.$bulan_charts.'") ')
                        ->group_by('v_data_penyakit.nama_icd10 , (YEAR(tgl_diagnosa) = "'.$tahun_charts.'") and  (MONTH(tgl_diagnosa) = "'.$bulan_charts.'")');
                        
                    }else{
                $this->db
                        ->select('nama_icd10,  COUNT(nama_icd10) AS jum')
                        ->from('v_data_penyakit')
                        ->where('tgl_diagnosa BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'"')
                        ->group_by("'.$tgl_diagnosa.'")
                        ->group_by('nama_icd10 ') ;
                    }
            $this->db
                    ->order_by('jum','DESC')
                    ->limit(10);

        }else if($pilih_data == 2){ //data pekerjaan pasien
            if($charts_perbandingan1 == 1){
                $this->db
                        ->select('nama_pekerjaan,  COUNT(nama_pekerjaan) AS jum')
                        ->from('v_demografi_pekerjaan_pasien')
                        ->where('(YEAR(tgl_pendaftaran) = "'.$tahun_charts.'")')
                        ->group_by('nama_pekerjaan , (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'")');
            }else if($charts_perbandingan1 == 2){
                $this->db
                        ->select('nama_pekerjaan,  COUNT(nama_pekerjaan) AS jum')
                        ->from('v_demografi_pekerjaan_pasien')
                        ->where('(YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'") ')
                        ->group_by('v_demografi_pekerjaan_pasien.nama_pekerjaan , (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'")');
            }else{
                $this->db
                        ->select('nama_pekerjaan,  COUNT(nama_pekerjaan) AS jum')
                        ->from('v_demografi_pekerjaan_pasien')
                        ->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'"')
                        ->group_by("'.$tgl_pendaftaran.'")
                        ->group_by('nama_pekerjaan ') ;
            }
             $this->db
                    ->order_by('jum','DESC');

        }else if($pilih_data == 3){ //data pendidikan pasien
            if($charts_perbandingan1 == 1){
                 $this->db
                        ->select('pendidikan_nama,  COUNT(pendidikan_nama) AS jum')
                        ->from('v_demografi_pendidikan_pasien')
                        ->where('(YEAR(tgl_pendaftaran) = "'.$tahun_charts.'")')
                        ->group_by('pendidikan_nama , (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'")');

            }else if($charts_perbandingan1 == 2){
                 $this->db
                        ->select('pendidikan_nama,  COUNT(pendidikan_nama) AS jum')
                        ->from('v_demografi_pendidikan_pasien')
                        ->where('(YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'") ')
                        ->group_by('v_demografi_pekerjaan_pasien.pendidikan_nama , (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'")');

            }else{
                 $this->db
                        ->select('pendidikan_nama,  COUNT(pendidikan_nama) AS jum')
                        ->from('v_demografi_pendidikan_pasien')
                        ->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'"')
                        ->group_by("'.$tgl_pendaftaran.'")
                        ->group_by('pendidikan_nama ') ;

            }
             $this->db
                    ->order_by('jum','DESC');
        }else if($pilih_data == 4){ //data pekerjaan PJ
            if($charts_perbandingan1 == 1){
                     $this->db
                        ->select('nama_pekerjaan,  COUNT(nama_pekerjaan) AS jum')
                        ->from('v_demografi_pekerjaan_pj')
                        ->where('(YEAR(tgl_pendaftaran) = "'.$tahun_charts.'")')
                        ->group_by('nama_pekerjaan , (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'")');
            }else if($charts_perbandingan1 == 2){
                     $this->db
                        ->select('nama_pekerjaan,  COUNT(nama_pekerjaan) AS jum')
                        ->from('v_demografi_pekerjaan_pj')
                        ->where('(YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'") ')
                        ->group_by('v_demografi_pekerjaan_pasien.nama_pekerjaan , (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'")');

            }else{
                 $this->db
                        ->select('nama_pekerjaan,  COUNT(nama_pekerjaan) AS jum')
                        ->from('v_demografi_pekerjaan_pj')
                        ->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'"')
                        ->group_by("'.$tgl_pendaftaran.'")
                        ->group_by('nama_pekerjaan ') ;
            }
            $this->db
                    ->order_by('jum','DESC');
        }else if($pilih_data == 5){ //data kunujungan
            if($charts_perbandingan1 == 1){
                 $this->db->select('CONCAT(v_report_jk_1.`status_kunjungan`, " Laki") AS `status`, COUNT(v_report_jk_1.`pasien_lama_laki`) AS jum FROM v_report_jk_1 WHERE (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") 
                UNION SELECT CONCAT(v_report_jk_2.`status_kunjungan`, " Laki") AS `status`, COUNT(v_report_jk_2.`pasien_baru_laki`) AS jum FROM v_report_jk_2 WHERE (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'")  
                UNION SELECT CONCAT(v_report_jk_3.`status_kunjungan`, " Perempuan") AS `status`, COUNT(v_report_jk_3.`pasien_lama_perem`) AS jum FROM v_report_jk_3 WHERE (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") 
                UNION SELECT CONCAT(v_report_jk_4.`status_kunjungan`, " Perempuan")AS `status`, COUNT(v_report_jk_4.`pasien_baru_perem`) AS jum FROM v_report_jk_4 WHERE (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") ');
            }else if($charts_perbandingan1 == 2){
                                $this->db->select('CONCAT(v_report_jk_1.`status_kunjungan`, " Laki") AS `status`, COUNT(v_report_jk_1.`pasien_lama_laki`) AS jum FROM v_report_jk_1 WHERE (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'") 
                UNION SELECT CONCAT(v_report_jk_2.`status_kunjungan`, " Laki") AS `status`, COUNT(v_report_jk_2.`pasien_baru_laki`) AS jum FROM v_report_jk_2 WHERE (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'") 
                UNION SELECT CONCAT(v_report_jk_3.`status_kunjungan`, " Perempuan") AS `status`, COUNT(v_report_jk_3.`pasien_lama_perem`) AS jum FROM v_report_jk_3 WHERE (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'") 
                UNION SELECT CONCAT(v_report_jk_4.`status_kunjungan`, " Perempuan")AS `status`, COUNT(v_report_jk_4.`pasien_baru_perem`) AS jum FROM v_report_jk_4 WHERE (YEAR(tgl_pendaftaran) = "'.$tahun_charts.'") and  (MONTH(tgl_pendaftaran) = "'.$bulan_charts.'")');
            }else{
                $this->db->select('CONCAT(v_report_jk_1.`status_kunjungan`, " Laki") AS `status`, COUNT(v_report_jk_1.`pasien_lama_laki`) AS jum FROM v_report_jk_1 WHERE v_report_jk_1.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'"  
                UNION SELECT CONCAT(v_report_jk_2.`status_kunjungan`, " Laki") AS `status`, COUNT(v_report_jk_2.`pasien_baru_laki`) AS jum FROM v_report_jk_2 WHERE v_report_jk_2.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'" 
                UNION SELECT CONCAT(v_report_jk_3.`status_kunjungan`, " Perempuan") AS `status`, COUNT(v_report_jk_3.`pasien_lama_perem`) AS jum FROM v_report_jk_3 WHERE v_report_jk_3.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'" 
                UNION SELECT CONCAT(v_report_jk_4.`status_kunjungan`, " Perempuan")AS `status`, COUNT(v_report_jk_4.`pasien_baru_perem`) AS jum FROM v_report_jk_4 WHERE v_report_jk_4.tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal_charts." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir_charts." 23:59:59")).'"');
            }
        }else if($pilih_data == 6){ //data pembayaran 
            if($charts_perbandingan1 == 0){

            // }else if($charts_perbandingan1 == 2){

            }else{
                  $this->db->select('
CONCAT("Januari") AS bulan, (v_data_pembayaran_rj_pri.`Januari_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Januari_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Januari_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Februari") AS bulan, (v_data_pembayaran_rj_pri.`Februari_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Februari_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Februari_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Maret") AS bulan, (v_data_pembayaran_rj_pri.`Maret_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Maret_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Maret_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("April") AS bulan, (v_data_pembayaran_rj_pri.`April_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`April_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`April_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Mei") AS bulan, (v_data_pembayaran_rj_pri.`Mei_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Mei_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Mei_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Juni") AS bulan, (v_data_pembayaran_rj_pri.`Juni_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Juni_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Juni_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Juli") AS bulan, (v_data_pembayaran_rj_pri.`Juli_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Juli_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Juli_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Agustus") AS bulan, (v_data_pembayaran_rj_pri.`Agustus_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Agustus_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Agustus_bpjs`) AS bpjs  FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("September") AS bulan, (v_data_pembayaran_rj_pri.`September_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`September_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`September_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Oktober") AS bulan, (v_data_pembayaran_rj_pri.`Oktober_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Oktober_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Oktober_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("November") AS bulan, (v_data_pembayaran_rj_pri.`November_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`November_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`November_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Desember") AS bulan, (v_data_pembayaran_rj_pri.`Desember_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Desember_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Desember_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
        ');
            }
        }
        $query = $this->db->get();
        // var_dump($query);die();
        
        return $query->result(); 
    
    }

    public function getDataPembayaran($tgl_awal, $tgl_akhir){
        $this->db->select('
CONCAT("Januari") AS bulan, (v_data_pembayaran_rj_pri.`Januari_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Januari_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Januari_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Februari") AS bulan, (v_data_pembayaran_rj_pri.`Februari_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Februari_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Februari_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Maret") AS bulan, (v_data_pembayaran_rj_pri.`Maret_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Maret_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Maret_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("April") AS bulan, (v_data_pembayaran_rj_pri.`April_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`April_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`April_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Mei") AS bulan, (v_data_pembayaran_rj_pri.`Mei_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Mei_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Mei_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Juni") AS bulan, (v_data_pembayaran_rj_pri.`Juni_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Juni_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Juni_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Juli") AS bulan, (v_data_pembayaran_rj_pri.`Juli_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Juli_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Juli_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Agustus") AS bulan, (v_data_pembayaran_rj_pri.`Agustus_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Agustus_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Agustus_bpjs`) AS bpjs  FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("September") AS bulan, (v_data_pembayaran_rj_pri.`September_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`September_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`September_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Oktober") AS bulan, (v_data_pembayaran_rj_pri.`Oktober_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Oktober_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Oktober_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("November") AS bulan, (v_data_pembayaran_rj_pri.`November_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`November_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`November_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
UNION
SELECT CONCAT("Desember") AS bulan, (v_data_pembayaran_rj_pri.`Desember_pri`) AS pribadi, (v_data_pembayaran_rj_asu.`Desember_asu`) AS asuransi, (v_data_pembayaran_rj_bpjs.`Desember_bpjs`) AS bpjs FROM v_data_pembayaran_rj_pri, v_data_pembayaran_rj_asu, v_data_pembayaran_rj_bpjs
        ');
        $query = $this->db->get();
        return $query->result();
    }

    public function getHasilVaksinasi($tgl_awal, $tgl_akhir, $tipe){
        if($tipe == 1){
            $table = 'v_pasienimun_bidan2'; //VAKSIN 1
        }else if($tipe == 2){
            $table = 'v_pasienimun_bidan'; // VAKSIN 2
        }
        $this->db->from($table)
                ->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" ');

        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
        return $query->result(); 
    
    }

    function countHasilVaksinasi($tgl_awal, $tgl_akhir, $tipe){    
        if($tipe == 1){
            $table = 'v_pasienimun_bidan2'; //VAKSIN 1
        }else if($tipe == 2){
            $table = 'v_pasienimun_bidan'; // VAKSIN 2
        }
        $this->db->from($table)
                ->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'" ');    
        return $this->db->count_all_results();
    }
    
    public function countDataBidanAll($tgl_awal, $tgl_akhir){
        $this->db->from('v_pasien_kebidanan')
                ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        return $this->db->count_all_results();
    }

    public function getNoRM(){
        $this->db->from('v_report_rm_pasien_rj');
        $this->db->order_by('no_rekam_medis','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataPendaftaranPoli($tgl_awal, $tgl_akhir){
        $this->db
                ->from('v_report_pendaftaran_poli')
                ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        return $query->result(); 
    }

    public function countDataPendaftaranPoliAll($tgl_awal, $tgl_akhir){
        $this->db
                ->from('v_report_pendaftaran_poli')
                ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        return $this->db->count_all_results();
    }
    
    public function getDataLapDinkes($tgl_awal, $tgl_akhir){
        $this->db
                ->from('v_laporandinkes')
                ->where('daftartindakan_id != " "')
                ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $length = $this->input->get('length'); 
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function countDataLapDinkes($tgl_awal, $tgl_akhir){
        $this->db
                ->from('v_laporandinkes')
                ->where('daftartindakan_id != " "')                
                //    ->where('daftartindakan_id = 39 OR daftartindakan_id = 43 OR daftartindakan_id = 44 OR daftartindakan_id = 49 OR daftartindakan_id = 91')
                ->where('tgl_pendaftaran between"'.date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        return $this->db->count_all_results();
    }




}