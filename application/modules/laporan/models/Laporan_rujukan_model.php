<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_rujukan_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_laporan($tgl_awal, $tgl_akhir){
        // $this->db->select('t_stok_farmasi.stok_masuk,t_stok_farmasi.stok_keluar,t_stok_farmasi.stok_akhir,m_barang_farmasi.nama_barang,t_stok_farmasi.instalasi_id');
        // $this->db->join('m_barang_farmasi','m_barang_farmasi.id_barang = t_stok_farmasi.barang_id');
        // $this->db->from('v_report_rujukan');
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"')->where('instalasi_id =  2');
        $this->db->from('v_report_rujukan');  
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }

    public function count_all_list($tgl_awal, $tgl_akhir){
        $this->db->from('v_report_rujukan');  
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        
        return $this->db->count_all_results();
    }

    public function count_all_list_filtered($tgl_awal, $tgl_akhir){
        $this->db->from('v_report_rujukan');  
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function get_data($tgl_awal, $tgl_akhir){
        $this->db->from('v_report_rujukan');  
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        $query = $this->db->get();
        return $query->result();
    }
}
?>