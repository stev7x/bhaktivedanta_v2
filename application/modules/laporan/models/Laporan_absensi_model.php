<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_absensi_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_absensi_list($tgl_awal, $tgl_akhir){
            $this->db->select('aauth_users.nama_lengkap,m_branch.nama,t_absen.*');
            $this->db->join('aauth_users','aauth_users.id = t_absen.id_user');
            $this->db->join('m_branch','m_branch.id_branch = t_absen.id_branch');
            $this->db->from('t_absen');  
            // $this->db->where(' BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        
        
        

        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result(); 
    }
    public function count_absensi_all(){
        $this->db->select('*');
        $this->db->from('t_absen');

        return $this->db->count_all_results();
    }
    function get_users_autocomplete($val=''){
        $this->db->select('DISTINCT(aauth_users.nama_lengkap),aauth_users.id');
        $this->db->join('t_absen','t_absen.id_user = aauth_users.id');
        $this->db->from('aauth_users');  
        $this->db->like('nama_lengkap', $val);
        $this->db->limit(20);
        $query = $this->db->get();
        
        return $query->result(); 
    }
    public function get_list_absen_user($tgl_awal,$tgl_akhir,$id_user)
    {
            // die($id_user);
            $this->db->select('aauth_users.nama_lengkap,m_branch.nama,t_absen.*');
            $this->db->join('aauth_users','aauth_users.id = t_absen.id_user');
            $this->db->join('m_branch','m_branch.id_branch = t_absen.id_branch');
            $this->db->from('t_absen');
            $this->db->where('t_absen.id_user',$id_user);  
            // $this->db->where(' BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        
        
        

        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    public function get_klinik()
    {
        $this->db->select('nama,id_branch');
        $this->db->from('m_branch');
        $query = $this->db->get();
        
        return $query->result();
    }
}