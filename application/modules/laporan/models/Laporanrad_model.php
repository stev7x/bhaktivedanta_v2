<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanrad_model extends CI_Model{
	var $table = "v_lap_radiologi";

    public function __construct() {
        parent::__construct();
    }

    public function get_radiologi_datatables($tgl_awal, $tgl_akhir){
    	$this->db->select('*');
    	$this->db->from($this->table);
    	$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

    	$query = $this->db->get();

    	return $query->result();
    }

    public function count_radiologi_all($tgl_awal, $tgl_akhir){
    	$this->db->select('*');
    	$this->db->from($this->table);
    	$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');
    	
        return $this->db->count_all_results();
    }

    public function get_export_radiologi($tgl_awal, $tgl_akhir){
    	$this->db->select('*');
    	$this->db->from($this->table);
    	$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');

    	$query = $this->db->get();

    	return $query->result();
    }
}
?>