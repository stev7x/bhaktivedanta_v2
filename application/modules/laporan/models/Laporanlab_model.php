<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanlab_model extends CI_Model{
	
    public function __construct() {
        parent::__construct();
    }
	
	function get_data_laboratorium($tgl_awal, $tgl_akhir){
		$this->db->select('*');
		$this->db->from('v_lap_laboratorium');
		$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');
		
		$length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
	
		$query = $this->db->get();
		return $query->result();
	}
	
	function count_get_data_laboratorium($tgl_awal, $tgl_akhir){
		$this->db->select('*');
		$this->db->from('v_lap_laboratorium');
		$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');
		
		$query = $this->db->count_all_results();
		return $query;
	}
	
	function get_export_laboratorium($tgl_awal, $tgl_akhir){
		$this->db->select('*');
		$this->db->from('v_lap_laboratorium');
				$this->db->where('tgl_tindakan BETWEEN "'.date('Y-m-d', strtotime($tgl_awal)).'" AND "'.date('Y-m-d', strtotime($tgl_akhir)).'"');

		$query = $this->db->get();
		return $query->result();
	}

}
/* End of file Laporanlab_model.php */