<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Apotek  -  Barang Masuk</h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Apotek</a></li>
                            <li class="active">Barang Masuk</li> 
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">  
                            <div class="panel-heading"> Data Barang Masuk Apotek
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">  
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="inputName1" class="control-label"></label>
                                                <dl class="text-right">  
                                                    <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                    <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                                </dl> 
                                        </div> 
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label>Tanggal Transaksi, Dari</label>
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                     </div>
                                                </div>
                                                <div class="col-md-5">  
                                                    <label>Sampai</label>    
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                     </div>
                                                </div>
                                                <div class="col-md-2"> 
                                                    <label>&nbsp;</label><br>
                                                    <button type="button" class="btn btn-success col-md-12" onclick="cariBarangMasuk()">Cari</button>        
                                                </div> 
                                                <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="kode_barang">Kode Barang</option>
                                                        <option value="nama_barang">Nama Barang</option>
                                                        <option value="jenis_barang">Jenis Barang</option>
                                                        <!-- <option value="nama_supplier">Supplier</option>    -->
                                                        <!-- <option value="status">Status</option>    -->
                                                    </select></b>
                                                         
                                                </div>    
                                                <div class="form-group col-md-9" style="margin-top: 7px">   
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                    <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariBarangMasuk()" >       
                                                </div> 
                                            </div>
                                        </div> 
                                    </div><br><hr style="margin-top: -27px">


                                    <div class="col-md-12" style="margin-top: -10px">
                                        <ul class="nav customtab nav-tabs" role="tablist">
                                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#barang_masuk" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Barang Masuk</span></a></li>
                                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#barang_approval" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Pengajuan Barang</span></a></li>  
                                        </ul>
                                        <div class="tab-content"> 
                                            <div role="tabpanel" class="tab-pane fade active in" id="barang_masuk">
                                                <div class="table">   
                                                    <table id="table_barang_masuk_list" class="table table-responsive table-striped dataTable" cellspacing="0">
                                                        <thead>  
                                                            <tr> 
                                                                <th>No</th>
                                                                <th>Tanggal Masuk</th>
                                                                <th>Kode Barang</th>
                                                                <th>Nama Barang</th>
                                                                <th>Jenis Barang</th>
                                                                <th>Satuan</th>
                                                                <th>Jumlah Barang</th>
                                                                <th>Harga Satuan</th>
                                                                <th>Harga Pack</th>
                                                                <!-- <th>Nama Suplier</th> -->
                                                                <th>Total Harga</th>
                                                                <th>Expired</th>
                                                                <th>Keterangan</th>
                                                                <!-- <th>Aksi</th>  -->
                                                            </tr>  
                                                        </thead>
                                                        <tbody> 
                                                            <tr>   
                                                                <td colspan="4">No Data to Display</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>  
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="barang_approval">
                                                <div class="row" style="padding-bottom: 30px;margin-top: -10px"> 
                                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_pengajuan_barang" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus p-r-10"></i>Tambah Barang Masuk Apotek</button>
                                                </div>
                                                <div class="table">   
                                                    <table id="table_barang_pengajuan_list" class="table table-responsive table-striped dataTable" cellspacing="0">
                                                        <thead>  
                                                            <tr> 
                                                                <th>No</th>
                                                                <th>Tanggal Pengajuan</th>
                                                                <th>Kode Barang</th>
                                                                <th>Nama Barang</th>
                                                                <th>Jumlah Barang</th>
                                                                <th>Tanggal Respon</th>
                                                                <th>Status</th>
                                                                <th>Aksi</th>
                                                            </tr>  
                                                        </thead>
                                                        <tbody> 
                                                            <tr>   
                                                                <td colspan="4">No Data to Display</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->

<div id="modal_pengajuan_barang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog modal-large" style="max-width: 1000px">
    <?php echo form_open('#',array('id' => 'fmCreateBarangPengajuan'))?>
    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

        <div class="modal-content" style="height:530px;">   
            <div class="modal-header" style="background: #01c0c8;">  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b style="color: white">Tambah Barang Masuk Apotek</b></h2>     
             </div> 
             <div class="modal-body" style="background: #ecf0f4;overflow: auto;">
                <div class="white-box">
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kode Barang</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="kode_barang" name="kode_barang" placeholder="Kode Barang" readonly>
                                    <span class="input-group-btn">  
                                        <button class="btn btn-info" type="button" title="Lihat List Kode Barang" data-toggle="modal" data-target="#modal_list_barang" onclick="dialogBarang()"><i class="fa fa-list"></i></button>
                                    </span>
                                </div>
                            </div>    
                            <div class="form-group">
                                <label>Nama Barang</label>
                                <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama Barang" readonly />
                            </div>    
                            <div class="form-group">
                                <label>Satuan</label>
                                <select class="form-control" name="kode_sediaan" id="kode_sediaan" readonly>
                                    <option value="">Pilih Satuan</option>
                                    <?php
                                    $list_sediaan = $this->Barang_masuk_apotek_model->get_list_sediaan();
                                    foreach ($list_sediaan as $list) {
                                        echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                    }
                                    ?>
                                </select>
                            </div>    
                            <div class="form-group">
                                <label>Jenis Barang</label>
                                <select class="form-control" name="jenis_barang" id="jenis_barang" readonly>
                                <option value="">Pilih Jenis Barang</option>
                                    <?php 
                                        $list_jenis = $this->Barang_masuk_apotek_model->get_jenis_barang();
                                        foreach ($list_jenis as $list) {
                                            echo "<option value=".$list->id_jenis_barang.">".$list->nama_jenis."</option>";
                                        }
                                    ?> 
                                </select>
                            </div>    
                            <div class="form-group">
                                <label>Expired</label>
                                <div class="input-group"> 
                                    <input name="expired" id="expired" type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy" readonly> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                </div>
                            </div> 
                        </div>
                        <input type="hidden" name="harga_satuan" id="harga_satuan">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jumlah Barang</label>
                                <input type="text" name="jumlah_barang" id="jumlah_barang" class="form-control" placeholder="Jumlah Barang" />
                            </div> 
                            <div class="form-group">
                                <label>Catatan</label>
                                <textarea rows="4" class="form-control" cols="" name="catatan" id="catatan" style="max-height:none !important" ></textarea>
                            </div> 
                        </div>
                    </div>
                    
                    <br><br>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success col-sm-2" id="savePengajuan"><i class="fa fa-floppy-o"></i> &nbsp;SIMPAN</button> 
                        </div>
                    </div>

                </div>
             </div>
              
          </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal_detail_approval" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-lg">         
        <div class="modal-content" style="margin-top:20%;">   
            <div class="modal-header" style="background: #01c0c8;"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel" style="color:white;"><b>Detail Data Pengajuan</b></h2>     
             </div>  
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row"> 
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                                <div>
                                    <p id="card_message1" class=""></p>
                                </div>
                            </div>
                        </div> 
                            <div class="col-md-12">
                                <div class="card" style="width:100%;text-align:center;">
                                    <div class="card-block">
                                        <h4 class="card-title">Data Pengajuan</h4>
                                        <table align="center" border="0px" width="70%">
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Kode Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_kode_barang_lbl"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Nama Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_nama_barang_lbl">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Jumlah Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_jumlah_barang_lbl">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Catatan Pengajuan
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_alasan_pengajuan_lbl">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"><br><br></div>
                            <div class="col-md-12">
                                <div class="card" style="width:100%;text-align:center;">
                                    <div class="card-block">
                                        <h4 class="card-title">Data Respon</h4>
                                        <table align="center" border="0px" width="70%">
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Kode Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_kode_barang_lbl2"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Nama Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_nama_barang_lbl2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Jumlah Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_jumlah_barang_lbl2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Status Pengajuan
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_status_lbl"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl" valign="top">
                                                    <label for=""></label>Catatan Respon
                                                </td>
                                                <td valign="top">:</td>
                                                <td id="val">
                                                    <label id="det_alasan_respon_lbl"></label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<style>
    #lbl{
        width:50%;
        text-align:left;
        font-weight:bold;
    }
    #val{
        width:50%;
        padding-left:10px;
        text-align:left;
        vertical-align:text-bottom;
    }
</style>

 


<div id="modal_list_barang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-lg">          
        <div class="modal-content" style="margin-top:102px; margin-right:6%; margin-left:6%;">   
            <div class="modal-header" style="background: #b1bfbf;"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b style="color: #323236;">Daftar Kode Barang</b></h2>     
             </div>   
            <div class="modal-body" style="height: 350px;overflow: auto;">
                <div class="row">       
                <div class="table-responsive">   
                <table id="table_daftar_barang_list" class="table table-striped dataTable" cellspacing="0">
                         <thead>  
                             <tr> 
                                 <th>Kode Barang</th>
                                 <th>Nama Barang</th>
                                 <th>Jenis Barang</th>
                                 <th>Satuan</th>
                                 <th>Stok Akhir</th>
                                 <th>Kondisi Barang</th>
                                 <th>Pilih</th>
                             </tr>  
                         </thead>
                         <tbody> 
                             <tr>   
                                 <td colspan="4">No Data to Display</td>
                             </tr>
                         </tbody>
                     </table>  
                 </div>
                    
                </div>  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<?php $this->load->view('footer');?>
      