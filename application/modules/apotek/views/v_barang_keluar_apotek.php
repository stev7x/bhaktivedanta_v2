<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Apotek  -  Barang Keluar</h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Apotek</a></li>
                            <li class="active">Barang Keluar</li> 
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">  
                            <div class="panel-heading"> Data Barang Keluar Apotek
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">  
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="inputName1" class="control-label"></label>
                                                <dl class="text-right">  
                                                    <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                    <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                                </dl> 
                                        </div> 
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label>Tanggal Transaksi, Dari</label>
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                     </div>
                                                </div>
                                                <div class="col-md-5">  
                                                    <label>Sampai</label>    
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                     </div>
                                                </div>
                                                <div class="col-md-2"> 
                                                    <label>&nbsp;</label><br>
                                                    <button type="button" class="btn btn-success col-md-12" onclick="cariBarangMasuk()">Cari</button>        
                                                </div> 
                                                <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="kode_barang">Kode Barang</option>
                                                        <option value="nama_barang">Nama Barang</option>
                                                        <option value="jenis_barang">Jenis Barang</option>
                                                        <option value="nama_supplier">Supplier</option>   
                                                        <!-- <option value="status">Status</option>    -->
                                                    </select></b>
                                                         
                                                </div>    
                                                <div class="form-group col-md-9" style="margin-top: 7px">   
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                    <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariBarangMasuk()" >       
                                                </div> 
                                            </div>
                                        </div> 
                                    </div><br><hr style="margin-top: -27px">
                                    <!--   
                                    <div class="row" style="padding-bottom: 30px;margin-top: -10px"> 

                                     <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_masuk_barang" ><i class="fa fa-plus p-r-10"></i>Tambah Barang Masuk Apotek</button>
                                   </div> -->
                                    
                                    <div class="table">   
                                        <table id="table_barang_keluar_apotek" class="table table-responsive table-striped dataTable" cellspacing="0">
                                            <thead>  
                                                <tr> 
                                                    <th>No</th>
                                                    <th>Tanggal Keluar</th>
                                                    <th>Kode Barang</th>
                                                    <th>Nama Barang</th>
                                                    <th>Satuan</th>
                                                    <th>Jenis Barang</th>
                                                    <th>Jumlah Barang</th>
                                                    <th>Harga Satuan</th>
                                                    <!-- <th>Harga Pack</th> -->
                                                    <th>Di Tujukan</th>
                                                    <th>Keterangan</th> 
                                                    <!-- <th>Aksi</th>  -->
                                                </tr>  
                                            </thead>
                                            <tbody> 
                                                <tr>   
                                                    <td colspan="4">No Data to Display</td>
                                                </tr>
                                            </tbody>
                                        </table>  
                                    </div>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->





<?php $this->load->view('footer');?>
      