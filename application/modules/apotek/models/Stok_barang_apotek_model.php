<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_barang_apotek_model extends CI_Model {  
    var $column = array('kode_barang','expired');
    var $order = array('expired' => 'ASC');

   
    public function __construct() {
        parent::__construct();
    } 
     
    public function get_barang_stok_list($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired){
        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join("m_sediaan_obat","m_sediaan_obat.id_sediaan = t_apotek_stok.id_sediaan","left");
        $this->db->join("m_jenis_barang_farmasi","m_jenis_barang_farmasi.id_jenis_barang = t_apotek_stok.id_jenis_barang","left");
        $this->db->from('t_apotek_stok');
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($kode_barang)){
            $this->db->like('kode_barang', $kode_barang);
        }
        if(!empty($nama_barang)){
            $this->db->like('nama_barang', $nama_barang);
        }
        if(!empty($jenis_barang)){
            $this->db->like('nama_jenis', $jenis_barang);
        }       

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        
        return $query->result();
    }

    public function count_barang_stok_all($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired){
         $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        $this->db->join("m_sediaan_obat","m_sediaan_obat.id_sediaan = t_apotek_stok.id_sediaan","left");
        $this->db->join("m_jenis_barang_farmasi","m_jenis_barang_farmasi.id_jenis_barang = t_apotek_stok.id_jenis_barang","left");
        $this->db->from('t_apotek_stok');

        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($kode_barang)){
            $this->db->like('kode_barang', $kode_barang);
        }
        if(!empty($nama_barang)){
            $this->db->like('nama_barang', $nama_barang);
        }
        if(!empty($jenis_barang)){
            $this->db->like('nama_jenis', $jenis_barang);
        }
     
        return $this->db->count_all_results();
    }
    
   

 
}