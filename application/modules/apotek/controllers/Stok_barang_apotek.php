<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_barang_apotek extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Stok_barang_apotek_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);

        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('apotek_stok_barang_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "apotek_stok_barang_view";
        $comments = "Stok Barang";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_stok_barang_apotek', $this->data);
    }

    function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }


    public function ajax_list_barang_stok(){
        $kode_barang    = $this->input->get('kode_barang',TRUE);
        $nama_barang    = $this->input->get('nama_barang',TRUE);
        $jenis_barang   = $this->input->get('jenis_barang',TRUE);
        $kondisi_barang = $this->input->get('kondisi_barang',TRUE);
        $expired        = $this->input->get('expired',TRUE);
        $list = $this->Stok_barang_apotek_model->get_barang_stok_list($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired);
        // $list = $this->Barang_stok_model->get_barang_stok_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $barang_stok){
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = $barang_stok->expired;
            $row[] = $barang_stok->kode_barang;
            $row[] = $barang_stok->nama_barang;
            $row[] = $barang_stok->nama_jenis;
            $row[] = $barang_stok->nama_sediaan;
            $row[] = $barang_stok->stok_masuk;
            $row[] = $barang_stok->stok_keluar;
            $row[] = $barang_stok->stok_akhir;
            $row[] = $barang_stok->kondisi_barang;
            $row[] = $barang_stok->harga_satuan;
            // $row[] = $barang_stok->harga_pack;
            $row[] = $barang_stok->keterangan;
            $row[] =  date('d-M-Y H:i:s', strtotime($barang_stok->perubahan_terakhir));
            $row[] = '
                     <button class="btn btn-danger btn-circle" titel="klik untuk hapus" onclick="hapusBarang('."'".$barang_stok->kode_barang."'".')"><i class="fa fa-trash"></i></button>
                     ';

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Stok_barang_apotek_model->count_barang_stok_all($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired),
                    "recordsFiltered" => $this->Stok_barang_apotek_model->count_barang_stok_all($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    function convert_to_number($rupiah){
        return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
     }

    public function do_create_barang_masuk(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_masuk_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        // form validate
        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_barang','Kode Barang', 'required|trim');
        $this->form_validation->set_rules('nama_barang','Nama Barang', 'required|trim');
        $this->form_validation->set_rules('kode_sediaan','Satuan', 'required|trim');
        $this->form_validation->set_rules('jenis_barang','Jenis Barang', 'required|trim');
        $this->form_validation->set_rules('jenis_transaksi','Jenis transaksi', 'required|trim');
        $this->form_validation->set_rules('kode_supplier','Nama Supplier', 'required|trim');
        $this->form_validation->set_rules('harga_barang_keseluruhan','Harga Barang Keseluruhan', 'required|trim');
        $this->form_validation->set_rules('jumlah_barang','Jumlah Barang Keseluruhan', 'required|trim');


        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "farmasi_barang_masuk_view";
            $comments = "Gagal input Barang Masuk pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $tanggal_masuk   = date('Y-m-d H:i:s');
            // $tanggal_masuk1  = date_format($tanggal_masuk, 'Y-m-d');

            $kode_barang     = $this->input->post('kode_barang', TRUE);
            $nama_barang     = $this->input->post('nama_barang', TRUE);
            $kode_sediaan    = $this->input->post('kode_sediaan', TRUE);
            $jenis_barang    = $this->input->post('jenis_barang', TRUE);
            $jumlah_barang   = $this->input->post('jumlah_barang', TRUE);
            $harga_satuan    = $this->input->post('harga_barang_per_item', TRUE);
            $harga_satuan    = $this->input->post('harga_barang_per_itema', TRUE);
            $harga_pack      = $this->input->post('harga_barang_per_pack', TRUE);
            $total_harga     = $this->input->post('harga_barang_keseluruhan', TRUE);
            $kode_supplier   = $this->input->post('kode_supplier', TRUE);
            $exp             = date_create($this->input->post('exp', TRUE));
            $exp1            = date_format($exp, 'Y-m-d');
            $keterangan      = $this->input->post('keterangan', TRUE);

            $harga_satuan_int = $this->convert_to_number($harga_satuan);
            $harga_pack_int   = $this->convert_to_number($harga_pack);
            // $total_harga_int = $this->convert_to_number($total_harga);


            $data_barang_masuk = array(


                'tanggal_masuk'     => $tanggal_masuk,
                'kode_barang'       => $kode_barang,
                'nama_barang'       => $nama_barang,
                'satuan'            => $kode_sediaan,
                'jenis_barang'      => $jenis_barang,
                'jumlah_barang'     => $jumlah_barang,
                'harga_satuan'      => $harga_satuan_int,
                'harga_pack'        => $harga_pack_int,
                'total_harga'       => $total_harga,
                'nama_supplier'     => $kode_supplier,
                'exp'               => $exp1,
                'keterangan'        => $keterangan
            );

            $ins = $this->Barang_masuk_apotek_model->insert_barang_masuk($data_barang_masuk);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => true,
                    'messages'      => 'Barang Masuk berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms       = "farmasi_barang_masuk_view";
                $comments    = "Berhasil menambahkan Barang Masuk dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal menambahkan Barang Masuk, hubungi web administrator.');

                // if permitted, do logit
                $perms = "farmasi_barang_masuk_view";
                $comments = "Gagal menambahkan Barang Masuk dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_barang_masuk(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_masuk_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_barang_masuk = $this->input->post('id_barang_masuk', TRUE);

        $delete = $this->Barang_masuk_apotek_model->delete_barang_masuk($id_barang_masuk);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => true,
                'messages'      => 'Barang Masuk Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_masuk_delete";
            $comments = "Berhasil membatalkan Barang Masuk Pasien dengan id Barang Masuk Pasien Operasi = '". $id_barang_masuk ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_masuk_delete";
            $comments = "Gagal membatalkan data Barang Masuk Pasien dengan ID = '". $id_barang_masuk ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }


    public function ajax_list_daftar_barang(){
        $list = $this->Barang_masuk_apotek_model->get_list_daftar_barang();
        $data = array();
        foreach($list as $daftar_barang){
            $row   = array();
            $row[] = $daftar_barang->kode_barang;
            $row[] = $daftar_barang->nama_barang;
            $row[] = $daftar_barang->jenis_barang;
            $row[] = $daftar_barang->stok_akhir;
            $row[] = $daftar_barang->kondisi_barang;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Barang Masuk" onclick="pilihBarang('."'".$daftar_barang->kode_barang."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw"              => $this->input->get('draw'),
            "recordsTotal"      => $this->Barang_masuk_apotek_model->count_list_daftar_barang(),
            "recordsFiltered"   => $this->Barang_masuk_apotek_model->count_list_filtered_daftar_barang(),
            "data"              => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_get_daftar_barang_by_id(){
        $kode_barang = $this->input->get('kode_barang',TRUE);
        $data_daftar_barang = $this->Barang_masuk_apotek_model->get_daftar_barang_by_id($kode_barang);
        if (sizeof($data_daftar_barang)>0){
            $daftar_barang = $data_daftar_barang[0];
            $res = array(
                "success" => true,
                "messages" => "Data Barang Masuk ditemukan",
                "data" => $daftar_barang
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data Barang Masuk tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }

}
