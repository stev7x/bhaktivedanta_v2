<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_keluar_apotek extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Barang_keluar_apotek_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);

        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('apotek_barang_keluar_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "apotek_barang_keluar_view";
        $comments = "Barang keluar";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_barang_keluar_apotek', $this->data);
    }

    function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }


    public function ajax_list_barang_keluar(){
        $tgl_awal        = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir       = $this->input->get('tgl_akhir',TRUE);
        $kode_barang     = $this->input->get('kode_barang',TRUE);
        $nama_barang     = $this->input->get('nama_barang',TRUE);
        $jenis_barang = $this->input->get('jenis_barang',TRUE);
        $nama_supplier   = $this->input->get('nama_supplier',TRUE);
        $list = $this->Barang_keluar_apotek_model->get_barang_keluar_list($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier);
        $data   = array();
        $no     = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $barang_keluar){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d-M-Y H:i:s', strtotime($barang_keluar->tanggal_keluar));
            $row[] = $barang_keluar->kode_barang;
            $row[] = $barang_keluar->nama_barang;
            $row[] = $barang_keluar->nama_sediaan;
            $row[] = $barang_keluar->nama_jenis;
            $row[] = $barang_keluar->jumlah_barang;
            $row[] = $this->convert_to_rupiah($barang_keluar->harga_satuan).",00";
            // $row[] = $this->convert_to_rupiah($barang_keluar->harga_pack).",00";
            $row[] = $barang_keluar->di_tujukan;
            // $row[] = $barang_keluar->dari_gudang;
            // $row[] = $this->convert_to_rupiah($barang_keluar->harga_satuan).",00";
            // $row[] = $barang_keluar->nama_supplier;
            // $row[] = date('d-M-Y', strtotime($barang_keluar->expired));
            $row[] = $barang_keluar->keterangan;
            // $row[] = '<button class="btn btn-danger btn-circle" onclick="hapusBarangMasuk('."'".$barang_keluar->id_barang_keluar."'".')"><i class="fa fa-trash"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw"            => $this->input->get('draw'),
                    "recordsTotal"    => $this->Barang_keluar_apotek_model->count_barang_keluar_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier),
                    "recordsFiltered" => $this->Barang_keluar_apotek_model->count_barang_keluar_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier),
                    "data"            => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    function convert_to_number($rupiah){
        return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
     }




    public function ajax_list_daftar_barang(){
        $list = $this->barang_keluar_apotek_model->get_list_daftar_barang();
        $data = array();
        foreach($list as $daftar_barang){
            $row   = array();
            $row[] = $daftar_barang->kode_barang;
            $row[] = $daftar_barang->nama_barang;
            $row[] = $daftar_barang->jenis_barang;
            $row[] = $daftar_barang->stok_akhir;
            $row[] = $daftar_barang->kondisi_barang;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Barang Masuk" onclick="pilihBarang('."'".$daftar_barang->kode_barang."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw"              => $this->input->get('draw'),
            "recordsTotal"      => $this->barang_keluar_apotek_model->count_list_daftar_barang(),
            "recordsFiltered"   => $this->barang_keluar_apotek_model->count_list_filtered_daftar_barang(),
            "data"              => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_get_daftar_barang_by_id(){
        $kode_barang = $this->input->get('kode_barang',TRUE);
        $data_daftar_barang = $this->barang_keluar_apotek_model->get_daftar_barang_by_id($kode_barang);
        if (sizeof($data_daftar_barang)>0){
            $daftar_barang = $data_daftar_barang[0];
            $res = array(
                "success" => true,
                "messages" => "Data Barang Masuk ditemukan",
                "data" => $daftar_barang
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data Barang Masuk tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }

}
