<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends CI_Controller {
//    public $data = array();
    
    public function _construct(){
        parent::__construct();
        $this->load->model('Hello_model');
    }
    
    public function index(){
        $this->load->model('Hello_model');
//        $data['test'] = $this->Hello_model->get_test()->result();
        $this->load->view('hello');
    }
}

