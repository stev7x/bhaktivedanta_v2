<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Checkin_model');

        $this->data['users'] = $this->aauth->get_user();
        $this->data['groups'] = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar'] = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);

    }

    public function do_checkin(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('todo','Todo', 'required|trim');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
           

        }else if ($this->form_validation->run() == TRUE)  {
            $todo = $this->input->post('todo', TRUE);
            $absen_date = date("Y-m-d H:i:s");
            $user_id = $this->aauth->get_user()->id;
            $branch_id = $this->session->tempdata('data_session')[0];
            $status = 'CHECKIN';
            $data_tindakanpasien = array(
                'id_user'        => $user_id,
                'id_branch'      => $branch_id,
                'to_do'          => $todo,
                'absen_date'     => $absen_date,
                'status'         => $status
            );

            $ins = $this->Checkin_model->checkin($data_tindakanpasien);
            
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $this->session->set_tempdata('status_checkin', $this->Checkin_model->getStatusCheckin($this->data['users']->id,date("Y-m-d")), 3600*24);
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');
                
                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
	
    }

    public function do_checkout(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('todo','Todo', 'required|trim');
        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
           

        }else if ($this->form_validation->run() == TRUE)  {
            $todo = $this->input->post('todo', TRUE);
            $absen_date = date("Y-m-d H:i:s");
            $user_id = $this->aauth->get_user()->id;
            $branch_id = $this->session->tempdata('data_session')[0];
            $status = 'CHECKOUT';
            $data_tindakanpasien = array(
                'id_user'        => $user_id,
                'id_branch'      => $branch_id,
                'to_do'          => $todo,
                'absen_date'     => $absen_date,
                'status'         => $status
            );

            $ins = $this->Checkin_model->checkout($data_tindakanpasien);
            
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $this->session->set_tempdata('status_checkin', $this->Checkin_model->getStatusCheckin($this->data['users']->id,date("Y-m-d")), 3600*24);
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');
                
                // if permitted, do logit
                $perms = "penunjang_laboratorium_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
	
    }
    
}
