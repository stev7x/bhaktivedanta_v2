<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin_model extends CI_Model {

	public function __construct(){
        parent::__construct();
    }

    public function getBranchList() {
        $this->db->select('id_branch,nama');
        $this->db->from('m_branch');
        $query = $this->db->get();
        return $query->result();
    }

    public function getStatusCheckin($id,$tanggal) {
        $this->db->select('status');
        $this->db->from('t_absen');
        $this->db->where('id_user',$id);
        $this->db->like('absen_date',$tanggal);
        $this->db->order_by("absen_date", "desc");
        $query = $this->db->get();
        $ret = $query->row();
        if ($ret) {
            return $ret->status;
        }
        else {
            return "BELUM CHECKIN";
        }
        
    }

    public function checkin($data=array()){
        $insert = $this->db->insert('t_absen',$data);
        return $insert;
    }

    public function checkout($data=array()){
        $insert = $this->db->insert('t_absen',$data);
        return $insert;
    }

}