<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Floor_stok extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Floor_stok_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_stok_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "farmasi_barang_stok_view";
        $comments = "Barang stok";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_floor_stok', $this->data);
    }

    public function ajax_list_floor_stok(){
        // $tgl_awal       = $this->input->get('tgl_awal',TRUE);
        // $tgl_akhir      = $this->input->get('tgl_akhir',TRUE);
        // $kode_barang    = $this->input->get('kode_barang',TRUE);
        // $nama_barang    = $this->input->get('nama_barang',TRUE);
        // $jenis_barang   = $this->input->get('jenis_barang',TRUE);
        // $kondisi_barang = $this->input->get('kondisi_barang',TRUE);
        // $expired        = $this->input->get('expired',TRUE);
        // $list = $this->Floor_stok_model->get_floor_stok_list($tgl_awal, $tgl_akhir, $nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired);
        $list = $this->Floor_stok_model->get_floor_stok_list();
        // $list = $this->floor_stok_model->get_floor_stok_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $floor_stok){
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = $floor_stok->expired;
            $row[] = $floor_stok->kode_barang;
            $row[] = $floor_stok->nama_barang;
            $row[] = $floor_stok->jenis_barang;
            $row[] = $floor_stok->stok_masuk;
            $row[] = $floor_stok->stok_keluar;
            $row[] = $floor_stok->stok_akhir;
            $row[] = $floor_stok->kondisi_barang;
            $row[] = $floor_stok->harga_satuan;
            $row[] = $floor_stok->harga_pack;
            $row[] = $floor_stok->satuan;
            $row[] = $floor_stok->keterangan;
            $row[] =  date('d-M-Y H:i:s', strtotime($floor_stok->perubahan_terakhir));

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    // "recordsTotal" => $this->Floor_stok_model->count_floor_stok_all($tgl_awal, $tgl_akhir, $nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired),
                    "recordsTotal" => $this->Floor_stok_model->count_floor_stok_all(),
                    "recordsFiltered" => $this->Floor_stok_model->count_floor_stok_all(),
                    // "recordsFiltered" => $this->Floor_stok_model->count_floor_stok_all($tgl_awal, $tgl_akhir, $nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    // public function ajax_list_daftar_barang(){
    //     $list = $this->floor_stok_model->get_list_daftar_barang();
    //     $data = array();
    //     foreach($list as $daftar_barang){
    //         $row = array();

    //         $row[] = $daftar_barang->kode_barang;
    //         $row[] = $daftar_barang->nama_barang;
    //         if($daftar_barang->id_jenis_barang == 1){
    //             $row[] = 'Obat';
    //         }else if($daftar_barang->id_jenis_barang == 2){
    //             $row[] = 'Alkes';
    //         }else if($daftar_barang->id_jenis_barang == 3){
    //             $row[] = 'BHP';
    //         }else{
    //             $row[] = 'Tidak Valid';
    //         }
    //         $row[] = $daftar_barang->stok_akhir;
    //         $row[] = $daftar_barang->kondisi_barang;
    //         $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Diagnosa" onclick="pilihBarang('."'".$daftar_barang->kode_barang."'".')"><i class="fa fa-check"></i></button>';

    //         $data[] = $row;
    //     }

    //     $output = array(
    //         "draw" => $this->input->get('draw'),
    //         "recordsTotal" => $this->floor_stok_model->count_list_daftar_barang(),
    //         "recordsFiltered" => $this->floor_stok_model->count_list_filtered_daftar_barang(),
    //         "data" => $data,
    //     );
    //     //output to json format
    //     echo json_encode($output);
    // }


    // public function ajax_get_daftar_barang_by_id(){
    //     $kode_barang = $this->input->get('kode_barang',TRUE);
    //     $data_daftar_barang = $this->floor_stok_model->get_daftar_barang_by_id($kode_barang);
    //     if (sizeof($data_daftar_barang)>0){
    //         $daftar_barang = $data_daftar_barang[0];
    //         $res = array(
    //             "success" => true,
    //             "messages" => "Data diagnosa ditemukan",
    //             "data" => $daftar_barang
    //         );
    //     } else {
    //         $res = array(
    //             "success" => false,
    //             "messages" => "Data diagnosa tidak ditemukan",
    //             "data" => null
    //         );
    //     }
    //     echo json_encode($res);
    // }

}
