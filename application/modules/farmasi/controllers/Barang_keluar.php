<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_keluar extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Barang_keluar_model');
        $this->load->model('rekam_medis/list_pasien_model');
        $this->data['list_pengambilan_barang']    = array('1'=>'Per Pack','2'=>'Per Pieces');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        $this->data['data_user'] = $this->list_pasien_model->get_data_user($this->data['users']->id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;

    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_keluar_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "farmasi_barang_keluar_view";
        $comments = "Barang keluar";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_barang_keluar', $this->data);
    }

    public function ajax_list_barang_keluar(){
        $tgl_awal = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir = $this->input->get('tgl_akhir',TRUE);
        $kode_barang = $this->input->get('kode_barang',TRUE);
        $nama_barang = $this->input->get('nama_barang',TRUE);
        $jenis_barang = $this->input->get('jenis_barang',TRUE);
        $dari_gudang = $this->input->get('dari_gudang',TRUE);
        $di_tujukan = $this->input->get('di_tujukan',TRUE);

        $list = $this->Barang_keluar_model->get_barang_keluar_list($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $dari_gudang, $di_tujukan);

        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $barang_keluar){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d-M-Y H:i:s', strtotime($barang_keluar->tanggal_keluar));
            $row[] = $barang_keluar->kode_barang;
            $row[] = $barang_keluar->nama_barang;
            $row[] = $barang_keluar->nama_jenis;
            $row[] = $barang_keluar->nama_sediaan;
            $row[] = $barang_keluar->jumlah_barang;
            $row[] = $barang_keluar->dari_gudang == 1 ? "Farmasi" : "Farmasi" ;
            $row[] = $barang_keluar->di_tujukan  == 1 ? "Apotek" : "Floor Stok";
            $row[] = $barang_keluar->keterangan;
            $row[] = '<button class="btn btn-danger btn-circle" onclick="hapusBarangKeluar('."'".$barang_keluar->id_barang_keluar."'".')"><i class="fa fa-trash"></i></button>';


            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Barang_keluar_model->count_barang_keluar_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $dari_gudang, $di_tujukan),
                    "recordsFiltered" => $this->Barang_keluar_model->count_barang_keluar_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $dari_gudang, $di_tujukan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

     public function ajax_list_barang_pengajuan(){
        $tgl_awal        = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir       = $this->input->get('tgl_akhir',TRUE);
        $kode_barang     = $this->input->get('kode_barang',TRUE);
        $nama_barang     = $this->input->get('nama_barang',TRUE);
        $jenis_barang = $this->input->get('jenis_barang',TRUE);
        $nama_supplier   = $this->input->get('nama_supplier',TRUE);
        $list = $this->Barang_keluar_model->get_barang_pengajuan_list($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier);
        // $list   = $this->Barang_masuk_apotek_model->get_barang_masuk_list();
        $data   = array();
        $no     = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $barang_pengajuan){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $barang_pengajuan->tanggal_pengajuan;
            $row[] = $barang_pengajuan->kode_barang;
            $row[] = $barang_pengajuan->nama_barang;
            $row[] = $barang_pengajuan->jumlah_barang;
            $row[] = $barang_pengajuan->tanggal_respons;

        if($barang_pengajuan->status_approval == "Ditolak"){
                $row[] = '<span class="label label-danger">'.$barang_pengajuan->status_approval.'</span>';
                $row[] = '
                        <button class="btn btn-info btn-circle" title="klik untuk lihat detail" onclick="pilihDetailPengajuan('."'".$barang_pengajuan->id_approval."'".')"><i class="fa fa-search-plus"></i></button>

                ';
            }else if($barang_pengajuan->status_approval == "Disetujui"){
             $row[] = '<span class="label label-success">'.$barang_pengajuan->status_approval.'</span>';
             $row[] = '
                        <button class="btn btn-info btn-circle" title="klik untuk lihat detail" onclick="pilihDetailPengajuan('."'".$barang_pengajuan->id_approval."'".')"><i class="fa fa-search-plus"></i></button>

             ';
            }else{
                $row[] = '<span class="label label-warning">'.$barang_pengajuan->status_approval.'</span>';
                $row[] = '
                        <button class="btn btn-info btn-circle" title="klik untuk cancel" onclick="pilihPengajuan('."'".$barang_pengajuan->id_approval."'".')"><i class="fa fa-check"></i></button>

                ';
            }
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw"            => $this->input->get('draw'),
                    "recordsTotal"    => $this->Barang_keluar_model->count_barang_pengajuan_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier),
                    "recordsFiltered" => $this->Barang_keluar_model->count_barang_pengajuan_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier),
                    "data"            => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    function convert_to_number($rupiah){
        return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
     }

    public function do_create_barang_keluar(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_keluar_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        // form validate
        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_barang','Kode Barang', 'required|trim');
        $this->form_validation->set_rules('jumlah_barang','Jumlah Barang Keseluruhan', 'required|trim');
        $this->form_validation->set_rules('jenis_transaksi','Jenis Transaksi', 'required|trim');
        $this->form_validation->set_rules('dari_gudang','Asal Gudang', 'required|trim');
        $this->form_validation->set_rules('di_tujukan','Tujuan', 'required|trim');


        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "farmasi_barang_keluar_create";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $tanggal_keluar   = date('Y-m-d H:i:s');
            $kode_barang = $this->input->post('kode_barang', TRUE);
            $nama_barang = $this->input->post('nama_barang', TRUE);
            $kode_sediaan = $this->input->post('kode_sediaan', TRUE);
            $jenis_barang = $this->input->post('jenis_barang', TRUE);
            $jumlah_barang = $this->input->post('jumlah_barang', TRUE);
            $dari_gudang = $this->input->post('dari_gudang', TRUE);
            $di_tujukan = $this->input->post('di_tujukan', TRUE);
            $keterangan = $this->input->post('keterangan', TRUE);
            $expired = $this->input->post('expired', TRUE);
            $harga_satuan = $this->input->post('harga_satuan', TRUE);
            $harga_pack = $this->input->post('harga_pack', TRUE);
            $total = $harga_satuan * $jumlah_barang;


            $data_barang_keluar = array(
                'tanggal_keluar'    => $tanggal_keluar,
                'kode_barang'       => $kode_barang,
                'nama_barang'       => $nama_barang,
                'id_sediaan'        => $kode_sediaan,
                'id_jenis_barang'   => $jenis_barang,
                'jumlah_barang'     => $jumlah_barang,
                'dari_gudang'       => $dari_gudang,
                'di_tujukan'        => $di_tujukan,
                'keterangan'        => $keterangan
            );

            if ($di_tujukan == 1){
                $data_barang_masuk_apotek = array(
                    'tanggal_masuk'     => $tanggal_keluar,
                    'kode_barang'       => $kode_barang,
                    'nama_barang'       => $nama_barang,
                    'id_sediaan'        => $kode_sediaan,
                    'id_jenis_barang'   => $jenis_barang,
                    'jumlah_barang'     => $jumlah_barang,
                    'expired'           => $expired,
                    'harga_satuan'      => $harga_satuan,
                    'harga_pack'        => $harga_pack,
                    'total_harga'       => $total
                );

                $data_barang_stok_apotek = array(
                    'kode_barang'       => $kode_barang,
                    'nama_barang'       => $nama_barang,
                    'id_sediaan'        => $kode_sediaan,
                    'id_jenis_barang'   => $jenis_barang,
                    'stok_masuk'        => $jumlah_barang,
                    'stok_keluar'       => 0,
                    'stok_akhir'        => $jumlah_barang,
                    'perubahan_terakhir'=> $tanggal_keluar,
                    'harga_satuan'      => $harga_satuan,
                    'harga_pack'        => $harga_pack

                );

                    $notif = array(
                        'title'         => $this->data['data_user']->name ,
                        'description'   => 'Barang baru telah masuk',
                        'sender'        => $this->data['data_user']->name,
                        'receiver'      => 'Apotik',
                        'date_notif'    =>  date('Y-m-d H:i:s'),
                        'view'          => 0
                    );
                    $this->Barang_keluar_model->insert_notif($notif);
                // print_r($notif);die();

            }

            $query1 = $this->db->get_where('t_stok_farmasi',"stok_akhir <=' $jumlah_barang'"); //cek stok tersedia
            $query = $this->db->get_where('t_apotek_stok',array('kode_barang'=>$kode_barang));//cek kode barang

            // print_r($query1);die();

            if ($query1->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Stok barang tidak mencukupi');
                    // var_dump($query1);die();
                    // if permitted, do logit

                    $perms = "master_data_barang_farmasi_create";
                    $comments = "Gagal menambahkan kode barang dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{

                if($query->num_rows() > 0){
                    $ins = $this->Barang_keluar_model->insert_barang_keluar($data_barang_keluar);
                    $ins = $this->Barang_keluar_model->insert_barang_masuk_apotek($data_barang_masuk_apotek);
                }else{
                    $ins = $this->Barang_keluar_model->insert_barang_keluar($data_barang_keluar);
                    $ins = $this->Barang_keluar_model->insert_barang_masuk_apotek($data_barang_masuk_apotek);
                    $ins = $this->Barang_keluar_model->insert_barang_stok_apotek($data_barang_stok_apotek);
                }
                    if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Tindakan berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "farmasi_barang_keluar_create";
                    $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                    }else{
                        $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                        // if permitted, do logit
                        $perms = "farmasi_barang_keluar_create";
                        $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                        $this->aauth->logit($perms, current_url(), $comments);
                    }
            }
        }
        echo json_encode($res);
    }

    public function do_delete_barang_keluar(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_keluar_delete');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_barang_keluar = $this->input->post('id_barang_keluar', TRUE);

        $delete = $this->Barang_keluar_model->delete_barang_keluar($id_barang_keluar);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_keluar_delete";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $id_barang_keluar ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_keluar_delete";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $id_barang_keluar ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_list_daftar_barang(){
        $list = $this->Barang_keluar_model->get_list_daftar_barang();
        $data = array();
        foreach($list as $daftar_barang){
            $row = array();

            $row[] = $daftar_barang->kode_barang;
            $row[] = $daftar_barang->nama_barang;
            $row[] = $daftar_barang->nama_jenis;
            $row[] = $daftar_barang->nama_sediaan;
            $row[] = $daftar_barang->stok_akhir;
            $row[] = $daftar_barang->kondisi_barang;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Diagnosa" onclick="pilihBarang('."'".$daftar_barang->kode_barang."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Barang_keluar_model->count_list_daftar_barang(),
            "recordsFiltered" => $this->Barang_keluar_model->count_list_filtered_daftar_barang(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_get_daftar_barang_by_id(){
        $kode_barang = $this->input->get('kode_barang',TRUE);
        $data_daftar_barang = $this->Barang_keluar_model->get_daftar_barang_by_id($kode_barang);
        if (sizeof($data_daftar_barang)>0){
            $daftar_barang = $data_daftar_barang[0];
            $res = array(
                "success" => true,
                "messages" => "Data diagnosa ditemukan",
                "data" => $daftar_barang
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data diagnosa tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }


    public function do_tolak_pengajuan(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_keluar_delete');
        if(!$is_permit){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $id_approval = $this->input->post('id_approval_input2', TRUE);
        $alasan_respon = $this->input->post('alasan_tolak', TRUE);

        $data = array(
          'status_approval' => 'Ditolak',
          'tanggal_respons'  => date('Y-m-d H:i:s'),
          'alasan_respon'   => $alasan_respon,
          'jumlah_barang_respon' => 0
        );
        $tolak = $this->Barang_keluar_model->tolak_pengajuan($data, $id_approval);
        if($tolak){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Pengajuan berhasil ditolak'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_keluar_delete";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id  ";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_keluar_delete";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID =";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_get_pengajuan_by_id(){
        $id_approval = $this->input->get('id_approval',TRUE);
        $data_pengajuan = $this->Barang_keluar_model->get_pengajuan_by_id($id_approval);
        // $data_approval ['list_data'] = $this->Barang_keluar_model->get_data_approval($id_approval);
        // $this->load->view('v_barang_keluar', $data_approval);
        if (sizeof($data_pengajuan)>0){
            $pengajuan = $data_pengajuan[0];
            $res = array(
                "success" => true,
                "messages" => "Data diagnosa ditemukan",
                "data" => $pengajuan
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data diagnosa tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }

    public function verifikasi_data_pengajuan($id_approval){
        $data_approval ['list_data'] = $this->Barang_keluar_model->get_data_approval($id_approval);

        $this->load->view('v_barang_keluar', $data_approval);
    }


    public function do_create_pengajuan(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_keluar_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('jumlah_barang_input', 'Jumlah Barang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            //$this->session->set_flashdata('message_type', 'error');
            //$this->session->set_flashdata('messages', $error);
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "farmasi_barang_keluar_create";
            $comments = "Gagal update agama dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE){
            $id_approval = $this->input->post('id_approval_input',TRUE);
            $kode_barang = $this->input->post('kode_barang_input', TRUE);
            $nama_barang = $this->input->post('nama_barang_input', TRUE);
            $jenis_barang = $this->input->post('jenis_barang_input', TRUE);
            $satuan_barang = $this->input->post('satuan_barang_input', TRUE);
            $jumlah_barang = $this->input->post('jumlah_barang_input', TRUE);
            $di_tujukan = $this->input->post('di_tujukan', TRUE);
            $alasan_respon = $this->input->post('alasan_terima', TRUE);
            $expired = $this->input->post('expired_input', TRUE);
            $harga_satuan = $this->input->post('harga_satuan_input', TRUE);
            // $alasan_respon = $this->input->post('alasan_tolak', TRUE);
            $tanggal     = date('Y-m-d H:i:s');


            $data_pengajuan = array(
                'status_approval'          => 'Disetujui',
                'tanggal_respons'          => $tanggal,
                'jumlah_barang_respon'     => $jumlah_barang,
                'alasan_respon'            => $alasan_respon,
                // 'alasan_respon'            => $alasan_tolak
            );

            $data_keluar_farmasi = array(
                'tanggal_keluar'    => $tanggal,
                'kode_barang'       => $kode_barang,
                'nama_barang'       => $nama_barang,
                'id_sediaan'        => $satuan_barang,
                'id_jenis_barang'   => $jenis_barang,
                'jumlah_barang'     => $jumlah_barang,
                'harga_satuan'      => $harga_satuan,
                // 'expired'           => $expired,

                // 'dari_gudang'       => $dari_gudang,
                'di_tujukan'        => 1
                // 'keterangan'        => $keterangan
            );

            if ($di_tujukan == 1){
                $data_barang_masuk_apotek = array(
                    'tanggal_masuk'     => $tanggal,
                    'kode_barang'       => $kode_barang,
                    'nama_barang'       => $nama_barang,
                    'id_sediaan'        => $satuan_barang,
                    'id_jenis_barang'   => $jenis_barang,
                    'jumlah_barang'     => $jumlah_barang,
                    'expired'           => $expired,
                    'harga_satuan'      => $harga_satuan,
                    'expired'           => $expired

                    // 'harga_pack'        => $harga_pack,
                    // 'total_harga'       => $total
                );

                $data_barang_stok_apotek = array(
                    'kode_barang'       => $kode_barang,
                    'nama_barang'       => $nama_barang,
                    'id_sediaan'        => $satuan_barang,
                    'id_jenis_barang'   => $jenis_barang,
                    'stok_masuk'        => $jumlah_barang,
                    'stok_keluar'       => 0,
                    'stok_akhir'        => $jumlah_barang,
                    'perubahan_terakhir'=> $tanggal,
                    'harga_satuan'      => $harga_satuan,
                    'expired'           => $expired

                    // 'harga_pack'        => $harga_pack

                );

                $notif = array(
                    'title'         => $this->data['data_user']->name ,
                    'description'   => 'Barang Baru Telah Masuk',
                    'sender'        => $this->data['data_user']->name,
                    'receiver'      => 'apotik',
                    'date_notif'    =>  date('Y-m-d'),
                    'view'          => 0
                );

                $this->Barang_keluar_model->insert_notif($notif);

            }

            // print_r($data_pengajuan);die();



            //$ins=true;

            $query1 = $this->db->get_where('t_stok_farmasi',"stok_akhir <=' $jumlah_barang'"); //cek stok tersedia
            // $query2 = $this->db->get_where('t_stok_farmasi',"kode_barang' => $kode_barang"); //tampil stok
            // $query2 = $this->db->get_where('t_stok_farmasi',array('kode_barang'=>$kode_barang));//cek kode barang
            $list = $this->Barang_keluar_model->tampil_stok($kode_barang);
            $query = $this->db->get_where('t_apotek_stok',array('kode_barang'=>$kode_barang));//cek kode barang



            // print_r($list);die();

            if ($query1->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Stok barang tidak mencukupi. Stok yang tersedia untuk barang '.$list->nama_barang.' hanya '.$list->stok_akhir);
                    // var_dump($query1);die();
                    // if permitted, do logit
                    // $t = $list->stok_akhir;

                    // print_r($t);die();

                    $perms = "master_data_barang_farmasi_create";
                    $comments = "Gagal menambahkan kode barang dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{


                if($query->num_rows() > 0){
                    $update = $this->Barang_keluar_model->setuju_pengajuan($data_pengajuan, $id_approval);
                    $update = $this->Barang_keluar_model->insert_barang_keluar($data_keluar_farmasi);
                    $update = $this->Barang_keluar_model->insert_barang_masuk_apotek($data_barang_masuk_apotek);
                }else{
                    $update = $this->Barang_keluar_model->setuju_pengajuan($data_pengajuan, $id_approval);
                    $update = $this->Barang_keluar_model->insert_barang_keluar($data_keluar_farmasi);
                    $update = $this->Barang_keluar_model->insert_barang_masuk_apotek($data_barang_masuk_apotek);
                    $update = $this->Barang_keluar_model->insert_barang_stok_apotek($data_barang_stok_apotek);
                }
                if($update){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Barang berhasil ditambah'
                    );

                    // if permitted, do logit
                    $perms = "farmasi_barang_keluar_create";
                    $comments = "Berhasil mengubah agama  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal mengubah agama , hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "farmasi_barang_keluar_create";
                    $comments = "Gagal mengubah agama  dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }

}
