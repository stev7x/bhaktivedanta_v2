<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_stok extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Barang_stok_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_stok_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "farmasi_barang_stok_view";
        $comments = "Barang stok";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_barang_stok', $this->data);
    }

    public function ajax_list_barang_stok(){
        $kode_barang    = $this->input->get('kode_barang',TRUE);
        $nama_barang    = $this->input->get('nama_barang',TRUE);
        $jenis_barang   = $this->input->get('jenis_barang',TRUE);
        $kondisi_barang = $this->input->get('kondisi_barang',TRUE);
        $expired        = $this->input->get('expired',TRUE);
        $list = $this->Barang_stok_model->get_barang_stok_list($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired);
        // $list = $this->Barang_stok_model->get_barang_stok_list();
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $barang_stok){
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = date('d M Y', strtotime($barang_stok->expired));
            $row[] = $barang_stok->kode_barang;
            $row[] = $barang_stok->nama_barang;
            $row[] = $barang_stok->nama_jenis;
            $row[] = $barang_stok->nama_sediaan;
            if($barang_stok->is_bpjs == 1){
                $row[] = "BPJS";
            } else{
                $row[] = "NON BPJS";
            }
            $row[] = $barang_stok->stok_masuk;
            $row[] = $barang_stok->stok_keluar;
            $row[] = $barang_stok->stok_akhir;
            $row[] = $barang_stok->kondisi_barang;
            $row[] = $barang_stok->harga_satuan;
            $row[] = $barang_stok->harga_pack;
            $row[] = $barang_stok->harga_pokok;
            $row[] = $barang_stok->type_call;
            $row[] = $barang_stok->jenis_pasien;
            $row[] = $barang_stok->keterangan;
            $row[] =  date('d-M-Y H:i:s', strtotime($barang_stok->perubahan_terakhir));
            $row[] = '
                     <button class="btn btn-danger btn-circle" titel="klik untuk edit" data-toggle="modal" data-target="#modal_edit" data-backdrop="static" onclick="editBarang('."'".$barang_stok->kode_barang."'".')"><i class="fa fa-pencil-square-o"></i></button>
                     <button class="btn btn-danger btn-circle" titel="klik untuk hapus" onclick="hapusBarang('."'".$barang_stok->kode_barang."'".')"><i class="fa fa-trash"></i></button>

                     ';

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Barang_stok_model->count_barang_stok_all($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired),
                    "recordsFiltered" => $this->Barang_stok_model->count_barang_stok_all($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    public function ajax_list_daftar_barang(){
        $list = $this->Barang_stok_model->get_list_daftar_barang();
        $data = array();
        foreach($list as $daftar_barang){
            $row = array();

            $row[] = $daftar_barang->kode_barang;
            $row[] = $daftar_barang->nama_barang;
            if($daftar_barang->id_jenis_barang == 1){
                $row[] = 'Obat';
            }else if($daftar_barang->id_jenis_barang == 2){
                $row[] = 'Alkes';
            }else if($daftar_barang->id_jenis_barang == 3){
                $row[] = 'BHP';
            }else{
                $row[] = 'Tidak Valid';
            }
            $row[] = $daftar_barang->stok_akhir;
            $row[] = $daftar_barang->kondisi_barang;
            $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Diagnosa" onclick="pilihBarang('."'".$daftar_barang->kode_barang."'".')"><i class="fa fa-check"></i></button>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => $this->Barang_stok_model->count_list_daftar_barang(),
            "recordsFiltered" => $this->Barang_stok_model->count_list_filtered_daftar_barang(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_get_daftar_barang_by_id(){
        $kode_barang = $this->input->get('kode_barang',TRUE);
        $data_daftar_barang = $this->Barang_stok_model->get_daftar_barang_by_id($kode_barang);
        if (sizeof($data_daftar_barang)>0){
            $daftar_barang = $data_daftar_barang[0];
            $res = array(
                "success" => true,
                "messages" => "Data diagnosa ditemukan",
                "data" => $daftar_barang
            );
        } else {
            $res = array(
                "success" => false,
                "messages" => "Data diagnosa tidak ditemukan",
                "data" => null
            );
        }
        echo json_encode($res);
    }
    public function do_create_stok($value='')
    {
        date_default_timezone_set('Asia/Jakarta');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_barang', 'Kode Barang', 'required');
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('stok_masuk', 'Stok Masuk', 'required');
        $this->form_validation->set_rules('stok_keluar', 'Stok Keluar', 'required');
        $this->form_validation->set_rules('stok_akhir', 'Stok Akhir', 'required');
        $this->form_validation->set_rules('id_jenis_barang', 'id_jenis_barang', 'required');
        $this->form_validation->set_rules('id_sediaan', 'id_sediaan', 'required');
        $this->form_validation->set_rules('harga_satuan', 'harga_satuan', 'required');
        $this->form_validation->set_rules('harga_pack', 'harga_pack', 'required');
        $this->form_validation->set_rules('expired', 'expired', 'required');
        // $this->form_validation->set_rules('barang_id', 'barang_id', 'required');
        $this->form_validation->set_rules('type_call', 'type_call', 'required');
        $this->form_validation->set_rules('jenis_pasien', 'jenis_pasien', 'required');
        $this->form_validation->set_rules('harga_pokok', 'harga_pokok', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);


        }else if ($this->form_validation->run() == TRUE)  {

            $asal_obat = $this->input->post('asal_obat',TRUE);
            if (empty($asal_obat)) {
                $asal_obat = 0;
            }

            $data = array(
                
                'kode_barang' => $this->input->post('kode_barang',TRUE),
                'nama_barang' => $this->input->post('nama_barang',TRUE),
                'stok_masuk' => $this->input->post('stok_masuk',TRUE),
                'stok_keluar' => $this->input->post('stok_keluar',TRUE),
                'stok_akhir' => $this->input->post('stok_akhir',TRUE),
                'id_jenis_barang' => $this->input->post('id_jenis_barang',TRUE),
                'id_sediaan' => $this->input->post('id_sediaan',TRUE),
                'harga_satuan' => $this->input->post('harga_satuan',TRUE),
                'harga_pack' => $this->input->post('harga_pack',TRUE),
                'expired' => $this->Barang_stok_model->casting_date_indo(strtolower($this->input->post('expired',TRUE))) ,
                'perubahan_terakhir' => date('Y-m-d H:i:s'),
                'keterangan' => $this->input->post('keterangan',TRUE),
                'barang_id' => '1',
                'type_call' => $this->input->post('type_call',TRUE),           
                'jenis_pasien' => $this->input->post('jenis_pasien',TRUE),
                'harga_pokok' => $this->input->post('harga_pokok',TRUE),       
                'is_extend'    => $asal_obat
            );

            $ins = $this->Barang_stok_model->insert_stok_barang($data);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Barang berhasil ditambahkan'
                );

   
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Laboratorium, hubungi web administrator.');

               
            }
        }
        echo json_encode($res);
    }
    public function do_update_stok($value='')
    {
        date_default_timezone_set('Asia/Jakarta');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_barang', 'Kode Barang', 'required');
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('stok_masuk', 'Stok Masuk', 'required');
        $this->form_validation->set_rules('stok_keluar', 'Stok Keluar', 'required');
        $this->form_validation->set_rules('stok_akhir', 'Stok Akhir', 'required');
        $this->form_validation->set_rules('id_jenis_barang', 'id_jenis_barang', 'required');
        $this->form_validation->set_rules('id_sediaan', 'id_sediaan', 'required');
        $this->form_validation->set_rules('harga_satuan', 'harga_satuan', 'required');
        $this->form_validation->set_rules('harga_pack', 'harga_pack', 'required');
        $this->form_validation->set_rules('expired', 'expired', 'required');
        // $this->form_validation->set_rules('barang_id', 'barang_id', 'required');
        $this->form_validation->set_rules('type_call', 'type_call', 'required');
        $this->form_validation->set_rules('jenis_pasien', 'jenis_pasien', 'required');
        $this->form_validation->set_rules('harga_pokok', 'harga_pokok', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);


        }else if ($this->form_validation->run() == TRUE)  {

            $data = array(
                'nama_barang' => $this->input->post('nama_barang',TRUE),
                'stok_masuk' => $this->input->post('stok_masuk',TRUE),
                'stok_keluar' => $this->input->post('stok_keluar',TRUE),
                'stok_akhir' => $this->input->post('stok_akhir',TRUE),
                'id_jenis_barang' => $this->input->post('id_jenis_barang',TRUE),
                'id_sediaan' => $this->input->post('id_sediaan',TRUE),
                'harga_satuan' => $this->input->post('harga_satuan',TRUE),
                'harga_pack' => $this->input->post('harga_pack',TRUE),
                'expired' => $this->Barang_stok_model->casting_date_indo(strtolower($this->input->post('expired',TRUE))) ,
                'perubahan_terakhir' => date('Y-m-d H:i:s'),
                'keterangan' => $this->input->post('keterangan',TRUE),
                'barang_id' => '1',
                'type_call' => $this->input->post('type_call',TRUE),           
                'jenis_pasien' => $this->input->post('jenis_pasien',TRUE),
                'harga_pokok' => $this->input->post('harga_pokok',TRUE),           
            );
            $id = $this->input->post('kode_barang',TRUE);
            $ins = $this->Barang_stok_model->update_stok_barang($data,$id);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Barang diupdate'
                );

   
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal Edit, hubungi web administrator.');

               
            }
        }
        echo json_encode($res);
    }
    public function ajax_get_barang_id(){
        $barang_stok = $this->input->get('id',TRUE);
        $data = $this->Barang_stok_model->get_barang_by_id($barang_stok);

        if(count($data) > 0) {
            $res = array(
                'success' => true,
                'messages' => "Data found",
                'data' => $data
                );
        } else {
            $res = array(
                'success' => false,
                'messages' => "Barang tidak ditemukan");
        }

        echo json_encode($res);
    }

    public function do_delete_barang_stok(){
        $is_permit = $this->aauth->control_no_redirect('farmasi_barang_stok_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $kode_barang = $this->input->post('kode_barang', TRUE);

        $delete = $this->Barang_stok_model->delete_barang_stok($kode_barang);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Barang Stok berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_stok_view";
            $comments = "Berhasil menghapus Barang Stok dengan kode = '". $kode_barang ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "farmasi_barang_stok_view";
            $comments = "Gagal menghapus data Barang stok dengan Kode = '". $kode_barang ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

}
