<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_floor_stok_keluar extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }
        $this->load->model('Menu_model');
        $this->load->model('Barang_floor_stok_keluar_model');
        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('barang_keluar_floor_stok_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }


        // if permitted, do logit
        $perms = "barang_keluar_floor_stok_view";
        $comments = "floor stok masuk";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_barang_floor_stok_keluar', $this->data);
    }

    function convert_to_rupiah($angka){
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }


    public function ajax_list_floor_stok_keluar(){
        // $tgl_awal        = $this->input->get('tgl_awal',TRUE);
        // $tgl_akhir       = $this->input->get('tgl_akhir',TRUE);
        // $kode_barang     = $this->input->get('kode_barang',TRUE);
        // $nama_barang     = $this->input->get('nama_barang',TRUE);
        // $jenis_barang = $this->input->get('jenis_barang',TRUE);
        // $nama_supplier   = $this->input->get('nama_supplier',TRUE);
        // $list = $this->barang_keluar_floor_stok_model->get_barang_keluar_list($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier);
        $list = $this->barang_keluar_floor_stok_model->get_floor_stok_keluar_list();
        // $list   = $this->barang_keluar_floor_stok_model->get_barang_keluar_list();
        $data   = array();
        $no     = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $floor_stok_keluar){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d-M-Y H:i:s', strtotime($floor_stok_keluar->tanggal_masuk));
            $row[] = $floor_stok_keluar->kode_barang;
            $row[] = $floor_stok_keluar->nama_barang;
            $row[] = $floor_stok_keluar->satuan;
            $row[] = $floor_stok_keluar->jenis_barang;
            $row[] = $floor_stok_keluar->jumlah_barang;
            $row[] = $this->convert_to_rupiah($floor_stok_keluar->harga_satuan).",00";
            $row[] = $this->convert_to_rupiah($floor_stok_keluar->harga_pack).",00";
            $row[] = $this->convert_to_rupiah($floor_stok_keluar->total_harga).",00";
            $row[] = $floor_stok_keluar->nama_supplier;
            $row[] = date('d-M-Y', strtotime($floor_stok_keluar->exp));
            $row[] = $floor_stok_keluar->keterangan;
            $row[] = '<button class="btn btn-danger btn-circle" onclick="hapusBarangMasuk('."'".$floor_stok_keluar->id_barang_keluar."'".')"><i class="fa fa-trash"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw"            => $this->input->get('draw'),
                    // "recordsTotal"    => $this->barang_keluar_floor_stok_model->count_floor_stok_keluar_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier),
                    "recordsTotal"    => $this->Barang_floor_stok_keluar_model->count_floor_stok_keluar_all(),
                    "recordsFiltered" => $this->Barang_floor_stok_keluar_model->count_floor_stok_keluar_all(),
                    // "recordsFiltered" => $this->barang_keluar_floor_stok_model->count_floor_stok_keluar_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $nama_supplier),
                    "data"            => $data,
                    );
        //output to json format
        echo json_encode($output);

    }

    // function convert_to_number($rupiah){
    //     return preg_replace("/\.*([^0-9\\.])/i", "", $rupiah);
    //  }

    // public function do_create_barang_keluar(){
    //     $is_permit = $this->aauth->control_no_redirect('farmasi_barang_keluar_view');
    //     if(!$is_permit) {
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash'      => $this->security->get_csrf_hash(),
    //             'success'       => false,
    //             'messages'      => $this->lang->line('aauth_error_no_access'));
    //         echo json_encode($res);
    //         exit;
    //     }
    //     // form validate
    //     $this->load->library('form_validation');
    //     $this->form_validation->set_rules('kode_barang','Kode Barang', 'required|trim');
    //     $this->form_validation->set_rules('nama_barang','Nama Barang', 'required|trim');
    //     $this->form_validation->set_rules('kode_sediaan','Satuan', 'required|trim');
    //     $this->form_validation->set_rules('jenis_barang','Jenis Barang', 'required|trim');
    //     $this->form_validation->set_rules('jenis_transaksi','Jenis transaksi', 'required|trim');
    //     $this->form_validation->set_rules('kode_supplier','Nama Supplier', 'required|trim');
    //     $this->form_validation->set_rules('harga_barang_keseluruhan','Harga Barang Keseluruhan', 'required|trim');
    //     $this->form_validation->set_rules('jumlah_barang','Jumlah Barang Keseluruhan', 'required|trim');


    //     if ($this->form_validation->run() == FALSE)  {
    //         $error = validation_errors();
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash'      => $this->security->get_csrf_hash(),
    //             'success'       => false,
    //             'messages'      => $error);

    //         // if permitted, do logit
    //         $perms = "farmasi_barang_keluar_view";
    //         $comments = "Gagal input Barang Masuk pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
    //         $this->aauth->logit($perms, current_url(), $comments);

    //     }else if ($this->form_validation->run() == TRUE)  {
    //         $tanggal_masuk   = date('Y-m-d H:i:s');
    //         // $tanggal_masuk1  = date_format($tanggal_masuk, 'Y-m-d');

    //         $kode_barang     = $this->input->post('kode_barang', TRUE);
    //         $nama_barang     = $this->input->post('nama_barang', TRUE);
    //         $kode_sediaan    = $this->input->post('kode_sediaan', TRUE);
    //         $jenis_barang    = $this->input->post('jenis_barang', TRUE);
    //         $jumlah_barang   = $this->input->post('jumlah_barang', TRUE);
    //         $harga_satuan    = $this->input->post('harga_barang_per_item', TRUE);
    //         $harga_satuan    = $this->input->post('harga_barang_per_itema', TRUE);
    //         $harga_pack      = $this->input->post('harga_barang_per_pack', TRUE);
    //         $total_harga     = $this->input->post('harga_barang_keseluruhan', TRUE);
    //         $kode_supplier   = $this->input->post('kode_supplier', TRUE);
    //         $exp             = date_create($this->input->post('exp', TRUE));
    //         $exp1            = date_format($exp, 'Y-m-d');
    //         $keterangan      = $this->input->post('keterangan', TRUE);

    //         $harga_satuan_int = $this->convert_to_number($harga_satuan);
    //         $harga_pack_int   = $this->convert_to_number($harga_pack);
    //         // $total_harga_int = $this->convert_to_number($total_harga);


    //         $data_barang_keluar = array(


    //             'tanggal_masuk'     => $tanggal_masuk,
    //             'kode_barang'       => $kode_barang,
    //             'nama_barang'       => $nama_barang,
    //             'satuan'            => $kode_sediaan,
    //             'jenis_barang'      => $jenis_barang,
    //             'jumlah_barang'     => $jumlah_barang,
    //             'harga_satuan'      => $harga_satuan_int,
    //             'harga_pack'        => $harga_pack_int,
    //             'total_harga'       => $total_harga,
    //             'nama_supplier'     => $kode_supplier,
    //             'exp'               => $exp1,
    //             'keterangan'        => $keterangan
    //         );

    //         $ins = $this->barang_keluar_floor_stok_model->insert_barang_keluar($data_barang_keluar);
    //         //$ins=true;
    //         if($ins){
    //             $res = array(
    //                 'csrfTokenName' => $this->security->get_csrf_token_name(),
    //                 'csrfHash'      => $this->security->get_csrf_hash(),
    //                 'success'       => true,
    //                 'messages'      => 'Barang Masuk berhasil ditambahkan'
    //             );

    //             // if permitted, do logit
    //             $perms       = "farmasi_barang_keluar_view";
    //             $comments    = "Berhasil menambahkan Barang Masuk dengan data berikut = '". json_encode($_REQUEST) ."'.";
    //             $this->aauth->logit($perms, current_url(), $comments);
    //         }else{
    //             $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash'      => $this->security->get_csrf_hash(),
    //             'success'       => false,
    //             'messages'      => 'Gagal menambahkan Barang Masuk, hubungi web administrator.');

    //             // if permitted, do logit
    //             $perms = "farmasi_barang_keluar_view";
    //             $comments = "Gagal menambahkan Barang Masuk dengan data berikut = '". json_encode($_REQUEST) ."'.";
    //             $this->aauth->logit($perms, current_url(), $comments);
    //         }
    //     }
    //     echo json_encode($res);
    // }

    // public function do_delete_barang_keluar(){
    //     $is_permit = $this->aauth->control_no_redirect('farmasi_barang_keluar_delete');
    //     if(!$is_permit) {
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash'      => $this->security->get_csrf_hash(),
    //             'success'       => false,
    //             'messages'      => $this->lang->line('aauth_error_no_access'));
    //         echo json_encode($res);
    //         exit;
    //     }

    //     $id_barang_keluar = $this->input->post('id_barang_keluar', TRUE);

    //     $delete = $this->barang_keluar_floor_stok_model->delete_barang_keluar($id_barang_keluar);
    //     if($delete){
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash'      => $this->security->get_csrf_hash(),
    //             'success'       => true,
    //             'messages'      => 'Barang Masuk Pasien berhasil di batalkan'
    //         );
    //         // if permitted, do logit
    //         $perms = "farmasi_barang_keluar_delete";
    //         $comments = "Berhasil membatalkan Barang Masuk Pasien dengan id Barang Masuk Pasien Operasi = '". $id_barang_keluar ."'.";
    //         $this->aauth->logit($perms, current_url(), $comments);
    //     }else{
    //         $res = array(
    //             'csrfTokenName' => $this->security->get_csrf_token_name(),
    //             'csrfHash'      => $this->security->get_csrf_hash(),
    //             'success'       => false,
    //             'messages'      => 'Gagal membatalkan data, silakan hubungi web administrator.'
    //         );
    //         // if permitted, do logit
    //         $perms = "farmasi_barang_keluar_delete";
    //         $comments = "Gagal membatalkan data Barang Masuk Pasien dengan ID = '". $id_barang_keluar ."'.";
    //         $this->aauth->logit($perms, current_url(), $comments);
    //     }
    //     echo json_encode($res);
    // }


    // public function ajax_list_daftar_barang(){
    //     $list = $this->barang_keluar_floor_stok_model->get_list_daftar_barang();
    //     $data = array();
    //     foreach($list as $daftar_barang){
    //         $row   = array();
    //         $row[] = $daftar_barang->kode_barang;
    //         $row[] = $daftar_barang->nama_barang;
    //         $row[] = $daftar_barang->jenis_barang;
    //         $row[] = $daftar_barang->stok_akhir;
    //         $row[] = $daftar_barang->kondisi_barang;
    //         $row[] = '<button class="btn btn-info btn-circle" title="Klik Untuk Pilih Barang Masuk" onclick="pilihBarang('."'".$daftar_barang->kode_barang."'".')"><i class="fa fa-check"></i></button>';

    //         $data[] = $row;
    //     }

    //     $output = array(
    //         "draw"              => $this->input->get('draw'),
    //         "recordsTotal"      => $this->barang_keluar_floor_stok_model->count_list_daftar_barang(),
    //         "recordsFiltered"   => $this->barang_keluar_floor_stok_model->count_list_filtered_daftar_barang(),
    //         "data"              => $data,
    //     );
    //     //output to json format
    //     echo json_encode($output);
    // }


    // public function ajax_get_daftar_barang_by_id(){
    //     $kode_barang = $this->input->get('kode_barang',TRUE);
    //     $data_daftar_barang = $this->barang_keluar_floor_stok_model->get_daftar_barang_by_id($kode_barang);
    //     if (sizeof($data_daftar_barang)>0){
    //         $daftar_barang = $data_daftar_barang[0];
    //         $res = array(
    //             "success" => true,
    //             "messages" => "Data Barang Masuk ditemukan",
    //             "data" => $daftar_barang
    //         );
    //     } else {
    //         $res = array(
    //             "success" => false,
    //             "messages" => "Data Barang Masuk tidak ditemukan",
    //             "data" => null
    //         );
    //     }
    //     echo json_encode($res);
    // }

}
