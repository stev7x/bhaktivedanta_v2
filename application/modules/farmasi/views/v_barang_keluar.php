<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Inventori  -  Barang </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Inventori</a></li>
                            <li class="active">Barang</li> 
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">  
                            <div class="panel-heading"> Data Barang
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">  
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="inputName1" class="control-label"></label>
                                                <dl class="text-right">  
                                                    <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                    <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                                </dl> 
                                        </div> 
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label>Tanggal Transaksi, Dari</label>
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                     </div>
                                                </div>
                                                <div class="col-md-5">  
                                                    <label>Sampai</label>    
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                     </div>
                                                </div>
                                                <div class="col-md-2"> 
                                                    <label>&nbsp;</label><br>
                                                    <button type="button" class="btn btn-success col-md-12" onclick="cariBarangKeluar()">Cari</button>        
                                                </div> 
                                                <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="kode_barang">Kode Barang</option>
                                                        <option value="nama_barang">Nama Barang</option>
                                                        <option value="jenis_barang">Jenis Barang</option>
                                                        <option value="dari_gudang">Asal Gudang</option>   
                                                        <option value="di_tujukan">Tujuan Distribusi</option>   
                                                        <!-- <option value="status">Status</option>    -->
                                                    </select></b>
                                                         
                                                </div>    
                                                <div class="form-group col-md-9" style="margin-top: 7px">   
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                    <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariBarangKeluar()" >       
                                                </div> 
                                            </div>
                                        </div> 
                                    </div><br><hr style="margin-top: -27px">
                                   <div class="col-md-12" style="margin-top: -10px">
                                        <ul class="nav customtab nav-tabs" role="tablist">
                                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#barang_keluar" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Barang Masuk</span></a></li>
                                            <li role="presentation" class="nav-item" style="width: 100%;text-align: center;"><a href="#barang_approval" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Pengajuan Barang</span></a></li>  
                                        </ul>
                                        <div class="tab-content"> 
                                            <div role="tabpanel" class="tab-pane fade active in" id="barang_keluar">
                                                <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_keluar_barang" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus p-r-10"></i>Tambah Barang keluar</button>
                                                </div>
                                                <div class="table">   
                                                    <table id="table_barang_keluar_list" class="table table-responsive table-striped dataTable" cellspacing="0">
                                                        <thead>  
                                                            <tr> 
                                                                <th>No</th>
                                                                <th>Tanggal Transaksi</th>
                                                                <th>Kode Barang</th>
                                                                <th>Nama Barang</th>
                                                                <th>Jenis Barang</th>
                                                                <th>Satuan</th>
                                                                <th>Jumlah Barang</th>
                                                                <th>Gudang</th>
                                                                <th>Tujuan</th>
                                                                <th>Keterangan</th>
                                                                <th>Aksi</th>
                                                            </tr>  
                                                        </thead>
                                                        <tbody> 
                                                            <tr>   
                                                                <td colspan="4">No Data to Display</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>  
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="barang_approval">
                                                <div class="table">   
                                                    <table id="table_barang_pengajuan_list" class="table table-responsive table-striped dataTable" cellspacing="0">
                                                        <thead>  
                                                            <tr> 
                                                                <th>No</th>
                                                                <th>Tanggal Pengajuan</th>
                                                                <th>Kode Barang</th>
                                                                <th>Nama Barang</th>
                                                                <th>Jumlah Barang</th>
                                                                <th>Tanggal Respon</th>
                                                                <th>Status</th>
                                                                <th>Aksi</th>
                                                            </tr>  
                                                        </thead>
                                                        <tbody> 
                                                            <tr>   
                                                                <td colspan="4">No Data to Display</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->

<div id="modal_add_keluar_barang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-large" >    
    <?php echo form_open('#',array('id' => 'fmCreateBarangKeluar'))?>
    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />
     
        <div class="modal-content" style="overflow:auto;">   
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Tambah Barang keluar</b></h2>     
             </div>  
            <div class="modal-body">
                <div class="row">   
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                                <div>
                                    <p id="card_message" class=""></p>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-6">     
                        <div class="form-group">
                            <label class="control-label">
                                Tanggal Keluar Barang<span style="color: red;">*</span>
                            </label>
                            <div class="input-group">
                                <input onkeydown="return false" name="tanggal_keluar" id="tanggal_keluar" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy" disabled> <span class="input-group-addon"><i class="icon-calender" ></i></span>    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Kode Barang<span style="color: red;">*</span>
                            </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_barang" name="kode_barang" placeholder="Kode Barang" readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-info" type="button" title="Lihat List Kode Barang" data-toggle="modal" data-target="#modal_list_barang" data-backdrop="static" data-keyboard="false" onclick="dialogBarang()"><i class="fa fa-list"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Barang<span style="color: red;">*</span></label>
                            <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama Barang" readonly>
                        </div>
                        <div class="form-group">
                            <label>Satuan<span style="color: red;">*</span></label>
                                <select class="form-control" name="kode_sediaan" id="kode_sediaan" readonly>
                                <option disabled selected>Pilih Satuan</option>
                                <?php
                                    $list_sediaan = $this->Barang_keluar_model->get_sediaan_list();
                                    foreach($list_sediaan as $list){
                                        echo "<option value=".$list->id_sediaan." >".$list->nama_sediaan."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenis Barang<span style="color: red;">*</span></label>
                            <select class="form-control" name="jenis_barang" id="jenis_barang" readonly>
                            <option value=""  selected>Pilih Jenis Barang</option>
                            <?php
                                $list_jenis = $this->Barang_keluar_model->get_jenis_list();
                                foreach ($list_jenis as $list) {
                                    echo "<option value=".$list->id_jenis_barang.">".$list->nama_jenis."</option>";
                                } 
                            ?>
                            </select>
                        </div> 
                        <div class="form-group">
                                <label>Jumlah Barang Keseluruhan<span style="color: red;">*</span></label>
                                <input type="number" name="jumlah_barang" id="jumlah_barang_keseluruhan" class="form-control" placeholder="Jumlah Barang" min="1" readonly>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Jenis Transaksi Barang<span style="color: red;">*</span></label>
                            <select name="jenis_transaksi" id="jenis_transaksi" class="form-control" onchange="pilih()">
                                    <option value="" disabled selected >Pilih</option>
                                    <option value="1">Per Pack</option>
                                    <option value="2">Per Pieces</option>
                            </select>
                        </div>
                        <div class="per_pack" style="display:none;">
                            <div class="form-group">
                                <label>Jumlah Barang Per Pack<span style="color: red;">*</span></label>
                                <input type="number" name="" id="jumlah_barang_per_pack" class="form-control" placeholder="Jumlah Barang" min="1">
                            </div>
                            <div class="form-group">
                                <label>Jumlah Pack<span style="color: red;">*</span></label>
                                <input type="number" name="" id="jumlah_pack" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="perhitunganPack()">
                            </div>
                            
                        </div>
                        <div class="per_pieces" style="display:none;">
                            <div class="form-group">
                                <label>Jumlah Barang Per Pieces<span style="color: red;">*</span></label>
                                <input type="number" name="" id="jumlah_barang_per_pieces" class="form-control" placeholder="Jumlah Barang" min="1"  onkeyup="perhitunganPieces()">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Asal Gudang<span style="color: red;">*</span></label>
                            <select name="dari_gudang" id="dari_gudang" class="form-control select2">
                            <option value=""  >Pilih Asal Gudang</option>
                            <option value="1"selected>Farmasi</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tujuan Barang<span style="color: red;">*</span></label>
                            <select name="di_tujukan" id="di_tujukan" class="form-control select2">
                            <option value="" >Pilih Tujuan</option>
                            <option value="1" selected>Apotek</option>
                            <option value="2">Floor Stok</option>
                            </select>
                        </div>
                    
                        
                        
                        <div class="form-group">
                            <label>Keterangan</label>  
                            <textarea class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-success pull-right" id="saveBarangKeluar"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>  
                    </div>
                    <input type="hidden" name="expired" id="expired" value="">
                    <input type="hidden" name="harga_satuan" id="harga_satuan" value="">
                    <input type="hidden" name="harga_pack" id="harga_pack" value="">
                </div>  
            </div>
        </div>
        <?php echo form_close(); ?>

        <!-- /.modal-content -->

    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

 


<div id="modal_list_barang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-large">         
        <div class="modal-content" style="margin-top:100px; margin-right:6%; margin-left:6%;">   
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Kode Barang</b></h2>     
             </div>  
            <div class="modal-body">
                <div class="row">     
                <div class="table-responsive">   
                <table id="table_daftar_barang_list" class="table table-striped dataTable" cellspacing="0">
                         <thead>  
                             <tr> 
                                 <th>Kode Barang</th>
                                 <th>Nama Barang</th>
                                 <th>Jenis Barang</th>
                                 <th>Satuan Barang</th>
                                 <th>Stok Akhir</th>
                                 <th>Kondisi Barang</th>
                                 <th>Pilih</th>
                             </tr>  
                         </thead>
                         <tbody> 
                             <tr>   
                                 <td colspan="4">No Data to Display</td>
                             </tr>
                         </tbody>
                     </table>  
                 </div>
                    
                </div>  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_approval" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-lg">         
        <div class="modal-content" style="margin-top:20%;">   
            <div class="modal-header" style="background: #01c0c8;"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel" style="color:white;"><b>Verifikasi Data Pengajuan</b></h2>     
             </div>  
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row"> 
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                                <div>
                                    <p id="card_message1" class=""></p>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-12">
                                <div class="card" style="width:100%;text-align:center;">
                                    <div class="card-block">
                                        <h4 class="card-title">Data Pengajuan</h4>
                                        <table align="center" border="0px" width="70%">
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Kode Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="kode_barang_lbl"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Nama Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="nama_barang_lbl">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Jumlah Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="jumlah_barang_lbl">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl" valign="top">
                                                    <label for=""></label>Catatan Pengajuan
                                                </td>
                                                <td valign="top">:</td>
                                                <td id="val">
                                                    <label id="alasan_pengajuan_lbl">
                                                </td>
                                            </tr>
                                        </table>
                                        <br>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select class="form-control" id="pilih_aksi" onchange="pilihAksi()">
                                                        <option value="" selected>Pilih Aksi</option>
                                                        <option value="1">Terima</option>
                                                        <option value="2">Tolak</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12" id="terima" style="display:none;">
                                                        <?php echo form_open('#',array('id' => 'fmCreateBarangKeluar'))?>
                                                        <label>Masukan Jumlah Barang Yang Disetujui </label>
                                                        <input class="form-control" type="text" name="jumlah_barang_input" id="jumlah_barang_input">                                        
                                                    
                                                        <label>Masukan Catatan</label>
                                                        <textarea name="alasan_terima" id="alasan_terima" rows="4" cols="" class="form-control"  style="max-height:none !important"></textarea>
                                                        <br>
                                                        <input type="hidden" name="id_approval_input" id="id_approval_input">
                                                        <input type="hidden" name="kode_barang_input" id="kode_barang_input">
                                                        <input type="hidden" name="nama_barang_input" id="nama_barang_input">
                                                        <input type="hidden" name="satuan_barang_input" id="satuan_barang_input">
                                                        <input type="hidden" name="jenis_barang_input" id="jenis_barang_input">
                                                        <input type="hidden" name="expired_input" id="expired_input">
                                                        <input type="hidden" name="harga_satuan_input" id="harga_satuan_input">
                                                        <input type="hidden" name="di_tujukan" id="di_tujukan" value="1">

                                                        <button type="button" class="btn btn-info" id="savePengajuanBarang">SUBMIT</button>
                                                    <?php echo form_close(); ?>
                                                </div>
                                                <div class="col-md-12" id="tolak" style="display:none;">
                                                    <?php echo form_open('#',array('id' => 'fmCreateBarangKeluar'))?>
                                                        <label>Alasan Penolakan </label>    
                                                        <textarea name="alasan_tolak" id="alasan_tolak" rows="4" cols="" class="form-control" style="max-height:none !important" ></textarea>
                                                    <br> 
                                                        <input type="hidden" name="id_approval_input2" id="id_approval_input2">
                                                        <button type="button" class="btn btn-info" id="tolakPengajuanBarang">SUBMIT</button>
                                                    <?php echo form_close(); ?>
                                                </div>    
                                            </div>
                                        </div>

                                        

                                                                             
                                    </div>
                                </div>
                                <div class="col-md-3">
                                
                                </div>
                            </center>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_detail_approval" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-lg">         
        <div class="modal-content" style="margin-top:20%;">   
            <div class="modal-header" style="background: #01c0c8;"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel" style="color:white;"><b>Detail Data Pengajuan</b></h2>     
             </div>  
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row"> 
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                                <div>
                                    <p id="card_message1" class=""></p>
                                </div>
                            </div>
                        </div> 
                            <div class="col-md-12">
                                <div class="card" style="width:100%;text-align:center;">
                                    <div class="card-block">
                                        <h4 class="card-title">Data Pengajuan</h4>
                                        <table align="center" border="0px" width="70%">
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Kode Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_kode_barang_lbl"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Nama Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_nama_barang_lbl">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Jumlah Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_jumlah_barang_lbl">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Catatan Pengajuan
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_alasan_pengajuan_lbl">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"><br><br></div>
                            <div class="col-md-12">
                                <div class="card" style="width:100%;text-align:center;">
                                    <div class="card-block">
                                        <h4 class="card-title">Data Respon</h4>
                                        <table align="center" border="0px" width="70%">
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Kode Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_kode_barang_lbl2"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Nama Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_nama_barang_lbl2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Jumlah Barang
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_jumlah_barang_lbl2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl">
                                                    <label for=""></label>Status Pengajuan
                                                </td>
                                                <td>:</td>
                                                <td id="val">
                                                    <label id="det_status_lbl"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="lbl" valign="top">
                                                    <label for=""></label>Catatan Respon
                                                </td>
                                                <td valign="top">:</td>
                                                <td id="val">
                                                    <label id="det_alasan_respon_lbl"></label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<style>
    #lbl{
        width:50%;
        text-align:left;
        font-weight:bold;
    }
    #val{
        width:50%;
        padding-left:10px;
        text-align:left;
        vertical-align:text-bottom;
    }
</style>


<?php $this->load->view('footer');?>
      