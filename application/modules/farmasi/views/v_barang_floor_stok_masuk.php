<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Inventori  -  Barang Masuk Floor Stok</h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Inventori</a></li>
                            <li class="active">Barang Masuk Floor Stok </li> 
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">  
                            <div class="panel-heading"> Data Barang Masuk Floor Stok
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">  
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="inputName1" class="control-label"></label>
                                                <dl class="text-right">  
                                                    <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                    <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>  
                                                </dl> 
                                        </div> 
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label>Tanggal Transaksi, Dari</label>
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                                                     </div>
                                                </div>
                                                <div class="col-md-5">  
                                                    <label>Sampai</label>    
                                                    <div class="input-group">   
                                                        <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>  
                                                     </div>
                                                </div>
                                                <div class="col-md-2"> 
                                                    <label>&nbsp;</label><br>
                                                    <button type="button" class="btn btn-success col-md-12" onclick="cariBarangMasuk()">Cari</button>        
                                                </div> 
                                                <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="kode_barang">Kode Barang</option>
                                                        <option value="nama_barang">Nama Barang</option>
                                                        <option value="jenis_barang">Jenis Barang</option>
                                                        <option value="nama_supplier">Supplier</option>   
                                                        <!-- <option value="status">Status</option>    -->
                                                    </select></b>
                                                         
                                                </div>    
                                                <div class="form-group col-md-9" style="margin-top: 7px">   
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                    <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariBarangMasuk()" >       
                                                </div> 
                                            </div>
                                        </div> 
                                    </div><br><hr style="margin-top: -27px">
 <div class="row" style="padding-bottom: 30px;margin-top: -10px">

                                     <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add_masuk_barang" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus p-r-10"></i>Tambah Pengajuan Barang</button>
                                            </div>
                                    
                                    <div class="table">   
                                        <table id="table_floor_stok_masuk_list" class="table table-responsive table-striped dataTable" cellspacing="0">
                                            <thead>  
                                                <tr> 
                                                    <th>No</th>
                                                    <th>Tanggal Transaksi</th>
                                                    <th>Kode Barang</th>
                                                    <th>Nama Barang</th>
                                                    <th>Satuan</th>
                                                    <th>Jenis Barang</th>
                                                    <th>Jumlah Barang</th>
                                                    <th>Harga Satuan</th>
                                                    <th>Harga Pack</th>
                                                    <th>Total Harga</th>
                                                    <th>Supplier</th>
                                                    <th>Expired</th>
                                                    <th>Keterangan</th>
                                                    <th>Aksi</th>
                                                </tr>  
                                            </thead>
                                            <tbody> 
                                                <tr>   
                                                    <td colspan="4">No Data to Display</td>
                                                </tr>
                                            </tbody>
                                        </table>  
                                    </div>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->

<div id="modal_add_masuk_barang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-large">
    <?php echo form_open('#',array('id' => 'fmCreateBarangMasuk'))?>
    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

        <div class="modal-content" style="overflow:auto;height:630px;">   
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Tambah Barang Masuk</b></h2>     
             </div>  
            <div class="modal-body">
            
                <div class="row">   
                <div class="col-md-12">
                <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div>
                    <p id="card_message" class=""></p>
                </div>
            </div>
            </div>
                    <div class="col-md-6">     
                        <div class="form-group">
                            <label class="control-label">
                                Tanggal Masuk Barang<span style="color: red;">*</span>
                            </label>
                            <div class="input-group">
                                <input onkeydown="return false" name="tanggal_masuk" id="tanggal_masuk" type="text" value="<?php echo date('d F Y'); ?>" disabled class="form-control mydatepicker" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Kode Barang<span style="color: red;">*</span>
                            </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_barang" name="kode_barang" placeholder="Kode Barang" readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-info" type="button" title="Lihat List Kode Barang" data-toggle="modal" data-target="#modal_list_barang" data-backdrop="static" data-keyboard="false" onclick="dialogBarang()"><i class="fa fa-list"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Barang<span style="color: red;">*</span></label>
                            <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama Barang" readonly>
                        </div>
                        <div class="form-group">
                            <label>Satuan<span style="color: red;">*</span></label>
                            <select class="form-control" name="kode_sediaan" id="kode_sediaan" readonly >
                                <option disabled selected>Pilih Satuan</option>
                                <?php
                                    // $list_sediaan = $this->Barang_masuk_model->get_sediaan_list();
                                    // foreach($list_sediaan as $list){
                                    //     echo "<option value=".$list->nama_sediaan.">".$list->nama_sediaan."</option>";
                                    // }
                                ?>
                            </select>
                        </div>
                        <div class="hitung">
                            <div class="form-group">
                                <label>Jumlah Barang Keseluruhan<span style="color: red;">*</span></label>
                                <input type="number" name="jumlah_barang" id="jumlah_barang_keseluruhan" class="form-control" placeholder="Jumlah Barang" min="1" readonly>
                            </div>
                            <div class="form-group">
                                <label>Harga Barang Keseluruhan<span style="color: red;">*</span></label>
                                <input type="text" onkeyup="formatAsRupiah(this)"  name="harga_barang_keseluruhan" id="harga_barang_keseluruhan" class="form-control" placeholder="Harga Barang" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Jenis Barang<span style="color: red;">*</span></label>
                            <select class="form-control" name="jenis_barang" id="jenis_barang" readonly >
                               <option value="" disabled selected>Pilih Jenis Barang</option>
                               <option value="OBAT" >OBAT</option>
                               <option value="BHP" >BHP</option>
                               <option value="ALKES" >ALKES</option>
                            </select>
                        </div> 
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Jenis Transaksi Barang<span style="color: red;">*</span></label>
                            <select name="jenis_transaksi" id="jenis_transaksi" class="form-control" onchange="pilih()">
                                    <option value="" disabled selected >Pilih</option>
                                    <option value="1">Per Pack</option>
                                    <option value="2">Per Pieces</option>
                            </select>
                        </div>
                        <div class="per_pack" style="display:none;">
                            <div class="form-group">
                                <label>Jumlah Barang Per Pack<span style="color: red;">*</span></label>
                                <input type="number" name="jumlah_barang_per_pack" id="jumlah_barang_per_pack" class="form-control" placeholder="Jumlah Barang" min="1">
                            </div>
                            <div class="form-group">
                                <label>Jumlah Pack<span style="color: red;">*</span></label>
                                <input type="number" name="jumlah_pack" id="jumlah_pack" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="perhitunganPack()">
                            </div>
                            
                            <div class="form-group">
                                <label>Harga Barang Per Item<span style="color: red;">*</span></label>
                                <input type="text" name="harga_barang_per_item" id="harga_barang_per_item" class="form-control" placeholder="Harga Barang"   onkeyup="perhitunganHarga(this)">
                            </div>
                            <div class="form-group">
                                <label>Harga Barang Per Pack<span style="color: red;">*</span></label>
                                <input type="text" onkeyup="formatAsRupiah(this)" name="harga_barang_per_pack" id="harga_barang_per_pack" class="form-control" placeholder="Harga Barang" >
                            </div>
                        </div>
                        <div class="per_pieces" style="display:none;">
                            <div class="form-group">
                                <label>Jumlah Barang Per Pieces<span style="color: red;">*</span></label>
                                <input type="number" name="jumlah_barang_per_pieces" onkeyup="perhitunganPieces()" id="jumlah_barang_per_pieces" class="form-control" placeholder="Jumlah Barang" min="1">
                            </div>
                            <div class="form-group">
                                 <label>Harga Barang Per Item<span style="color: red;">*</span></label>
                                <input type="text" name="harga_barang_per_itema" id="harga_barang_per_itema" class="form-control" placeholder="Harga Barang"   onkeyup="perhitunganHargaa(this)">
                             </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Tanggal Expired Barang
                            </label>
                            <div class="input-group">
                                <input onkeydown="return false" name="exp" id="exp" type="text" class="form-control mydatepicker" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>  
                            <textarea class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <br><br>
                    <div class="col-md-12">
                        <br><br>
                        <button type="button" id="saveBarangMasuk"class="btn btn-success pull-right"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>  
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

 


<div id="modal_list_barang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-large">         
        <div class="modal-content" style="margin-top:100px; margin-right:6%; margin-left:6%;">   
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Daftar Kode Barang</b></h2>     
             </div>  
            <div class="modal-body">
                <div class="row">     
                <div class="table-responsive">   
                <table id="table_daftar_barang_list" class="table table-striped dataTable" cellspacing="0">
                         <thead>  
                             <tr> 
                                 <th>Kode Barang</th>
                                 <th>Nama Barang</th>
                                 <th>Jenis Barang</th>
                                 <th>Stok Akhir</th>
                                 <th>Kondisi Barang</th>
                                 <th>Pilih</th>
                             </tr>  
                         </thead>
                         <tbody> 
                             <tr>   
                                 <td colspan="4">No Data to Display</td>
                             </tr>
                         </tbody>
                     </table>  
                 </div>
                    
                </div>  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<?php $this->load->view('footer');?>
      