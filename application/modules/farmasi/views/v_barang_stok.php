<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Inventori  -  Barang Farmasi </h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Inventori</a></li>
                            <li class="active">Barang Farmasi</li> 
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!--row --> 
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">  
                            <div class="panel-heading"> Data Barang Stok Farmasi
                            </div>
                            <div class="panel-body">
                                
                                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">  
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="inputName1" class="control-label"></label>
                                                    <dl class="text-right">  
                                                        <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                        <dd>Ketik / pilih untuk mencari atau filter data </dd>  
                                                    </dl> 
                                            </div> 
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="form-group col-md-3" style="margin-top: 7px"> 
                                                        <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                        <b>
                                                        <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                            <option value="" disabled selected>Pilih Berdasarkan</option>
                                                            <option value="kode_barang">Kode Barang</option>
                                                            <option value="nama_barang">Nama Barang</option>
                                                            <option value="jenis_barang">Jenis Barang</option>
                                                            <option value="kondisi_barang">Kondisi Barang</option>
                                                            <option value="expired">Expired</option>   
                                                            <!-- <option value="status">Status</option>    -->
                                                        </select></b>
                                                             
                                                    </div>    
                                                    <div class="form-group col-md-9" style="margin-top: 7px">   
                                                        <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                        <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariBarangStok()" >       
                                                    </div> 
                                                </div>
                                            </div> 
                                        </div><br><hr style="margin-top: -27px">
                                         
                                       <div class="row" style="padding-bottom: 30px;margin-top: -10px">
                                            <div class="col-md-6">
                                               <button  type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_add" data-backdrop="static" data-keyboard="false">Tambah Stok Obat</button>  
                                                
                                            </div>    
                                            
                                           </div>
                                        <div class="table">   
                                            <table id="table_barang_stok_list" class="table table-striped dataTable table-responsive" cellspacing="0">
                                                <thead>  
                                                    <tr> 
                                                        <th>No</th>
                                                        <th>Expired</th>
                                                        <th>Kode Barang</th>
                                                        <th>Nama Barang</th>
                                                        <th>Jenis Barang</th>
                                                        <th>Satuan</th>
                                                        <th>BPJS/NON BPJS</th>
                                                        <th>Stok Masuk</th>
                                                        <th>Stok Keluar</th>
                                                        <th>Stok Akhir</th>
                                                        <th>Kondisi Barang</th>
                                                        <th>Harga Satuan</th>
                                                        <th>harga Per Pack</th>
                                                        <th>Harga Pokok</th>
                                                        <th>Tipe Call</th>
                                                        <th>Jenis Pasien</th>
                                                        <th>Keterangan</th>
                                                        <th>Perubahan Terakhir</th>
                                                        <th>Aksi</th>
                                                    </tr>  
                                                </thead>
                                                <tbody> 
                                                    <tr>   
                                                        <td colspan="4">No Data to Display</td>
                                                    </tr>
                                                </tbody>
                                            </table>  
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div> 
                    </div>
                </div>
                <!--/row -->
            </div>
            <!-- /.container-fluid -->

<div id="modal_add" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-large" >
    <?php echo form_open('#',array('id' => 'fmCreateBarangStok'))?>
    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

        <div class="modal-content"  >   
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Tambah Barang stok</b></h2>     
             </div>  
            <div class="modal-body" style="overflow:auto; height: 520px;">
                <div class="row">    
                     
                    <div class="col-md-6">     
                        
                        <div class="form-group">
                            <label class="control-label">
                                Kode Barang<span style="color: red;">*</span>
                            </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_barang" name="kode_barang" placeholder="Kode Barang">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Barang<span style="color: red;">*</span></label>
                            <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama Barang" >
                        </div>
                        <div class="form-group">
                            <label>Satuan<span style="color: red;">*</span></label>
                            <select class="form-control" name="id_sediaan" id="kode_sediaan"  >
                                <option disabled selected>Pilih Satuan</option>
                                <?php
                                    $list_sediaan = $this->Barang_stok_model->get_sediaan_list();
                                    foreach($list_sediaan as $list){
                                        echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <!-- <div class="form-group">
                            <label>Jumlah Keseluruhan<span style="color: red;">*</span></label>
                            <input type="number" name="jumlah_barang_keseluruhan" id="jumlah_barang_keseluruhan" class="form-control" placeholder="Jumlah Barang" min="1" readonly >
                        </div>
                        <div class="form-group">
                            <label>Harga Barang Keseluruhan<span style="color: red;">*</span></label>
                            <input type="text" onkeyup="formatAsRupiah(this)" name="harga_barang_keseluruhan" id="harga_barang_keseluruhan" class="form-control" placeholder="Harga Barang" readonly>
                        </div> -->
                        <div class="form-group">
                            <label>Jenis Barang<span style="color: red;">*</span></label>
                            <select class="form-control" name="id_jenis_barang" id="id_jenis_barang"  >
                               <option value="" disabled selected>Pilih Jenis Barang</option>
                               <option value="1">OBAT</option>
                               <option value="2">BHP</option>
                               <option value="3">ALKES</option>
                            </select>
                        </div> 
                        <!-- <div class="form-group">
                            <label>Jumlah Barang Per Pack<span style="color: red;">*</span></label>
                            <input type="number" name="jumlah_barang_per_pack" id="jumlah_barang_per_pack" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="Perhitungan()">
                        </div>
                        <div class="form-group">
                            <label>Jumlah Pack<span style="color: red;">*</span></label>
                            <input type="number" name="jumlah_pack" id="jumlah_pack" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="Perhitungan()">
                        </div> -->
                        
                        <div class="form-group">
                            <label>Harga Barang Per Item<span style="color: red;">*</span></label>
                            <input type="text"  name="harga_satuan" id="harga_barang_per_item" class="form-control" placeholder="Harga Barang" onkeyup="Perhitungan()" >
                        </div>
                        <div class="form-group">
                            <label>Harga Barang Per Pack<span style="color: red;">*</span></label>
                            <input type="text"  name="harga_pack" id="harga_barang_per_pack" class="form-control" placeholder="Harga Barang" onkeyup="Perhitungan()">
                        </div>
                        <div class="form-group">
                            <label>Harga Barang Pokok<span style="color: red;">*</span></label>
                            <input type="text"  name="harga_pokok" id="harga_barang_per_pack" class="form-control" placeholder="Harga Barang" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        
                        
                        
                        <div class="form-group">
                            <label>Stok Masuk<span style="color: red;">*</span></label>
                            <input type="number" name="stok_masuk" id="stok_masuk" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="">
                        </div>
                        <div class="form-group">
                            <label>Stok Keluar<span style="color: red;">*</span></label>
                            <input type="number" name="stok_keluar" id="stok_keluar" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="">
                        </div>
                        <div class="form-group">
                            <label>Stok Akhir<span style="color: red;">*</span></label>
                            <input type="number" name="stok_akhir" id="stok_akhir" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Expired<span style="color: red;">*</span>
                            </label>
                            <div class="input-group">
                                <input onkeydown="return false" name="expired" id="tgl_awal" type="text" class="form-control datepicker-id" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tipe Call<span style="color: red">*</span></label>
                            <select class="form-control" id="type_call" name="type_call">
                                                <option selected disabled>Choose</option>
                                                <option value="VISIT">VISIT</option>
                                                <option value="CALL">CALL</option>
                                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenis Pasien<span style="color: red">*</span></label>
                            <select class="form-control" id="jenis_pasien" name="jenis_pasien">
                                                <option selected disabled>Choose</option>
                                                <option value="LOKAL">LOKAL</option>
                                                <option value="DOMESTIK">DOMESTIK</option>
                                                <option value="ASING">ASING</option>
                                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Asal Obat</label>
                            <div class="form-control">
                                <input type="checkbox" name="asal_obat" id="asal_obat" value="1">
                                <span style="color: red">* Centang jika obat dibeli di luar.</span>
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <label>Keterangan</label>  
                            <textarea class="form-control" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button id="save" type="button" class="btn btn-success pull-right"><i class="fa fa-floppy-o p-r-10"></i>SIMPAN</button>  
                    </div>
                </div>  
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;overflow: hidden;"> 
    <div class="modal-dialog modal-large" >
    <?php echo form_open('#',array('id' => 'fmUpdateBarangStok'))?>
    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_del" name="<?php echo $this->security->get_csrf_token_name()?>_del" value="<?php echo $this->security->get_csrf_hash()?>" />

        <div class="modal-content"  >   
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title"><b>Edit Barang stok</b></h2>     
             </div>  
            <div class="modal-body" style="overflow:auto; height: 520px;">
                <div class="row">    
                     
                    <div class="col-md-6">     
                        
                        <div class="form-group">
                            <label class="control-label">
                                Kode Barang<span style="color: red;">*</span>
                            </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="upd_kode_barang" name="kode_barang" placeholder="Kode Barang">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Barang<span style="color: red;">*</span></label>
                            <input type="text" name="nama_barang" id="upd_nama_barang" class="form-control" placeholder="Nama Barang" >
                        </div>
                        <div class="form-group">
                            <label>Satuan<span style="color: red;">*</span></label>
                            <select class="form-control" name="id_sediaan" id="upd_id_sediaan"  >
                                <option disabled selected>Pilih Satuan</option>
                                <?php
                                    $list_sediaan = $this->Barang_stok_model->get_sediaan_list();
                                    foreach($list_sediaan as $list){
                                        echo "<option value=".$list->id_sediaan.">".$list->nama_sediaan."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenis Barang<span style="color: red;">*</span></label>
                            <select class="form-control" name="id_jenis_barang" id="upd_id_jenis_barang"  >
                               <option value="" disabled selected>Pilih Jenis Barang</option>
                               <option value="1">OBAT</option>
                               <option value="2">BHP</option>
                               <option value="3">ALKES</option>
                            </select>
                        </div> 
                        
                        
                        <div class="form-group">
                            <label>Harga Barang Per Item<span style="color: red;">*</span></label>
                            <input type="text"  name="harga_satuan" id="upd_harga_satuan" class="form-control" placeholder="Harga Barang" onkeyup="Perhitungan()" >
                        </div>
                        <div class="form-group">
                            <label>Harga Barang Per Pack<span style="color: red;">*</span></label>
                            <input type="text"  name="harga_pack" id="upd_harga_pack" class="form-control" placeholder="Harga Barang" onkeyup="Perhitungan()">
                        </div>
                        <div class="form-group">
                            <label>Harga Barang Pokok<span style="color: red;">*</span></label>
                            <input type="text"  name="harga_pokok" id="upd_harga_pokok" class="form-control" placeholder="Harga Barang" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        
                        
                        
                        <div class="form-group">
                            <label>Stok Masuk<span style="color: red;">*</span></label>
                            <input type="number" name="stok_masuk" id="upd_stok_masuk" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="">
                        </div>
                        <div class="form-group">
                            <label>Stok Keluar<span style="color: red;">*</span></label>
                            <input type="number" name="stok_keluar" id="upd_stok_keluar" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="">
                        </div>
                        <div class="form-group">
                            <label>Stok Akhir<span style="color: red;">*</span></label>
                            <input type="number" name="stok_akhir" id="upd_stok_akhir" class="form-control" placeholder="Jumlah Barang" min="1" onkeyup="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">
                                Expired<span style="color: red;">*</span>
                            </label>
                            <div class="input-group">
                                <input onkeydown="return false" name="expired" id="upd_expired" type="text" class="form-control datepicker-id" value="" placeholder="dd/mmm/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>    
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tipe Call<span style="color: red">*</span></label>
                            <select class="form-control" id="upd_type_call" name="type_call">
                                                <option selected disabled>Choose</option>
                                                <option value="VISIT">VISIT</option>
                                                <option value="CALL">CALL</option>
                                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenis Pasien<span style="color: red">*</span></label>
                            <select class="form-control" id="upd_jenis_pasien" name="jenis_pasien">
                                                <option selected disabled>Choose</option>
                                                <option value="LOKAL">LOKAL</option>
                                                <option value="DOMESTIK">DOMESTIK</option>
                                                <option value="ASING">ASING</option>
                                            </select>
                        </div>
                      
                        <div class="form-group">
                            <label>Keterangan</label>  
                            <textarea class="form-control" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button id="update" type="button" class="btn btn-success pull-right"><i class="fa fa-floppy-o p-r-10"></i>Update</button>  
                    </div>
                </div>  
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

 






<?php $this->load->view('footer');?>
      