<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_floor_stok_masuk_model extends CI_Model {
    // var $column = array('id_barang_masuk','tanggal_masuk');
    // var $order = array('tanggal_masuk' => 'ASC');

    // var $column1 = array('kode_barang','nama_barang');
    // var $order1 = array('kode_barang' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    
    // public function get_barang_masuk_list($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $id_jenis_barang, $nama_supplier){
    public function get_floor_stok_masuk_list(){
         $this->db->from('t_floor_stok_masuk');
        //  $this->db->where('tanggal_masuk BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        //   if(!empty($kode_barang)){
        //       $this->db->like('kode_barang', $kode_barang);
        //   }
        //   if(!empty($nama_barang)){
        //       $this->db->like('nama_barang', $nama_barang);
        //   }
        //   if(!empty($id_jenis_barang)){
        //       $this->db->like('id_jenis_barang', $id_jenis_barang);
        //   }
        //   if(!empty($nama_supplier)){
        //       $this->db->like('nama_supplier', $nama_supplier);
        //   }
         
  
          $length = $this->input->get('length');
          if($length !== false){
              if($length != -1) {
                  $this->db->limit($this->input->get('length'), $this->input->get('start'));
              }
          }
  
          $query = $this->db->get();
          
          return $query->result();
    }
    
    // public function count_barang_masuk_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $id_jenis_barang, $nama_supplier){
    public function count_floor_stok_masuk_all(){
        $this->db->from('t_floor_stok_masuk');
        // $this->db->where('tanggal_masuk BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        //  if(!empty($kode_barang)){
        //      $this->db->like('kode_barang', $kode_barang);
        //  }
        //  if(!empty($nama_barang)){
        //      $this->db->like('nama_barang', $nama_barang);
        //  }
        //  if(!empty($id_jenis_barang)){
        //      $this->db->like('id_jenis_barang', $id_jenis_barang);
        //  }
        //  if(!empty($nama_supplier)){
        //      $this->db->like('nama_supplier', $nama_supplier);
        //  }

        return $this->db->count_all_results();
    }
    
    // public function count_barang_masuk_filtered(){
    //     $this->db->from('t_barang_masuk_farmasi');
    //     $i = 0;
    //     $search_value = $this->input->get('search');
    //     if($search_value){
    //         foreach ($this->column as $item){
    //             ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
    //             $i++;
    //         }
    //     }
    //     $order_column = $this->input->get('order');
    //     if($order_column !== false){
    //         $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
    //     } 
    //     else if(isset($this->order)){
    //         $order = $this->order;
    //         $this->db->order_by(key($order), $order[key($order)]);
    //     }
    //     $query = $this->db->get();
    //     return $query->num_rows();
    // }
    
    // public function insert_barang_masuk($data=array()){
    //     $insert = $this->db->insert('t_barang_masuk_farmasi',$data);
        
    //     return $insert;
    // }
    
    // public function get_barang_masuk_by_id($id){
    //     $query = $this->db->get_where('m_barang_masuk', array('barang_masuk_id' => $id), 1, 0);
        
    //     return $query->row();
    // }
    
    // public function update_barang_masuk($data, $id){
    //     $update = $this->db->update('m_barang_masuk', $data, array('barang_masuk_id' => $id));
        
    //     return $update;
    // }
    
    // public function delete_barang_masuk($id){
    //     $delete = $this->db->delete('t_barang_masuk_farmasi', array('id_barang_masuk' => $id));
        
    //     return $delete;
    // }
    

    // public function get_sediaan_list(){
    //     return $this->db->get('m_sediaan_obat')->result();
    // }

    // public function get_supplier_list(){
    //     return $this->db->get('m_supplier')->result();
    // }


    // public function get_list_daftar_barang(){
    //     $this->db->from('t_stok_farmasi');
    //     $i = 0;
    //     $search_value = $this->input->get('search');
    //     if($search_value){
    //         foreach ($this->column1 as $item){
    //             ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
    //             $i++;
    //         }
    //     }
    //     $order_column = $this->input->get('order');
    //     if($order_column !== false){
    //         $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
    //     } 
    //     else if(isset($this->order1)){
    //         $order1 = $this->order1;
    //         $this->db->order_by(key($order1), $order1[key($order1)]);
    //     }
        
    //     $length = $this->input->get('length');
    //     if($length !== false){
    //         if($length != -1) {
    //             $this->db->limit($this->input->get('length'), $this->input->get('start'));
    //         }
    //     }
        
    //     $query = $this->db->get();
        
    //     return $query->result();
    // }
    
    // public function count_list_daftar_barang(){
    //     $this->db->from('t_stok_farmasi');

    //     return $this->db->count_all_results();
    // }
    
    // public function count_list_filtered_daftar_barang(){
    //     $this->db->from('t_stok_farmasi');
    //     $i = 0;
    //     $search_value = $this->input->get('search');
    //     if($search_value){
    //         foreach ($this->column1 as $item){
    //             ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
    //             $i++;
    //         }
    //     }
    //     $order_column = $this->input->get('order');
    //     if($order_column !== false){
    //         $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
    //     } 
    //     else if(isset($this->order1)){
    //         $order1 = $this->order1;
    //         $this->db->order_by(key($order1), $order1[key($order1)]);
    //     }
    //     $query = $this->db->get();
    //     return $query->num_rows();
    // }

    // public function get_daftar_barang_by_id($id){
    //     $query = $this->db->get_where('t_stok_farmasi', array('kode_barang' => $id), 1, 0);
        
    //     return $query->result();
    // }

 
}