<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Floor_stok_model extends CI_Model {
    var $column = array('kode_barang','expired');
    var $order = array('expired' => 'ASC');

  
    
    public function __construct() {
        parent::__construct();
    }
    
    // public function get_barang_stok_list($tgl_awal, $tgl_akhir, $nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired){
    public function get_floor_stok_list(){
        $this->db->from('t_floor_stok');
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        // if(!empty($kode_barang)){
        //     $this->db->like('kode_barang', $kode_barang);
        // }
        // if(!empty($nama_barang)){
        //     $this->db->like('nama_barang', $nama_barang);
        // }
        // if(!empty($jenis_barang)){
        //     $this->db->like('jenis_barang', $jenis_barang);
        // }
        // if(!empty($kondisi_barang)){
        //     $this->db->like('kondisi_barang', $kondisi_barang);
        // }
        // if(!empty($nama_supplier)){
        //     $this->db->like('nama_supplier', $nama_supplier);
        // }
        // if(!empty($expired)){
        //     $this->db->like('expired', $expired);
        // }
       

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        
        return $query->result();
    }
    
    // public function count_floor_stok_all($tgl_awal, $tgl_akhir, $nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired){
    public function count_floor_stok_all(){
        $this->db->from('t_floor_stok');

        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        // if(!empty($kode_barang)){
        //     $this->db->like('kode_barang', $kode_barang);
        // }
        // if(!empty($nama_barang)){
        //     $this->db->like('nama_barang', $nama_barang);
        // }
        // if(!empty($jenis_barang)){
        //     $this->db->like('jenis_barang', $jenis_barang);
        // }
        // if(!empty($kondisi_barang)){
        //     $this->db->like('kondisi_barang', $kondisi_barang);
        // }
        // if(!empty($nama_supplier)){
        //     $this->db->like('nama_supplier', $nama_supplier);
        // }
        // if(!empty($expired)){
        //     $this->db->like('expired', $expired);
        // }

        return $this->db->count_all_results();
    }
    
    // public function count_barang_stok_filtered(){
    //     $this->db->from('t_stok_farmasi');
    //     $i = 0;
    //     $search_value = $this->input->get('search');
    //     if($search_value){
    //         foreach ($this->column as $item){
    //             ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
    //             $i++;
    //         }
    //     }
    //     $order_column = $this->input->get('order');
    //     if($order_column !== false){
    //         $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
    //     } 
    //     else if(isset($this->order)){
    //         $order = $this->order;
    //         $this->db->order_by(key($order), $order[key($order)]);
    //     }
    //     $query = $this->db->get();
    //     return $query->num_rows();
    // }
    
    // public function insert_barang_stok($data=array()){
    //     $insert = $this->db->insert('m_barang_stok',$data);
        
    //     return $insert;
    // }
    
    // public function get_barang_stok_by_id($id){
    //     $query = $this->db->get_where('m_barang_stok', array('barang_stok_id' => $id), 1, 0);
        
    //     return $query->row();
    // }
    
    // public function update_barang_stok($data, $id){
    //     $update = $this->db->update('m_barang_stok', $data, array('barang_stok_id' => $id));
        
    //     return $update;
    // }
    
    // public function delete_barang_stok($id){
    //     $delete = $this->db->delete('m_barang_stok', array('barang_stok_id' => $id));
        
    //     return $delete;
    // }
    

    // public function get_sediaan_list(){
    //     return $this->db->get('m_sediaan_obat')->result();
    // }

    // public function get_supplier_list(){
    //     return $this->db->get('m_supplier')->result();
    // }

}