<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_stok_model extends CI_Model {
    var $column = array('kode_barang','expired');
    var $order = array('expired' => 'ASC');

  
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_barang_stok_list($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired){
        $this->db->select("*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis");
        $this->db->join("m_sediaan_obat","m_sediaan_obat.id_sediaan = t_stok_farmasi.id_sediaan","left");
        $this->db->join("m_jenis_barang_farmasi","m_jenis_barang_farmasi.id_jenis_barang = t_stok_farmasi.id_jenis_barang","left");
        $this->db->from('t_stok_farmasi');
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($kode_barang)){
            $this->db->like('kode_barang', $kode_barang);
        }
        if(!empty($nama_barang)){
            $this->db->like('nama_barang', $nama_barang);
        }
        if(!empty($jenis_barang)){
            $this->db->like('nama_jenis', $jenis_barang);
        }
        if(!empty($kondisi_barang)){
            $this->db->like('kondisi_barang', $kondisi_barang);
        }
        if(!empty($nama_supplier)){
            $this->db->like('nama_supplier', $nama_supplier);
        }
        if(!empty($expired)){
            $this->db->like('expired', $expired);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();
        return $query->result();
    }
    
    public function count_barang_stok_all($nama_barang, $kode_barang, $jenis_barang, $kondisi_barang, $expired){
        $this->db->select("*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis");
        $this->db->join("m_sediaan_obat","m_sediaan_obat.id_sediaan = t_stok_farmasi.id_sediaan","left");
        $this->db->join("m_jenis_barang_farmasi","m_jenis_barang_farmasi.id_jenis_barang = t_stok_farmasi.id_jenis_barang","left");
        $this->db->from('t_stok_farmasi');
        // $this->db->where('perubahan_terakhir BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($kode_barang)){
            $this->db->like('kode_barang', $kode_barang);
        }
        if(!empty($nama_barang)){
            $this->db->like('nama_barang', $nama_barang);
        }
        if(!empty($jenis_barang)){
            $this->db->like('nama_jenis', $jenis_barang);
        }
        if(!empty($kondisi_barang)){
            $this->db->like('kondisi_barang', $kondisi_barang);
        }
        if(!empty($nama_supplier)){
            $this->db->like('nama_supplier', $nama_supplier);
        }
        if(!empty($expired)){
            $this->db->like('expired', $expired);
        }
        return $this->db->count_all_results();

    }
    
    // public function count_barang_stok_filtered(){
    //     $this->db->from('t_stok_farmasi');
    //     $i = 0;
    //     $search_value = $this->input->get('search');
    //     if($search_value){
    //         foreach ($this->column as $item){
    //             ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
    //             $i++;
    //         }
    //     }
    //     $order_column = $this->input->get('order');
    //     if($order_column !== false){
    //         $this->db->order_by($this->column[$order_column['0']['column']], $order_column['0']['dir']);
    //     } 
    //     else if(isset($this->order)){
    //         $order = $this->order;
    //         $this->db->order_by(key($order), $order[key($order)]);
    //     }
    //     $query = $this->db->get();
    //     return $query->num_rows();
    // }
    
    // public function insert_barang_stok($data=array()){
    //     $insert = $this->db->insert('m_barang_stok',$data);
        
    //     return $insert;
    // }
    
    // public function get_barang_stok_by_id($id){
    //     $query = $this->db->get_where('m_barang_stok', array('barang_stok_id' => $id), 1, 0);
        
    //     return $query->row();
    // }
    
    // public function update_barang_stok($data, $id){
    //     $update = $this->db->update('m_barang_stok', $data, array('barang_stok_id' => $id));
        
    //     return $update;
    // }
    
    public function delete_barang_stok($id){
        $delete = $this->db->delete('t_stok_farmasi', array('kode_barang' => $id));
        
        return $delete;
    }
    public function get_barang_by_id($id)
    {
        $query = $this->db->get_where('t_stok_farmasi', array('kode_barang' => $id), 1, 0);
        
        return $query->row();
    }
    public function insert_stok_barang($data)
    {
        $insert = $this->db->insert('t_stok_farmasi',$data);
        // print_r($this->db->error());die;
        // $id = $this->db->insert_id();
        // var_dump($insert);die;
        return $insert;
    }
    public function update_stok_barang($data, $id){
        $update = $this->db->update('t_stok_farmasi', $data, array('kode_barang' => $id));
        // print_r($this->db->error());die;
        return $update;
    }
    public function get_sediaan_list(){
        return $this->db->get('m_sediaan_obat')->result();
    }

    public function get_supplier_list(){
        return $this->db->get('m_supplier')->result();
    }
    public function casting_date_indo($date){
        $dates = strtolower($date);
        $month = "";
        list($tgl,$bln,$thn) = explode(" ", $dates) ;
        if ($bln == "januari") {
            $month = "01" ;
        } elseif ($bln == "februari" || $bln == "pebruari") {
            $month = "02" ;          
        } elseif ($bln == "maret") {
            $month = "03" ;          
        } elseif ($bln == "april") {
            $month = "04" ;          
        } elseif ($bln == "mei") {
            $month = "05" ;          
        } elseif ($bln == "juni") {
            $month = "06" ;          
        } elseif ($bln == "juli") {
            $month = "07" ;          
        } elseif ($bln == "agustus") {
            $month = "08" ;          
        } elseif ($bln == "september") {
            $month = "09" ;          
        } elseif ($bln == "oktober") {
            $month = "10" ;          
        } elseif ($bln == "november") {
            $month = "11" ;          
        } elseif ($bln == "desember") {
            $month = "12" ;          
        }
        $format_date_indo = $thn."-".$month."-".$tgl ;
        return $format_date_indo ;
    }

}