<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_keluar_model extends CI_Model {
    var $column = array('id_barang_keluar','tanggal_keluar');
    var $order = array('tanggal_keluar' => 'ASC');

    var $column1 = array('kode_barang','nama_barang');
    var $order1 = array('kode_barang' => 'ASC');
    
    public function __construct() {
        parent::__construct();
    }
    

    public function tolak_pengajuan($data, $id_approval){
      
      $update = $this->db->update('t_approval_bf', $data, array('id_approval' => $id_approval));
      return $update;
    }


    public function setuju_pengajuan($data, $id_approval){
      $update = $this->db->update('t_approval_bf', $data, array('id_approval' => $id_approval));
    //   var_dump($update);die();
      return $update;
    }

    public function tampil_stok($kode_barang){
        $this->db->from('t_stok_farmasi');
        $this->db->where('kode_barang',$kode_barang);
        
        $query = $this->db->get();
        return $query->row();
    }

    public function get_barang_keluar_list($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $dari_gudang, $di_tujukan){
        $this->db->select("*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis");
        $this->db->join("m_sediaan_obat","m_sediaan_obat.id_sediaan = t_barang_keluar_farmasi.id_sediaan","left");
        $this->db->join("m_jenis_barang_farmasi","m_jenis_barang_farmasi.id_jenis_barang = t_barang_keluar_farmasi.id_jenis_barang","left");
        $this->db->from('t_barang_keluar_farmasi');
        $this->db->where('tanggal_keluar BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($kode_barang)){
            $this->db->like('kode_barang', $kode_barang);
        }
        if(!empty($nama_barang)){
            $this->db->like('nama_barang', $nama_barang);
        }
        if(!empty($jenis_barang)){
            $this->db->like('nama_jenis', $jenis_barang);
        }
        if(!empty($dari_gudang)){
            $this->db->like('dari_gudang', $dari_gudang);
        }
        if(!empty($di_tujukan)){
            $this->db->like('di_tujukan', $di_tujukan);
        }
      
       

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();
        
        return $query->result();
        
    }
    
    public function count_barang_keluar_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $jenis_barang, $dari_gudang, $di_tujukan){
         $this->db->select("*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis");
        $this->db->join("m_sediaan_obat","m_sediaan_obat.id_sediaan = t_barang_keluar_farmasi.id_sediaan","left");
        $this->db->join("m_jenis_barang_farmasi","m_jenis_barang_farmasi.id_jenis_barang = t_barang_keluar_farmasi.id_jenis_barang","left");
        $this->db->from('t_barang_keluar_farmasi');
        $this->db->where('tanggal_keluar BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        if(!empty($kode_barang)){
            $this->db->like('kode_barang', $kode_barang);
        }
        if(!empty($nama_barang)){
            $this->db->like('nama_barang', $nama_barang);
        }
        if(!empty($jenis_barang)){
            $this->db->like('nama_jenis', $jenis_barang);
        }
        if(!empty($dari_gudang)){
            $this->db->like('dari_gudang', $dari_gudang);
        }
        if(!empty($di_tujukan)){
            $this->db->like('di_tujukan', $di_tujukan);
        }
       
        return $this->db->count_all_results();
    }
    
    public function get_barang_pengajuan_list($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $id_jenis_barang, $nama_supplier){
         $this->db->from('t_approval_bf');   
         // $this->db->where('tanggal_masuk BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
          if(!empty($kode_barang)){
              $this->db->like('kode_barang', $kode_barang);
          }
          if(!empty($nama_barang)){
              $this->db->like('nama_barang', $nama_barang);
          }
          if(!empty($id_jenis_barang)){
              $this->db->like('id_jenis_barang', $id_jenis_barang);
          }
          if(!empty($nama_supplier)){
              $this->db->like('nama_supplier', $nama_supplier);
          }
          $length = $this->input->get('length');
          if($length !== false){
              if($length != -1) {
                  $this->db->limit($this->input->get('length'), $this->input->get('start'));
              }
          }
  
          $query = $this->db->get();
          return $query->result();
    }
    
    public function count_barang_pengajuan_all($tgl_awal, $tgl_akhir, $kode_barang, $nama_barang, $id_jenis_barang, $nama_supplier){
        $this->db->from('t_approval_bf');
        // $this->db->where('tanggal_masuk BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
         if(!empty($kode_barang)){
             $this->db->like('kode_barang', $kode_barang);
         }
         if(!empty($nama_barang)){
             $this->db->like('nama_barang', $nama_barang);
         }
         if(!empty($id_jenis_barang)){
             $this->db->like('id_jenis_barang', $id_jenis_barang);
         }
         if(!empty($nama_supplier)){
             $this->db->like('nama_supplier', $nama_supplier);
         }

        return $this->db->count_all_results();
    }
    
    public function insert_barang_keluar($data=array()){
        $insert = $this->db->insert('t_barang_keluar_farmasi',$data);
        
        return $insert;
    }

    //insert to table apotek masuk
    public function insert_barang_masuk_apotek($data=array()){
        $insert = $this->db->insert('t_apotek_masuk',$data);
        
        return $insert;
    }

    //insert to table apotek stok
    public function insert_barang_stok_apotek($data=array()){
        $insert = $this->db->insert('t_apotek_stok',$data);
        
        return $insert;
    }
    
    // public function get_barang_keluar_by_id($id){
    //     $query = $this->db->get_where('m_barang_keluar', array('barang_keluar_id' => $id), 1, 0);
        
    //     return $query->row();
    // }
    
    // public function update_barang_keluar($data, $id){
    //     $update = $this->db->update('m_barang_keluar', $data, array('barang_keluar_id' => $id));
        
    //     return $update;
    // }
    
    public function delete_barang_keluar($id){
        $delete = $this->db->delete('t_barang_keluar_farmasi', array('id_barang_keluar' => $id));
        
        return $delete;
    }
    

    public function get_sediaan_list(){
        return $this->db->get('m_sediaan_obat')->result();
    }

    public function get_jenis_list(){
        return $this->db->get('m_jenis_barang_farmasi')->result();
    }

    public function get_supplier_list(){
        return $this->db->get('m_supplier')->result();
    }


    public function get_list_daftar_barang(){
        $this->db->select("*, m_jenis_barang_farmasi.nama_jenis, m_sediaan_obat.nama_sediaan");
        $this->db->join("m_jenis_barang_farmasi","m_jenis_barang_farmasi.id_jenis_barang = t_stok_farmasi.id_jenis_barang",'left');
        $this->db->join("m_sediaan_obat","m_sediaan_obat.id_sediaan = t_stok_farmasi.id_sediaan",'left');
        $this->db->from('t_stok_farmasi');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    public function count_list_daftar_barang(){
        $this->db->from('t_stok_farmasi');

        return $this->db->count_all_results();
    }
    
    public function count_list_filtered_daftar_barang(){
        $this->db->from('t_stok_farmasi');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        } 
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_daftar_barang_by_id($id){
        $query = $this->db->get_where('t_stok_farmasi', array('kode_barang' => $id), 1, 0);
        
        return $query->result();
    }

    public function get_pengajuan_by_id($id){
        $query = $this->db->get_where('t_approval_bf', array('id_approval' => $id), 1, 0);
        
        return $query->result();
    }

     public function get_data_approval($id_approval){
        $query = $this->db->get_where('t_approval_bf', array('id_approval' => $id_approval), 1, 0);

        return $query->row();
    }

    public function insert_notif($data){   
        $this->db->insert('t_notif',$data);
        $id = $this->db->insert_id();
        return $id;    
    }

 
}