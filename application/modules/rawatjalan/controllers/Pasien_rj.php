<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_rj extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->library("Aauth");
        $this->load->library("excel");

        if (!$this->aauth->is_loggedin()) {
        	$this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Please login first.');
                redirect('login');
        }

        $this->load->model('Menu_model');
        $this->load->model('Pasien_rj_model');
        $this->load->model('Models');

        $this->data['users']                = $this->aauth->get_user();
        $this->data['groups']               = $this->aauth->get_user_groups();
        $this->data['list_menu_sidebar']    = $this->Menu_model->get_list_menu($this->data['groups'][0]->group_id);
        // data ketersediaan kamar di header
        $this->load->model('Ketersediaan_kamar_model');
        $get_data_kamar = $this->Ketersediaan_kamar_model->get_ketersediaan();
        foreach($get_data_kamar as $key => $value){
          // print_r($value);
          $ketersediaan_kamar[$value['kelaspelayanan_id']] = $value['tersedia'];
        }
        $this->data['ketersediaan_kamar'] = $ketersediaan_kamar;
    }

    public function index(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $this->session->set_flashdata('notification', $this->lang->line('aauth_error_no_access'));
            redirect('no_permission');
        }

        // if permitted, do logit
        $perms      = "rawat_jalan_pasienrj_view";
        $comments   = "List Pasien Rawat Jalan";
        $this->aauth->logit($perms, current_url(), $comments);
    	$this->load->view('v_pasien_rj', $this->data);
    }

    public function ajax_list_pasienrj(){
        $tgl_awal       = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir      = $this->input->get('tgl_akhir',TRUE);
        $poliruangan    = $this->input->get('poliruangan',TRUE);
        $dokter_poli    = $this->input->get('dokter_poli',TRUE);
        $no_pendaftaran = $this->input->get('no_pendaftaran',TRUE);
        $no_rekam_medis = $this->input->get('no_rekam_medis',TRUE);
        $no_bpjs        = $this->input->get('no_bpjs',TRUE);
        $pasien_nama    = $this->input->get('pasien_nama',TRUE);
        $pasien_alamat  = $this->input->get('pasien_alamat',TRUE);
        $name_dokter    = $this->input->get('name_dokter',TRUE);
        $urutan         = $this->input->get('urutan',TRUE);
        $list           = $this->Pasien_rj_model->get_pasienrj_list($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan);
        $data           = array();
        $no             = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pasienrj){
            $data_reservasi = $this->Pasien_rj_model->get_reservasi_id($pasienrj->no_pendaftaran);
            @$hotel = $this->Pasien_rj_model->get_hotel_by_id($data_reservasi->hotel_id);

            $no++;
            $row = array();
            $row[] = $pasienrj->no_pendaftaran;
            $row[] = '<span data-toggle="modal" data-target="#modal_riwayat_penyakit_pasien" data-backdrop="static" data-keyboard="false" title="klik untuk Riwayat Penyaakit Pasien"  onclick="riwayatPenyakitPasien('."'".$pasienrj->pasien_id."'".')">'.$pasienrj->no_rekam_medis."/ \n".$pasienrj->pasien_nama.'</span>';
//            $row[] = $pasienrj->type_pembayaran == "Asuransi" ? $pasienrj->no_asuransi : $pasienrj->no_bpjs;
            $row[] = date('d M Y H:i:s', strtotime($pasienrj->tgl_pendaftaran));
            $row[] = $pasienrj->nama_poliruangan;
            $row[] = $pasienrj->kelaspelayanan_nama;
            $row[] = $pasienrj->NAME_DOKTER;
//            $row[] = $pasienrj->jam_periksa;
            $row[] = @$hotel->nama_hotel;
            $row[] = @$data_reservasi->room_number;
            $row[] = $pasienrj->jenis_kelamin;
            $row[] = $pasienrj->umur;
            $row[] = $pasienrj->pasien_alamat;
            $row[] = $pasienrj->type_pembayaran;
            $row[] = $pasienrj->status_periksa;

            if(trim(strtoupper($pasienrj->status_periksa)) == "SEDANG DI PENUNJANG" || trim(strtoupper($pasienrj->status_periksa)) == "BATAL PERIKSA"){

                $row[] = '<button disabled type="button" class="btn btn-default"  title="Klik untuk print" onclick="form_print('."'".$pasienrj->pasien_id."'".') ">RESCHEDULE</button>';
                $row[] = '<button disabled type="button" class="btn btn-default"  title="Klik untuk print" onclick="form_print('."'".$pasienrj->pasien_id."'".') ">PRINT</button>';
                $row[] = '<button type="button" class="btn btn-default" title="Klik untuk pemeriksaan pasien" disabled>PERIKSA</button>';
                //$row[] = '<button type="button" class="btn btn-default" title="Klik untuk memberi reseptur pasien"  disabled>RESEPTUR</button>';
                //$row[] = '<button type="button" class="btn btn-default" title="Klik untuk kirim ke penunjang" disabled>PENUNJANG</button>';
                //$row[] = '<button type="button" class="btn btn-default" title="Klik untuk kirim ke IGD" disabled>IGD</button>';
                //$row[] = '<button type="button" class="btn btn-default btn-circle" title="Klik untuk membatalkan pemeriksaan pasien"  disabled><i class="fa fa-times"></i></button>';
                $row[] = '
                    <div class="dropdown-action" onclick="$(this).children().toggleClass('."'d-block'".')">
                        <span class="title-dropdown">Pilih Action</span><span class="arrow">▼</span>
                        <div class="dropdown-menu-action">
                            <button disabled type="button" class="btn btn-success" data-target="#modal_reseptur" data-toggle="modal" data-backdrop="static" data-keyboard="false" title="Klik untuk memberi reseptur pasien" onclick="resepturPasienRj('."'".$pasienrj->pendaftaran_id."'".')">RESEPTUR</button>
                            <button disabled type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_pulangkan" data-backdrop="static" data-keyboard="false"  title="Klik untuk pulangkan pasien" onclick="pulangkanPasien('."'".$pasienrj->pendaftaran_id."'".')" '.(trim(strtoupper($pasienrj->status_periksa)) == "DIPULANGKAN" ? "disabled" : "").'>PULANGKAN</button>
                            <button disabled type="button" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasienrj->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                ';
            }else{
                // $no_reg = $pasienrj->no_pendaftaran;
                @$res_id = $data_reservasi->reservasi_id;
                $row[] = '<button type="button" class="btn btn-success"  title="Klik untuk print" onclick="form_reschedule('."'".$pasienrj->pendaftaran_id."','".$pasienrj->id_M_DOKTER."','".$res_id."'".')">RESCHEDULE</button>';
                $row[] = '<button type="button" class="btn btn-success"  title="Klik untuk print" onclick="form_print('."'".$pasienrj->pasien_id."'".')">PRINT</button>';
                $row[] = '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_periksa_pasienrj" data-backdrop="static" data-keyboard="false" title="Klik untuk pemeriksaan pasien" onclick="periksaPasienRj('."'".$pasienrj->pendaftaran_id."'".')">PERIKSA</button>';
                $row[] = '
                    <div class="dropdown-action" onclick="$(this).children().toggleClass('."'d-block'".')">
                        <span class="title-dropdown">Pilih Action</span><span class="arrow">▼</span>
                        <div class="dropdown-menu-action">
                            <button type="button" class="btn btn-success" data-target="#modal_reseptur" data-toggle="modal" data-backdrop="static" data-keyboard="false" title="Klik untuk memberi reseptur pasien" onclick="resepturPasienRj('."'".$pasienrj->pendaftaran_id."'".')">RESEPTUR/OBAT</button>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_pulangkan" data-backdrop="static" data-keyboard="false"  title="Klik untuk pulangkan pasien" onclick="pulangkanPasien('."'".$pasienrj->pendaftaran_id."'".')" '.(trim(strtoupper($pasienrj->status_periksa)) == "DIPULANGKAN" ? "disabled" : "").'>PULANGKAN</button>
                            <button '.(trim(strtoupper($pasienrj->status_periksa)) == "SUDAH BAYAR" ? 'disabled' : '').' type="button" class="btn btn-danger btn-circle" title="Klik untuk membatalkan pemeriksaan pasien" onclick="batalPeriksa('."'".$pasienrj->pendaftaran_id."'".')"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                ';
            }
            //add html for action
            $data[] = $row;
        }

        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_rj_model->count_all($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan),
                    "recordsFiltered" => $this->Pasien_rj_model->count_all($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_pulangkan_pasien() {
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
		if(!$is_permit) {
			$res = array(
				'csrfTokenName' => $this->security->get_csrf_token_name(),
				'csrfHash' => $this->security->get_csrf_hash(),
				'success' => false,
				'messages' => $this->lang->line('aauth_error_no_access'));
			echo json_encode($res);
			exit;
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
		$this->form_validation->set_rules('pasien_id','Status Pulang', 'required');

		if ($this->form_validation->run() == FALSE)  {
			$error = validation_errors();
			$res = array(
				'csrfTokenName' => $this->security->get_csrf_token_name(),
				'csrfHash'      => $this->security->get_csrf_hash(),
				'success'       => false,
				'messages'      => $error);

			// if permitted, do logit
			$perms = "rawat_darurat_pasienrd_view";
			$comments = "Gagal set status pulang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
			$this->aauth->logit($perms, current_url(), $comments);

		}else if ($this->form_validation->run() == TRUE)  {
			// inisialisasi data post untuk update an ke tabel t_pendaftaran
			$pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
			$kondisi_pasien = $this->input->post('kondisi_pasien', TRUE);
			$catatan_pulang = $this->input->post('catatan', TRUE);
			$user_id = $this->data['users']->id;
			$pasen_id = $this->input->post('pasien_id', TRUE);

			// assign field dengan data hasil post untuk di update
			$data = array(
				'status_periksa' => 'DIPULANGKAN',
				'carapulang' => $kondisi_pasien,
				'catatan_pulang' => $catatan_pulang,
				'petugas_id' => $user_id
			);

			$data_pasien = $this->Models->where("m_pasien", array('pasien_id' => $pasen_id))[0];
			$data_pendaftaran = $this->Models->where("t_pendaftaran", array('pendaftaran_id' => $pendaftaran_id))[0];
			$data_reservasi = $this->Models->where("t_reservasi", array('no_pendaftaran' => $data_pendaftaran['no_pendaftaran']))[0];
			$hotel_id = $data_reservasi['hotel_id'] != null ? $data_reservasi['hotel_id'] : 0;
			$fee_sokter = $this->get_fee_dokter_params($hotel_id, $data_pasien['jenis_pasien'], $data_pasien['type_call'], $data_reservasi['pemanggilan']);

            $data_fee_dokter = array(
                'pasien_id'      => $pasen_id,
                'pendaftaran_id' => $pendaftaran_id,
                'harga'          => $fee_sokter,
                'diskon'         => 0,
                'total_harga'    => $fee_sokter,
                'cara_bayar'     => '',
                'pembayaran_id'  => 0,
                'status_bayar'   => 0
            );

			$update_pendaftaran = $this->Pasien_rj_model->update_pendaftaran($pendaftaran_id, $data);

			if($update_pendaftaran){

			    $this->Models->insert("rawatjalan_feedokter", $data_fee_dokter);

				$res = array(
					'csrfTokenName' => $this->security->get_csrf_token_name(),
					'csrfHash' => $this->security->get_csrf_hash(),
					'success' => true,
					'messages' => 'Berhasil set status pulang'
				);
				// if permitted, do logit
				$perms = "rawat_darurat_pasienrd_view";
				$comments = "Berhasil set status pulang";
				$this->aauth->logit($perms, current_url(), $comments);
			}else{
				$res = array(
					'csrfTokenName' => $this->security->get_csrf_token_name(),
					'csrfHash' => $this->security->get_csrf_hash(),
					'success' => false,
					'messages' => 'Gagal set status pulang, silakan hubungi web administrator.'
				);
				// if permitted, do logit
				$perms = "rawat_darurat_pasienrd_view";
				$comments = "Gagal set status pulang";
				$this->aauth->logit($perms, current_url(), $comments);
			}
		}
		echo json_encode($res);
    }

    public function do_save_ambulance_assistance() {
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

            $reservasi_id     = (int)$this->input->post('reservasi_id', TRUE);
            $driver_id_2      = $this->input->post('driver_2', TRUE);
            $cek_malam        = $this->input->post('cek_malam', TRUE);
            // echo json_encode($pendaftaran_id); die();
            $data_tindakanpasien = array(
                'driver_id_2'     => $driver_id_2,
                'pemanggilan'     => $cek_malam
            );

            // var_dump($data_tindakanpasien);die();
            $ins = $this->Pasien_rj_model->insert_ambulance_assistance($data_tindakanpasien, $reservasi_id);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        echo json_encode($res);
    }

    public function do_save_medical_information() {
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

            $pasien_id     = (int)$this->input->post('pasien_id', TRUE);
            $present_complain  = $this->input->post('present_complain', TRUE);
            $date_first_complain          = $this->input->post('date_first_complain', TRUE);
            $date_first_consultation             = $this->input->post('date_first_consultation', TRUE);
            $allergy_history           = $this->input->post('allergy_history', TRUE);
            $pregnant       = $this->input->post('pregnant', TRUE);
            $weight     = $this->input->post('weight', TRUE);
            $current_medication           = $this->input->post('current_medication', TRUE);
            $medical_history         = $this->input->post('medical_history', TRUE);
            $medical_event         = $this->input->post('medical_event', TRUE);
            // echo json_encode($pendaftaran_id); die();
            $data_tindakanpasien = array(
                'present_complain'        => $present_complain,
                'date_first_complain'     => $date_first_complain,
                'date_first_consultation'             => $date_first_consultation,
                'allergy_history'     => $allergy_history,
                'pregnant'          => $pregnant,
                'current_medication'        => $current_medication,
                'weight'  => $weight,
                'medical_history'           => $medical_history,
                'medical_event'             => $medical_event
            );

            // var_dump($data_tindakanpasien);die();
            $ins = $this->Pasien_rj_model->insert_medical_information($data_tindakanpasien, $pasien_id);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        echo json_encode($res);
    }

    public function periksa_pasienrj($pendaftaran_id){
        $pasienrj['list_pasien'] = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienrj['medical_information'] = $this->Pasien_rj_model->get_medical_information($pasienrj['list_pasien']->pasien_id);
        $pasienrj['reservasi'] = $this->Pasien_rj_model->get_reservasi_by_rekam_medis($pasienrj['list_pasien']->no_rekam_medis);
        $pasienrj['perawat'] = $this->Pasien_rj_model->get_perawat_pasien($pasienrj['list_pasien']->pasien_id, $pendaftaran_id);

//        echo "<pre>";
//        print_r($pasienrj['reservasi']);
//        exit;

        $this->load->view('periksa_pasienrj', $pasienrj);
    }

    public function pulangkan_pasien($pendaftaran_id){
        $pasienrj['list_pasien'] = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
		$pasienrj['list_kondisi'] = $this->Pasien_rj_model->get_lookup_pasien();
        $this->load->view('pulangkan_pasien', $pasienrj);
    }

    public function reseptur_pasienrj($pendaftaran_id){
        $pasienrj['list_pasien'] = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
        $pasienrj['medical_information'] = $this->Pasien_rj_model->get_medical_information($pasienrj['list_pasien']->pasien_id);
        $pasienrj['reservasi'] = $this->Pasien_rj_model->get_reservasi_by_rekam_medis($pasienrj['list_pasien']->no_rekam_medis);

        $this->load->view('reseptur_pasienrj', $pasienrj);
    }

    public function bidan_pasienrj($pendaftaran_id){
        $pasienrj['list_kebidanan'] = $this->Pasien_rj_model->get_datapasien_kebidanan($pendaftaran_id);
        $this->load->view('periksa_pasienrj', $pasienrj);
    }

    public function ajax_list_diagnosa_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list           = $this->Pasien_rj_model->get_diagnosa_pasien_list($pendaftaran_id);
        $data           = array();
        foreach($list as $diagnosa){
//            $tgl_diagnosa = date_create($diagnosa->tgl_diagnosa);
            $row = array();
            $row[] = date('d M Y', strtotime($diagnosa->tgl_diagnosa));
            $row[] = $diagnosa->kode_diagnosa;
            $row[] = $diagnosa->nama_diagnosa;
            $row[] = $diagnosa->kode_icd10;
            $row[] = $diagnosa->nama_icd10;
            $row[] = $diagnosa->jenis_diagnosa == '1' ? "Diagnosa Utama" : "Diagnosa Tambahan";
            $row[] = $diagnosa->diagnosa_by;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusDiagnosa('."'".$diagnosa->diagnosapasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function do_create_diagnosa_pasien(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_diagnosa','Kode Diagnosa', 'required|trim');
        $this->form_validation->set_rules('nama_diagnosa','Nama Diagnosa', 'required|trim');
        $this->form_validation->set_rules('jenis_diagnosa','Jenis Diagnosa', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input diagnosa pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id          = $this->input->post('pasien_id', TRUE);
            $diagnosa_id        = $this->input->post('diagnosa_id', TRUE);
            $dokter             = $this->input->post('dokter_id_diagnosa', TRUE);
            $nama_diagnosa      = $this->input->post('nama_diagnosa', TRUE);
            $kode_icd10         = $this->input->post('kode_diagnosa_icd10', TRUE);
            $nama_icd10         = $this->input->post('nama_diagnosa_icd10', TRUE);
            $kd_diagnosa        = $this->input->post('kode_diagnosa', TRUE);
            $jenis_diagnosa     = $this->input->post('jenis_diagnosa', TRUE);
            $data_diagnosapasien = array(
                'nama_diagnosa'  => $nama_diagnosa,
                'tgl_diagnosa'   => date('Y-m-d'),
                'kode_diagnosa'  => $kd_diagnosa,
                'kode_icd10'     => $kode_icd10,
                'nama_icd10'     => $nama_icd10,
                'kode_diagnosa'  => $kd_diagnosa,
                'jenis_diagnosa' => $jenis_diagnosa,
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id'      => $pasien_id,
                'dokter_id'      => $dokter,
                'diagnosa_id'    => $diagnosa_id,
                'diagnosa_by'    => 'Poli RJ'
            );
            // cek diagnosa primer
            $query = $this->db->get_where('t_diagnosapasien', array('pendaftaran_id'=>$pendaftaran_id,'1'=>$jenis_diagnosa));
            if ($query->num_rows() > 0) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Diagnosa Utama Hanya Bisa Diinput Satu Kali');

                    // if permitted, do logit
                    $perms      = "master_data_dokter_ruangan_view";
                    $comments   = "Gagal menambahkan dokter Ruangan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $current = $this->Pasien_rj_model->get_current_status($pendaftaran_id);
                $ins = $this->Pasien_rj_model->insert_diagnosapasien($pendaftaran_id, $data_diagnosapasien, $current->status_periksa);
                //$ins=true;
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash'      => $this->security->get_csrf_hash(),
                        'success'       => true,
                        'messages'      => 'Diagnosa berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms      = "rawat_jalan_pasienrj_view";
                    $comments   = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                    // if permitted, do logit
                    $perms      = "rawat_jalan_pasienrj_view";
                    $comments   = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
            // end diagnosa primer
        }
        echo json_encode($res);
    }

    public function ajax_list_tindakan_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $pasien         = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
        $list           = $this->Pasien_rj_model->get_tindakan_pasien_list($pendaftaran_id);
        $data           = array();
        foreach($list as $tindakan){
            $row   = array();
            $row[] = date('d M Y', strtotime($tindakan->tgl_tindakan));
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanpasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw"              => $this->input->get('draw'),
                    "recordsTotal"      => count($list),
                    "recordsFiltered"   => count($list),
                    "data"              => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_laboratorium_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $pasien         = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
        $list           = $this->Pasien_rj_model->get_laboratorium_pasien_list($pendaftaran_id);
        $data           = array();
        foreach($list as $tindakan){
            $row   = array();
            $row[] = date('d M Y', strtotime($tindakan->tgl_tindakan));
            $row[] = $tindakan->daftartindakan_nama;
            $row[] = $tindakan->jml_tindakan;
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus tindakan" onclick="hapusTindakan('."'".$tindakan->tindakanpasien_id."'".')"><i class="fa fa-times"></i></button>';
            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw"              => $this->input->get('draw'),
                    "recordsTotal"      => count($list),
                    "recordsFiltered"   => count($list),
                    "data"              => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_tindakan_list(){
        $kelaspelayanan_id  = $this->input->get('kelaspelayanan_id',TRUE);
        $is_bpjs            = $this->input->get('type_pembayaran',TRUE);
        $jenis_pasien       = $this->input->get('jenis_pasien',TRUE);
        $type_call      = $this->input->get('type_call',TRUE);

        $jenis_pasien = $this->Pasien_rj_model->get_m_jenis_pasien_by_id($jenis_pasien);
        if (!empty($jenis_pasien)) {
            $jenis_pasien = $jenis_pasien->nama_jenis_pasien;
        }
        if ($type_call > 3) {
            $type_call = "CALL";
        }
        else {
            $type_call = "VISIT";
        }
        
        // $list_tindakan      = $this->Pasien_rj_model->get_tindakan($kelaspelayanan_id, $is_bpjs, $jenis_pasien, $type_call);
        $list_tindakan      = $this->Pasien_rj_model->get_tindakan($is_bpjs, $jenis_pasien, $type_call);
        $list               = "<option value=\"\" disabled selected>Pilih Tindakan</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list"      => $list,
            "success"   => true
        );

        echo json_encode($res);
    }

    function get_laboratorium_list(){
        $kelaspelayanan_id  = $this->input->get('kelaspelayanan_id',TRUE);
        $is_bpjs            = $this->input->get('type_pembayaran',TRUE);
        $jenis_pasien       = $this->input->get('jenis_pasien',TRUE);
        $type_call      = $this->input->get('type_call',TRUE);

        $jenis_pasien = $this->Pasien_rj_model->get_m_jenis_pasien_by_id($jenis_pasien);
        if (!empty($jenis_pasien)) {
            $jenis_pasien = $jenis_pasien->nama_jenis_pasien;
        }
        if ($type_call > 3) {
            $type_call = "CALL";
        }
        else {
            $type_call = "VISIT";
        }
        
        $list_tindakan      = $this->Pasien_rj_model->get_laboratorium($kelaspelayanan_id, $is_bpjs, $jenis_pasien, $type_call);
        
        $list               = "<option value=\"\" disabled selected>Pilih Tindakan Lab</option>";
        foreach($list_tindakan as $list_tindak){
            $list .= "<option value='".$list_tindak->daftartindakan_id."'>".$list_tindak->daftartindakan_nama."</option>";
        }

        $res = array(
            "list"      => $list,
            "success"   => true
        );

        echo json_encode($res);
    }

    function get_tarif_tindakan(){
        $daftartindakan_id  = $this->input->get('daftartindakan_id',TRUE);
        $tarif_tindakan     = $this->Pasien_rj_model->get_tarif_tindakan($daftartindakan_id);


        $res = array(
            "list"      => $tarif_tindakan,
            "success"   => true
        );

        echo json_encode($res);
    }

    public function do_create_fee_perawat() {
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Diagnosa', 'required');
        $this->form_validation->set_rules('perawat1','Fee perawat1', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        } else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = (int)$this->input->post('pendaftaran_id', TRUE);
            $kelaspelayanan_id  = $this->input->post('kelaspelayanan_id', TRUE);
            $pasien_id          = $this->input->post('id_pasien', TRUE);
            $dokter             = $this->input->post('dokter_id_tindakan', TRUE);

            $tindakan           = 99;
            $jml_tindakan       = 1;
            $perawat1           = $this->input->post('perawat1', TRUE);
            $perawat2           = $this->input->post('perawat2', TRUE);
            $perawat3           = $this->input->post('perawat3', TRUE);
            $data_pendaftaran   = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
            // echo json_encode($pendaftaran_id); die();

            if ($perawat1 == $perawat2) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Perawat tidak boleh sama.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Perawat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);

                echo json_encode($res);
                exit;
            } else if ($perawat1 == $perawat3) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Perawat tidak boleh sama.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Perawat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);

                echo json_encode($res);
                exit;
            } else if ($perawat2 == $perawat3) {
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Perawat tidak boleh sama.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Perawat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);

                echo json_encode($res);
                exit;
            } else {
                $dataPerawat = array(
                    'pasien_id'      => $pasien_id,
                    'pendaftaran_id' => $pendaftaran_id,
                    'dokter_id_1'    => $perawat1,
                    'dokter_id_2'    => $perawat2,
                    'dokter_id_3'    => $perawat3,
                    'harga'          => 0
                );

//            if (!empty($perawat2)) {
//                $dataPerawat[] = array(
//                    'pasien_id'     => $pasien_id,
//                    'dokter_id'     => $perawat2,
//                    'harga'         => 0,
//                );
//            }
//            if (!empty($perawat3)) {
//                $dataPerawat[] = array(
//                    'pasien_id'     => $pasien_id,
//                    'dokter_id'     => $perawat3,
//                    'harga'         => 0,
//                );
//            }
//            if (!empty($perawat1)) {
//                $dataPerawat[] = array(
//                    'pasien_id'     => $pasien_id,
//                    'dokter_id'     => $perawat1,
//                    'harga'         => 0,
//                );
//            }
//
//            foreach ($dataPerawat as $item) {
//                $ins = $this->Pasien_rj_model->insert_fee_dokter_perawat($item);
//            }

                $ins = $this->Pasien_rj_model->insert_fee_dokter_perawat($dataPerawat);

                // var_dump($data_tindakanpasien);die();
                //$ins=true;
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Perawat berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms = "rawat_jalan_pasienrj_view";
                    $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => 'Gagal menambahkan Perawat, hubungi web administrator.');

                    // if permitted, do logit
                    $perms = "rawat_jalan_pasienrj_view";
                    $comments = "Gagal menambahkan Perawat dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
            echo json_encode($res);
        }
    }

    public function do_create_fee_dokter(){
        // echo json_encode((int)$this->input->post('tindakan', TRUE)); die();
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Diagnosa', 'required');
        $this->form_validation->set_rules('fee_dokter','Fee Dokter', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = (int)$this->input->post('pendaftaran_id', TRUE);
            $kelaspelayanan_id  = $this->input->post('kelaspelayanan_id', TRUE);
            $pasien_id          = $this->input->post('id_pasien', TRUE);
            $dokter             = $this->input->post('dokter_id_tindakan', TRUE);
            $tindakan           = 99;
            $jml_tindakan       = 1;
            $harga_tindakan     = $this->input->post('fee_dokter', TRUE);
            $subtotal           = $this->input->post('fee_dokter', TRUE);
            $totalharga         = $this->input->post('fee_dokter', TRUE);
            $data_pendaftaran   = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
            // echo json_encode($pendaftaran_id); die();
            $data_tindakanpasien = array(
                'pasien_id'     => $pasien_id,
                'dokter_id'     => $dokter,
                'harga'         => $subtotal,
            );

            // var_dump($data_tindakanpasien);die();
            $ins = $this->Pasien_rj_model->insert_fee_dokter_perawat($data_tindakanpasien);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }
    
    public function do_create_tindakan_pasien(){
        // echo json_encode((int)$this->input->post('tindakan', TRUE)); die();
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Diagnosa', 'required');
        $this->form_validation->set_rules('tindakan','Tindakan', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan','Jumlah Tindakan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = (int)$this->input->post('pendaftaran_id', TRUE);
            $kelaspelayanan_id  = $this->input->post('kelaspelayanan_id', TRUE);
            $pasien_id          = $this->input->post('id_pasien', TRUE);
            $dokter             = $this->input->post('dokter_id_tindakan', TRUE);
            $tindakan           = $this->input->post('tindakan', TRUE);
            $jml_tindakan       = $this->input->post('jml_tindakan', TRUE);
            $harga_tindakan     = $this->input->post('harga_tindakan_satuan', TRUE);
            $subtotal           = $this->input->post('subtotal', TRUE);
            $totalharga         = $this->input->post('totalharga', TRUE);
            $data_pendaftaran   = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
            // echo json_encode($pendaftaran_id); die();
            $data_tindakanpasien = array(
                'pendaftaran_id'        => $pendaftaran_id,
                'kelaspelayanan_id'     => $kelaspelayanan_id,
                'pasien_id'             => $pasien_id,
                'daftartindakan_id'     => $tindakan,
                'jml_tindakan'          => $jml_tindakan,
                'harga_tindakan'        => $harga_tindakan,
                'total_harga_tindakan'  => $subtotal,
                'total_harga'           => $totalharga,
                'dokter_id'             => $dokter,
                'tgl_tindakan'          => date('Y-m-d'),
                'ruangan_id'            => $data_pendaftaran->poliruangan_id
            );

            // var_dump($data_tindakanpasien);die();
            $current = $this->Pasien_rj_model->get_current_status($pendaftaran_id);
            $ins = $this->Pasien_rj_model->insert_tindakanpasien($pendaftaran_id, $data_tindakanpasien, $current->status_periksa);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_create_laboratorium_pasien(){
        // echo json_encode((int)$this->input->post('tindakan', TRUE)); die();
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        //$this->form_validation->set_rules('tgl_tindakan','Tanggal Diagnosa', 'required');
        $this->form_validation->set_rules('laboratorium','Laboratorium', 'required|trim');
        $this->form_validation->set_rules('jml_tindakan_lab','Jumlah Tindakan', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input tindakan pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = (int)$this->input->post('pendaftaran_id', TRUE);
            $kelaspelayanan_id  = $this->input->post('kelaspelayanan_id', TRUE);
            $pasien_id          = $this->input->post('id_pasien', TRUE);
            $dokter             = $this->input->post('dokter_id_tindakan', TRUE);
            $tindakan           = $this->input->post('laboratorium', TRUE);
            $jml_tindakan       = $this->input->post('jml_tindakan_lab', TRUE);
            $harga_tindakan     = $this->input->post('harga_tindakan_satuan_lab', TRUE);
            $subtotal           = $this->input->post('subtotal_lab', TRUE);
            $totalharga         = $this->input->post('totalharga_lab', TRUE);
            $data_pendaftaran   = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
            // echo json_encode($pendaftaran_id); die();
            $data_tindakanpasien = array(
                'pendaftaran_id'        => $pendaftaran_id,
                'kelaspelayanan_id'     => $kelaspelayanan_id,
                'pasien_id'             => $pasien_id,
                'daftartindakan_id'     => $tindakan,
                'jml_tindakan'          => $jml_tindakan,
                'harga_tindakan'        => $harga_tindakan,
                'total_harga_tindakan'  => $subtotal,
                'total_harga'           => $totalharga,
                'dokter_id'             => $dokter,
                'tgl_tindakan'          => date('Y-m-d'),
                'ruangan_id'            => $data_pendaftaran->poliruangan_id
            );

            // var_dump($data_tindakanpasien);die();
            $current = $this->Pasien_rj_model->get_current_status($pendaftaran_id);
            $ins = $this->Pasien_rj_model->insert_tindakanpasien($pendaftaran_id, $data_tindakanpasien, $current->status_periksa);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Tindakan berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Tindakan, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

    public function do_delete_diagnosa(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $diagnosapasien_id = $this->input->post('diagnosapasien_id', TRUE);

        $delete = $this->Pasien_rj_model->delete_diagnosa($diagnosapasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $diagnosapasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_all_kspr(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
        $date = date('Y-m-d');


        $delete = $this->Pasien_rj_model->delete_all_kspr($pendaftaran_id, $date);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_kspr(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_skor_kspr = $this->input->post('id_skor_kspr', TRUE);
        // $date = date('Y-m-d');


        $delete = $this->Pasien_rj_model->delete_kspr($id_skor_kspr);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $id_skor_kspr ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $id_skor_kspr ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_kebidanan(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pemeriksaan_obgyn_id = $this->input->post('pemeriksaan_obgyn_id', TRUE);

        $delete = $this->Pasien_rj_model->delete_kebidanan($pemeriksaan_obgyn_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $pemeriksaan_obgyn_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $pemeriksaan_obgyn_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }


    public function do_delete_pemfisik(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $id_detail_pemeriksaan = $this->input->post('id_detail_pemeriksaan', TRUE);

        $delete = $this->Pasien_rj_model->delete_pemfisik($id_detail_pemeriksaan);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Diagnosa Pasien dengan id Diagnosa Pasien Operasi = '". $id_detail_pemeriksaan ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Diagnosa Pasien dengan ID = '". $id_detail_pemeriksaan ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }


    public function do_delete_imun(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pasienimunisasi_id = $this->input->post('pasienimunisasi_id', TRUE);

        $delete = $this->Pasien_rj_model->delete_imun($pasienimunisasi_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Imun Pasien dengan id Diagnosa Pasien Operasi = '". $pasienimunisasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Imun Pasien dengan ID = '". $pasienimunisasi_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }


    public function do_delete_pnc(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakanpasien_id = $this->input->post('tindakanpasien_id', TRUE);

        $delete = $this->Pasien_rj_model->delete_pnc($tindakanpasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Imun Pasien dengan id Diagnosa Pasien Operasi = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Imun Pasien dengan ID = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_kb(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pasienkb_id = $this->input->post('pasienkb_id', TRUE);

        $delete = $this->Pasien_rj_model->delete_kb($pasienkb_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Diagnosa Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Imun Pasien dengan id Diagnosa Pasien Operasi = '". $pasienkb_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Imun Pasien dengan ID = '". $pasienkb_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_delete_tindakan(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $tindakanpasien_id = $this->input->post('tindakanpasien_id', TRUE);

        $delete = $this->Pasien_rj_model->delete_tindakan($tindakanpasien_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Tindakan Pasien dengan id Tindakan Pasien Operasi = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Tindakan Pasien dengan ID = '". $tindakanpasien_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function ajax_resep () {
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);

        @$list = $this->Models->where("rawatjalan_reseptur_obat", array('pendaftaran_id' => $pendaftaran_id));
        $obat = $this->Models->all("farmasi_obat");
        $batch = $this->Models->all("farmasi_batch");
        $jenis = $this->Models->all("farmasi_jenis_barang");
        $persediaan = $this->Models->all("farmasi_persediaan");
        $dosis = $this->Models->all("farmasi_dosis");
        $pemberian = $this->Models->all("farmasi_pemberian");
        $aturan_penggunaan = $this->Models->all("farmasi_aturan_penggunaan");
        @$penjualan = $this->Models->all("rawatjalan_penjualan_obat");
        $pasien = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);

        $data = array();
        foreach($list as $key => $value){
            $row = array();
            $row[] = $batch[$value['batch_id']]['number'];
            $row[] = $obat[$value['obat_id']]['nama'];
            $row[] = $jenis[$value['jenis_barang_id']]['nama'];
            $row[] = $persediaan[$value['persediaan_id']]['persediaan_3'];
            $row[] = $value['jumlah_obat'];
            $row[] = $value['dosis_id'] != 0 ? ($dosis[$value['dosis_id']]['konten'] ? $dosis[$value['dosis_id']]['konten'] : $value['custom_dosis']) : ($value['custom_dosis'] != "" ? $value['custom_dosis'] : "-");
            $row[] = $value['pemberian_id'] != 0 ? ($pemberian[$value['pemberian_id']]['konten'] ? $pemberian[$value['pemberian_id']]['konten'] : $value['custom_pemberian']) : ($value['custom_pemberian'] != "" ? $value['custom_pemberian'] : "-");
            $row[] = $value['aturan_penggunaan_id'] != 0 ? ($aturan_penggunaan[$value['aturan_penggunaan_id']]['konten'] ? $aturan_penggunaan[$value['aturan_penggunaan_id']]['konten'] : $value['custom_aturan_penggunaan']) : ($value['custom_aturan_penggunaan'] != "" ? $value['custom_aturan_penggunaan'] : "-");
            $row[] = $penjualan[$value['penjualan_id']]['harga_total'];
            if($pasien->status_periksa != 'SUDAH BAYAR') {
                $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus Resep" onclick="hapusResep('."'".$value['id']."'".')"><i class="fa fa-times"></i></button>
                          <button class="btn btn-warning btn-circle" title="klik untuk Print Sticker"  onclick="printStickerResep('."'".$value['id']."'".')"><i class="fa fa-print"></i></button>';
            }

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->get('draw'),
            "recordsTotal" => count($list),
            "recordsFiltered" => count($list),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function ajax_list_resep_pasien(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $pasien         = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
        $list = $this->Pasien_rj_model->get_resep_pasien_list($pendaftaran_id);
        $data = array();
        foreach($list as $resep){
            $tgl_ = date_create($resep->tgl_reseptur);
            $row = array();
            $row[] = date_format($tgl_, 'd-M-Y');
            $row[] = $resep->nama_barang;
            $row[] = $resep->nama_jenis;
            $row[] = $resep->nama_sediaan;
            $row[] = $resep->qty;
            $row[] = $resep->info_pemakaian_obat;
            if( $resep->is_bpjs == 1){
                $row[] = "BPJS";
            }else{
                $row[] = "NON BPJS";
            }
            $row[] = $resep->harga_jual;
            // $row[] = $resep->type_pembayaran;
            // $row[] = $resep->nama_asuransi;
            // $row[] = $resep->no_asuransi;

            // $row[] = $resep->signa;obat
            if($pasien->status_periksa == 'SUDAH BAYAR'){}
            else{
            $row[] = '<button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus Resep" onclick="hapusResep('."'".$resep->reseptur_id."','".$resep->tgl_reseptur."'".')"><i class="fa fa-times"></i></button>';
            }

            //add html for action
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function autocomplete_obat(){
        $val_obat = $this->input->get('val_obat');
        if(empty($val_obat)) {
            $res = array(
                'success' => false,
                'messages' => "Tidak ada Obat dipilih"
                );
        }
        $obat_list = $this->Pasien_rj_model->get_obat_autocomplete($val_obat);
        if(count($obat_list) > 0) {
            // $parent = $qry->row(1);
            for ($i=0; $i<count($obat_list); $i++){
                $list[] = array(
                    "id" => $obat_list[$i]->obat_id,
                    "value" => $obat_list[$i]->nama_obat,
                    "deskripsi" => $obat_list[$i]->jenisoa_nama,
                    "satuan" => $obat_list[$i]->satuan,
                    "harga_netto" => $obat_list[$i]->harga_modal,
                    "harga_jual" => $obat_list[$i]->harga_jual,
                    "current_stok" => $obat_list[$i]->qtystok
                );
            }

            $res = array(
                'success' => true,
                'messages' => "Obat ditemukan",
                'data' => $list
                );
        }else{
            $res = array(
                'success' => false,
                'messages' => "Obat Not Found"
                );
        }
        echo json_encode($res);
    }

    public function do_create_resep_pasien(){
//        echo "<pre>";
//        print_r($_POST);
//        exit;

        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('obat_id','Nama Obat', 'required|trim');
        $this->form_validation->set_rules('jumlah_obat','Jumlah Obat', 'required');
        $this->form_validation->set_rules('harga_jual','Harga Obat', 'required');
//        $this->form_validation->set_rules('aturan_pakai_id','Aturan Pakai', 'required');
//        $this->form_validation->set_rules('dosis_id','Dosis', 'required');
//        $this->form_validation->set_rules('pemberian_id','Pemberian', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input resep pasien dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        } else if ($this->form_validation->run() == TRUE)  {
            $stok_gudang_farmasi_id      = $this->input->post('stok_id', TRUE);
            $pasien_id                   = $this->input->post('pasien_id', TRUE);
            $pendaftaran_id              = $this->input->post('pendaftaran_id', TRUE);
            $obat_id                     = $this->input->post('obat_id', TRUE);
            $jenis_barang_id             = $this->input->post('jenis_barang_id', TRUE);
            $persediaan_id               = $this->input->post('persediaan_id', TRUE);
            $aturan_penggunaan_id        = $this->input->post('aturan_pakai_id', TRUE);
            $dosis_id                    = $this->input->post('dosis_id', TRUE);
            $pemberian_id                = $this->input->post('pemberian_id', TRUE);
            $harga_satuan                = $this->input->post('harga', TRUE);
            $harga_total                 = $this->input->post('harga_jual', TRUE);
            $jumlah_obat                 = $this->input->post('jumlah_obat', TRUE);
            $dokter_nama                 = $this->input->post('dokter', TRUE);
            $apotek_luar                 = $this->input->post('apotek_luar', TRUE);
            $custom_dosis                = $this->input->post('custom_dosis', TRUE) ? 1 : 0;
            $custom_pemberian            = $this->input->post('custom_pemberian', TRUE) ? 1 : 0;
            $custom_aturan_penggunaan    = $this->input->post('custom_aturan_pakai', TRUE) ? 1 : 0;
            $custom_dosis_id             = $this->input->post('custom_dosis_id', TRUE);
            $custom_pemberian_id         = $this->input->post('custom_pemberian_id', TRUE);
            $custom_aturan_penggunaan_id = $this->input->post('custom_aturan_pakai_id', TRUE);

            $stok_gudang_farmasi = $this->Models->show("logistik_stok_gudang_farmasi", $stok_gudang_farmasi_id);
            $gudang_farmasi      = $this->Models->show("logistik_gudang_farmasi", $stok_gudang_farmasi['gudang_farmasi_id']);
            $gudang_utama        = $this->Models->show("logistik_gudang_utama", $gudang_farmasi['gudang_utama_id']);

            $data_cek = array (
                'obat_id'        => $obat_id,
                'pasien_id'      => $pasien_id,
                'pendaftaran_id' => $pendaftaran_id
            );

            $data_penjualan = array (
                'stok_gudang_farmasi_id' => $stok_gudang_farmasi_id,
                'obat_id'                => $obat_id,
                'batch_id'               => $gudang_utama['batch_id'],
                'suplier_id'             => $gudang_utama['suplier_id'],
                'pasien_id'              => $pasien_id,
                'pendaftaran_id'         => $pendaftaran_id,
                'jumlah_obat'            => $jumlah_obat,
                'harga_satuan'           => $harga_satuan,
                'harga_total'            => $harga_total,
                'diskon'                 => 0,
                'cara_bayar'             => '',
                'pembayaran_id'          => 0,
                'status_bayar'           => 0
            );

            if (count($this->Models->where("rawatjalan_penjualan_obat", $data_cek)) < 1) {
                if ($this->Models->insert("rawatjalan_penjualan_obat", $data_penjualan)) {
                    $penjualan_id = $this->db->insert_id();

                    $data_resep = array (
                        'stok_gudang_farmasi_id'   => $stok_gudang_farmasi_id,
                        'pasien_id'                => $pasien_id,
                        'pendaftaran_id'           => $pendaftaran_id,
                        'obat_id'                  => $obat_id,
                        'batch_id'                 => $gudang_utama['batch_id'],
                        'suplier_id'               => $gudang_utama['suplier_id'],
                        'persediaan_id'            => $persediaan_id,
                        'jenis_barang_id'          => $jenis_barang_id,
                        'dosis_id'                 => $custom_dosis == 1 ? "" : $dosis_id,
                        'pemberian_id'             => $custom_pemberian == 1 ? "" : $pemberian_id,
                        'aturan_penggunaan_id'     => $custom_aturan_penggunaan == 1 ? "" : $aturan_penggunaan_id,
                        'jumlah_obat'              => $jumlah_obat,
                        'penjualan_id'             => $penjualan_id,
                        'apotek_luar'              => $apotek_luar == null ? 0 : 1,
                        'dokter_nama'              => $dokter_nama,
                        'custom_dosis'             => $custom_dosis == 1 ? $custom_dosis_id : "",
                        'custom_pemberian'         => $custom_pemberian == 1 ? $custom_pemberian_id : "",
                        'custom_aturan_penggunaan' => $custom_aturan_penggunaan == 1 ? $custom_aturan_penggunaan_id : ""
                    );

                    if ($this->Models->insert("rawatjalan_reseptur_obat", $data_resep)) {
                        $data_stok = array (
                            'stok_keluar' => $stok_gudang_farmasi['stok_keluar'] + $jumlah_obat,
                            'stok_akhir'  => $stok_gudang_farmasi['stok_akhir'] - $jumlah_obat
                        );

                        if ($this->Models->update("logistik_stok_gudang_farmasi", $data_stok, $stok_gudang_farmasi_id)) {
                            $res = array(
                                'csrfTokenName' => $this->security->get_csrf_token_name(),
                                'csrfHash' => $this->security->get_csrf_hash(),
                                'success' => true,
                                'messages' => 'Tindakan berhasil ditambahkan'
                            );

                            // if permitted, do logit
                            $perms = "rawat_jalan_pasienrj_view";
                            $comments = "Berhasil menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                            $this->aauth->logit($perms, current_url(), $comments);
                        } else {
                            $res = array(
                                'csrfTokenName' => $this->security->get_csrf_token_name(),
                                'csrfHash' => $this->security->get_csrf_hash(),
                                'success' => false,
                                'messages' => "Gagal Mengupdate stok obat");

                            // if permitted, do logit
                            $perms = "rawat_jalan_pasienrj_view";
                            $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                        }
                    } else {
                        $res = array(
                            'csrfTokenName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => false,
                            'messages' => "Gagal Menambahkan resep");

                        // if permitted, do logit
                        $perms = "rawat_jalan_pasienrj_view";
                        $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    }
                } else {
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => "Gagal Menambahkan penjualan");

                    // if permitted, do logit
                    $perms = "rawat_jalan_pasienrj_view";
                    $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
                }
            } else {
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => "Obat ini sudah Anda beli, silahkan hapus terlebih dahulu jika terjadi kesalahan inputan !");

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Tindakan dengan data berikut = '". json_encode($_REQUEST) ."'.";
            }
        }

        echo json_encode($res);
    }

    public function delete_resep() {
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $resep_id = $this->input->post('id', TRUE);
        $resep = $this->Models->show("rawatjalan_reseptur_obat", $resep_id);
        $penjualan = $this->Models->show("rawatjalan_penjualan_obat", $resep['penjualan_id']);
        $stok = $this->Models->show("logistik_stok_gudang_farmasi", $penjualan['stok_gudang_farmasi_id']);

        $this->db->trans_begin();

        if ($this->Models->delete("rawatjalan_penjualan_obat", $penjualan['id'])) {
            if ($this->Models->delete("rawatjalan_reseptur_obat", $resep['id'])) {
                $data_stok = array(
                    'stok_keluar' => $stok['stok_keluar'] - $resep['jumlah_obat'],
                    'stok_akhir'  => $stok['stok_akhir'] + $resep['jumlah_obat']
                );

                if ($this->Models->update("logistik_stok_gudang_farmasi", $data_stok, $stok['id'])) {
                    $this->db->trans_commit();
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => true,
                        'messages' => 'Resep Pasien berhasil dihapus'
                    );
                    // if permitted, do logit
                    $perms = "rawat_jalan_pasienrj_view";
                    $comments = "Berhasil menghapus Resep Pasien dengan id Resep Pasien = '". $resep['id'] ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                } else {
                    $this->db->trans_rollback();
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => false,
                        'messages' => 'Gagal mengupdate data stok, silakan hubungi web administrator.'
                    );
                    // if permitted, do logit
                    $perms = "rawat_jalan_pasienrj_view";
                    $comments = "Gagal mengupdate data stok dengan ID = '". $stok['id'] ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            } else {
                $this->db->trans_rollback();
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal menghapus reseptur, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menghapus reseptur dengan ID = '".$resep['id'] ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        } else {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus penjualan, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus penjualan dengan ID = '".$penjualan['id'] ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }

    public function do_delete_resep(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        //start transaction
        $this->db->trans_begin();

        $reseptur_id = $this->input->post('reseptur_id', TRUE);
        $tgl_reseptur = $this->input->post('tgl_reseptur', TRUE);

        $delete = $this->Pasien_rj_model->delete_resep($reseptur_id, $tgl_reseptur);
        if($delete){
            $this->Pasien_rj_model->delete_stok_resep($reseptur_id);
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menghapus data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal menghapus data Resep Pasien dengan ID = '". $reseptur_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $this->db->trans_commit();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Resep Pasien berhasil dihapus'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil menghapus Resep Pasien dengan id Resep Pasien = '". $reseptur_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function do_batal_periksa(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $delete = $this->Pasien_rj_model->delete_periksa($pendaftaran_id);
        if($delete){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Tindakan Pasien berhasil di batalkan'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil membatalkan Tindakan Pasien dengan id  = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal membatalkan data, silakan hubungi web administrator.'
            );
            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal membatalkan data Tindakan Pasien dengan ID = '". $pendaftaran_id ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }
        echo json_encode($res);
    }

    public function kirim_ke_penunjang(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('pendaftaran_id','Pendaftaran', 'required');
        $this->form_validation->set_rules('penunjang_id','Penunjang', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal mengirim pasien ke penunjang dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $penunjang_id = $this->input->post('penunjang_id', TRUE);
            $last_pendaftaran = $this->Pasien_rj_model->get_last_pendaftaran($pendaftaran_id);

            if(count($last_pendaftaran) > 0){
                $datapenunjang['kelaspelayanan_id'] = $last_pendaftaran->kelaspelayanan_id;
                $datapenunjang['pendaftaran_id'] = $last_pendaftaran->pendaftaran_id;
                $datapenunjang['pasien_id'] = $last_pendaftaran->pasien_id;
                $datapenunjang['no_masukpenunjang'] = $last_pendaftaran->no_pendaftaran;
                $datapenunjang['tgl_masukpenunjang'] = date('Y-m-d H:i:s');
                $datapenunjang['statusperiksa'] = "ANTRIAN";
                $datapenunjang['poli_ruangan_asal'] = $last_pendaftaran->poliruangan_id;
                if($penunjang_id == 6){
                    $keberadaan = "RADIOLOGI";
                }else{
                    $keberadaan = "LABORATORIUM";
                }
                $datapenunjang['poli_ruangan_id']   = $penunjang_id;
                $datapenunjang['status_keberadaan'] = $keberadaan;
                $datapenunjang['dokterpengirim_id'] = $last_pendaftaran->dokter_id;
                $datapenunjang['create_time'] = date('Y-m-d H:i:s');
                $datapenunjang['create_by'] = $this->data['users']->id;
                $insert_penunjang = $this->Pasien_rj_model->insert_penunjang($datapenunjang);
            }

            if(isset($insert_penunjang)){
                $this->Pasien_rj_model->update_to_penunjang($pendaftaran_id);
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Pasien Berhasil dikirim ke penunjang'
                );
                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Pasien Berhasil dikirim ke penunjang";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Gagal mengirim pasien ke penunjang, silakan hubungi web administrator.'
                );
                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal mengirim pasien ke penunjang";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }

        echo json_encode($res);
    }

    function convert_stok ($stok, $p2, $p3) {
        return array(
            'p1' => @bcdiv(($stok / ($p2 * $p3)), 1, 0),
            'p2' => @bcdiv(($stok % ($p2 * $p3) / $p3), 1, 0),
            'p3' => @$stok % $p3
        );
    }

    public function get_detail_stok() {
        $stok_id = $this->input->get("id");
        $data['stok'] = $this->Models->show("logistik_stok_gudang_farmasi", $stok_id);
        $data['obat'] = $this->Models->show("farmasi_obat", $data['stok']['obat_id']);
        $data['persediaan'] = $this->Models->show("farmasi_persediaan", $data['obat']['persediaan_id']);

        $output = array(
            "success" => true,
            "message" => "Data Detail Stok Gudang Farmasi ",
            "data" => $data
        );

        //output to json format
        echo json_encode($output);
    }

	public function ajax_list_obat(){
        $query = "SELECT
                    a.id AS id,
                    c.number AS batch_number,
                    b.nama AS nama_obat,
                    f.nama AS jenis_obat,
                    g.persediaan_1 AS persediaan_1,
                    g.persediaan_2 AS persediaan_2,
                    g.persediaan_3 AS persediaan_3,
                    b.jumlah_persediaan_1 AS jumlah_persediaan_1,
                    b.jumlah_persediaan_2 AS jumlah_persediaan_2,
                    b.jumlah_persediaan_3 AS jumlah_persediaan_3,
                    h.nama AS nama_suplier,
                    SUM(a.stok_akhir) AS stok_akhir,
                    c.exp_date AS exp_date
                FROM
                    logistik_stok_gudang_farmasi a,
                    farmasi_obat b,
                    farmasi_batch c,
                    logistik_gudang_farmasi d,
                    logistik_gudang_utama e,
                    farmasi_jenis_barang f,
                    farmasi_persediaan g,
                    farmasi_suplier h
                WHERE
                    a.branch = '".$this->session->tempdata('data_session')[2]."' 
                    AND a.obat_id = b.id 
                    AND c.obat_id = b.id 
                    AND a.gudang_farmasi_id = d.id 
                    AND d.gudang_utama_id = e.id 
                    AND c.id = e.batch_id 
                    AND b.jenis_barang_id = f.id 
                    AND b.persediaan_id = g.id
                    AND e.suplier_id = h.id
                    AND a.stok_akhir != 0 
                GROUP BY
                    c.id 
                ORDER BY
                    c.exp_date ASC;";
		$list = $this->Models->query($query);
		$data = array();

		foreach($list as $key => $value){
		    $stok = $this->convert_stok($value['stok_akhir'], $value['jumlah_persediaan_2'], $value['jumlah_persediaan_3']);
            $p_stok_akhir =
                " - " . $stok['p1'] . " " . $value['persediaan_1'] . "<br>" .
                " - " . $stok['p2'] . " " . $value['persediaan_2'] . "<br>" .
                " - " . $stok['p3'] . " " . $value['persediaan_3'];

			$row = array();
			$row[] = $value['batch_number'];
			$row[] = $value['nama_obat'];
			$row[] = $value['jenis_obat'];
            $row[] = $p_stok_akhir;
			$row[] = $value['nama_suplier'];
			$row[] = date('d - m - Y', strtotime($value['exp_date']));
			$row[] = '<button class="btn btn-info btn-circle" onclick="pilihObat('."'".$value['id']."'".')"><i class="fa fa-check"></i></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" => count($list),
			"recordsFiltered" => count($list),
			"data" => $data,
		);

        //output to json format
        echo json_encode($output);
	}

	public function ajax_get_obat_by_id(){
		$kode_barang = $this->input->get('kode_barang',TRUE);

		$data_barang = $this->Pasien_rj_model->get_obat_by_id($kode_barang);
		if (sizeof($data_barang)>0){
			$barang = $data_barang[0];

			$res = array(
				"success" => true,
				"messages" => "Data obat ditemukan",
				"data" => $barang
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data obat tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
    }

    public function ajax_list_diagnosa(){
        $pendaftaran_id   = $this->input->get('pendaftaran_id',TRUE);
        // die($pendaftaran_id);
		$data_diagnosa    = $this->Pasien_rj_model->get_list_diagnosa();
        $pasien           = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
		$data = array();
		foreach($data_diagnosa as $diagnosa){
			$row = array();
			$row[] = $diagnosa->diagnosa_kode;
			$row[] = $diagnosa->diagnosa_nama;
			$row[] = $diagnosa->kode_diagnosa;
			$row[] = $diagnosa->nama_diagnosa;
            if($pasien->status_periksa == "SUDAH BAYAR"){}
            else{
			     $row[] = '<button class="btn btn-info btn-circle" onclick="pilihDiagnosa('."'".$diagnosa->diagnosa2_id."'".')"><i class="fa fa-check"></i></button>';
            }

			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->get('draw'),
			"recordsTotal" => $this->Pasien_rj_model->count_list_diagnosa(),
			"recordsFiltered" => $this->Pasien_rj_model->count_list_filtered_diagnosa(),
			"data" => $data,
		);
        //output to json format
        echo json_encode($output);
	}

	public function ajax_get_diagnosa_by_id(){
		$diagnosa2_id = $this->input->get('diagnosa2_id',TRUE);

		$data_diagnosa = $this->Pasien_rj_model->get_diagnosa_by_id($diagnosa2_id);
		if (sizeof($data_diagnosa)>0){
			$diagnosa = $data_diagnosa[0];

			$res = array(
				"success" => true,
				"messages" => "Diagnosa obat ditemukan",
				"data" => $diagnosa
			);
		} else {
			$res = array(
				"success" => false,
				"messages" => "Data Diagnosa tidak ditemukan",
				"data" => null
			);
		}
		echo json_encode($res);
	}


    public function kirim_ke_igd(){

        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }
        $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);

        $data = array(
            'instalasi_id' => 2,
            'status_pasien' => 1,
            'tgl_update' => date('Y-m-d H:i:s'),
            'tgl_pendaftaran' => date('Y-m-d H:i:s')
        );

        $update = $this->Pasien_rj_model->update_to_igd($data, $pendaftaran_id);
        //$ins=true;
        if($update){
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true,
                'messages' => 'Pasien berhasil dipindahkan ke IGD'
            );

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Berhasil memindahakan pasien ke IGD  dengan data berikut = '". json_encode($_REQUEST) ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else{
            $res = array(
            'csrfTokenName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash(),
            'success' => false,
            'messages' => 'Gagal mengirim pasien ke IGD, hubungi web administrator.');

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal mengirim pasien ke IGD dengan data berikut = '". json_encode($_REQUEST) ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }

        echo json_encode($res);
    }

    public function print_data($pasien_id,$view){
        $datapasien = $this->Pasien_rj_model->get_data_print($pasien_id);
        $print['data_pasien'] = $datapasien;
        // echo "<pre>";
        // print_r($print);die();
        // echo "</pre>";

        $print['barcode'] = "<img alt='barcode' width='500' height='500' src='".base_url()."/rawatjalan/pasien_rj/create_barcode/".$datapasien->no_rekam_medis."'>";


        $this->load->view('rekam_medis/'.$view,$print);
    }

    public function print_sticker () {
        $resep_id = $this->input->get('id', TRUE);

        $data['resep'] = $this->Models->show("rawatjalan_reseptur_obat", $resep_id);
        $data['penjualan'] = $this->Models->show("rawatjalan_penjualan_obat", $data['resep']['penjualan_id']);
        $data['stok'] = $this->Models->show("logistik_stok_gudang_farmasi", $data['penjualan']['stok_gudang_farmasi_id']);
        $data['pasien'] = $this->Models->where("m_pasien", array('pasien_id' => $data['resep']['pasien_id']))[0];
        $data['obat'] = $this->Models->show("farmasi_obat", $data['resep']['obat_id']);
        $data['dosis'] = $this->Models->show("farmasi_dosis", $data['resep']['dosis_id']);
        $data['aturan_penggunaan'] = $this->Models->show("farmasi_aturan_penggunaan", $data['resep']['aturan_penggunaan_id']);
        $data['persediaan'] = $this->Models->show("farmasi_persediaan", $data['resep']['persediaan_id']);

        $this->load->view('print_sticker', $data);
    }

    public function create_barcode($no_rm){
        //load library
        $this->load->library('Zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');

        $barcodeOptions = array(
            'text' => $no_rm,
            'barHeight'=> 74,
            'factor'=>3.98,
        );
        //generate barcode
        Zend_Barcode::render('code128', 'image', $barcodeOptions, array());

    }

    public function do_create_pemeriksaan_fisik(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('bb', 'bb', 'required');
        $this->form_validation->set_rules('tb', 'tb', 'required');
        $this->form_validation->set_rules('sistole', 'sistole', 'required');
        $this->form_validation->set_rules('diastole', 'diastole', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input Bahasa dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {


            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $bb          = $this->input->post('bb', TRUE);
            $tb          = $this->input->post('tb', TRUE);
            $sistole     = $this->input->post('sistole', TRUE);
            $diastole    = $this->input->post('diastole', TRUE);
            $anamnesis   = $this->input->post('anamnesis', TRUE);
            $keterangan  = $this->input->post('keterangan', TRUE);


            $data_pemeriksaan_fisik = array(
                'pendaftaran_id'     => $pendaftaran_id,
                'pasien_id'     => $pasien_id,
                'tgl_periksa'   => date('Y-m-d'),
                'bb'            => $bb,
                'tb'            => $tb,
                'sistole'       => $sistole,
                'diastole'      => $diastole,
                'anamnesis'     => $anamnesis,
                'keterangan'    => $keterangan
            );

            $current = $this->Pasien_rj_model->get_current_status($pendaftaran_id);
            $ins = $this->Pasien_rj_model->insert_pemeriksaan_fisik($pendaftaran_id, $data_pemeriksaan_fisik, $current->status_periksa);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Bahasa berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Bahasa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Bahasa, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan bahasa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_create_kb(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipe_kb', 'Type KB', 'required');
        $this->form_validation->set_rules('tindakan_kb', 'Tindakan KB', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input asd dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id          = $this->input->post('pasien_id', TRUE);
            $kelaspelayanan_id  = $this->input->post('kelaspelayanan_id', TRUE);
            $dokter             = $this->input->post('dokter_id_kb', TRUE);
            $tipe_kb            = $this->input->post('tipe_kb', TRUE);
            $tindakan_kb        = $this->input->post('tindakan_kb', TRUE);
            $data_pendaftaran   = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);

            // var_dump($ujang);die();
            if($tipe_kb == 1){
                $tipe_kb = "Prenatal";
                $tindakan = 1;
            }else if($tipe_kb == 2){
                $tindakan = 2;
                $tipe_kb = "Pasca Persalinan";
            }else if($tipe_kb == 3){
                $tindakan = 3;
                $tipe_kb = "Pasca Keguguran";
            }else{
                $tipe_kb = "Tidak Valid";
            }

            // $tindakan_ke        = $this->Pasien_rj_model->get_tindakan_ke($pasien_id);
            // $tindakan           = $tindakan_ke;
            // $tindakan++;
            // foreach ($tindakan_ke as $list) {
            //     $tindakan = $list->tindakan_max;
            // }
            // $tindakan++;
            $data_kb = array(
                'pendaftaran_id'    => $pendaftaran_id,
                'pasien_id'         => $pasien_id,
                'kelaspelayanan_id' => $kelaspelayanan_id,
                'dokter_id'         => $dokter,
                'tanggal_tindakan'  => date('Y-m-d H:i:s'),
                'vaksinasi_id'      => $tindakan_kb,
                'tipe_kb'           => $tipe_kb,
                'ruangan_id'        => $data_pendaftaran->poliruangan_id,
                'tindakan_ke'       => $tindakan,
            );


            $ins = $this->Pasien_rj_model->insert_kb($data_kb);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'asd berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan asd dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan asd, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan asd dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_create_skorKSPR(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('sum_kspr', 'sum_kspr', 'required');
        // $this->form_validation->set_rules('tindakan_kb', 'Tindakan KB', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input asd dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id     = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id          = $this->input->post('pasien_id', TRUE);
            $kspr_id            = $this->input->post('kspr', TRUE);
            $jml                = $this->input->post('jml', TRUE);
            $skor_kspr          = $this->input->post('skor_kspr', TRUE);
            $nama_kspr          = $this->input->post('nama_kspr', TRUE);
            // $data_pendaftaran   = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);

            // var_dump($ujang);die();
            $data_kspr = array(
                'pendaftaran_id'    => $pendaftaran_id,
                'pasien_id'         => $pasien_id,
                'tanggal'           => date('Y-m-d'),
                'kspr_id'           => $kspr_id,
                'jml'               => $jml,
                'skor_kspr'         => $skor_kspr,
                'nama_kspr'         => $nama_kspr,
                'total'             => $jml * $skor_kspr
            );
            
            $ins = $this->Pasien_rj_model->insert_skor_kspr($data_kspr);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'asd berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan asd dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan asd, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan asd dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }

     public function ajax_list_pemeriksaan_fisik(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rj_model->get_pemeriksaan_fisik_list($pendaftaran_id);
        $pasien = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pem_fisik){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d M Y', strtotime($pem_fisik->tgl_periksa));
            $row[] = $pem_fisik->bb.' KG';
            $row[] = $pem_fisik->tb.' CM';
            $row[] = $pem_fisik->sistole.' mm.Hg';
            $row[] = $pem_fisik->diastole.' mm.Hg';
            $row[] = $pem_fisik->anamnesis;
            $row[] = $pem_fisik->keterangan;
            //add html for action
            if($pasien->status_periksa == "SUDAH BAYAR"){}
            else{
            $row[] = '
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus PemFisik" id="hapusPemFisik" onclick="hapusPemFisik('."'".$pem_fisik->id_detail_pemeriksaan."'".')"><i class="fa fa-times"></i></button>
                      ';
            }
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_rj_model->count_pemeriksaan_fisik_all(),
                    "recordsFiltered" => $this->Pasien_rj_model->count_pemeriksaan_fisik_all(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_list_skor_kspr(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rj_model->get_skor_kspr_list($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kspr){
            $no++;
            $row = array();
            $row[] = $no;
            // $row[] = date('d M Y', strtotime($pem_fisik->tgl_periksa));
            $row[] = $kspr->kspr_nama;
            $row[] = $kspr->skor_kspr;
            $row[] = $kspr->jml;
            $row[] = $kspr->total;
            //add html for action
            $row[] = '
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus PemFisik" id="hapusPemFisik" onclick="hapusKSPR('."'".$kspr->id_skor_kspr."'".')"><i class="fa fa-times"></i></button>
                      ';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_rj_model->count_skor_kspr_all(),
                    "recordsFiltered" => $this->Pasien_rj_model->count_skor_kspr_all(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_list_faktor_resiko(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rj_model->get_faktor_resiko($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kspr){
            $no++;
            $row = array();
            $row[] = $kspr->faktor;
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => count($list),
                    "recordsFiltered" => count($list),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function get_dokter_poli(){
        $poliruangan_id = $this->input->get('poliruangan_id',TRUE);
        $list_dokter_poli = $this->Pasien_rj_model->get_dokter_poli($poliruangan_id);
        $list = "<option value=\"\" disabled selected>Pilih Dokter</option>";
        foreach($list_dokter_poli as $list_dok){
            $list .= "<option value='".$list_dok->id_M_DOKTER."'>".$list_dok->NAME_DOKTER."</option>";
        }

        $res = array(
            "list" => $list,
            "success" => true
        );

        echo json_encode($res);
    }


    public function do_create_pemeriksaan_kebidanan(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('jum_skor', 'jumlah skor kspr', 'required');
        // $this->form_validation->set_rules('resiko_keterangan', 'Faktor Resiko', 'required');
        // $this->form_validation->set_rules('vaksinasi_nonhamil', 'vaksinasi non hamil', 'required');
        // $this->form_validation->set_rules('vaksinasi_hamil', 'vaksinasi hamil', 'required');
        $this->form_validation->set_rules('usia_kehamilan', 'usia kehamilan', 'required');
        // $this->form_validation->set_rules('anc_g', 'Status Kehamilan G', 'required');
        // $this->form_validation->set_rules('anc_pa', 'Status Kehamilan Pa', 'required');
        // $this->form_validation->set_rules('anc_p', 'Status Kehamilan P', 'required');
        // $this->form_validation->set_rules('anc_i', 'Status Kehamilan i', 'required');
        // $this->form_validation->set_rules('anc_a', 'Status Kehamilan a', 'required');
        // $this->form_validation->set_rules('anc_h', 'Status Kehamilan h', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash'      => $this->security->get_csrf_hash(),
                'success'       => false,
                'messages'      => $error);

            // if permitted, do logit
            $perms      = "rawat_jalan_pasienrj_view";
            $comments   = "Gagal input Bahasa dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {

            $pendaftaran_id     = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id          = $this->input->post('pasien_id', TRUE);
            $dokter             = $this->input->post('dokter_id_bidan', TRUE);
            $vaksinasi_hamil    = $this->input->post('vaksinasi_hamil', TRUE);
            $vaksinasi_nonhamil = $this->input->post('vaksinasi_nonhamil', TRUE);
            $pasca_persalinan   = $this->input->post('pasca_persalinan', TRUE);
            $pasca_keguguran    = $this->input->post('pasca_keguguran', TRUE);
            $anc_g              = $this->input->post('anc_g', TRUE);
            $anc_pa             = $this->input->post('anc_pa', TRUE);
            $anc_p              = $this->input->post('anc_p', TRUE);
            $anc_a              = $this->input->post('anc_a', TRUE);
            $anc_i              = $this->input->post('anc_i', TRUE);
            $anc_h              = $this->input->post('anc_h', TRUE);
            $jum_skor           = $this->input->post('jum_skor', TRUE);
            $hpht               = strtotime($this->input->post('hpht', TRUE));
            $hpl_otomatis_2     = $this->input->post('hpl_1', TRUE);
            $hpl_manual         = strtotime($this->input->post('hpl_2', TRUE));
            $usia_kehamilan     = $this->input->post('usia_kehamilan', TRUE);
            $resiko_keterangan  = $this->input->post('resiko_keterangan', TRUE);
            $create_by          = $this->data['users']->id;

            $hpl_otomatis       = strtotime($hpl_otomatis_2);
            $hpl                = date('Y-m-d', $hpl_otomatis);
            $hpl                = strtotime($hpl);

            if($hpht == "" ){
                $hpht_fix           = date('Y-m-d', $hpht);
                $hpht_fix           = NULL;
                $usia_kehamilan_fix = $usia_kehamilan.' Minggu';
                $hpl_fix            = date('Y-m-d', $hpl_manual);
            }else{
                $hpht_fix           = date('Y-m-d', $hpht);
                $usia_kehamilan_fix = $usia_kehamilan.' Minggu';
                $hpl_fix            = date('Y-m-d', $hpl);
            }


            $data_pemeriksaan_kebidanan = array(
                'pendaftaran_id'     => $pendaftaran_id,
                'pasien_id'          => $pasien_id,
                'dokter_id'          => $dokter,
                'tanggal_periksa'    => date('Y-m-d H:i:s'),
                'vaksinasi_hamil'    => $vaksinasi_hamil,
                'vaksinasi_nonhamil' => $vaksinasi_nonhamil,
                'hpht'               => $hpht_fix,
                'hpl'                => $hpl_fix,
                'usia_kehamilan'     => $usia_kehamilan_fix,
                'g'                  => $anc_g,
                'pa'                 => $anc_pa,
                'p'                  => $anc_p,
                'a'                  => $anc_a,
                'i'                  => $anc_i,
                'h'                  => $anc_h,
                'skor_kspr'          => $jum_skor,
                'faktor_resiko_ket'  => $resiko_keterangan,
                'create_by'          => $create_by,
            );

            $current = $this->Pasien_rj_model->get_current_status($pendaftaran_id);
            $ins = $this->Pasien_rj_model->insert_pemeriksaan_kebidanan($pendaftaran_id, $data_pemeriksaan_kebidanan, $current->status_periksa);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Bahasa berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Bahasa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Bahasa, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan bahasa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }


    public function do_create_imun(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('imun', 'imun', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id = $this->input->post('pasien_id', TRUE);
            $dokter = $this->input->post('dokter', TRUE);
            $imun = $this->input->post('imun', TRUE);

            if($imun == 4 || $imun == 5 || $imun == 6 || $imun == 7 || $imun == 8 || $imun == 9 || $imun == 10 || $imun == 11 || $imun == 12 || $imun == 13)
            {
                $tindakan = null;
            }else{
                $tindakan_ke        = $this->Pasien_rj_model->get_tindakan_ke($pasien_id);
                    foreach ($tindakan_ke as $list) {
                        $tindakan = $list->tindakan_max;
                    }
                    $tindakan++;
            }

            $data_imun = array(
                'pendaftaran_id' => $pendaftaran_id,
                'pasien_id'      => $pasien_id,
                'dokter_id'      => $dokter,
                'imunisasi_id'   => $imun,
                'tanggal_tindakan' => date('Y-m-d'),
                'imun_ke'       => $tindakan
            );

            $query = $this->db->get_where('t_pasienimunisasi', array('pendaftaran_id'=>$pendaftaran_id,'imunisasi_id'=>$imun,'tanggal_tindakan'=>date('Y-m-d')));
            // $ins = $this->Pasien_rj_model->insert_imun($data_imun);

            //$ins=true;
            if($query->num_rows() > 0){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false,
                    'messages' => 'Anda Sudah Memasukan Data Ini !'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Agama dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $ins = $this->Pasien_rj_model->insert_imun($data_imun);
               //$ins=true;
                if($ins){
                    $res = array(
                        'csrfTokenName' => $this->security->get_csrf_token_name(),
                        'csrfHash'      => $this->security->get_csrf_hash(),
                        'success'       => true,
                        'messages'      => 'Diagnosa berhasil ditambahkan'
                    );

                    // if permitted, do logit
                    $perms      = "rawat_jalan_pasienrj_view";
                    $comments   = "Berhasil menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }else{
                    $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash'      => $this->security->get_csrf_hash(),
                    'success'       => false,
                    'messages'      => 'Gagal menambahkan Diagnosa, hubungi web administrator.');

                    // if permitted, do logit
                    $perms      = "rawat_jalan_pasienrj_view";
                    $comments   = "Gagal menambahkan Diagnosa dengan data berikut = '". json_encode($_REQUEST) ."'.";
                    $this->aauth->logit($perms, current_url(), $comments);
                }
            }
        }
        echo json_encode($res);
    }


    public function do_create_pnc(){
        $is_permit = $this->aauth->control_no_redirect('rawat_jalan_pasienrj_view');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tindakan_pnc', 'Tindakan PNC', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rawat_jalan_pasienrj_view";
            $comments = "Gagal input produk dengan error sebagai berikut = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);

        }else if ($this->form_validation->run() == TRUE)  {
            $pendaftaran_id = $this->input->post('pendaftaran_id', TRUE);
            $pasien_id      = $this->input->post('pasien_id', TRUE);
            $tindakan_pnc   = $this->input->post('tindakan_pnc', TRUE);
            $harga_tindakan = $this->input->post('harga_tindakan', TRUE);
            // $imun = $this->input->post('imun', TRUE);

            $pnc_ke        = $this->Pasien_rj_model->get_pnc_ke($pasien_id);
            foreach ($pnc_ke as $list) {
                $pnc = $list->tindakan_max;
            }
            $pnc++;
            $data_pnc = array(
                'pendaftaran_id'    => $pendaftaran_id,
                'pasien_id'         => $pasien_id,
                'pnc_id'            => $tindakan_pnc,
                'harga_tindakan'    => $harga_tindakan,
                'tanggal_tindakan'      => date('Y-m-d'),
                'pnc_ke'            => $pnc,
            );

            $ins = $this->Pasien_rj_model->insert_pnc($data_pnc);
            //$ins=true;
            if($ins){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Agama berhasil ditambahkan'
                );

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Berhasil menambahkan Agama dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Gagal menambahkan Agama, hubungi web administrator.');

                // if permitted, do logit
                $perms = "rawat_jalan_pasienrj_view";
                $comments = "Gagal menambahkan Agama dengan data berikut = '". json_encode($_REQUEST) ."'.";
                $this->aauth->logit($perms, current_url(), $comments);
            }
        }
        echo json_encode($res);
    }




    public function ajax_list_imun(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rj_model->get_imun_list($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $imun){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d M Y', strtotime($imun->tanggal_tindakan));
            $row[] = $imun->jenis;
            //add html for action
            $row[] =
                    '
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus imun" id="hapusImun" onclick="hapusImun('."'".$imun->pasienimunisasi_id."'".')"><i class="fa fa-times"></i></button>
                    ';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_rj_model->count_imun_all(),
                    "recordsFiltered" => $this->Pasien_rj_model->count_imun_all(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_list_pnc(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rj_model->get_pnc_list($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $pnc){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d F Y', strtotime($pnc->tanggal_tindakan));
            $row[] = $pnc->daftartindakan_nama;
            //add html for action
            $row[] =
                    '
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus imun" id="hapusPNC" onclick="hapusPNC('."'".$pnc->tindakanpnc_id."'".')"><i class="fa fa-times"></i></button>
                    ';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_rj_model->count_pnc_all(),
                    "recordsFiltered" => $this->Pasien_rj_model->count_pnc_all(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }


    public function ajax_list_kb(){
        $pendaftaran_id = $this->input->get('pendaftaran_id',TRUE);
        $list = $this->Pasien_rj_model->get_kb_list($pendaftaran_id);
        $data = array();
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($list as $kb){
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d F Y', strtotime($kb->tanggal_tindakan));
            $row[] = $kb->tipe_kb;
            $row[] = $kb->vaksinasi_nama;
            //add html for action
            $row[] =
                    '
                      <button class="btn btn-danger btn-circle" data-position="bottom" data-delay="50" title="klik untuk menghapus imun" id="hapusKB" onclick="hapusKB('."'".$kb->pasienkb_id."'".')"><i class="fa fa-times"></i></button>
                    ';
            $data[] = $row;
        }
        $output = array(
                    "draw" => $this->input->get('draw'),
                    "recordsTotal" => $this->Pasien_rj_model->count_kb_all(),
                    "recordsFiltered" => $this->Pasien_rj_model->count_kb_all(),
                    "data" => $data,
                    );
        //output to json format
        echo json_encode($output);
    }

    function save_reschedule(){
        $is_permit = $this->aauth->control_no_redirect('rekam_medis_pendaftaran_create');
        if(!$is_permit) {
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $this->lang->line('aauth_error_no_access'));
            echo json_encode($res);
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tgl_reservasi', 'Tanggal Reschedule', 'required|trim');
//        $this->form_validation->set_rules('jam', 'Jam Periksa', 'required|trim');

        if ($this->form_validation->run() == FALSE)  {
            $error = validation_errors();
            $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => $error);

            // if permitted, do logit
            $perms = "rekam_medis_pendaftaran_create";
            $comments = "Gagal melakukan pendaftaran with errors = '". validation_errors('', "\n") ."'.";
            $this->aauth->logit($perms, current_url(), $comments);
        }else if ($this->form_validation->run() == TRUE)  {
            $tgl_res = $this->input->post('tgl_reservasi', TRUE);
            $data_res['tanggal_reservasi'] = date('Y-m-d', strtotime($tgl_res));
            $data_res['is_reschedule'] = 1;
            $reservasi_id = $this->input->post('reservasi_id', TRUE);

            $this->Pasien_rj_model->update_reservasi($reservasi_id, $data_res);

            $data_pen['tgl_pendaftaran'] = date('Y-m-d H:i:s', strtotime($tgl_res . "00:00:01"));
            $data_pen['jam_periksa'] = $this->input->post('jam', TRUE);;
            $pendaftaran_id = $this->input->post('res_pendaftaran_id', TRUE);

            $update_pendaftaran = $this->Pasien_rj_model->update_pendaftaran($pendaftaran_id, $data_pen);

            if($update_pendaftaran){
                $res = array(
                    'csrfTokenName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true,
                    'messages' => 'Reschedule has been saved to database',
                    'pendaftaran_id' => $pendaftaran_id
                );

                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Success to Create a new patient register with post data";
                $this->aauth->logit($perms, current_url(), $comments);
            }
            else{
                $res = array(
                'csrfTokenName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'messages' => 'Failed insert user group to database, please contact web administrator.');

                // if permitted, do logit
                $perms = "rekam_medis_pendaftaran_create";
                $comments = "Failed to Create a new patient register when saving to database";
                $this->aauth->logit($perms, current_url(), $comments);
            }


            //apabila transaksi sukses maka commit ke db, apabila gagal maka rollback db.
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
            }
        }
        echo json_encode($res);
    }


    function get_data_pasien($pendaftaran_id){
        // $pendaftaran_id = $this->input->post('res_pendaftaran_id', TRUE);
        $result = $this->Pasien_rj_model->get_pendaftaran_pasien($pendaftaran_id);

        if($result){
            $ttl = $result->tanggal_lahir;

            $res = array(
                "data" => $result,
                "tgl_lahir" => date('d F Y', strtotime($ttl)),
                "success" => true
            );
        }else{

            $res = array(
                "success" => false

            );
        }

        echo json_encode($res);
    }

     public function getCaraPembayaran($id){
        $data = $this->Pasien_rj_model->getCaraPembayaran($id);
        echo json_encode($data);

    }


    function exportToExcelBidan(){
        $tgl_awal       = $this->input->get('tgl_awal',TRUE);
        $tgl_akhir      = $this->input->get('tgl_akhir',TRUE);
        $this->load->library("Excel");
        // $this->excel->load("/path/to/input.xls");
        $object = new PHPExcel();

        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setTitle("Kebidanan");
         $object->getActiveSheet()
                        ->getStyle("A1:AN1")
                        ->getFont()
                        ->setSize(14)
                        ->setBold(true)
                        ->getColor()
                        ->setRGB('FFFFFF');
        $object->getActiveSheet()
                        ->getStyle("A1:AN1")
                        ->getFill()
                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB('01c0c8');

        $table_columns = array("NO REG", "NO RM","NAMA","JK","NAMA ORTU","TGL LAHIR","UMUR","ALAMAT","KELURAHAN","TANGGAL PERIKSA","BB","TB","SISTOLE","DIASTOLE","VAKSINASI NON HAMIL","VAKSINASI HAMIL","IMUN 1","IMUN 2","IMUN 3","IMUN 4","IMUN 5","PRENATAL","PASCA PERSALIAN","PASCA KEGUGURAN","G","Pa","P","i","a","h","HPHT","HPL","USIA KEHAMILAN","TINDAKAN 1","TINDAKAN 2","TINDAKAN 3", "DOKTER PJ","ASISTEN","KETERANGA (FAKTOR RESIKO YANG DITEMUKAN)","SKOR KSPR");

        $column = 0;

        foreach($table_columns as $field){
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $employee_data = $this->Pasien_rj_model->getDataBidan($tgl_awal, $tgl_akhir);
        // var_dump($employee_data);die();
        // echo "<pre>";
        // print_r($employee_data);
        // echo "</pre>";
        // die();
        $excel_row = 2;
        $no = isset($_GET['start']) ? $_GET['start'] : 0;
        foreach($employee_data as $row){
            $no++;
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->no_pendaftaran);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->no_rekam_medis);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->pasien_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->jenis_kelamin);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->nama_orangtua);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->tanggal_lahir);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->umur);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->pasien_alamat_domisili);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->kelurahan_nama);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->tanggal_periksa);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->bb);
            $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->tb);
            $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->sistole);
            $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->diastole);
            $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->vaksinasi_nonhamil);
            $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row->vaksinasi_hamil);
            $object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->imun_1);
            $object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->imun_2);
            $object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $row->imun_3);
            $object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row->imun_4);
            $object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $row->imun_5);
            $object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, $row->prenatal);
            $object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $row->pasca_persalinan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, $row->pasca_keguguran);
            $object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, $row->G);
            $object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, $row->Pa);
            $object->getActiveSheet()->setCellValueByColumnAndRow(26, $excel_row, $row->P);
            $object->getActiveSheet()->setCellValueByColumnAndRow(27, $excel_row, $row->i);
            $object->getActiveSheet()->setCellValueByColumnAndRow(28, $excel_row, $row->a);
            $object->getActiveSheet()->setCellValueByColumnAndRow(29, $excel_row, $row->h);
            $object->getActiveSheet()->setCellValueByColumnAndRow(30, $excel_row, $row->HPHT);
            $object->getActiveSheet()->setCellValueByColumnAndRow(31, $excel_row, $row->HPL);
            $object->getActiveSheet()->setCellValueByColumnAndRow(32, $excel_row, $row->usia_kehamilan);
            $object->getActiveSheet()->setCellValueByColumnAndRow(33, $excel_row, "T 1");
            $object->getActiveSheet()->setCellValueByColumnAndRow(34, $excel_row, "T 2");
            $object->getActiveSheet()->setCellValueByColumnAndRow(35, $excel_row, "T 3");
            $object->getActiveSheet()->setCellValueByColumnAndRow(36, $excel_row, "dokter pj");
            $object->getActiveSheet()->setCellValueByColumnAndRow(37, $excel_row, "asisten");
            $object->getActiveSheet()->setCellValueByColumnAndRow(38, $excel_row, "ket");
            $object->getActiveSheet()->setCellValueByColumnAndRow(39, $excel_row, $row->skor_kspr);
            $excel_row++;
            // var_dump($no);
    }
    // die();


         foreach(range('A','AA') as $columnID) {
               $object->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $object->getActiveSheet()->getStyle('A1:' . $object->getActiveSheet()->getHighestColumn() . $object->getActiveSheet()->getHighestRow())->applyFromArray($styleArray);



        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Kebidanan.xlsx"');
        $object_writer->save('php://output');
    }

    public function get_fee_dokter_params($hotel_id = false, $jenis_pasien, $type_call, $shift) {
        $shift = ($shift == "PAGI") ? 1 : 2;
        $result = 0;
        $feeDokterShift = $this->Models->where('fee_dokter_by_shift', array('shift' => $shift, 'branch' => $this->session->tempdata('data_session')[2]))[0];

        if ($type_call < 4) {
            if ($type_call == 3) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['vs_local'] = 0;
                    $feeDokterHotel['vs_domestik'] = 0;
                    $feeDokterHotel['vs_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['vs_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['vs_domestik'];
                } else {
                    $result = $feeDokterHotel['vs_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['vs_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['vs_dome'];
                } else {
                    $result = $feeDokterShift['vs_asing'];
                }
            }
        } else {
            if ($type_call == 5) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['oc_local'] = 0;
                    $feeDokterHotel['oc_domestik'] = 0;
                    $feeDokterHotel['oc_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['oc_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['oc_domestik'];
                } else {
                    $result = $feeDokterHotel['oc_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['oc_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['oc_dome'];
                } else {
                    $result = $feeDokterShift['oc_asing'];
                }
            }
        }

        return $result;
    }

    // START : Fungsi untuk mengambil harga dokter
    public function get_fee_dokter()
    {
        $hotel_id = $this->input->get('hotel_id', TRUE);
        $jenis_pasien = $this->input->get('jenis_pasien', TRUE);
        $type_call = $this->input->get('type_call', TRUE);
        $shift = ($this->input->get('shift', TRUE) == "PAGI") ? 1 : 2;
//        $shift = ($this->input->get('shift', TRUE) >= 8) && ($this->input->get('shift', TRUE) < 22) ? 1 : 2;
        $result = 0;
        $feeDokterShift = $this->Models->where('fee_dokter_by_shift', array('shift' => $shift, 'branch' => $this->session->tempdata('data_session')[2]))[0];

        if ($type_call < 4) {
            if ($type_call == 3) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['vs_local'] = 0;
                    $feeDokterHotel['vs_domestik'] = 0;
                    $feeDokterHotel['vs_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['vs_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['vs_domestik'];
                } else {
                    $result = $feeDokterHotel['vs_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['vs_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['vs_dome'];
                } else {
                    $result = $feeDokterShift['vs_asing'];
                }
            }
        } else {
            if ($type_call == 5) {
                if ($this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0]) {
                    $feeDokterHotel = $this->Models->where('fee_dokter_by_hotel', array('id_hotel' => $hotel_id, 'branch' => $this->session->tempdata('data_session')[2]))[0];
                } else {
                    $feeDokterHotel['oc_local'] = 0;
                    $feeDokterHotel['oc_domestik'] = 0;
                    $feeDokterHotel['oc_guest'] = 0;
                }

                if ($jenis_pasien == 1) {
                    $result = $feeDokterHotel['oc_local'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterHotel['oc_domestik'];
                } else {
                    $result = $feeDokterHotel['oc_guest'];
                }
            } else {
                if ($jenis_pasien == 1) {
                    $result = $feeDokterShift['oc_lokal'];
                } else if ($jenis_pasien == 2) {
                    $result = $feeDokterShift['oc_dome'];
                } else {
                    $result = $feeDokterShift['oc_asing'];
                }
            }
        }

        echo $result;
    }
}
