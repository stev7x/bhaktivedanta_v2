<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_rj_model extends CI_Model {
    var $column = array('pendaftaran_id','nama_poliruangan','no_pendaftaran','tgl_pendaftaran','no_rekam_medis','pasien_nama','jenis_kelamin','umur','pasien_alamat','type_pembayaran','status_periksa');
    var $order = array('tgl_pendaftaran' => 'ASC');

    var $column1 = array('kode_barang');
    var $order1 = array('nama_barang' => 'ASC');

    var $column2    =   array('diagnosa_kode', 'diagnosa_nama');
    var $order2     =   array('diagnosa_nama','ASC');

    public function __construct(){
        parent::__construct(); 
    }

    public function delete_periksa($pendaftaran_id){
      $datas = array(
          'status_periksa' => 'BATAL PERIKSA',
          'status_pasien' => 0
      );
      $update = $this->db->update('t_pendaftaran', $datas, array('pendaftaran_id' => $pendaftaran_id));
      return $update;
    }

    public function get_pasienrj_list($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan){
        $this->db->from('v_pasien_rj');
//        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $this->db->order_by('tgl_pendaftaran', 'DESC');

        if(!empty($poliruangan)){
            $this->db->where('poliruangan_id',$poliruangan);
        }
        if(!empty($dokter_poli)){
            $this->db->where('id_m_dokter',$dokter_poli);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_nama)){ 
            $this->db->like('pasien_nama', $pasien_nama);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($name_dokter)){
            $this->db->like('NAME_DOKTER', $name_dokter);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

//        $this->db->where("status_periksa != 'SUDAH BAYAR'");

        if(!empty($urutan)){
            $this->db->order_by($urutan, "ASC");
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');

        $query = $this->db->get();
        
        return $query->result();

//        echo $this->db->last_query();
//        exit;
    }
    
    function count_all($tgl_awal, $tgl_akhir, $poliruangan, $dokter_poli, $no_pendaftaran, $no_rekam_medis, $no_bpjs, $pasien_nama, $pasien_alamat, $name_dokter, $urutan){
        $this->db->from('v_pasien_rj');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');

        if(!empty($poliruangan)){
            $this->db->where('poliruangan_id',$poliruangan);
        }
        if(!empty($dokter_poli)){
            $this->db->where('id_m_dokter',$dokter_poli);
        }
        if(!empty($no_pendaftaran)){
            $this->db->like('no_pendaftaran', $no_pendaftaran);
        }
        if(!empty($no_rekam_medis)){
            $this->db->like('no_rekam_medis', $no_rekam_medis);
        }
        if(!empty($no_bpjs)){
            $this->db->like('no_bpjs', $no_bpjs);
        }
        if(!empty($pasien_nama)){
            $this->db->like('pasien_nama', $pasien_nama);
        }
        if(!empty($pasien_alamat)){
            $this->db->like('pasien_alamat', $pasien_alamat);
        }
        if(!empty($name_dokter)){
            $this->db->like('NAME_DOKTER', $name_dokter);
        }
        if(!empty($dokter_id)){
            $this->db->where('id_M_DOKTER', $dokter_id);
        }

        if(!empty($urutan)){
            $this->db->order_by($urutan, "ASC");
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $this->db->where('no_pendaftaran LIKE', '%'.$this->session->tempdata('data_session')[2].'%');
        
        return $this->db->count_all_results();
    }

    public function get_pendaftaran_pasien($pendaftaran_id){
        
        // $this->db->select('*');
        // $this->db->from('v_pasien_rj');
        // $this->db->join('t_pemeriksaan_obgyn','t_pemeriksaan_obgyn.pendaftaran_id = v_pasien_rj.pendaftaran_id','left');
        // $this->db->where('v_pasien_rj.pendaftaran_id', $pendaftaran_id);
        // $this->db->limit(1);
        // $query = $this->db->get();
        // return $query->row();
        $query = $this->db->get_where('v_pasien_rj', array('pendaftaran_id' => $pendaftaran_id), 1, 0);
        return $query->row();
    }

    public function get_datapasien_kebidanan($pendaftaran_id){
        $this->db->from('t_pemeriksaan_obgyn');

        return $this->db->row();
        
    }
 

    public function get_dokter_list(){
        $this->db->select('id_M_DOKTER, NAME_DOKTER');
        $this->db->from('v_dokterruangan');
        $this->db->where('instalasi_id', 1); //rawat jalan
        $this->db->group_by('id_M_DOKTER');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_diagnosa_pasien_list($pendaftaran_id){
        $this->db->from('t_diagnosapasien');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function insert_diagnosapasien($pendaftaran_id, $data=array(), $current){
        if ($current != "DIPULANGKAN") {
            $this->update_to_sudahperiksa($pendaftaran_id);
        }

        $insert = $this->db->insert('t_diagnosapasien',$data);

        return $insert;
    }

     public function insert_resep_to_aoptek($data){
        $insert = $this->db->insert('t_apotek_keluar',$data);
        return $insert;
    }

    public function insert_skor_kspr($data){
        $insert = $this->db->insert('t_skor_kspr',$data);
        return $insert;
    }
     public function insert_pemeriksaan_fisik($pendaftaran_id, $data, $current){
         if ($current != "DIPULANGKAN") {
             $this->update_to_sudahperiksa($pendaftaran_id);
         }

        $insert = $this->db->insert('t_detail_pemeriksaan_fisik',$data);

        return $insert;
    }

    public function update_to_sudahperiksa($id){
        $data = array(
            'status_periksa' => 'SUDAH PERIKSA'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

    public function update_to_penunjang($id){
        $data = array(
            'status_periksa' => 'SEDANG DI PENUNJANG'
        );
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }

    public function get_tindakan_pasien_list($pendaftaran_id){ 
        $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama,m_tindakan.type_tindakan');
        $this->db->from('t_tindakanpasien'); 
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);
        $this->db->where('m_tindakan.type_tindakan',"TINDAKAN");   
        $length = $this->input->get('length');   
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    public function get_laboratorium_pasien_list($pendaftaran_id){ 
        $this->db->select('t_tindakanpasien.*, m_tindakan.daftartindakan_nama,m_tindakan.type_tindakan');
        $this->db->from('t_tindakanpasien'); 
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien.daftartindakan_id');
        $this->db->where('t_tindakanpasien.pendaftaran_id',$pendaftaran_id);
        $this->db->where('m_tindakan.type_tindakan',"LABORATORIUM");   
        $length = $this->input->get('length');   
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_kelas_pelayanan(){
        $this->db->from('m_kelaspelayanan');
        $query = $this->db->get();

        return $query->result();
    }
 
    public function get_tindakan($is_bpjs, $jenis_pasien, $type_call){
        // $this->db->select('m_tindakan.*');
        
        $this->db->from('m_tindakan');  
        // $this->db->where('m_tindakan.kelaspelayanan_id',$kelaspelayanan_id);   
        // $this->db->where('m_tindakan.kelompoktindakan_id',2);         
        
        // $this->db->where('m_tindakan.komponentarif_id','1');  
        // if(strtoupper($is_bpjs) == strtoupper("BPJS")){ 
        //     $this->db->where('m_tindakan.bpjs_non_bpjs','BPJS');
        // }else{  
        //     $this->db->where('m_tindakan.bpjs_non_bpjs','Non BPJS');   
        // }
        $this->db->where('m_tindakan.jenis_pasien',$jenis_pasien);
        $this->db->where('m_tindakan.type_call',$type_call);
        $this->db->where('m_tindakan.type_tindakan',"TINDAKAN");
        $query = $this->db->get();

        return $query->result();
    }

    public function get_m_jenis_pasien_by_id($jenis_pasien_id) {
        $this->db->where("id_jenis_pasien", $jenis_pasien_id);
        return $this->db->get("m_jenis_pasien")->row();
    }

    public function get_reservasi_by_rekam_medis($no_rekam_medis) {
        $this->db->where("no_rekam_medis", $no_rekam_medis);
        return $this->db->get("t_reservasi")->row();
    }

    public function get_laboratorium($kelaspelayanan_id, $is_bpjs, $jenis_pasien, $type_call){
        // $this->db->select('m_tindakan.*');
        
        $this->db->from('m_tindakan');  
        // $this->db->where('m_tindakan.kelaspelayanan_id',$kelaspelayanan_id);   
        // $this->db->where('m_tindakan.kelompoktindakan_id',2);         
        
        // $this->db->where('m_tindakan.komponentarif_id','1');  
        if(strtoupper($is_bpjs) == strtoupper("BPJS")){ 
            $this->db->where('m_tindakan.bpjs_non_bpjs','BPJS');
        }else{  
            $this->db->where('m_tindakan.bpjs_non_bpjs','Non BPJS');   
        }
        $this->db->where('m_tindakan.jenis_pasien',$jenis_pasien);
        $this->db->where('m_tindakan.type_call',$type_call);
        $this->db->where('m_tindakan.type_tindakan',"LABORATORIUM");
        $query = $this->db->get();

        return $query->result();
    }

    public function get_tarif_tindakan($daftartindakan_id){
        $query = $this->db->get_where('m_tindakan', array('daftartindakan_id' => $daftartindakan_id), 1, 0);
 
        return $query->row();   
    } 

    public function insert_tindakanpasien($pendaftaran_id, $data=array(), $current){
        if ($current != "DIPULANGKAN") {
            $this->update_to_sudahperiksa($pendaftaran_id);
        }
        $insert = $this->db->insert('t_tindakanpasien',$data);

        return $insert;
    }
    public function delete_diagnosa($id){
        $delete = $this->db->delete('t_diagnosapasien', array('diagnosapasien_id' => $id));

        return $delete;
    }

    public function delete_all_kspr($id, $date){
        $delete = $this->db->delete('t_skor_kspr', array('pendaftaran_id' => $id, 'tanggal' => $date));
        return $delete;
    }

    public function delete_kspr($id){
        $delete = $this->db->delete('t_skor_kspr', array('id_skor_kspr' => $id));
        return $delete;
    }

    public function delete_pnc($id){
        $delete = $this->db->delete('t_tindakanpasien_pnc', array('tindakanpnc_id' => $id));

        return $delete;
    }

    public function delete_kebidanan($id){
        $delete = $this->db->delete('t_pemeriksaan_obgyn', array('pemeriksaan_obgyn_id' => $id));

        return $delete;
    }

    public function delete_kb($id){
        $delete = $this->db->delete('t_pasienkb', array('pasienkb_id' => $id));

        return $delete;
    }

    public function delete_imun($id){
        $delete = $this->db->delete('t_pasienimunisasi', array('pasienimunisasi_id' => $id));

        return $delete;
    }

    public function delete_pemfisik($id){
        $delete = $this->db->delete('t_detail_pemeriksaan_fisik', array('id_detail_pemeriksaan' => $id));

        return $delete;
    }

    public function delete_tindakan($id){
        $delete = $this->db->delete('t_tindakanpasien', array('tindakanpasien_id' => $id));

        return $delete;
    } 
    
    public function get_resep_pasien_list($pendaftaran_id){
        $this->db->select('t_resepturpasien.*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis, m_dokter.NAME_DOKTER');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_resepturpasien.id_sediaan','left');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_resepturpasien.id_jenis_barang','left');
        $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_resepturpasien.dokter_id','left');
        $this->db->from('t_resepturpasien');
        // $this->db->join('m_obat','m_obat.obat_id = t_resepturpasien.obat_id');
        $this->db->where('t_resepturpasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }

    public function get_alergi_pasien_list($pendaftaran_id){
        $this->db->select('t_alergi_pasien.*, m_alergi.nama_alergi, m_alergi.kode_alergi');
        $this->db->join('m_alergi','m_alergi.alergi_id = t_alergi_pasien.alergi_id','left');
        $this->db->from('t_alergi_pasien');
        // $this->db->join('m_obat','m_obat.obat_id = t_resepturpasien.obat_id');
        $this->db->where('t_alergi_pasien.pendaftaran_id',$pendaftaran_id);
        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }

        $query = $this->db->get();

        return $query->result();
    }
    
    function get_obat_autocomplete($val=''){
        $this->db->select('*');
        $this->db->from('v_stokobatalkes');
        $this->db->like('nama_obat', $val);
        $this->db->where('qtystok !=', 0);
        $query = $this->db->get();

        return $query->result();
    }
    
    public function insert_reseppasien($data=array()){
        $this->db->insert('t_resepturpasien',$data);
        // $error = $this->db->error();
        // echo json_encode($error); die();
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function insert_resep_to_apotek($data=array()){
        // $this->db->insert('t_apotek_keluar',$data);
        $this->db->insert('t_barang_keluar_farmasi',$data);
        $id = $this->db->insert_id();
        return $id;
    }


    
    public function insert_stokobatalkes_keluar($data=array()){
        $stok = $this->db->insert('t_stokobat',$data);
        
        return $stok;
    }
    
    public function delete_resep($id, $tgl_reseptur){
        $delete = $this->db->delete('t_resepturpasien', array('reseptur_id' => $id));
        $delete = $this->db->delete('t_barang_keluar_farmasi', array('tanggal_keluar' => $tgl_reseptur));

        return $delete;
    }
    
    public function delete_stok_resep($id){
        $stok = $this->db->delete('t_stokobat', array('penjualandetail_id' => $id));
        
        return $stok;
    }

    public function get_list_penunjang(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => 4));

        return $query->result();
    }

    public function insert_penunjang($data){
        $insert = $this->db->insert('t_pasienmasukpenunjang',$data);
        return $insert;
    }

    public function get_last_pendaftaran($id){
        $q = $this->db->get_where('t_pendaftaran', array('pendaftaran_id' => $id));
        return $q->row();
    }
    
    function get_ruangan(){
        $this->db->order_by('nama_poliruangan', 'ASC');
        $query = $this->db->get_where('m_poli_ruangan', array('instalasi_id' => INSTALASI_RAWAT_JALAN));
        
        return $query->result();
    }
	
	function get_list_obat($jenis_pasien, $type_call){
        $this->db->select('*, m_sediaan_obat.nama_sediaan, m_jenis_barang_farmasi.nama_jenis');
        // $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_apotek_stok.id_sediaan');
        $this->db->join('m_sediaan_obat','m_sediaan_obat.id_sediaan = t_stok_farmasi.id_sediaan');
        // $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_apotek_stok.id_jenis_barang');
        $this->db->join('m_jenis_barang_farmasi','m_jenis_barang_farmasi.id_jenis_barang = t_stok_farmasi.id_jenis_barang');
        $this->db->from('t_stok_farmasi');
        $this->db->where('t_stok_farmasi.type_call',$type_call);
        $this->db->where('t_stok_farmasi.jenis_pasien',$jenis_pasien);
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }



    public function count_list_filtered_obat(){
        // $this->db->from('t_apotek_stok');
        $this->db->from('t_stok_farmasi');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column1 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column1[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order1)){
            $order1 = $this->order1;
            $this->db->order_by(key($order1), $order1[key($order1)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_list_obat(){
            // $this->db->from('t_apotek_stok');
            $this->db->from('t_stok_farmasi');
            $this->db->where('stok_akhir !=', 0);
        
            return $this->db->count_all_results();
    }
	
	function get_obat_by_id($id){
            $this->db->select('*');
            // $this->db->from('t_apotek_stok');
            $this->db->from('t_stok_farmasi');
            $this->db->where('kode_barang =', $id);
            $query = $this->db->get();

            return $query->result();
    }

	function get_list_diagnosa(){
         $this->db->select('*, m_diagnosa_icd10.kode_diagnosa, m_diagnosa_icd10.nama_diagnosa');
        $this->db->join('m_diagnosa_icd10','m_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
        $this->db->from('m_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column2 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column2[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order2)){
            $order2 = $this->order2;
            $this->db->order_by(key($order2), $order2[key($order2)]);
        }

        $length = $this->input->get('length');
        if($length !== false){
            if($length != -1) {
                $this->db->limit($this->input->get('length'), $this->input->get('start'));
            }
        }
        $query = $this->db->get();

        return $query->result();
    }



    public function count_list_filtered_diagnosa(){
        $this->db->select('*, m_diagnosa_icd10.kode_diagnosa, m_diagnosa_icd10.nama_diagnosa');
        $this->db->join('m_diagnosa_icd10','m_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
        $this->db->from('m_diagnosa');
        $i = 0;
        $search_value = $this->input->get('search');
        if($search_value){
            foreach ($this->column2 as $item){
                ($i==0) ? $this->db->like($item, $search_value['value']) : $this->db->or_like($item, $search_value['value']);
                $i++;
            }
        }
        $order_column = $this->input->get('order');
        if($order_column !== false){
            $this->db->order_by($this->column2[$order_column['0']['column']], $order_column['0']['dir']);
        }
        else if(isset($this->order2)){
            $order2 = $this->order2;
            $this->db->order_by(key($order2), $order2[key($order2)]);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_list_diagnosa(){
            $this->db->from('m_diagnosa');
        
            return $this->db->count_all_results();
    }
	
	function get_diagnosa_by_id($id){
            // $this->db->select('*');
            $this->db->select('*, m_diagnosa_icd10.kode_diagnosa, m_diagnosa_icd10.nama_diagnosa');
            $this->db->join('m_diagnosa_icd10','m_diagnosa_icd10.diagnosa_id = m_diagnosa.diagnosa_id','left');
            $this->db->from('m_diagnosa');
            $this->db->where('diagnosa2_id =', $id);            
            $query = $this->db->get();

            return $query->result();
    }
    
    function get_sediaan_list(){
        return $this->db->get("m_sediaan_obat")->result();
    }

    function get_poli(){
        $this->db->from("m_poli_ruangan");
        $this->db->where('instalasi_id',1);
        $query = $this->db->get();
        return $query->result();
    }
	

    public function update_to_igd($data, $id){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));
        
        return $update;
    }

    function get_data_print($pasien_id){   
  
    $this->db->select('*, CONCAT(nama_asuransi , IF(nama_asuransi2="" && nama_asuransi ="" || nama_asuransi2=""," " , ",") , nama_asuransi2 ) AS asuransi');    
    $this->db->from('t_pendaftaran');
    $this->db->join('m_pasien','m_pasien.pasien_id = t_pendaftaran.pasien_id');
    $this->db->join('m_propinsi','m_propinsi.propinsi_id = m_pasien.propinsi_id', 'left');
    $this->db->join('m_kabupaten','m_kabupaten.kabupaten_id = m_pasien.kabupaten_id', 'left');
    $this->db->join('m_kelurahan','m_kelurahan.kelurahan_id = m_pasien.kelurahan_id', 'left');
    $this->db->join('m_kecamatan','m_kecamatan.kecamatan_id = m_pasien.kecamatan_id', 'left');
    $this->db->join('m_pekerjaan','m_pekerjaan.pekerjaan_id = m_pasien.pekerjaan_id','left');
    $this->db->join('m_penanggungjawab','m_penanggungjawab.penanggungjawab_id = t_pendaftaran.penanggungjawab_id');
    $this->db->join('m_dokter','m_dokter.id_M_DOKTER = t_pendaftaran.dokter_id');
    $this->db->join('t_pembayaran','t_pembayaran.pembayaran_id = t_pendaftaran.pembayaran_id');
    $this->db->join('m_instalasi','m_instalasi.instalasi_id = t_pendaftaran.instalasi_id');
    $this->db->join('m_agama','m_agama.agama_id = m_pasien.agama_id','left');
    $this->db->join('m_bahasa','m_bahasa.bahasa_id = m_pasien.bahasa_id','left');
    $this->db->join('m_pendidikan','m_pendidikan.pendidikan_id = m_pasien.pendidikan_id','left');
    $this->db->where('t_pendaftaran.pasien_id', $pasien_id,1,0);   
    $query = $this->db->get();   

    
    return $query->row();
    }

    function get_data_diagnosa($jenis_diagnosa, $pendaftaran_id){
        $this->db->select('*');
        $this->db->from('t_diagnosapasien');
        $this->db->where('jenis_diagnosa',$jenis_diagnosa);
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();   
        return $query->result();
    }

    function get_data_tindakan($pendaftaran_id){
        $this->db->select('*');
        $this->db->from('t_tindakanpasien_icd9');
        $this->db->where('pendaftaran_id',$pendaftaran_id);
        $query = $this->db->get();   
        return $query->result();
    }

    function get_pemeriksaan_fisik_list($pendaftaran_id){
        $this->db->from('t_detail_pemeriksaan_fisik');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_pemeriksaan_fisik_all(){
            $this->db->from('t_detail_pemeriksaan_fisik');
            return $this->db->count_all_results();
    }

    function get_skor_kspr_list($pendaftaran_id){
        $this->db->select('t_skor_kspr.*, m_kspr.kspr_nama');
        $this->db->from('t_skor_kspr');
        $this->db->join('m_kspr','m_kspr.kspr_id = t_skor_kspr.kspr_id','left');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->result();
    }

    function count_skor_kspr_all(){
            $this->db->from('t_skor_kspr');
            return $this->db->count_all_results();
    }

    
    function get_dokter_poli($poliruangan_id){
        $this->db->select('*, m_poli_ruangan.nama_poliruangan, m_dokter.name_dokter');
        $this->db->join('m_poli_ruangan','m_poli_ruangan.poliruangan_id = m_dokterruangan.poliruangan_id','left');
        $this->db->join('m_dokter','m_dokter.id_m_dokter = m_dokterruangan.dokter_id','left');
        $this->db->from('m_dokterruangan');
        $this->db->where('m_dokterruangan.poliruangan_id',$poliruangan_id);
        $this->db->order_by('NAME_DOKTER', 'ASC');
        $query = $this->db->get();
        
        return $query->result();
    }

    function get_kspr(){
        return $this->db->get("m_kspr")->result();
    }

    function get_imun(){
        $this->db->from('m_imunisasi');
        // $this->db->where('kelompoktindakan_id', 9);
        $query = $this->db->get();
        return $query->result();
    }

    function get_tarif_imun($data){
        $this->db->from('m_tindakan');
        $this->db->where('kelompoktindakan_id', 9);
        $this->db->where('daftartindakan_id', $data);
        $query = $this->db->get();
        return $query->result();
    }

    function get_kb(){
        $this->db->select('*');
        $this->db->from('m_vaksinasi');
        // $this->db->where('kelompoktindakan_id', 10);
        $query = $this->db->get();
        return $query->result();
    }

    function get_pnc(){
        $this->db->from('m_tindakan');
        $this->db->where('kelompoktindakan_id', 9);
        $query = $this->db->get();
        return $query->result();
    }


    public function insert_pemeriksaan_kebidanan($pendaftaran_id, $data, $current){
        if ($current != "DIPULANGKAN") {
            $this->update_to_sudahperiksa($pendaftaran_id);
        }
        $insert = $this->db->insert('t_pemeriksaan_obgyn',$data);

        return $insert;
    }


    public function insert_imun($data){
        $insert = $this->db->insert('t_pasienimunisasi',$data);
        return $insert;
    }

    public function insert_pnc($data){
        $insert = $this->db->insert('t_tindakanpasien_pnc',$data);
        // $insert = $this->db->insert('t_tindakanpasien',$data);
        return $insert;
    }

    public function insert_kb($data){
        $insert = $this->db->insert('t_pasienkb',$data);
        return $insert;
    }

    function get_imun_list($pendaftaran_id){
        $this->db->select('*, m_imunisasi.jenis');
        $this->db->join('m_imunisasi','m_imunisasi.imunisasi_id = t_pasienimunisasi.imunisasi_id');
        $this->db->from('t_pasienimunisasi');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->result();
    }

    function count_imun_all(){
        $this->db->select('*, m_imunisasi.jenis');
        $this->db->join('m_imunisasi','m_imunisasi.imunisasi_id = t_pasienimunisasi.imunisasi_id');
        $this->db->from('t_pasienimunisasi');
        
        return $this->db->count_all_results();
    }


    function get_pnc_list($pendaftaran_id){
        $this->db->select('*, m_tindakan.daftartindakan_nama');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien_pnc.pnc_id');
        $this->db->from('t_tindakanpasien_pnc');
        $this->db->where('m_tindakan.kelompoktindakan_id', 9);
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->result();
    }

    function count_pnc_all(){
         $this->db->select('*, m_tindakan.daftartindakan_nama');
        $this->db->join('m_tindakan','m_tindakan.daftartindakan_id = t_tindakanpasien_pnc.tindakanpnc_id');
        $this->db->from('t_tindakanpasien_pnc');
        $this->db->where('m_tindakan.kelompoktindakan_id', 9);
        
        return $this->db->count_all_results();
    }
    
    
    function get_kb_list($pendaftaran_id){
        $this->db->select('*, m_vaksinasi.vaksinasi_nama');
        $this->db->join('m_vaksinasi','m_vaksinasi.vaksinasi_id = t_pasienkb.vaksinasi_id');
        $this->db->from('t_pasienkb');
        $this->db->where('pendaftaran_id', $pendaftaran_id);
        $query = $this->db->get();
        return $query->result();
    }
    
    
    function count_kb_all(){
        $this->db->select('*, m_vaksinasi.vaksinasi_nama');
        $this->db->join('m_vaksinasi','m_vaksinasi.vaksinasi_id = t_pasienkb.vaksinasi_id');
        $this->db->from('t_pasienkb');
        // $this->db->where('pendaftaran_id', $pendaftaran_id);
        
        return $this->db->count_all_results();
    }


    public function update_reservasi($id, $data){
        $update = $this->db->update('t_reservasi', $data, array('reservasi_id' => $id));

        return $update;
    }

    public function update_pendaftaran($id,$data){
        $update = $this->db->update('t_pendaftaran', $data, array('pendaftaran_id' => $id));

        return $update;
    }
    
    public function insert_assessment_pasien($data){
        $insert = $this->db->insert('t_assstment_kirimpasien',$data);
        return $insert;
    }

    function get_reservasi_id($id){
        $query =  $this->db->get_where('t_reservasi', array('no_pendaftaran' => $id) );
        return $query->row();
    }

     public function getCaraPembayaran($id){
        $query = $this->db->get_where('t_pembayaran', array('pembayaran_id' => $id), 1, 0);
        return $query->row(); 
    }

    public function getDataBidan(){
        $this->db->from('v_pasien_kebidanan');
        $this->db->where('tgl_pendaftaran BETWEEN "'. date('Y-m-d H:i:s', strtotime($tgl_awal." 00:00:01")). '" and "'. date('Y-m-d H:i:s', strtotime($tgl_akhir." 23:59:59")).'"');
        $query = $this->db->get();
        return $query->result(); 
    
    }

    function get_imun_list_print(){
        $this->db->select('*, m_imunisasi.jenis, t_pendaftaran.pasien_id');
        $this->db->join('m_imunisasi','m_imunisasi.imunisasi_id = t_pasienimunisasi.imunisasi_id');
        $this->db->join('t_pendaftaran','t_pendaftaran.pasien_id = t_pasienimunisasi.pasien_id');
        $this->db->from('t_pasienimunisasi');
        $this->db->where('t_pendaftaran.pasien_id = t_pasienimunisasi.pasien_id');
        $query = $this->db->get();
        return $query->result();
    }

    function get_tindakan_ke($id){
        $sql = 'SELECT MAX(imun_ke) AS tindakan_max FROM t_pasienimunisasi WHERE pasien_id = "'.$id.'"';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pnc_ke($id){
        $sql = 'SELECT MAX(pnc_ke) AS tindakan_max FROM t_tindakanpasien_pnc WHERE pasien_id = "'.$id.'"';
        $query = $this->db->query($sql);
        return $query->result();
    }


    function get_faktor_resiko($pendaftaran_id){
        $sql = 'SELECT CONCAT(m_kspr.`kspr_nama`," ", t_skor_kspr.`jml`,"x") AS faktor
            FROM t_skor_kspr
            LEFT JOIN m_kspr ON m_kspr.`kspr_id` = t_skor_kspr.`kspr_id`
            WHERE pendaftaran_id = "'.$pendaftaran_id.'"';
        $query = $this->db->query($sql);
        return $query->result();        
    }

    public function get_driver() {
        return $this->db->get("m_driver")->result();
    }

    public function get_medical_information($pasien_id) {
        $this->db->where("pasien_id", $pasien_id);
        return $this->db->get("m_pasien")->row();
    }

    public function get_perawat() {
        $this->db->where("kelompokdokter_id", 16);
        return $this->db->get("m_dokter")->result();
    }

    public function get_lookup_pasien(){
        $this->db->from('m_lookup')
                 ->where('type', 'kondisi.saat.pulang'); 
        $query = $this->db->get();

        return $query->result();
    }

    public function insert_medical_information($data_tindakanpasien, $pasien_id) {
        $this->db->where("pasien_id", $pasien_id);
        return $this->db->update("m_pasien", $data_tindakanpasien);
    }

    public function insert_ambulance_assistance($data_tindakanpasien, $reservasi_id) {
        $this->db->where("reservasi_id", $reservasi_id);
        return $this->db->update("t_reservasi", $data_tindakanpasien);
    }

    public function insert_fee_dokter_perawat($data_tindakanpasien) {
        return $this->db->insert("t_fee_dokter_perawat", $data_tindakanpasien);
    }

    // START : Model buat ambil harga dokter
    public function get_fee_dokter_by_hotel($id_hotel) {
        $this->db->select('*');
        $this->db->from('fee_dokter_by_hotel');
        $this->db->where('id_hotel', $id_hotel);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_fee_dokter_by_shift($shift) {
        $this->db->select('*');
        $this->db->from('fee_dokter_by_shift');
        $this->db->where('shift', $shift);
        $query = $this->db->get();

        return $query->result();
    }
    // END

    // START : model untuk mengambil data hotel sesuai id
    function get_hotel_by_id($id) {
        $this->db->select('*');
        $this->db->from('m_hotel');
        $this->db->where("id_hotel", $id);
        $query = $this->db->get();

        return $query->row();
    }
    // END

    // START : model untuk mengambil data jenis pasien sesuai id
    function get_jenis_pasien_by_id($id) {
        $this->db->select('*');
        $this->db->from('m_jenis_pasien');
        $this->db->where("id_jenis_pasien", $id);
        $query = $this->db->get();

        return $query->row();
    }
    // END

    // START : model untuk mengambil data jenis pasien sesuai id
    function get_layanan_by_id($id) {
        $this->db->select('*');
        $this->db->from('m_layanan');
        $this->db->where("id_layanan", $id);
        $query = $this->db->get();

        return $query->row();
    }
    // END

    function get_warganegara_by_id($id) {
        $this->db->select('*');
        $this->db->from('m_kewarganegaraan');
        $this->db->where("id", $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_perawat_pasien($pasien_id, $pendaftaran_id) {
        $this->db->select('*');
        $this->db->from('t_fee_dokter_perawat');
        $this->db->where("pasien_id", $pasien_id);
        $this->db->where("pendaftaran_id", $pendaftaran_id);
        $this->db->order_by('id_fee', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_current_status($pendaftaran_id) {
        $this->db->select('status_periksa');
        $this->db->from('t_pendaftaran');
        $this->db->where("pendaftaran_id", $pendaftaran_id);

        $query = $this->db->get();

        return $query->row();
    }
}