<?php $this->load->view('header_iframe');?>
<body>
<div class="white-box" style="height:640px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important"> 
    
    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Penanggung Jawab</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>

        <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
            <div><p id="card_message1" class=""></p></div>
        </div>
        <div class="modal-body" style="background: #fafafa">
            <?php echo form_open('#',array('id' => 'formkirim'))?>
                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                <input type="hidden" id="no_pendaftaran" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>">
                <input type="hidden" id="no_rm" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>">
                <input type="hidden" id="poliruangan" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>">
                <input type="hidden" id="poliruangan_id" name="poliruangan_id" class="validate" value="<?php echo $list_pasien->poliruangan_id; ?>">
                <input type="hidden" id="nama_pasien" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>">
                <input type="hidden" id="umur" name="umur" value="<?php echo $list_pasien->umur; ?>">
                <input type="hidden" id="jenis_kelamin" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>">
                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                <input type="hidden" id="instalasi_id" name="instalasi_id" value="<?php echo $list_pasien->instalasi_id; ?>"> 
                <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                <!-- FORM INTSLASI -->
                <div class="panel-body readonly" style="border: 1px solid #000; margin-bottom: 16px">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Asal Ruangan :  </label>
                               <div class="form-group">
                                   <strong>
                                     <?php
                                    if ($list_pasien->instalasi_id == 1) {
                                        echo "Instalasi Rawat Jalan";
                                    }elseif ($list_pasien->instalasi_id == 2) {
                                        echo "Instalasi Gawat Darurat";
                                    }elseif($list_pasien->instalasi_id == 3){
                                         echo "Instalasi Rawat Inap";
                                    }else{
                                         echo "Instalasi Penunjang";
                                    }
                                    ?>
                                    </strong> 
                               </div>
                           </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tujuan :  </label>
                           </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                           <div class="form-group" id="div_jam_periksa" name="div_jam_periksa">
                                <label class="control-label">Jam Periksa<span style="color: red;">*</span></label>
                                <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                    <input type="text" name="jam_periksa" id="jam_periksa" class="form-control" value="<?= date("H:i") ?>" disabled> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Instalasi<span style="color:red">*</span></label>
                                <select id="tujuan_instalasi" name="tujuan_instalasi" class="form-control">
                                    <option value="" selected>Pilih Instalasi</option>
                                    <?php
                                    $data = $this->Pasien_rj_model->get_instalasi();
                                    foreach($data as $list){
                                        echo "<option value='".$list->instalasi_id."'>".$list->instalasi_nama."</option>";
                                    }
                                    ?>
                                </select> 
                            </div>
                            <div class="form-group">
                                <label>Poliklinik/Ruangan<span style="color:red">*</span></label>
                                <select id="poli_ruangan" name="poli_ruangan" class="form-control">
                                    <option value=""  selected>Pilih Poli/Ruangan</option>
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Dokter Penanggung Jawab<span style="color:red">*</span></label>
                                <select id="dokter_pj" name="dokter_pj" class="form-control" disabled>
                                    <option value=""  selected>Dokter Penanggung Jawab</option>
                                    <?php
                                        $items = $this->Pasien_rj_model->get_dokter_all();
                                        foreach($items as $item) {
                                            if ($item->id_M_DOKTER == $list_pasien->id_M_DOKTER) {
                                                ?>
                                                <option value="<?= $item->id_M_DOKTER; ?>"  selected><?= $item->NAME_DOKTER; ?></option>
                                                <?php
                                            }
                                            else { ?>
                                            <option value="<?= $item->id_M_DOKTER; ?>"><?= $item->NAME_DOKTER; ?></option>
                                            <?php
                                            }
                                        }
                                    ?>
                                </select> 
                            </div>

                            <div class="form-group">
                                <label>Kelas Pelayanan<span style="color:red">*</span></label>
                                <select id="kelas_pelayanan" name="kelas_pelayanan" class="form-control" disabled>
                                    <option value="" disabled selected>Pilih Kelas Pelayanan</option>
                                    <?php
                                        $items = $this->Pasien_rj_model->get_kelas_pelayanan();
                                        foreach($items as $item) {
                                            if ($item->kelaspelayanan_id == $list_pasien->kelaspelayanan_id) {
                                                ?>
                                                <option value="<?= $item->kelaspelayanan_id; ?>"  selected><?= $item->kelaspelayanan_nama; ?></option>
                                                <?php
                                            }
                                            else { ?>
                                            <option value="<?= $item->kelaspelayanan_id; ?>"><?= $item->kelaspelayanan_nama; ?></option>
                                            <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FORM TAB -->
                <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active"><a href="#assestment" class="nav-link" aria-controls="assestment" role="tab" data-toggle="tab" aria-expanded="false" onclick="get_table_assesment()">
                        <span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Assestment</span></a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#background" class="nav-link" aria-controls="background" role="tab" data-toggle="tab" aria-expanded="false" onclick="get_table_background()">
                        <span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Background</span></a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true" onclick="get_table_diagnosa()">
                        <span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Diagnosa</span></a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#tindakans" class="nav-link" aria-controls="tindakans" role="tab" data-toggle="tab" aria-expanded="true" onclick="get_table_tindakan()">
                        <span class="visible-xs"><i class="ti-user"></i></span><span class="hidden-xs">Tindakan</span></a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="assestment">
                        <input id="assessment_pasien_id" type="hidden" name="assessment_pasien_id" value="" class="form-control">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Tanda - Tanda Fital<span style="color: red;"> *</span></label>
                            </div>
                        </div>
                        <table id="table_assesment" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Instalasi</th>
                                    <th>Tensi</th>
                                    <th>Nadi 1</th>
                                    <th>Suhu</th>
                                    <th>Tensi 2</th>
                                    <th>Saturasi</th>
                                    <th>Nyeri</th>
                                    <th>Numeric Wong Baker</th>
                                    <th>Resiko Jatuh</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="10">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Instalasi</th>
                                    <th>Tensi</th>
                                    <th>Nadi 1</th>
                                    <th>Suhu</th>
                                    <th>Tensi 2</th>
                                    <th>Saturasi</th>
                                    <th>Nyeri</th>
                                    <th>Numeric Wong Baker</th>
                                    <th>Resiko Jatuh</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="background">
                        <div class="form-group col-md-12">
                            <label class="control-label">Keluhan saat masuk<span style="color: red;"> *</span></label>
                            <textarea readonly id="keluhan" name="keluhan" class="form-control" rows="2" placeholder="Silahkan isi Keluhan"><?php
                            $pendaftaran = $this->Pasien_rj_model->get_t_pendaftaran($list_pasien->pendaftaran_id);
                            if(!empty($pendaftaran->catatan_keluhan)){
                            echo $pendaftaran->catatan_keluhan;
                            }
                            ?></textarea>
                        </div>
                        <table id="table_background" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Alergi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="3">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Alergi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="diagnosa">
                        <table id="table_diagnosa" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                  <th>Tgl.Diagnosa</th>
                                  <th>Kode Diagnosa</th>
                                  <th>Nama Diagnosa</th>
                                  <th>Kode ICD 10</th>
                                  <th>Nama ICD 10</th>
                                  <th>Jenis Diagnosa</th>
                                  <th>Nama Dokter</th>
                                  <th>Diagnosa Medis</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <tr>
                                     <td colspan="9" align="center">No Data to Display</td>
                                 </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th>Tgl.Diagnosa</th>
                                  <th>Kode Diagnosa</th>
                                  <th>Nama Diagnosa</th>
                                  <th>Kode ICD 10</th>
                                  <th>Nama ICD 10</th>
                                  <th>Jenis Diagnosa</th>
                                  <th>Nama Dokter</th>
                                  <th>Diagnosa Medis</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tindakans">
                        <table id="table_tindakan" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Tgl.Tindakan</th>
                                    <th>Nama Tindakan</th>
                                    <th>Jumlah Tindakan</th>
                                    <th>Cyto</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="4">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th>Tgl.Tindakan</th>
                                    <th>Nama Tindakan</th>
                                    <th>Jumlah Tindakan</th>
                                    <th>Cyto</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                </div>
                <!-- FORM PERHATIAN -->
                <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Hal Yang Harus Diperhatikan :  </label>
                               <div class="form-group">
                                   <textarea class="form-control" id="halperhatian" name="halperhatian" rows="2"></textarea>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
                <!-- FORM TINDAKAN -->
                <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <label>Rencana Tindakan :  </label>
                                <table id="table_rencana_tindakan" class="table table-striped dataTable" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="20%">Tindakan ID</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th width="20%">Tindakan ID</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>  
                        </div>
                    </div>
                </div>
            <?php echo form_close();?> 
        </div>
    </div>
</div>

<?php $this->load->view('footer_iframe_inap');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatjalan/pasien_rj/v_transfer_before.js"></script>
</body>
 
</html>