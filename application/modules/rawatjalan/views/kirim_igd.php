<?php $this->load->view('header_iframe');?>
<body>


<div class="white-box" style="height:640px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important"> 
    
    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>


        <div class="modal-body" style="background: #fafafa"> 
            <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                <div><p id="card_message1" class=""></p></div>
            </div>
            <?php echo form_open('#',array('id' => 'formkirim'))?>
                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                <input type="hidden" id="no_pendaftaran" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                <input type="hidden" id="no_rm" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                <input type="hidden" id="poliruangan" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                <input type="hidden" id="poliruangan_id" name="poliruangan_id" class="validate" value="<?php echo $list_pasien->poliruangan_id; ?>" readonly>
                <input type="hidden" id="nama_pasien" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                <input type="hidden" id="umur" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                <input type="hidden" id="jenis_kelamin" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                <input type="hidden" id="instalasi_id" name="instalasi_id" value="<?php echo $list_pasien->instalasi_id; ?>"> 
                <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>" readonly>
                <!-- FORM INTSLASI -->
                <div class="panel-body readonly" style="border: 1px solid #000; margin-bottom: 16px">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Asal Ruangan :  </label>
                               <div class="form-group">
                                   <strong>
                                     <?php
                                    if ($list_pasien->instalasi_id == 1) {
                                        echo "Instalasi Rawat Jalan";
                                    }elseif ($list_pasien->instalasi_id == 2) {
                                        echo "Instalasi Gawat Darurat";
                                    }elseif($list_pasien->instalasi_id == 3){
                                         echo "Instalasi Rawat Inap";
                                    }else{
                                         echo "Instalasi Penunjang";
                                    }
                                    ?>
                                    </strong> 
                               </div>
                           </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                                <label class="control-label">Jam Periksa<span style="color: red;">*</span></label>
                                <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                    <input type="text" name="jam_periksa" id="jam_periksa" class="form-control" value="<?= date("H:i") ?>"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Poliklinik/Ruangan<span style="color:red">*</span></label>
                                <select id="poli_ruangan" name="poli_ruangan" class="form-control" onchange="getKelasPoliruangan_kk()">
                                    <?php
                                    $list_ruangan = $this->Pasien_rj_model->get_ruangan_igd();
                                    echo '<option value="'.$list_ruangan[0]->poliruangan_id.'">Pilih Poli/Ruangan</option>';
                                    foreach($list_ruangan as $list){
                                        echo "<option value='".$list->poliruangan_id."' selected>".$list->nama_poliruangan."</option>";
                                    }
                                    ?>
                                </select> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kelas Pelayanan<span style="color:red">*</span></label>
                                <select id="kelas_pelayanan" name="kelas_pelayanan" class="form-control" onchange="getKamar_kk()">
                                    <?php
                                    $list_kelas = $this->Pasien_rj_model->get_kelas_poliruangan_nofilter();
                                    echo '<option value="'.$list_kelas[0]->kelaspelayanan_id.'"  selected>Pilih Kelas Pelayanan</option>';
                                    foreach($list_kelas as $list){
                                        echo "<option value='".$list->kelaspelayanan_id."'>".$list->kelaspelayanan_nama."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <!-- <div class="form-group">
                                <label>Kamar<span style="color: red;"> *</span></label>
                                <select id="kamarruangan" name="kamarruangan" class="form-control">
                                    <option value=""  selected>Pilih Kamar</option>
                                    <?php
                                    // $list_kamar = $this->Pasien_rj_model->get_kamar_nofilter();
                                    // foreach($list_kamar as $list){
                                    //     $status = $list->status_bed == '1' ? 'IN USE' : 'OPEN';
                                    //     if($list->status_bed == '0'){
                                    //         echo "<option value='".$list->kamarruangan_id."'>".$list->no_kamar." - ".$list->no_bed."(".$status.")</option>";
                                    //     }else{
                                    //         echo "<option value='".$list->kamarruangan_id."' disabled>".$list->no_kamar." - ".$list->no_bed."(".$status.")</option>";
                                    //     }
                                    // }
                                    ?>
                                </select>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- FORM TAB -->
                <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active"><a href="#assestment" class="nav-link" aria-controls="assestment" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Assestment</span></a></li>
                    <li role="presentation" class="nav-item"><a href="#background" class="nav-link" aria-controls="background" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Background</span></a></li>
                    <li role="presentation" class="nav-item"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Diagnosa</span></a></li>
                    <li role="presentation" class="nav-item"><a href="#tindakans" class="nav-link" aria-controls="tindakans" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-user"></i></span><span class="hidden-xs">Tindakan</span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="assestment">
                        <form action="#" id="fmCreateAssesmentPasien">
                        <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                        <input id="assessment_pasien_id" type="hidden" name="assessment_pasien_id" value="" class="form-control">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Tanda - Tanda Fital<span style="color: red;"> *</span></label>
                            </div>
                        </div>
                        <table id="table_assesment" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Instalasi</th>
                                    <th>Tensi</th>
                                    <th>Nadi 1</th>
                                    <th>Suhu</th>
                                    <th>Tensi 2</th>
                                    <th>Saturasi</th>
                                    <th>Nyeri</th>
                                    <th>Numeric Wong Baker</th>
                                    <th>Resiko Jatuh</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="10">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Instalasi</th>
                                    <th>Tensi</th>
                                    <th>Nadi 1</th>
                                    <th>Suhu</th>
                                    <th>Tensi 2</th>
                                    <th>Saturasi</th>
                                    <th>Nyeri</th>
                                    <th>Numeric Wong Baker</th>
                                    <th>Resiko Jatuh</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="row">
                            <div class="form-group col-md-5">
                                <input id="tensi" type="text" name="tensi" value="" class="form-control" placeholder="Tensi">
                            </div>
                            <div class="form-group col-md-1">
                                <span>mmHG</span>
                            </div>

                            <div class="form-group col-md-5">
                                <input id="nadi_2" type="text" name="nadi_2" value="" class="form-control" placeholder="Tensi 2">
                            </div>
                            <div class="form-group col-md-1">
                                <span>mmHG</span>
                            </div>
                            
                            <div class="form-group col-md-5">
                                <input id="nadi_1" type="text" name="nadi_1" value="" class="form-control" placeholder="Nadi">
                            </div>
                            <div class="form-group col-md-1">
                                <span>x/menit</span>
                            </div>
                            <div class="form-group col-md-5">
                                <input id="suhu" type="text" name="suhu" value="" class="form-control" placeholder="Suhu">
                            </div>
                            <div class="form-group col-md-1">
                                <span>°C</span>
                            </div>

                            <div class="form-group col-md-5">
                                <input id="penggunaan" type="text" name="penggunaan" value="" class="form-control" placeholder="Penggunaan 02">
                            </div>
                            <div class="form-group col-md-1">
                                <span>It/menit</span>
                            </div>

                            <div class="form-group col-md-5">
                                <input id="saturasi" type="text" name="saturasi" value="" class="form-control" placeholder="Saturasi 02">
                            </div>
                            <div class="form-group col-md-1">
                                <span>%</span>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="nyeri" name="nyeri" class="form-control select2">
                                        <option value="" selected>Pilih Nyeri</option>
                                        <?php
                                        $list_nyeri = $this->Pasien_rj_model->get_reference_where('m_lookup','type','assesstment.nyeri');
                                        foreach($list_nyeri as $list){
                                            echo "<option value='".$list->code."'>".$list->name."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <input type="text" name="numeric_wong_baker_name" id="numeric_wong_baker_name" disabled class="form-control">
                            </div>
                            <div class="form-group col-md-2">
                                <input type="hidden" name="numeric_wong_baker" id="numeric_wong_baker">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_wong_baker" data-backdrop="static" data-keyboard="false">Pilih skala nyeri</button>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="">Resiko Jatuh</label>
                                <select name="resiko_jatuh" id="resiko_jatuh" class="form-control">
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <table id="table_asessment_therapy" class="table table-striped dataTable" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Program Therapy</th>
                                        <th width="30%">Hapus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2">No data to display</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Program Therapy</th>
                                        <th width="30%">Hapus</th>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="control-label">Pilih  Program Therapy<span style="color: red;"> *</span></label>
                                <select id="nama_program" name="nama_program" class="form-control select2" onchange="$('#nama_therapy').val($('#nama_program option:selected').text())" placeholder="Pilih Therapy">
                                    <option value="#" selected>Pilih Therapy</option>
                                    <?php
                                    // $list_therapy = $this->list_phatologi_model->get_theraphy_pasien();
                                    $list_therapy = $this->Pasien_rj_model->get_reference('m_program_therapy');
                                    foreach($list_therapy as $list){
                                        echo "<option value='".$list->program_therapy_id."'>".$list->nama."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2" style="margin-top:27px;">
                                <button type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" id="addTherapy"><i class="fa fa-plus"></i> TAMBAH</button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="button" style="width: 100%; background-color: #01c0c8;" class="btn btn-success" id="saveAssessment" name="saveAssessment" onclick="simpan_assesment()"><i class="fa fa-floppy-o"></i> SIMPAN ASSESSMENT</button>
                            </div>
                            <div class="col-md-6">
                                <button style="width: 100%" type="button" class="btn btn-success" id="RiwayatAssestment">RIWAYAT ASSESSMENT</button>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="background">
                        <div class="form-group col-md-12">
                            <label class="control-label">Keluhan saat masuk<span style="color: red;"> *</span></label>
                            <textarea id="keluhan" name="keluhan" class="form-control" rows="2" placeholder="Silahkan isi Keluhan"><?php
                            $t_pendaftaran = $this->Pasien_rj_model->get_t_pendaftaran($list_pasien->pendaftaran_id);
                            if(!empty($t_pendaftaran->catatan_keluhan)){
                            echo $t_pendaftaran->catatan_keluhan;
                            }
                            ?></textarea>
                        </div>
                        <table id="table_background_pasienrd_kk" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Alergi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="3">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Alergi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="diagnosa">
                        <table id="table_diagnosa_pasienrd_kk" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                  <th>Tgl.Diagnosa</th>
                                  <th>Kode Diagnosa</th>
                                  <th>Nama Diagnosa</th>
                                  <th>Kode ICD 10</th>
                                  <th>Nama ICD 10</th>
                                  <th>Jenis Diagnosa</th>
                                  <th>Nama Dokter</th>
                                  <th>Diagnosa Medis</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <tr>
                                     <td colspan="9" align="center">No Data to Display</td>
                                 </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th>Tgl.Diagnosa</th>
                                  <th>Kode Diagnosa</th>
                                  <th>Nama Diagnosa</th>
                                  <th>Kode ICD 10</th>
                                  <th>Nama ICD 10</th>
                                  <th>Jenis Diagnosa</th>
                                  <th>Nama Dokter</th>
                                  <th>Diagnosa Medis</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tindakans">
                        <table id="table_tindakan_pasienrd_kk" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Tgl.Tindakan</th>
                                    <th>Nama Tindakan</th>
                                    <th>Jumlah Tindakan</th>
                                    <th>Cyto</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="7">No data to display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th>Tgl.Tindakan</th>
                                    <th>Nama Tindakan</th>
                                    <th>Jumlah Tindakan</th>
                                    <th>Cyto</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                </div>
                <!-- FORM PERHATIAN -->
                <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Hal Yang Harus Diperhatikan :  </label>
                               <div class="form-group">
                                   <textarea class="form-control" id="halperhatian" name="halperhatian" rows="2"></textarea>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
                <!-- FORM TINDAKAN -->
                <div class="panel-body" style="border: 1px solid #000; margin-bottom: 16px">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <table id="table_rencana_tindakan" class="table table-striped dataTable" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Tindakan</th>
                                            <th width="20%">Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Tindakan</th>
                                            <th width="20%">Hapus</th> 
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>  
                        </div>
                        <div class="form-group col-md-12">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="control-label">ID Tindakan<span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" id="kode_tindakan" name="kode_tindakan" placeholder="Kode Tindakan" readonly>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">Tindakan<span style="color: red;">*</span></label>
                                    <div class="input-group">
                                    <input type="text" class="form-control" id="nama_tindakan" name="nama_tindakan" placeholder="Nama Tindakan" readonly>
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" title="Lihat List Tindakan" data-toggle="modal" data-target="#modal_tindakan" data-backdrop="static" data-keyboard="false" onclick="dialogTindakan()"><i class="fa fa-list"></i></button>
                                    </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <!-- <button type="button" style="width: 100%; margin-top: 26px;" class="btn btn-success" id="saveTindakan" onclick="saveRencanaTindakan()"><i class="fa fa-plus"></i> TAMBAH</button>&nbsp -->
                                    <button type="button" style="width: 100%; margin-top: 26px;" class="btn btn-success" onclick="add_tindakan_append()"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php echo form_close();?> 
        </div>
        
    </div>
    <div class="box-footer">
        <button type="button" class="btn btn-success pull-right" onclick="saveIGD()"><i class="fa fa-floppy-o p-r-9"></i > KIRIM K.K IGD</button>
    </div>
</div>

<!-- MODAL LIST TINDAKAN -->
<div id="modal_tindakan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" >
        <div class="modal-header" style="background: #fafafa">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel"><b>List Tindakan</b></h2> </div>
            <div class="modal-body table-responsive" style="background: #fafafa">
               <table id="table_list_tindakan" class="table table-striped dataTable " cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID<br>Tindakan</th>
                            <th>Nama<br>Tindakan</th>
                            <th>Tarif</th>
                            <th>Pilih</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td colspan="5">No Data to Display</td>
                        </tr>
                    </tbody>
                </table>
        </div>

    </div>
</div>
</div>

<!-- MODAL LIST TINDAKAN -->
<div id="modal_wong_baker" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" >
        <div class="modal-header" style="background: #fafafa">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel"><b>Skala Nyeri</b></h2> </div>
            <div class="modal-body">
            <div class="row">
            <?php
                $list_nyeri = $this->Pasien_rj_model->get_reference_where('m_lookup','type','assesstment.numeric.wong');
                foreach($list_nyeri as $list){
                    echo "<img src='". base_url() . $list->description ."' class='col-md-2' title='Skala $list->name' style='cursor: pointer' onclick='setWongBaker($list->lookup_id, $list->name)' data-dismiss='modal' aria-hidden='true'>";
                }
            ?>
            </div>
        </div>
    </div>
</div>
</div>

<?php $this->load->view('footer_iframe');?>    
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatjalan/pasien_rj/kirim_igd.js"></script>
<!-- <script src="<?php echo base_url()?>assets/dist/js/pages/rawatdarurat/pasien_rd/kirim_penunjangrd.js"></script>
 --></body>
 
</html>