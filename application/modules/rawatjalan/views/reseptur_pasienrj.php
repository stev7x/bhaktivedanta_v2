<?php $this->load->view('header_iframe');?>
<style>
    #judul{
        width:250px;
        font-size:12pt;
    }
    #isian{
        width:400px;
        font-size:12pt;
        font-weight:600;
    }
    #separator{
        font-size:12pt;

    }
</style>
<body>


<div class="white-box" style="height:700px; overflow:none; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    
       <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
     <!-- <p class="text-muted m-b-30">Data table example</p> -->
     <div class="row">
        <div class="col-md-12" style="padding-bottom:15px">
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">
                                    <tr>
                                        <td><b>Tgl.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Poliklinik</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>
                                     <tr>
                                        <td><b>Nama Asuransi</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $reservasi->asuransi1; ?> / <?= $reservasi->asuransi2; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No Asuransi</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $reservasi->no_asuransi1; ?> / <?= $reservasi->no_asuransi2; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Hotel / Kamar</b></td>
                                        <td>:</td>
                                        <?php
                                        $data_reservasi = $this->Pasien_rj_model->get_reservasi_id($list_pasien->no_pendaftaran);
                                        $hotel = $this->Pasien_rj_model->get_hotel_by_id($data_reservasi->hotel_id);
                                        ?>
                                        <td style="padding-left: 15px"><?= @$hotel->nama_hotel ." / ". $data_reservasi->room_number; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">
                                    <tr>
                                        <td><b>Nama Pasien</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                        <?php
                                            $tgl_lahir = $list_pasien->tanggal_lahir;
                                            $umur  = new Datetime($tgl_lahir);
                                            $today = new Datetime();
                                            $diff  = $today->diff($umur);
                                            echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
                                        ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Alamat</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_alamat; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kewarganegaraan</b></td>
                                        <td>:</td>
                                        <?php $negara = $this->Pasien_rj_model->get_warganegara_by_id($list_pasien->warga_negara) ?>
                                        <td style="padding-left: 15px"><?= @$negara->nama_negara; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Pasien</b></td>
                                        <td>:</td>
                                        <?php $jenis_pasien = $this->Pasien_rj_model->get_jenis_pasien_by_id($list_pasien->jenis_pasien) ?>
                                        <td style="padding-left: 15px"><?= $jenis_pasien->nama_jenis_pasien ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Call</b></td>
                                        <td>:</td>
                                        <?php $layanan_pasien = $this->Pasien_rj_model->get_layanan_by_id($list_pasien->type_call) ?>
                                        <td style="padding-left: 15px"><?= $layanan_pasien->nama_layanan; ?></td>
                                    </tr>
                                    
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">
                                <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                                <input type="hidden" id="jenis_pasien" name="jenis_pasien" value="<?php echo $list_pasien->jenis_pasien; ?>">
                                <input type="hidden" id="type_call" name="type_call" value="<?php echo $list_pasien->type_call; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="col-md-12" style="margin-top: 10px">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active nav-item">
        <a href="#resep" class="nav-link" aria-controls="resep" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs">PEMBELIAN RESEP</span><span class="hidden-xs"> PEMBERIAN RESEP</span></a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="resep">
            <table id="table_resep_pasienrj" class="table table-striped dataTable" cellspacing="0">
                <thead>
                    <tr>
                        <th>Batch Number</th>
                        <th>Nama Obat</th>
                        <th>Jenis Obat</th>
                        <th>Satuan</th>
                        <th>Jumlah</th>
                        <th>Dosis</th>
                        <th>Pemberian</th>
                        <th>Aturan Pakai</th>
                        <th>Harga</th>
                        <?php if($list_pasien->status_periksa == 'SUDAH BAYAR'){} else { ?>
                        <th>Batal / Hapus</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="7">Tidak ada data yang dapat ditampilkan</td>
                </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Batch Number</th>
                        <th>Nama Obat</th>
                        <th>Jenis Obat</th>
                        <th>Satuan</th>
                        <th>Jumlah</th>
                        <th>Dosis</th>
                        <th>Pemberian</th>
                        <th>Aturan Pakai</th>
                        <th>Harga</th>
                        <?php if($list_pasien->status_periksa == 'SUDAH BAYAR'){} else { ?>
                            <th>Batal / Hapus</th>
                        <?php } ?>
                     </tr>
                 </tfoot>
            </table><br>
             <?php echo form_open('#',array('id' => 'fmCreateResepPasien'))?>
                 <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delres" name="<?php echo $this->security->get_csrf_token_name()?>_delres" value="<?php echo $this->security->get_csrf_hash()?>" />
            <div class="row">

                <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button>
                    <div >
                        <p id="card_message"></p>
                    </div>
                </div>
            <?php if($list_pasien->status_periksa == 'SUDAH BAYAR') {}
            else{ ?>
                <div class="form-group col-sm-6">
                    <label class="control-label">Input Obat<span style="color: red;"> *</span></label>
                    <input type="hidden" name="jenis_barang_id" id="jenis_barang_id">
                    <input type="hidden" name="persediaan_id" id="persediaan_id">
                    <input type="hidden" name="is_bpjs" id="is_bpjs">
                    <input type="hidden" name="stok_id" id="stok_id">

                    <div class="input-group custom-search-form">
                        <input readonly required type="text" class="form-control" placeholder="Nama Obat" name="nama_obat" id="nama_obat"  value="">
                        <span class="input-group-btn">
                            <button class="btn btn-info" title="Lihat List Pasien" data-toggle="modal" data-target="#modal_reseptur" onclick="dialogObat()" type="button">  <i class="fa fa-bars"></i> </button  >
                        </span>
                    </div>
                </div>

                <div class="form-group col-sm-3">
                    <label class="control-label">Jumlah Obat<span style="color: red;"> *</span></label>
                    <input required type="number" min="1" class="form-control" id="jumlah_obat" name="jumlah_obat" placeholder="Jumlah Obat" onchange="countHarga(this);">
                    <input id="obat_id" type="hidden" name="obat_id" value="" readonly>
                    <input id="current_stok" type="hidden" name="current_stok" value="" readonly>
                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                    <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                </div>
                <div class="form-group col-sm-3">
                    <label class="control-label">Satuan</label>
                    <input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan" disabled>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-3">
                    <label class="control-label">Dosis</label>
                    <input type="text" class="form-control" id="custom_dosis_id" name="custom_dosis_id" placeholder="Dosis" style="display: none;">
                    <select name="dosis_id" id="dosis_id" class="form-control" onchange="customDosis(this);">
                        <option value="" selected>Pilih Dosis</option>
                        <?php
                            $list = $this->Models->all("farmasi_dosis");
                            foreach ($list as $key => $value) {
                                echo "<option value=".$value['id'].">".$value['konten']."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group col-sm-1" style="padding-top: 34px;">
                    <input type="checkbox" name="custom_dosis" id="custom_dosis" style="margin-right: 1px;" onchange="editDosis(this);">
                    <label class="control-label" for="custom_dosis">Edit</label>
                </div>

                <div class="form-group col-sm-3">
                    <label class="control-label">Pemberian</label>
                    <input type="text" class="form-control" id="custom_pemberian_id" name="custom_pemberian_id" placeholder="Pemberian" style="display: none;">
                    <select name="pemberian_id" id="pemberian_id" class="form-control" onchange="customPemberian(this);">
                        <option value="" selected>Pilih Pemberian</option>
                        <?php
                        $list = $this->Models->all("farmasi_pemberian");
                        foreach ($list as $key => $value) {
                            echo "<option value=".$value['id'].">".$value['konten']."</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-sm-1" style="padding-top: 34px;">
                    <input type="checkbox" name="custom_pemberian" id="custom_pemberian" style="margin-right: 1px;" onchange="editPemberian(this);">
                    <label class="control-label" for="custom_pemberian">Edit</label>
                </div>

                <div class="form-group col-sm-3">
                    <label class="control-label">Aturan Pakai</label>
                    <input type="text" class="form-control" id="custom_aturan_pakai_id" name="custom_aturan_pakai_id" placeholder="Aturan Pakai" style="display: none;">
                    <select name="aturan_pakai_id" id="aturan_pakai_id" class="form-control" onchange="customAturanPakai(this);">
                        <option value="" selected>Pilih Aturan Pakai</option>
                        <?php
                            $list = $this->Models->all("farmasi_aturan_penggunaan");
                            foreach ($list as $key => $value) {
                                echo "<option value=".$value['id'].">".$value['konten']."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group col-sm-1" style="padding-top: 34px;">
                    <input type="checkbox" name="custom_aturan_pakai" id="custom_aturan_pakai" style="margin-right: 1px;" onchange="editAturanPakai(this);">
                    <label class="control-label" for="custom_aturan_pakai">Edit</label>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="control-label">Harga Jual<span style="color: red;"> *</span></label>
                    <input required type="number" class="form-control" id="harga_jual" name="harga_jual" placeholder="Harga Obat" readonly>
                    <input type="checkbox" name="apotek_luar" id="apotek_luar" ><label for="apotek_luar" style="color: red; font-size: 14px; margin-top-2px; "> * Jika Pembelian Obat Dari Luar</label>
                    <input type="hidden" id="harga" name="harga">
                </div>

                <div class="form-group col-sm-12">
                    <button type="button" class="btn btn-success" id="saveResep"><i class="fa fa-floppy-o"></i> TAMBAH</button>
                </div>
            </div>

            <?php } echo form_close()?>

        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  </div>
 </div>




<div id="modal_reseptur" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
    <!-- style="overflow: auto;height: 365px;width:687px;margin-left:-97px"   -->
        <div class="modal-content" >
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>List Obat</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                   <table id="table_list_obat" class="table table-striped dataTable " cellspacing="0">
                        <thead>
                            <tr>
                                <th>Batch Number</th>
                                <th>Nama Obat</th>
                                <th>Jenis Obat</th>
                                <th>Persediaan Stok</th>
                                <th>Suplier</th>
                                <th>Expired Date</th>
                                <th>Pilih</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6">No Data to Display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Batch Number</th>
                                <th>Nama Obat</th>
                                <th>Jenis Obat</th>
                                <th>Persediaan Stok</th>
                                <th>Suplier</th>
                                <th>Expired Date</th>
                                <th>Pilih</th>
                            </tr>
                        </tfoot>
                    </table>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatjalan/pasien_rj/periksa_pasienrj1.js"></script>
</body>

</html>
