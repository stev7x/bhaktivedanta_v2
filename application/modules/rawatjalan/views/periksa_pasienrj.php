
<?php $this->load->view('header_iframe');?>
<style>
    #judul{
        width:250px;
        font-size:12pt;
    }
    #isian{
        width:400px;
        font-size:12pt;
        font-weight:600;
    }
    #separator{
        font-size:12pt;

    }
</style>
<body>

<?php 
// print_r($reservasi);
 ?>

<div class="white-box" style="height:740px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">
    <!-- <h3 class="box-title m-b-0">Data Pasien</h3> -->
    <!-- <p class="text-muted m-b-30">Data table example</p> -->

    <div class="row">
        <div class="col-md-12 col-sm-12" style="padding-bottom:15px">
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <!-- <div class="col-sm-6"> -->
                                    <table  width="400px" align="right" style="font-size:16px;text-align: left;">
                                        <tr>
                                            <td><b>Tgl.Pendaftaran</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>No.Pendaftaran</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>No.Rekam Medis</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Poliklinik</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Dokter Pemeriksa</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Nama Asuransi</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->nama_asuransi; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>No Asuransi</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->no_asuransi; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Hotel / Kamar</b></td>
                                            <td>:</td>
                                            <?php
                                                $data_reservasi = $this->Pasien_rj_model->get_reservasi_id($list_pasien->no_pendaftaran);
                                                $hotel = $this->Pasien_rj_model->get_hotel_by_id($data_reservasi->hotel_id);
                                            ?>
                                            <td style="padding-left: 15px"><?= @$hotel->nama_hotel ." / ". $data_reservasi->room_number; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Jenis Pasien</b></td>
                                            <td>:</td>
                                            <?php $jenis_pasien = $this->Pasien_rj_model->get_jenis_pasien_by_id($list_pasien->jenis_pasien) ?>
                                            <td style="padding-left: 15px"><?= $jenis_pasien->nama_jenis_pasien ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Type Call</b></td>
                                            <td>:</td>
                                            <?php $layanan_pasien = $this->Pasien_rj_model->get_layanan_by_id($list_pasien->type_call) ?>
                                            <td style="padding-left: 15px"><?= $layanan_pasien->nama_layanan; ?></td>
                                        </tr>
                                    </table>
                                <!-- </div> -->
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <!-- <div class="col-sm-6"> -->
                                    <table  width="400px" style="font-size:16px;text-align: left;">
                                        <tr>
                                            <td><b>Nama Pasien</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Umur</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px">
                                                <?php
                                                    $tgl_lahir = $list_pasien->tanggal_lahir;
                                                    $umur  = new Datetime($tgl_lahir);
                                                    $today = new Datetime();
                                                    $diff  = $today->diff($umur);
                                                    echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari";
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>Jenis Kelamin</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Alamat</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->pasien_alamat; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Kewarganegaraan</b></td>
                                            <td>:</td>
                                            <?php $negara = $this->Pasien_rj_model->get_warganegara_by_id($list_pasien->warga_negara) ?>
                                            <td style="padding-left: 15px"><?= @$negara->nama_negara; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Kelas Pelayanan</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Type Pembayaran</b></td>
                                            <td>:</td>
                                            <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                        </tr>


                                    </table>
                                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                    <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">
                                    <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                    <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                    <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                    <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                    <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                    <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                    <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">
                                    <input type="hidden" id="type_pembayaran" name="type_pembayaran" value="<?php echo $list_pasien->type_pembayaran; ?>">
                                    <input type="hidden" id="jenis_pasien" name="jenis_pasien" value="<?php echo $list_pasien->jenis_pasien; ?>">
                                    <input type="hidden" id="type_call" name="type_call" value="<?php echo $list_pasien->type_call; ?>">
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#tabs-pemeriksaan-fisik" class="nav-link" aria-controls="pemeriksaan-fisik" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Pemeriksaan Fisik</span></a></li>
                <li role="presentation" class="nav-item"><a href="#diagnosa" class="nav-link" aria-controls="diagnosa" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-user-md"></i></span><span class="hidden-xs"> Diagnosa</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-tindakan" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Tindakan</span></a></li>
                <?php if($list_pasien->nama_poliruangan == "POLI ANAK") { ?>
                    <li role="presentation" class="nav-item"><a href="#tabs-imun" class="nav-link" aria-controls="imun" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Imun</span></a></li>
                <?php } ?>
                <?php if($list_pasien->nama_poliruangan == "POLI OBGYN") {?>
                    <li role="presentation" class="nav-item"><a href="#tabs-kebidanan" onclick="tampilFormBidan();skorKSPR();faktorResiko()" class="nav-link" aria-controls="kebidanan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Kebidanan</span></a></li>
                    <li role="presentation" class="nav-item"><a href="#tabs-imun" class="nav-link" aria-controls="imun" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Imun</span></a></li>
                <?php } ?>
                <li role="presentation" class="nav-item"><a href="#tabs-laboratorium" class="nav-link" aria-controls="tindakan" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Laboratorium</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-medical-information" class="nav-link" aria-controls="tabs-medical-information" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Medical Information</span></a></li>
                <li role="presentation" class="nav-item"><a href="#tabs-ambulance-assistance" class="nav-link" aria-controls="tabs-ambulance-assistance" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user-md"></i></span> <span class="hidden-xs">Ambulance Assistance</span></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane" id="diagnosa">
                    <table id="table_diagnosa_pasienrj" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>
                                <th>Kode ICD 10</th>
                                <th>Nama ICD 10</th>
                                <th>Jenis Diagnosa</th>
                                <th>Diagnosa By</th>
                                <?php
                                    if($list_pasien->status_periksa == "SUDAH BAYAR"){}
                                    else{
                                 ?>
                                <th>Batal / Hapus</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="6">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Diagnosa</th>
                                <th>Kode Diagnosa</th>
                                <th>Nama Diagnosa</th>
                                <th>Kode ICD 10</th>
                                <th>Nama ICD 10</th>
                                <th>Jenis Diagnosa</th>
                                <th>Diagnosa By</th>
                                <?php
                                    if($list_pasien->status_periksa == "SUDAH BAYAR"){}
                                    else{
                                 ?>
                                <th>Batal / Hapus</th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateDiagnosaPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="pasien_id" name="pasien_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">
                    <input type="hidden" id="dokter_id_diagnosa" name="dokter_id_diagnosa" value="<?php echo $list_pasien->id_M_DOKTER; ?>">
                    <input type="hidden" id="diagnosa_id" name="diagnosa_id">
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div >
                            <p id="card_message"></p>
                        </div>
                    </div>
                    <?php
                        if($list_pasien->status_periksa == "SUDAH BAYAR"){}
                        else {
                    ?>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="control-label">
                            Kode Diagnosa<span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_diagnosa" name="kode_diagnosa" placeholder="Kode Diagnosa">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button" title="Lihat List Diagnosa" data-toggle="modal" data-target="#modal_diagnosa" onclick="dialogDiagnosa(<?php echo $list_pasien->pendaftaran_id; ?>)"><i class="fa fa-list"></i></button>
                              </span>
                            </div>

                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label">Nama Diagnosa<span style="color: red;"> *</span></label>
                            <input id="nama_diagnosa" type="text" name="nama_diagnosa" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label class="control-label">
                            Kode Diagnosa ICD 10<span style="color: red;">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="kode_diagnosa_icd10" name="kode_diagnosa_icd10" placeholder="Kode Diagnosa">
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label">Nama Diagnosa ICD 10<span style="color: red;"> *</span></label>
                            <input id="nama_diagnosa_icd10" type="text" name="nama_diagnosa_icd10" class="form-control" placeholder="Nama Diagnosa" value="">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label">Jenis Diagnosa<span style="color: red;"> *</span></label>
                            <select id="jenis_diagnosa" name="jenis_diagnosa" class="form-control" >
                                <option value="" disabled selected>Pilih Jenis Diagnosa</option>
                                <option value="1">Diagnosa Utama</option>
                                <option value="2">Diagnosa Tambahan</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <button type="button" style="width: 100%; margin-top: 26px;" class="btn btn-success" id="saveDiagnosa">
                                <i class="fa fa-plus"></i> TAMBAH
                            </button>
                        </div>
                    </div>
                    <?php } ?>
                        <?php echo form_close();?>
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane active" id="tabs-pemeriksaan-fisik">
                    <table id="table_pemeriksaan_fisik" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl.Periksa</th>
                                <th>BB</th>
                                <th>TB</th>
                                <th>Sistole</th>
                                <th>Diastole</th>
                                <th>Anamnesis</th>
                                <th>Keterangan</th>
                                <?php
                                    if($list_pasien->status_periksa == "SUDAH BAYAR"){}
                                    else{
                                ?>
                                <th>Batal/Hapus</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                 <td colspan="6">No Data to Display</td>
                             </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Tgl.Periksa</th>
                                <th>BB</th>
                                <th>TB</th>
                                <th>Sistole</th>
                                <th>Diastole</th>
                                <th>Anamnesis</th>
                                <th>Keterangan</th>
                                <?php
                                    if($list_pasien->status_periksa == "SUDAH BAYAR"){}
                                    else{
                                ?>
                                <th>Batal/Hapus</th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table><br>
                <?php echo form_open('#',array('id' => 'fmCreatePemeriksaanFisik'))?>
                    <div class="col-md-12">
                        <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                        <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_fisik" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" >&times;</button>
                            <div>
                                <p id="card_message_fisik"></p>
                            </div>
                        </div>
                        <?php if($list_pasien->status_periksa == "SUDAH BAYAR"){} else { ?>
                        <div class="form-group col-md-3">
                            <label for="">BB<span style="color: red;">*</span></label>
                            <input type="number" name="bb" id="bb" class="form-control" value="0" placeholder="KG" min="0">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">TB<span style="color: red;">*</span></label>
                            <input type="number" name="tb" id="tb" class="form-control" value="0" placeholder="CM" min="0">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Sistole<span style="color: red;">*</span></label>
                            <input type="number" name="sistole" id="sistole" class="form-control" value="60" placeholder="mm.Hg">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Diastole<span style="color: red;">*</span></label>
                            <input type="number" name="diastole" id="diastole" class="form-control" value="40" placeholder="mm.Hg">
                        </div> 
                        <?php } ?>
                    </div>
                    <?php if($list_pasien->status_periksa == "SUDAH BAYAR"){} else { ?>
                    <div class="col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Anamnesis</label>
                            <textarea rows="" cols="" class="form-control" name="anamnesis" id="anamnesis" placeholder="Anamnesis"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Keterangan</label>
                            <textarea class="form-control" rows="" cols="" name="keterangan" id="keterangan" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <button type="button" class="btn btn-success" id="savePemeriksaanFisik"><i class="fa fa-floppy-o"></i> Simpan</button>
                        </div>
                    </div>
                    <?php } ?>
                <?php echo form_close();?>

                </div>

                <div role="tabpanel" class="tab-pane" id="tabs-kebidanan">
                    <ul class="nav customtab nav-tabs" role="tablist">

                        <li id="form_tabs" role="presentation" class="nav-item" style="display:none;width: 100%;text-align: center;"><a href="#form" id="form_a" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">FORM ISIAN</span></a></li>
                        <li id="result_tabs" role="presentation" class="nav-item" style="display:none;width: 100%;text-align: center;"><a href="#result" id="result_a" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">HASIL ISIAN</span></a></li>
                    </ul>

                    <!-- tabs sub kebidanan  -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="form" style="display:none;">
                            <div class="col-md-12">
                                <h3 style="font-weight:900;">KB</h3>
                                <hr style="margin-top:-9px;margin-bottom:10px;">
                            <?php echo form_open('#',array('id' => 'fmCreateKB'))?>
                                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                                <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                                <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" name="dokter_id_kb" id="dokter_id_kb" value="<?php echo $list_pasien->id_M_DOKTER; ?>">
                                <input type="hidden" name="kelaspelayanan_id" id="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">

                                <div class="form-group col-md-3">
                                    <label for="">Tipe KB<span style="color: red;"> *</span></label>
                                    <select name="tipe_kb" id="tipe_kb" class="form-control">
                                        <option value="" disabled selected>Pilih Tipe</option>
                                        <option value="1">Prenatal</option>
                                        <option value="2">Pasca Persalinan</option>
                                        <option value="3">Pasca Keguguran</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-3">
                                    <label>Tindakan KB<span style="color: red;"> *</span></label>
                                    <select name="tindakan_kb" id="tindakan_kb" class="form-control select2" >
                                        <option value="" selected>Pilih Tindakan KB</option>
                                        <optgroup label="Pilih KB">
                                        <?php
                                            $list_kb = $this->Pasien_rj_model->get_kb();
                                            foreach ($list_kb as $list) {
                                                echo "<option value='".$list->vaksinasi_id."'>".$list->vaksinasi_nama." - Merk (".$list->vaksinasi_merk.")</option>";
                                            }
                                        ?>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <button type="button" class="btn btn-success" style="margin-top:25px;" id="saveKB"><i class="fa fa-plus"></i> TAMBAH</button>
                                </div>
                            <?php echo form_close();?>
                            </div>
                            <div class="col-md-12">
                                 <table id="table_kb" class="table table-striped dataTable" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl.Periksa</th>
                                            <th>Type KB</th>
                                            <th>Tindakan</th>
                                            <th>Batal/Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6">No Data to Display</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl.Periksa</th>
                                            <th>Type KB</th>
                                            <th>Tindakan</th>
                                            <th>Batal/Hapus</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <h3 style="font-weight:900;">PNC</h3>
                                <hr style="margin-top:-9px;margin-bottom:10px;">
                            <?php echo form_open('#',array('id' => 'fmCreatePNC'))?>
                                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                                <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                                <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <div class="form-group col-md-6">
                                        <label>TINDAKAN</label>
                                         <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                                        <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly>
                                        <select name="tindakan_pnc" id="tindakan_pnc" class=" form-control select2">
                                            <optgroup label="Pilih Tindakan PNC">
                                            <?php
                                                $list_imun = $this->Pasien_rj_model->get_pnc();
                                                foreach ($list_imun as $list) {
                                                    echo "<option value='".$list->daftartindakan_id."'>".$list->daftartindakan_nama."</option>";
                                                }
                                                ?>
                                            </optgroup>
                                        </select>
                                </div>
                                 <div class="form-group col-md-2">
                                    <button type="button" class="btn btn-success" style="margin-top:25px;" id="savePNC"><i class="fa fa-plus"></i> TAMBAH</button>
                                </div>
                            <?php echo form_close();?>

                            </div>
                            <div class="col-md-12">
                                 <table id="table_pnc" class="table table-striped dataTable" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl.Periksa</th>
                                            <th>Tindakan</th>
                                            <th>Batal/Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6">No Data to Display</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl.Periksa</th>
                                            <th>Tindakan</th>
                                            <th>Batal/Hapus</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="col-md-12">
<!-- =========================== for kspr dulu ===================================== -->
             <!-- <php echo form_open('#',array('id' => 'fmCreatePemeriksaanKebidanan'))?>
                                <input type="hidden" id="<php echo $this->security->get_csrf_token_name()?>_deltind" name="<php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                                <input type="hidden" name="pasien_id" id="pasien_id" value="<php echo $list_pasien->pasien_id; ?>">
                                <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_id_bidan" name="dokter_id_bidan" value="<php echo $list_pasien->id_M_DOKTER; ?>">
                                <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_bidan" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                    <div >
                                        <p id="card_message_bidan"></p>
                                    </div>
                                </div>
                                <h3 style="font-weight:900;">KSPR</h3>
                                <hr style="margin-top:-9px;margin-bottom:10px;">
                                <div class="form-group col-md-6">
                                    <label for="">Score KSPR</label>
                                    <select multiple name="kspr[]" id="kspr" class="select2" onchange="selectFunction(event)">
                                            <optgroup label="Pilih KSPR">
                                            <option value="2" selected="true" disabled="disabled">Default</option>

                                            <php
                                                 $list_kspr = $this->Pasien_rj_model->get_kspr();
                                            foreach ($list_kspr as $list) {
                                                echo "<option value='".$list->kspr_id."' data-skor='".$list->kspr_jumlah."'>".$list->kspr_nama." skor : (".$list->kspr_jumlah.")</option>";
                                            }
                                                ?>
                                            </optgroup>
                                        </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="">Score KSPR<span style="color: red;"> *</span></label>
                                    <input type="text" readonly name="jum_skor" id="jum_skor" value="2" class="form-control" placeholder="Score KSPR">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="">Score KSPR</label>
                                    <input type="" name="" value="" class="form-control" placeholder="Score KSPR" readonly>
                                </div>
                                <div class="form-group col-md-2">
                                    <button class="btn btn-danger" style="margin-top:25px;" type="button" onclick="ubah()"><i class="fa fa-refresh" ></i> RESET</button> -->
<!-- =========================== for kspr dulu ===================================== -->


<!-- ================================= form kspr baru ==========================================  -->
            <?php echo form_open('#',array('id' => 'fmCreateSkorKSPR'))?>
                                <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                                <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                                <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_bidan" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" >&times;</button>
                                    <div >
                                        <p id="card_message_bidan"></p>
                                    </div>
                                </div>
                                <h3 style="font-weight:900;">KSPR</h3>
                                <hr style="margin-top:-9px;margin-bottom:10px;">
                                <div class="form-group col-md-6">
                                    <label for="">Score KSPR</label>
                                    <select name="kspr" id="kspr" class="select2 form-control" onchange="putKSPR(event)">
                                            <optgroup label="Pilih KSPR">
                                                <option value=""selected>Tidak Memilih</option>

                                                <?php
                                                    $list_kspr = $this->Pasien_rj_model->get_kspr();
                                                    foreach ($list_kspr as $list) {
                                                        echo "<option value='".$list->kspr_id."' data-val='".$list->kspr_nama."' data-skor='".$list->kspr_jumlah."'>".$list->kspr_nama." skor : (".$list->kspr_jumlah.")</option>";
                                                    }
                                                ?>
                                            </optgroup>
                                    </select>
                                    <input type="hidden" name="skor_kspr" id="skor_kspr" value="">
                                    <input type="hidden" name="nama_kspr" id="nama_kspr" value="">
                                </div>
                                <div class="form-group col-md-2">
                                    <!-- <label for="">Jumlah<span style="color: red;"> *</span></label> -->
                                    <label for="">Jumlah</label>
                                    <input type="number" max="2" name="jml" id="jml" class="form-control" placeholder="Jumlah" onkeyup="jml_kspr()">
                                    <input type="hidden" name="sum_kspr" id="sum_kspr" value="">
                                </div>
                                <div class="form-group col-md-4">
                                    <!-- <button id="saveSkorKSPR" class="btn btn-success" onclick="appendFaktorResiko()" style="margin-top:25px;" type="button"><i class="fa fa-plus" ></i> TAMBAH</button> -->
                                    <button id="saveSkorKSPR" class="btn btn-success" style="margin-top:25px;" type="button"><i class="fa fa-plus" ></i> TAMBAH</button>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for=""><span style="color:red;">*</span> Jika skor KSPR tidak di isi maka otomatis skor KSPR : 2</label>
                                    <button onclick="cekSkorKSPR()" class="btn btn-info col-md-12" type="button"><i class="fa fa-eye" ></i> CEK TOTAL KESELURUHAN SKOR KSPR</button>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="">&nbsp;</label>
                                    <button class="btn btn-danger col-md-12" type="button" onclick="hapusAllKSPR(<?php echo $list_pasien->pendaftaran_id; ?>)"><i class="fa fa-refresh" ></i> HAPUS SEMUA</button>
                                </div>
                <?php echo form_close();?>
                                <div class="form-group col-md-12">
                                    <table id="table_skor_kspr" class="table table-striped dataTable" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama KSPR</th>
                                                <th>Skor KSPR</th>
                                                <th>Jumlah</th>
                                                <th>Total</th>
                                                <th>Batal/Hapus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="6">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama KSPR</th>
                                                <th>Skor KSPR</th>
                                                <th>Jumlah</th>
                                                <th>Total</th>
                                                <th>Batal/Hapus</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
<!-- ================================= form kspr baru ==========================================  -->


                <?php echo form_open('#',array('id' => 'fmCreatePemeriksaanKebidanan'))?>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                            <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                            <input type="hidden" id="dokter_id_bidan" name="dokter_id_bidan" value="<?php echo $list_pasien->id_M_DOKTER; ?>">
                            <!-- skor kspr -->
                            <input type="hidden" id="jum_skor" name="jum_skor" value="2">
                            <!-- skor kspr -->

                            <div class="form-group col-md-12">
                            <!-- table resiko baru -->
                                <div class="form-group col-md-6">
                                    <table id="table_faktor_resiko" class="table table-striped dataTable" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Faktor Resiko Yang Ditemukan Oleh Sistem</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="1">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <th>Faktor Resiko</th>
                                            </tr>
                                        </tfoot> -->
                                    </table>
                                </div>
                                <div class="form-group col-md-6">
                                <!-- end table -->
                                    <!-- <label for="">Keterangan (Faktor Resiko Yang Ditemukan)<span style="color: red;">*</span></label> -->
                                    <label for="">Keterangan (Faktor Resiko Yang Ditemukan) oleh manual<span style="color: red;"></span></label>
                                    <textarea class="form-control" rows="" cols="" name="resiko_keterangan" id="resiko_keterangan" placeholder="Keterangan"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <!-- <label for="">Vaksinasi TT Non Hamil<span style="color: red;"> *</span></label> -->
                                    <label for="">Vaksinasi TT Non Hamil<span style="color: red;"></span></label>
                                    <select name="vaksinasi_nonhamil" id="vaksinasi_nonhamil" class="form-control">
                                            <option value="" disabled selected>Pilih Vaksinasi TT Non Hamil</option>
                                            <option value="tidak">TIDAK</option>
                                            <option value="ya">YA</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <!-- <label for="">Vaksinasi TT Hamil<span style="color: red;"> *</span></label> -->
                                    <label for="">Vaksinasi TT Hamil<span style="color: red;"></span></label>
                                    <select name="vaksinasi_hamil" id="vaksinasi_hamil" class="form-control">
                                        <option value="" disabled selected>Pilih Vaksinasi TT Hamil</option>
                                        <option value="tidak">TIDAK</option>
                                        <option value="ya">YA</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <h3 style="font-weight:900;">STATUS KEHAMILAN</h3>
                                <hr style="margin-top:-9px;margin-bottom:10px;">
                                <div class="form-group col-md-2">
                                    <!-- <label for="">Gravida<span style="color: red;"> *</span></label> -->
                                    <label for="">Gravida<span style="color: red;"></span></label>
                                    <input type="number" class="form-control" name="anc_g" id="anc_g" value="" placeholder="00">
                                </div>
                                <div class="form-group col-md-2">
                                    <!-- <label for="">Partus<span style="color: red;"> *</span></label> -->
                                    <label for="">Partus<span style="color: red;"></span></label>
                                    <input type="number" class="form-control" name="anc_pa" id="anc_pa" value="" placeholder="00">
                                </div>
                                <div class="form-group col-md-2">
                                    <!-- <label for="">Prematur<span style="color: red;"> *</span></label> -->
                                    <label for="">Prematur<span style="color: red;"></span></label>
                                    <input type="number" class="form-control" name="anc_p" id="anc_p" value="" placeholder="00">
                                </div>
                                <div class="form-group col-md-2">
                                    <!-- <label for="">imatur<span style="color: red;"> *</span></label> -->
                                    <label for="">imatur<span style="color: red;"></span></label>
                                    <input type="number" class="form-control" name="anc_i" id="anc_i" value="" placeholder="00">
                                </div>
                                <div class="form-group col-md-2">
                                    <!-- <label for="">abortus<span style="color: red;"> *</span></label> -->
                                    <label for="">abortus<span style="color: red;"></span></label>
                                    <input type="number" class="form-control" name="anc_a" id="anc_a" value="" placeholder="00">
                                </div>
                                <div class="form-group col-md-2">
                                    <!-- <label for="">hidup<span style="color: red;"> *</span></label> -->
                                    <label for="">hidup</label>
                                    <input type="number" class="form-control" name="anc_h" id="anc_h" value="" placeholder="00">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">HPHT<span style="color: red;"> *</span></label>
                                    <input type="text" class="form-control mydatepicker" onchange="changeHPHT()" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy" name="hpht" id="hpht">
                                    <br><input type="checkbox" id="hasil_usg" name="hasil_usg" onchange="changeHasilUSG()"><label for="hasil_usg">Ceklis bila ada hasil USG</label>
                                </div>
                                <div class="form-group col-md-4" style="display:block;" id="hpl_otomatis">
                                    <label for="">HPL<span style="color: red;"> *</span></label>
                                    <input readonly  type="text" class="form-control" id="hpl">
                                    <input readonly type="hidden" class="form-control" id="hpl_1" name="hpl_1">
                                </div>
                                <div class="form-group col-md-4" style="display:none;" id="hpl_manual">
                                    <label for="">HPL<span style="color: red;"> *</span></label>
                                    <input type="text" class="form-control mydatepicker" name="hpl_2" id="hpl_2"  value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Usia Kehamilan (Minggu)<span style="color: red;"> *</span></label>
                                    <input type="number" max="3" class="form-control" name="usia_kehamilan" id="usia_kehamilan" value="" placeholder="Minggu" readonly>
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="button" style="margin-top:25px;" class="btn btn-success" id="savePemeriksaanKebidanan" onclick="tampilFormBidan();jum_kspr()"><i class="fa fa-floppy-o"></i> SIMPAN</button>
                                </div>
                            </div>
                                    <br>
            <?php echo form_close();?>

                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="result" style="display:none;">
                            <div class="col-md-12">
                                <!-- <button type="button" class="btn btn-info" onclick="exportToExcelBidan()" title="klik untuk print data"><i class="fa fa-print"></i> Print</button> -->
                                <button type="button" class="btn btn-danger pull-right" onclick="hapusKebidanan(<?= $list_pasien->pemeriksaan_obgyn_id; ?>)" title="klik untuk reset data"><i class="fa fa-undo"></i> Reset Data</button>
                            </div>
                            <div class="col-md-6">
                                <h3 style="font-weight:900;">KSPR</h3>
                                <hr style="margin-top:-9px;margin-bottom:10px;">
                                <!-- parameter if show result bidan -->
                                <input type="hidden" value="<?= $list_pasien->usia_kehamilan; ?>" id="skor_cek">
                                <table>
                                    <tr>
                                        <td id="judul">Skor KSPR</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->skor_kspr; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">Vaksinasi TT Non Hamil</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->vaksinasi_nonhamil; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">Vaksianasi TT Hamil</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->vaksinasi_hamil; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul" valign="top">Keterangan Faktor Resiko</td>
                                        <td id="separator" valign="top">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->faktor_resiko_ket; ?></td>
                                    </tr>
                                </table>
                                <!-- <table>
                                    <h3 style="font-weight:900;">KB</h3>
                                    <hr style="margin-top:-9px;margin-bottom:10px;">
                                    <tr>
                                        <td id="judul">Prenatal</td>
                                        <td id="separator">:</td>
                                        <td id="isian">00</td>
                                    </tr>
                                    <tr>
                                        <td id="judul">Pasca Persalinan</td>
                                        <td id="separator">:</td>
                                        <td id="isian">00</td>
                                    </tr>
                                    <tr>
                                        <td id="judul">Pasca Keguguran</td>
                                        <td id="separator">:</td>
                                        <td id="isian">00</td>
                                    </tr>
                                </table> -->
                                <br>
                                <table id="table_kb_show" class="table table-striped dataTable" cellspacing="0">
                                    <h3 style="font-weight:900;margin-top: 6px;">KB</h3>
                                    <hr style="margin-top:-9px;margin-bottom:10px;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl.Periksa</th>
                                            <th>Type KB</th>
                                            <th>Tindakan</th>
                                            <!-- <th>Keterangan</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6">No Data to Display</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl.Periksa</th>
                                            <th>Type KB</th>
                                            <th>Tindakan</th>
                                            <!-- <th>Keterangan</th> -->
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="col-md-6">
                                    <h3 style="font-weight:900;">STATUS KEHAMILAN</h3>
                                    <hr style="margin-top:-9px;margin-bottom:10px;">
                                <table>
                                    <tr>
                                        <td id="judul">Gravida</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->G; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">Partus</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->Pa; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">Prematur</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->P; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">imartus</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->i; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">abortus</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->a; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">hidup</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->h; ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">HPHT</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= date('d F Y', strtotime($list_pasien->HPHT)); ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">HPL</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= date('d F Y', strtotime($list_pasien->HPL)); ?></td>
                                    </tr>
                                    <tr>
                                        <td id="judul">Usia Kehamilan</td>
                                        <td id="separator">:&nbsp;</td>
                                        <td id="isian"> <?= $list_pasien->usia_kehamilan; ?></td>
                                    </tr>
                                </table>
                                 <table id="table_pnc_show" class="table table-striped dataTable" cellspacing="0">
                                     <h3 style="font-weight:900;">PNC</h3>
                                    <hr style="margin-top:-9px;margin-bottom:10px;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl.Periksa</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6">No Data to Display</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Tgl.Periksa</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>


                <div role="tabpanel" class="tab-pane" id="tabs-imun">
                    <div class="col-md-12">
                        <h3 style="font-weight:900;">IMUN</h3>
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_vaksinasi" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message_vaksinasi"></p>
                        </div>
                    </div>
                        <hr style="margin-top:-9px;margin-bottom:10px;">
                    <?php echo form_open('#',array('id' => 'fmCreateImun'))?>
                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                        <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">
                        <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                        <div class="form-group col-md-6">
                                <label>IMUN</label>
                                <select name="imun" id="imun" class="form-control select2" >
                                    <option value="" selected>Pilih Imun</option>
                                    <optgroup label="Pilih Imun">
                                    <?php
                                        $list_imun = $this->Pasien_rj_model->get_imun();
                                        foreach ($list_imun as $list) {
                                            echo "<option value='".$list->imunisasi_id."'>".$list->jenis." - ".$list->merk." - ". $list->produsen."</option>";
                                        }
                                    ?>
                                    </optgroup>
                                </select>
                        </div>
                            <div class="form-group col-md-2">
                            <button type="button" class="btn btn-success" style="margin-top:25px;" id="saveImun"><i class="fa fa-plus"></i> TAMBAH</button>
                        </div>
                    <?php echo form_close();?>

                    </div>
                    <div class="col-md-12">
                            <table id="table_imun" class="table table-striped dataTable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tgl.Periksa</th>
                                    <th>Imun</th>
                                    <th>Batal/Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6">No Data to Display</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Tgl.Periksa</th>
                                    <th>Imun</th>
                                    <th>Batal/Hapus</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="tabs-tindakan">

                    <table id="table_tindakan_pasienrj" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <?php
                                    if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                                    else{
                                ?>
                                <th>Batal/Hapus</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <?php
                                    if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                                    else{
                                ?>
                                <th>Batal/Hapus</th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateTindakanPasien'))?>
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_tindakan" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message_tindakan"></p>
                        </div>
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="id_pasien" id="id_pasien" value="<?php echo $list_pasien->pasien_id; ?>">
                    <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">
                    <input type="hidden" id="dokter_id_tindakan" name="dokter_id_tindakan" value="<?php echo $list_pasien->id_M_DOKTER; ?>">
                    <input type="hidden" id="hotel_id" name="hotel_id" value="<?php echo $reservasi->hotel_id; ?>">
                    <input type="hidden" id="shift" name="shift" value="<?php echo $reservasi->pemanggilan; ?>">

                    <?php
                        if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                        else{ ?>
                        <div class="row">
                            <div class="form-group col-md-5">
                                <label class="control-label">Input Tindakan<span style="color: red;"> *</span></label>
                                <select required id="tindakan" name="tindakan" class="form-control select2"  >
                                    <option value=""  selected>Pilih Tindakan</option>
                                </select>
                                <input type="hidden" name="harga_cyto" id="harga_cyto" readonly>
                                <input type="hidden" name="harga_tindakan" id="harga_tindakan" readonly>
                            </div>
                            <div class="form-group col-md-5">
                                <label class="control-label">Jumlah Tindakan<span style="color: red;"> *</span></label>
                                <input id="jml_tindakan" name="jml_tindakan" class="form-control" min="1" value="" type="number" onkeyup="hitungHarga()" min="1" placeholder="Jumlah Tindakan" required>
                                <input type="hidden" id="harga_tindakan_satuan" name="harga_tindakan_satuan" value="0" readonly>
                                <input type="hidden" id="subtotal" name="subtotal" value="0" readonly>
                                <input type="hidden" id="totalharga" name="totalharga" value="0" readonly>
                                <input type="hidden" id="is_cyto" name="is_cyto" value="0" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <button type="button" class="btn btn-success" style="width: 100%; margin-top: 26px;" id="saveTindakan"><i class="fa fa-plus"></i> TAMBAH</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-5">
                                <label class="control-label">Fee Dokter<span style="color: red;"> *</span></label>
                                <input id="fee_dokter" name="fee_dokter" class="form-control" type="number" placeholder="Fee Dokter" readonly required>
                            </div>
                        </div>
<!--                        <div class="row">-->
<!--                            <div class="alert alert-warning col-md-12" id="warning_perawat">-->
<!--                                <div>-->
<!--                                    <p id="card_message_perawat">Semua perawat harus diisi!</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="control-label">Input Perawat 1<span style="color: red;"> *</span></label>
                                <select required id="perawat1" name="perawat1" class="form-control select2" onchange="pilihPerawat1(this);">
                                    <option value=""  selected>Pilih Perawat</option>
                                    <?php
                                        $list = $this->Models->all('masterdata_perawat');
                                        foreach ($list as $key => $value) {
                                            if ($value['id'] == $perawat->dokter_id_1) {
                                                echo "<option value='".$value['id']."' selected>".$value['nama']."</option>";
                                            } else {
                                                echo "<option value='".$value['id']."'>".$value['nama']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                                <input type="hidden" id="c_perawat1" value=""/>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="control-label">Input Perawat 2<span style="color: red;"> *</span></label>
                                <select id="perawat2" name="perawat2" class="form-control select2" onchange="pilihPerawat2(this);">
                                    <option value=""  selected>Pilih Perawat</option>
                                    <?php
                                        $list = $this->Models->all('masterdata_perawat');
                                        foreach ($list as $key => $value) {
                                            if ($value['id'] == $perawat->dokter_id_2) {
                                                echo "<option value='".$value['id']."' selected>".$value['nama']."</option>";
                                            } else {
                                                echo "<option value='".$value['id']."'>".$value['nama']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                                <input type="hidden" id="c_perawat2" value=""/>
                            </div>
                            <div class="form-group col-md-3">
                                <label class="control-label">Input Perawat 3<span style="color: red;"> *</span></label>
                                <select id="perawat3" name="perawat3" class="form-control select2" onchange="pilihPerawat3(this);">
                                    <option value=""  selected>Pilih Perawat</option>
                                    <?php
                                        $list = $this->Models->all('masterdata_perawat');
                                        foreach ($list as $key => $value) {
                                            if ($value['id'] == $perawat->dokter_id_3) {
                                                echo "<option value='".$value['id']."' selected>".$value['nama']."</option>";
                                            } else {
                                                echo "<option value='".$value['id']."'>".$value['nama']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                                <input type="hidden" id="c_perawat3" value=""/>
                            </div>
                            <div class="form-group col-md-3">
                                <button type="button" class="btn btn-success" style="width: 100%; margin-top: 26px;" id="savePerawat"><i class="fa fa-plus"></i> TAMBAH</button>
                            </div>
                        </div>
                    <?php } ?>

                    <?php echo form_close();?>
                    <div class="clearfix"></div>
                </div>


                <div role="tabpanel" class="tab-pane fade" id="tabs-laboratorium">

                    <table id="table_laboratorium_pasienrj" class="table table-striped dataTable" cellspacing="0">
                        <thead>
                            <tr>
                            <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <?php
                                    if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                                    else{
                                ?>
                                <th>Batal/Hapus</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5">No data to display</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tgl.Tindakan</th>
                                <th>Nama Tindakan</th>
                                <th>Jumlah Tindakan</th>
                                <?php
                                    if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                                    else{
                                ?>
                                <th>Batal/Hapus</th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table><br>

                    <?php echo form_open('#',array('id' => 'fmCreateLabPasien'))?>
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_laboratorium" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div>
                            <p id="card_message_laboratorium"></p>
                        </div>
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deltind" name="<?php echo $this->security->get_csrf_token_name()?>_deltind" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="id_pasien" id="id_pasien" value="<?php echo $list_pasien->pasien_id; ?>">
                    <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>">
                    <input type="hidden" id="dokter_id_tindakan" name="dokter_id_tindakan" value="<?php echo $list_pasien->id_M_DOKTER; ?>">

                    <?php
                        if($list_pasien->status_periksa == 'SUDAH BAYAR') { }
                        else{ ?>
                        <div class="form-group col-md-5">
                            <label class="control-label">Input Tindakan<span style="color: red;"> *</span></label>
                            <select required id="laboratorium" name="laboratorium" class="form-control select2" onchange="getTarifTindakanLab()" >
                                <option value="" selected>Pilih Tindakan</option>
                            </select>
                            <input type="hidden" name="harga_cyto_lab" id="harga_cyto_lab" readonly>
                            <input type="hidden" name="harga_tindakan_lab" id="harga_tindakan_lab" readonly>
                        </div>
                        <div class="form-group col-md-5">
                            <label class="control-label">Jumlah Tindakan<span style="color: red;"> *</span></label>
                             <input id="jml_tindakan_lab" name="jml_tindakan_lab" class="form-control" min="1" value="" type="number" onkeyup="hitungHarga()" min="1" placeholder="Jumlah Tindakan" required>
                            <input type="hidden" id="harga_tindakan_satuan_lab" name="harga_tindakan_satuan_lab" value="0" readonly>
                            <input type="hidden" id="subtotal_lab" name="subtotal_lab" value="0" readonly>
                            <input type="hidden" id="totalharga_lab" name="totalharga_lab" value="0" readonly>
                            <input type="hidden" id="is_cyto_lab" name="is_cyto_lab" value="0" readonly>

                        </div>
                        <div class="form-group col-md-2">
                            <button type="button" class="btn btn-success" style="width: 100%; margin-top: 26px;" id="saveTindakanLab"><i class="fa fa-plus"></i> TAMBAH</button>
                        </div>
                    <?php } ?>

                    <?php echo form_close();?>
                    <div class="clearfix"></div>
                </div>

                <!-- TAB MEDICAL INFORMATION -->
                <div role="tabpanel" class="tab-pane fade" id="tabs-medical-information">
                    <?php echo form_open('#',array('id' => 'fmCreateMedicalInformation'))?>
                    <h3 class="mt10" style="margin-bottom: 40px">Medical Information</h3>  
                    <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id; ?>">          
                    <div class="row" style="margin-top: -29px">
                        <div class="form-group col-sm-6" >
                            <label for="present_complain" class="control-label">Present Complain <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" name="present_complain" id="present_complain" placeholder="I have a headache" value="<?= $medical_information->present_complain; ?>">
                        </div>    
                    </div>            
                    <div class="row">
                        <?php
                            date_default_timezone_set('Asia/Jakarta');
                            setlocale(LC_ALL, 'IND');
                            setlocale(LC_ALL, 'id_ID');
                            
                            $date_first_complain = ($medical_information->date_first_complain == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($medical_information->date_first_complain));
                            $date_first_consultation = ($medical_information->date_first_consultation == "0000-00-00") ? "" : strftime('%d %B %Y ', strtotime($medical_information->date_first_consultation));
                        ?>
                        <div class="form-group col-sm-6">
                    
                            <label class="control-label">Date First Complain</label>
                                <div class="input-group">
                                    <input name="date_first_complain" id="date_first_complain" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" value="<?= $date_first_complain == "01 January 1970 " ? date('d F Y') : $date_first_complain ?>"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="control-label">Date First Consultation</label>
                                <div class="input-group">
                                    <input name="date_first_consultation" id="date_first_consultation" type="text" class="form-control datepicker-id" placeholder="mm/dd/yyyy" value="<?= $date_first_consultation == "01 January 1970 " ? date('d F Y') : $date_first_consultation ?>"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6 " >
                            <label for="allergy_history" class="control-label">Allergy History</label>
                            <input type="text" class="form-control" name="allergy_history" id="allergy_history" placeholder="Yes, For...." value="<?= $medical_information->allergy_history ?>">
                        </div>
                        <div class="form-group col-sm-6 " >
                            <label for="pregnant" class="control-label">Pregnant</label>
                            <input type="text" class="form-control" name="pregnant" id="pregnant" placeholder="Month " value="<?= $medical_information->pregnant ?>">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="weight" class="control-label">Weight <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" name="weight" id="weight" placeholder="Kg.."  value="<?= $medical_information->weight ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="textarea" class="control-label">Current Medication</label>
                            <textarea id="current_medication" name="current_medication"  class="form-control" ><?= $medical_information->current_medication ?></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="textarea" class="control-label">Do you have any past medical history</label>
                            <textarea id="medical_history" name="medical_history"  class="form-control"><?= $medical_information->medical_history ?></textarea>
                            <div class="help-block with-errors"></div>

                        </div>
                        <div class="form-group col-sm-12">
                            <label for="textarea" class="control-label">Please note all important medical event</label>
                            <textarea id="medical_event" name="medical_event" class="form-control"><?= $medical_information->medical_event ?></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="button" class="btn btn-success" style="width: 100%; margin-top: 26px;" id="saveMedicalInformation"><i class="fa fa-envelope"></i> Simpan</button>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>

                <!-- TAB AMBULANCE ASSISTANCE -->
                <div role="tabpanel" class="tab-pane fade" id="tabs-ambulance-assistance">
                    <?php echo form_open('#',array('id' => 'fmCreateAmbulanceAssistance'))?>
                    <input type="hidden" name="reservasi_id" id="reservasi_id" value="<?php echo $reservasi->reservasi_id; ?>">

                    <?php
                        $cek_malam = "PAGI";
                        $driver_id = 0;
                        if (!empty($reservasi)) {
                            $driver_id = $reservasi->driver_id_2;
                            $cek_malam = $reservasi->pemanggilan;
                        }
                    ?>

                    <h3 class="mt10" style="margin-bottom: 40px">Ambulance Assistance</h3>
                    <div class="row" style="margin-top: -29px">
                        <div class="form-group col-sm-6">
                            <label for="inputPassword" class="control-label">Driver<span style="color: red;">*</span></label>
                            <select class="form-control" id="driver_2" name="driver_2">
                                <option value="0" selected>PILIH</option>
                                <?php
                                    $list = $this->Models->where('masterdata_perawat', array('driver' => 1));
                                    foreach ($list as $key => $value) {
                                        if ($value['id'] == $driver_id) {
                                            echo "<option value='".$value['id']."' selected>".$value['nama']."</option>";
                                        } else {
                                            echo "<option value='".$value['id']."'>".$value['nama']."</option>";
                                        }
                                    }
                                    ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="inputPassword" class="control-label">Pemanggilan <span style="color: red;">*</span></label>
                            <input type="text" class="form-control" value="<?php echo $cek_malam; ?>" name="cek_malam" readonly>
                            <select style="display: none;" class="form-control" id="cek_malam" name="cek_malam" disabled>
                                <option value="PAGI" <?= $cek_malam == "PAGI" ? "selected":"" ?>>PAGI (22.00 - 08:00)</option>
                                <option value="SIANG" <?= $cek_malam == "MALAM" ? "selected":"" ?>>SIANG (08.00 - 22:00)</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="button" class="btn btn-success" style="width: 100%; margin-top: 26px;" id="saveAmbulanceAssistance"><i class="fa fa-envelope"></i> Simpan</button>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>

            </div>
        </div>
    </div>
</div>





<div id="modal_reseptur" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-lg">
    <div class="modal-content" style="overflow: auto;height: 365px;width:687px;margin-left:-97px">
        <div class="modal-header" style="background: #fafafa">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel"><b>List Obat</b></h2> </div>
        <div class="modal-body" style="background: #fafafa">
               <table id="table_list_obat" class="table table-striped dataTable table-responsive" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama Obat</th>
                            <th>Satuan</th>
                            <th>Jenis Obat</th>
                            <th>Harga Jual</th>
                            <th>Jumlah Stok</th>
                            <th>Pilih</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td colspan="6">No Data to Display</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Nama Obat</th>
                            <th>Satuan</th>
                            <th>Jenis Obat</th>
                            <th>Harga Jual</th>
                            <th>Jumlah Stok</th>
                            <th>Pilih</th>
                        </tr>
                    </tfoot>
                </table>
        </div>

    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<div id="modal_diagnosa" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-lg">
    <div class="modal-content" >
        <div class="modal-header" style="background: #fafafa">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h2 class="modal-title" id="myLargeModalLabel"><b>List Diagnosa</b></h2> </div>
        <div class="modal-body table-responsive" style="background: #fafafa">
               <table id="table_list_diagnosa" class="table table-striped dataTable " cellspacing="0">
                    <thead>
                        <tr>
                            <th>Kode Diagnosa Perawat</th>
                            <th>Nama Diagnosa Perawat</th>
                            <th>Kode ICD 10</th>
                            <th>Nama ICD 10</th>
                            <th>Pilih</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td colspan="6">No Data to Display</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Kode Diagnosa Perawat</th>
                            <th>Nama Diagnosa Perawat</th>
                            <th>Kode ICD 10</th>
                            <th>Nama ICD 10</th>
                            <th>Pilih</th>
                        </tr>
                    </tfoot>
                </table>
        </div>

    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatjalan/pasien_rj/periksa_pasienrj1.js"></script>

</body>

</html>
