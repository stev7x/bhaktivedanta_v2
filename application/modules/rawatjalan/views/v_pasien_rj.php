<?php $this->load->view('header');?>

<?php $this->load->view('sidebar');?>

      <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-7 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> Pasien Rawat Jalan</h4> </div>
                    <div class="col-lg-5 col-sm-8 col-md-8 col-xs-12 pull-right">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Rawat Jalan</a></li>
                            <li class="active">Pasien Rawat Jalan</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert" >&times;</button>
                    <div >
                        <p id="card_message"></p>
                    </div>
                </div>

                <!--row -->
                <div class="row">
                    <div class="col-sm-12">
                    <div class="panel panel-info1">
                            <div class="panel-heading"> Data Pasien Rawat Jalan
                            </div>
                            <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_delda" name="<?php echo $this->security->get_csrf_token_name()?>_delda" value="<?php echo $this->security->get_csrf_hash()?>" />
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                        <label for="inputName1" class="control-label"></label>
                                            <dl class="text-right">
                                                <dt><h4 style="color: gray"><b>Filter Data</b></h4></dt>
                                                <dd>Ketik / pilih untuk mencari atau filter data <br>&nbsp;<br>&nbsp;<br>&nbsp;</dd>
                                            </dl>
                                        </div>
                                        <div class="col-md-10 col-sm-10">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group col-md-4 col-sm-12">
                                                        <label for="inputName1" class="control-label"><b>Poli :</b></label>
                                                        <b>
                                                        <select name="poliruangan" id="poliruangan" class="form-control select" style="margin-bottom: 7px" onchange="getDokter()">
                                                            <option value="" disabled selected>Pilih Berdasarkan</option>
                                                            <?php
                                                                $list_poli = $this->Pasien_rj_model->get_poli();
                                                                foreach ($list_poli as $list) {
                                                                    echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                                                }
                                                            ?>
                                                        </select></b>

                                                    </div>
                                                    <div class="form-group col-md-4 col-sm-12">
                                                        <label for="inputName1" class="control-label"><b>Dokter :</b></label>
                                                        <b>
                                                        <select name="dokter_poli" id="dokter_poli" class="form-control select" style="margin-bottom: 7px">
                                                            <option value="" >Pilih Berdasarkan</option>
                                                        </select></b>

                                                    </div>
                                                    <div class="form-group col-md-4 col-sm-12">
                                                        <label for="inputName1" class="control-label"><b>Pemanggilan :</b></label>
                                                        <b>
                                                        <select name="urutan" id="urutan" class="form-control select" style="margin-bottom: 7px">
                                                            <option value="" selected disabled>Pilih Berdasarkan</option>
                                                            <option value="no_pendaftaran" >Reservasi</option>
                                                            <option value="tgl_pendaftaran" >Datang</option>
                                                        </select></b>

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <label>Tanggal Kunjungan,Dari</label>
                                                        <div class="input-group">
                                                            <input onkeydown="return false" name="tgl_awal" id="tgl_awal" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <label>Sampai</label>
                                                        <div class="input-group">
                                                            <input onkeydown="return false" name="tgl_akhir" id="tgl_akhir" type="text" class="form-control mydatepicker" value="<?php echo date('d F Y'); ?>" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>&nbsp;</label><br>
                                                        <button type="button" class="btn btn-success col-md-12" onclick="cariPasien()">Cari</button>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3" style="margin-top: 7px">
                                                    <label for="inputName1" class="control-label"><b>Cari berdasarkan :</b></label>
                                                    <b>
                                                    <select name="#" class="form-control select" style="margin-bottom: 7px" id="change_option" >
                                                        <option value="" disabled selected>Pilih Berdasarkan</option>
                                                        <option value="no_pendaftaran">No. Pendaftaran</option>
                                                        <option value="no_rekam_medis">No. Rekam Medis</option>
                                                        <option value="pasien_nama">Nama Pasien</option>
                                                        <option value="pasien_alamat">Alamat Pasien</option>
                                                        <option value="NAME_DOKTER">Dokter PJ</option>
                                                    </select></b>

                                                </div>
                                                <div class="form-group col-md-9" style="margin-top: 7px">
                                                    <label for="inputName1" class="control-label"><b>&nbsp;</b></label>
                                                    <input type="Text" class="form-control pilih" placeholder="Cari informasi berdasarkan yang anda pilih ....." style="margin-bottom: 7px" onkeyup="cariPasien()" >
                                                </div>
                                            </div>
                                        </div>
                                    </div><br><hr style="margin-top: -27px">
                                   <table id="table_list_pasienrj" class="table table-striped table-responsive" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th style="width: 100px !important">No. Pendaftaran</th>
                                                <th width="150px">No. RM / Nama Pasien</th>
<!--                                                <th>No. BPJS</th>-->
                                                <th>Tanggal Pendaftaran</th>
                                                <th>Poliklinik</th>
                                                <th>Kelas Pelayanan</th>
                                                <th>Dokter Penanggungjawab</th>
                                                <th>Nama Hotel</th>
                                                <th>Kamar Hotel</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Umur Pasien</th>
                                                <th>Alamat Pasien</th>
                                                <th>Pembayaran</th>
                                                <th>Status Periksa</th>
                                                <th>Reschedule</th>
                                                <th>Print</th>
                                                <th>Periksa Pasien</th>
                                                <!-- <th>Reseptur</th>
                                                <th>Kirim ke Penunjang</th>
                                                <th>Kirim ke IGD</th>
                                                <th>Batal Periksa</th> -->
                                                <th>Lainnya</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="15">No Data to Display</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!--/row -->

            </div>
            <!-- /.container-fluid -->


<!-- modal print -->
<div id="modal_print" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-large" style="max-width: 80% !important;">
        <div class="modal-content" style="margin-top: 250px;height: 750px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Print Pendaftaran Rawat Jalan</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa;">
                <div class="col-sm-12" >
                    <div class="form-group col-md-4">
                        <!-- <label>Print Surat Ket.Lahir</label>  -->
                        <button type="button" class="btn btn-primary col-md-12 " id="printSurat"  ><i class="fa fa-print p-r-10"></i>PRINT SURAT KET.LAHIR</button>
                    </div>
                    <div class="form-group col-md-4">
                        <!-- <label>Print Kartu Berobat</label>  -->
                        <button type="button" class="btn btn-info col-md-12 " id="printKartu"  ><i class="fa fa-print p-r-10"></i>PRINT KARTU BEROBAT</button>
                    </div>
                    <div class="form-group col-md-4">
                        <!-- <label>Print Detail Pasien</label>  -->
                        <button type="button" class="btn btn-success col-md-12 " id="printDetail" ><i class="fa fa-print p-r-10"></i>PRINT DETAIL PASIEN</button>
                    </div>
                    <div class="form-group col-md-4">
                        <!-- <label>Print Detail Pasien</label>  -->
                        <button type="button" class="btn btn-danger col-md-12 " id="printKartuHilang" ><i class="fa fa-print p-r-10"></i>PRINT HILANG KARTU</button>
                    </div>
                    <div class="form-group col-md-4">
                        <!-- <label>Print Detail Pasien</label>  -->
                        <button type="button" class="btn btn-warning col-md-12 " id="printSticker" ><i class="fa fa-print p-r-10"></i>PRINT STICKER </button>
                    </div>
                    <div class="form-group col-md-4">
                        <!-- <label>Print Detail Pasien</label>  -->
                        <button type="button" class="btn btn-warning col-md-12 " style="background: #ea5f0d !important; border-color: #ea5f0d !important;" id="printGelang" ><i class="fa fa-print p-r-10"></i>PRINT GELANG </button>
                    </div>
<!--                    <div class="form-group col-md-4">-->
                        <!-- <label>Print Detail Pasien</label>  -->
<!--                        <button type="button" class="btn btn-warning col-md-12 " style="background: #ea5f0d !important; border-color: #ea5f0d !important;" id="printCasemix" ><i class="fa fa-print p-r-10"></i>PRINT CASEMIX </button>-->
<!--                    </div>-->
                        <!-- <select class="form-control" name="Kategori_print" id="Kategori_print">
                            <option disabled selected> Pilih Kategori </option>
                            <option value="ket.lahir">Print Surat Ket.Lahir</option>
                            <option value="kartu_berobat">Kartu Berobat</option>
                            <option value="detail_pasien">Detail Pasien</option>
                        </select>  -->

                </div>
              <!--   <div class="col-sm-12">
                    <button type="button" class="btn btn-success col-md-4 pull-right" onclick="do_print()" ><i class="fa fa-print"></i>Print</button>
                </div> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal  print -->


<!-- modal print -->
<div id="modal_schedule" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-large" style="max-width: 80% !important">
        <div class="modal-content" style="margin-top: 250px;height: 750px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Form Reschedule</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
             <?php echo form_open('#',array('id' => 'fmCreateReschedule', 'data-toggle' => 'validator'))?>
                <div class="row" style="padding-bottom: 16px">

                    <div class="panel panel-info1 col-sm-12" >
                        <div class="panel-heading" align="center"> Data Pasien </div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body" style="border:1px solid #f5f5f5">
                                <div class="row ">
                                    <div class="col-md-6 col-sm-6">
                                        <!-- <div class="col-sm-6"> -->
                                            <table  width="100%" style="font-size:16px;">
                                                <tr>
                                                    <td width="45%">Nama Pasien</td>
                                                    <td width="5%">:</td>
                                                    <td width="50%"><b id="nama_pasien"></b></td>
                                                </tr>
                                                <tr>
                                                    <td>Tempat, Tanggl lahir</td>
                                                    <td>:</td>
                                                    <td><b id="ttl"></b></td>
                                                </tr>
                                                <tr>
                                                    <td>Dokter Penanggungjawab</td>
                                                    <td>:</td>
                                                    <td><b id="dokter_pj"></b></td>
                                                </tr>
                                            </table>
                                        <!-- </div> -->
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <!-- <div class="col-sm-6"> -->
                                            <table  width="100%" style="font-size:16px;">
                                                <tr>
                                                    <td width="45%">No Rekam Medis</td>
                                                    <td width="5%">:</td>
                                                    <td width="50%"><b id="no_rm"></b></td>
                                                </tr>
                                                <tr>
                                                    <td>Poliklinik</td>
                                                    <td>:</td>
                                                    <td><b id="poli"></b></td>
                                                </tr>
                                                <tr>
                                                    <td>Jam Periksa</td>
                                                    <td>:</td>
                                                    <td><b id="jam_per"></b></td>
                                                </tr>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-bottom: 16px; ">
                    <div class="form-group col-md-3">
                        <input type="hidden" name="id_dokter" id="id_dokter">
                        <input type="hidden" name="reservasi_id" id="reservasi_id">
                        <input type="hidden" name="res_pendaftaran_id" id="res_pendaftaran_id">
                        <label for="tgl_reservasi" class="control-label">Tanggal Reschedule<span style="color: red;">*</span></label>
                        <div class="input-group" onchange="getJamDokter()">
                            <input name="tgl_reservasi" id="tgl_reservasi" type="text" class="form-control datepick" placeholder="mm/dd/yyyy" value=""> <span class="input-group-addon"><i class="icon-calender"></i></span>
                        </div>
                    </div>
<!--                    <div class="form-group col-md-3">-->
<!--                        <label for="jam_periksa">Jam Periksa<span style="color: red;">*</span></label>-->
<!--                        <select class="form-control" id="jam" name="jam" disabled>-->
<!--                            <option value="" disabled selected>PILIH</option>-->
<!--                        </select>-->
<!--                    </div>-->
                    <div class="form-group col-md-1">
                        <label for="save">&nbsp;</label>
                        <button type="button" id="save" class="btn btn-info" onclick="saveReschedule()">RESCHEDULE</button>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal  print -->


<div id="modal_periksa_pasienrj" onclick="reloadTablePasien()" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large" style="max-width: 80% !important;">
        <div class="modal-content">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="modalPeriksa">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Pasien</b></h2>
             </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9" style="height:750px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal_reseptur" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large" style="max-width: 80% !important">
        <div class="modal-content" style="margin-top:65px;height: 750px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Detail Pasien</b></h2> </div>
            <div class="modal-body" style="background: #fafafa">
                   <div class="embed-responsive embed-responsive-16by9" style="height:710px !important;padding: 0px !important">
                        <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>
                    </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal_kirim_penunjang" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="max-width: 80% !important">
        <div class="modal-content" style="overflow: auto;margin-top:65px;height: 750px;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Kirim Ke Penunjang</b></h2> </div>
                <div class="modal-body" style="background: #fafafa">
                    <?php echo form_open('#',array('id' => 'fmKirimKePenunjang'))?>
                    <div class="alert alert-success alert-dismissable col-md-12" id="modal_notif3" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button>
                        <div >
                            <p id="card_message3"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Pilih Penunjang<span style="color: red">*</span></label>
                        <select id="penunjang_id" name="penunjang_id" class="form-control">
                            <option value="" disabled selected>Pilih Penunjang</option>
                            <?php
                            $list_penunjang = $this->Pasien_rj_model->get_list_penunjang();
                                foreach($list_penunjang as $list){
                                    echo "<option value='".$list->poliruangan_id."'>".$list->nama_poliruangan."</option>";
                                }
                            ?>
                        </select>
                        <input type="hidden" id="pendaftaran_id" name="pendaftaran_id">
                    </div>
                    <div class="form-group">
                        <button id="saveToPenunjang" type="button" class="btn btn-success pull-right"><i class="fa fa-floppy-o m-r-10" ></i>Kirim</button>
                    </div>
                    <?php echo form_close(); ?>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal_riwayat_penyakit_pasien" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">     
        <div class="modal-content"> 
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Riwayat Penyakit Pasien</b></h2>
             </div>  
            <div class="modal-body" style="background: #fafafa">        
                <div class="embed-responsive embed-responsive-16by9" style="height:450px !important;padding: 0px !important">
                    <iframe id="iframe_modal" class="embed-responsive-item" frameborder="0"></iframe>    
                </div> 
            </div>
        </div>        <!-- /.modal-content -->
    </div>    <!-- /.modal-dialog -->
</div>

<div id="modal_pulangkan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-large">
        <div class="modal-content" style="overflow: auto;height: auto;">
            <div class="modal-header" style="background: #fafafa">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel"><b>Pulangkan Pasien</b></h2>
            </div>
            <div class="modal-body" style="background: #fafafa">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>
