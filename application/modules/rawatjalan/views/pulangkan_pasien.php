<?php $this->load->view('header_iframe');?>
<body id="body">

<div class="white-box" style="min-height:440px; overflow:auto; margin-top: -6px;border:1px solid #f5f5f5;padding:15px !important">    

    <div class="row"> 
        <div class="col-md-12" style="padding-bottom:15px">  
            <div class="panel panel-info1">
                <div class="panel-heading" align="center"> Data Pasien 
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body" style="border:1px solid #f5f5f5">
                        <div class="row">
                            <div class="col-md-6">
                                <table  width="400px" align="right" style="font-size:16px;text-align: left;">       
                                    <tr>   
                                        <td><b>Tgl.Pendaftaran</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?php echo date('d M Y H:i:s', strtotime($list_pasien->tgl_pendaftaran)); ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Pendaftaran</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_pendaftaran; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>No.Rekam Medis</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->no_rekam_medis; ?></td>
                                    </tr>     
                                    <tr>
                                        <td><b>Ruangan</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->nama_poliruangan; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Dokter Pemeriksa</b></td>
                                        <td>:</td>       
                                        <td style="padding-left: 15px"><?= $list_pasien->NAME_DOKTER; ?></td>
                                    </tr>  
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table  width="400px" style="font-size:16px;text-align: left;">       
                                    <tr>
                                        <td><b>Nama Pasien</b></td> 
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->pasien_nama; ?></td>
                                    </tr>    
                                    <tr>
                                        <td><b>Umur</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px">
                                            <?php 
                                                $tgl_lahir = $list_pasien->tanggal_lahir;
                                                $umur  = new Datetime($tgl_lahir);
                                                $today = new Datetime();
                                                $diff  = $today->diff($umur);
                                                echo $diff->y." Tahun ".$diff->m." Bulan ".$diff->d." Hari"; 
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Jenis Kelamin</b></td>
                                        <td>:</td>
                                        <td style="padding-left: 15px"><?= $list_pasien->jenis_kelamin; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Kelas Pelayanan</b></td>
                                        <td>:</td> 
                                        <td style="padding-left: 15px"><?= $list_pasien->kelaspelayanan_nama; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type Pembayaran</b></td>
                                        <td>:</td>  
                                        <td style="padding-left: 15px"><?= $list_pasien->type_pembayaran; ?></td>
                                    </tr> 
                                </table>
                                <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                                <input type="hidden" id="dokter_periksa" name="dokter_periksa" value="<?php echo $list_pasien->NAME_DOKTER; ?>">  
                                <input id="no_pendaftaran" type="hidden" name="no_pendaftaran" class="validate" value="<?php echo $list_pasien->no_pendaftaran; ?>" readonly>
                                <input id="no_rm" type="hidden" name="no_rm" class="validate" value="<?php echo $list_pasien->no_rekam_medis; ?>" readonly>
                                <input id="poliruangan" type="hidden" name="poliruangan" class="validate" value="<?php echo $list_pasien->nama_poliruangan; ?>" readonly>
                                <input id="nama_pasien" type="hidden" name="nama_pasien" class="validate" value="<?php echo $list_pasien->pasien_nama; ?>" readonly>
                                <input id="umur" type="hidden" name="umur" value="<?php echo $list_pasien->umur; ?>" readonly>
                                <input id="jenis_kelamin" type="hidden" name="jenis_kelamin" class="validate" value="<?php echo $list_pasien->jenis_kelamin; ?>" readonly>
                                <input type="hidden" id="kelaspelayanan_id" name="kelaspelayanan_id" value="<?php echo $list_pasien->kelaspelayanan_id; ?>"> 
                                <input type="hidden" id="dokter_id" name="dokter_id" value="<?php echo $list_pasien->id_M_DOKTER;?>">
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
        </div>  

        <div class="col-md-12" style="margin-top: -10px">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active nav-item"><a href="#informasi" class="nav-link" aria-controls="informasi" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Informasi</span></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content"> 
                <div role="tabpanel" class="tab-pane active" id="informasi">
                    <?php echo form_open('#',array('id' => 'fmInformasiPasien'))?>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>_deldiag" name="<?php echo $this->security->get_csrf_token_name()?>_deldiag" value="<?php echo $this->security->get_csrf_hash()?>" />
                    <input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="<?php echo $list_pasien->pendaftaran_id; ?>">
                    <input type="hidden" name="pasien_id" id="pasien_id" value="<?php echo $list_pasien->pasien_id;?>">

                    <div class="alert alert-success alert-dismissable col-md-12 " id="modal_notif1" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
                        <div>
                            <p id="card_message1" class=""></p>
                        </div>
                    </div>

                    <div class="col-md-6">   

                        <div class="form-group">   
                            <label class="control-label">Kondisi Saat Keluar<span style="color: red;"> *</span></label>
                            <select id="kondisi_pasien" name="kondisi_pasien" class="form-control" >
                                <option value="" disabled selected>Pilih Kondisi Pasien</option>
                                <?php
                                    foreach($list_kondisi as $list){
                                        echo "<option value='".$list->name."'>".$list->name."</option>";
                                    }
                                ?>
                            </select> 
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group col-md-12">   
                            <label class="control-label">Catatan<span style="color: red;"> *</span></label>
                            <textarea name="catatan" id="catatan" class="form-control"></textarea>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">  

                        <div class="form-group">   
                            <input type="checkbox" name="rujuk" id="rujuk">
                            <label class="control-label">Rujuk</label>
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Alasan Rujuk</label>
                            <select id="alasan_rujuk" name="alasan_rujuk" class="form-control" >
                                <option value="" disabled selected>Pilih Alasan Rujuk</option>
                                <?php
                                    foreach($list_alasan_rujuk as $list){
                                        echo "<option value='".$list->alasan_rujuk_id."'>".$list->nama."</option>";
                                    }
                                ?>
                            </select> 
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Instansi Tujuan</label>
                            <select id="instansi" name="instansi" class="form-control" >
                                <option value="" disabled selected>Pilih Instansi</option>
                                <?php
                                    foreach($list_instansi_tujuan as $list){
                                        echo "<option value='".$list->rs_rujukan_id."'>".$list->nama_rs."</option>";
                                    }
                                ?>
                            </select> 
                        </div>
                        <div class="form-group">   
                            <label class="control-label">Catatan Rujukan<span style="color: red;"> *</span></label>
                            <textarea name="catatan_rujukan" id="catatan_rujukan" class="form-control"></textarea>
                        </div>
                        <div class="form-group">   
                            <input type="checkbox" name="jaminan" id="jaminan">
                            <label class="control-label">Dengan Jaminan</label>
                        </div>
                    </div> -->

                    <?php echo form_close();?>  
                    <div class="clearfix"></div>
                </div>
            </div>  
        </div>
    </div>

</div>

<div class="col-md-12">
    <button class="btn btn-info" style="float: right;margin-right: 4px;margin-bottom: 16px;" onclick="doPulangkanPasien()">Pulangkan</button>
    <div class="clearfix"></div>
</div>

<!-- /.modal-dialog -->
</div>

<?php $this->load->view('footer_iframe');?>
<script src="<?php echo base_url()?>assets/dist/js/pages/rawatjalan/pasien_rj/pulangkan_pasien.js"></script>

</body>

</html>
