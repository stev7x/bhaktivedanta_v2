<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        table {

        }
        th {
            text-align: left;
            width: 175px;
        }

        .rows{
            width: 250px;
        }

        .center{
            text-align: center;
            font-weight: bold;
        }
    </style>
</head>

<body onload="window.print()">

<?php for($i = 1;$i<=10;$i++){?>

    <?php
        $tanggal = new DateTime($pasien['tanggal_lahir']);
        $sekarang = new DateTime();
        $perbedaan = $tanggal->diff($sekarang);
    ?>

    <table>
        <tr>
            <td>
                <table style="border-collapse:collapse;border:1px solid black;">
                    <tr>
                        <td><b><?= $pasien['no_rekam_medis'] ?></b></td>
                    </tr>
                    <tr>
                        <td><?= $pasien['pasien_nama'] . " " . $perbedaan->y . "th " . $obat['nama'] ?></td>
                    </tr>
                    <tr>
                        <td><?= $dosis['konten'] . " " . $persediaan['persediaan_3'] . " " . $aturan_penggunaan['konten'] ?></td>
                    </tr>
                    <tr>
                        <td><?= $resep['dokter_nama'] ?></td>
                    </tr>
                </table>
            </td>
            <td>
                <table style="border-collapse:collapse;border:1px solid black;">
                    <tr>
                        <td><b><?= $pasien['no_rekam_medis'] ?></b></td>
                    </tr>
                    <tr>
                        <td><?= $pasien['pasien_nama'] . " " . $perbedaan->y . "th " . $obat['nama'] ?></td>
                    </tr>
                    <tr>
                        <td><?= $dosis['konten'] . " " . $persediaan['persediaan_3'] . " " . $aturan_penggunaan['konten'] ?></td>
                    </tr>
                    <tr>
                        <td><?= $resep['dokter_nama'] ?></td>
                    </tr>
                </table>
            </td>
            <td>
                <table style="border-collapse:collapse;border:1px solid black;">
                    <tr>
                        <td><b><?= $pasien['no_rekam_medis'] ?></b></td>
                    </tr>
                    <tr>
                        <td><?= $pasien['pasien_nama'] . " " . $perbedaan->y . "th " . $obat['nama'] ?></td>
                    </tr>
                    <tr>
                        <td><?= $dosis['konten'] . " " . $persediaan['persediaan_3'] . " " . $aturan_penggunaan['konten'] ?></td>
                    </tr>
                    <tr>
                        <td><?= $resep['dokter_nama'] ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php } ?>
</body>
</html>