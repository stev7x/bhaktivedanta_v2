<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model  {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->model('Models');
    }    

    function get_list_menu($group_id) {
        $user_id = $this->session->userdata()['id'];
        $user_access_permission = json_decode($this->Models->show("aauth_users", $user_id)['user_access_permission']);
    	$res = $this->db->query("select z.* from (select * from aauth_perms where name like '%_view') x, aauth_perm_to_group y, aauth_role z   
			where x.id = y.perm_id  
			and x.role_id = z.role_id        
             and y.group_id = ".$group_id." order by z.order_layout asc");  
            // and y.group_id = ".$group_id." order by  z.role_name  asc");

//        select z.* from (select * from aauth_perms where name like '%_view') x, aauth_perm_to_group y, aauth_role z where x.id = y.perm_id and x.role_id = z.role_id and y.group_id = 1 order by z.order_layout asc;

    	if($res->num_rows() > 0) {
//    		$list = $res->result_array();
            $list = array();
            $index = 0;
    		foreach ($res->result_array() as $key => $value) {
                if (in_array($value['role_id'], $user_access_permission)) {
                    $list[$index]['role_id'] = $value['role_id'];
                    $list[$index]['role_name'] = $value['role_name'];
                    $list[$index]['url'] = $value['url'];
                    $list[$index]['parent'] = $value['parent'];
                    $list[$index]['order_layout'] = $value['order_layout'];
                    $list[$index]['icon'] = $value['icon'];

                    $index++;
                }
            }

//             echo "<pre>";
//             print_r($list);die();
//             echo "</pre>";
    		
            return $list;
    	}
    	return FALSE; 
    }
}