<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ketersediaan_kamar_model extends CI_Model  {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    function get_ketersediaan() {
    	$res = $this->db->query("SELECT * FROM v_count_ketersediaan_kamar");
            // and y.group_id = ".$group_id." order by  z.role_name  asc");

    	if($res->num_rows()>0) {
    		$list = $res->result_array();

            // echo "<pre>";
            // print_r($list);die();
            // echo "</pre>";

            return $list;
    	}
    	return FALSE;
    }
}
