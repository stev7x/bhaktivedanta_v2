var table_laboratorium;
$(document).ready(function(){
    // $('#modal_periksa_laboratorium').on('hidden.bs.modal', function(){
    //     $(this).find('iframe').html("");
    //     $(this).find('iframe').attr("src", "");
    // });

    // $('#modal_periksa_laboratorium').modal('show'); 

    table_laboratorium = $('#table_list_laboratorium').DataTable({
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "penunjang/laboratorium/ajax_list_laboratorium",
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.no_masukpenunjang = $("#no_masukpenunjang").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.no_bpjs = $("#no_bpjs").val();
            }
        }
    });
    
    $('button#searchPasien').click(function(){
        table_laboratorium.columns.adjust().draw();
    });
    
    // var year = new Date().getFullYear();
    // $('.datepicker').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
});

function reloadTablePasien(){
    table_laboratorium.columns.adjust().draw();
}

function periksalaboratorium(pasienmasukpenunjang_id){
    $('#modal_periksa_laboratorium').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    var src = ci_baseurl + "penunjang/laboratorium/periksa_laboratorium/"+pasienmasukpenunjang_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_periksa_laboratorium iframe").attr({ 
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function batalPeriksa(pasienmasukpenunjang_id, pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pasienmasukpenunjang_id"] = pasienmasukpenunjang_id;
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
    if(pasienmasukpenunjang_id != ''){
      swal({
          text: "Apakah anda yakin ingin membatalkan periksa ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){  
             $.ajax({
                    url: ci_baseurl + "penunjang/laboratorium/do_delete_laboratorium",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                      success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.notif').removeClass('red').addClass('green');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                                table_laboratorium.columns.adjust().draw();
                                swal( 
                                    'Berhasil!',
                                    'Berhasil Dibatalkan.', 
                                    'success',
                                  ) 
                            } else {
                                $('.notif').removeClass('green').addClass('red');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                                swal( 
                                    'Gagal!',
                                    'Gagal Dibatalkan.', 
                                    'error',
                                  )
                            }
                        }
                  });
          
        });
    }

}

function cariPasien(){
    table_laboratorium = $('#table_list_laboratorium').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "penunjang/laboratorium/ajax_list_laboratorium",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.no_masukpenunjang = $("#no_masukpenunjang").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.pasien_alamat = $("#pasien_alamat").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_laboratorium.columns.adjust().draw();
}

$(document).ready(function(){
    $('#change_option').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});