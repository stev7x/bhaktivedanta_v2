var table_tindakan_radiologi;
var table_hasil_tindakan_radiologi;
var table_list_tindakan;
var table_list_user;

$(document).ready(function(){
    getTindakan();
  

    table_hasil_tindakan_radiologi = $('#table_hasil_tindakan_radiologi').DataTable({
    "data" : console.log("masuk sini"),
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "penunjang/radiologi/ajax_list_hasil_tindakan_pasien",
          "type": "GET",
          "data" : console.log("lebih bagus masuk sini"),
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_hasil_tindakan_radiologi.columns.adjust().draw();

    table_tindakan_radiologi = $('#table_tindakan_radiologi_pasien').DataTable({
    "data" : console.log("masuk sini"),
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "penunjang/radiologi/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : console.log("lebih bagus masuk sini"),
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_radiologi.columns.adjust().draw();

    $('button#saveTindakan').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasienmasukpenunjang_id = $("#pasienmasukpenunjang_id").val();
        pasien_id = $("#pasien_id").val();
        dokter_pengirim = $("#dokter_pengirim").val();
        dokter_rad = $("#dokter_rad").val();
        dokter_anastesi = $("#dokter_anastesi").val();
        perawat = $("#perawat").val();
        

        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "penunjang/radiologi/do_create_tindakan_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateTindakanPasien').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                       
                        if(ret === true) {
                            // $('.modal_notif').removeClass('alert-danger').addClass('alert-success');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();
                            // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            //     $(".modal_notif").hide();
                            // });
                            $('.select2').val("").trigger("change");
                            // $('#fmCreateTindakanPasien').select2("val", "");
                            $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                            $('#fmCreateTindakanPasien').find("input[type=number]").val("");
                            $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',0);
                            $("#tindakan").select2("val", "");                            

                            // $("#table_hasil_tindakan_radiologi").ajax.reload();
                            table_tindakan_radiologi.columns.adjust().draw();
                            table_hasil_tindakan_radiologi.columns.adjust().draw();
                            // $('#scroll').animate({ scrollTop: 200 }, "fast");
                            swal( 
                                'Berhasil!',
                                'Data Berhasil Disimpan.', 
                                'success',
                            );

                            
                        } else {
                            $('.modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(550, function(){
                                $(".modal_notif").hide();
                            });
                            $('#scroll').animate({ scrollTop: 500 }, "fast");

                          console.log("masuk else");
                        }
                    }
                });
          
        })
    
    });

    function reloadTablePasien(){
        table_tindakan_radiologi.columns.adjust().draw();
        table_hasil_tindakan_radiologi.columns.adjust().draw();
    }
});

function tes(){
 if(table_list_tindakan){
        table_list_tindakan.destroy();
    }
    table_list_tindakan = $('#table_list_tindakan').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "penunjang/Radiologi/ajax_list_tindakan",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_tindakan.columns.adjust().draw();
}


function cariuser(){
 if(table_list_user){
        table_list_user.destroy();
    }
    table_list_user = $('#table_list_user').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "penunjang/radiologi/ajax_list_user",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_user.columns.adjust().draw();
}



function pilihListTindakan(daftartindakan_id,id){
  console.log('jalan')
    if (daftartindakan_id != ""){
    $.ajax({
      url: ci_baseurl + "penunjang/Radiologi/ajax_get_tindakan_by_id",
      type: 'get',
      dataType: 'json',
            data: { daftartindakan_id: daftartindakan_id },
      success: function(data) {
        var ret = data.success;
        if (ret == true){
          console.log('kesini')
          $('#kode_tindakan'+id).val(data.data['daftartindakan_id']);
          $('#tindakan_id'+id).val(data.data['daftartindakan_id']);
          $('#nama_tindakan2').val(data.data['daftartindakan_nama']);

          // console.log(  $('#kode_tindakan'+id).val(data.data['daftartindakan_id']));
          // console.log( $('#tindakan_id'+id).val(data.data['daftartindakan_id']));
          // $('#nama_obat').val(data.data['nama_barang']);
          // $('#satuan_obat').val(data.data['id_sediaan']);
          // $('#harga_jual').val(data.data['harga_satuan']);
          $('#modal_tindakan').modal('hide');
        } else {
          // $("#harga_netto").val('');
          // $("#current_stok").val('');
          console.log('ajax failed : '+daftartindakan_id+' kolom ke : '+id);
          $('#kode_tindakan'+id).val('');
          $('#tindakan_id'+id).val('');
        }
      }
    });
  } else {
    // $('#modal_list_obat').closeModal('toggle');
    $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
  }
}



function pilihListUser(id,iduser){
  console.log('jalan')
    if (id != ""){
    $.ajax({
      url: ci_baseurl + "penunjang/Radiologi/ajax_get_user_by_id",
      type: 'get',
      dataType: 'json',
            data: { id: id },
      success: function(data) {
        var ret = data.success;
        if (ret == true){
          console.log('kesini')
          $('#username'+iduser).val(data.data['username']);
          $('#user'+iduser).val(data.data['id']);

          console.log($('#username'+iduser).val(data.data['username']));
          console.log($('#user'+iduser).val(data.data['id']));
          // console.log(  $('#kode_tindakan'+id).val(data.data['daftartindakan_id']));
          // console.log( $('#tindakan_id'+id).val(data.data['daftartindakan_id']));
          // $('#nama_obat').val(data.data['nama_barang']);
          // $('#satuan_obat').val(data.data['id_sediaan']);
          // $('#harga_jual').val(data.data['harga_satuan']);
          $('#modal_user').modal('hide');
        } else {
          // $("#harga_netto").val('');
          // $("#current_stok").val('');
          console.log('ajax failed : '+id+' kolom ke : '+iduser);
          $('#username'+iduser).val('');
          $('#user'+iduser).val('');
        }
      }
    });
  } else {
    // $('#modal_list_obat').closeModal('toggle');
    $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
  }
}



    function reloadTablePasien(){
        table_tindakan_radiologi.columns.adjust().draw();
        table_hasil_tindakan_radiologi.columns.adjust().draw();
    }

function getTindakan(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    var type_pembayaran = $('#type_pembayaran').val();
    $.ajax({ 
        url: ci_baseurl + "penunjang/radiologi/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id,type_pembayaran:type_pembayaran},
        success: function(data) { 
            $("#tindakan").html(data.list); 
        }
    });
}   

function getTarifTindakan(){

    var daftartindakan_id = $("#tindakan").val();
    $.ajax({
        url: ci_baseurl + "penunjang/radiologi/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {daftartindakan_id:daftartindakan_id},
        success: function(data) {
            $("#harga_tindakan").val(data.list.harga_tindakan);
            $("#harga_cyto").val(data.list.cyto_tindakan);
            $("#jml_tindakan").removeAttr('readonly');
        }
    });
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();

    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $("#subtotal").val(subtotal);
    $("#totalharga").val(total);
}



function printTindakan(tindakanradiologi_id){
  var jsonVariable = {};
    jsonVariable["tindakanradiologi_id"] = tindakanradiologi_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    console.log("masuk fungsi");
    swal({
            text: "Fungsi Print Belum Diaktifkan !!!!",
            type: 'error',
            showCancelButton: false,
            cancelButtonColor: '#3085d6',
            cancelButtonText: ''
          })
}

function hapusTindakan(tindakanradiologi_id){
    var jsonVariable = {};
    jsonVariable["tindakanradiologi_id"] = tindakanradiologi_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "penunjang/radiologi/do_delete_tindakan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_tindakan_radiologi.columns.adjust().draw();
                            table_hasil_tindakan_radiologi.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success'
            )
          });
        }

        
function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();

    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $("#subtotal").val(subtotal);
    $("#totalharga").val(total);
}



function printHasil(pendaftaran_id){ 
console.log('masukk')
     console.log("masuk print : "+ci_baseurl+"penunjang/Radiologi/print_resep/"+pendaftaran_id); 
     console.log(pendaftaran_id = $("#pendaftaran_id").val());
     console.log('tess')
 
    window.open(ci_baseurl+'penunjang/Radiologi/print_resep/'+pendaftaran_id,'printHasil','top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
} 
  
    function addobat(){
       tbody = '<tr><td><input class="tbody-input-left" type="text" value=""></td><td><input class="tbody-input" type="number" value=""></td><td><input class="tbody-input" type="text" value=""></td><td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus obat" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td></tr>';
        $('#table_tindakan_obat tbody').append(tbody);
    }

    function addbhp(){
       tbody = '<tr><td><input class="tbody-input-left" type="text" value=""></td><td><input class="tbody-input" type="number" value=""></td><td><input class="tbody-input"type="text" value=""></td><td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus BHP" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td></tr>';
        $('#table_tindakan_bhp tbody').append(tbody);
    }

    // $('button#addTindakanObat').click(function(){
    //     tbody = '<tr><td><input class="tbody-input-left" type="text" value=""></td><td><input class="tbody-input" type="number" value=""></td><td><input class="tbody-input" type="text" value=""></td><td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus obat" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td></tr>';
    //     $('#table_tindakan_obat tbody').append(tbody);
    // });

    // $('button#addTindakanBHP').click(function(){
    //     tbody = '<tr><td><input class="tbody-input-left" type="text" value=""></td><td><input class="tbody-input" type="number" value=""></td><td><input class="tbody-input"type="text" value=""></td><td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus BHP" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td></tr>';
    //     $('#table_tindakan_bhp tbody').append(tbody);
    // });
