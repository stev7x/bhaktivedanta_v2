var table_tindakan_lab;
var table_hasil_tindakan_lab;
var table_hasil_tindakan_lab_non_bpjs;
var table_hasil_tindakan_lab_all;

$(document).ready(function(){
    getTindakan();
    table_tindakan_lab = $('#table_tindakan_laboratorium').DataTable({
        "data" : console.log("masuk sini"),
        "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "penunjang/laboratorium/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : console.log("lebih bagus masuk sini"),
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_lab.columns.adjust().draw();

    
    table_hasil_tindakan_lab = $('#table_hasil_tindakan_lab').DataTable({
        "data" : console.log("masuk sini"),
        "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": ci_baseurl + "penunjang/laboratorium/ajax_list_hasil_pasien_lab",
          "type": "GET",
          "data" : console.log("lebih bagus masuk sini"),
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
              d.type_pembayaran = 1;
              console.log(d.pendaftaran_id);
              console.log(d.type_pembayaran);
          }
      }
    });
    table_hasil_tindakan_lab.columns.adjust().draw();
    
    table_hasil_tindakan_lab_all = $('#table_hasil_tindakan_lab_all').DataTable({
        "data" : console.log("masuk sini"),
        "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": ci_baseurl + "penunjang/laboratorium/ajax_list_hasil_pasien_lab_all",
          "type": "GET",
          "data" : console.log("lebih bagus masuk sini"),
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
              d.type_pembayaran = 1;
              console.log(d.pendaftaran_id);
              console.log(d.type_pembayaran);
          }
      }
    });
    table_hasil_tindakan_lab_all.columns.adjust().draw();

  table_hasil_tindakan_lab_non_bpjs = $('#table_hasil_tindakan_lab_non_bpjs').DataTable({
        "data" : console.log("masuk sini"),
        "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": ci_baseurl + "penunjang/laboratorium/ajax_list_hasil_pasien_lab",
          "type": "GET",
          "data" : console.log("lebih bagus masuk sini"),
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
              d.type_pembayaran = 0;
              console.log(d.pendaftaran_id);
              console.log(d.type_pembayaran);
          }
      }
    });
  table_hasil_tindakan_lab_non_bpjs.columns.adjust().draw();


    $('button#saveTindakan').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasienmasukpenunjang_id = $("#pasienmasukpenunjang_id").val();
        pasien_id = $("#pasien_id").val();
        dokter_pengirim = $("#dokter_pengirim").val();
        dokter_lab = $("#dokter_lab").val();
        dokter_anastesi = $("#dokter_anastesi").val();
        perawat = $("#perawat").val();
        
        swal({
          text: 'Apakah data yang anda masukan sudah benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "penunjang/laboratorium/do_create_tindakan_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateTindakanPasien').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                       
                        if(ret === true) {
                            // $('.modal_notif').removeClass('red').addClass('green');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();
                            // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            //     $(".modal_notif").hide();
                            // });
                            $('.select2').val("").trigger("change");
                            $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                            $('#fmCreateTindakanPasien').find("input[type=number]").val("");
                            $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',0);
                            table_hasil_tindakan_lab.columns.adjust().draw();
                            table_hasil_tindakan_lab_non_bpjs.columns.adjust().draw();
                            swal( 
                                'Berhasil!',
                                'Data Berhasil Disimpan.', 
                                'success',
                            );
                        } else {
                            $('.modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(550, function(){
                                $(".modal_notif").hide();
                            });
                            $('#scroll').animate({ scrollTop: 500 }, "fast");
                          console.log("masuk else");
                        }
                    }
                });
        })
    
    });




});

function hapusDong(tindakan_lab_id, lab_id) {
  var jsonVariable = {};
  jsonVariable["tindakan_lab_id"] = tindakan_lab_id;
  jsonVariable["lab_id"] = lab_id;
  alert("helo i'm here"+jsonVariable);
}
function hapusHasilTindakan(tindakan_lab_id, lab_id) {
  var jsonVariable = {};
  jsonVariable["tindakan_lab_id"] = tindakan_lab_id;
  jsonVariable["lab_id"] = lab_id;
  jsonVariable[csrf_name] = $('#' + csrf_name + '_deltind').val();
  
  
  console.log("masuk fungsi");
  swal({
    text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',
    confirmButtonText: 'Ya'
  }).then(function () {
    $.ajax({
      url: ci_baseurl + "penunjang/laboratorium/do_delete_hasil_tindakan",
      type: 'post',
      dataType: 'json',
      data: jsonVariable,
      success: function (data) {
        var ret = data.success;
        $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
        $('input[name=' + data.csrfTokenName + '_deltind]').val(data.csrfHash);
        $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);


        if (ret === true) {
          table_hasil_tindakan_lab.columns.adjust().draw();
          table_hasil_tindakan_lab_all.columns.adjust().draw();
          swal(
            'Berhasil!',
            'Data Berhasil Dihapus.',
            'success',
          )
        } else {
          swal(
            'Gagal!',
            'Data Gagal Dihapus.',
            'error',
          )
          console.log("gagal");
        }
      }
    });

  });
} 

function getTindakan(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    var type_pembayaran = $('#type_pembayaran').val();
    $.ajax({
        url: ci_baseurl + "penunjang/laboratorium/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id,type_pembayaran:type_pembayaran},
        success: function(data) {
            $("#tindakan_lab").html(data.list);
        }
    });
}

function getTarifTindakanLab(){
    var tariftindakan_id = $("#tindakan_lab").val();
    $.ajax({ 
        data : console.log('masuk tarif'),
        url: ci_baseurl + "penunjang/laboratorium/get_tarif_tindakan",
        type: 'GET', 
        dataType: 'JSON',
        data: {tariftindakan_id:tariftindakan_id},
        success: function(data){
            $("#jml_tindakan").removeAttr('readonly');
            $("#harga_tindakan").val(data.list.harga_tindakan);
            $("#harga_cyto").val(data.list.cyto_tindakan);
        }
    }); 
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();

    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){ 
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;   
    console.log("test : "+ subtotal.toLocaleString());
    $("#subtotal").val("Rp. "+numberWithCommas(subtotal));               
    $("#totalharga").val("Rp. "+numberWithCommas(total));               
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function hapusTindakan(tindakanlab_id){
    var jsonVariable = {};
    jsonVariable["tindakanlab_id"] = tindakanlab_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            console.log("masuk delete");
            $.ajax({
                  url: ci_baseurl + "penunjang/laboratorium/do_delete_tindakan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_tindakan_lab.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
            )
          });
        }
        
function hapusTindakan(tindakanlab_id){
    var jsonVariable = {};
    jsonVariable["tindakanlab_id"] = tindakanlab_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    // console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            // console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "penunjang/laboratorium/do_delete_tindakan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_tindakan_lab.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
            )
          });
        }


$(document).ready(function(){

  /* START HEMATOLOGI */
  $('#cek_darah_lengkap').change(function(){
    if (this.checked) {
      $('#darah_lengkap').show(499);
    }else{
      $('#darah_lengkap').hide(499);
    }
  });

  $('#cek_pendarahan').change(function(){
    if (this.checked) {
      $('#div_darah').show(499);
    }else{
      $('#div_darah').hide(499);
    }
  });

  $('#cek_pembekuan').change(function(){
    if (this.checked) {
      $('#div_beku').show(499);
    }else{
      $('#div_beku').hide(499);
    }
  });

  $('#cek_goldar').change(function(){
    if (this.checked) {
      $('#div_goldar').show(499);
    }else{
      $('#div_goldar').hide(499);
    }
  });
  /* END HEMATOLOGI */

  /* START URINE */
  $('#cek_urine_lengkap').change(function(){
    if (this.checked) {
      $('#div_urin_lengkap').show(499);
    }else{
      $('#div_urin_lengkap').hide(499);
    }
  });

  /* END URINE */

  /* START USG */
  $('#cek_usg_kebidanan').change(function(){
    if (this.checked) {
      $('#div_usg_kebidanan').show(499);
    }else{
      $('#div_usg_kebidanan').hide(499);
    }
  });

  $('#cek_tvs').change(function(){
    if (this.checked) {
      $('#div_tvs').show(499);
    }else{
      $('#div_tvs').hide(499);
    }
  });

  $('#cek_usg_4d').change(function(){
    if (this.checked) {
      $('#div_usg_4d').show(499);
    }else{
      $('#div_usg_4d').hide(499);
    }
  });
  $('#cek_ecg').change(function(){
    if (this.checked) {
      $('#div_ecg').show(499);
    }else{
      $('#div_ecg').hide(499);
    }
  });
  $('#cek_nst').change(function(){
    if (this.checked) {
      $('#div_nst').show(499);
    }else{
      $('#div_nst').hide(499);
    }
  });
  /* END USG */

  /* START LAIN LAIN */

  $('#cek_tespack').change(function(){
    if (this.checked) {
      $('#div_testpack').show(499);
    }else{
      $('#div_testpack').hide(499);
    }
  });

  $('#cek_papsmear').change(function(){
    if (this.checked) {
      $('#div_papsmear').show(499);
    }else{
      $('#div_papsmear').hide(499);
    }
  });

  $('#cek_sperma').change(function(){
    if (this.checked) {
      $('#div_sperma').show(499);
    }else{
      $('#div_sperma').hide(499);
    }
  });

  /* END LAIN LAIN */

  /* START IMUNO SEROLOGI */  
  $('#cek_widal').change(function(){
    if (this.checked) {
      $('#div_widal').show(499);
    }else{
      $('#div_widal').hide(499);
    }
  });

  $('#cek_igm').change(function(){
    if (this.checked) {
      $('#div_igm').show(499);
    }else{
      $('#div_igm').hide(499);
    }
  });

  $('#cek_igg').change(function(){
    if (this.checked) {
      $('#div_igg').show(499);
    }else{
      $('#div_igg').hide(499);
    }
  });

  /* END IMUNO SEROLOGI  */

  /* START KIMIA KLINIK */

  $('#cek_sgot').change(function(){
    if (this.checked) {
      $('#div_sgot').show(499);
    }else{
      $('#div_sgot').hide(499);
    }
  });

  $('#cek_sgpt').change(function(){
    if (this.checked) {
      $('#div_sgpt').show(499);
    }else{
      $('#div_sgpt').hide(499);
    }
  });

  $('#cek_ureum').change(function(){
    if (this.checked) {
      $('#div_ureum').show(499);
    }else{
      $('#div_ureum').hide(499);
    }
  });

  $('#cek_kreatinin').change(function(){
    if (this.checked) {
      $('#div_kreatinin').show(499);
    }else{
      $('#div_kreatinin').hide(499);
    }
  });

  $('#cek_bilirubin_total').change(function(){
    if (this.checked) {
      $('#div_bilirubin_total').show(499);
    }else{
      $('#div_bilirubin_total').hide(499);
    }
  });

  $('#cek_glukosa_puasa').change(function(){
    if (this.checked) {
      $('#div_glukosa_puasa').show(499);
    }else{
      $('#div_glukosa_puasa').hide(499);
    }
  });

  $('#cek_glukosa_2jam_pp').change(function(){
    if (this.checked) {
      $('#div_glukosa_2jam_pp').show(499);
    }else{
      $('#div_glukosa_2jam_pp').hide(499);
    }
  });

  $('#cek_glukosa_sewaktu').change(function(){
    if (this.checked) {
      $('#div_glukosa_sewaktu').show(499);
    }else{
      $('#div_glukosa_sewaktu').hide(499);
    }
  });

  /* END KIMIA KLINIK */
});

function cekRange(lab_id,item){
  var kurang_dari = null, lebih_dari = null;
  var jk          = $("#jenis_kelamin").val();
  var tgl_lahir   = $("#tgl_lahir").val();
  var item_nama   = item;
  var item_nama_2 = item.replace(/_/g, " ");
  var item        = $("#" + item).val();

  var born        = new Date(tgl_lahir);
  var today       = new Date();
  var timeDiff    = today.getTime() - born.getTime();
  var diffDays    = Math.round(timeDiff / (1000 * 3600 * 24));
  var tahun       = Math.round(diffDays / 365);
  var sisahari    = diffDays % 365;
  var bulan       = Math.floor(sisahari / 30);
  var hari        = sisahari % 30;

 
  $.ajax({
    url: ci_baseurl + "penunjang/laboratorium/get_range_lab",
    type: 'GET',
    dataType: 'JSON',
    data: { lab_id: lab_id },
    success: function (data) {
        var tarif = parseInt(data.list.tarif); 
        var nama_pemeriksaan = data.list.nama_pemeriksaan; 
        var lab_id = data.list.lab_id; 
      if (jk == "Laki-laki") {
        kurang_dari = parseFloat(data.list.terendah_laki);
        lebih_dari  = parseFloat(data.list.tertinggi_laki);
        var range   = data.list.tampil_laki;
        console.log(range);
        if (item <= kurang_dari) {
        var
          val_message = item_nama_2.toUpperCase() + " Kurang",
            status = "abnormal";
          range_normal = "range normal " + range;
            
        } else if (item >= lebih_dari) {
          var
            val_message = item_nama_2.toUpperCase() + " Lebih",
            status = "abnormal";
            range_normal = "range normal " + range;
            
        }else if(item >= kurang_dari && item <= lebih_dari ){
          var
            val_message = item_nama_2.toUpperCase() + " Normal",
            status = "normal";
            range_normal = "range normal";
            
        }else{

        }
        console.log("tahun" + tahun);
        console.log("bulan " + bulan);
        console.log("hari " + hari);

        // console.log(lab_id);
        // console.log(nama_pemeriksaan);
        // call function notif
         notif(status, item_nama, val_message, range_normal);
        $("#notification_"+item_nama).append('<div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_' + item_nama + '" style="display: none; height: 50px;"><button type = "button" class="close" data-dismiss="alert">&times;</button><div><p id="card_message_'+item_nama+'"></p></div></div>');
        $("#tarif_" + item_nama).append('<input type="hidden" name="'+item_nama+'_tarif" id="' + item_nama + '_tarif" value="' + tarif + '">');
        $("#periksa_" + item_nama).append('<input type="hidden" name="'+item_nama+'_periksa" id="' + item_nama + '_periksa" value="' + nama_pemeriksaan + '">');
        $("#id_" + item_nama).append('<input type="hidden" name="'+item_nama+'_id" id="' + item_nama + '_id" value="' + lab_id + '">');
        
      }else if(jk == "Perempuan"){
        kurang_dari = parseFloat(data.list.terendah_per);
        lebih_dari = parseFloat(data.list.tertinggi_per);
        if (item <= kurang_dari) {
          var
            val_message = item_nama_2.toUpperCase() + " Kurang",
            status = "abnormal";
        } else if (item >= lebih_dari) {
          var
            val_message = item_nama_2.toUpperCase() + " Lebih",
            status = "abnormal";
        } else if (item >= kurang_dari && item <= lebih_dari) {
          var
            val_message = item_nama_2.toUpperCase() + " Normal",
            status = "normal";
        } else {

        }
      
        notif(status, item_nama, val_message);
        $("#notification_" + item_nama).append('<div class="alert alert-success alert-dismissable col-md-12" id="modal_notif_' + item_nama + '" style="display: none; height: 50px;"><button type = "button" class="close" data-dismiss="alert">&times;</button><div><p id="card_message_' + item_nama + '"></p></div></div>');
        $("#tarif_"+item_nama).append('<input type="hidden" id="'+item_nama+'_tarif" value="'+tarif+'">');
      }
    }
  }); 
}

function notif(status, item_nama, val_message, range_normal) {
  if(status == "normal"){
    $("#modal_notif_"+item_nama).removeClass('alert-danger').addClass('alert-success');
    $("#card_message_"+item_nama).html(range_normal);
    $("#modal_notif_"+item_nama).show();
    $("#modal_notif_"+item_nama).fadeTo(3500, 350).slideUp("fast", function () {
      $("#modal_notif_"+item_nama).hide();
    });
    $("html, body").animate({ scrollTop: 100 }, "fast");
    console.log("masuk if bawah");
  }else if(status == "abnormal"){
    $("#modal_notif_"+item_nama).removeClass('alert-success').addClass('alert-danger');
    $("#card_message_"+item_nama).html(range_normal);
    $("#modal_notif_"+item_nama).show();
    $("#modal_notif_"+item_nama).fadeTo(3500, 350).slideUp("fast", function () {
      $("#modal_notif_"+item_nama).hide();
    });
    $("html, body").animate({ scrollTop: 100 }, "fast");
    // console.log("masuk else if bawah");
    
  }
}


function pilihMenuTindakan(){
  var menu_tindakan = $("#menu_tindakan").val();

  if(menu_tindakan == 1){
    $("#pilih_paket").show();
    $("#menu_hematologi").hide();
    $("#menu_urine").hide();
    $("#menu_usg").hide();
    $("#menu_kimia_klinik").hide();
    $("#menu_dll").hide();
    $("#menu_serologi").hide();
    // console.log("masuk if");
  }else if(menu_tindakan == 2){
    $("#pilih_paket").hide();
    $("#menu_hematologi").show();
    $("#menu_urine").show();
    $("#menu_usg").show();
    $("#menu_kimia_klinik").show();
    $("#menu_dll").show();
    $("#menu_serologi").show();
  }
}

function pilihPaket() {
  var paket = $("#menu_paket").val();

  if(paket == 1){
    $("#menu_hematologi").show();
    $("#menu_urine").hide();
    $("#menu_usg").hide();
    $("#menu_kimia_klinik").hide();
    $("#menu_dll").hide();
    $("#menu_serologi").hide();
  }else if(paket == 2){
    $("#menu_urine").show();
    $("#menu_hematologi").hide();
    $("#menu_usg").hide();
    $("#menu_kimia_klinik").hide();
    $("#menu_dll").hide();
    $("#menu_serologi").hide();
  // }else if(paket == 3){
  //   $("#menu_usg").show();
  //   $("#menu_hematologi").hide();
  //   $("#menu_urine").hide();
  //   $("#menu_kimia_klinik").hide();
  //   $("#menu_dll").hide();
  //   $("#menu_serologi").hide();
  }else if(paket == 3){
    $("#menu_kimia_klinik").show();
    $("#menu_serologi").hide();
    $("#menu_hematologi").hide();
    $("#menu_urine").hide();
    $("#menu_usg").hide();
    $("#menu_dll").hide();
  }else if(paket == 4){
    $("#menu_serologi").show();
    $("#menu_urine").hide();
    $("#menu_usg").hide();
    $("#menu_dll").hide();
    $("#menu_kimia_klinik").hide();
    $("#menu_hematologi").hide();
  }else if(paket == 5){
    $("#menu_dll").show();
    $("#menu_hematologi").hide();
    $("#menu_urine").hide();
    $("#menu_usg").hide();
    $("#menu_kimia_klinik").hide();
    $("#menu_serologi").hide();
  }else{
    $("#menu_hematologi").hide();
    $("#menu_urine").hide();
    $("#menu_usg").hide();
    $("#menu_kimia_klinik").hide();
    $("#menu_dll").hide();
    $("#menu_serologi").hide();
  }
}

$(document).ready(function () {

  $('#check_all_hematologi').change(function () {
    if(this.checked){
      $('#all_darah_lengkap').prop('checked', true);
      $('#hematologi_waktupendarahan').prop('checked', true);
      $('#hematologi_goldar_abo').prop('checked', true);
      $('#hematologi_waktupembekuan').prop('checked', true);
      $('#menu_darah_lengkap').show(499);
      $('#menu_darah_lengkap').show(499);
      $('#hematologi_hb').prop('checked', true)
      $('#hematologi_hitungjenis').prop('checked', true)
      $('#hematologi_leukosit').prop('checked', true)
      $('#hematologi_eritrosit').prop('checked', true)
      $('#hematologi_led').prop('checked', true)
      $('#hematologi_mch').prop('checked', true)
      $('#hematologi_mchc').prop('checked', true)
      $('#hematologi_hematrokrit').prop('checked', true)
      $('#hematologi_mcv').prop('checked', true)
      $('#hematologi_trombosit').prop('checked', true)
    }else{
      $('#menu_darah_lengkap').hide(499);            
      $('#all_darah_lengkap').prop('checked', false);
      $('#hematologi_waktupendarahan').prop('checked', false);
      $('#hematologi_goldar_abo').prop('checked', false);
      $('#hematologi_waktupembekuan').prop('checked', false);

    }
  });

  $('#all_darah_lengkap').change(function () {
    if (this.checked) {
      $('#menu_darah_lengkap').show(499);
      $('#hematologi_hb').prop('checked', true)
      $('#hematologi_hitungjenis').prop('checked', true)
      $('#hematologi_leukosit').prop('checked', true)
      $('#hematologi_eritrosit').prop('checked', true)
      $('#hematologi_led').prop('checked', true)
      $('#hematologi_mch').prop('checked', true)
      $('#hematologi_mchc').prop('checked', true)
      $('#hematologi_hematrokrit').prop('checked', true)
      $('#hematologi_mcv').prop('checked', true)
      $('#hematologi_trombosit').prop('checked', true)
    } else{
      $('#menu_darah_lengkap').hide(499);      
      // $('#hematologi_hb').prop('checked', false);
      // $('#hematologi_hitungjenis').prop('checked', false);
      // $('#hematologi_leukosit').prop('checked', false);
      // $('#hematologi_eritrosit').prop('checked', false);
      // $('#hematologi_led').prop('checked', false);
      // $('#hematologi_mch').prop('checked', false);
      // $('#hematologi_mchc').prop('checked', false);
      // $('#hematologi_hematrokrit').prop('checked', false);
      // $('#hematologi_mcv').prop('checked', false);
      // $('#hematologi_trombosit').prop('checked', false);
    }
  });

});

function printData(pasien_id, view, id) {
  window.open(ci_baseurl + 'penunjang/laboratorium/hasil_lab/' + pasien_id + '/' + view + '/' + id, 'v_print_lab', 'top=100,left=320,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

function showCaraBayar(jenis_poli) {
  if (jenis_poli == "umum") {
    value = $('#type_pembayaran').val();
    console.log('value : ' + value);
    div       = $('.pembayaran1');
    div2      = $('.totalumum');
    nama      = $('#nama_asuransi');
    nomor     = $('#no_asuransi');
    lblNama   = $('#lblNamaPembayaran_umum');
    lblNomor  = $('#lblNomorPembayaran_umum');
    // hitungTotal();
  }else{

  } 
  id = $('#pembayaran_id').val();
  console.log('id :' + id);

  $.ajax({
    url: ci_baseurl + "penunjang/laboratorium/getCaraPembayaran/" + id,
    type: 'GET',
    dataType: 'JSON',
    success: function (data) {
      console.log('masuk success ajax');

      console.log(data);

      if (value != "PRIBADI") {
        console.log('masuk if bpjs');
        div.show(500);
        // div2.show(500);
        if (value == "ASURANSI1") {
          nama.val(data.nama_asuransi);
          nomor.val(data.no_asuransi);
          lblNama.text('Nama Asuransi ');
          lblNomor.text('Nomor Asuransi ');

        }
        if (value == "ASURANSI2") {
          nama.val(data.nama_asuransi2);
          nomor.val(data.no_asuransi2);

          lblNama.text('Nama Asuransi 2');
          lblNomor.text('Nomor Asuransi 2');
        }

      } else {
        div.hide(500);
        div2.hide(500);
      }

    }
  });
}