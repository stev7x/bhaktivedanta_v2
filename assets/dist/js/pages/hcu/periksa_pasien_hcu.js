// View Assestment
function viewAssestmentPasienHCU(pendaftaran_id) {
    var src = ci_baseurl + "hcu/view_assestment_pasienhcu/" + pendaftaran_id;
    // console.log(pasien_id);
    $("#modal_riwayat_assesment_pasienhcu iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen': ''
    });
}

// Insert Assestment
function insertAssestment() {
    let assestment = $("#fmCreateAssestment").serialize() + 
        "&program_therapy=" +  JSON.stringify(programTherapyArr) +
        "&pendaftaran_id=" + $("#pendaftaran_id").val() + 
        "&pasien_id=" + $("#pasien_id").val()

    // console.log(assestment)
    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        $.ajax({
            url: ci_baseurl + "hcu/insert_assestment",
            type: 'POST',
            dataType: 'JSON',
            data: assestment,
            success: function(response) {
                var ret = response.success;
                $('input[name='+response.csrfTokenName+']').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deldiag]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deltind]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delres]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delda]').val(response.csrfHash);
                if(ret === true) {
                    document.getElementById("fmCreateAssestment").reset()
                    clearProgramTherapy()
                    swal(
                        'Berhasil!',
                        'Data Berhasil Disimpan.',
                        'success'
                    )
                }
                console.log(response)
            }
        })
    })
}

// Program Therapy Array
var programTherapyArr = []
function displayProgramTherapy() {
    let display = document.getElementById("table_list_program_theraphy_hcu")
    display = display.getElementsByTagName("tbody")[0]
    display.innerHTML = ""

    let tr, td, button, i = 0
    programTherapyArr.forEach(element => {
        tr = document.createElement("tr")
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(++i))
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.name))

        td = document.createElement("td")
        button = document.createElement("button")
        button.setAttribute("class", "btn btn-danger")
        button.appendChild(document.createTextNode("Hapus"))
        td.appendChild(button)
        tr.appendChild(td)
        display.appendChild(tr)
    });
}

// Add Program Therapy 
function addProgramTherapy() {
    let program_therapy = $("#program_therapy")
    programTherapyArr.push({
        id: program_therapy.val(),
        name: $("#program_therapy option:selected").text()
    })
    displayProgramTherapy()
}

// Remove Program Therapy
function removeProgramTherapy(id) {
    programTherapyArr.splice((id - 1), 1)
    displayProgramTherapy()
}

// Clear Program Therapy
function clearProgramTherapy() {
    programTherapyArr = []
    displayProgramTherapy()
}

// View Alergi
table_list_alergi_hcu = $('#table_list_alergi_hcu').DataTable({
    "destroy": true,
    "ordering": false,
    "bFilter": false, 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    // "sScrollX": "100%",

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": ci_baseurl + "hcu/ajax_list_alergi_pasienhcu",
        "type": "GET",
        "data": function(d){
            d.pendaftaran_id = $("#pendaftaran_id").val()
        } 

    }, 
    //Set column definition initialisation properties.
});
table_list_alergi_hcu.columns.adjust().draw();

// Insert Alergi
function insertAlergiPasien() {
    let alergi = "alergi_id=" + $("#bg_alergi").val() +
        "&pendaftaran_id=" + $("#pendaftaran_id").val() +
        "&pasien_id=" + $("#pasien_id").val()

    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        $.ajax({
            url: ci_baseurl + "hcu/insert_alergi_pasienhcu",
            type: 'POST',
            dataType: 'JSON',
            data: alergi,
            success: function(response) {
                var ret = response.success;
                $('input[name='+response.csrfTokenName+']').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deldiag]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deltind]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delres]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delda]').val(response.csrfHash);
                if(ret === true) {
                    table_list_alergi_hcu.columns.adjust().draw();
                    swal(
                        'Berhasil!',
                        'Alergi Berhasil Disimpan.',
                        'success'
                    )
                }
                console.log(response)
            }
        })
    })
}

// Remove Alergi
function removeAlergi(alergi_pasien_id) {
    $.ajax({
        url: ci_baseurl + "hcu/remove_alergi_pasienhcu",
        type: 'POST',
        dataType: 'JSON',
        data: "alergi_pasien_id=" + alergi_pasien_id,
        success: function(response) {
            var ret = response.success;
            if(ret === true) {
                table_list_alergi_hcu.columns.adjust().draw();
                swal(
                    'Berhasil!',
                    'Data Berhasil Dihapus.',
                    'success'
                )
            }
            console.log(response)
        }
    })
}

// Update Background
function saveBackground() {
    let background = "keluhan_masuk=" + $("#keluhan").val() + 
        "&pendaftaran_id=" + $("#pendaftaran_id").val()

    $.ajax({
        url: ci_baseurl + "hcu/update_background",
        type: 'POST',
        dataType: 'JSON',
        data: background,
        success: function(response) {
            var ret = response.success;
            if(ret === true) {
                swal(
                    'Berhasil!',
                    'Data Berhasil Diperbaharui.',
                    'success'
                )
            }
            console.log(response)
        }
    })
}

// View Diagnosa
table_list_diagnosa_hcu = $('#table_list_diagnosa_hcu').DataTable({
    "destroy": true,
    "ordering": false,
    "bFilter": false, 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    // "sScrollX": "100%",

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": ci_baseurl + "hcu/ajax_list_diagnosa_pasienhcu",
        "type": "GET",
        "data": function(d){
            d.pendaftaran_id = $("#pendaftaran_id").val()
        } 

    }, 
    //Set column definition initialisation properties.
});
table_list_diagnosa_hcu.columns.adjust().draw();

// Get Data Dokter
function getDataDokter(position, targetID, targetNama) {
    $("#position_dokter").val(position)
    $("#targetID_dokter").val(targetID)
    $("#targetNama_dokter").val(targetNama)
    modal_dokter_pasienhcu = $('#modal_dokter_pasienhcu table').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",
    
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "hcu/ajax_list_dokter",
            "type": "GET",
            "data": function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val()
            } 
    
        }, 
        //Set column definition initialisation properties.
    });
    modal_dokter_pasienhcu.columns.adjust().draw();
}

// Set Data Dokter
function setDokterPasien(id_dokter, nama_dokter) {
    position = $("#position_dokter").val()
    targetID = $("#targetID_dokter").val()
    targetNama = $("#targetNama_dokter").val()

    $("#" + targetID).val(id_dokter)
    $("#" + targetNama).val(nama_dokter)

    $("#modal_dokter_pasienhcu").modal("hide");
}

// Get Data Diagnosa
function getDataDiagnosa() {
    modal_diagnosa_pasienhcu = $('#modal_diagnosa_pasienhcu table').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",
    
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "hcu/ajax_list_tindakan",
            "type": "GET",
            "data": function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val()
            } 
    
        }, 
        //Set column definition initialisation properties.
    });
    modal_diagnosa_pasienhcu.columns.adjust().draw();
}

// Set Diagnosa From Tindakan
function setDiagnosa(icd) {
    $.ajax({
        url: ci_baseurl + "hcu/ajax_get_diagnosa_icd_9",
        type: 'POST',
        dataType: 'JSON',
        data: "icd=" + icd,
        success: function(response) {
            var ret = response.success;
            if(ret === true) {
                var data = response.data;
                $("#kode_diagnosa").val(data.kode_diagnosa);
                $("#nama_diagnosa").val(data.nama_diagnosa);
                $("#map_icd_10").val(data.nama_diagnosa + "("+ data.kode_diagnosa +")");

                $("#modal_diagnosa_pasienhcu").modal("hide")
            }
            console.log(response)
        }
    })
}

// Get Data Diagnosa ICD 10
function getDataDiagnosaICD10() {
    modal_diagnosa_pasienhcu = $('#modal_diagnosa_icd10_pasienhcu table').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",
    
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "hcu/ajax_list_diagnosa_icd_10",
            "type": "GET",
            "data": function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val()
            } 
    
        }, 
        //Set column definition initialisation properties.
    });
    modal_diagnosa_pasienhcu.columns.adjust().draw();
}

// Set Data Diagnosa ICD 10
function setDiagnosaICD10(kode_diagnosa, nama_diagnosa) {
    $("#kode_diagnosa_icd_10").val(kode_diagnosa);
    $("#nama_diagnosa_icd_10").val(nama_diagnosa);

    $("#modal_diagnosa_icd10_pasienhcu").modal("hide")
}

// Save Diagnosa
function saveDiagnosa() {
    let diagnosa = $("#frmDiagnosa").serialize() + 
        "&pendaftaran_id=" + $("#pendaftaran_id").val() + 
        "&pasien_id=" + $("#pasien_id").val()

    console.log(diagnosa)
    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        $.ajax({
            url: ci_baseurl + "hcu/insert_diagnosa",
            type: 'POST',
            dataType: 'JSON',
            data: diagnosa,
            success: function(response) {
                var ret = response.success;
                $('input[name='+response.csrfTokenName+']').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deldiag]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deltind]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delres]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delda]').val(response.csrfHash);
                if(ret === true) {
                    document.getElementById("frmDiagnosa").reset()
                    table_list_diagnosa_hcu.columns.adjust().draw();
                    swal(
                        'Berhasil!',
                        'Data Berhasil Disimpan.',
                        'success'
                    )
                }
                console.log(response)
            }
        })
    })
}

// View Tindakan
table_list_tindakan_hcu = $('#table_list_tindakan_hcu').DataTable({
    "destroy": true,
    "ordering": false,
    "bFilter": false, 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    // "sScrollX": "100%",

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": ci_baseurl + "hcu/ajax_list_tindakan_pasienhcu",
        "type": "GET",
        "data": function(d){
            d.pendaftaran_id = $("#pendaftaran_id").val()
        } 

    }, 
    //Set column definition initialisation properties.
});
table_list_tindakan_hcu.columns.adjust().draw();

// Obat Tindakan Array
var obatTindakanArr = []
function displayObatTindakan() {
    let display = document.getElementById("table_list_obat_hcu")
    display = display.getElementsByTagName("tbody")[0]
    display.innerHTML = ""

    let tr, td, button
    obatTindakanArr.forEach(element => {
        tr = document.createElement("tr")
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.name))
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.jumlah))

        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.satuan)) 

        td = document.createElement("td")
        button = document.createElement("button")
        button.setAttribute("class", "btn btn-danger")
        button.appendChild(document.createTextNode("Hapus"))
        td.appendChild(button)
        tr.appendChild(td)
        display.appendChild(tr)
    });
}

// Add Obat Tindakan 
function addObatTindakan() {
    let tindakan_obat = $("#tindakan_obat")
    obatTindakanArr.push({
        id: tindakan_obat.val(),
        name: $("#tindakan_obat option:selected").text(),
        jumlah: $("#tindakan_obat_jumlah").val(),
        id_satuan: $("#tindakan_obat_satuan").val(),
        satuan: $("#tindakan_obat_satuan option:selected").text()
    })
    displayObatTindakan()
}

// Remove Obat Tindakan
function removeObatTindakan(id) {
    obatTindakanArr.splice((id - 1), 1)
    displayObatTindakan()
}

// Clear Obat Tindakan
function clearObatTindakan() {
    obatTindakanArr = []
    displayObatTindakan()
}

// BHP Tindakan Array
var bhpTindakanArr = []
function displayBHPTindakan() {
    let display = document.getElementById("table_list_alat_kesehatan_hcu")
    display = display.getElementsByTagName("tbody")[0]
    display.innerHTML = ""

    let tr, td, button
    bhpTindakanArr.forEach(element => {
        tr = document.createElement("tr")
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.name))
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.jumlah))

        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.satuan)) 

        td = document.createElement("td")
        button = document.createElement("button")
        button.setAttribute("class", "btn btn-danger")
        button.appendChild(document.createTextNode("Hapus"))
        td.appendChild(button)
        tr.appendChild(td)
        display.appendChild(tr)
    });
}

// Add BHP Tindakan 
function addBHPTindakan() {
    let tindakan_obat = $("#tindakan_bhp")
    bhpTindakanArr.push({
        id: tindakan_obat.val(),
        name: $("#tindakan_bhp option:selected").text(),
        jumlah: $("#tindakan_bhp_jumlah").val(),
        id_satuan: $("#tindakan_bhp_satuan").val(),
        satuan: $("#tindakan_bhp_satuan option:selected").text()
    })
    displayBHPTindakan()
}

// Remove BHP Tindakan
function removeBHPTindakan(id) {
    bhpTindakanArr.splice((id - 1), 1)
    displayBHPTindakan()
}

// Clear BHP Tindakan
function clearBHPTindakan() {
    bhpTindakanArr = []
    displayBHPTindakan()
}

// Get Data Tindakan
function getDataTindakan() {
    modal_tindakan_pasienhcu = $('#modal_tindakan_pasienhcu table').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",
    
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "hcu/ajax_list_tindakan2",
            "type": "GET",
            "data": function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val()
            } 
    
        }, 
        //Set column definition initialisation properties.
    });
    modal_tindakan_pasienhcu.columns.adjust().draw();
}

// Set Tindakan From Tindakan
function setTindakan(id, icd) {
    $.ajax({
        url: ci_baseurl + "hcu/ajax_get_diagnosa_icd_9",
        type: 'POST',
        dataType: 'JSON',
        data: "icd=" + icd,
        success: function(response) {
            var ret = response.success;
            if(ret === true) {
                var data = response.data;
                $("#id_tindakan").val(id)
                $("#kode_tindakan").val(data.kode_diagnosa);
                $("#nama_tindakan").val(data.nama_diagnosa);
                $("#map_icd_9_tindakan").val(data.nama_diagnosa + "("+ data.kode_diagnosa +")");

                $("#modal_tindakan_pasienhcu").modal("hide")
            }
            console.log(response)
        }
    })
}

// Get Data Tindakan ICD 10
function getDataTindakanICD9() {
    modal_tindakan_icd9_pasienhcu = $('#modal_tindakan_icd9_pasienhcu table').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",
    
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "hcu/ajax_list_diagnosa_icd_9",
            "type": "GET",
            "data": function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val()
            } 
    
        }, 
        //Set column definition initialisation properties.
    });
    modal_tindakan_icd9_pasienhcu.columns.adjust().draw();
}

// Set Data Tindakan ICD 10
function setDiagnosaICD9(kode_diagnosa, nama_diagnosa) {
    $("#kode_icd_9").val(kode_diagnosa);
    $("#nama_icd_9").val(nama_diagnosa);

    $("#modal_tindakan_icd9_pasienhcu").modal("hide")
}

// Save Tindakan Pasien
function saveTindakan() {
    let tindakan = $("#frmTindakanPasienHCU").serialize() + 
        "&pendaftaran_id=" + $("#pendaftaran_id").val() + 
        "&pasien_id=" + $("#pasien_id").val() +
        "&tindakan_obat=" + JSON.stringify(obatTindakanArr) + 
        "&tindakan_bhp=" + JSON.stringify(bhpTindakanArr)

    console.log(tindakan)
    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        $.ajax({
            url: ci_baseurl + "hcu/insert_tindakan",
            type: 'POST',
            dataType: 'JSON',
            data: tindakan,
            success: function(response) {
                var ret = response.success;
                $('input[name='+response.csrfTokenName+']').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deldiag]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deltind]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delres]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delda]').val(response.csrfHash);
                if(ret === true) {
                    document.getElementById("frmTindakanPasienHCU").reset()
                    table_list_tindakan_hcu.columns.adjust().draw();
                    clearBHPTindakan();
                    clearObatTindakan();
                    swal(
                        'Berhasil!',
                        'Data Berhasil Disimpan.',
                        'success'
                    )
                }
            }
        })
    })
}

// Remove Tindakan Pasien
function removeTindakanPasienHCU(pasien_tindakan_id) {
    $.ajax({
        url: ci_baseurl + "hcu/remove_tindakan_pasien",
        type: 'POST',
        dataType: 'JSON',
        data: "pasien_tindakan_id=" + pasien_tindakan_id,
        success: function(response) {
            var ret = response.success;
            if(ret === true) {
                table_list_tindakan_hcu.columns.adjust().draw();
                swal(
                    'Berhasil!',
                    'Data Berhasil Dihapus.',
                    'success'
                )
            }
            console.log(response)
        }
    })
}

// Get Data Obat All
function getDataObat() {
    modal_reseptur_obat_pasienhcu = $('#modal_reseptur_obat_pasienhcu table').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",
    
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "hcu/get_obat_list",
            "type": "GET",
            "data": function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val()
            } 
    
        }, 
        //Set column definition initialisation properties.
    });
    modal_reseptur_obat_pasienhcu.columns.adjust().draw();
}

// Set Data Obat
function setResepturObat(id, nama_obat) {
    $("#nama_obat").val(nama_obat)
    $("#id_jenis_barang").val(id)
    $("#modal_reseptur_obat_pasienhcu").modal("hide")
}

// Racikan Obat Array
var racikanObatArr = []
function displayRacikanObat() {
    let display = document.getElementById("panel_racikan_obat")
    display = display.getElementsByTagName("table")[0]
    display = display.getElementsByTagName("tbody")[0]
    console.log(display)
    display.innerHTML = ""

    let tr, td, button, no = 0
    racikanObatArr.forEach(element => {
        tr = document.createElement("tr")
        no++;
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(no))
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.name))
        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.jumlah))

        tr
            .appendChild(document.createElement("td"))
            .appendChild(document.createTextNode(element.satuan)) 

        td = document.createElement("td")
        button = document.createElement("button")
        button.setAttribute("class", "btn btn-danger")
        button.appendChild(document.createTextNode("Hapus"))
        td.appendChild(button)
        tr.appendChild(td)
        display.appendChild(tr)
    });
}

// Add BHP Tindakan 
function addRacikanObat() {
    let obat_racikan = $("#obat_racikan")
    racikanObatArr.push({
        id: obat_racikan.val(),
        name: $("#obat_racikan option:selected").text(),
        jumlah: $("#jumlah_racikan").val(),
        id_satuan: $("#sediaan_racikan").val(),
        satuan: $("#sediaan_racikan option:selected").text(),
        keterangan: $("#keterangan_racikan").val()
    })
    displayRacikanObat()
}

// Remove BHP Tindakan
function removeRacikanObat(id) {
    racikanObatArr.splice((id - 1), 1)
    displayRacikanObat()
}

// Clear BHP Tindakan
function clearRacikanObat() {
    racikanObatArr = []
    displayRacikanObat()
}

// View Tindakan
table_resep_pasienhcu = $('#table_resep_pasienhcu').DataTable({
    "destroy": true,
    "ordering": false,
    "bFilter": false, 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    // "sScrollX": "100%",

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": ci_baseurl + "hcu/ajax_get_reseptur_pasien",
        "type": "GET",
        "data": function(d){
            d.pendaftaran_id = $("#pendaftaran_id").val()
        } 

    }, 
    //Set column definition initialisation properties.
});
table_resep_pasienhcu.columns.adjust().draw();

// Save Reseptur Obat
function saveResepturObat() {
    let resep_obat = $("#fmCreateResepPasien").serialize() + 
        "&pendaftaran_id=" + $("#pendaftaran_id").val() + 
        "&pasien_id=" + $("#pasien_id").val() +
        "&dokter_id=" + $("#dokter_id").val() +
        "&racikan_obat=" + JSON.stringify(racikanObatArr) 

    console.log(resep_obat)
    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        $.ajax({
            url: ci_baseurl + "hcu/insert_reseptur_obat",
            type: 'POST',
            dataType: 'JSON',
            data: resep_obat,
            success: function(response) {
                var ret = response.success;
                $('input[name='+response.csrfTokenName+']').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deldiag]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_deltind]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delres]').val(response.csrfHash);
                $('input[name='+response.csrfTokenName+'_delda]').val(response.csrfHash);
                if(ret === true) {
                    document.getElementById("fmCreateResepPasien").reset()
                    table_resep_pasienhcu.columns.adjust().draw();
                    clearRacikanObat();
                    $('#panel_racikan_obat').toggle();
                    swal(
                        'Berhasil!',
                        'Data Berhasil Disimpan.',
                        'success'
                    )
                }
            }
        })
    })
}