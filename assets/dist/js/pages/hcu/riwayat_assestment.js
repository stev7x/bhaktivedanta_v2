table_pasienhcu = $('#table_list_assestment_pasienhcu').DataTable({
    "destroy": true,
    "ordering": false,
    "bFilter": false, 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    // "sScrollX": "100%",

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": ci_baseurl + "hcu/ajax_list_assestment_pasienhcu",
        "type": "GET",
        "data": function(d){
            d.pendaftaran_id = $("#pendaftaran_id").val()
        } 

    }, 
    //Set column definition initialisation properties.
});
table_pasienhcu.columns.adjust().draw();

function removeAssestment(assestment_id) {
    $.ajax({
        url: ci_baseurl + "hcu/remove_assestment",
        type: 'POST',
        dataType: 'JSON',
        data: "assestment_id=" + assestment_id,
        success: function(response) {
            var ret = response.success;
            if(ret === true) {
                table_pasienhcu.columns.adjust().draw();
                swal(
                    'Berhasil!',
                    'Data Berhasil Dihapus.',
                    'success'
                )
            }
            console.log(response)
        }
    })
}