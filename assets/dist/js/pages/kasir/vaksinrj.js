var table_kasirrj;
$(document).ready(function(){ 
   table_kasirrj = $('#table_list_kasirrj_belumbayar').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "bFilter"   : false,
      "ajax": {
          "url": ci_baseurl + "kasir/vaksinrj/ajax_list_kasirrj",
          "type": "GET",
         "data": function(d){ 
                d.tgl_awal          = $("#tgl_awal").val();   
                d.tgl_akhir         = $("#tgl_akhir").val();
                d.dokter_poli       = $("#dokter_poli").val();
                d.poliruangan       = $("#poliruangan").val();
                d.poliklinik        = $("#poliklinik").val();
                d.status            = $("#status").val();
                d.pasien_nama       = $("#pasien_nama").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.no_rekam_medis    = $("#no_rekam_medis").val();
                d.no_bpjs           = $("#no_bpjs").val();
                d.pasien_alamat     = $("#pasien_alamat").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.urutan            = $("#urutan").val(); 
              } 
      },   
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0 ], //last column  
        "orderable": false //set not orderable
      }
      ]
    });
    table_kasirrj.columns.adjust().draw();
});

function cariPasien(){   
    table_kasirrj = $('#table_list_kasirrj_belumbayar').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/vaksinrj/ajax_list_kasirrj",
          "type": "GET",
          "data": function(d){
                d.tgl_awal          = $("#tgl_awal").val();   
                d.tgl_akhir         = $("#tgl_akhir").val();
                d.dokter_poli       = $("#dokter_poli").val();
                d.poliruangan       = $("#poliruangan").val();
                d.poliklinik        = $("#poliklinik").val();
                d.status            = $("#status").val();
                d.pasien_nama       = $("#pasien_nama").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.no_rekam_medis    = $("#no_rekam_medis").val();
                d.no_bpjs           = $("#no_bpjs").val();
                d.pasien_alamat     = $("#pasien_alamat").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.urutan            = $("#urutan").val();
               
            }
      },
      //Set column definition initialisation properties.
    });  
    table_kasirrj.columns.adjust().draw();
}