var table_kasirrd;
$(document).ready(function(){
    $('#modal_bayar_kasirrd').on('hidden.bs.modal', function(){
        $(this).find('iframe').html("");
        $(this).find('iframe').attr("src", "");
    });

   table_kasirrd = $('#table_list_kasirrd').DataTable({
//       "sDom": '<"top"f>rt<"bottom"ip><"clear">',
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      // "scrollX": true, 

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrd/ajax_list_kasirrd",
          "type": "GET",
//          "data": function(d){
//                d.tgl_awal = $("#tgl_awal").val();
//                d.tgl_akhir = $("#tgl_akhir").val();
//            }
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]
    });
    table_kasirrd.columns.adjust().draw();

});

function reloadTableKasir(){ 
    table_kasirrd.columns.adjust().draw();
}    

function pulangkanSemua(){ 
  var tgl_kunjungan = $("#tgl_kunjungan").val();        
  swal({      
    text: "Apakah anda yakin ingin memulangkan semua pasien  ?",
    type: 'warning',
    showCancelButton: true,  
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',  
    confirmButtonText: 'Ya' 
  }).then(function(){ 
      $.ajax({    
        url : ci_baseurl + "kasir/kasirrd/set_status_pulang_semua",
        method : 'GET',             
        dataType : 'JSON',            
        data : {tgl_kunjungan : tgl_kunjungan},  
        success : function(data){
          var ret = data.success; 
          console.log('masuk fungsi');
          if(ret === true){ 
            table_kasirrd.columns.adjust().draw();       
            console.log('masuk kondisi true');
            swal( 
              'Berhasil',
              data.messages  
              );
          }else{       
            console.log('masuk kondisi false');
            swal(
              'Gagal',         
              data.messages
              );
          }
        }
      });
  });

}

function bayarTagihan(pendaftaran_id){
    $('#modal_bayar_kasirrd').modal({
      backdrop: 'static',
      keyboard: false,
      show: true
  });
    var src = ci_baseurl + "kasir/kasirrd/bayar_tagihan/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_bayar_kasirrd iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function generate(pendaftaran_id){
  $('#modal_generate').modal('show'); 
  var src = ci_baseurl + "kasir/kasirrd/generate/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
  $("#modal_generate iframe").attr({
      'src': src,
      'height': 380,
      'width': '100%',
      'allowfullscreen':''
  });
}