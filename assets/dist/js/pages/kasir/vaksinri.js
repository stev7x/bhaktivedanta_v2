var table_kasirrj;
$(document).ready(function(){ 
   table_kasirrj = $('#table_list_kasirri').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "bFilter"   : false,
      "ajax": {
        "url": ci_baseurl + "kasir/vaksinri/ajax_list_kasirri",
          "type": "GET",
         "data": function(d){ 
                d.tgl_awal          = $("#tgl_awal").val();   
                d.tgl_akhir         = $("#tgl_akhir").val();
                d.dokter_poli       = $("#dokter_poli").val();
                d.poliruangan       = $("#poliruangan").val();
                d.poliklinik        = $("#poliklinik").val();
                d.status            = $("#status").val();
                d.pasien_nama       = $("#pasien_nama").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.no_rekam_medis    = $("#no_rekam_medis").val();
                d.no_bpjs           = $("#no_bpjs").val();
                d.pasien_alamat     = $("#pasien_alamat").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.urutan            = $("#urutan").val(); 
              } 
      },   
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0 ], //last column  
        "orderable": false //set not orderable
      }
      ]
    });
    table_kasirrj.columns.adjust().draw();
});

function cariPasien(){   
  table_kasirrj = $('#table_list_kasirri').DataTable({
    "destroy": true,
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "bFilter"   : false,
    "ajax": {
      "url": ci_baseurl + "kasir/vaksinri/ajax_list_kasirri",
        "type": "GET",
       "data": function(d){ 
              d.tgl_awal          = $("#tgl_awal").val();   
              d.tgl_akhir         = $("#tgl_akhir").val();
              d.dokter_poli       = $("#dokter_poli").val();
              d.poliruangan       = $("#poliruangan").val();
              d.poliklinik        = $("#poliklinik").val();
              d.status            = $("#status").val();
              d.pasien_nama       = $("#pasien_nama").val();
              d.no_pendaftaran    = $("#no_pendaftaran").val();
              d.no_rekam_medis    = $("#no_rekam_medis").val();
              d.no_bpjs           = $("#no_bpjs").val();
              d.pasien_alamat     = $("#pasien_alamat").val();
              d.no_pendaftaran    = $("#no_pendaftaran").val();
              d.urutan            = $("#urutan").val(); 
            } 
    },   
    //Set column definition initialisation properties.
    "columnDefs": [
    {
      "targets": [ -1,0 ], //last column  
      "orderable": false //set not orderable
    }
    ]
  });
  table_kasirrj.columns.adjust().draw();
}