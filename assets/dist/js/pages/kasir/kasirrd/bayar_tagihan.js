var table_tindakan;
var table_rad;
var table_lab;
var table_obat;
$(document).ready(function(){

  


//    printKwitansi('1');
    table_tindakan = $('#table_tindakan_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrd/ajax_list_tindakan_umum",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan.columns.adjust().draw(); 

    table_rad = $('#table_tindakanrad_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false, 

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrd/ajax_list_tindakan_rad",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_rad.columns.adjust().draw();

    table_lab = $('#table_tindakanlab_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrd/ajax_list_tindakan_lab",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_lab.columns.adjust().draw();

    table_obat = $('#table_obat_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrd/ajax_list_obat_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }  
    }); 
    table_obat.columns.adjust().draw();
          table_kasirrd = $('#table_list_kasirrd').DataTable({ 
//       "sDom": '<"top"f>rt<"bottom"ip><"clear">', 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      // "scrollX": true,   
 
      // Load data for the table's content from an Ajax source
      "ajax": { 
          "url": ci_baseurl + "kasir/kasirrd/ajax_list_kasirrd",
          "type": "GET", 
//          "data": function(d){
//                d.tgl_awal = $("#tgl_awal").val();
//                d.tgl_akhir = $("#tgl_akhir").val();
//            }
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]
    });       
    table_kasirrd.columns.adjust().draw(); 

    $('button#savePembayaran').click(function(){
        swal({ 
          text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){  
             $.ajax({
                    url: ci_baseurl + "kasir/kasirrd/do_create_pembayaran",
                    type: 'POST', 
                    dataType: 'JSON', 
                    data: $('form#fmPembayaran').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide(); 
                            }); 
                            console.log("id pembayaran : " + data.pembayarankasir_id);
                            $('button#print').removeAttr('disabled').removeClass('btn-default').addClass('btn-info'); 
                            $('button#printdetail').removeAttr('disabled').removeClass('btn-default').addClass('btn-info'); 
                            $('button#savePembayaran').attr('disabled','disabled').removeClass('btn-success').addClass('btn-default');    
                            $('button#print').attr('onclick','printKwitansi('+data.pembayarankasir_id+')');       
                            $('button#printdetail').attr('onclick','printDetail('+data.pembayarankasir_id+')');        
                            printKwitansi(data.pembayarankasir_id);   
                            table_kasirrd.columns.adjust().draw();


                                swal( 
                                'Berhasil!',
                                'Data Berhasil Disimpan.', 
                              )

                        } else {    
                            swal( 
                              'Gagal!', 
                              'Data Gagal Disimpan.', 
                            )     
                            // $('.modal_notif').removeClass('green').addClass('red');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();
                            // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            //     $(".modal_notif").hide();
                            // }); 
                        }
                    }
                });
        }) 
    });   
});
function reloadTableKasir(){ 
        table_kasirrd.columns.adjust().draw();
}  

function hitungHarga(){
    subtotal = parseInt($('#subtotal_val').val());
    diskon = parseInt($('#discount').val());

    hitung_diskon = (subtotal * diskon)/100;
    if(!isNaN(hitung_diskon)){
        total = subtotal - hitung_diskon;
    }else{
        total = subtotal;
    }

    $('#total_bayar').val(formatNumber(total));
    $('#total_bayar_val').val(total);
}

function formatNumber(user_input){
    var str = user_input.toString();
    var filtered_number = str.replace(/[^0-9]/gi, '');
    var length = filtered_number.length;
    var breakpoint = 1;
    var formated_number = '';

    for(i = 1; i <= length; i++){
        if(breakpoint > 3){
            breakpoint = 1;
            formated_number = ',' + formated_number;
        }
        var next_letter = i + 1;
        formated_number = filtered_number.substring(length - i, length - (i - 1)) + formated_number;

        breakpoint++;
    } 
    return formated_number;
} 

function printKwitansi(pembayarankasir_id){ 
     console.log("masuk print : "+ci_baseurl+"kasir/kasirrd/print_kwitansi/"+pembayarankasir_id);

    window.open(ci_baseurl+'kasir/kasirrd/print_kwitansi/'+pembayarankasir_id,'Print_Kwitansi','top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

function printDetail(pembayarankasir_id){  
     console.log("masuk print : "+ci_baseurl+"kasir/kasirrd/print_detail/"+pembayarankasir_id); 
 
    window.open(ci_baseurl+'kasir/kasirrd/print_detail/'+pembayarankasir_id,'Print_detail','top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
} 
