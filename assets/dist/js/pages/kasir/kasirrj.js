var table_kasirrj;
var table_kasirrj_sudahbayar;
$(document).ready(function(){ 
   table_kasirrj = $('#table_list_kasirrj_belumbayar').DataTable({
//       "sDom": '<"top"f>rt<"bottom"ip><"clear">',
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "bFilter"   : false,
      // "scrollX": true, 

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_kasirrj",
          "type": "GET",
         "data": function(d){
//             d.tgl_awal = $("#tgl_awal").val();
                d.tgl_awal          = $("#tgl_awal").val();   
                d.tgl_akhir         = $("#tgl_akhir").val();
                d.dokter_poli       = $("#dokter_poli").val();
                d.poliruangan       = $("#poliruangan").val();
                d.poliklinik        = $("#poliklinik").val();
                d.status            = $("#status").val();
                d.pasien_nama       = $("#pasien_nama").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.no_rekam_medis    = $("#no_rekam_medis").val();
                d.no_bpjs           = $("#no_bpjs").val();
                d.pasien_alamat     = $("#pasien_alamat").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.urutan            = $("#urutan").val(); 
              } 
      },   
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0 ], //last column  
        "orderable": false //set not orderable
      }
      ]
    });
    table_kasirrj.columns.adjust().draw();
    

   table_kasirrj_sudahbayar = $('#table_list_kasirrj_sudahbayar').DataTable({
//       "sDom": '<"top"f>rt<"bottom"ip><"clear">',
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "bFilter"   : false,
      // "scrollX": true, 
 
      // Load data for the table's content from an Ajax source
      "ajax": { 
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_kasirrj_sudahbayar",   
          "type": "GET",
         "data": function(d){ 
                d.tgl_awal          = $("#tgl_awal_byr").val();   
                d.tgl_akhir         = $("#tgl_akhir_byr").val();
                d.dokter_poli       = $("#dokter_poli_byr").val();
                d.poliruangan       = $("#poliruangan_byr").val();
                d.poliklinik        = $("#poliklinik_byr").val();
                d.status            = $("#status_byr").val(); 
                d.pasien_nama       = $("#pasien_nama_byr").val();
                d.no_pendaftaran    = $("#no_pendaftaran_byr").val();
                d.no_rekam_medis    = $("#no_rekam_medis_byr").val();
                d.no_bpjs           = $("#no_bpjs_byr").val();
                d.pasien_alamat     = $("#pasien_alamat_byr").val();
                d.no_pendaftaran    = $("#no_pendaftaran_byr").val();
                d.urutan            = $("#urutan_byr").val();  
           }
      }, 
      //Set column definition initialisation properties.
      "columnDefs": [ 
      {
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]
    });  
    table_kasirrj_sudahbayar.columns.adjust().draw();

});

function reloadTableKasir(){
    table_kasirrj.columns.adjust().draw();
}  

$(document).ready(function(){
        $('#change_option').on('change', function(e){
            var valueSelected = this.value;

            console.log("data valuenya : " + valueSelected);
            // alert(valueSelected);
            $('.pilih').attr('id',valueSelected);
            $('.pilih').attr('name',valueSelected);
        });
    });   

$(document).ready(function(){
        $('#change_option_byr').on('change', function(e){
            var valueSelected = this.value;  

            console.log("data valuenya : " + valueSelected);
            // alert(valueSelected);
            $('.pilihbyr').attr('id',valueSelected);
            $('.pilihbyr').attr('name',valueSelected);   
        });
    }); 
     

function cariPasien(){   
    table_kasirrj = $('#table_list_kasirrj_belumbayar').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_kasirrj",
          "type": "GET",
          "data": function(d){
                d.tgl_awal          = $("#tgl_awal").val();   
                d.tgl_akhir         = $("#tgl_akhir").val();
                d.dokter_poli       = $("#dokter_poli").val();
                d.poliruangan       = $("#poliruangan").val();
                d.poliklinik        = $("#poliklinik").val();
                d.status            = $("#status").val();
                d.pasien_nama       = $("#pasien_nama").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.no_rekam_medis    = $("#no_rekam_medis").val();
                d.no_bpjs           = $("#no_bpjs").val();
                d.pasien_alamat     = $("#pasien_alamat").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.urutan            = $("#urutan").val();
               
            }
      },
      //Set column definition initialisation properties.
    });  
    table_kasirrj.columns.adjust().draw();
}

function cariPasienByr(){   
    table_kasirrj = $('#table_list_kasirrj_sudahbayar').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_kasirrj_sudahbayar", 
          "type": "GET",
          "data": function(d){  
                d.tgl_awal          = $("#tgl_awal_byr").val();   
                d.tgl_akhir         = $("#tgl_akhir_byr").val();
                d.dokter_poli       = $("#dokter_poli_byr").val();
                d.poliruangan       = $("#poliruangan_byr").val();
                d.poliklinik        = $("#poliklinik_byr").val();
                d.status            = $("#status_byr").val(); 
                d.pasien_nama       = $("#pasien_nama_byr").val();
                d.no_pendaftaran    = $("#no_pendaftaran_byr").val();
                d.no_rekam_medis    = $("#no_rekam_medis_byr").val();
                d.no_bpjs           = $("#no_bpjs_byr").val();
                d.pasien_alamat     = $("#pasien_alamat_byr").val();
                d.no_pendaftaran    = $("#no_pendaftaran_byr").val();
                d.urutan            = $("#urutan_byr").val();
               
            }
      },
      //Set column definition initialisation properties.
    });  
    table_kasirrj.columns.adjust().draw();
}

function pulangkan(pendaftaran_id){     
  swal({      
    text: "Apakah anda yakin ingin memulangkan pasien ini ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',
    confirmButtonText: 'Ya' 
  }).then(function(){ 
      $.ajax({  
        url : ci_baseurl + "kasir/kasirrj/set_status_pulang",
        method : 'POST',       
        dataType : 'json', 
        data : {pendaftaran_id : pendaftaran_id},
        success : function(data){
          var ret = data.success; 
          if(ret === true){
            table_kasirrj.columns.adjust().draw();       
            swal(
              'Berhasil',
              'Pasien Telah Dipulangkan'
              );
          }else{
            swal(
              'Gagal',  
              'Pasien Gagal Dipulangkan'
              );
          }
        }
      });
  });

}
  
function pulangkanSemua(){ 
  var tgl_kunjungan = $("#tgl_kunjungan").val();        
  swal({      
    text: "Apakah anda yakin ingin memulangkan semua pasien  ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',  
    confirmButtonText: 'Ya' 
  }).then(function(){ 
      $.ajax({  
        url : ci_baseurl + "kasir/kasirrj/set_status_pulang_semua",
        method : 'GET',             
        dataType : 'JSON',            
        data : {tgl_kunjungan : tgl_kunjungan},  
        success : function(data){
          var ret = data.success; 
          console.log('masuk fungsi');
          if(ret === true){
            table_kasirrj.columns.adjust().draw();       
            console.log('masuk kondisi true');
            swal( 
              'Berhasil',
              data.messages
              );
          }else{     
            console.log('masuk kondisi false');
            swal(
              'Gagal',         
              data.messages
              );
          }
        }
      });
  });

}

function bayarTagihan(pendaftaran_id,action){
    // $('#modal_bayar_kasirrj').openModal('toggle');
    var src = ci_baseurl + "kasir/kasirrj/bayar_tagihan/"+pendaftaran_id+"/"+action;  
//    var src = ci_baseurl + "dashboard";
    $("#modal_bayar_kasirrj iframe").attr({ 
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function generate(pendaftaran_id,action){
  // $('#modal_bayar_kasirrj').openModal('toggle');
  var src = ci_baseurl + "kasir/kasirrj/generate/"+pendaftaran_id+"/"+action;  
//    var src = ci_baseurl + "dashboard";
  $("#modal_generate iframe").attr({ 
      'src': src,
      'height': 380,
      'width': '100%',
      'allowfullscreen':''
  });
}

function viewInvoice(pembayarankasir_id, pendaftaran_id) {
    if (pembayarankasir_id == 0) {
        window.open(ci_baseurl+'kasir/kasirrj/view_nota/'+pendaftaran_id)
    } else {
        window.open(ci_baseurl+'kasir/kasirrj/print_kwitansi/'+pembayarankasir_id,'Print_Kwitansi/,/gtop=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
    }
}