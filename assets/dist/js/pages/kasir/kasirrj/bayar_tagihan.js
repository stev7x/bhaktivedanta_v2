var table_tindakan;
var table_rad;
var table_lab;
var table_obat;
var table_obat_pribadi;
var table_tindakan_cover;
var table_obat_cover;

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

$(document).ready(function(){

    $.fn.editable.defaults.mode = 'inline';

    $('a.discount').editable();
    table_tindakan = $('#table_tindakan_kasir').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "kasir/kasirrj/ajax_list_tindakan_umum",
            "type": "GET",
            "cache" : "false",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
                d.action = $("#action").val();
            }
        },

        "columnDefs": [
            {
                "targets": 0,
                "checkboxes": {
                    "selectRow": true
                }
            }
        ],
        "select": {
            "style": 'multi'
        },
        "order": [[1, 'asc']]
    });
    table_tindakan.columns.adjust().draw();

    table_tindakan_cover = $('#table_tindakan_cover').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "kasir/kasirrj/ajax_list_tindakan_cover",
            "type": "GET",
            "data" : function(d){
                d.detail_id = $("#detail_pembayaran_id").val();
                d.pasien_id = $("#pasien_id").val();
            }
        }
    });
    table_tindakan_cover.columns.adjust().draw();

    table_obat_cover = $('#table_obat_cover').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "kasir/kasirrj/ajax_list_obat_cover",
            "type": "GET",
            "data" : function(d){
                d.pembayaran_id = $("#pembayaran_obat_id").val();
                // d.pasien_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_tindakan_cover.columns.adjust().draw();

    table_rad = $('#table_tindakanrad_kasir').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "kasir/kasirrj/ajax_list_tindakan_rad",
            "type": "GET",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_rad.columns.adjust().draw();

    table_lab = $('#table_tindakanlab_kasir').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "kasir/kasirrj/ajax_list_tindakan_lab",
            "type": "GET",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_lab.columns.adjust().draw();

    table_obat = $('#table_obat_kasir').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "kasir/kasirrj/ajax_list_obat_pasien",
            "type": "GET",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
                d.action = $("#action").val();
            }
        }
    });
    table_obat.columns.adjust().draw();

    table_obat_pribadi = $('#table_obat_pribadi_kasir').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "kasir/kasirrj/ajax_list_obat_pribadi_pasien",
            "type": "GET",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_obat.columns.adjust().draw();

    $('button#saveTindakanUmum').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "kasir/kasirrj/do_save_tindakanumum",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmTindakanUmum').serialize(),
                success: function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if(ret === true) {
                        table_tindakan.columns.adjust().draw();
                        // table_tindakan_cover.columns.adjust().draw();

                        $('button#savePembayaran').removeAttr('disabled');

                        swal(
                            "Berhasil",
                            data.messages,
                        );

                    } else {
                        console.log('Gagal Save Tindakan Umum');
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#modal_card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(2000, 1000).slideUp(1000, function(){
                            $(".modal_notif").hide();
                        });
                    }
                }
            });

            $.ajax({
                url: ci_baseurl + "kasir/kasirrj/do_save_tindakanobat",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmObat').serialize(),
                success: function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if(ret === true) {
                        table_obat.columns.adjust().draw();
                        console.log('Berhasil Save Obat');
                    } else {
                        console.log('Gagal Save Obat');
                    }
                }
            });

            table_obat.columns.adjust().draw();
        })
    });

    $('button#saveTindakanObat').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "kasir/kasirrj/do_save_tindakanobat",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmObat').serialize(),
                success: function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if(ret === true) {
                        // $('#val_total_umum').val(data.total);
                        // $('#val_total_umum').attr('disabled/,/gTRUE');
                        // $('#detail_pembayaran_id').val(data.detail_pembayaran_id);
                        // $('button#printAsuransi1').attr('onclick/,/gprintkwitansi_asuransi('+data.pasien_id+/,/g+data.pendaftaran_id+/,/g+data.detail_pembayaran_id+',"'+data.nama_asuransi+'")');
                        // $('button#printAsuransi2').attr('onclick/,/gprintkwitansi_asuransi('+data.pasien_id+/,/g+data.pendaftaran_id+/,/g+data.detail_pembayaran_id+',"'+data.nama_asuransi2+'")');
                        // $(".totalumum *").attr("disabled", "disabled").off('click');
                        table_obat.columns.adjust().draw();
                        // table_tindakan_cover.columns.adjust().draw();
                        console.log('pendaftaran_id : '+data.pendaftaran_id);
                        swal(
                            "Berhasil",
                            data.messages,
                        );

                    } else {
                        console.log('Gagal Save Tindakan Umum');
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#modal_card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(2000, 1000).slideUp(1000, function(){
                            $(".modal_notif").hide();
                        });
                    }
                }
            });

        })
    });


    $('button#saveObat').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "kasir/kasirrj/do_save_obat",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmObat').serialize(),
                success: function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if(ret === true) {
                        $('#pembayaran_obat_id').val(data.pembayaran_obat_id);
                        table_obat.columns.adjust().draw();
                        table_obat_cover.columns.adjust().draw();
                        swal(
                            "Berhasil",
                            data.messages,
                        );

                    } else {
                        console.log('Gagal Save Obat');
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#modal_card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(2000, 1000).slideUp(1000, function(){
                            $(".modal_notif").hide();
                        });
                        // swal(
                        //   "Gagal",
                        //   );
                    }
                }
            });

        })
    });

    $('button#savePembayaran').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "kasir/kasirrj/do_create_pembayaran",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmPembayaran').serialize(),
                success: function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if(ret === true) {
                        $('#discount').attr('readonly', 'true');

                        $('button#print').removeAttr('disabled');
                        $('button#print').removeClass('btn-default').addClass('btn-info');

                        $('button#printAsuransi1').removeAttr('disabled');
                        $('button#printAsuransi1').removeClass('btn-default').addClass('btn-info');

                        $('button#printAsuransi2').removeAttr('disabled');
                        $('button#printAsuransi2').removeClass('btn-default').addClass('btn-info');

                        $('button#printNota').removeAttr('disabled');
                        $('button#printNota').removeClass('btn-default').addClass('btn-info');

                        $('#savePembayaran').removeClass('btn-success').addClass('btn-default');
                        $('#savePembayaran').attr('disabled','gtrue');
                        $('button#print').attr('onclick','gprintKwitansi('+data.pembayarankasir_id+')');

                        $('button#print').attr('onclick','gprintKwitansi('+data.pembayarankasir_id+')');
                        $('button#printNota').attr('onclick','gprintDetail_Kwitansi('+data.pembayarankasir_id+')');

                        $("#pembayarankasir_id").val(data.pembayarankasir_id);
                        $('button#printKwitansi').removeAttr('disabled');

                        printKwitansi(data.pembayarankasir_id);
                        console.log(data.pembayarankasir_id);

                        swal(
                            "Berhasil",
                            "Pembayaran berhasil disimpan"
                        );

                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#modal_card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(2000, 1000).slideUp(1000, function(){
                            $(".modal_notif").hide();
                        });

                        swal(
                            "Gagal",
                            data.messages
                        );
                    }
                }
            });

        })
    });
    reloadTableTindakanCover();
});

function reloadTableTindakanCover(){
    table_tindakan_cover.columns.adjust().draw();
}

function hapusTindakanCover(id){
    console.log('detailbayar_tindakan_id : '+id);
    var detail_id = $('#detail_pembayaran_id').val();
    var pasien_id = $('#pasien_id').val();
    swal({
        text: "Apakah data yang dimasukan sudah benar?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        $.ajax({
            url  : ci_baseurl + "kasir/kasirrj/hapusTindakanCover/"+id,
            type : 'GET',
            dataType : 'json',
            data : {detail_id : detail_id , pasien_id : pasien_id},
            success : function(data){
                $('#val_total_umum').val(data.total);
                table_tindakan.columns.adjust().draw();
                table_tindakan_cover.columns.adjust().draw();
                swal(
                    'Berhasil',
                    'Data Berhasil Dihapus'
                );
            }
        });
    });
}

function hapusObatCover(id){
    console.log('detailbayar_obat_id : '+id);

    swal({
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        $.ajax({
            url  : ci_baseurl + "kasir/kasirrj/hapusObatCover/"+id,
            type : 'GET',
            dataType : 'json',
            // data : {detail_id : detail_id , pasien_id : pasien_id},
            success : function(data){
                var ret = data.success;
                if(ret === true){
                    table_obat.columns.adjust().draw();
                    table_obat_cover.columns.adjust().draw();
                    swal(
                        'Berhasil',
                        'Data Berhasil Dihapus'
                    );
                }else{
                    swal(
                        'Gagal',
                        'Data Gagal Dihapus'
                    );
                }

            }
        });
    });
}

function UbahMetodeBayar(){
    var bSaveTindakan = $('#saveTindakanUmum');
    var carabayar = $('#cara_pembayaran_umum');
    var nama = $('#nama_asuransi_umum');
    var nomor = $('#nomor_asuransi_umum');
    var jumlahbyr = $('#val_total_umum');
    var bUbahTindakan = $('#ubahTindakanUmum');

    bSaveTindakan.removeAttr('disabled');
    carabayar.removeAttr('disabled');
    nama.removeAttr('disabled');
    nomor.removeAttr('disabled');
    jumlahbyr.removeAttr('disabled');
    bUbahTindakan.hide(100);

}



function hitungHargaItem(){
    total = parseInt($('#total_harga').text());
    diskon = parseInt($('#val_diskon').text());

    console.log("total : "+total);
    console.log("discount : "+diskon);

    hitung_diskon = (subtotal * diskon)/100;
    if(!isNaN(hitung_diskon)){
        total = subtotal - hitung_diskon;
    }else{
        total = subtotal;
    }

    $('#total_bayar').val(formatNumber(total));
    $('#total_bayar_val').val(total);
}

function hitungHarga(){
    subtotal = parseInt($('#subtotal_val').val());
    diskon = parseInt($('#discount').val());

    hitung_diskon = (subtotal * diskon)/100;
    if(!isNaN(hitung_diskon)){
        total = subtotal - hitung_diskon;
    }else{
        total = subtotal;
    }

    $('#total_bayar').val(formatNumber(total));
    $('#total_bayar_val').val(total);
}

function hitungTotal(){
    var total1 = $('#val_total_umum').val();
    var total = $('#total_value').val();

    console.log("Tindakan Umum :"+total1);
    console.log("Total :"+total) ;

    if(total1 == 0 || total1 == null){
        hasil = total;
    }else{
        hasil = total - total1;
    }
    console.log("hasil : "+hasil);

    $('#total_bayar').val(formatNumber(hasil));
    $('#total_bayar_val').val(hasil);
    $('#subtotal_val').val(hasil);
    $('#sisa_pembayaran').val(hasil);
}

function hitungSurcharge() {
    var surcharge    = $('#surcharge').val().toString().replace(/,/g, '');
    var tindakanUmum = $('#totaltindakanumum').val().toString().replace(/,/g, '');
    var obat         = $('#totaltindakanobat').val().toString().replace(/,/g, '');
    var diskon       = $('#discount').val().toString().replace(/,/g, '');

    // if (surcharge === "") {
    //     surcharge = 0;
    // }

    var newSubTotal = +tindakanUmum + +obat + +surcharge;
    var newTotal    = +newSubTotal - +diskon;

    $('#total_bayar').val(formatNumber(newTotal));
    $('#total_bayar_val').val(newTotal);
    $('#subtotal').val(formatNumber(newSubTotal));
    $('#subtotal_val').val(newSubTotal);
    $('#total_value').val(newSubTotal);
    $('#surcharge').val(formatNumber(surcharge));
}

function open_diskon(id){
    $('#btnDiskonPersen').attr("onclick","hitungdiskon('tindakan','persen')");
    $('#btnDiskonNominal').attr("onclick","hitungdiskon('tindakan','nominal')");

    $('#tindakanpasien_id').val(id);
    $('#reseptur_id').val("");
    $('#fee_dokter_id').val("");

    $('#modal_diskon').modal("toggle");
}

function open_diskon_obat(id){
    $('#btnDiskonPersen').attr("onclick","hitungdiskon('obat','persen')");
    $('#btnDiskonNominal').attr("onclick","hitungdiskon('obat','nominal')");

    $('#tindakanpasien_id').val("");
    $('#reseptur_id').val(id);
    $('#fee_dokter_id').val("");

    $('#modal_diskon').modal("toggle");
}

function open_diskon_dokter(id){
    $('#btnDiskonPersen').attr("onclick","hitungdiskon('dokter','persen')");
    $('#btnDiskonNominal').attr("onclick","hitungdiskon('dokter','nominal')");

    $('#tindakanpasien_id').val("");
    $('#reseptur_id').val("");
    $('#fee_dokter_id').val(id);

    $('#modal_diskon').modal("toggle");
}

function reset_diskon(id){
    $('#tindakanpasien_id').val(id);
    $('#reseptur_id').val("");
    $('#fee_dokter_id').val("");

    $('#diskon_nominal').val(0);

    hitungdiskon('tindakan','nominal');
}

function reset_diskon_obat(id){
    $('#tindakanpasien_id').val("");
    $('#reseptur_id').val(id);
    $('#fee_dokter_id').val("");

    ('#diskon_nominal').val(0);

    hitungdiskon('tindakan','nominal');
}

function riset_diskon_dokter(id){
    $('#tindakanpasien_id').val("");
    $('#reseptur_id').val("");
    $('#fee_dokter_id').val(id);

    ('#diskon_nominal').val(0);

    hitungdiskon('tindakan','nominal');
}

function hitungdiskon(jenis, type){
    var tindakanpasien_id = $('#tindakanpasien_id').val();
    var reseptur_id       = $('#reseptur_id').val();
    var fee_dokter_id     = $('#fee_dokter_id').val();
    var action            = $('#action').val();

    if(type == "persen"){
        var persen = $('#diskon_persen').val();
    }else{
        var nominal = $('#diskon_nominal').val();
    }

    console.log(persen);

    swal({
        text: "Apakah data yang dimasukan sudah benar?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        $.ajax({
            url:ci_baseurl + "kasir/kasirrj/hitungdiskon",
            method : 'POST',
            dataType : 'json',
            data : {jenis:jenis ,persen:persen , nominal : nominal, tindakanpasien_id:tindakanpasien_id , reseptur_id:reseptur_id, fee_dokter_id:fee_dokter_id,action:action},
            success : function(data){
                var ret = data.status;
                if(ret == "success"){
                    $('#modal_diskon').modal('hide');
                    table_tindakan.columns.adjust().draw();
                    table_obat.columns.adjust().draw();

                    var currentSurcharge     = $('#surcharge').val().toString().replace(/,/g, '');

                    if (jenis === "tindakan") {
                        var currentTotalTindakan = $('#totaltindakan').text().toString().replace(/,/g, '');
                        var currentTotalDiskon   = $('#discount').val().toString().replace(/,/g, '');
                        var currentTotalBayar    = $('#total_bayar').val().toString().replace(/,/g, '');

                        var updateTotalTindakan = +currentTotalTindakan - +data.diskon;
                        var updateTotalDiskon   = +currentTotalDiskon + +data.diskon;
                        var plainTotalBayar     = (+currentTotalBayar - +currentSurcharge) - +data.diskon;
                        var updateSurcharge     = +plainTotalBayar * 0.03;
                        var updateTotalBayar    = +plainTotalBayar + +updateSurcharge;
                        var updateSubTotal      = +updateTotalBayar + +updateTotalDiskon;

                        $('#surcharge').val(formatNumber(updateSurcharge));
                        $('#totaltindakan').text(formatNumber(updateTotalTindakan));
                        $('#discount').val(formatNumber(updateTotalDiskon));
                        $('#total_bayar').val(formatNumber(updateTotalBayar));
                        $('#total_bayar_val').val(updateTotalBayar);
                        $('#subtotal').val(formatNumber(updateSubTotal));
                        $('#subtotal_val').val(updateSubTotal);
                        $('#total_value').val(updateSubTotal);
                    } else if (jenis === "obat") {
                        var currentTotalObat   = $('#totalobat').text().toString().replace(/,/g, '');
                        var currentTotalDiskon = parseInt($('#discount').val().toString().replace(/,/g, ''));
                        var currentTotalBayar  = $('#total_bayar').val().toString().replace(/,/g, '');

                        var updateTotalObat    = +currentTotalObat - +data.diskon;
                        var updateTotalDiskon  = +currentTotalDiskon + +data.diskon;
                        var plainTotalBayar    = (+currentTotalBayar - +currentSurcharge) - +data.diskon;
                        var updateSurchare     = +plainTotalBayar * 0.03;
                        var updateTotalBayar   = +plainTotalBayar + +updateSurchare;
                        var updateSubTotal     = +updateTotalBayar + +updateTotalDiskon;

                        $('#surcharge').val(formatNumber(updateSurchare));
                        $('#totalobat').text(formatNumber(updateTotalObat));
                        $('#discount').val(formatNumber(updateTotalDiskon));
                        $('#total_bayar').val(formatNumber(updateTotalBayar));
                        $('#total_bayar_val').val(updateTotalBayar);
                        $('#subtotal').val(formatNumber(updateSubTotal));
                        $('#subtotal_val').val(updateSubTotal);
                        $('#total_value').val(updateSubTotal);
                    } else {
                        var currentTotalTindakan = $('#totaltindakan').text().toString().replace(/,/g, '');
                        var currentTotalDiskon   = $('#discount').val().toString().replace(/,/g, '');
                        var currentTotalBayar    = $('#total_bayar').val().toString().replace(/,/g, '');

                        var updateTotalTindakan = +currentTotalTindakan - +data.diskon;
                        var updateTotalDiskon   = +currentTotalDiskon + +data.diskon;
                        var plainTotalBayar     = (+currentTotalBayar - +currentSurcharge) - +data.diskon;
                        var updateSurchare      = +plainTotalBayar * 0.03;
                        var updateTotalBayar    = +plainTotalBayar + +updateSurchare;
                        var updateSubTotal      = +updateTotalBayar + +updateTotalDiskon;

                        $('#surcharge').val(formatNumber(updateSurchare));
                        $('#totaltindakan').text(formatNumber(updateTotalTindakan));
                        $('#discount').val(formatNumber(updateTotalDiskon));
                        $('#total_bayar').val(formatNumber(updateTotalBayar));
                        $('#total_bayar_val').val(updateTotalBayar);
                        $('#subtotal').val(formatNumber(updateSubTotal));
                        $('#subtotal_val').val(updateSubTotal);
                        $('#total_value').val(updateSubTotal);
                    }

                    swal(
                        'Berhasil'
                    )
                }else{
                    swal(
                        'Gagal'
                    )
                }
            }
        });
    })
}

function printKwitansi(pembayarankasir_id) {
    if (pembayarankasir_id == "") {
        pembayarankasir_id = $("#pembayarankasir_id").val();
    }

    window.open(ci_baseurl+'kasir/kasirrj/print_kwitansi/'+pembayarankasir_id,'Print_Kwitansi/,/gtop=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

function printKwitansiN() {
    pembayarankasir_id = $("#pembayarankasir_id").val();

    window.open(ci_baseurl+'kasir/kasirrj/print_kwitansi/'+pembayarankasir_id,'Print_Kwitansi/,/gtop=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

function printkwitansi_asuransi(pasien_id,pendaftaran_id,detail_id,jenis_asuransi){
    // var jenis_asuransi = $('#asuransi1').val();
    var detailobat_id = $('#pembayaran_obat_id').val();
    if(detailobat_id == '' || detailobat_id == null){
        detailobat_id =null;
    }
    if(detail_id == '' || detail_id == null){
        detail_id = null;
    }

    console.log("pendaftaran_id : "+pendaftaran_id);
    console.log("detailobat_id : "+detailobat_id);
    console.log("jenis_asuransi : "+jenis_asuransi);
    window.open(ci_baseurl+'kasir/kasirrj/print_kwitansi_2/'+pasien_id+'/'+pendaftaran_id+'/'+detail_id+'/'+detailobat_id+'/'+jenis_asuransi,'Print_Kwitansi/,/gtop=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

function printDetail_Kwitansi(pembayarankasir_id){
    window.open(ci_baseurl+'kasir/kasirrj/printNota/'+pembayarankasir_id,'Print_Kwitansi/,/gtop=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no');
}

// Change Cara Pembayaran Umum
function doCaraBayar(cara_bayar) {
    $("#container_cash_umum").show(300);
    if (cara_bayar === "EDC") {
        $("#container_tf_umum").hide();
        $("#container_cc_umum").hide();
        $("#container_edc_umum").show(300);
    } else if (cara_bayar === "CC") {
        $("#container_tf_umum").hide();
        $("#container_cc_umum").show(300);
        $("#container_edc_umum").hide();
    } else if (cara_bayar === "TF") {
        $("#container_tf_umum").show(300);
        $("#container_cc_umum").hide();
        $("#container_edc_umum").hide();
    } else {
        $("#container_tf_umum").hide();
        $("#container_cc_umum").hide();
        $("#container_edc_umum").hide();
    }
}

function doCaraBayarObat(cara_bayar) {
    $("#container_cash_umum").show(300);
    if (cara_bayar === "EDC") {
        $("#container_tf_umum_obat").hide();
        $("#container_cc_umum_obat").hide();
        $("#container_edc_umum_obat").show(300);
    } else if (cara_bayar === "CC") {
        $("#container_tf_umum_obat").hide();
        $("#container_cc_umum_obat").show(300);
        $("#container_edc_umum_obat").hide();
    } else if (cara_bayar === "TF") {
        $("#container_tf_umum_obat").show(300);
        $("#container_cc_umum_obat").hide();
        $("#container_edc_umum_obat").hide();
    } else {
        $("#container_tf_umum_obat").hide();
        $("#container_cc_umum_obat").hide();
        $("#container_edc_umum_obat").hide();
    }
}

function showCaraBayar(cara_bayar) {
    if (cara_bayar === "Pribadi") {
        $("#asuransi-container").hide();
        $("#metodebayar").show(300);
    } else if (cara_bayar === "Asuransi") {
        $("#asuransi-container").show(300);
        $("#metodebayar").hide();
    } else if (cara_bayar === "Bill Hotel") {
        $("#asuransi-container").hide();
        $("#metodebayar").hide();
    } else {
        $("#asuransi-container").hide();
        $("#metodebayar").hide();
    }
}

function cekListTindakan(e) {
    // $.ajax({
    //     url: ci_baseurl + "kasir/kasirrj/get_items_by_id",
    //     type: 'GET',
    //     dataType: 'JSON',
    //     data: {id:e.value, jenis:0},
    //     success: function(data){
    //         var ret = data.success;
    //         if(ret === true) {
    //             console.log(data);
    //             var currentTotalTindakan     = $('#totaltindakan').text().toString().replace(/,/g, '');
    //             var currentTotalDiskon       = $('#discount').val().toString().replace(/,/g, '');
    //             var currentTotalBayar        = $('#total_bayar').val().toString().replace(/,/g, '');
    //             var currentTotalTindakanUmum = $('#totaltindakanumum').val().toString().replace(/,/g, '');
    //             var currentSurcharge         = $('#surcharge').val().toString().replace(/,/g, '');
    //
    //             if (e.checked) {
    //                 var updateTotalTindakan     = +currentTotalTindakan + +data.data.total_harga;
    //                 var updateTotalDiskon       = +currentTotalDiskon + +data.data.diskon;
    //                 var plainTotalBayar         = (+currentTotalBayar - +currentSurcharge) + +data.data.total_harga;
    //                 var updateSurcharge         = +plainTotalBayar * 0.03;
    //                 var updateTotalBayar        = +plainTotalBayar + +updateSurcharge;
    //                 var updateTotalTindakanUmum = +currentTotalTindakanUmum + (+data.data.total_harga + +data.data.diskon);
    //             } else {
    //                 var updateTotalTindakan     = +currentTotalTindakan - +data.data.total_harga;
    //                 var updateTotalDiskon       = +currentTotalDiskon - +data.data.diskon;
    //                 var plainTotalBayar         = (+currentTotalBayar - +currentSurcharge) - +data.data.total_harga;
    //                 var updateSurcharge         = +plainTotalBayar * 0.03;
    //                 var updateTotalBayar        = +plainTotalBayar + +updateSurcharge;
    //                 var updateTotalTindakanUmum = +currentTotalTindakanUmum - (+data.data.total_harga + +data.data.diskon);
    //             }
    //
    //             var updateSubTotal = +updateTotalBayar + +data.data.diskon;
    //
    //             $('#surcharge').val(formatNumber(updateSurcharge));
    //             $('#totaltindakan').text(formatNumber(updateTotalTindakan));
    //             $('#discount').val(formatNumber(updateTotalDiskon));
    //             $('#total_bayar').val(formatNumber(updateTotalBayar));
    //             $('#total_bayar_val').val(updateTotalBayar);
    //             $('#totaltindakanumum').val(formatNumber(updateTotalTindakanUmum));
    //             $('#subtotal').val(formatNumber(updateSubTotal));
    //             $('#subtotal_val').val(updateSubTotal);
    //             $('#total_value').val(updateSubTotal);
    //         }
    //     }
    // });
}

function cekListObat(e) {
    // $.ajax({
    //     url: ci_baseurl + "kasir/kasirrj/get_items_by_id",
    //     type: 'GET',
    //     dataType: 'JSON',
    //     data: {id:e.value, jenis:1},
    //     success: function(data){
    //         var ret = data.success;
    //         if(ret === true) {
    //             console.log(data);
    //             var currentTotalObat         = $('#totalobat').text().toString().replace(/,/g, '');
    //             var currentTotalDiskon       = $('#discount').val().toString().replace(/,/g, '');
    //             var currentTotalBayar        = $('#total_bayar').val().toString().replace(/,/g, '');
    //             var currentTotalTindakanObat = $('#totaltindakanobat').val().toString().replace(/,/g, '');
    //             var currentSubTotal          = $('#subtotal').val().toString().replace(/,/g, '');
    //             var currentSurcharge         = $('#surcharge').val().toString().replace(/,/g, '');
    //
    //             console.log(currentSurcharge);
    //
    //             if (e.checked) {
    //                 var updateTotalObat         = +currentTotalObat + +data.data.harga_total;
    //                 var updateTotalDiskon       = +currentTotalDiskon + +data.data.diskon;
    //                 var plainTotalBayar         = (+currentTotalBayar - +currentSurcharge) + +data.data.harga_total;
    //                 var updateSurcharge         = +plainTotalBayar * 0.03;
    //                 var updateTotalBayar        = +plainTotalBayar + +updateSurcharge;
    //                 var updateTotalTindakanObat = +currentTotalTindakanObat + (+data.data.harga_total + +data.data.diskon);
    //             } else {
    //                 var updateTotalObat         = +currentTotalObat - (+data.data.harga_total);
    //                 var updateTotalDiskon       = +currentTotalDiskon - +data.data.diskon;
    //                 var updateTotalTindakanObat = +currentTotalTindakanObat - (+data.data.harga_total + +data.data.diskon);
    //
    //                 if (updateTotalObat == 0) {
    //                     var updateTotalBayar    = (+currentTotalBayar - +data.data.harga_total) - +currentSurcharge;
    //                     var updateSurcharge     = 0;
    //                 } else {
    //                     var plainTotalBayar     = (+currentTotalBayar - +currentSurcharge) + +data.data.harga_total;
    //                     var updateSurcharge     = +updatePlainTotalBayar * 0.03;
    //                     var updateTotalBayar    = plainTotalBayar + updateSurcharge;
    //                 }
    //             }
    //
    //             var updateSubTotal = +updateTotalBayar - +updateTotalDiskon;
    //
    //             $('#surcharge').val(formatNumber(updateSurcharge));
    //             $('#totalobat').text(formatNumber(updateTotalObat));
    //             $('#discount').val(formatNumber(updateTotalDiskon));
    //             $('#total_bayar').val(formatNumber(updateTotalBayar));
    //             $('#total_bayar_val').val(updateTotalBayar);
    //             $('#totaltindakanobat').val(formatNumber(updateTotalTindakanObat));
    //             $('#subtotal').val(formatNumber(updateSubTotal));
    //             $('#subtotal_val').val(updateSubTotal);
    //             $('#total_value').val(updateSubTotal);
    //         }
    //     }
    // });
}

function cekListDokter(e) {
    // $.ajax({
    //     url: ci_baseurl + "kasir/kasirrj/get_items_by_id",
    //     type: 'GET',
    //     dataType: 'JSON',
    //     data: {id:e.value, jenis:2},
    //     success: function(data){
    //         var ret = data.success;
    //         if(ret === true) {
    //             console.log(data);
    //             var currentTotalTindakan     = $('#totaltindakan').text().toString().replace(/,/g, '');
    //             var currentTotalDiskon       = $('#discount').val().toString().replace(/,/g, '');
    //             var currentTotalBayar        = $('#total_bayar').val().toString().replace(/,/g, '');
    //             var currentTotalTindakanUmum = $('#totaltindakanumum').val().toString().replace(/,/g, '');
    //             var currentSurcharge         = $('#surcharge').val().toString().replace(/,/g, '');
    //
    //             if (e.checked) {
    //                 var updateTotalTindakan     = +currentTotalTindakan + +data.data.total_harga;
    //                 var updateTotalDiskon       = +currentTotalDiskon + +data.data.diskon;
    //                 var plainTotalBayar         = (+currentTotalBayar - +currentSurcharge) + +data.data.total_harga;
    //                 var updateSurcharge         = +plainTotalBayar * 0.03;
    //                 var updateTotalBayar        = +plainTotalBayar + +updateSurcharge;
    //                 var updateTotalTindakanUmum = +currentTotalTindakanUmum + (+data.data.total_harga + +data.data.diskon);
    //             } else {
    //                 var updateTotalTindakan     = +currentTotalTindakan - +data.data.total_harga;
    //                 var updateTotalDiskon       = +currentTotalDiskon - +data.data.diskon;
    //                 var plainTotalBayar         = (+currentTotalBayar - +currentSurcharge) - +data.data.total_harga;
    //                 var updateSurcharge         = +plainTotalBayar * 0.03;
    //                 var updateTotalBayar        = +plainTotalBayar + +updateSurcharge;
    //                 var updateTotalTindakanUmum = +currentTotalTindakanUmum - (+data.data.total_harga + +data.data.diskon);
    //             }
    //
    //             var updateSubTotal = +updateTotalBayar + +data.data.diskon;
    //
    //             $('#surcharge').val(formatNumber(updateSurcharge));
    //             $('#totaltindakan').text(formatNumber(updateTotalTindakan));
    //             $('#discount').val(formatNumber(updateTotalDiskon));
    //             $('#total_bayar').val(formatNumber(updateTotalBayar));
    //             $('#total_bayar_val').val(updateTotalBayar);
    //             $('#totaltindakanumum').val(formatNumber(updateTotalTindakanUmum));
    //             $('#subtotal').val(formatNumber(updateSubTotal));
    //             $('#subtotal_val').val(updateSubTotal);
    //             $('#total_value').val(updateSubTotal);
    //         }
    //     }
    // });
}

function reloadTableTindakan(){
    console.log('hadir reload');
    location.reload();
}

// disable mousewheel on a input number field when in focus
// (to prevent Cromium browsers change the value when scrolling)
$('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault()
    })
})
$('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
})


function bayar_tagihan_hide(className, carabayar, type){
    var elements = document.getElementsByClassName(className)

    for (var i = 0; i < elements.length; i++){
        elements[i].style.display = 'none';
    }

    var idDisplay = "asuransi-container";

    switch (carabayar.toLowerCase()) {
        case "pribadi":
            if (type == 0)
                idDisplay = "pribadi-container";
            else if (type == 3)
                idDisplay = "pribadi-container-obat";
            break;
        case "asuransi":
            if (type == 0)
                idDisplay = "asuransi-container";
            else if (type == 3)
                idDisplay = "asuransi-container-obat";
            break;
        case "bill hotel":
            if (type == 0)
                idDisplay = "bill-hotel-container";
            else if (type == 3)
                idDisplay = "bill-hotel-container-obat";
            break;
        case "tanggungan hotel":
            if (type == 0)
                idDisplay = "tanggungan-hotel-container";
            else if (type == 3)
                idDisplay = "tanggungan-hotel-container-obat";
            break;
    }

    document.getElementById(idDisplay).style.display = 'block';
}