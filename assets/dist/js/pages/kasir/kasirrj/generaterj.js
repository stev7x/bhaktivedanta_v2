var table_tindakan;
var table_rad;
var table_lab;
var table_obat;
var table_obat_pribadi; 
var table_tindakan_cover;
var table_obat_cover;
$(document).ready(function(){ 

    $.fn.editable.defaults.mode = 'inline';
         
    $('a.discount').editable();
    table_tindakan = $('#table_tindakan_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,     
      "paging": false, 
      "sDom": '<"top"l>rt<"bottom"p><"clear">',   
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_tindakan_umum",
          "type": "GET",
          "cache" : "false",  
          "data" : function(d){     
              d.pendaftaran_id = $("#pendaftaran_id").val();
              d.action = $("#action").val();     
          }    
      },

      "columnDefs": [ 
         {
            "targets": 0,
            "checkboxes": {
               "selectRow": true
            }
         }
      ],
      "select": {
         "style": 'multi'
      },
      "order": [[1, 'asc']]  
    });      
    table_tindakan.columns.adjust().draw(); 
    
    table_tindakan_cover = $('#table_tindakan_cover').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false, 

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_tindakan_cover",
          "type": "GET", 
          "data" : function(d){ 
              d.detail_id = $("#detail_pembayaran_id").val();
              d.pasien_id = $("#pasien_id").val(); 
          }
      } 
    });      
    table_tindakan_cover.columns.adjust().draw(); 
  
    table_obat_cover = $('#table_obat_cover').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false, 
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false, 

      // Load data for the table's content from an Ajax source
      "ajax": { 
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_obat_cover",
          "type": "GET",   
          "data" : function(d){                       
              d.pembayaran_id = $("#pembayaran_obat_id").val();
              // d.pasien_id = $("#pendaftaran_id").val(); 
          }
      }
    });      
    table_tindakan_cover.columns.adjust().draw();

    table_rad = $('#table_tindakanrad_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_tindakan_rad",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_rad.columns.adjust().draw();

    table_lab = $('#table_tindakanlab_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_tindakan_lab",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });  
    table_lab.columns.adjust().draw();
    
    table_obat = $('#table_obat_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">', 
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_obat_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
              d.action = $("#action").val();   
          }
      } 
    });    
    table_obat.columns.adjust().draw();    
     
    table_obat_pribadi = $('#table_obat_pribadi_kasir').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source 
      "ajax": {
          "url": ci_baseurl + "kasir/kasirrj/ajax_list_obat_pribadi_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_obat.columns.adjust().draw();   

    $('button#saveTindakanUmum').click(function(){ 
      swal({
          text: "Apakah data yang dimasukan sudah benar?",
          type: 'warning',
          showCancelButton: true, 
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya'  
        }).then(function(){       
            $.ajax({ 
                    url: ci_baseurl + "kasir/kasirrj/do_save_tindakanumumrj",
                    type: 'POST',  
                    dataType: 'JSON', 
                    data: $('form#fmTindakanUmum').serialize(), 
                    success: function(data){                   
                        var ret = data.success;   
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        if(ret === true) {
                            $('#val_total_umum').val(data.total);            
                            $('#val_total_umum').attr('disabled','TRUE');                   
                            $('#detail_pembayaran_id').val(data.detail_pembayaran_id);                       
                            $('button#printAsuransi1').attr('onclick','printkwitansi_asuransi('+data.pasien_id+','+data.pendaftaran_id+','+data.detail_pembayaran_id+',"'+data.nama_asuransi+'")'); 
                            $('button#printAsuransi2').attr('onclick','printkwitansi_asuransi('+data.pasien_id+','+data.pendaftaran_id+','+data.detail_pembayaran_id+',"'+data.nama_asuransi2+'")'); 
                            $(".totalumum *").attr("disabled", "disabled").off('click');                   
                            table_tindakan.columns.adjust().draw();            
                            table_tindakan_cover.columns.adjust().draw(); 
                            console.log('pendaftaran_id : '+data.pendaftaran_id);
                            swal(                      
                              "Berhasil",   
                              data.messages,  
                              );                
  
                        } else {     
                            console.log('Gagal Save Tindakan Umum');
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#modal_card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(2000, 1000).slideUp(1000, function(){
                                $(".modal_notif").hide();
                            });
                        }
                    }
                });             
          
        })
  });

   
   $('button#saveObat').click(function(){  
      swal({  
          text: "Apakah data yang dimasukan sudah benar?",
          type: 'warning',
          showCancelButton: true, 
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya'  
        }).then(function(){   
            $.ajax({ 
                    url: ci_baseurl + "kasir/kasirrj/do_save_obatrj",
                    type: 'POST',  
                    dataType: 'JSON',
                    data: $('form#fmObat').serialize(), 
                    success: function(data){           
                        var ret = data.success;   
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        if(ret === true) { 
                            $('#pembayaran_obat_id').val(data.pembayaran_obat_id);
                            table_obat.columns.adjust().draw();    
                            table_obat_cover.columns.adjust().draw(); 
                            swal(                         
                              "Berhasil",
                              data.messages,   
                              );                
  
                        } else {      
                            console.log('Gagal Save Tindakan Umum');
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#modal_card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(2000, 1000).slideUp(1000, function(){
                                $(".modal_notif").hide();
                            });
                            // swal(
                            //   "Gagal",
                            //   ); 
                        }  
                    }
                });           
          
        })
  });   

    $('button#savePembayaran').click(function(){
      swal({
          text: "Apakah data yang dimasukan sudah benar?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "kasir/kasirrj/do_create_pembayaranrj",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmPembayaran').serialize(),
                    success: function(data){
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        if(ret === true) {
                            $('#discount').attr('readonly','true');
                            
                            $('button#print').removeAttr('disabled');  
                            $('button#print').removeClass('btn-default').addClass('btn-info');

                            $('button#printAsuransi1').removeAttr('disabled');   
                            $('button#printAsuransi1').removeClass('btn-default').addClass('btn-info');
                              
                            $('button#printAsuransi2').removeAttr('disabled');    
                            $('button#printAsuransi2').removeClass('btn-default').addClass('btn-info');

                            $('button#printNota').removeAttr('disabled');      
                            $('button#printNota').removeClass('btn-default').addClass('btn-info');      



                            $('#savePembayaran').removeClass('btn-success').addClass('btn-default');  
                            $('#savePembayaran').attr('disabled','true');      
                            $('button#print').attr('onclick','printKwitansi('+data.pembayarankasir_id+')'); 
                             
                            $('button#print').attr('onclick','printKwitansi('+data.pembayarankasir_id+')'); 
                            $('button#printNota').attr('onclick','printDetail_Kwitansi('+data.pembayarankasir_id+')'); 
                            // printKwitansi(data.pembayarankasir_id);      
                            swal(
                              "Berhasil",
                              ); 
  
                        } else { 
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#modal_card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(2000, 1000).slideUp(1000, function(){
                                $(".modal_notif").hide();
                            });
                        }
                    }
                });           
          
        }) 
  });    
    reloadTableTindakanCover();        

});
function reloadTableTindakanCover(){
  table_tindakan_cover.columns.adjust().draw();
}


function hapusTindakanCover(id){
  console.log('detailbayar_tindakan_id : '+id);
  var detail_id = $('#detail_pembayaran_id').val();
  var pasien_id = $('#pasien_id').val();   
   swal({          
          text: "Apakah data yang dimasukan sudah benar?",
          type: 'warning',    
          showCancelButton: true, 
          confirmButtonColor: '#3085d6',  
          cancelButtonColor: '#d33', 
          cancelButtonText: 'Tidak',   
          confirmButtonText: 'Ya'         
        }).then(function(){     
            $.ajax({   
            url  : ci_baseurl + "kasir/kasirrj/hapusTindakanCover/"+id,
            type : 'GET', 
            dataType : 'json',         
            data : {detail_id : detail_id , pasien_id : pasien_id},
            success : function(data){   
              $('#val_total_umum').val(data.total);    
              table_tindakan.columns.adjust().draw();
              table_tindakan_cover.columns.adjust().draw();
              swal(
                'Berhasil',
                'Data Berhasil Dihapus'
                );   
            }
          }); 
        });
}

function hapusObatCover(id){
  console.log('detailbayar_obat_id : '+id);
   
   swal({          
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',    
          showCancelButton: true, 
          confirmButtonColor: '#3085d6',     
          cancelButtonColor: '#d33', 
          cancelButtonText: 'Tidak',   
          confirmButtonText: 'Ya'         
        }).then(function(){     
            $.ajax({   
            url  : ci_baseurl + "kasir/kasirrj/hapusObatCover/"+id,
            type : 'GET', 
            dataType : 'json',              
            // data : {detail_id : detail_id , pasien_id : pasien_id},
            success : function(data){
              var ret = data.success;
              if(ret === true){
                table_obat.columns.adjust().draw();
                table_obat_cover.columns.adjust().draw();
                swal(
                  'Berhasil',
                  'Data Berhasil Dihapus'
                  );  
              }else{
                swal( 
                  'Gagal',
                  'Data Gagal Dihapus'
                  );
              }    
                 
            }
          }); 
        });
}

function UbahMetodeBayar(){ 
      var bSaveTindakan = $('#saveTindakanUmum');
      var carabayar = $('#cara_pembayaran_umum');
      var nama = $('#nama_asuransi_umum');
      var nomor = $('#nomor_asuransi_umum');
      var jumlahbyr = $('#val_total_umum'); 
      var bUbahTindakan = $('#ubahTindakanUmum');
    
    bSaveTindakan.removeAttr('disabled'); 
    carabayar.removeAttr('disabled');
    nama.removeAttr('disabled');
    nomor.removeAttr('disabled'); 
    jumlahbyr.removeAttr('disabled');           
    bUbahTindakan.hide(100);    

}



function hitungHargaItem(){ 
    total = parseInt($('#total_harga').text());
    diskon = parseInt($('#val_diskon').text());  

         
    console.log("total : "+total);
    console.log("discount : "+diskon);  
    
    hitung_diskon = (subtotal * diskon)/100;
    if(!isNaN(hitung_diskon)){
        total = subtotal - hitung_diskon;
    }else{
        total = subtotal;
    } 
    
    $('#total_bayar').val(formatNumber(total));
    $('#total_bayar_val').val(total);
}            

function hitungHarga(){
    subtotal = parseInt($('#subtotal_val').val());
    diskon = parseInt($('#discount').val());
    
    hitung_diskon = (subtotal * diskon)/100;
    if(!isNaN(hitung_diskon)){
        total = subtotal - hitung_diskon;
    }else{
        total = subtotal;
    } 
    
    $('#total_bayar').val(formatNumber(total));
    $('#total_bayar_val').val(total);
}
    
function hitungTotal(){
  var total1 = $('#val_total_umum').val();
  var total = $('#total_value').val();           
   
   console.log("Tindakan Umum :"+total1);     
   console.log("Total :"+total) ;   
      
  if(total1 == 0 || total1 == null){ 
        hasil = total;  
    }else{        
        hasil = total - total1; 
    } 
    console.log("hasil : "+hasil);

  $('#total_bayar').val(formatNumber(hasil));
  $('#total_bayar_val').val(hasil);      
  $('#subtotal_val').val(hasil);      
  $('#sisa_pembayaran').val(hasil);          
} 

  

 function open_diskon(id){    
  $('#btnDiskonPersen').attr("onclick","hitungdiskon('tindakan','persen')");
  $('#btnDiskonNominal').attr("onclick","hitungdiskon('tindakan','nominal')");
  $('#tindakanpasien_id').val(id);             
 }
 function open_diskon_obat(id){    
  $('#btnDiskonPersen').attr("onclick","hitungdiskon('obat','persen')");
  $('#btnDiskonNominal').attr("onclick","hitungdiskon('obat','nominal')");
  $('#reseptur_id').val(id);                   
 } 

 function hitungdiskon(jenis,type){       
   
    var tindakanpasien_id = $('#tindakanpasien_id').val();
    var reseptur_id       = $('#reseptur_id').val();          
    var action            = $('#action').val(); 
    if(type == "persen"){   
         var persen = $('#diskon_persen').val();
    }else{
        var nominal = $('#diskon_nominal').val();
    }   
    swal({          
          text: "Apakah data yang dimasukan sudah benar?",
          type: 'warning',    
          showCancelButton: true, 
          confirmButtonColor: '#3085d6', 
          cancelButtonColor: '#d33', 
          cancelButtonText: 'Tidak',  
          confirmButtonText: 'Ya'   
        }).then(function(){       
          $.ajax({     
            url:ci_baseurl + "kasir/kasirrj/hitungdiskon",
            method : 'POST', 
            dataType : 'json',        
            data : {jenis:jenis ,persen:persen , nominal : nominal,tindakanpasien_id:tindakanpasien_id , reseptur_id:reseptur_id, action:action},
            success : function(data){
                var ret = data.status;  
                if(ret == "success"){      
                  $('#modal_diskon').modal('hide'); 
                  table_tindakan.columns.adjust().draw();
                  table_obat.columns.adjust().draw();    
                  swal(
                    'Berhasil' 
                    )
                }else{
                  swal(   
                    'Gagal'
                    )
                }
            }
          });  
        })    
    
 }   
 

function formatNumber(user_input){    
    var str = user_input.toString();
    var filtered_number = str.replace(/[^0-9]/gi,'');
    var length = filtered_number.length;
    var breakpoint = 1;
    var formated_number = '';

    for(i = 1; i <= length; i++){
        if(breakpoint > 3){
            breakpoint = 1;
            formated_number = ',' + formated_number;
        }
        var next_letter = i + 1;   
        formated_number = filtered_number.substring(length - i, length - (i - 1)) + formated_number; 

        breakpoint++;
    }  
    return formated_number;         
}  

function printKwitansi(pembayarankasir_id){             
    window.open(ci_baseurl+'kasir/kasirrj/print_kwitansi/'+pembayarankasir_id,'Print_Kwitansi','top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}       





function printkwitansi_asuransi(pasien_id,pendaftaran_id,detail_id,jenis_asuransi){   
  // var jenis_asuransi = $('#asuransi1').val();               
  var detailobat_id = $('#pembayaran_obat_id').val();  
  if(detailobat_id == '' || detailobat_id == null){   
    detailobat_id =null;             
  }      
  if(detail_id == '' || detail_id == null){
    detail_id = null;
  }
        
    console.log("pendaftaran_id : "+pendaftaran_id);                                  
    console.log("detailobat_id : "+detailobat_id);                                       
    console.log("jenis_asuransi : "+jenis_asuransi);                                 
    window.open(ci_baseurl+'kasir/kasirrj/print_kwitansi_2/'+pasien_id+'/'+pendaftaran_id+'/'+detail_id+'/'+detailobat_id+'/'+jenis_asuransi,'Print_Kwitansi','top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}   
   
function printDetail_Kwitansi(pembayarankasir_id){     
    window.open(ci_baseurl+'kasir/kasirrj/printNota/'+pembayarankasir_id,'Print_Kwitansi','top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no');
}  

     
function reloadTableTindakan(){  
  console.log('hadir reload');
  location.reload();       
}      

// disable mousewheel on a input number field when in focus
// (to prevent Cromium browsers change the value when scrolling)
$('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
      e.preventDefault()
    })
  })
  $('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
  })


 

  function showCaraBayar(jenis_poli){
    if(jenis_poli == "umum"){
        value = $('#cara_pembayaran_umum').val();
        console.log('value : '+value);
        div = $('.pembayaran1');
        div2 = $('.totalumum');        
        nama = $('#nama_asuransi_umum'); 
        nomor = $('#nomor_asuransi_umum');  
        lblNama = $('#lblNamaPembayaran_umum');      
        lblNomor = $('#lblNomorPembayaran_umum'); 
        hitungTotal();     
    }else if(jenis_poli == "rad"){
        value = $('#cara_pembayaran_rad').val();
        console.log('value : '+value);
        div = $('.pembayaran2');
        div2 = $('.totalrad');        
        nama = $('#nama_asuransi_rad');  
        nomor = $('#nomor_asuransi_rad');  
        lblNama = $('#lblNamaPembayaran_rad');  
        lblNomor = $('#lblNomorPembayaran_rad'); 
    }else if(jenis_poli == "lab"){
        value = $('#cara_pembayaran_lab').val();
        console.log('value : '+value);
        div = $('.pembayaran3');   
        div2 = $('.totallab');         
        nama = $('#nama_asuransi_lab'); 
        nomor = $('#nomor_asuransi_lab');  
        lblNama = $('#lblNamaPembayaran_lab');  
        lblNomor = $('#lblNomorPembayaran_lab'); 
    }else{
        value = $('#cara_pembayaran_obat').val();
        console.log('value : '+value);
        div = $('.pembayaran4');    
        div2 = $('.totalobat');            
        nama = $('#nama_asuransi_obat'); 
        nomor = $('#nomor_asuransi_obat');      
        lblNama = $('#lblNamaPembayaran_obat');  
        lblNomor = $('#lblNomorPembayaran_obat'); 

    } 
        
    id = $('#pembayaran_id').val();  
    console.log('id :'+id );   
  
    $.ajax({ 
        url : ci_baseurl + "kasir/kasirrj/getCaraPembayaran/"+id,
        type : 'GET',
        dataType : 'JSON',
        success : function(data){ 
            console.log('masuk success ajax');
  
            console.log(data);   

            if(value != "PRIBADI"){  
            console.log('masuk if bpjs'); 
            div.show(500);     
            // div2.show(500);
            if(value == "ASURANSI1"){
              nama.val(data.nama_asuransi);    
              nomor.val(data.no_asuransi);    
              lblNama.text('Nama Asuransi ');   
              lblNomor.text('Nomor Asuransi ');   
      
            } 
            if(value == "ASURANSI2"){        
              nama.val(data.nama_asuransi2);  
              nomor.val(data.no_asuransi2);  
        
              lblNama.text('Nama Asuransi 2');
              lblNomor.text('Nomor Asuransi 2');
            } 

          }else{      
            div.hide(500);         
            // div2.hide(500);
          }

        }
    });
    }