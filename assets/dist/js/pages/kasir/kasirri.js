var table_kasirri;
$(document).ready(function(){
    // $('#modal_bayar_kasirri').on('hidden.bs.modal', function(){
    //     $(this).find('iframe').html("");
    //     $(this).find('iframe').attr("src", "");
    // });

   table_kasirri = $('#table_list_kasirri').DataTable({
//       "sDom": '<"top"f>rt<"bottom"ip><"clear">',
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      // "scrollX": true, 
      // "bFilter": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "kasir/kasirri/ajax_list_kasirri",
          "type": "GET",
//          "data": function(d){
//                d.tgl_awal = $("#tgl_awal").val();
//                d.tgl_akhir = $("#tgl_akhir").val();
//            }
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]
    });
    table_kasirri.columns.adjust().draw();
});

function reloadTableKasir(){
    table_kasirri.columns.adjust().draw();
}

function bayarTagihan(pendaftaran_id){
    // $('#modal_bayar_kasirri').openModal('toggle');
    var src = ci_baseurl + "kasir/kasirri/bayar_tagihan/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_bayar_kasirri iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function generate(pendaftaran_id){
    $('#modal_generate').modal('show'); 
    var src = ci_baseurl + "kasir/kasirri/generate/"+pendaftaran_id;
  //    var src = ci_baseurl + "dashboard";
    $("#modal_generate iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
  }