var table_stok_farmasi;
var table_penyesuaian_barang;

$(document).ready(function(){
    $('#new_batch').show();
    $('#old_batch').hide();

    table_stok_farmasi = $('#table_stok_farmasi_list').DataTable({
        // "processing": true, //Feature control the processing indicator.
        // "serverSide": true,
        // "searching" :false, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

        "createdRow": function( row, data, dataIndex ) {
            if (data[7] === "Lebih" || data[7] === "Kurang" || data[7] === "Habis") {
                $('td', row).css('background-color', '#ffcdd2');
            }
        },

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "logistik/stokfarmasi/ajax_list",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [ -1,0 ], //last column
                "orderable": false //set not orderable
            }
        ]

    });

    table_penyesuaian_barang = $('#table_penyesuaian_list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching" :false, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "logistik/stokfarmasi/ajax_stok_list",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [ -1,0 ], //last column
                "orderable": false //set not orderable
            }
        ]

    });

    table_penyesuaian_barang.columns.adjust().draw();
    table_stok_farmasi.columns.adjust().draw();

    $('.tooltipped').tooltip({delay: 50});

    $('button#saveFarmasiUtama').click(function(){
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "logistik/farmasiutama/do_create",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateFarmasiUtama').serialize(),

                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $('#modal_add_masuk_barang').modal('hide');
                        $('#fmCreateFarmasiUtama').find("input[type=text]").val("");
                        $('#fmCreateFarmasiUtama').find("input[type=number]").val("");
                        $('#fmCreateFarmasiUtama').find("select").prop('selectedIndex',0);
                        $("#kode_supplier").select2("val", "");
                        table_stok_farmasi.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                        swal(
                            'Gagal!',
                            'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                            'error'
                        )
                    }
                }
            });

        })
    });

    $('button#penyesuaianGudangFarmasi').click(function(){
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "logistik/stokfarmasi/do_penyesuaian",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmPenyesuaianGudangFarmasi').serialize(),

                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $('#modal_penyesuaian').modal('hide');
                        table_penyesuaian_barang.columns.adjust().draw();

                        swal(
                            'Berhasil!',
                            'Penyesuaian Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                        swal(
                            'Gagal!',
                            'Penyesuaian Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                            'error'
                        )
                    }
                }
            });
        })
    });
});

function hapusFarmasiUtama(id_stok_farmasi){
    var jsonVariable = {};
    jsonVariable["id_stok_farmasi"] = id_stok_farmasi;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
        text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        console.log("masuk delete");
        $.ajax({

            url: ci_baseurl + "farmasi/stok_farmasi/do_delete_stok_farmasi",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function(data) {
                var ret = data.success;
                $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                if(ret === true) {
                    $('.notif').removeClass('red').addClass('green');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    table_stok_farmasi.columns.adjust().draw();
                } else {
                    $('.notif').removeClass('green').addClass('red');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    console.log("gagal");
                }
            }
        });
        swal(
            'Berhasil!',
            'Data Berhasil Dihapus.',
            'success',
        )
    });
}

function penyesuaian(id){
    $.ajax({
        url: ci_baseurl + "logistik/stokfarmasi/ajax_stok",
        type: 'get',
        dataType: 'json',
        data: { id:id },
        success: function(data) {
            var ret = data.success;
            if (ret == true){
                console.log(data.data);
                var obat = data.data.obat;
                var persediaan = data.data.persediaan;
                var data = data.data.data;

                $('#tf_id_farmasi').val(data.gudang_farmasi_id);
                $('#tf_id_stok').val(data.id);

                var stok_1 = Math.trunc(data.stok_akhir / (obat.jumlah_persediaan_2 * obat.jumlah_persediaan_3));
                var stok_2 = Math.trunc(data.stok_akhir % (obat.jumlah_persediaan_2 * obat.jumlah_persediaan_3) / obat.jumlah_persediaan_3);
                var stok_3 = data.stok_akhir % obat.jumlah_persediaan_3;

                $('#stok_jml_brg_persediaan_1').val(stok_1);
                $('#stok_jml_brg_persediaan_2').val(stok_2);
                $('#stok_jml_brg_persediaan_3').val(stok_3);

                $('#stok_persediaan_1').val(persediaan.persediaan_1);
                $('#stok_persediaan_2').val(persediaan.persediaan_2);
                $('#stok_persediaan_3').val(persediaan.persediaan_3);

                $('#penyesuaian_1').attr("placeholder", "Jumlah " + persediaan.persediaan_1);
                $('#penyesuaian_2').attr("placeholder", "Jumlah " + persediaan.persediaan_2);
                $('#penyesuaian_3').attr("placeholder", "Jumlah " + persediaan.persediaan_3);

                $('#tf_persediaan_1').val(persediaan.persediaan_1);
                $('#tf_persediaan_2').val(persediaan.persediaan_2);
                $('#tf_persediaan_3').val(persediaan.persediaan_3);
            }
        }
    });
}

function dialogBarang(){
    if(table_daftar_barang){
        table_daftar_barang.destroy();
    }

    table_daftar_barang = $('#table_daftar_barang_list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching" :true,
        "iDisplayLength" : 5,
        "aLengthMenu": [[5, 10, 20, 50, -1], [5, 10, 20, 50, "All"]],//Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "logistik/farmasiutama/ajax_list_daftar_barang",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [ -1, 0 ], //last column
                "orderable": false //set not orderable
            }
        ]
    });

    table_daftar_barang.columns.adjust().draw();
}


function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function pilihBarang(id){
    if (id != ""){
        $.ajax({
            url: ci_baseurl + "farmasi/stok_farmasi/ajax_get_daftar_barang_by_id",
            type: 'get',
            dataType: 'json',
            data: { id:id },
            success: function(data) {
                var ret = data.success;
                if (ret == true){
                    $('#kode_barang').val(data.data['kode_barang']);
                    $('#nama_barang').val(data.data['nama_barang']);
                    $('#jenis_barang').val(data.data['id_jenis_barang']);
                    $('#kode_sediaan').val(data.data['id_sediaan']);
                    $('#harga_barang_keseluruhan').val(data.data['harga_barang_keseluruhan']);
                    $("#modal_list_barang").modal('hide');
                } else {

                }
            }
        });
    } else {
        // $('#modal_list_obat').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function pilih(){
    var jenis_transaksi = $("#jenis_transaksi").val();
    if (jenis_transaksi == 1){
        $(".per_pack").show(500);
        $(".per_pieces").hide(500);
    }else {
        $(".per_pieces").show(500);
        $(".per_pack").hide(500);
    }
}

function typeBatch(){
    var type = $("#type_batch_number").val();

    if (type == 1){
        $('#new_batch').show();
        $('#old_batch').hide();
    }else {
        $('#new_batch').hide();
        $('#old_batch').show();
    }
}

function setSuplier(e) {
    var id_batch = $('#batch_id').val();
    var id_obat = $('#obat_id').val();
    var id_suplier = e.value;

    $.ajax({
        url: ci_baseurl + "/logistik/farmasiutama/ajax_detail_harga",
        type: 'get',
        dataType: 'json',
        data: {suplier_id:id_suplier, obat_id:id_obat, batch_id:id_batch},
        success: function(data) {
            var ret = data.success;
            if(ret === true) {
                console.log(data.data);
                var harga_beli = data.data.data;

                $('#harga_persediaan_1').val(harga_beli.harga_persediaan_1);
                $('#harga_persediaan_2').val(harga_beli.harga_persediaan_2);
                $('#harga_persediaan_3').val(harga_beli.harga_persediaan_3);
            } else {
                $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                $('#card_message').html(data.data);
                $('#modal_notif').show();
                $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                    $("#modal_notif").hide();
                });
            }
        }
    });
}

function setBatch(e) {
    $.ajax({
        url: ci_baseurl + "/logistik/farmasiutama/ajax_detail_batch",
        type: 'get',
        dataType: 'json',
        data: {id:e.value},
        success: function(data) {
            var ret = data.success;
            if(ret === true) {
                console.log(data.data);

                var batch = data.data.data;
                var persediaan = data.data.persediaan;
                var obat = data.data.obat;
                var jenis_barang = data.data.jenis_barang;

                $('#nama_barang').val(obat.nama);
                $('#obat_id').val(obat.id);

                $('#jenis_barang').val(jenis_barang.nama);
                $('#jenis_barang_id').val(jenis_barang.id);

                $('#suplier_id').val("").change();

                $('#harga_persediaan_1').val(0);
                $('#harga_persediaan_2').val(0);
                $('#harga_persediaan_3').val(0);

                $('#jml_brg_persediaan_1').attr("placeholder", "Jumlah " + persediaan.persediaan_1);
                $('#jml_brg_persediaan_2').attr("placeholder", "Jumlah " + persediaan.persediaan_2);
                $('#jml_brg_persediaan_3').attr("placeholder", "Jumlah " + persediaan.persediaan_3);

                $('#jml_brg_persediaan_1').val(0);
                $('#jml_brg_persediaan_2').val(0);
                $('#jml_brg_persediaan_3').val(0);

                $('#persediaan_1').val(persediaan.persediaan_1);
                $('#persediaan_2').val(persediaan.persediaan_2);
                $('#persediaan_3').val(persediaan.persediaan_3);

                $('#harga_persediaan_1').attr("placeholder", "Harga per 1 " + persediaan.persediaan_1);
                $('#harga_persediaan_2').attr("placeholder", "Harga per 1 " + persediaan.persediaan_2);
                $('#harga_persediaan_3').attr("placeholder", "Harga per 1 " + persediaan.persediaan_3);
            } else {
                $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                $('#notification_messages').html(data.messages);
                $('#notification_type').show();
            }
        }
    });
}

function sumTotal() {
    var total = ($('#jml_brg_persediaan_1').val() * $('#harga_persediaan_1').val()) + ($('#jml_brg_persediaan_2').val() * $('#harga_persediaan_2').val()) + ($('#jml_brg_persediaan_3').val() * $('#harga_persediaan_3').val());
    $('#total').val(total);
}

function perhitunganPack(){
    var jumlah_barang_per_pack = $("#jumlah_barang_per_pack").val();
    var jumlah_pack = $("#jumlah_pack").val();
    var jumlah_barang_keseluruhan = parseInt(jumlah_barang_per_pack * jumlah_pack);
    document.getElementById("jumlah_barang_keseluruhan").value = jumlah_barang_keseluruhan;
    $('#jumlah_barang_keseluruhan').attr('value', jumlah_barang_keseluruhan);
}

function perhitunganPieces(){
    var jumlah_barang_per_pieces = $("#jumlah_barang_per_pieces").val();
    var jumlah_barang_keseluruhan = jumlah_barang_per_pieces;
    document.getElementById("jumlah_barang_keseluruhan").value = jumlah_barang_keseluruhan;
    $('#jumlah_barang_keseluruhan').attr('value', jumlah_barang_keseluruhan);
}


function perhitunganHarga(angka){
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var jumlah_barang_keseluruhan = $("#jumlah_barang_keseluruhan").val();
    var harga_barang_per_item = $("#harga_barang_per_item").val();
    // console.log("ini " + harga_barang_per_item);
    var harga_item = Number(harga_barang_per_item.replace(/\.*([^0-9\\.])/g, ""));
    // console.log("ini " + harga_item);
    var harga_barang_keseluruhan = parseInt(jumlah_barang_keseluruhan * harga_item);
    document.getElementById("harga_barang_keseluruhan").value = harga_barang_keseluruhan;
    $('#harga_barang_keseluruhan').attr('value', harga_barang_keseluruhan);
    $('#harga_barang_per_itemaa').attr('value', harga_item);

}

function perhitunganHargaa(angka){
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var jumlah_barang_keseluruhan = $("#jumlah_barang_keseluruhan").val();
    var harga_barang_per_itema = $("#harga_barang_per_itema").val();
    // console.log("ini " + harga_barang_per_itema);
    var harga_itema = Number(harga_barang_per_itema.replace(/\.*([^0-9\\.])/g, ""));
    // console.log("ini " + harga_itema);
    var harga_barang_keseluruhan = parseInt(jumlah_barang_keseluruhan * harga_itema);
    document.getElementById("harga_barang_keseluruhan").value = harga_barang_keseluruhan;
    $('#harga_barang_keseluruhan').attr('value', harga_barang_keseluruhan);
    $('#harga_barang_per_itemaa').attr('value', harga_itema);

}


function cariFarmasiUtama(){
    table_stok_farmasi = $('#table_stok_farmasi_list').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "farmasi/stok_farmasi/ajax_list_stok_farmasi",
            "type": "GET",
            "data": function(d){
                d.tgl_awal        = $("#tgl_awal").val();
                d.tgl_akhir       = $("#tgl_akhir").val();
                d.kode_barang     = $("#kode_barang").val();
                d.nama_barang     = $("#nama_barang").val();
                d.jenis_barang    = $("#jenis_barang").val();
                d.nama_supplier   = $("#nama_supplier").val();
            },
            "success" : function(data){
                $("button#exportToExcel").fadeIn(100);
            },
        },
        //Set column definition initialisation properties.
    });
    table_stok_farmasi.columns.adjust().draw();
}


$(document).ready(function(){
    $('#change_option').on('change', function(e){
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "logistik/stokfarmasi/exporttoexcel/";
}