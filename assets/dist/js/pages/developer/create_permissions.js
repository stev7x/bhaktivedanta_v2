$(function () {
	//alert(ci_baseurl);
	
	$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
	{
	    return {
	        "iStart": oSettings._iDisplayStart,
	        "iEnd": oSettings.fnDisplayEnd(),
	        "iLength": oSettings._iDisplayLength,
	        "iTotal": oSettings.fnRecordsTotal(),
	        "iFilteredTotal": oSettings.fnRecordsDisplay(),
	        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
	        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
	    };
	};

	// Setup - add a text input to each footer cell
    /*$('#list_role tfoot th').each( function () {
        var title = $(this).text();
        if(title != '') {        	
        	$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    } );*/

	var table = $("#list_permissions").DataTable({
	    /*"dom": 'Bfrtip',*/
	    "processing": true,
	    "serverSide": true,
	    "searchDelay": 700,
	    "ajax": ci_baseurl + "developer/ajax_list_permissions",
	    "columns": [
	        {
	        	"data": "ID",
	            "searchable": false,
	            "sortable": false
	        },
	        /*{
	        	"data": "ROLE_ID",
	            "searchable": false,
	            "sortable": false
	        },*/
	        {"data": "NAME"},
	        {"data": "DEFINITION"},
	        {
	        	"data": "ROLE_NAME",
	        	"searchable": false,
	            "sortable": false
	        },
	        {
	            "class": "text-center",
	            "data": "aksi",
	            "searchable": false
	        }
	    ],
	    "order": [[1, 'asc']],
	    "rowCallback": function (row, data, iDisplayIndex) {
	        var info = this.fnPagingInfo();
	        var page = info.iPage;
	        var length = info.iLength;
	        var index = page * length + (iDisplayIndex + 1);
	        $('td:eq(0)', row).html(index);
	    }
	});	

	// Apply the search
/*    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
        	//alert(that.search);
        	//alert(this.value);
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    });*/

    $('button#btnSavePermissions').click( function() {
	    $.ajax({
	        url: ci_baseurl + "developer/do_create_permissions",
	        type: 'post',
	        dataType: 'json',
	        data: $('form#fmCreatePermissions').serialize(),
	        success: function(data) {
	        	var ret = data.success;
	        	$('input[name='+data.csrfTokenName+']').val(data.csrfHash);
	        	if(ret == true) {
	        		$('#alert_type').removeClass('alert alert-dismissable').addClass('alert alert-success alert-dismissable');
	        		$('#alert_messages').html(data.messages);
	        		$("#alert_type").show();
	                $("#alert_type").fadeTo(2000, 500).slideUp(500, function(){
	               		$("#alert_type").hide();
	                });
	        		$('#fmCreatePermissions').find("input[type=text]").val("");
            		table.draw(); 
	        	} else {
	        		$('#alert_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
	        		$('#alert_messages').html(data.messages);
	        		$("#alert_type").show();
	                $("#alert_type").fadeTo(2000, 500).slideUp(500, function(){
	               		$("#alert_type").hide();
	                });
	        	}
            }
	    });
	});

	$('button#reloadPermissions').click( function(){
        table.draw(); 
	});

});