var table_pasienrj;
$(document).ready(function(){
    $('#modal_periksa_pasienrj').on('hidden.bs.modal', function(){
        $(this).find('iframe').html("");
        $(this).find('iframe').attr("src", "");
    });

   table_pasienrj = $('#table_list_pasienrj').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "paging": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
         //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_pasienrj",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.dokter_id = $("#dokter_pj").val();
            }
      },
      //Set column definition initialisation properties.

    });
    table_pasienrj.columns.adjust().draw();

    $('button#saveToPenunjang').click(function(){

        swal({
          text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
             $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/kirim_ke_penunjang",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmKirimKePenunjang').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('#modal_kirim_penunjang').modal('hide');
                            // $('#modal_notif3').removeClass('alert-danger').addClass('alert-success');
                            // $('#card_message3').html(data.messages);
                            // $('#modal_notif3').show();
                            // $("#modal_notif3").fadeTo(4000, 500).slideUp(1000, function(){
                            //     $("#modal_notif3").hide();
                            // });
                            $('#fmKirimKePenunjang').find("input[type=text]").val("");
                            $('#fmKirimKePenunjang').find("select").prop('selectedIndex',"");
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success',
                              )
                        table_pasienrj.columns.adjust().draw();
                        } else {
                            $('#modal_notif3').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message3').html(data.messages);
                            $('#modal_notif3').show();
                            $("#modal_notif3").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif3").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        }
                    }
                });

        });

    });

    var year = new Date().getFullYear();
});

function reloadTablePasien(){
    table_pasienrj.columns.adjust().draw();
    location.reload();
}

function cariPasien(){
    table_pasienrj = $('#table_list_pasienrj').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "paging": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_pasienrj",
          "type": "GET",
          "data": function(d){
                d.tgl_awal          = $("#tgl_awal").val();
                d.tgl_akhir         = $("#tgl_akhir").val();
                d.dokter_poli       = $("#dokter_poli").val();
                d.poliruangan       = $("#poliruangan").val();
                d.poliklinik        = $("#poliklinik").val();
                d.status            = $("#status").val();
                d.pasien_nama       = $("#pasien_nama").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.no_rekam_medis    = $("#no_rekam_medis").val();
                d.no_bpjs           = $("#no_bpjs").val();
                d.pasien_alamat     = $("#pasien_alamat").val();
                d.no_pendaftaran    = $("#no_pendaftaran").val();
                d.urutan            = $("#urutan").val();

            }
      },
      //Set column definition initialisation properties.
    });
    table_pasienrj.columns.adjust().draw();
}

function periksaPasienRj(pendaftaran_id){
    var src = ci_baseurl + "rawatjalan/pasien_rj/periksa_pasienrj/"+pendaftaran_id;
    $("#modal_periksa_pasienrj iframe").attr({
        'src': src,
        'height': 880,
        'width': '100%',
        'allowfullscreen':''
    });
}

function resepturPasienRj(pendaftaran_id){
    var src = ci_baseurl + "rawatjalan/pasien_rj/reseptur_pasienrj/"+pendaftaran_id;
    $("#modal_reseptur iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function pulangkanPasien(pendaftaran_id){
    var src = ci_baseurl + "rawatjalan/pasien_rj/pulangkan_pasien/"+pendaftaran_id;
    $("#modal_pulangkan iframe").attr({
        'src': src,
        'height': 880,
        'width': '100%',
        'allowfullscreen':''
    });
}

function batalPeriksa(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
    if(pendaftaran_id){


        swal({
          text: "Apakah anda yakin ingin membatalkan pemeriksaan?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){

            $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_batal_periksa",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            swal(
                                'Berhasil!',
                                'Pemeriksaan Berhasil Dibatalkan.',
                                'success',
                              )
                            table_pasienrj.columns.adjust().draw();
                            $("htmml,body").animate({ scrollTop: 50 }, "fast");

                        } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            swal(
                                'Gagal!',
                                'Pemeriksaan Gagal Dibatalkan.',
                                'error',
                              )
                        }
                    }
                });


        });

    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }

}

function kirimKePenunjang(pendaftaran_id){
    $('#pendaftaran_id').val(pendaftaran_id);
}


function kirimKeIGD(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
    if(pendaftaran_id){
        swal({
            text: "Apakah anda yakin ingin memindahkan ke IGD ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            $.ajax({
                url: ci_baseurl + "rawatjalan/pasien_rj/kirim_ke_igd",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        $('.notif').removeClass('red').addClass('green');
                        $('#card_message').html(data.messages);
                        $('.notif').show();
                        $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".notif").hide();
                        });
                    table_pasienrj.columns.adjust().draw();
                        $("htmml,body").animate({ scrollTop: 50 }, "fast");
                        swal(
                            'Berhasil!',
                            'Pasien Berhasil Dipindahkan.',
                            'success'
                          )
                    } else {
                        $('.notif').removeClass('green').addClass('red');
                        $('#card_message').html(data.messages);
                        $('.notif').show();
                        $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".notif").hide();
                        });
                        swal(
                            'Gagal!',
                            'Pasien Gagal Dipindahkan.',
                            'error'
                          )
                    }
                }
            });

          })
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }

}

function form_print(pasien_id){
    $('#modal_print').modal('show');
    // $('button#printSurat').attr('onclick','printKartuBerobat('')');
    $('button#printKartu').attr('onclick','printData('+pasien_id+',"print_kartu")');
    $('button#printDetail').attr('onclick','printData('+pasien_id+',"print_data_pasien")');
    $('button#printSurat').attr('onclick','printData('+pasien_id+',"form_input_surat")');
    $('button#printKartuHilang').attr('onclick','printData('+pasien_id+',"print_hilang_kartu")');
    $('button#printSticker').attr('onclick','printData('+pasien_id+',"print_sticker")');
    $('button#printGelang').attr('onclick','printData('+pasien_id+',"print_gelang")');
    $('button#printCasemix').attr('onclick','printData('+pasien_id+',"print_casemix")');

}


function form_reschedule(pendaftaran_id,dokter_id,reservasi_id){
    $('#modal_schedule').modal('show');
    $('#id_dokter').val(dokter_id);
    $('#reservasi_id').val(reservasi_id);
    $('#res_pendaftaran_id').val(pendaftaran_id);

    $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/get_data_pasien/"+pendaftaran_id,
        type: 'GET',
        dataType: 'JSON',
        success: function(result){

            if (result.success == true) {

                $('#nama_pasien').text(result.data.pasien_nama);
                $('#ttl').text(result.data.tempat_lahir +", " + result.tgl_lahir);
                $('#dokter_pj').text(result.data.NAME_DOKTER);
                $('#no_rm').text(result.data.no_rekam_medis);
                $('#poli').text(result.data.nama_poliruangan);
                $('#jam_per').text(result.data.jam_periksa);

            }else{
            }
        }
    });

}


function printData(pasien_id,view){
    window.open(ci_baseurl+'rawatjalan/pasien_rj/print_data/'+pasien_id+'/'+view,'Print_Surat','top=100,left=320,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')

}

    $(document).ready(function(){
        $('#change_option').on('change', function(e){
            var valueSelected = this.value;
            $('.pilih').attr('id',valueSelected);
            $('.pilih').attr('name',valueSelected);
        });
    });


function getDokter() {
    var poliruangan_id = $("#poliruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/get_dokter_poli",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function (data) {
            $("#dokter_poli").html(data.list);

        }
    });
}

function getJamDokter(){
    var dokter_id = $("#id_dokter").val();
    var tgl_reservasi = $("#tgl_reservasi").val();
    // var no_urut = $("#next_no_urut").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/reservasi/get_jam_dokter",
        type: 'GET',
        dataType: 'JSON',
        data: {dokter_id:dokter_id,tgl_reservasi:tgl_reservasi},
        success: function(data) {
            $("#jam").removeAttr("disabled");
            $("#jam").html(data.list);
            // $("#jumlah_pasien").text(no_urut + " / "+ data.jumlah_pasien);
        }
    });
}

function saveReschedule(){

    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
    }).then(function () {
        $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/save_reschedule",
        type: 'POST',
        dataType: 'JSON',
        data: $('form#fmCreateReschedule').serialize(),
        success: function(data) {
            console.log(data);
            var ret = data.success;
            var display = data.display;
            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);

            if(ret === true){
                $('#modal_schedule').modal('hide');

                $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                $('#card_message').html(data.messages);
                $('#modal_notif').show();
                $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                    $("#modal_notif").hide();
                });

                $("html, body").animate({
                    scrollTop: 100
                }, "fast");
                swal(
                    'Berhasil !',
                    'Reschedule pasien berhasil',
                    'success'
                );

                table_pasienrj.columns.adjust().draw();
            }else{
                $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                $('#card_message').html(data.messages);
                $('#modal_notif').show();
                $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                    $("#modal_notif").hide();
                });
                $("html, body").animate({ scrollTop: 100 }, "fast");
            }
        }
        });
    });
}

function riwayatPenyakitPasien(pasien_id) {
    var src = ci_baseurl + "rekam_medis/list_pasien/riwayatPenyakitPasien/" + pasien_id;
    console.log(pasien_id);
    $("#modal_riwayat_penyakit_pasien iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen': ''
    });
}