var paketOperasiArr = [];
var theraphyArr     = [];

var table_diagnosa_rd;
var table_tindakan_rd;
var table_background_rd;
var table_resep_rd;
var table_obat_rd;
var table_rs_rujukan;
var table_assestment_rd_kk;
var table_rencanatindakan_rd_kk;

$(document).ready(function(){
    // get_data_admisi_kk();

    table_diagnosa_rd = $('#table_diagnosa_pasienrd_kk').DataTable({ 
        "processing"  : true,
        "serverSide"  : true,
        "paging"      : false,
        "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
        "ordering"    : false,
        "ajax"        : {
            "url"     : ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa_pasien_rawatinap",
            "type"    : "GET",
            "data"    : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        },
    });
    table_diagnosa_rd.columns.adjust().draw();

    table_tindakan_rd = $('#table_tindakan_pasienrd_kk').DataTable({ 
        "processing"  : true,
        "serverSide"  : true,
        "paging"      : false,
        "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
        "ordering"    : false,
        "ajax"        : {
            "url"     : ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_tindakan_pasien_rawatinap",
            "type"    : "GET",
            "data"    : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_tindakan_rd.columns.adjust().draw();


    table_background_rd = $('#table_background_pasienrd_kk').DataTable({ 
        "processing"  : true,
        "serverSide"  : true,
        "paging"      : false,
        "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
        "ordering"    : false,
        "ajax"        : {
            "url"     : ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_background_kkinap",
            "type"    : "GET",
            "data"    : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_background_rd.columns.adjust().draw();

    table_rencanatindakan_rd_kk = $('#table_rencanatindakan_kk').DataTable({ 
        "processing"  : true,
        "serverSide"  : true,
        "paging"      : false,
        "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
        "ordering"    : false,
        "ajax"        : {
            "url"     : ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_rencanatindakan_kkinap",
            "type"    : "GET",
            "data"    : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_rencanatindakan_rd_kk.columns.adjust().draw();

    get_table_assesment()
    getKelasPoliruangan_kk()
});

function getKelasPoliruangan_kk(){
  var poliruangan_id = $("#poli_ruangan").val();
  $.ajax({
    url         : ci_baseurl + "rawatdarurat/pasien_rd/get_kelasruangan_list",
    type        : 'GET',
    dataType    : 'JSON',
    data        : {poliruangan_id:poliruangan_id},
    success: function(data) {
        $("#kelas_pelayanan").html(data.list);
        console.log("MASUK SINI")
    }
  });
  console.log("MASUK SINI AAAA")
}

function getKamar_kk(){
  var poliruangan_id = $("#poli_ruangan").val();
  var kelaspelayanan_id = $("#kelas_pelayanan").val();
  $.ajax({
    url         : ci_baseurl + "rawatdarurat/pasien_rd/get_kamarruangan",
    type        : 'GET',
    dataType    : 'JSON',
    data        : {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
    success: function(data) {
        $("#kamarruangan").html(data.list);
    }
  });
}

function get_data_admisi_kk() {
  var pendaftaran_id = $('#pendaftaran_id').val();
  $.ajax({
    url     : ci_baseurl + "rawatdarurat/pasien_rd/get_data_admisi",
    type    : 'POST',
    dataType: 'JSON',
    data    : {pendaftaran_id:pendaftaran_id},
    success: function(data) {
      var ret = data.success;
      if(ret == true) {
        $('#poli_ruangan').val(data.data.poli_ruangan_id);
        $('#kelas_pelayanan').val(data.data.kelaspelayanan_id);
        $('#kamarruangan').val(data.data.kamarruangan_id);

        $('#poli_ruangan').css("pointer-events","none").attr('readonly',true);
        $('#kelas_pelayanan').css("pointer-events","none").attr('readonly',true);
        $('#kamarruangan').css("pointer-events","none").attr('readonly',true);    
      }else{
        swal("Data Admisi Tidak Ditemukan");
      }
    }
  });
}

function get_kelas_nofilter_kk(){
  $.ajax({
    url     : ci_baseurl + "rawatdarurat/pasien_rd/get_kelasruangan_list_nofilter",
    type    : 'GET',
    dataType: 'JSON',
    success: function(data) {
        $("#kelas_pelayanan").html(data.list);
    }
  });
}

function get_kamar_nofilter_kk(){
  $.ajax({
    url     : ci_baseurl + "rawatdarurat/pasien_rd/get_kamarruangan_nofilter",
    type    : 'GET',
    dataType: 'JSON',
    success : function(data) {
        $("#kamarruangan").html(data.list);
    }
  });
}

function set_therapy() {
    id_terapi   = $('#nama_program').val();
    terapi      = $('#nama_program option:selected').text();

    $('#therapy_id').val(id_terapi);
    $('#nama_therapy').val(terapi);
}

function add_Therapy() {
    pasien_id       = $("#pasien_id").val();
    therapy_id      = $("#therapy_id").val();

    swal({
        text                : "Apakah data yang dimasukan sudah benar ?",
        type                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#3085d6',
        cancelButtonColor   : '#d33',
        cancelButtonText    : 'tidak',
        confirmButtonText   : 'Ya' 
    }).then(function(){  
        $.ajax({
            url         : ci_baseurl + "rawatdarurat/pasien_rd/ajax_save_therapy",
            type        : 'POST',
            dataType    : 'JSON',
            data        : {pasien_id:pasien_id, therapy_id:therapy_id},
            success: function(data) {
                var ret = data.success;
                if(ret == true) {
                    swal('Berhasil!','Data Berhasil Disimpan.','success');
                    table_assestment_rd_kk.ajax.reload(null,false);
                } else {
                    $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message2').html(data.messages);
                    $('#modal_notif2').show();
                    $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif2").hide();
                    });
                    $("html, body").animate({ scrollTop: 100 }, "fast");
                }
            }
        });
    });
}

function hapus_therapy(assestment_therapy_id) {
    swal({
        text                : assestment_therapy_id,//"Yakin Data Akan Dihapus ?",
        type                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#3085d6',
        cancelButtonColor   : '#d33',
        cancelButtonText    : 'tidak',
        confirmButtonText   : 'Ya' 
    }).then(function(){  
        $.ajax({
            url     : ci_baseurl + "rawatdarurat/pasien_rd/ajax_delete_therapy",
            type    : "POST",
            dataType: "JSON",
            data    : {assestment_therapy_id:assestment_therapy_id},
            success : function(data){
                swal('Berhasil!','Data Berhasil Dihapus.','success');
                table_assestment_rd_kk.ajax.reload(null,false);
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal("Gagal", "Data Gagal Dihapus", "error");
            }
        });
    });
}   

var tbody_row = '';
var tbody_row_no = 0;
var terapi_array = [];
    $('button#addTherapy').click(function(){
        // read existing table value
        program_therapy_id = $("#nama_program option:selected").val();
        if(program_therapy_id == '#' || program_therapy_id == null){
          swal('Pilih Therapy terlebih dahulu');
        }else{
          console.log('jos');
          ++tbody_row_no
          program_therapy = $("#nama_program option:selected").html();
          program_therapy_id = $("#nama_program option:selected").val();
          terapi_array.push(program_therapy_id);
          // program_therapy_id = $("#nama_program option:selected").val();
          if($('#table_asessment_therapy').find("td").html() == "No data to display"){
            tbody_row = ""
          }else{
            tbody_row = $('#table_asessment_therapy tbody').html()
          }
          tbody_row += "<tr id='terapi_id_"+tbody_row_no+"'><td>"+program_therapy+"</td>";
          tbody_row += '<td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusTherapy('+tbody_row_no+')"><i class="fa fa-times"></i></button></td></tr>';

          // write to existing
          $("#table_asessment_therapy tbody").html(tbody_row);
        }
    });

function simpan_assesment() {swal({
    text: "Apakah data yang dimasukan sudah benar ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',
    confirmButtonText: 'Ya'
  }).then(function(){
    var assessment = {
        "instalasi_id" : 2,
        "pendaftaran_id" : $("#pendaftaran_id").val(),
        "pasien_id" : $("#pasien_id").val(),
        "tensi" : $("#tensi").val(),
        "nadi_1" : $("#nadi_1").val(),
        "suhu" : $("#suhu").val(),
        "nadi_2" : $("#nadi_2").val(),
        "penggunaan" : $("#penggunaan").val(),
        "saturasi" : $("#saturasi").val(),
        "nyeri" : $("#nyeri").val(),
        "numeric_wong_baker" : $("#numeric_wong_baker").val(),
        "resiko_jatuh" : $("#resiko_jatuh").val(),
        "program_therapy" : [1,2,3,4]
      };
      assessment = "pendaftaran_id=" + $("#pendaftaran_id").val() + "&" +
                    "tensiok=" + $("#tensi").val() + "&" +
                    "suhuok=" + $("#suhu").val() + "&" +
                    "nadiok=" + $("#nadi_1").val() + "&" +
                    "nadi_1ok=" + $("#nadi_2").val() + "&" +
                    "penggunaanok=" + $("#penggunaan").val() + "&" +
                    "saturasiok=" + $("#saturasi").val() + "&" +
                    "nama_nyeriok=" + $("#nyeri").val() + "&" +
                    "numericok=" + $("#numeric_wong_baker").val() + "&" +
                    "resikook=" + $("#resiko_jatuh").val();
        $.ajax({
            url: ci_baseurl + "rawatjalan/pasien_rj/do_create_assestment_igd",
            type: 'POST',
            dataType: 'JSON',
            data: assessment + "&theraphyArr=" + JSON.stringify(terapi_array),
            // data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +diagnosa.pendaftaran_id+ "&pasien_id=" +diagnosa.pasien_id+ "&dokter_id=" +diagnosa.dokter_id+ "&dokter_id_2=" +diagnosa.dokter_id_2+ "&dokter_id_3=" +diagnosa.dokter_id_3,
            success: function(data) {
                var ret = data.success;
                $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                if(ret === true) {
                    swal(
                           'Berhasil!',
                           'Data Berhasil Disimpan.',
                           'success'
                        )
                        get_table_assesment();
                } else {
                    swal(
                        'Gagal!',
                        'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        'error'
                      )
                }
            }
        });

  });
}

function saveIGD() {
    swal({
        text                : "Apakah data yang dimasukan sudah benar ?",
        type                : 'warning',
        showCancelButton    : true,
        confirmButtonColor  : '#3085d6',
        cancelButtonColor   : '#d33',
        cancelButtonText    : 'tidak',
        confirmButtonText   : 'Ya' 
    }).then(function(){  
        $.ajax({
            url     : ci_baseurl + "rawatjalan/pasien_rj/kirim_ke_igd",
            type    : "POST",
            dataType: "JSON",
            data    : $('#formkirim').serialize(),
            success : function(data){
                var ret = data.success;
                if (ret == true) {
                    swal('Berhasil!','Data Berhasil Disimpan.','success');
                }else{
                    $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message2').html(data.messages);
                    $('#modal_notif2').show();
                    $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif2").hide();
                    });
                    $("html, body").animate({ scrollTop: 100 }, "fast");
                }
            }
        });
    });
}
  
function get_table_assesment() {
  pendaftaran_id = $('#pendaftaran_id').val();
  table_assesment = $('#table_assesment').DataTable({ 
      "processing"  : true,
      "serverSide"  : true,
      "paging"      : false,
      "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
      "ordering"    : false,
      "ajax"        : {
          "url"     : ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_assesment",
          "type"    : "GET",
          "data"    : function(d){
          d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
  });
  table_assesment.columns.adjust().draw();
  table_assesment.destroy();
}

function dialogTindakan() {
  table_list_tindakan = $('#table_list_tindakan').DataTable({ 
    "destroy"       : true,
    "processing"    : true,
    "serverSide"    : true,
    "pageLength"    : 5,
    "ordering"      : false,
    "searching"     : true,
    "info"          : false,
    "ajax": {
        "url"   : ci_baseurl + "rawatjalan/pasien_rj/get_list_tindakan_igd",
        "type"  : "POST",
    },
});

  var table_list_tindakan = $('#table_list_tindakan').DataTable();
  table_list_tindakan.columns().every( function () {
      var that = this;
      $( 'cari', this.footer() ).on( 'keyup change', function () {
          if (that.search() !== this.value) {
              that.search(this.value ).draw();
          }
      } );
  } );

  table_list_tindakan.columns.adjust().draw();
}

function pilihTindakan(tindakan_id) {
    if (tindakan_id != "") {
        $.ajax({
            url     : ci_baseurl + "rawatdarurat/pasien_rd/get_tindakan_by_id",
            type    : 'get',
            dataType: 'json',
            data    : { tindakan_id: tindakan_id },
            success: function (data) {
                var ret = data.success;
                if (ret == true) {
                    $('#kode_tindakan').val(data.data['daftartindakan_id']);
                    $('#nama_tindakan').val(data.data['daftartindakan_nama']);
                    $("#modal_tindakan").modal('hide');
                } else {
                    swal({
                        title   : "Gagal !",
                        text    : "Data Tindakan Tidak Diketahui",
                        type    : "error",
                        timer   : 1500,
                    });
                }
            }
        });
    } else {
        swal({
            title   : "Gagal !",
            text    : "Data Tindakan Tidak Diketahui",
            type    : "error",
            timer   : 1500,
        });
    }
}

function add_tindakan_append(argument) {
  var kode_tindakan   = $('#kode_tindakan').val();
  var nama_tindakan   = $('#nama_tindakan').val();
  if(kode_tindakan == "" || nama_tindakan == ""){
      swal("Perhatian!","Silahkan Pilih Rencana Tindakan Terlebih Dahulu","error",1500);
  }else{
      $.ajax({
          url         : ci_baseurl + "rawatdarurat/pasien_rd/append_tindakan",
          type        : 'POST',
          dataType    : 'JSON',
          data        : {kode_tindakan: kode_tindakan, nama_tindakan:nama_tindakan},
          success: function(data) { /* console.log(data); */
              $('#kode_tindakan').val('');
              $('#nama_tindakan').val('');
              $('#table_rencana_tindakan > tbody').append(data.data);
          }
      });
  }
} 

function setWongBaker(id, name) {
    $("#numeric_wong_baker_name").val("Skala " + name)
    $("#numeric_wong_baker").val(id)
}