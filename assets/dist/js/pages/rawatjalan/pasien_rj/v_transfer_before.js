jQuery(document).ready(function () {
    var table_background;
    var table_diagnosa;
    var table_tindakan;
    set_instalasi();
    get_poliruangan();
    get_table_assesment();
    getDataKirimPasien();
    getDataRencanaTindakan();
  });
  
  function set_instalasi() {
    tujuan = $('#tujuan').val();
    $('#tujuan_instalasi').val($("#instalasi_id").val()).css("pointer-events","none").attr('readonly',true);
  }
  
  function get_poliruangan() {
    instalasi_id = $('#tujuan_instalasi').val();
  
    $.ajax({
    url     : ci_baseurl + "rawatjalan/pasien_rj/get_poliruangan_list",
      type    : 'GET',
      dataType: 'JSON',
      data    : {instalasi_id:instalasi_id},
      success: function(data) {
          $("#poli_ruangan").html(data.list);
          $("#poli_ruangan").val($("#poliruangan_id").val()).css("pointer-events","none").attr('readonly',true);
      }
    });
  }
  
  function get_dokter() {
    poliruangan_id = $('#poli_ruangan').val();
  
    $.ajax({
    url     : ci_baseurl + "rawatjalan/pasien_rj/get_dokter_list",
      type    : 'GET',
      dataType: 'JSON',
      data    : {poliruangan_id:poliruangan_id},
      success: function(data) {
          $("#dokter_pj").html(data.list);
      }
    });
  }
  
  function get_kamar() {
    poliruangan_id      = $('#poli_ruangan').val();
    kelaspelayanan_id   = $('#kelas_pelayanan').val();
  
    $.ajax({
    url     : ci_baseurl + "rawatjalan/pasien_rj/get_kamarruangan",
      type    : 'GET',
      dataType: 'JSON',
      data    : {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
      success: function(data) {
          $("#kamarruangan").html(data.list);
          console.log("Kamar " + data)
      }
    });
  }

  function getDataKirimPasien() {
    pendaftaran_id = $('#pendaftaran_id').val();
    $.ajax({
        url     : ci_baseurl + "rawatjalan/pasien_rj/ajax_list_data_kirimpasien",
        type    : 'GET',
        dataType: 'JSON',
        data    : {
            pendaftaran_id:pendaftaran_id
        },
        success: function(data) {
            if (data.success) {
                data = data.data;
                document.getElementById("halperhatian").innerHTML = data.map((item) => {
                    return "- " + item.hal_perhatian + "&#13;&#10;"
                })
            }
        }
    });
  }

  function getDataRencanaTindakan() {
    pendaftaran_id = $('#pendaftaran_id').val();
    $.ajax({
        url     : ci_baseurl + "rawatjalan/pasien_rj/ajax_list_data_rencana_tindakan",
        type    : 'GET',
        dataType: 'JSON',
        data    : {
            pendaftaran_id:pendaftaran_id
        },
        success: function(data) {
            if (data.success) {
                data = data.data;
                if (data.length > 0)
                    $("#table_rencana_tindakan tbody").html(`${data.map((item, index) => {
                        return `<tr>
                            <td>${(index + 1)}</td>
                            <td>${item.daftartindakan_nama}</td>
                        </tr>`
                    }).join('')}`)
            }
        }
    });
  }
  
  function get_table_assesment() {
    pendaftaran_id = $('#pendaftaran_id').val();
    table_assesment = $('#table_assesment').DataTable({ 
        "processing"  : true,
        "serverSide"  : true,
        "paging"      : false,
        "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
        "ordering"    : false,
        "ajax"        : {
            "url"     : ci_baseurl + "rawatjalan/pasien_rj/ajax_list_assesment",
            "type"    : "GET",
            "data"    : function(d){
            d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_assesment.columns.adjust().draw();
    table_assesment.destroy();
  }
  
  function get_table_background() {
    table_background = $('#table_background').DataTable({ 
      "processing"  : true,
      "serverSide"  : true,
      "paging"      : false,
      "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
      "ordering"    : false,
      "ajax"        : {
        "url"     : ci_baseurl + "rawatjalan/pasien_rj/ajax_list_background",
        "type"    : "GET",
        "data"    : function(d){
          d.pendaftaran_id = $("#pendaftaran_id").val();
        }
      }
    });
    table_background.columns.adjust().draw();
    table_background.destroy();
  }
  
  function get_table_diagnosa() {
    table_diagnosa = $('#table_diagnosa').DataTable({ 
      "processing"  : true,
      "serverSide"  : true,
      "paging"      : false,
      "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
      "ordering"    : false,
      "ajax"        : {
        "url"     : ci_baseurl + "rawatjalan/pasien_rj/get_list_diagnosa_pasien",
        "type"    : "GET",
        "data"    : function(d){
          d.pendaftaran_id = $("#pendaftaran_id").val();
        }
      }
    });
    table_diagnosa.columns.adjust().draw();
    table_diagnosa.destroy();
  }
  
  function get_table_tindakan() {
    table_tindakan = $('#table_tindakan').DataTable({ 
      "processing"  : true,
      "serverSide"  : true,
      "paging"      : false,
      "sDom"        : '<"top"l>rt<"bottom"p><"clear">',
      "ordering"    : false,
      "ajax"        : {
        "url"     : ci_baseurl + "rawatjalan/pasien_rj/get_list_tindakan_pasien",
        "type"    : "GET",
        "data"    : function(d){
          d.pendaftaran_id = $("#pendaftaran_id").val();
        }
      }
    });
    table_tindakan.columns.adjust().draw();
    table_tindakan.destroy();
  }