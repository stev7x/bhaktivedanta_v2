function doPulangkanPasien() {
    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya' 
    }).then(function(){  
            $.ajax({
                url: ci_baseurl + "rawatjalan/pasien_rj/do_pulangkan_pasien",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmInformasiPasien').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        $('#modal_paket_operasi').modal('hide');
                        
                        $('#fmPaketOperasi').find("input[type=text]").val("");
                        $('#fmPaketOperasi').find("select").prop('selectedIndex',0);
                    swal( 
                        'Berhasil!',
                        'Data Berhasil Disimpan.', 
                        'success'
                        )
                    } else {
                        $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message2').html(data.messages);
                        $('#modal_notif2').show();
                        $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif2").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada yang Kosong !',
                        //         'error'
                        //     )
                    }
                }
            });
        
    });
}