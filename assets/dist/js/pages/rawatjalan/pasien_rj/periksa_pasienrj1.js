var table_diagnosa_rj;
var table_tindakan_rj;
var table_resep_rj;
var table_obat_rj;
var table_list_diagnosa;
var table_pemeriksaan_fisik;
var table_imun;
var table_kb;
var table_kb_show;
var table_pnc;
var table_pnc_show;
var table_skor_kspr;
var global_kspr;
var table_faktor_resiko;

$(document).ready(function(){
    // $('table.display').DataTable();
    cekPerawat();
    getTindakan();
    getLaboratorium();
    getFeeDokter();
    table_diagnosa_rj = $('#table_diagnosa_pasienrj').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_diagnosa_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      {
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_diagnosa_rj.columns.adjust().draw();

    table_imun = $('#table_imun').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_imun",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      {
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_imun.columns.adjust().draw();

    table_pnc = $('#table_pnc').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_pnc",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      {
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_pnc.columns.adjust().draw();

    table_pnc_show = $('#table_pnc_show').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_pnc",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      {
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_pnc_show.columns.adjust().draw();

    table_kb = $('#table_kb').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true,
      "retrieve": true,//Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_kb",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      {
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_kb.columns.adjust().draw();

    table_kb_show = $('#table_kb_show').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true,
      "retrieve": true,//Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_kb",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      {
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_kb_show.columns.adjust().draw();


    table_tindakan_rj = $('#table_tindakan_pasienrj').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rj.columns.adjust().draw();

    table_laboratorium_rj = $('#table_laboratorium_pasienrj').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_laboratorium_pasien",
            "type": "GET",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
      });
      table_laboratorium_rj.columns.adjust().draw();

    table_pemeriksaan_fisik = $('#table_pemeriksaan_fisik').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_pemeriksaan_fisik",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_pemeriksaan_fisik.columns.adjust().draw();


    table_resep_rj = $('#table_resep_pasienrj').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_resep",
          "type": "GET",
          "data" : function(d) {
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_resep_rj.columns.adjust().draw();

    var year = new Date().getFullYear();
    // $('.datepicker_diagnosa').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });

    // $('.datepicker_tindakan').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });

    //  $('.datepicker_resep').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });

     // $('#nama_obat').keyup(function() {
     //    var val_obat = $('#nama_obat').val();
     //    $.ajax({
     //        url: ci_baseurl + "rawatjalan/pasien_rj/autocomplete_obat",
     //        type: 'get',
     //        async: false,
     //        dataType: 'json',
     //        data: {val_obat: val_obat},
     //        success: function(data) { /* console.log(data); */
     //            var ret = data.success;
     //            if(ret == true) {
     //                // console.log(data.data);
     //                $(".autocomplete").autocomplete({
     //                    source: data.data,
     //                    open: function(event, ui) {
     //                        $(".ui-autocomplete").css("position: absolute");
     //                        $(".ui-autocomplete").css("top: 0");
     //                        $(".ui-autocomplete").css("left: 0");
     //                        $(".ui-autocomplete").css("cursor: default");
     //                        $(".ui-autocomplete").css("z-index","999999");
     //                        $(".ui-autocomplete").css("font-weight : bold");
     //                    },
     //                    select: function (e, ui) {
     //                        $('#obat_id').val(ui.item.id);
     //                        $('#nama_obat').val(ui.item.value);
     //                        $('#satuan_obat').val(ui.item.satuan);
     //                        $('#harga_jual').val(ui.item.harga_jual);
     //                        $('#harga_netto').val(ui.item.harga_netto);
     //                        $('#current_stok').val(ui.item.current_stok);
     //                        $('#lbl_nama_obat').addClass('active');
     //                        $('#lbl_harga_jual').addClass('active');
     //                        $('#lbl_satuan_obat').addClass('active');
     //                    }
     //              }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
     //                  return $( "<li>" )
     //                    .append( "<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.deskripsi +"- Stok :"+ item.current_stok +"</font></a>" )
     //                    .appendTo( ul );
     //                };
     //            }else{
     //              $("#harga_netto").val('');
     //              $("#current_stok").val('');
     //              $("#obat_id").val('');
     //            }
     //        }
     //    });
     //  });
     $('button#saveResep').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        currentstok = $("#current_stok").val();

          swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
              $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_create_resep_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateResepPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('input[type=number]').val("");
                            $('input[type=text]').val("");
                            getFeeDokter();
                            $('#nama_asuransi').val("");
                            $('#no_asuransi').val("");
                            $('#nama_obat').val("");
                            $('select').prop('selectedIndex',0);
                            $('#fmCreateResepPasien').find("input[type=text]").val("");
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        table_resep_rj.columns.adjust().draw();
                        }else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        }
                    }
                });
          })


    });
    $('button#saveDiagnosa').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{
            swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){

                $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_create_diagnosa_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            // $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                            // $('#card_message').html(data.messages);
                            // $('#modal_notif').show();
                            // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            //     $("#modal_notif").hide();
                            // });
                            swal(
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.',
                                'success'
                              )
                            $('select').prop('selectedIndex',0);
                            $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                            table_diagnosa_rj.columns.adjust().draw();
                        }else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");

                        }
                    }
                });

            });

        }
    });

    $('button#savePemeriksaanFisik').click(function(){
        if ($("#bb").val() < 0 || $("#bb").val() > 150) {
            swal(
                'Error Berat Badan!',
                'Data Berat Badan minimal 0, maksimal 150 kg.',
                'error');
            return;
        }
        if ($("#tb").val() < 0 || $("#tb").val() > 200) {
            swal(
                'Error Tinggi Badan!',
                'Data Tinggi Badan minimal 0, maksimal 200 cm.',
                'error');
            return;
        }
        if ($("#sistole").val() < 60 || $("#sistole").val() > 240) {
            swal(
                'Error Sistole!',
                'Data Sistole minimal 60, maksimal 240.',
                'error');
            return;
        }
        if ($("#diastole").val() < 40 || $("#diastole").val() > 120) {
            swal(
                'Error Diastole!',
                'Data Diastole minimal 40, maksimal 120.',
                'error');
            return;
        }
        swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_create_pemeriksaan_fisik",
                    type: 'POST',
                    dataType: 'JSON',
                    // data: $('form#fmCreatePemeriksaanFisik').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    data: $('form#fmCreatePemeriksaanFisik').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            swal(
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.',
                                'success'
                              )
                            $('select').prop('selectedIndex',0);
                            $('#fmCreatePemeriksaanFisik').find("input[type=text]").val("");
                            $('#fmCreatePemeriksaanFisik').find("input[type=number]").val("");
                            $('#anamnesis').val("");
                            $('#keterangan').val("");
                            table_pemeriksaan_fisik.columns.adjust().draw();
                        }else {
                            $('#modal_notif_fisik').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message_fisik').html(data.messages);
                            $('#modal_notif_fisik').show();
                            $("#modal_notif_fisik").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif_fisik").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");

                        }
                    }
                });

            });

    });

    $('button#saveKB').click(function(){
        swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_create_kb",
                    type: 'POST',
                    dataType: 'JSON',
                    // data: $('form#fmCreatePemeriksaanFisik').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    data: $('form#fmCreateKB').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            swal(
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.',
                                'success'
                              )
                            $('select').prop('selectedIndex',0);
                            $('#fmCreateKB').find("input[type=text]").val("");
                            $('#fmCreateKB').find("input[type=number]").val("");
                            // $('#anamnesis').val("");
                            // $('#keterangan').val("");
                            table_kb.columns.adjust().draw();
                        }else {
                            swal(
                                'gagal!',
                                'Data Masih Ada Yang Kosong.',
                                'error'
                            )
                            $('#modal_notif_fisik').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message_fisik').html(data.messages);
                            $('#modal_notif_fisik').show();
                            $("#modal_notif_fisik").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif_fisik").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");

                        }
                    }
                });

            });

    });

    $('button#savePNC').click(function(){
        swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_create_pnc",
                    type: 'POST',
                    dataType: 'JSON',
                    // data: $('form#fmCreatePemeriksaanFisik').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    data: $('form#fmCreatePNC').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            swal(
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.',
                                'success'
                              )
                            $('select').prop('selectedIndex',0);
                            $('#fmCreateKB').find("input[type=text]").val("");
                            $('#fmCreateKB').find("input[type=number]").val("");
                            // $('#anamnesis').val("");
                            // $('#keterangan').val("");
                            table_pnc.columns.adjust().draw();
                        }else {
                            swal(
                                'Gagal!',
                                'Data Masih Ada Yang Kosong.',
                                'error'
                            )
                        }
                    }
                });

            });

    });

    $('button#savePemeriksaanKebidanan').click(function(){
        swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_create_pemeriksaan_kebidanan",
                    type: 'POST',
                    dataType: 'JSON',
                    // data: $('form#fmCreatePemeriksaanFisik').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    data: $('form#fmCreatePemeriksaanKebidanan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            // getTarifImun();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                            $('select').prop('selectedIndex',0);
                            $('#fmCreatePemeriksaanKebidanan').find("input[type=text]").val("");
                            $('#fmCreatePemeriksaanKebidanan').find("input[type=number]").val("");
                            $("#kspr").select2("val", "");
                        }else {
                            swal(
                                'Gagal!',
                                'Data Masih Ada Yang Kosong !.',
                                'error'
                            )
                            $('#modal_notif_bidan').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message_bidan').html(data.messages);
                            $('#modal_notif_bidan').show();
                            $("#modal_notif_bidan").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif_bidan").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");

                        }
                    }
                });

            });
    });


    $('button#saveImun').click(function(){
        swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_create_imun",
                    type: 'POST',
                    dataType: 'JSON',
                    // data: $('form#fmCreatePemeriksaanFisik').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    data: $('form#fmCreateImun').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            // getTarifImun();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                            $('select').prop('selectedIndex',0);
                            $('#fmCreateImun').find("input[type=text]").val("");
                            $('#fmCreateImun').find("input[type=number]").val("");
                            // $('#modal_notif_vaksinasi');
                            // $('#card_message_vaksinasi').html(data.messages);
                            // $('#modal_notif_vaksinasi').show();
                            // $("#modal_notif_vaksinasi").fadeTo(4000, 500).slideUp(1000, function () {
                            //     $("#modal_notif_vaksinasi").hide();
                            // });
                            // $("html, body").animate({ scrollTop: 100 }, "fast");
                            table_imun.columns.adjust().draw();
                        }else {
                            swal(
                                'Gagal!',
                                'Data Yang Anda Masukan Sudah Ada ! ',
                                'error'
                            )
                            // $('#modal_notif_vaksinasi').removeClass('alert-success').addClass('alert-danger');
                            // $('#card_message_vaksinasi').html(data.messages);
                            // $('#modal_notif_vaksinasi').show();
                            // $("#modal_notif_vaksinasi").fadeTo(4000, 500).slideUp(1000, function(){
                            //     $("#modal_notif_vaksinasi").hide();
                            // });
                            // $("html, body").animate({ scrollTop: 100 }, "fast");
                        }
                    }
                });
            });
    });

    $('button#saveSkorKSPR').click(function(){

        swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatjalan/pasien_rj/do_create_skorKSPR",
                    type: 'POST',
                    dataType: 'JSON',
                    // data: $('form#fmCreatePemeriksaanFisik').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    data: $('form#fmCreateSkorKSPR').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {

                            // getTarifImun();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                            $('select').prop('selectedIndex',0);
                            $('#fmCreateSkorKSPR').find("input[type=text]").val("");
                            $('#fmCreateSkorKSPR').find("input[type=number]").val("");
                            // $('#modal_notif_vaksinasi');
                            // $('#card_message_vaksinasi').html(data.messages);
                            // $('#modal_notif_vaksinasi').show();
                            // $("#modal_notif_vaksinasi").fadeTo(4000, 500).slideUp(1000, function () {
                            //     $("#modal_notif_vaksinasi").hide();
                            // });
                            // $("html, body").animate({ scrollTop: 100 }, "fast");
                            table_faktor_resiko.columns.adjust().draw();
                            table_skor_kspr.columns.adjust().draw();

                        }else {
                            swal(
                                'Gagal!',
                                'Data Yang Anda Masukan Sudah Ada ! ',
                                'error'
                            )
                            // $('#modal_notif_vaksinasi').removeClass('alert-success').addClass('alert-danger');
                            // $('#card_message_vaksinasi').html(data.messages);
                            // $('#modal_notif_vaksinasi').show();
                            // $("#modal_notif_vaksinasi").fadeTo(4000, 500).slideUp(1000, function(){
                            //     $("#modal_notif_vaksinasi").hide();
                            // });
                            // $("html, body").animate({ scrollTop: 100 }, "fast");
                        }
                    }
                });
            });
    });

    $('#tindakan').change(function () {
        var daftartindakan_id = $("#tindakan").val();
    // alert (daftartindakan_id);
        $.ajax({
            url: ci_baseurl + "rawatjalan/pasien_rj/get_tarif_tindakan",
            type: 'GET',
            dataType: 'JSON',
            data: {daftartindakan_id:daftartindakan_id},
            success: function(data) {
                $('input[name=harga_tindakan]').val(data.list.harga_tindakan);
                $('input[name=harga_cyto]').val(data.list.cyto_tindakan);
            }
        });
    });

    $('button#savePerawat').click(function() {
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#perawat1").val();
        kelas_pelayanan =$("#kelaspelayanan_id").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{

            swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){

                $.ajax({
                        url: ci_baseurl + "rawatjalan/pasien_rj/do_create_fee_perawat",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter+ "&kelas_pelayanan=" +kelas_pelayanan,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                // $('#modal_notif2').removeClass('alert-danger').addClass('alert-success');
                                // $('#card_message2').html(data.messages);
                                // $('#modal_notif2').show();
                                // $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                                //     $("#modal_notif2").hide();
                                // });
                                swal(
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.',
                                'success'
                              )
                                $('input[type=number]').val("");
                                getFeeDokter();
                                $('select').prop('selectedIndex',0);
                                $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                                table_tindakan_rj.columns.adjust().draw();
                                table_laboratorium_rj.columns.adjust().draw();
                            }else {
                                $('#modal_notif_tindakan').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message_tindakan').html(data.messages);
                                $('#modal_notif_tindakan').show();
                                $("#modal_notif_tindakan").fadeTo(4000, 500).slideUp(1000, function(){
                                $("#modal_notif_tindakan").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                            }
                        }
                    });

            });

        }
    })

    $('button#saveFeeDokter').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        kelas_pelayanan =$("#kelaspelayanan_id").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{

            swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){

                $.ajax({
                        url: ci_baseurl + "rawatjalan/pasien_rj/do_create_fee_dokter",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter+ "&kelas_pelayanan=" +kelas_pelayanan,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                // $('#modal_notif2').removeClass('alert-danger').addClass('alert-success');
                                // $('#card_message2').html(data.messages);
                                // $('#modal_notif2').show();
                                // $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                                //     $("#modal_notif2").hide();
                                // });
                                swal(
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.',
                                'success'
                              )
                                $('input[type=number]').val("");
                                getFeeDokter();
                                $('select').prop('selectedIndex',0);
                                $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                                table_tindakan_rj.columns.adjust().draw();
                                table_laboratorium_rj.columns.adjust().draw();
                            }else {
                                $('#modal_notif_tindakan').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message_tindakan').html(data.messages);
                                $('#modal_notif_tindakan').show();
                                $("#modal_notif_tindakan").fadeTo(4000, 500).slideUp(1000, function(){
                                $("#modal_notif_tindakan").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                            }
                        }
                    });

            });

        }
    });

    $('button#saveAmbulanceAssistance').click(function() {
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
        }).then(function(){

            $.ajax({
                url: ci_baseurl + "rawatjalan/pasien_rj/do_save_ambulance_assistance",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateAmbulanceAssistance').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        swal(
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.',
                            'success'
                        )
                        $('input[type=number]').val("");
                        getFeeDokter();
                        $('select').prop('selectedIndex',0);
                        $('#fmCreateMedicalInformation').find("input[type=text]").val("");
                        location.reload();
                    }else {
                        $('#modal_notif_tindakan').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message_tindakan').html(data.messages);
                        $('#modal_notif_tindakan').show();
                        $("#modal_notif_tindakan").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif_tindakan").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                    }
                }
            });

        });
    });

    $('button#saveMedicalInformation').click(function() {
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
        }).then(function(){

            $.ajax({
                url: ci_baseurl + "rawatjalan/pasien_rj/do_save_medical_information",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateMedicalInformation').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        swal(
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.',
                            'success'
                        )
                        $('input[type=number]').val("");
                        getFeeDokter();
                        $('select').prop('selectedIndex',0);
                        $('#fmCreateMedicalInformation').find("input[type=text]").val("");
                        location.reload();
                    }else {
                        $('#modal_notif_tindakan').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message_tindakan').html(data.messages);
                        $('#modal_notif_tindakan').show();
                        $("#modal_notif_tindakan").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif_tindakan").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                    }
                }
            });

        });
    })

    $('button#saveTindakan').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        kelas_pelayanan =$("#kelaspelayanan_id").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{

            swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){

                $.ajax({
                        url: ci_baseurl + "rawatjalan/pasien_rj/do_create_tindakan_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter+ "&kelas_pelayanan=" +kelas_pelayanan,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                // $('#modal_notif2').removeClass('alert-danger').addClass('alert-success');
                                // $('#card_message2').html(data.messages);
                                // $('#modal_notif2').show();
                                // $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                                //     $("#modal_notif2").hide();
                                // });
                                swal(
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.',
                                'success'
                              )
                                $('input[type=number]').val("");
                                getFeeDokter();
                                $('select').prop('selectedIndex',0);
                                $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                                table_tindakan_rj.columns.adjust().draw();
                                table_laboratorium_rj.columns.adjust().draw();
                            }else {
                                $('#modal_notif_tindakan').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message_tindakan').html(data.messages);
                                $('#modal_notif_tindakan').show();
                                $("#modal_notif_tindakan").fadeTo(4000, 500).slideUp(1000, function(){
                                $("#modal_notif_tindakan").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                            }
                        }
                    });

            });

        }
    });

    $('button#saveTindakanLab').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        kelas_pelayanan =$("#kelaspelayanan_id").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{

            swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){

                $.ajax({
                        url: ci_baseurl + "rawatjalan/pasien_rj/do_create_laboratorium_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateLabPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter+ "&kelas_pelayanan=" +kelas_pelayanan,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                                $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                // $('#modal_notif2').removeClass('alert-danger').addClass('alert-success');
                                // $('#card_message2').html(data.messages);
                                // $('#modal_notif2').show();
                                // $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                                //     $("#modal_notif2").hide();
                                // });
                                swal(
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.',
                                'success'
                              )
                                $('input[type=number]').val("");
                                getFeeDokter();
                                $('select').prop('selectedIndex',0);
                                $('#fmCreateLabPasien').find("input[type=text]").val("");
                                table_tindakan_rj.columns.adjust().draw();
                                table_laboratorium_rj.columns.adjust().draw();
                            }else {
                                $('#modal_notif_laboratorium').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message_laboratorium').html(data.messages);
                                $('#modal_notif_laboratorium').show();
                                $("#modal_notif_laboratorium").fadeTo(4000, 500).slideUp(1000, function(){
                                $("#modal_notif_laboratorium").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                            }
                        }
                    });

            });

        }
    });

});



function getTindakan(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    var type_pembayaran = $('#type_pembayaran').val();
    var jenis_pasien = $('#jenis_pasien').val();
    var type_call = $('#type_call').val();
    $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id,type_pembayaran:type_pembayaran,jenis_pasien:jenis_pasien,type_call:type_call},
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    });
}

function getFeeDokter() {
    var jenis_pasien = $('#jenis_pasien').val();
    var type_call = $('#type_call').val();
    var hotel_id = $('#hotel_id').val();
    var shift = $('#shift').val();

    $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/get_fee_dokter",
        type: 'GET',
        dataType: 'JSON',
        data: {hotel_id:hotel_id,jenis_pasien:jenis_pasien,type_call:type_call,shift:shift},
        success: function(data) {
            $("#fee_dokter").val(data);
            console.log(data);
        }
    });
}

function getLaboratorium(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    var type_pembayaran = $('#type_pembayaran').val();
    var jenis_pasien = $('#jenis_pasien').val();
    var type_call = $('#type_call').val();
    $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/get_laboratorium_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id,type_pembayaran:type_pembayaran,jenis_pasien:jenis_pasien,type_call:type_call},
        success: function(data) {
            $("#laboratorium").html(data.list);
        }
    });
}

function getTarifTindakan(){
    var daftartindakan_id = $("#tindakan").val();
    // alert (daftartindakan_id);
    $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {daftartindakan_id:daftartindakan_id},
        success: function(data) {
            $('input[name=harga_tindakan]').val(data.list.harga_tindakan);
            $('input[name=harga_cyto]').val(data.list.cyto_tindakan);
        }
    });


}

function getTarifTindakanLab(){
    var daftartindakan_id = $("#laboratorium").val();
    // alert (daftartindakan_id);
    $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {daftartindakan_id:daftartindakan_id},
        success: function(data) {
            $('input[name=harga_tindakan_lab]').val(data.list.harga_tindakan);
            $('input[name=harga_cyto_lab]').val(data.list.cyto_tindakan);
        }
    });
}

$(document).on('keyup mouseup', '#jml_tindakan', function() {
    hitungHarga();
  });

  $(document).on('keyup mouseup', '#jml_tindakan_lab', function() {
    hitungHargaLab();
  });

function hitungHarga(){
    // alert("aya");
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();

    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $('input[name=harga_tindakan_satuan]').val(tarif_tindakan);
    $('input[name=subtotal]').val(subtotal);
    $('input[name=totalharga]').val(total);
}

function hitungHargaLab(){
    // alert("aya");
    var tarif_tindakan = $("#harga_tindakan_lab").val();
    var tarif_cyto = $("#harga_cyto_lab").val();
    var jml_tindakan = $("#jml_tindakan_lab").val();
    var is_cyto = $("#is_cyto_lab").val();

    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $('input[name=harga_tindakan_satuan_lab]').val(tarif_tindakan);
    $('input[name=subtotal_lab]').val(subtotal);
    $('input[name=totalharga_lab]').val(total);
}



function hapusDiagnosa(diagnosapasien_id){
    var jsonVariable = {};
    jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if(diagnosapasien_id){

      swal({
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_diagnosa",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                  success: function(data){
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              // $('.modal_notif').removeClass('red').addClass('green');
                              // $('#modal_card_message').html(data.messages);
                              // $('.notmodal_notifif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          table_diagnosa_rj.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {

                          swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                          )
                              // $('.modal_notif').removeClass('green').addClass('red');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          }
                      }
                  });

        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusAllKSPR(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if (pendaftaran_id){
      swal({
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_all_kspr",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                  success: function(data){
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              // $('.modal_notif').removeClass('red').addClass('green');
                              // $('#modal_card_message').html(data.messages);
                              // $('.notmodal_notifif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          $("#resiko_keterangan").val("");
                          table_skor_kspr.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {

                          swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                          )
                              // $('.modal_notif').removeClass('green').addClass('red');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          }
                      }
                  });

        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusKSPR(id_skor_kspr){
    var jsonVariable = {};
    jsonVariable["id_skor_kspr"] = id_skor_kspr;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if (id_skor_kspr){
      swal({
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_kspr",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                  success: function(data){
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              // $('.modal_notif').removeClass('red').addClass('green');
                              // $('#modal_card_message').html(data.messages);
                              // $('.notmodal_notifif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          table_skor_kspr.columns.adjust().draw();
                        table_faktor_resiko.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {

                          swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                          )
                              // $('.modal_notif').removeClass('green').addClass('red');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          }
                      }
                  });

        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}


function hapusImun(pasienimunisasi_id){
    var jsonVariable = {};
    jsonVariable["pasienimunisasi_id"] = pasienimunisasi_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if (pasienimunisasi_id){
      swal({
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_imun",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                  success: function(data){
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                          table_imun.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {

                          swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                          )
                              // $('.modal_notif').removeClass('green').addClass('red');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          }
                      }
                  });

        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusKebidanan(pemeriksaan_obgyn_id){
    var jsonVariable = {};
    jsonVariable["pemeriksaan_obgyn_id"] = pemeriksaan_obgyn_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if (pemeriksaan_obgyn_id){

      swal({
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_kebidanan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                  success: function(data){
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                          swal(
                            'Berhasil!',
                            'Data Berhasil Direset.',
                            'success',
                          )
                          } else {
                          swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                          )
                              // $('.modal_notif').removeClass('green').addClass('red');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          }
                      }
                  });

        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusPNC(tindakanpasien_id){
    var jsonVariable = {};
    jsonVariable["tindakanpasien_id"] = tindakanpasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if (tindakanpasien_id){
      swal({
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_pnc",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                  success: function(data){
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                          table_pnc.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {

                          swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                          )
                              // $('.modal_notif').removeClass('green').addClass('red');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          }
                      }
                  });

        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusKB(pasienkb_id){
    var jsonVariable = {};
    jsonVariable["pasienkb_id"] = pasienkb_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if (pasienkb_id){
      swal({
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_kb",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                  success: function(data){
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                          table_kb.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {

                          swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                          )
                              // $('.modal_notif').removeClass('green').addClass('red');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                          }
                      }
                  });

        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}
function hapusTindakan(tindakanpasien_id){
    var jsonVariable = {};
    jsonVariable["tindakanpasien_id"] = tindakanpasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    if(tindakanpasien_id){

      swal({
          text: "Apakah anda yakin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya'
        }).then(function(){

          $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_tindakan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                          table_tindakan_rj.columns.adjust().draw();
                          table_laboratorium_rj.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success'
                            )
                          } else {
                            swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error'
                            )
                          }
                      }
                });


      });

    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusPemFisik(id_detail_pemeriksaan){
    var jsonVariable = {};
    jsonVariable["id_detail_pemeriksaan"] = id_detail_pemeriksaan;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    if (id_detail_pemeriksaan){

      swal({
          text: "Apakah anda yakin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya'
        }).then(function(){

          $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/do_delete_pemfisik",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              table_pemeriksaan_fisik.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success'
                            )
                          } else {
                            swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error'
                            )
                          }
                      }
                });


      });

    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}
function hapusResep(id){
    var jsonVariable = {};
    jsonVariable["id"] = id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delres').val();

    if(id){
      swal({
          text: "Apakah anda yakin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            $.ajax({
                  url: ci_baseurl + "rawatjalan/pasien_rj/delete_resep",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              $('.modal_notif').removeClass('red').addClass('green');
                              $('#modal_card_message').html(data.messages);
                              $('.notmodal_notifif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                          table_resep_rj.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success'
                          )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                              swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error'
                              )
                          }
                      }
                });
        });

    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function dialogObat() {
    table_obat_rj = $('#table_list_obat').DataTable({
       // "searching" :true,
        "destroy" : true,
        // "processing": true, //Feature control the processing indicator.
        // "serverSide": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_obat",
            "type": "GET",
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1,0 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });

    table_obat_rj.columns.adjust().draw();
}


function dialogDiagnosa($pendaftaran_id) {
    console.log($pendaftaran_id);
    // alert($pendaftaran_id);
    // $('#modal_list_obat').openModal('toggle');
    if (table_list_diagnosa) {
        table_list_diagnosa.destroy();
    }
    table_list_diagnosa = $('#table_list_diagnosa').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "searching": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_diagnosa/",
            "type": "GET",
            "data" : {
                        "pendaftaran_id": $pendaftaran_id,
                    }
        },
        "columnDefs": [{
            "targets": [-1],
            "orderable": false,
        },],
    });
    table_list_diagnosa.columns.adjust().draw();
}

function pilihDiagnosa(diagnosa2_id) {
    if (diagnosa2_id != "") {
        $.ajax({
            url: ci_baseurl + "rawatjalan/pasien_rj/ajax_get_diagnosa_by_id",
            type: 'get',
            dataType: 'json',
            data: { diagnosa2_id: diagnosa2_id },
            success: function (data) {
                var ret = data.success;
                if (ret == true) {
                    $('#diagnosa_id').val(data.data['diagnosa_id']);
                    $('#kode_diagnosa').val(data.data['diagnosa_kode']);
                    $('#nama_diagnosa').val(data.data['diagnosa_nama']);
                    $('#kode_diagnosa_icd10').val(data.data['kode_diagnosa']);
                    $('#nama_diagnosa_icd10').val(data.data['nama_diagnosa']);
                    // $('#obat_id').val(data.data['kode_barang']);
                    $("#modal_diagnosa").modal('hide');
                } else {
                    $("#harga_netto").val('');
                    $("#current_stok").val('');
                    $("#obat_id").val('');
                }
            }
        });
    } else {
        // $('#modal_list_obat').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}

function countHarga(e) {
    $('#harga_jual').val(e.value * $('#harga').val());
}

function pilihObat(id){
    var jenis_pasien = $('#jenis_pasien').val();
    var type_call = $('#type_call').val();

	if (id != ""){
		$.ajax({
			url: ci_baseurl + "rawatjalan/pasien_rj/get_detail_stok",
			type: 'get',
			dataType: 'json',
			data: { id:id },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
				    stok = data.data.stok;
                    obat = data.data.obat;
                    persediaan = data.data.persediaan;

                    $('#harga_jual').val("");
                    $('#jumlah_obat').val("");

                    $('#nama_obat').val(obat.nama);
                    $('#obat_id').val(obat.id);
                    $('#stok_id').val(stok.id);
                    $('#persediaan_id').val(obat.persediaan_id);
                    $('#jenis_barang_id').val(obat.jenis_barang_id);
                    $('#satuan').val(persediaan.persediaan_3);
                    $('#current_stok').val(stok.stok_akhir);

                    if (type_call < 4 && type_call > 0) {
                        if (jenis_pasien == 1) {
                            $('#harga').val(obat.harga_visit_local);
                        } else if (jenis_pasien == 2) {
                            $('#harga').val(obat.harga_visit_dome);
                        } else if (jenis_pasien == 3) {
                            $('#harga').val(obat.harga_visit_asing);
                        } else {
                            alert("Jenis Pasien tidak ditemukan");
                        }
                    } else if (type_call > 3 && type_call < 7) {
                        if (jenis_pasien == 1) {
                            $('#harga').val(obat.harga_oncall_local);
                        } else if (jenis_pasien == 2) {
                            $('#harga').val(obat.harga_oncall_dome);
                        } else if (jenis_pasien == 3) {
                            $('#harga').val(obat.harga_oncall_asing);
                        } else {
                            alert("Jenis Pasien tidak ditemukan");
                        }
                    } else {
                        alert("type Call Pasien tidak ditemukan");
                    }



					// $('#obat_id').val(data.data['kode_barang']);
					// $('#nama_obat').val(data.data['nama_barang']);
					// $('#satuan_obat').val(data.data['id_sediaan']);
					// $('#harga_jual').val(data.data['harga_satuan']);
                    // $('#harga_netto').val(data.data['harga_satuan']);
					// $('#current_stok').val(data.data['stok_akhir']);
					// $('#lbl_nama_obat').addClass('active');
					// $('#lbl_harga_jual').addClass('active');
                    // $('#lbl_satuan_obat').addClass('active');
                    // $('#id_jenis_barang').val(data.data['id_jenis_barang']);
                    // $('#is_bpjs').val(data.data['is_bpjs']);
                    // $('#id_sediaan').val(data.data['id_sediaan']);
                    $("#modal_reseptur").modal('hide');
				} else {
					$("#harga_netto").val('');
					$("#current_stok").val('');
					$("#obat_id").val('');
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}



function getSkorKSPR() {
    var propinsi_id = $("#pj_propinsi").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: { propinsi_id: propinsi_id },
        success: function (data) {
            $("#pj_kabupaten").html(data.list);
        }
    });
}



function getTarifImun() {
    var daftartindakan_id = $("#imun").val();
    $.ajax({
        url: ci_baseurl + "rawatjalan/pasien_rj/get_tarif_imun",
        type: 'GET',
        dataType: 'JSON',
        data: { daftartindakan_id: daftartindakan_id },
        success: function (data) {
            $("#harga_tindakan_imun").val(data.list.harga_tindakan);
            $("#harga_cyto_imun").val(data.list.cyto_tindakan);
        }
    });
}


function getSUMKSPR(){
    // var skor = $("#kspr : selected").val();
    // var skor[i] = selected.value;
    var skor = $("#kspr option:selected").val();
    var tampung = skor;
    var jum = tampung++;
    // var foo = $('#multiple').val();
}

function selectFunction(e) {
    var skor = $('select option:selected').map(function () {
        return $(this).attr('data-skor');
    })
        .get().map(parseFloat).reduce(function (a, b) {
            return a + b
        });
    var jum = 2 + skor;
    $("#jum_skor").val(jum);
}


function putKSPR(e) {
    var skor = $('select option:selected').map(function () {
        return $(this).attr('data-skor');
    })
        .get().map(parseFloat).reduce(function (a, b) {
            return a + b
        });
    var nama = $('#kspr option:selected').data('val');
    $("#nama_kspr").val(nama);
    $("#skor_kspr").val(skor);
}

function jml_kspr() {
    var skor = $("#skor_kspr").val();
    var jml  = $("#jml").val();
    var sum = skor * jml;
    var total = total + sum;

    $("#sum_kspr").val(sum);

    $("#total_skor_kspr");
}


$.fn.dataTable.Api.register('column().data().sum()', function () {
    return this.reduce(function (a, b) {
        var x = parseFloat(a) || 0;
        var y = parseFloat(b) || 0;
        return x + y;
    },0);
});


function jum_kspr() {
    global_kspr = (table_skor_kspr.column(4).data().sum()) + 2;
    $("#jum_skor").val(global_kspr);
}

function cekSkorKSPR() {
    // alert('Total skor KSPR: ' + table_skor_kspr.column(3).data().sum());

    global_kspr = (table_skor_kspr.column(4).data().sum());

        swal({
            text: "Total skor KSPR: " + global_kspr + " + 2",
            // type: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            // cancelButtonText: 'Tutup',
            confirmButtonText: 'Tutup'
        })
}

function tampilKebidanan() {
   $('#form_tabs').addClass('hidden');
   $('#result_tabs').removeClass('hidden');
    var a = global_kspr + 2;
    $("#jum_skor").val(a);
}

// function tampilKebidanan() {
//    $('#form_tabs').addClass('hidden');
//    $('#result_tabs').removeClass('hidden');
// }

function changeHasilUSG(){
    if ($('#hasil_usg').is(':checked')) {
        $("#hpl_1").attr('readonly', false);
        $("#usia_kehamilan").attr('readonly', false);
        $("#hpht").attr('readonly', true);
        $("#hpht").attr('disabled', true);
        $("#hpht").off('keyup');
        $("#hpl_1").val("");
        $("#usia_kehamilan").val("");
        $("#hpl_manual").show();
        $("#hpl_otomatis").hide();
        $("#hpht").val("");
        // $("#hpht").removeAttr('value');
    }else{
        $("#hpl_manual").hide();
        $("#hpl_otomatis").show();
        $("#hpht").attr('readonly', false);
        $("#hpht").attr('disabled', false);
        $("#hpl_1").attr('readonly', true);
        $("#usia_kehamilan").attr('readonly', true);
        $("#usia_kehamilan").val(0);
        // $("#hpht").on('keyup');

    }
}


function changeHPHT(){
    var hpht = $("#hpht").val();

// hitung hpl otomatis
    // tgl_skrg = new Date(hpht);
    // tgl_skrg.setDate(tgl_skrg.getDate() + 252);
    // var dd = tgl_skrg.getDate();
    // var mm = tgl_skrg.getMonth() + 1;
    // var y = tgl_skrg.getFullYear();
    // var hitung_hpl = mm + '/' + dd + '/' + y;
    // var hitung_hpl_show = dd + '/' + mm + '/' + y;
    // $('#hpl').val(hitung_hpl_show);
    // $('#hpl_1').val(hitung_hpl);

// hitung hpl baru
    tgl = new Date(hpht);
    tgl.setDate(tgl.getDate() + 7);
    tgl.setMonth(tgl.getMonth() - 2);
    var dd = tgl.getDate();
    var mm = tgl.getMonth() + 1;
    var y  = tgl.getFullYear();
    if (mm <= 0){
        var dd = tgl.getDate();
        var mm = tgl.getMonth();
        var y = tgl.getFullYear();
    }else{
        var dd = tgl.getDate();
        var mm = tgl.getMonth();
        var y = tgl.getFullYear() + 1;
    }

    var hitung_hpl = mm + '/' + dd + '/' + y;
    var hitung_hpl_show = dd + '/' + mm + '/' + y;
    $('#hpl').val(hitung_hpl_show);
    $('#hpl_1').val(hitung_hpl);
    // $("#hpl_2").val("");


// hitung usia kehamilan
    var date_now = new Date();
    var date_hpht = new Date(hpht);
    var hitung_kehamilan =  Math.abs(date_now.getTime() - date_hpht.getTime());
    var diffdays = Math.round(hitung_kehamilan / (1000 * 3600 * 24));
    hitung_kehamilan = Math.ceil(diffdays / 7);
    $("#usia_kehamilan").val(hitung_kehamilan);
}

function ubah() {
    $("#jum_skor").val(2);
    $("#kspr").select2("val", "");
}

function tampilFormBidan(){
    var skor = $("#skor_cek").val();
    if(skor != ""){
        $("#form_tabs").hide();
        $("#result_tabs").show();
        $("#form").hide();
        $("#result").show();
        $("#form_a").removeClass('active');
        $("#result_a").addClass('active');
    }else{
        $("#result_a").removeClass('active');
        $("#form_a").addClass('active');
        $("#form_tabs").show();
        $("#result_tabs").hide();
        $("#form").show();
        $("#result").hide();

    }
}

function showCaraBayar(jenis_poli) {
    if (jenis_poli == "umum") {
        value = $('#type_pembayaran').val();
        div = $('.pembayaran1');
        div2 = $('.totalumum');
        nama = $('#nama_asuransi');
        nomor = $('#no_asuransi');
        lblNama = $('#lblNamaPembayaran_umum');
        lblNomor = $('#lblNomorPembayaran_umum');
        // hitungTotal();
    } else {

    }
    id = $('#pembayaran_id').val();
    $.ajax({
        url: ci_baseurl + "penunjang/laboratorium/getCaraPembayaran/" + id,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            if (value != "PRIBADI") {
                div.show(500);
                // div2.show(500);
                if (value == "ASURANSI1") {
                    nama.val(data.nama_asuransi);
                    nomor.val(data.no_asuransi);
                    lblNama.text('Nama Asuransi ');
                    lblNomor.text('Nomor Asuransi ');
                }
                if (value == "ASURANSI2") {
                    nama.val(data.nama_asuransi2);
                    nomor.val(data.no_asuransi2);
                    lblNama.text('Nama Asuransi 2');
                    lblNomor.text('Nomor Asuransi 2');
                }

            } else {
                div.hide(500);
                div2.hide(500);
            }

        }
    });
}

function exportToExcelBidan() {
    window.location.href = ci_baseurl + "rawatjalan/pasien_rj/exportToExcelBidan";
}

function appendFaktorResiko() {
    var jum = $('#jml').val();
    var nama = $('#kspr option:selected').data('val');
    $('#resiko_keterangan').val($('#resiko_keterangan').val() + nama + "   " + jum + "X, ").append('<br>');
}

function skorKSPR(){

    table_skor_kspr = $('#table_skor_kspr').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_skor_kspr",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_skor_kspr.columns.adjust().draw();


    $.fn.dataTable.Api.register('column().data().sum()', function () {
        return this.reduce(function (a, b) {
            var x = parseFloat(a) || 0;
            var y = parseFloat(b) || 0;
            return x + y;
        }, 0);
    });


    global_kspr = (table_skor_kspr.column(4).data().sum());
    $("#jum_skor").val(global_kspr);
}

function printStickerResep(id){
    window.open(ci_baseurl+'rawatjalan/pasien_rj/print_sticker?id='+id);
}

function faktorResiko(){
    table_faktor_resiko = $('#table_faktor_resiko').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatjalan/pasien_rj/ajax_list_faktor_resiko",
            "type": "GET",
            "data": function (d) {
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
    });
    table_faktor_resiko.columns.adjust().draw();
}

function pilihPerawat1(e) {
    var currentPerawat1 = $('#c_perawat1').val();
    if (e.value === "") {
        $('#c_perawat1').val("");
        $('#perawat2 option[value="'+currentPerawat1+'"]').unwrap();
        $('#perawat3 option[value="'+currentPerawat1+'"]').unwrap();
    } else if (e.value != currentPerawat1) {
        $('#c_perawat1').val(e.value);
        $('#perawat2 option[value="'+e.value+'"]').wrap('<span/>');
        $('#perawat3 option[value="'+e.value+'"]').wrap('<span/>');
    } else {
        $('#c_perawat1').val(currentPerawat1);
        $('#perawat2 option[value="'+currentPerawat1+'"]').wrap('<span/>');
        $('#perawat3 option[value="'+currentPerawat1+'"]').wrap('<span/>');
    }
}

function pilihPerawat2(e) {
    var currentPerawat2 = $('#c_perawat2').val();
    if (e.value === "") {
        $('#c_perawat2').val("");
        $('#perawat1 option[value="'+currentPerawat2+'"]').unwrap();
        $('#perawat3 option[value="'+currentPerawat2+'"]').unwrap();
    } else if (e.value != currentPerawat2) {
        $('#c_perawat2').val(e.value);
        $('#perawat1 option[value="'+e.value+'"]').wrap('<span/>');
        $('#perawat3 option[value="'+e.value+'"]').wrap('<span/>');
    } else {
        $('#c_perawat2').val(currentPerawat2);
        $('#perawat1 option[value="'+currentPerawat2+'"]').wrap('<span/>');
        $('#perawat3 option[value="'+currentPerawat2+'"]').wrap('<span/>');
    }
}

function pilihPerawat3(e) {
    var currentPerawat3 = $('#c_perawat3').val();
    if (e.value === "") {
        $('#c_perawat3').val("");
        $('#perawat1 option[value="'+currentPerawat3+'"]').unwrap();
        $('#perawat2 option[value="'+currentPerawat3+'"]').unwrap();
    } else if (e.value != currentPerawat3) {
        $('#c_perawat3').val(e.value);
        $('#perawat1 option[value="'+e.value+'"]').wrap('<span/>');
        $('#perawat2 option[value="'+e.value+'"]').wrap('<span/>');
    } else {
        $('#c_perawat3').val(currentPerawat3);
        $('#perawat1 option[value="'+currentPerawat3+'"]').wrap('<span/>');
        $('#perawat2 option[value="'+currentPerawat3+'"]').wrap('<span/>');
    }
}

function cekPerawat() {
    var p1 = $('#c_perawat1').val();
    var p2 = $('#c_perawat2').val();
    var p3 = $('#c_perawat3').val();

    // if ((p1 == "") && (p2 == "") && (p3 == "")) {
    //
    // } else {
    //
    // }
}

function customDosis(e) {
    $('#custom_dosis_id').val($('#dosis_id option:selected').html());
}

function customPemberian(e) {
    $('#custom_pemberian_id').val($('#pemberian_id option:selected').html());
}

function customAturanPakai(e) {
    $('#custom_aturan_pakai_id').val($('#aturan_pakai_id option:selected').html());
}

function editDosis(e) {
    if (e.checked) {
        $('#custom_dosis_id').show();
        $('#dosis_id').hide();
    } else {
        $('#custom_dosis_id').hide();
        $('#dosis_id').show();
    }
}

function editPemberian(e) {
    if (e.checked) {
        $('#custom_pemberian_id').show();
        $('#pemberian_id').hide();
    } else {
        $('#custom_pemberian_id').hide();
        $('#pemberian_id').show();
    }
}

function editAturanPakai(e) {
    if (e.checked) {
        $('#custom_aturan_pakai_id').show();
        $('#aturan_pakai_id').hide();
    } else {
        $('#custom_aturan_pakai_id').hide();
        $('#aturan_pakai_id').show();
    }
}