var table_diagnosa_ri;
var table_tindakan_ri;
var table_resep_ri;
var table_daftar_obat;
var table_obat_operasi;
$(document).ready(function(){
    getTindakan();
    table_diagnosa_ri = $('#table_diagnosa_pasienri').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatinap/pasien_operasi/ajax_list_diagnosa_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      {
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_diagnosa_ri.columns.adjust().draw();


    table_tindakan_ri = $('#table_tindakan_pasien_operasi').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatinap/pasien_operasi/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_ri.columns.adjust().draw();

    table_obat_operasi = $('#table_resep_pasien_operasi').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatinap/pasien_operasi/ajax_list_resep_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_obat_operasi.columns.adjust().draw();

    
    
    // var year = new Date().getFullYear();
    // $('.datepicker_diagnosa').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });

    // $('.datepicker_tindakan').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    //  $('.datepicker_resep').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    //  $('#nama_obat').keyup(function() {
    //     var val_obat = $('#nama_obat').val();
    //     $.ajax({
    //         url: ci_baseurl + "rawatinap/pasien_operasi/autocomplete_obat",
    //         type: 'get',
    //         async: false,
    //         dataType: 'json',
    //         data: {val_obat: val_obat},
    //         success: function(data) { /* console.log(data); */
    //             var ret = data.success;
    //             if(ret == true) {
    //                 // console.log(data.data);
    //                 $(".autocomplete").autocomplete({
    //                     source: data.data,
    //                     open: function(event, ui) {
    //                         $(".ui-autocomplete").css("position: absolute");
    //                         $(".ui-autocomplete").css("top: 0");
    //                         $(".ui-autocomplete").css("left: 0");
    //                         $(".ui-autocomplete").css("cursor: default");
    //                         $(".ui-autocomplete").css("z-index","999999");
    //                         $(".ui-autocomplete").css("font-weight : bold");
    //                     },
    //                     select: function (e, ui) {
    //                         $('#obat_id').val(ui.item.id);
    //                         $('#nama_obat').val(ui.item.value);
    //                         $('#satuan_obat').val(ui.item.satuan);
    //                         $('#harga_jual').val(ui.item.harga_jual);
    //                         $('#harga_netto').val(ui.item.harga_netto);
    //                         $('#current_stok').val(ui.item.current_stok);
    //                         $('#lbl_nama_obat').addClass('active');
    //                         $('#lbl_harga_jual').addClass('active');
    //                         $('#lbl_satuan_obat').addClass('active');
    //                     }
    //               }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    //                   return $( "<li>" )
    //                     .append( "<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.deskripsi +"- Stok :"+ item.current_stok +"</font></a>" )
    //                     .appendTo( ul );
    //                 };
    //             }else{
    //               $("#harga_netto").val('');
    //               $("#current_stok").val('');
    //               $("#obat_id").val('');
    //             }
    //         }
    //     });
    // });

    $('button#saveResepOperasi').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        currentstok = $("#current_stok").val();
        qty = $("#qty_obat").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html('Silahkan pilih dokter terlebuh dahulu');
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
            });
            return false;
        }else if ($("#obat_id")=='' || $("#obat_id")==null || $("#obat_id")=='0' || $("#obat_id")=='0#') {
            $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html('Silahkan pilih obat terlebih dahulu');
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                $(".modal_notif").hide();
            });
            return false;
        }else if(parseInt(currentstok) < parseInt(qty)){
            $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html('Stok tidak mencukupi');
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                $(".modal_notif").hide();
            });
            return false;
        }else{
            swal({
                text: "Apakah data yang dimasukan sudah benar ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'tidak',
                confirmButtonText: 'Ya' 
            }).then(function(){  
                    $.ajax({
                        url: ci_baseurl + "rawatinap/pasien_operasi/do_create_resep_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateResepPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,                        
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                // $('.modal_notif2').removeClass('red').addClass('green');
                                // $('#modal_card_message2').html(data.messages);
                                // $('.notmodal_notifif2').show();
                                // $(".modal_notif2").fadeTo(2000, 500).slideUp(500, function(){
                                // $(".modal_notif2").hide().removeClass('error');
                                // });
                                $('#fmCreateResepPasien').find("input[type=number]").val("");
                                $('#fmCreateResepPasien').find("input[type=text]").val("");
                                $('#fmCreateResepPasien').find("select").prop('selectedIndex',"");
                                table_obat_operasi.columns.adjust().draw();
                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Disimpan.', 
                                    'success',
                                )
                            } else {
                                $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message2').html(data.messages);
                                $('#modal_notif2').show();
                                $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                                $("#modal_notif2").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                            }
                        }
                    });
            });
        }
    });

    $('button#saveDiagnosa').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        //console.log(dokter);
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{
            swal({
                text: "Apakah data yang dimasukan sudah benar ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'tidak',
                confirmButtonText: 'Ya' 
            }).then(function(){  
                    $.ajax({
                        url: ci_baseurl + "rawatinap/pasien_operasi/do_create_diagnosa_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                // $('.modal_notif4').removeClass('red').addClass('green');
                                // $('#modal_card_message4').html(data.messages);
                                // $('.notmodal_notifif4').show();
                                // $(".modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
                                // $(".modal_notif4").hide().removeClass('error');
                                // });
                                
                                $('.select2').prop('selectedIndex','0');
                                $(".select2").val(null).trigger("change");
                                $('#fmCreateDiagnosaPasien').find("input[type=number]").val("");
                                $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                                $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex','0');
                                $('#fmCreateDiagnosaPasien').find("select2").prop('selectedIndex','0');
                                
                                table_diagnosa_ri.columns.adjust().draw();
                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Disimpan.', 
                                    'success',
                                )
                            } else {
                                $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message4').html(data.messages);
                                $('#modal_notif4').show();
                                $("#modal_notif4").fadeTo(4000, 500).slideUp(1000, function(){
                                $("#modal_notif4").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                            }
                        }
                    });
            });
        }
    });

    $('button#saveTindakanOperasi').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        anastesi = $("#dokter_anastesi").val();
        perawat = $("#perawat").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{
            swal({
                text: "Apakah data yang dimasukan sudah benar ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'tidak',
                confirmButtonText: 'Ya' 
            }).then(function(){  
                    $.ajax({
                        url: ci_baseurl + "rawatinap/pasien_operasi/do_create_tindakan_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                // $('.modal_notif3').removeClass('red').addClass('green');
                                // $('#modal_card_message3').html(data.messages);
                                // $('.notmodal_notifif3').show();
                                // $(".modal_notif3").fadeTo(2000, 500).slideUp(500, function(){
                                // $(".modal_notif3").hide().removeClass('error');
                                // });
                                $('.select2').val(null).trigger("change");
                                $('#fmCreateTindakanPasien').find("input[type=number]").val("");
                                $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                                $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',"");
                                $('#fmCreateTindakanPasien').find("select2").prop('selectedIndex',"");
                                table_tindakan_ri.columns.adjust().draw();
                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Disimpan.', 
                                    'success',
                                )
                            } else {
                                $('#modal_notif3').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message3').html(data.messages);
                                $('#modal_notif3').show();
                                $("#modal_notif3").fadeTo(4000, 500).slideUp(1000, function(){
                                $("#modal_notif3").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                            }
                        }
                    });
            });
        }
    });
});

function getTindakan(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    var type_pembayaran = $('#type_pembayaran').val();
    $.ajax({
        url: ci_baseurl + "rawatinap/pasien_operasi/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id,type_pembayaran:type_pembayaran},
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    });
}

function getTarifTindakan(){
    var daftartindakan_id = $("#tindakan").val();
    $.ajax({
        url: ci_baseurl + "rawatinap/pasien_operasi/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {daftartindakan_id:daftartindakan_id},
        success: function(data) {
            
            $("#harga_tindakan").val(data.list.harga_tindakan);
            $("#harga_cyto").val(data.list.cyto_tindakan);
        }
    });
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();

    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $("#subtotal").val(subtotal);
    $("#totalharga").val(total);
}



function hapusDiagnosa(diagnosapasien_id){
    var jsonVariable = {};
    jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            $.ajax({
  
                  url: ci_baseurl + "rawatinap/pasien_operasi/do_delete_diagnosa",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              $('.modal_notif').removeClass('red').addClass('green');
                              $('#modal_card_message').html(data.messages);
                              $('.notmodal_notifif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              $(".modal_notif").hide().removeClass('error');
                              });
                          table_diagnosa_ri.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                              swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error',
                              )
                          }
                      }
                  });
          });
        }


    function hapusTindakan(tindakanpasien_id){
        var jsonVariable = {};
        jsonVariable["tindakanpasien_id"] = tindakanpasien_id;
        jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            $.ajax({
  
                  url: ci_baseurl + "rawatinap/pasien_operasi/do_delete_tindakan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              $('.modal_notif').removeClass('red').addClass('green');
                              $('#modal_card_message').html(data.messages);
                              $('.notmodal_notifif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              $(".modal_notif").hide().removeClass('error');
                              });
                              table_tindakan_ri.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                              swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error',
                              )
                          }
                      }
                  });
          });
        }

    function hapusResep(reseptur_id){
            var jsonVariable = {};
            jsonVariable["reseptur_id"] = reseptur_id;
            jsonVariable[csrf_name] = $('#'+csrf_name+'_delres').val();
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            $.ajax({
  
                  url: ci_baseurl + "rawatinap/pasien_operasi/do_delete_resep",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              $('.modal_notif').removeClass('red').addClass('green');
                              $('#modal_card_message').html(data.messages);
                              $('.notmodal_notifif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              $(".modal_notif").hide().removeClass('error');
                              });
                              table_obat_operasi.columns.adjust().draw();
                          swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                              swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error',
                              )
                          }
                      }
                  });
          });
        }



function dialogObat(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_daftar_obat){
        table_daftar_obat.destroy();
    }
    table_daftar_obat = $('#table_list_obat').DataTable({ 
      
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching" : true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatinap/pasien_operasi/ajax_list_obat",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1,0 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_daftar_obat.columns.adjust().draw();
}

function pilihObat(oid){
	if (oid != ""){
		$.ajax({
			url: ci_baseurl + "rawatinap/pasien_operasi/ajax_get_obat_by_id",
			type: 'get',
			dataType: 'json',
			data: { id_obat:oid },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
					$('#obat_id').val(data.data['obat_id']);
					$('#nama_obat').val(data.data['nama_obat']);
					$('#satuan_obat').val(data.data['satuan']);
					$('#harga_jual').val(data.data['harga_jual']);
					$('#harga_netto').val(data.data['harga_modal']);
					$('#current_stok').val(data.data['qtystok']);
					$('#lbl_nama_obat').addClass('active');
					$('#lbl_harga_jual').addClass('active');
					$('#lbl_satuan_obat').addClass('active');
                    $("#modal_reseptur").modal('hide');     
				} else {
					$("#harga_netto").val('');
					$("#current_stok").val('');
					$("#obat_id").val('');
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}