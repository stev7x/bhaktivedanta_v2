var table_pasienri;
$(document).ready(function(){
    table_pasienri = $('#table_list_pasienri').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatinap/pasien_ri/ajax_list_pasienri",
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.dokter_id = $("#dokter_pj").val();
            }
  
        },
        //Set column definition initialisation properties.
    });
    table_pasienri.columns.adjust().draw();
    
    $('button#saveToPenunjang').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatinap/pasien_ri/kirim_ke_penunjang",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmKirimKePenunjang').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('#modal_kirim_penunjang').modal('hide');
                            $('.modal_notif1').removeClass('red').addClass('green');
                            $('#modal_card_message1').html(data.messages);
                            $('.notmodal_notifif1').show();
                            $(".modal_notif1").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif1").hide().removeClass('error');
                            });
                            $('#fmKirimKePenunjang').find("input[type=text]").val("");
                            $('#fmKirimKePenunjang').find("select").prop('selectedIndex',"");
                        table_pasienri.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success' 
                          ) 
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        }
                    }
                });
           
          });
        
    });
    


    // $('button#saveKirimPenunjang').click(function(){
    //     swal({
    //       text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
    //       type: 'warning',
    //       showCancelButton: true,
    //       confirmButtonColor: '#3085d6',
    //       cancelButtonColor: '#d33',
    //       cancelButtonText:'Tidak',
    //       confirmButtonText: 'Ya!'
    //     }).then(function(){
    //         $.ajax({
    //                 url: ci_baseurl + "rawatinap/pasien_ri/kirim_ke_penunjang",
    //                 type: 'POST',
    //                 dataType: 'JSON',
    //                 data: $('form#fmKirimKePenunjang').serialize(),
                      
    //                 success: function(data) {
    //                     var ret = data.success;
    //                     $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
    //                     $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                       
    //                     if(ret === true) {
    //                         $('.modal_notif').removeClass('red').addClass('green');
    //                         $('#modal_card_message').html(data.messages);
    //                         $('.modal_notif').show();
    //                         $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                         $(".modal_notif").hide();
    //                         });
    //                         $('#modal_kirim_penunjang').modal('hide');
    //                         $('#fmKirimKePenunjang').find("input[type=text]").val("");
    //                         $('#fmKirimKePenunjang').find("select").prop('selectedIndex',0);
    //                         table_pasienri.columns.adjust().draw();
    //                     } else {
    //                       $('.notif').removeClass('green').addClass('red');
    //                       $('#card_message').html(data.messages);
    //                       $('.notif').show();
    //                       $(".notif").fadeTo(2000, 500).slideUp(500, function(){
    //                           $(".notif").hide();
    //                       });
    //                       console.log("masuk else");
    //                     }
    //                 }
    //             });
    //       swal(
    //         'Berhasil!',
    //         'Data Berhasil Disimpan.',
    //         'success'
    //       )
    //     })
    // });
});

function periksaPasienRi(pendaftaran_id){
    // $('#modal_periksa_pasienri').modal('show');
    var src = ci_baseurl + "rawatinap/pasien_ri/periksa_pasienri/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_periksa_pasienri iframe").attr({
        'src': src,
        'height': 400,
        'width': '100%',
        'allowfullscreen':''
    });
}


function batalRawat(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
                    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Batal Rawat Pasien Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "rawatinap/pasien_ri/do_batal_periksa",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              $('.modal_notif').removeClass('red').addClass('green');
                              $('#modal_card_message').html(data.messages);
                              $('.notmodal_notifif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              $(".modal_notif").hide().removeClass('error');
                              });
                              table_pasienri.columns.adjust().draw();
                              swal(
                                'Berhasil!',
                                'Data Berhasil Diubah.',
                                'success'
                              )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                              console.log("gagal");
                          }
                      }
                  });
           
          });
        }

function PulangkanPasienRi(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
                    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Pulangkan Pasien Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "rawatinap/pasien_ri/do_pulangkan_pasien",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                            // $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                            // $('#card_message').html(data.messages);
                            // $('#modal_notif').show();
                            // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            //     $("#modal_notif").hide();
                            // });
                            $('input[type=number]').val("");
                            $('select').prop('selectedIndex',0);
                              table_pasienri.columns.adjust().draw();
                              swal(
                                'Berhasil!',
                                'Data Berhasil Diubah.',
                                'success'
                              )
                          } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                          }
                      }
                  });
            
          });
        }

function pindahKamar(pendaftaran_id){
    // $('#modal_pindah_kamar').openModal('toggle');
    $('#pendaftaran_id').val(pendaftaran_id);
}

function getKelasPoliruangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatinap/pasien_ri/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
    getDokterRuangan();
}

function getDokterRuangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatinap/pasien_ri/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#dokter_pindah").html(data.list);
        }
    });
}

function getKamar(){
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rawatinap/pasien_ri/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
}

function savePindahKamar(){
    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya' 
      }).then(function(){
            $.ajax({
                url: ci_baseurl + "rawatinap/pasien_ri/do_pindah_kamar",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmPindahKamar').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        $('#modal_pindah_kamar').modal('hide');
                        // $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                        // $('#card_message').html(data.messages);
                        // $('#modal_notif').show();
                        // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                        //     $("#modal_notif").hide();
                        // });
                        $('input[type=number]').val("");
                        $('select').prop('selectedIndex',0);
                        $('#fmPindahKamar').find("input[type=text]").val("");
                        $('#fmPindahKamar').find("select").prop('selectedIndex',0);
                        $("#poli_ruangan").select2("val", "");
                        $("#kelas_pelayanan").select2("val", "");
                        $("#kamarruangan").select2("val", "");
                        $("#dokter_pindah").select2("val", "");
                        
                    table_pasienri.columns.adjust().draw();
                    swal( 
                        'Berhasil!',
                        'Data Berhasil Disimpan.', 
                        'success',
                      ) 
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                    }
                }
            });       
        
      });
    
}
function cariPasien(){
    table_pasienri = $('#table_list_pasienri').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatinap/pasien_ri/ajax_list_pasienri",
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.pasien_nama = $("#pasien_nama").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.lokasi = $("#lokasi").val();
            }
  
        },
        //Set column definition initialisation properties.
    });
    table_pasienri.columns.adjust().draw();
}

$(document).ready(function(){
    $('#cari_pasien').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});

function reloadTablePasien(){
    table_pasienri.columns.adjust().draw();
}

function kirimKePenunjang(pendaftaran_id){
    // $('#modal_kirim_penunjang').modal('show');
    $('#pendaftaran_id_p').val(pendaftaran_id);
}

function riwayatPenyakitPasien(pasien_id) {
    var src = ci_baseurl + "rekam_medis/list_pasien/riwayatPenyakitPasien/" + pasien_id;
    console.log(pasien_id);
    $("#modal_riwayat_penyakit_pasien iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen': ''
    });
}