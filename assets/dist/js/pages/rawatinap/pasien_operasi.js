var table_pasienri;
$(document).ready(function(){
    table_pasienri = $('#table_list_pasienri').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatinap/pasien_operasi/ajax_list_pasienri",
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.dokter_id = $("#dokter_pj").val();
            }
  
        },
        //Set column definition initialisation properties.
    });
    table_pasienri.columns.adjust().draw();
    
    // var year = new Date().getFullYear();
    // $('.datepicker').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    
    // $('button#saveKirimPenunjang').click(function(){
    //     jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
    //         if(r){
    //             $.ajax({
    //                 url: ci_baseurl + "rawatinap/pasien_operasi/kirim_ke_penunjang",
    //                 type: 'POST',
    //                 dataType: 'JSON',
    //                 data: $('form#fmKirimKePenunjang').serialize(),
    //                 success: function(data) {
    //                     var ret = data.success;
    //                     $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
    //                     $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
    //                     if(ret === true) {
    //                         $('.modal2_notif').removeClass('red').addClass('green');
    //                         $('#modal2_card_message').html(data.messages);
    //                         $('.modal2_notif').show();
    //                         $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                             $(".modal2_notif").hide();
    //                         });
    //                         $('#fmKirimKePenunjang').find("input[type=text]").val("");
    //                         $('#fmKirimKePenunjang').find("select").prop('selectedIndex',"");
    //                     table_pasienrj.columns.adjust().draw();
    //                     } else {
    //                         $('.modal2_notif').removeClass('green').addClass('red');
    //                         $('#modal2_card_message').html(data.messages);
    //                         $('.modal2_notif').show();
    //                         $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                             $(".modal2_notif").hide();
    //                         });
    //                     }
    //                 }
    //             });
    //         }
    //     });
    // });

    $('button#saveKirimPenunjang').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatinap/pasien_operasi/kirim_ke_penunjang",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmKirimKePenunjang').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('#modal_kirim_penunjang').modal('hide');
                            $('.modal_notif2').removeClass('red').addClass('green');
                            $('#modal_card_message2').html(data.messages);
                            $('.notmodal_notifif2').show();
                            $(".modal_notif2").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif2").hide().removeClass('error');
                            });
                            $('#fmKirimKePenunjang').find("input[type=text]").val("");
                            $('#fmKirimKePenunjang').find("select").prop('selectedIndex',"");
                            table_pasienri.columns.adjust().draw();
                            swal( 
                                'Berhasil!',
                                'Berhasil Dipindahkan.', 
                                'success',
                            )
                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        }
                    }
                });
          });
    });

    $('button#savePindahKamar').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatinap/pasien_operasi/do_pindah_kamar",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmPindahKamar').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('#modal_pindah_kamar').modal('hide');
                            // $('.modal_notif1').removeClass('red').addClass('green');
                            // $('#modal_card_message1').html(data.messages);
                            // $('.notmodal_notifif1').show();
                            // $(".modal_notif1").fadeTo(2000, 500).slideUp(500, function(){
                            // $(".modal_notif1").hide().removeClass('error');
                            // });
                            // $('#fmKirimKePenunjang').find("input[type=text]").val("");
                            $('#fmKirimKePenunjang').find("select").prop('selectedIndex',"");
                            table_pasienri.columns.adjust().draw();
                            swal( 
                                'Berhasil!',
                                'Berhasil Dipindahkan.', 
                                'success',
                            )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        }
                    }
                });
          });
    });



});

function periksaPasienRi(pendaftaran_id){
    $('#modal_periksa_pasienri').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    var src = ci_baseurl + "rawatinap/pasien_operasi/periksa_pasienoperasi/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_periksa_pasienri iframe").attr({
        'src': src, 
        'height': 400,
        'width': '100%',
        'allowfullscreen':''
    });
}


function batalRawat(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
swal({
    text: "Apakah Anda Yakin Ingin Membatalkan ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',
    confirmButtonText: 'Ya'
  }).then(function(){
    $.ajax({

          url: ci_baseurl + "rawatinap/pasien_operasi/do_batal_periksa",
          type: 'post',
          dataType: 'json',
          data: jsonVariable,
              success: function(data) {
                  var ret = data.success;
                  $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                  $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                  if(ret === true) {
                      $('.modal_notif').removeClass('red').addClass('green');
                      $('#modal_card_message').html(data.messages);
                      $('.notmodal_notifif').show();
                      $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                      $(".modal_notif").hide().removeClass('error');
                      });
                      table_pasienri.columns.adjust().draw();
                  swal(
                    'Berhasil!',
                    'Berhasil Dibatalkan.',
                    'success',
                  )
                  } else {
                      $('.modal_notif').removeClass('green').addClass('red');
                      $('#modal_card_message').html(data.messages);
                      $('.modal_notif').show();
                      $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                          $(".modal_notif").hide();
                      });
                      swal(
                        'Gagal!',
                        'Gagal Dibatalkan.',
                        'error',
                      )
                  }
              }
          });
  });
}

function pulangkanPasienRi(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
swal({
    text: "Apakah Anda Yakin Ingin Pulangkan ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',
    confirmButtonText: 'Ya'
  }).then(function(){
    $.ajax({

          url: ci_baseurl + "rawatinap/pasien_operasi/do_pulangkan_pasien",
          type: 'post',
          dataType: 'json',
          data: jsonVariable,
              success: function(data) {
                  var ret = data.success;
                  $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                  $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                  if(ret === true) {
                      $('.modal_notif').removeClass('red').addClass('green');
                      $('#modal_card_message').html(data.messages);
                      $('.notmodal_notifif').show();
                      $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                      $(".modal_notif").hide().removeClass('error');
                      });
                      table_pasienri.columns.adjust().draw();
                  swal(
                    'Berhasil!',
                    'Berhasil Dipulangkan.',
                    'success',
                  )
                  } else {
                      $('.modal_notif').removeClass('green').addClass('red');
                      $('#modal_card_message').html(data.messages);
                      $('.modal_notif').show();
                      $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                          $(".modal_notif").hide();
                      });
                      swal(
                        'Gagal!',
                        'Gagal Dipulangkan.',
                        'error',
                      )
                  }
              }
          });
  });
}




function pindahKamar(pendaftaran_id){
    $('#modal_pindah_kamar').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    $('#pendaftaran_id').val(pendaftaran_id); 
}

function getKelasPoliruangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatinap/pasien_operasi/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
    getDokterRuangan();
}

function getDokterRuangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatinap/pasien_operasi/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#dokter_pindah").html(data.list);
        }
    });
}

function getKamar(){
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rawatinap/pasien_operasi/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
}




function cariPasien(){
    table_pasienri = $('#table_list_pasienri').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatinap/pasien_operasi/ajax_list_pasienri",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.pasien_nama = $("#pasien_nama").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.lokasi = $("#lokasi").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_pasienri.columns.adjust().draw();
}

$(document).ready(function(){
    $('#cari_pasien').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});

function reloadTablePasien(){
    table_pasienri.columns.adjust().draw();
}

function kirimKePenunjang(pendaftaran_id){
    $('#modal_kirim_penunjang').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    }); 
    $('#pendaftaran_id_p').val(pendaftaran_id);
}