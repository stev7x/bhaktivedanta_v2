var table_poli_ruangan;
$(document).ready(function(){
    table_poli_ruangan = $('#table_poli_ruangan_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/poli_ruangan/ajax_list_poli_ruangan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_poli_ruangan.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#importData').click(function (e) {
        var data = new FormData($(this)[0]);
        var input = document.getElementById('file');
        var property = input.files[0];
        var form_data = new FormData();
        form_data.append("file", property);
        swal({
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "masterdata/poli_ruangan/upload",
                data: form_data,
                method: 'POST',
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_import]').val(data.csrfHash);
                        swal(
                            'Berhasil!',
                            'Data Berhasil Diimport.',
                            'success'
                        )
                        table_poli_ruangan.columns.adjust().draw();

                }
            });

        })
    });

    $('button#savePoliRuangan').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/poli_ruangan/do_create_poli_ruangan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreatePoliRuangan').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_poli_ruangan").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreatePoliRuangan').find("input[type=text]").val("");
                            $('#fmCreatePoliRuangan').find("select").prop('selectedIndex',0);
                            table_poli_ruangan.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                            swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

    $('button#changePoliRuangan').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/poli_ruangan/do_update_poli_ruangan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdatePoliRuangan').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_poli_ruangan").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmUpdatePoliRuangan').find("input[type=text]").val("");
                            $('#fmUpdatePoliRuangan').find("select").prop('selectedIndex',0);
                            table_poli_ruangan.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                            swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                          console.log("masuk else");
                        }
                    }
                });
         
        })
    });
});

function editPoliRuangan(poliruangan_id){
    if(poliruangan_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/poli_ruangan/ajax_get_poli_ruangan_by_id",
        type: 'get',
        dataType: 'json',
        data: {poliruangan_id:poliruangan_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_poli_ruangan').val(data.data.poliruangan_id);
                    $('#upd_nama_poli_ruangan').val(data.data.nama_poliruangan);
                    // $('#upd_select_instalasi option[value='+data.data.instalasi_id+']').attr('selected','selected');
                    $('#upd_select_instalasi').val(data.data.instalasi_id);
                    $('#upd_select_instalasi').select();
                    $('#upd_select_ruangan').val(data.data.type_ruangan);
                    $('#upd_select_ruangan').select();
                    $('#label_upd__nama_poli_ruangan').addClass('active');
                    $('#upd_nama_singkatan').val(data.data.nama_singkatan);       
                    $('#modal_update_poli_ruangan').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}


function hapusPoliRuangan(poliruangan_id){
    var jsonVariable = {};
    jsonVariable["poliruangan_id"] = poliruangan_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "masterdata/poli_ruangan/do_delete_poli_ruangan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_poli_ruangan.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }


function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/poli_ruangan/exportToExcel";
}

function showbtnimport() {
    var value = $('#file').val();
    console.log('isi : ' + value);
    if (value != '') {
        console.log('masuk if');
        $('#importData').fadeIn();
    }
} 