var table_kelurahan;
$(document).ready(function(){
    table_kelurahan = $('#table_kelurahan_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/kelurahan/ajax_list_kelurahan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_kelurahan.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#saveKelurahan').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/kelurahan/do_create_kelurahan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateKelurahan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateKelurahan').find("input[type=text]").val("");
                            $('#fmCreateKelurahan').find("select").prop('selectedIndex',0);
                            $('#modal_add_kelurahan').modal('hide');
                        table_kelurahan.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.', 
                            'success',
                          )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
             
          });
        
    });
    
    $('button#changeKelurahan').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){ 
                $.ajax({
                    url: ci_baseurl + "masterdata/kelurahan/do_update_kelurahan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateKelurahan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            //$('#fmCreateKelurahan').find("input[type=text]").val("");
                            //$('#fmCreateKelurahan').find("select").prop('selectedIndex',0);
                            $('#modal_update_kelurahan').modal('hide');

                        table_kelurahan.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Diperbarui.', 
                            'success'
                        ) 
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
            
          });
    });
});

function addkelurahan(){
    $('#modal_add_kelurahan').modal('show');
} 

function editKelurahan(kelurahan_id){
    if(kelurahan_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/kelurahan/ajax_get_kelurahan_by_id",
        type: 'get',
        dataType: 'json',
        data: {kelurahan_id:kelurahan_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_kelurahan').val(data.data.kelurahan_id);
                    $('#upd_nama_kelurahan').val(data.data.kelurahan_nama);
                    // $('#upd_select_kecamatan option[value='+data.data.kecamatan_id+']').attr('selected','selected');
                    $('#upd_select_kecamatan').val(data.data.kecamatan_id);
                    $('#upd_select_kecamatan').select2(); 
                    $('#modal_update_kelurahan').modal('show'); 
                    // $('#label_upd__nama_kelurahan').addClass('active');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}
function hapusKelurahan(kelurahan_id){
    var jsonVariable = {};
    jsonVariable["kelurahan_id"] = kelurahan_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(kelurahan_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/kelurahan/do_delete_kelurahan",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                            if(ret === true) {
                                $('.notif').removeClass('red').addClass('green');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            table_kelurahan.columns.adjust().draw();
                            } else {
                                $('.notif').removeClass('green').addClass('red');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            }
                        }
                });
            swal( 
              'Berhasil!',
              'Data Berhasil Dihapus.', 
              'success',
            ) 
          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}