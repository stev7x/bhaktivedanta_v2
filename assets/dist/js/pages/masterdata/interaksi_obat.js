var table_interaksi_obat;
$(document).ready(function(){
    table_interaksi_obat = $('#table_interaksi_obat_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/interaksi_obat/ajax_list_interaksi_obat",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_interaksi_obat.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#saveInteraksi').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/interaksi_obat/do_create_interaksi_obat",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateInteraksi').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_interaksi_obat").modal('hide');
                            // $('.modal_notif').removeClass('red').addClass('green');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();
                            // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            //     $(".modal_notif").hide();
                            // });
                            $('#fmCreateInteraksi').find("input[type=text]").val("");
                            $('#fmCreateInteraksi').find("select").prop('selectedIndex',0);
                            table_interaksi_obat.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

    $('button#changeInteraksi').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/interaksi_obat/do_update_interaksi_obat",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateInteraksi').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_interaksi_obat").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmUpdateInteraksi').find("input[type=text]").val("");
                            $('#fmUpdateInteraksi').find("select").prop('selectedIndex',0);
                            table_interaksi_obat.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });
});




function editInteraksi(id_interaksi){
    if(id_interaksi){    
        $.ajax({
        url: ci_baseurl + "masterdata/interaksi_obat/ajax_get_interaksi_obat_by_id",
        type: 'get',
        dataType: 'json',
        data: {id_interaksi:id_interaksi},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_interaksi').val(data.data.id_interaksi);
                    $('#upd_kode_interaksi').val(data.data.kode_interaksi);
                    $('#upd_nama_interaksi').val(data.data.nama_interaksi);
                    $('#upd_keterangan').val(data.data.keterangan);
                    // $('#modal_update_agama').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusInteraksi(id_interaksi){
    var jsonVariable = {};
    jsonVariable["id_interaksi"] = id_interaksi;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "masterdata/interaksi_obat/do_delete_interaksi_obat",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_interaksi_obat.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }
