var table_list_bahasa;
$(document).ready(function(){

	table_list_bahasa = $('#table_bahasa_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.

      "ajax": {
          "url": ci_baseurl + "masterdata/bahasa/ajax_list_bahasa",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_list_bahasa.columns.adjust().draw();

});


function openBahasa(){
    $('#form_judul').text("Tambah Data Bahasa");
    $('.form_aksi').removeAttr("id");
    $('.form_aksi').attr("id","fmCreateBahasa");
    $('.aksi_button').attr("onclick","saveBahasa()");
    $('#bahasa').val("");

}

function saveBahasa(){
	 swal({
          text: "Apakah data yang anda masukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){
            $.ajax({
                url : ci_baseurl + "masterdata/bahasa/do_create_bahasa",
                type : 'POST',
                dataType : 'JSON',
                data : $('form#fmCreateBahasa').serialize(),
                success : function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if (ret === true) {
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.', 
                        );
                        $('#modal_bahasa').modal('hide');
                        $('input[type=text]').val("");
                        table_list_bahasa.columns.adjust().draw();

                    }else{
                        console.log("data gagal ditambahkan");
                        
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                          $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");

                    }
                }
            });
        }); 
}

function getEditBahasa(bahasa_id){
	$.ajax({
      url: ci_baseurl + "masterdata/bahasa/ajax_get_bahasa_by_id",
      type: 'get',
      dataType: 'json',
      data: {bahasa_id:bahasa_id},
      success: function(data){
        var ret = data.success;
        if (ret === true) {
          console.log("test");
          $('#form_judul').text("Edit Data Bahasa");
          $('.form_aksi').removeAttr("id");
          $('.form_aksi').attr("id","fmEditBahasa");
          $('.aksi_button').attr("onclick","editBahasa()");
          $('#bahasa_id').val(data.data.bahasa_id);
          $('#bahasa').val(data.data.bahasa);

        }else{
          console.log("Data tidak ditemukan");
        }
      } 
    });
}

function editBahasa(){
	swal({
          text: "Apakah data yang anda masukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){
            $.ajax({
                url       : ci_baseurl + "masterdata/bahasa/do_update_bahasa",
                type      : 'POST',
                dataType  : 'JSON',
                data      : $('form#fmEditBahasa').serialize(),
                success   : function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);

                    if (ret === true) {
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.', 
                        );
                        $('#modal_bahasa').modal('hide');
                        table_list_bahasa.columns.adjust().draw();

                    }else{
                        console.log("data gagal ditambahkan");
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages); 
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                    }
                }
            });
        }); 
}


function hapusBahasa(bahasa_id){
  var jsonVariable = {};
      jsonVariable["bahasa_id"] = bahasa_id;
      jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();

  swal({
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya' 
      }).then(function(){  
          $.ajax({
              url: ci_baseurl + "masterdata/bahasa/do_delete_bahasa",
              type: 'post',
              dataType: 'json',
              data: jsonVariable,
              success : function(data){
                  var ret = data.success;
                  $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                  $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);

                  if(ret === true){
                      swal( 
                          'Berhasil!',
                          'Data Berhasil Dihapus.', 
                      );
                      table_list_bahasa.columns.adjust().draw();

                  }else{
                      console.log("gagal");
                  }
              }
          })
      });
}

function exportToExcel(){ 
    console.log('masukexport');
      window.location.href = ci_baseurl + "masterdata/bahasa/exportToExcel";
  }