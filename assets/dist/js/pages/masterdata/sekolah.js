var table_list_sekolah;
$(document).ready(function(){

	table_list_sekolah = $('#table_list_sekolah').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.

      "ajax": {
          "url": ci_baseurl + "masterdata/sekolah/ajax_list_sekolah",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_list_sekolah.columns.adjust().draw();

});


function openSekolah(){
    $('#form_judul').text("Tambah Data Sekolah");
    $('.form_aksi').removeAttr("id");
    $('.form_aksi').attr("id","fmCreateSekolah");
    $('.aksi_button').attr("onclick","saveSekolah()");
    $('#sekolah').val("");

}

function saveSekolah(){
	 swal({
          text: "Apakah data yang anda masukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){
            $.ajax({
                url : ci_baseurl + "masterdata/sekolah/do_create_sekolah",
                type : 'POST',
                dataType : 'JSON',
                data : $('form#fmCreateSekolah').serialize(),
                success : function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if (ret === true) {
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.', 
                        );
                        $('#modal_sekolah').modal('hide');
                        $('input[type=text]').val("");
                        table_list_sekolah.columns.adjust().draw();

                    }else{
                        console.log("data gagal ditambahkan");
                        
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                          $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");

                    }
                }
            });
        }); 
}

function getEditSekolah(sekolah_id){
	$.ajax({
      url: ci_baseurl + "masterdata/sekolah/ajax_get_sekolah_by_id",
      type: 'get',
      dataType: 'json',
      data: {sekolah_id:sekolah_id},
      success: function(data){
        var ret = data.success;
        if (ret === true) {
          console.log("test");
          $('#form_judul').text("Edit Data Sekolah");
          $('.form_aksi').removeAttr("id");
          $('.form_aksi').attr("id","fmEditSekolah");
          $('.aksi_button').attr("onclick","editSekolah()");
          $('#sekolah_id').val(data.data.sekolah_id);
          $('#sekolah').val(data.data.sekolah_nama);
          $('#sekolah_alamats').val(data.data.sekolah_alamat);
          $('#email').val(data.data.email);
          $('#telp').val(data.data.telp);
          $('#web').val(data.data.web);

        }else{
          console.log("Data tidak ditemukan");
        }
      } 
    });
}

function editSekolah(){
	swal({
          text: "Apakah data yang anda masukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){
            $.ajax({
                url       : ci_baseurl + "masterdata/sekolah/do_update_sekolah",
                type      : 'POST',
                dataType  : 'JSON',
                data      : $('form#fmEditSekolah').serialize(),
                success   : function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);

                    if (ret === true) {
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.', 
                        );
                        $('#modal_sekolah').modal('hide');
                        table_list_sekolah.columns.adjust().draw();

                    }else{
                        console.log("data gagal ditambahkan");
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages); 
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                    }
                }
            });
        }); 
}


function hapusSekolah(sekolah_id){
  var jsonVariable = {};
      jsonVariable["sekolah_id"] = sekolah_id;
      jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();

  swal({
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya' 
      }).then(function(){  
          $.ajax({
              url: ci_baseurl + "masterdata/sekolah/do_delete_sekolah",
              type: 'post',
              dataType: 'json',
              data: jsonVariable,
              success : function(data){
                  var ret = data.success;
                  $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                  $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);

                  if(ret === true){
                      swal( 
                          'Berhasil!',
                          'Data Berhasil Dihapus.', 
                      );
                      table_list_sekolah.columns.adjust().draw();

                  }else{
                      console.log("gagal");
                  }
              }
          })
      });
}

function exportToExcel(){ 
    console.log('masukexport');
      window.location.href = ci_baseurl + "masterdata/sekolah/exportToExcel";
  }