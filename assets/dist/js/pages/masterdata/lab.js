var table_list_lab;
$(document).ready(function () {

    table_list_lab = $('#table_lab_list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        "ajax": {
            "url": ci_baseurl + "masterdata/lab/ajax_list_lab",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [-1, 0], //last column
                "orderable": false //set not orderable
            }
        ]

    });
    table_list_lab.columns.adjust().draw();

});


function openLab() {
    $('#form_judul').text("Tambah Data lab");
    $('.form_aksi').removeAttr("id");
    $('.form_aksi').attr("id", "fmCreateLab");
    $('.aksi_button').attr("onclick", "saveLab()");
    // $('#lab').val("");

}

function saveLab() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/lab/do_create_lab",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmCreateLab').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                        'success'
                    );
                    $('#modal_lab').modal('hide');
                    $('input[type=text]').val("");
                    $('input[type=number]').val("");
                    table_list_lab.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");

                    $('#modal_notif_lab').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message_lab').html(data.messages);
                    $('#modal_notif_lab').show();
                    $("#modal_notif_lab").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif_lab").hide();
                    });
                    $("html, modal-content").animate({ scrollTop: 100 }, "fast");

                }
            }
        });
    });
}

function getEditLab(lab_id) {
    $.ajax({
        url: ci_baseurl + "masterdata/lab/ajax_get_lab_by_id",
        type: 'get',
        dataType: 'json',
        data: { lab_id: lab_id },
        success: function (data) {
            var ret = data.success;
            if (ret === true) {
                console.log("test");
                $('#form_judul').text("Edit Data lab");
                $('.form_aksi').removeAttr("id");
                $('.form_aksi').attr("id", "fmEditLab");
                $('.aksi_button').attr("onclick", "editLab()");
                $('#lab_id').val(data.data.lab_id);
                $('#nama_pemeriksaan').val(data.data.nama_pemeriksaan);
                $('#jenis').val(data.data.jenis);
                $('#metode_pemeriksaan').val(data.data.metode);
                $('#satuan').val(data.data.satuan);
                $('#tarif').val(data.data.tarif);
                $('#terendah_per').val(data.data.terendah_per);
                $('#terendah_laki').val(data.data.terendah_laki);
                $('#tertinggi_per').val(data.data.tertinggi_per);
                $('#tertinggi_laki').val(data.data.tertinggi_laki);
                $('#terendah_per_anak').val(data.data.terendah_per_anak);
                $('#terendah_laki_anak').val(data.data.terendah_laki_anak);
                $('#tertinggi_per_anak').val(data.data.tertinggi_per_anak);
                $('#tertinggi_laki_anak').val(data.data.tertinggi_laki_anak);
                $('#tarif').val(data.data.tarif);
                $('#ket').val(data.data.keterangan);
                $('#type').select();

            } else {
                console.log("Data tidak ditemukan");
            }
        }
    });
}

function editLab() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/lab/do_update_lab",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmEditLab').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                $('input[number=' + data.csrfTokenName + ']').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                        'success'
                    );
                    $('#modal_lab').modal('hide');
                    table_list_lab.columns.adjust().draw();

                } else {
                    $('#modal_notif_lab').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message_lab').html(data.messages);
                    $('#modal_notif_lab').show();
                    $("#modal_notif_lab").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif_lab").hide();
                    });
                    $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                }
            }
        });
    });
}


function hapuslab(lab_id) {
    var jsonVariable = {};
    jsonVariable["lab_id"] = lab_id;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_delda').val();

    swal({
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/lab/do_delete_lab",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Dihapus.',
                    );
                    table_list_lab.columns.adjust().draw();

                } else {
                    console.log("gagal");
                }
            }
        })
    });
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/lab/exportToExcel";
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}