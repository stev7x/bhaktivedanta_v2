var table_jadwaldokter;
$(document).ready(function(){  
    table_jadwaldokter = $('#table_jadwaldokter_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,
 
      // Load data for the table's content from an Ajax source 
      "ajax": {
          "url": ci_baseurl + "masterdata/jadwal_dokter/ajax_list_jadwaldokter",
          "type": "GET"
      }, 
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]   

    });  
    // table_jadwaldokter.columns.adjust().draw();
    getBranch();
});

  function saveJadwal(){ 
    swal({
      text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
      type: 'warning',
      showCancelButton: true, 
      confirmButtonColor: '#3085d6',  
      cancelButtonColor: '#d33',
      cancelButtonText:'Tidak',
      confirmButtonText: 'Ya!' 
    }).then(function(){
        $.ajax({
                url: ci_baseurl + "masterdata/jadwal_dokter/do_create_jadwal",
                type: 'POST',       
                dataType: 'JSON',
                data: $('form#fmJadwalDokter').serialize(),  
                  
                success: function(data) {
                    var ret = data.success;   
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                    
                    if(ret === true) {
                        $("#modal_jadwal").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmJadwalDokter').find("input[type=text]").val("");
                        $('#fmJadwalDokter').find("select").prop('selectedIndex',0);
                        table_jadwaldokter.columns.adjust().draw(); 
                        swal(
                            'Berhasil!', 
                            'Data Berhasil Disimpan.',
                            'success'
                          )
                    } else {   
                        $('.modal_notif').removeClass('green').addClass('red');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();   
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        }); 
                        swal(
                            'Gagal!',
                            data.messages,
                            'error'
                          )
                      console.log("masuk else");
                    }
                }
            });  
      
    }) 

  }
    
function tambahJadwal(){                  
  document.getElementById("fmJadwalDokter").reset();    
  $("#dokter_id").select2('destroy');
  $("#dokter_id").select2();
  $('#modal_jadwal').modal('show');        
  $('#myLargeModalLabel').text('Tambah Jadwal Dokter');    
  $('#btnJadwalDokter').removeAttr('onclick'); 
  $('#btnJadwalDokter').attr('onclick','saveJadwal()');   

}
 

function editJadwal(jadwal_id){  
    if(jadwal_id){        
        $.ajax({
        url: ci_baseurl + "masterdata/jadwal_dokter/ajax_get_jadwaldokter_by_id",
        type: 'get',
        dataType: 'json',  
        data: {jadwal_id:jadwal_id},
            success: function(data) { 
                var ret = data.success; 
                if(ret === true) {    
                    console.log(data.data); 
                    $('#dokter_id').val(data.data.dokter_id);  
                    $('#dokter_id').select2();         
                    $('#detail_jadwal_id').val(data.data.detail_jadwal_id);
                    $('#jam_mulai').val(data.data.jam_mulai);     
                    $('#jam_selesai').val(data.data.jam_selesai);          
                    $('#jadwal_dokter_id').val(data.data.jadwal_dokter_id);  
                    $('#kuota_pasien').val(data.data.kuota);  
                    $('select[name="id_branch"]').val(data.data.id_branch);      
                    $('#myLargeModalLabel').text('Edit Jadwal Dokter');    
                    $('#btnJadwalDokter').removeAttr('onclick');
                    $('#btnJadwalDokter').attr('onclick','do_edit_jadwal('+jadwal_id+')');

                    $('#modal_jadwal').modal('show');   
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}
function do_edit_jadwal(id){  
   swal({ 
      text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
      type: 'warning',
      showCancelButton: true, 
      confirmButtonColor: '#3085d6',  
      cancelButtonColor: '#d33',
      cancelButtonText:'Tidak',
      confirmButtonText: 'Ya!'
    }).then(function(){  
        $.ajax({          
                url: ci_baseurl + "masterdata/jadwal_dokter/do_update_jadwal/"+id,
                type: 'POST',       
                dataType: 'JSON', 
                data: $('form#fmJadwalDokter').serialize(),  
                  
                success: function(data) {  
                    var ret = data.success;   
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                   
                    if(ret === true) {
                        $("#modal_jadwal").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmJadwalDokter').find("input[type=text]").val("");
                        $('#fmJadwalDokter').find("select").prop('selectedIndex',0);
                        table_jadwaldokter.columns.adjust().draw(); 
                        swal(
                            'Berhasil!', 
                            'Data Berhasil Disimpan.',
                            'success'
                          )
                    } else {   
                        $('.modal_notif').removeClass('green').addClass('red');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();   
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        }); 
                        swal(
                            'Gagal!',
                          'Data Gagal Disimpan.',
                            'error'
                          )
                      console.log("masuk else");
                    }
                }
            });  
      
    })
}
 
function hapusJadwal(jadwal_id){   
  var jsonVariable = {};        
  jsonVariable["jadwal_id"] = jadwal_id;
  jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
  console.log("masuk fungsi");
  swal({
    text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',
    confirmButtonText: 'Ya'   
  }).then(function(){
      $.ajax({  
          url: ci_baseurl + "masterdata/jadwal_dokter/do_delete_jadwal",
          type: 'post',
          dataType: 'json',
          data: jsonVariable,
              success: function(data) {
                  var ret = data.success;
                  $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                  $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                   
                  if(ret === true) {
                    $('.notif').removeClass('red').addClass('green');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    }); 
                    swal(
                      'Berhasil!',
                      'Data Berhasil Dihapus.',
                      'success',
                    )           
                    table_jadwaldokter.columns.adjust().draw(); 
                  } else {
                    $('.notif').removeClass('green').addClass('red');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                      console.log("gagal");
                  }
              }
          });
    
  });
} 
function getBranch(){
    $.ajax({
        url: ci_baseurl + "masterdata/jadwal_dokter/get_branch_dokter",
        type: 'GET',
        dataType: 'JSON',
        data: {
                        
        },
        success: function (data) {
            $("select[name='id_branch']").html(data.list);
        }
    });
}
// function importExcel(){
//   var data = new FormData();
//   jQuery.each(jQuery('#file')[0].files, function(i, file) {
//       data.append('file-'+i, file);
//   });
//   swal({
//     text: 'Apakah file import yang dimasukan sudah benar ?',
//     type: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     cancelButtonText:'Tidak',
//     confirmButtonText: 'Ya!'
//   }).then(function(){
//       $.ajax({
//               url: ci_baseurl + "masterdata/agama/upload",
//               // enctype: 'multipart/form-data',
//               type: 'POST',
//               dataType: 'JSON',
//               cache: false,  
//               contentType: false,         
//               data: $('form#frmImport').serialize(),
                
//               success: function(data) {  
//                   var ret = data.success;
//                   $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
//                   $('input[name='+data.csrfTokenName+'_import]').val(data.csrfHash);
                 
//                   if(ret === true) {
//                       table_agama.columns.adjust().draw();
//                       swal(
//                           'Berhasil!',
//                           'Data Berhasil Diimport.',
//                           'success'
//                         )
//                   } else {

//                       swal(
//                           'Gagal!',
//                           'Data Gagal Disimpan.',
//                           'error'
//                         )
//                     console.log("masuk else");
//                   }
//               }
//           });
    
//   })
// }


   