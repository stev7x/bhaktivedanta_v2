var table_list_diagnosa;
$(document).ready(function () {

    table_list_diagnosa = $('#table_list_diagnosa').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        "ajax": {
            "url": ci_baseurl + "masterdata/list_diagnosa/ajax_list_diagnosa",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [-1, 0], //last column
                "orderable": false //set not orderable
            }
        ]

    });
    table_list_diagnosa.columns.adjust().draw();

});


function openDiagnosa() {
    $('#form_judul').text("Tambah Data Diagnosa");
    $('.form_aksi').removeAttr("id");
    $('.form_aksi').attr("id", "fmCreateDiagnosa");
    $('.aksi_button').attr("onclick", "saveDiagnosa()");
    $('#diagnosa_kode').val("");
    $('#diagnosa_nama').val("");
    $('#kode_diagnosa_icd').val('').trigger('change');

}

function saveDiagnosa() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/list_diagnosa/do_create_diagnosa",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmCreateDiagnosa').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                    );
                    $('#modal_diagnosa').modal('hide');
                    $('input[type=text]').val("");
                    $('#kode_diagnosa_icd').val('').trigger('change');
                    
                    table_list_diagnosa.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");
                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif").hide();
                    });
                    $("html, modal-content").animate({ scrollTop: 100 }, "fast");

                }
            }
        });
    });
}

function getEditDiagnosa(diagnosa2_id) {
    $.ajax({
        url: ci_baseurl + "masterdata/list_diagnosa/ajax_get_diagnosa_by_id",
        type: 'get',
        dataType: 'json',
        data: { diagnosa2_id: diagnosa2_id },
        success: function (data) {
            var ret = data.success;
            if (ret === true) {
                $('#form_judul').text("Edit Data Diagnosa");
                $('.form_aksi').removeAttr("id");
                $('.form_aksi').attr("id", "fmEditDiagnosa");
                $('.aksi_button').attr("onclick", "editDiagnosa()");
                $('#diagnosa2_id').val(data.data.diagnosa2_id);
                $('#diagnosa_kode').val(data.data.diagnosa_kode);
                $('#diagnosa_nama').val(data.data.diagnosa_nama);
                $('#kode_diagnosa_icd').val(data.data.diagnosa_id);
                $('#kode_diagnosa_icd').select2();
            } else {
                console.log("Data tidak ditemukan");
            }
        }
    });
}

function editDiagnosa() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/list_diagnosa/do_update_diagnosa",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmEditDiagnosa').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                    );
                    $('#modal_diagnosa').modal('hide');
                    table_list_diagnosa.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");
                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif").hide();
                    });
                }
            }
        });
    });
}


function hapusDiagnosa(diagnosa2_id) {
    var jsonVariable = {};
    jsonVariable["diagnosa2_id"] = diagnosa2_id;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_delda').val();

    swal({
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/list_diagnosa/do_delete_diagnosa",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Dihapus.',
                    );
                    table_list_diagnosa.columns.adjust().draw();

                } else {
                    console.log("gagal");
                }
            }
        })
    });
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/sekolah/exportToExcel";
}