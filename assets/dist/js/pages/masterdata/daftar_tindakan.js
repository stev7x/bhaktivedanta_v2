var table_daftar_tindakan;
var bhpListArray = [], 
    komponentarifNamaArray = [],
    bhpListArrayEdit = [],
    komponentarifNamaArrayEdit = [];

$(document).ready(function(){

  $("#pilih_bhp").change(function () {
        pilih_bhp = $("#pilih_bhp option:selected").text();
        bhp_name = pilih_bhp.split("-");
        $("#nama_barang_bhp").val(bhp_name[1]);
    });

    $("#pilih_bhp_edit").change(function () {
        pilih_bhp = $("#pilih_bhp_edit option:selected").text();
        bhp_name = pilih_bhp.split("-");
        $("#nama_barang_bhp_edit").val(bhp_name[1]);
    });

    $('button#saveBHP').click(function () {
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {

            let
                pilih_bhp = $("#pilih_bhp").val(),
                nama_barang_bhp = $("#nama_barang_bhp").val(),
                jumlah_bhp = $("#jumlah_bhp").val(),
                satuan_bhp = $("#satuan_bhp").val();

            if (pilih_bhp == "0" || nama_barang_bhp == "" || nama_barang_bhp == null || jumlah_bhp <= 0 || satuan_bhp == "0") {
                console.log("adc");
                $("#modal_notif_bhp").show();
                $('#modal_notif_bhp').removeClass('alert-success').addClass('alert-danger');
                $('#card_message_bhp').html("Harap isi atau periksa seluruh form");
                $("#modal_notif_bhp").fadeTo(2000, 500).slideUp(500, function () {
                    $("#modal_notif_bhp").hide();
                });
            } else {
                bhpListArray.push({
                    pilih_bhp: {
                        id: pilih_bhp,
                        name: $("#pilih_bhp option:selected").text()
                    },
                    nama_barang_bhp: nama_barang_bhp,
                    jumlah_bhp: jumlah_bhp,
                    satuan_bhp: {
                        id: satuan_bhp,
                        name: $("#satuan_bhp option:selected").text()
                    }
                });
                updateTableDataBhpAlkes();

                console.log(bhpListArray[0].pilih_bhp.name);
                console.log(nama_barang_bhp);
                console.log(jumlah_bhp);
                console.log(satuan_bhp);
            }
            $("#pilih_bhp").prop('selectedIndex', 0);
            $("#nama_barang_bhp").val("");
            $("#jumlah_bhp").val("");
            $("#satuan_bhp").prop('selectedIndex', 0);

        })
    });

    $('button#saveBHPEdit').click(function () {
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {

            let
                pilih_bhp = $("#pilih_bhp_edit").val(),
                nama_barang_bhp = $("#nama_barang_bhp_edit").val(),
                jumlah_bhp = $("#jumlah_bhp_edit").val(),
                satuan_bhp = $("#satuan_bhp_edit").val();

            if (pilih_bhp == "0" || nama_barang_bhp == "" || nama_barang_bhp == null || jumlah_bhp <= 0 || satuan_bhp == "0") {
                console.log("adc");
                $("#modal_notif_bhp_edit").show();
                $('#modal_notif_bhp_edit').removeClass('alert-success').addClass('alert-danger');
                $('#card_message_bhp_edit').html("Harap isi atau periksa seluruh form");
                $("#modal_notif_bhp_edit").fadeTo(2000, 500).slideUp(500, function () {
                    $("#modal_notif_bhp_edit").hide();
                });
            } else {
                bhpListArrayEdit.push({
                    pilih_bhp: {
                        id: pilih_bhp,
                        name: $("#pilih_bhp_edit option:selected").text()
                    },
                    nama_barang_bhp: nama_barang_bhp,
                    jumlah_bhp: jumlah_bhp,
                    satuan_bhp: {
                        id: satuan_bhp,
                        name: $("#satuan_bhp_edit option:selected").text()
                    }
                });
                updateTableDataBhpAlkesEdit();

                console.log(bhpListArrayEdit[0].pilih_bhp.name);
                console.log(nama_barang_bhp);
                console.log(jumlah_bhp);
                console.log(satuan_bhp);
            }
            $("#pilih_bhp_edit").prop('selectedIndex', 0);
            $("#nama_barang_bhp_edit").val("");
            $("#jumlah_bhp_edit").val("");
            $("#satuan_bhp_edit").prop('selectedIndex', 0);

        })
    });

    table_daftar_tindakan = $('#table_daftar_tindakan_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,
 
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/daftar_tindakan/ajax_list_daftar_tindakan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_daftar_tindakan.columns.adjust().draw();
    

    $('.tooltipped').tooltip({delay: 50});

    $('button#importData').click(function(e){  
      var data = new FormData($(this)[0]);    
      var input = document.getElementById('file');
      var property = input.files[0];          
      var form_data = new FormData();
      form_data.append("file",property);  

          swal({    
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true, 
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',  
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
          }).then(function(){            
              $.ajax({     
                      url: ci_baseurl + "masterdata/daftar_tindakan/upload",  
                      data: form_data,                
                      method: 'POST',  
                      type: 'POST',     
                      processData: false,
                      contentType: false,  
                      cache:false,  
                      success: function(data) {  
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_import]').val(data.csrfHash);
                         
                              table_daftar_tindakan.columns.adjust().draw();   
                              swal(
                                  'Berhasil!',
                                  'Data Berhasil Diimport.',
                                  'success'
                                )
                           
                      } 
                  });
            
          })
    });


    $('button#saveDaftarTindakan').click(function(){
        let daftarBHP = JSON.stringify(bhpListArray);
        let daftarKomponentTarifNama = JSON.stringify(komponentarifNamaArray);
        console.log(daftarKomponentTarifNama)
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/daftar_tindakan/do_create_daftar_tindakan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateTindakan').serialize() + 
                            "&bhpArray=" + daftarBHP + 
                            "&komponenTarifNama=" + daftarKomponentTarifNama,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_daftar_tindakan").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateTindakan').find("input[type=text]").val("");
                            $('#fmCreateTindakan').find("select").prop('selectedIndex',0);
                            
                            table_daftar_tindakan.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })    
    });
 
    

    function reloadTableDaftarTindakan(){
        table_daftar_tindakan.columns.adjust().draw();
    }

    $('button#changeDaftarTindakan').click(function(){
        let daftarBHP = JSON.stringify(bhpListArrayEdit);
        let daftarKomponentTarifNama = JSON.stringify(komponentarifNamaArrayEdit);
        console.log(daftarKomponentTarifNama)
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33', 
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/daftar_tindakan/do_update_daftar_tindakan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateTindakan').serialize() + 
                    "&bhpArray=" + daftarBHP + 
                    "&komponenTarifNama=" + daftarKomponentTarifNama,
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_daftar_tindakan").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmUpdateTindakan').find("input[type=text]").val("");
                            $('#fmUpdateTindakan').find("select").prop('selectedIndex',0);
                            table_daftar_tindakan.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
         
        })
    });


});
 
  
function showbtnimport(){
      var value = $('#file').val();
      console.log('isi : '+value);
      if(value != ''){   
        console.log('masuk if');                 
        $('#importData').fadeIn();  
      }
} 

function editDaftarTindakan(daftartindakan_id){
    if(daftartindakan_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/daftar_tindakan/ajax_get_daftar_tindakan_by_id",
        type: 'get',
        dataType: 'json',
        data: {daftartindakan_id:daftartindakan_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data); 
                    let daftar_tindakan = data.data.daftar_tindakan,
                        daftar_tindakan_bhp = data.data.daftar_tindakan_bhp,
                        daftar_tindakan_komponentarif = data.data.daftar_tindakan_komponen_tarif;
                    $('#upd_id_daftar_tindakan').val(daftar_tindakan.daftartindakan_id);
                    $('#upd_nama_daftar_tindakan').val(daftar_tindakan.daftartindakan_nama);
                    $('#upd_kelompoktindakan').val(daftar_tindakan.kelompoktindakan_id);
                    $('#upd_kelompoktindakan').select(); 
                    $('#upd_kelaspelayanan_nama').val(daftar_tindakan.kelaspelayanan_id);
                    $('#upd_kelaspelayanan_nama').select();   
                    $("#upd_icd9_cm_id").val(daftar_tindakan.icd9_cm_id);
                    $("#upd_icd9_cm_id").select();

                    $("#total_bpjs_cyto_edit").text(formatAsRupiah2(daftar_tindakan.total_harga_bpjs_cyto.toString()))
                    $("#total_bpjs_non_cyto_edit").text(formatAsRupiah2(daftar_tindakan.total_harga_bpjs_non_cyto.toString()))
                    $("#total_umum_cyto_edit").text(formatAsRupiah2(daftar_tindakan.total_harga_umum_cyto.toString()))
                    $("#total_umum_non_cyto_edit").text(formatAsRupiah2(daftar_tindakan.total_harga_umum_non_cyto.toString()))

                    for (let i = 0; i < daftar_tindakan_bhp.length; i++){
                        bhpListArrayEdit.push({
                            pilih_bhp: {
                                id: daftar_tindakan_bhp[i].barang_farmasi_id,
                                name: daftar_tindakan_bhp[i].kode_barang + "-" + daftar_tindakan_bhp[i].nama_barang
                            },
                            nama_barang_bhp: daftar_tindakan_bhp[i].nama_barang,
                            jumlah_bhp: daftar_tindakan_bhp[i].jumlah,
                            satuan_bhp: {
                                id: daftar_tindakan_bhp[i].sediaan_obat_id,
                                name: daftar_tindakan_bhp[i].nama_sediaan
                            }
                        });
                    }

                    let komponentarif = document.forms.komponentarif_edit;
                        let komponentarif_nama = komponentarif.elements['komponentarif_nama_edit[]'],
                            bpjs_cyto = komponentarif.elements['bpjs_cyto_edit[]'],
                            bpjs_non_cyto = komponentarif.elements['bpjs_non_cyto_edit[]'],
                            umum_cyto = komponentarif.elements['umum_cyto_edit[]'],
                            umum_non_cyto = komponentarif.elements['umum_non_cyto_edit[]'];

                        for (i = 0; i < daftar_tindakan_komponentarif.length; i++){
                            komponentarif_nama[i].value = daftar_tindakan_komponentarif[i].komponentarif_nama
                            bpjs_cyto[i].value = daftar_tindakan_komponentarif[i].harga_bpjs_cyto
                            bpjs_non_cyto[i].value = daftar_tindakan_komponentarif[i].harga_bpjs_non_cyto
                            umum_cyto[i].value = daftar_tindakan_komponentarif[i].harga_umum_cyto
                            umum_non_cyto[i].value = daftar_tindakan_komponentarif[i].harga_umum_non_cyto
                        }

                    updateTableDataBhpAlkesEdit();
                    getValueKomponenTarifEdit();
                    // $('#label_upd_nama_daftar_tindakan').addClass('active');
                    // $('#modal_update_daftar_tindakan').openModal('toggle'); 
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusDaftarTindakan(daftartindakan_id){
    var jsonVariable = {};
    jsonVariable["daftartindakan_id"] = daftartindakan_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val(); 
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "masterdata/daftar_tindakan/do_delete_daftar_tindakan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_daftar_tindakan.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }
        
function formatAsRupiah(angka) {
    angka.value = 'Rp.' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function formatAsRupiah2(angka) {
    angka = 'Rp.' + angka.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return angka;
}
function showNamaTindakan(){
  var h = $('#tag_nama_tindakan'); 
  h.toggle(300); 
      
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/daftar_tindakan/exportToExcel";
}

function removeDataTableBhpAlkes(position) {
    // alert(position);
    bhpListArray.splice(position, 1);
    updateTableDataBhpAlkes();
}

function updateTableDataBhpAlkes() {
    let i,
        tableBhpAlkes = document.getElementById("tbody_bhp_alkes"),
        tr, td;
    tableBhpAlkes.innerHTML = "";
    if (bhpListArray.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "6");
        td.setAttribute("class", "dataTables_empty");
        td.textContent = "No data available in table"
        tr.appendChild(td);
        tableBhpAlkes.appendChild(tr);
        return; 
    }
    for (i = 0; i < bhpListArray.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        var bhp_name = bhpListArray[i].pilih_bhp.name.split("-");

        td = document.createElement("td");
        td.textContent = bhp_name[0];
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = bhp_name[1];
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = bhpListArray[i].jumlah_bhp;
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = bhpListArray[i].satuan_bhp.name;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute('type', 'button');
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataTableBhpAlkes(" + i + ")");
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times");
        button.appendChild(span);
        td.appendChild(button);
        tr.appendChild(td);

        tableBhpAlkes.appendChild(tr);
    }
}

function removeDataTableBhpAlkesEdit(position) {
    // alert(position);
    bhpListArrayEdit.splice(position, 1);
    updateTableDataBhpAlkesEdit();
}

function updateTableDataBhpAlkesEdit() {
    let i,
        tableBhpAlkes = document.getElementById("tbody_bhp_alkes_edit"),
        tr, td;
    tableBhpAlkes.innerHTML = "";
    if (bhpListArrayEdit.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "6");
        td.setAttribute("class", "dataTables_empty");
        td.textContent = "No data available in table"
        tr.appendChild(td);
        tableBhpAlkes.appendChild(tr);
        return; 
    }
    for (i = 0; i < bhpListArrayEdit.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        var bhp_name = bhpListArrayEdit[i].pilih_bhp.name.split("-");

        td = document.createElement("td");
        td.textContent = bhp_name[0];
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = bhp_name[1];
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = bhpListArrayEdit[i].jumlah_bhp;
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = bhpListArrayEdit[i].satuan_bhp.name;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute('type', 'button');
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataTableBhpAlkesEdit(" + i + ")");
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times");
        button.appendChild(span);
        td.appendChild(button);
        tr.appendChild(td);

        tableBhpAlkes.appendChild(tr);
    }
}

function getValueKomponenTarif() {
    komponentarifNamaArray = [];

    let komponentarif = document.forms.komponentarif;
    let komponentarif_nama = komponentarif.elements['komponentarif_nama[]'],
        bpjs_cyto = komponentarif.elements['bpjs_cyto[]'],
        bpjs_non_cyto = komponentarif.elements['bpjs_non_cyto[]'],
        umum_cyto = komponentarif.elements['umum_cyto[]'],
        umum_non_cyto = komponentarif.elements['umum_non_cyto[]'];

        // console.log(bpjs_cyto.length + "\n");
        // console.log(bpjs_non_cyto.length + "\n");
        // console.log(umum_cyto.length + "\n");
        // console.log(umum_non_cyto.length + "\n");

    let average_bpjs_cyto = 0,
        average_bpjs_non_cyto = 0,
        average_umum_cyto = 0,
        average_umum_non_cyto = 0, 
        i = 0;

    for (i = 0; i < komponentarif_nama.length; i++){
        average_bpjs_cyto = parseInt(bpjs_cyto[i].value) + average_bpjs_cyto;
        average_bpjs_non_cyto = parseInt(bpjs_non_cyto[i].value) + average_bpjs_non_cyto;
        average_umum_cyto = parseInt(umum_cyto[i].value) + average_umum_cyto;
        average_umum_non_cyto = parseInt(umum_non_cyto[i].value) + average_umum_non_cyto;
        komponentarifNamaArray.push({
            nama: komponentarif_nama[i].value,
            harga_bpjs_cyto: parseInt(bpjs_cyto[i].value),
            harga_bpjs_non_cyto: parseInt(bpjs_non_cyto[i].value),
            harga_umum_cyto: parseInt(umum_cyto[i].value),
            harga_umum_non_cyto: parseInt(umum_non_cyto[i].value)
        });
    }

    let total_bpjs_cyto = document.getElementById("total_bpjs_cyto"),
        total_bpjs_non_cyto = document.getElementById("total_bpjs_non_cyto"),
        total_umum_cyto = document.getElementById("total_umum_cyto"),
        total_umum_non_cyto = document.getElementById("total_umum_non_cyto");

    total_bpjs_cyto.innerText = formatAsRupiah2(average_bpjs_cyto.toString());
    total_bpjs_non_cyto.innerText = formatAsRupiah2(average_bpjs_non_cyto.toString());
    total_umum_cyto.innerText = formatAsRupiah2(average_umum_cyto.toString());
    total_umum_non_cyto.innerText = formatAsRupiah2(average_umum_non_cyto.toString());
}

function getValueKomponenTarifEdit() {
    komponentarifNamaArrayEdit = [];

    let komponentarif = document.forms.komponentarif_edit;
    let komponentarif_nama = komponentarif.elements['komponentarif_nama_edit[]'],
        bpjs_cyto = komponentarif.elements['bpjs_cyto_edit[]'],
        bpjs_non_cyto = komponentarif.elements['bpjs_non_cyto_edit[]'],
        umum_cyto = komponentarif.elements['umum_cyto_edit[]'],
        umum_non_cyto = komponentarif.elements['umum_non_cyto_edit[]'];

        // console.log(bpjs_cyto.length + "\n");
        // console.log(bpjs_non_cyto.length + "\n");
        // console.log(umum_cyto.length + "\n");
        // console.log(umum_non_cyto.length + "\n");

    let average_bpjs_cyto = 0,
        average_bpjs_non_cyto = 0,
        average_umum_cyto = 0,
        average_umum_non_cyto = 0, 
        i = 0;

    for (i = 0; i < komponentarif_nama.length; i++){
        average_bpjs_cyto = parseInt(bpjs_cyto[i].value) + average_bpjs_cyto;
        average_bpjs_non_cyto = parseInt(bpjs_non_cyto[i].value) + average_bpjs_non_cyto;
        average_umum_cyto = parseInt(umum_cyto[i].value) + average_umum_cyto;
        average_umum_non_cyto = parseInt(umum_non_cyto[i].value) + average_umum_non_cyto;
        komponentarifNamaArrayEdit.push({
            nama: komponentarif_nama[i].value,
            harga_bpjs_cyto: parseInt(bpjs_cyto[i].value),
            harga_bpjs_non_cyto: parseInt(bpjs_non_cyto[i].value),
            harga_umum_cyto: parseInt(umum_cyto[i].value),
            harga_umum_non_cyto: parseInt(umum_non_cyto[i].value)
        });
    }

    let total_bpjs_cyto = document.getElementById("total_bpjs_cyto_edit"),
        total_bpjs_non_cyto = document.getElementById("total_bpjs_non_cyto_edit"),
        total_umum_cyto = document.getElementById("total_umum_cyto_edit"),
        total_umum_non_cyto = document.getElementById("total_umum_non_cyto_edit");

    total_bpjs_cyto.innerText = formatAsRupiah2(average_bpjs_cyto.toString());
    total_bpjs_non_cyto.innerText = formatAsRupiah2(average_bpjs_non_cyto.toString());
    total_umum_cyto.innerText = formatAsRupiah2(average_umum_cyto.toString());
    total_umum_non_cyto.innerText = formatAsRupiah2(average_umum_non_cyto.toString());
}