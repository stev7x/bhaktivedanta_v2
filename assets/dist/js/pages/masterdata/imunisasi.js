var table_list_imunisasi;
$(document).ready(function () {

    table_list_imunisasi = $('#table_imunisasi_list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        "ajax": {
            "url": ci_baseurl + "masterdata/imunisasi/ajax_list_imunisasi",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [-1, 0], //last column
                "orderable": false //set not orderable
            }
        ]

    });
    table_list_imunisasi.columns.adjust().draw();

});


function openImunisasi() {
    $('#form_judul').text("Tambah Data imunisasi");
    $('.form_aksi').removeAttr("id");
    $('.form_aksi').attr("id","fmCreateImunisasi");
    $('.aksi_button').attr("onclick", "saveImunisasi()");
    $('#jenis').val("");
    $('#harga').val("");
    $('#harga_cyto').val("");
    $('#keterangan').val("");

}

function saveImunisasi() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/imunisasi/do_create_imunisasi",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmCreateImunisasi').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                    );
                    $('#modal_imunisasi').modal('hide');
                    $('input[type=text]').val("");
                    table_list_imunisasi.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");

                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif").hide();
                    });
                    $("html, modal-content").animate({ scrollTop: 100 }, "fast");

                }
            }
        });
    });
}

function getEditImunisasi(imunisasi_id) {
    $.ajax({
        url: ci_baseurl + "masterdata/imunisasi/ajax_get_imunisasi_by_id",
        type: 'get',
        dataType: 'json',
        data: { imunisasi_id: imunisasi_id },
        success: function (data) {
            var ret = data.success;
            if (ret === true) {
                console.log("test");
                $('#form_judul').text("Edit Data imunisasi");
                $('.form_aksi').removeAttr("id");
                $('.form_aksi').attr("id", "fmeditImunisasi");
                $('.aksi_button').attr("onclick", "editImunisasi()");
                $('#imunisasi_id').val(data.data.imunisasi_id);
                $('#jenis').val(data.data.jenis);
                $('#harga').val(data.data.harga);
                $('#harga_cyto').val(data.data.harga_cyto);
                $('#keterangan').val(data.data.keterangan);
                $('#keterangan').select();

            } else {
                console.log("Data tidak ditemukan");
            }
        }
    });
}

function editImunisasi() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/imunisasi/do_update_imunisasi",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmeditImunisasi').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                    );
                    $('#modal_imunisasi').modal('hide');
                    table_list_imunisasi.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");
                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif").hide();
                    });
                }
            }
        });
    });
}


function hapusimunisasi(imunisasi_id) {
    var jsonVariable = {};
    jsonVariable["imunisasi_id"] = imunisasi_id;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_delda').val();

    swal({
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/imunisasi/do_delete_imunisasi",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Dihapus.',
                    );
                    table_list_imunisasi.columns.adjust().draw();

                } else {
                    console.log("gagal");
                }
            }
        })
    });
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/imunisasi/exportToExcel";
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}