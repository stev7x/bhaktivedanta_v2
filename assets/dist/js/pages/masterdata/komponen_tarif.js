var table_komponen_tarif;
$(document).ready(function(){
    table_komponen_tarif = $('#table_komponen_tarif_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/komponen_tarif/ajax_list_komponen_tarif",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_komponen_tarif.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});


    
    
    $('button#saveKomponenTarif').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/komponen_tarif/do_create_komponen_tarif",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateKomponenTarif').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_komponentarif").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateKomponenTarif').find("input[type=text]").val("");
                            $('#fmCreateKomponenTarif').find("input[type=checkbox]").prop('checked', false);
                            table_komponen_tarif.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            swal(
                                'Gagal!',
                                'Data Gagal Disimpan.',
                                'error'
                              )
                          console.log("masuk else");
                        }
                    }
                });
          
        })
    });

    $('button#changeKomponenTarif').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/komponen_tarif/do_update_komponen_tarif",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateKomponenTarif').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_komponentarif").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateKomponenTarif').find("input[type=text]").val("");
                            $('#fmCreateKomponenTarif').find("input[type=checkbox]").prop('checked', false);
                            table_komponen_tarif.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            swal(
                                'Gagal!',
                                'Data Gagal Diperbarui.',
                                'error'
                              )
                          console.log("masuk else");
                        }
                    }
                });
          
        })
        });
    });

    function editKomponentarif(komponentarif_id){
        if(komponentarif_id){    
            $.ajax({
            url: ci_baseurl + "masterdata/komponen_tarif/ajax_get_komponentarif_by_id",
            type: 'get',
            dataType: 'json',
            data: { komponentarif_id : komponentarif_id },
                success: function(data) {
                    var ret = data.success;
                    if(ret === true) {
                        console.log(data.data);
                        $('#komponentarif_id_update').val(data.data.komponentarif_id);
                        $('#komponentarif_nama_update').val(data.data.komponentarif_nama);
                        switch (data.data.allow_downpayment) {
                            case "0":
                                $('#allow_downpayment_update').prop('checked', false);
                                console.log("isinya 0");
                                break;
                            case "1":
                                $('#allow_downpayment_update').prop('checked', true);
                                console.log("isinya 1");
                                break;
                            default:
                                $('#allow_downpayment_update').prop('checked', false);
                                console.log("isinya undefined");
                                break;
                        }
                    } else {
                        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                        $('#notification_messages').html(data.messages);
                        $('#notification_type').show();
                    }
                }
            });
        }
        else
        {
            $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html(data.messages);
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                $(".modal_notif").hide();
            });
        }
    }

    function hapusKomponentarif(komponentarif_id){
      var jsonVariable = {};
      jsonVariable["komponentarif_id"] = komponentarif_id;
      jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
      console.log("masuk fungsi");
      swal({
        text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
      }).then(function(){
          $.ajax({
              url: ci_baseurl + "masterdata/komponen_tarif/do_delete_komponen_tarif",
              type: 'post',
              dataType: 'json',
              data: jsonVariable,
                  success: function(data) {
                      var ret = data.success;
                      $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                      $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                      if(ret === true) {
                        $('.notif').removeClass('red').addClass('green');
                        $('#card_message').html(data.messages);
                        $('.notif').show();
                        $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".notif").hide();
                        });
                        table_komponen_tarif.columns.adjust().draw();
                      } else {
                        $('.notif').removeClass('green').addClass('red');
                        $('#card_message').html(data.messages);
                        $('.notif').show();
                        $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".notif").hide();
                        });
                          console.log("gagal");
                      }
                  }
              });
        swal(
          'Berhasil!',
          'Data Berhasil Dihapus.',
          'success',
        )
      });
    } 

   