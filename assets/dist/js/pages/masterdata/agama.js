var table_agama;
$(document).ready(function(){
    table_agama = $('#table_agama_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/agama/ajax_list_agama",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_agama.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});


    $('button#importData').click(function(e){  
      var data = new FormData($(this)[0]);    
      var input = document.getElementById('file');
      var property = input.files[0];          
      var form_data = new FormData();
      form_data.append("file",property);    
          swal({    
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true, 
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',  
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
          }).then(function(){            
              $.ajax({     
                      url: ci_baseurl + "masterdata/agama/upload",  
                      data: form_data,              
                      method: 'POST',  
                      type: 'POST',  
                      processData: false,
                      contentType: false,  
                      cache:false,  
                      success: function(data) {  
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_import]').val(data.csrfHash);
                         
                              table_agama.columns.adjust().draw();
                              swal(
                                  'Berhasil!',
                                  'Data Berhasil Diimport.',
                                  'success'
                                )
                           
                      } 
                  });
            
          })
    });
    
    $('button#saveAgama').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/agama/do_create_agama",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateAgama').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_agama").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateAgama').find("input[type=text]").val("");
                            $('#fmCreateAgama').find("select").prop('selectedIndex',0);
                            table_agama.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            swal(
                                'Gagal!',
                                'Data Gagal Disimpan.',
                                'error'
                              )
                          console.log("masuk else");
                        }
                    }
                });
          
        })
    });

    $('button#changeAgama').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/agama/do_update_agama",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateAgama').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_agama").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmUpdateAgama').find("input[type=text]").val("");
                            $('#fmUpdateAgama').find("select").prop('selectedIndex',0);
                            table_agama.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            swal(
                                'Gagal!',
                                'Data Gagal Diperbarui.',
                                'error'
                              )
                          console.log("masuk else");
                        }
                    }
                });
          
        })
    });
});

function editAgama(agama_id){
    if(agama_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/agama/ajax_get_agama_by_id",
        type: 'get',
        dataType: 'json',
        data: {agama_id:agama_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_agama').val(data.data.agama_id);
                    $('#upd_nama_agama').val(data.data.agama_nama);
                    $('#label_upd__nama_agama').addClass('active');
                    // $('#modal_update_agama').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusAgama(agama_id){
  var jsonVariable = {};
  jsonVariable["agama_id"] = agama_id;
  jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
  console.log("masuk fungsi");
  swal({
    text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'tidak',
    confirmButtonText: 'Ya'
  }).then(function(){
      $.ajax({
          url: ci_baseurl + "masterdata/agama/do_delete_agama",
          type: 'post',
          dataType: 'json',
          data: jsonVariable,
              success: function(data) {
                  var ret = data.success;
                  $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                  $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                   
                  if(ret === true) {
                    $('.notif').removeClass('red').addClass('green');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    table_agama.columns.adjust().draw();
                  } else {
                    $('.notif').removeClass('green').addClass('red');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                      console.log("gagal");
                  }
              }
          });
    swal(
      'Berhasil!',
      'Data Berhasil Dihapus.',
      'success',
    )
  });
} 

function importExcel(){
  var data = new FormData();
  jQuery.each(jQuery('#file')[0].files, function(i, file) {
      data.append('file-'+i, file);
  });
  swal({
    text: 'Apakah file import yang dimasukan sudah benar ?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText:'Tidak',
    confirmButtonText: 'Ya!'
  }).then(function(){
      $.ajax({
              url: ci_baseurl + "masterdata/agama/upload",
              // enctype: 'multipart/form-data',
              type: 'POST',
              dataType: 'JSON',
              cache: false,  
              contentType: false,         
              data: $('form#frmImport').serialize(),
                
              success: function(data) {  
                  var ret = data.success;
                  $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                  $('input[name='+data.csrfTokenName+'_import]').val(data.csrfHash);
                 
                  if(ret === true) {
                      table_agama.columns.adjust().draw();
                      swal(
                          'Berhasil!',
                          'Data Berhasil Diimport.',
                          'success'
                        )
                  } else {

                      swal(
                          'Gagal!',
                          'Data Gagal Disimpan.',
                          'error'
                        )
                    console.log("masuk else");
                  }
              }
          });
    
  })
}


   