var table_tarif_tindakan;
$(document).ready(function(){

    // disable mousewheel on a input number field when in focus
    // (to prevent Cromium browsers change the value when scrolling)
    $('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault()
    })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
    })


    table_tarif_tindakan = $('#table_tarif_tindakan_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/tarif_tindakan/ajax_list_tarif_tindakan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_tarif_tindakan.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#saveTarif_tindakan').click(function(){

        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
               
            $.ajax({
                url: ci_baseurl + "masterdata/tarif_tindakan/do_create_tarif_tindakan",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateTarif_tindakan').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                    if(ret === true) {
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmCreateTarif_tindakan').find("input[type=text]").val("");
                        $('#fmCreateTarif_tindakan').find("input[type=number]").val("");
                        $('#fmCreateTarif_tindakan').find("select").prop('selectedIndex',0);
                    table_tarif_tindakan.columns.adjust().draw();
                    swal( 
                        'Berhasil!',
                        'Data Berhasil Disimpan.',
                        'success', 
                      ) 
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                         swal(
                            'Gagal!',
                            'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                            'error'
                          ) 
                    }
                }
            });

            
          });

    });
    
    $('button#changeTarif_tindakan').click(function(){

        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/tarif_tindakan/do_update_tarif_tindakan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateTarif_tindakan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            $('#modal_edit_tarif_tindakan').modal('hide');
                    
                            //$('#fmCreateTarif_tindakan').find("input[type=text]").val("");
                            //$('#fmCreateTarif_tindakan').find("select").prop('selectedIndex',0);
                            
                        table_tarif_tindakan.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Diperbarui.', 
                            'success',
                          ) 
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
            
          })
    });
});

function editTarif_tindakan(tariftindakan_id){
    if(tariftindakan_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/tarif_tindakan/ajax_get_tarif_tindakan_by_id",
        type: 'get',
        dataType: 'json',
        data: {tariftindakan_id:tariftindakan_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_tarif_tindakan').val(data.data.tariftindakan_id);
                    $('#upd_nama_tarif_tindakan').val(data.data.harga_tindakan);
                    $('#label_upd__nama_tarif_tindakan').addClass('active');
                    $('#upd_select_kelas').val(data.data.kelaspelayanan_id);
                    // $('#upd_select_kelas').material_select();
                    $('#upd_select_tindakan').val(data.data.daftartindakan_id);
                    // $('#upd_select_tindakan').material_select();
                    $('#upd_select_komponen').val(data.data.komponentarif_id);
                    // $('#upd_select_komponen').material_select();
                    $('#upd_bpjs_non_bpjs').val(data.data.bpjs_non_bpjs);
                    // $('#upd_bpjs_non_bpjs').material_select();
                    $('#upd_cyto_tindakan').val(data.data.cyto_tindakan);
                    $('#label_upd_cyto_tindakan').addClass('active');

                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}
function hapusTarif_tindakan(tariftindakan_id){
    var jsonVariable = {};
    jsonVariable["tariftindakan_id"] = tariftindakan_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(tariftindakan_id){

        swal({
            text: "Apakah anda yakin ingin menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){  
              
            $.ajax({
                url: ci_baseurl + "masterdata/tarif_tindakan/do_delete_tarif_tindakan",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                        table_tarif_tindakan.columns.adjust().draw();
                        } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                        }
                    }
            });
  
        swal( 
              'Berhasil!', 
              'Data berhasil dihapus.', 
              'success',
            ) 
          });

        
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}