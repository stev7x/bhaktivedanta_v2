var table_kelas_pelayanan;
$(document).ready(function(){
    table_kelas_pelayanan = $('#table_kelas_pelayanan_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/kelas_pelayanan/ajax_list_kelas_pelayanan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_kelas_pelayanan.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    

    $('button#importData').click(function (e) {
        var data = new FormData($(this)[0]);
        var input = document.getElementById('file');
        var property = input.files[0];
        var form_data = new FormData();
        form_data.append("file", property);
        swal({
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "masterdata/kelas_pelayanan/upload",
                data: form_data,
                method: 'POST',
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_import]').val(data.csrfHash);

                    swal(
                        'Berhasil!',
                        'Data Berhasil Diimport.',
                        'success'
                    )
                    table_kelas_pelayanan.columns.adjust().draw();

                }
            });

        })
    });


    $('button#saveKelas_pelayanan').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/kelas_pelayanan/do_create_kelas_pelayanan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateKelas_pelayanan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateKelas_pelayanan').find("input[type=text]").val("");
                            $('#fmCreateKelas_pelayanan').find("input[type=number]").val("");
                            $('#fmCreateKelas_pelayanan').find("select").prop('selectedIndex',0);
                            $('input:checkbox').removeAttr('checked');
                            $('#modal_add_kelas_pelayanan').modal("hide");
                        table_kelas_pelayanan.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.', 
                            'success',
                          ) 
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
           
          });
    });
    
    $('button#changeKelas_pelayanan').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/kelas_pelayanan/do_update_kelas_pelayanan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateKelas_pelayanan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            $('#modal_update_kelas_pelayanan').modal("hide");

                        table_kelas_pelayanan.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Diperbarui.', 
                            'success'
                        ) 
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
            
          });
    });
});

function add_kelas_pelayanan(){
    $('#modal_add_kelas_pelayanan').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
}
 
function editKelas_pelayanan(kelaspelayanan_id){
    if(kelaspelayanan_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/kelas_pelayanan/ajax_get_kelas_pelayanan_by_id",
        type: 'get',
        dataType: 'json',
        data: {kelaspelayanan_id:kelaspelayanan_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_kelas_pelayanan').val(data.data.kelaspelayanan_id);
                    $('#upd_nama_kelas_pelayanan').val(data.data.kelaspelayanan_nama);
                    $('#upd_tarif').val(data.data.tarif);
                    $('#upd_luas_ruangan').val(data.data.luas_ruangan);
                    $('#upd_isi_bed').val(data.data.isi_bed);
                    if(data.data.kamar_mandi_dalam=='1') {
                        $('#upd_kamar_mandi').prop('checked',true);
                    }else{
                        $('#upd_kamar_mandi').prop('checked',false);
                    }
                    if(data.data.ac=='1') {
                        $('#upd_ac').prop('checked',true);
                    }else{
                        $('#upd_ac').prop('checked',false);
                    }
                    if(data.data.baby_bed=='1') {
                        $('#upd_baby_bed').prop('checked',true);
                    }else{
                        $('#upd_baby_bed').prop('checked',false);
                    }
                    if(data.data.closed_duduk=='1') {
                        $('#upd_closet').prop('checked',true);
                    }else{
                        $('#upd_closet').prop('checked',false);
                    }
                    if(data.data.exhause_fan=='1') {
                        $('#upd_exhause_fan').prop('checked',true);
                    }else{
                        $('#upd_exhause_fan').prop('checked',false);
                    }
                    if(data.data.extra_bed=='1') {
                        $('#upd_extra_bed').prop('checked',true);
                    }else{
                        $('#upd_extra_bed').prop('checked',false);
                    }
                    if(data.data.free_wifi=='1') {
                        $('#upd_wifi').prop('checked',true);
                    }else{
                        $('#upd_wifi').prop('checked',false);
                    }
                    if(data.data.isi_bed=='1') {
                        $('#upd_isi_bed').prop('checked',true);
                    }else{
                        $('#upd_isi_bed').prop('checked',false);
                    }
                    if(data.data.kipas_angin=='1') {
                        $('#upd_kipas_angin').prop('checked',true);
                    }else{
                        $('#upd_kipas_angin').prop('checked',false);
                    }
                    if(data.data.kulkas=='1') {
                        $('#upd_kulkas').prop('checked',true);
                    }else{
                        $('#upd_kulkas').prop('checked',false);
                    }
                    if(data.data.led_tv_digital=='1') {
                        $('#upd_tv').prop('checked',true);
                    }else{
                        $('#upd_tv').prop('checked',false);
                    }
                    if(data.data.lemari=='1') {
                        $('#upd_lemari').prop('checked',true);
                    }else{
                        $('#upd_lemari').prop('checked',false);
                    }
                    if(data.data.paket_buah=='1') {
                        $('#upd_paket_buah').prop('checked',true);
                    }else{
                        $('#upd_paket_buah').prop('checked',false);
                    }
                    if(data.data.paket_peralatan_mandi=='1') {
                        $('#upd_peralatan_mandi').prop('checked',true);
                    }else{
                        $('#upd_peralatan_mandi').prop('checked',false);
                    }
                    if(data.data.ruang_keluarga=='1') {
                        $('#upd_ruang_keluarga').prop('checked',true);
                    }else{
                        $('#upd_ruang_keluarga').prop('checked',false);
                    }
                    if(data.data.ruang_tamu=='1') {
                        $('#upd_ruang_tamu').prop('checked',true);
                    }else{
                        $('#upd_ruang_tamu').prop('checked',false);
                    }
                    if(data.data.shower=='1') {
                        $('#upd_shower').prop('checked',true);
                    }else{
                        $('#upd_shower').prop('checked',false);
                    }
                    if(data.data.sofa_bed=='1') {
                        $('#upd_sofa_bed').prop('checked',true);
                    }else{
                        $('#upd_sofa_bed').prop('checked',false);
                    }
                    if(data.data.wastafel=='1') {
                        $('#upd_wastafel').prop('checked',true);
                    }else{
                        $('#upd_wastafel').prop('checked',false);
                    }
                    if(data.data.water_heater=='1') {
                        $('#upd_water_heater').prop('checked',true);
                    }else{
                        $('#upd_water_heater').prop('checked',false);
                    }

                    $('#modal_update_kelas_pelayanan').modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    });
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}
function detailKelas_pelayanan(kelaspelayanan_id){
    if(kelaspelayanan_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/kelas_pelayanan/ajax_get_kelas_pelayanan_by_id",
        type: 'get',
        dataType: 'json',
        data: {kelaspelayanan_id:kelaspelayanan_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_kelas_pelayanan').val(data.data.kelaspelayanan_id);
                    $('#upd_nama_kelas_pelayanan').val(data.data.kelaspelayanan_nama);
                    $('#upd_tarif').val(data.data.tarif);
                    $('#upd_luas_ruangan').val(data.data.luas_ruangan);
                    $('#upd_isi_bed').val(data.data.isi_bed);
                    






                    if(data.data.kamar_mandi_dalam=='1') {
                        $('#det_kamar_mandi').removeAttr('disabled',true)
                        $('#det_kamar_mandi').prop('checked',true);
                        $("#det_kamar_mandi").change(function() {
                            if(this.checked) {
                                $('#det_kamar_mandi').prop('checked',true);
                            }else{ 
                                $('#det_kamar_mandi').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.ac=='1') {
                        $('#det_ac').removeAttr('disabled',true)
                        $('#det_ac').prop('checked',true);
                        $("#det_ac").change(function() {
                            if(this.checked) {
                                $('#det_ac').prop('checked',true);
                            }else{ 
                                $('#det_ac').prop('checked',true);     
                            } 
                        }); 
                    }else{
                    }
                    if(data.data.baby_bed=='1') {
                        $('#det_baby_bed').removeAttr('disabled',true)
                        $('#det_baby_bed').prop('checked',true);
                        $("#det_baby_bed").change(function() {
                            if(this.checked) {
                                $('#det_baby_bed').prop('checked',true);
                            }else{ 
                                $('#det_baby_bed').prop('checked',true);     
                            } 
                        }); 
                    }else{
                    }
                    if(data.data.closed_duduk=='1') {
                        $('#det_closet').removeAttr('disabled',true)
                        $('#det_closet').prop('checked',true);
                        $("#det_closet").change(function() {
                            if(this.checked) {
                                $('#det_closet').prop('checked',true);
                            }else{ 
                                $('#det_closet').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.exhause_fan=='1') {
                        $('#det_exhause_fan').removeAttr('disabled',true)
                        $('#det_exhause_fan').prop('checked',true);
                        $("#det_exhause_fan").change(function() {
                            if(this.checked) {
                                $('#det_exhause_fan').prop('checked',true);
                            }else{ 
                                $('#det_exhause_fan').prop('checked',true);     
                            } 
                        }); 
                    }else{
                    }
                    if(data.data.extra_bed=='1') {
                        $('#det_extra_bed').removeAttr('disabled',true)
                        $('#det_extra_bed').prop('checked',true);
                        $("#det_extra_bed").change(function() {
                            if(this.checked) {
                                $('#det_extra_bed').prop('checked',true);
                            }else{ 
                                $('#det_extra_bed').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.free_wifi=='1') {
                        $('#det_wifi').removeAttr('disabled',true)
                        $('#det_wifi').prop('checked',true);
                        $("#det_wifi").change(function() {
                            if(this.checked) {
                                $('#det_wifi').prop('checked',true);
                            }else{ 
                                $('#det_wifi').prop('checked',true);     
                            } 
                        }); 
                    }else{
                    }
                    if(data.data.isi_bed=='1') {
                        $('#det_isi_bed').removeAttr('disabled',true)
                        $('#det_isi_bed').prop('checked',true);
                        $("#det_isi_bed").change(function() {
                            if(this.checked) {
                                $('#det_isi_bed').prop('checked',true);
                            }else{ 
                                $('#det_isi_bed').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.kipas_angin=='1') {
                        $('#det_kipas_angin').removeAttr('disabled',true)
                        $('#det_kipas_angin').prop('checked',true);
                        $("#det_kipas_angin").change(function() {
                            if(this.checked) {
                                $('#det_kipas_angin').prop('checked',true);
                            }else{ 
                                $('#det_kipas_angin').prop('checked',true);     
                            } 
                        }); 
                    }else{
                    }
                    if(data.data.kulkas=='1') {
                        $('#det_kulkas').removeAttr('disabled',true)
                        $('#det_kulkas').prop('checked',true);
                        $("#det_kulkas").change(function() {
                            if(this.checked) {
                                $('#det_kulkas').prop('checked',true);
                            }else{ 
                                $('#det_kulkas').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.led_tv_digital=='1') {
                        $('#det_tv').removeAttr('disabled',true)
                        $('#det_tv').prop('checked',true);
                        $("#det_tv").change(function() {
                            if(this.checked) {
                                $('#det_tv').prop('checked',true);
                            }else{ 
                                $('#det_tv').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.lemari=='1') {
                        $('#det_lemari').removeAttr('disabled',true)
                        $('#det_lemari').prop('checked',true);
                        $("#det_lemari").change(function() {
                            if(this.checked) {
                                $('#det_lemari').prop('checked',true);
                            }else{ 
                                $('#det_lemari').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.paket_buah=='1') {
                        $('#det_paket_buah').removeAttr('disabled',true)
                        $('#det_paket_buah').prop('checked',true);
                        $("#det_paket_buah").change(function() {
                            if(this.checked) {
                                $('#det_paket_buah').prop('checked',true);
                            }else{ 
                                $('#det_paket_buah').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.paket_peralatan_mandi=='1') {
                        $('#det_peralatan_mandi').removeAttr('disabled',true)
                        $('#det_peralatan_mandi').prop('checked',true);
                        $("#det_peralatan_mandi").change(function() {
                            if(this.checked) {
                                $('#det_peralatan_mandi').prop('checked',true);
                            }else{ 
                                $('#det_peralatan_mandi').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.ruang_keluarga=='1') {
                        $('#det_ruang_keluarga').removeAttr('disabled',true)
                        $('#det_ruang_keluarga').prop('checked',true);
                        $("#det_ruang_keluarga").change(function() {
                            if(this.checked) {
                                $('#det_ruang_keluarga').prop('checked',true);
                            }else{ 
                                $('#det_ruang_keluarga').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.ruang_tamu=='1') {
                        $('#det_ruang_tamu').removeAttr('disabled',true)
                        $('#det_ruang_tamu').prop('checked',true);
                        $("#det_ruang_tamu").change(function() {
                            if(this.checked) {
                                $('#det_ruang_tamu').prop('checked',true);
                            }else{ 
                                $('#det_ruang_tamu').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.shower=='1') {
                        $('#det_shower').removeAttr('disabled',true)
                        $('#det_shower').prop('checked',true);
                        $("#det_shower").change(function() {
                            if(this.checked) {
                                $('#det_shower').prop('checked',true);
                            }else{ 
                                $('#det_shower').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.sofa_bed=='1') {
                        $('#det_sofa_bed').removeAttr('disabled',true)
                        $('#det_sofa_bed').prop('checked',true);
                        $("#det_sofa_bed").change(function() {
                            if(this.checked) {
                                $('#det_sofa_bed').prop('checked',true);
                            }else{ 
                                $('#det_sofa_bed').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.wastafel=='1') {
                        $('#det_wastafel').removeAttr('disabled',true)
                        $('#det_wastafel').prop('checked',true);
                        $("#det_wastafel").change(function() {
                            if(this.checked) {
                                $('#det_wastafel').prop('checked',true);
                            }else{ 
                                $('#det_wastafel').prop('checked',true);     
                            } 
                        });
                    }else{
                    }
                    if(data.data.water_heater=='1') {
                        $('#det_water_heater').removeAttr('disabled',true)
                        $('#det_water_heater').prop('checked',true);
                        $("#det_water_heater").change(function() {
                            if(this.checked) {
                                $('#det_water_heater').prop('checked',true);
                            }else{ 
                                $('#det_water_heater').prop('checked',true);     
                            } 
                        });
                    }else{
                    }


                    // $('#modal_detail_fasilitas').modal('show'); 
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}
function hapusKelas_pelayanan(kelaspelayanan_id){
    var jsonVariable = {};
    jsonVariable["kelaspelayanan_id"] = kelaspelayanan_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(kelaspelayanan_id){

        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/kelas_pelayanan/do_delete_kelas_pelayanan",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                            if(ret === true) {
                                $('.notif').removeClass('red').addClass('green');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            table_kelas_pelayanan.columns.adjust().draw();
                            } else {
                                $('.notif').removeClass('green').addClass('red');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            }
                        }
                });
            swal( 
              'Berhasil!',
              'Data Berhasil Dihapus.', 
              'success',
            ) 
          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}


$("#ceklis_semua").change(function(){
    if ($('#ceklis_semua:checked').length == $('#ceklis_semua').length) {
       $('#kamar_mandi').prop('checked',true)       
       $('#closet').prop('checked',true)       
       $('#shower').prop('checked',true)       
       $('#wastafel').prop('checked',true)       
       $('#ac').prop('checked',true)       
       $('#water_heater').prop('checked',true)       
       $('#tv').prop('checked',true)       
       $('#kipas_angin').prop('checked',true)       
       $('#exhause_fan').prop('checked',true)       
       $('#baby_bed').prop('checked',true)       
       $('#kulkas').prop('checked',true)       
       $('#lemari').prop('checked',true)       
       $('#ruang_tamu').prop('checked',true)       
       $('#ruang_keluarga').prop('checked',true)       
       $('#extra_bed').prop('checked',true)       
       $('#sofa_bed').prop('checked',true)       
       $('#wifi').prop('checked',true)       
       $('#peralatan_mandi').prop('checked',true)       
       $('#paket_buah').prop('checked',true)       
    }else{
        $('#kamar_mandi').prop('checked',false)       
        $('#closet').prop('checked',false)       
        $('#shower').prop('checked',false)       
        $('#wastafel').prop('checked',false)       
        $('#ac').prop('checked',false)       
        $('#water_heater').prop('checked',false)       
        $('#tv').prop('checked',false)       
        $('#kipas_angin').prop('checked',false)       
        $('#exhause_fan').prop('checked',false)       
        $('#baby_bed').prop('checked',false)       
        $('#kulkas').prop('checked',false)       
        $('#lemari').prop('checked',false)       
        $('#ruang_tamu').prop('checked',false)       
        $('#ruang_keluarga').prop('checked',false)       
        $('#extra_bed').prop('checked',false)       
        $('#sofa_bed').prop('checked',false)       
        $('#wifi').prop('checked',false)       
        $('#peralatan_mandi').prop('checked',false)       
        $('#paket_buah').prop('checked',false)    
    }
});
$("#upd_ceklis_semua").change(function(){
    if ($('#upd_ceklis_semua:checked').length == $('#upd_ceklis_semua').length) {
       $('#upd_kamar_mandi').prop('checked',true)       
       $('#upd_closet').prop('checked',true)       
       $('#upd_shower').prop('checked',true)       
       $('#upd_wastafel').prop('checked',true)       
       $('#upd_ac').prop('checked',true)       
       $('#upd_water_heater').prop('checked',true)       
       $('#upd_tv').prop('checked',true)       
       $('#upd_kipas_angin').prop('checked',true)       
       $('#upd_exhause_fan').prop('checked',true)       
       $('#upd_baby_bed').prop('checked',true)       
       $('#upd_kulkas').prop('checked',true)       
       $('#upd_lemari').prop('checked',true)       
       $('#upd_ruang_tamu').prop('checked',true)       
       $('#upd_ruang_keluarga').prop('checked',true)       
       $('#upd_extra_bed').prop('checked',true)       
       $('#upd_sofa_bed').prop('checked',true)       
       $('#upd_wifi').prop('checked',true)       
       $('#upd_peralatan_mandi').prop('checked',true)       
       $('#upd_paket_buah').prop('checked',true)       
    }else{
        $('#upd_kamar_mandi').prop('checked',false)       
        $('#upd_closet').prop('checked',false)       
        $('#upd_shower').prop('checked',false)       
        $('#upd_wastafel').prop('checked',false)       
        $('#upd_ac').prop('checked',false)       
        $('#upd_water_heater').prop('checked',false)       
        $('#upd_tv').prop('checked',false)       
        $('#upd_kipas_angin').prop('checked',false)       
        $('#upd_exhause_fan').prop('checked',false)       
        $('#upd_baby_bed').prop('checked',false)       
        $('#upd_kulkas').prop('checked',false)       
        $('#upd_lemari').prop('checked',false)       
        $('#upd_ruang_tamu').prop('checked',false)       
        $('#upd_ruang_keluarga').prop('checked',false)       
        $('#upd_extra_bed').prop('checked',false)       
        $('#upd_sofa_bed').prop('checked',false)       
        $('#upd_wifi').prop('checked',false)       
        $('#upd_peralatan_mandi').prop('checked',false)       
        $('#upd_paket_buah').prop('checked',false)    
    }
});
// $('.ceklis :checkbox').change(function() {
//     if ($('.ceklis :checkbox:not(:checked)').length == 0) {
//       console.log('all are checked');
//     } else if ($('.ceklis :checkbox:checked').length == 0) {
//       console.log('all are unchecked');
//     }
//   });
function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/kelas_pelayanan/exportToExcel";
}

function showbtnimport() {
    var value = $('#file').val();
    console.log('isi : ' + value);
    if (value != '') {
        console.log('masuk if');
        $('#importData').fadeIn();
    }
} 