var table_paket_operasi;
var table_tindakan_paket;
var table_obat_paket;
$(document).ready(function(){
    table_paket_operasi = $('#table_paket_operasi_list').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "masterdata/paket_operasi/ajax_list_paket_operasi",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1,0 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_paket_operasi.columns.adjust().draw();
    

    $('button#importData').click(function (e) {
        var data = new FormData($(this)[0]);
        var input = document.getElementById('file');
        var property = input.files[0];
        var form_data = new FormData();
        form_data.append("file", property);
        swal({
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "masterdata/paket_operasi/upload",
                data: form_data,
                method: 'POST',
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_import]').val(data.csrfHash);
                        swal(
                            'Berhasil!',
                            'Data Berhasil Diimport.',
                            'success'
                        )
                        table_paket_operasi.columns.adjust().draw();

                }
            });

        })
    });

    $('button#savePaketOperasi').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/paket_operasi/do_create_paket_operasi",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreatePaketOperasi').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delope]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_paket_operasi").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreatePaketOperasi').find("input[type=text]").val("");
                            $('#fmCreatePaketOperasi').find("select").prop('selectedIndex',0);
                            table_paket_operasi.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                            swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
         
        })
    });

    $('button#changePaketOperasi').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/paket_operasi/do_update_paket_operasi",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdatePaketOperasi').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delope]').val(data.csrfHash);
                       console.log("masuk 1");
                        if(ret === true) {
                            console.log("masuk 2");
                            $("#modal_edit_paket_operasi").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                            });
                            $('#fmUpdatePaketOperasi').find("input[type=text]").val("");
                            $('#fmUpdatePaketOperasi').find("select").prop('selectedIndex',0);
                        table_paket_operasi.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Diperbarui.',
                            'success'
                          )
                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                          swal(
                            'Gagal!',
                            'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                            'error'
                          )
                        }
                    }
                });
         
        })
    });
    
    $('button#saveTindakanPaket').click(function(){
        paketoperasi_id = $('#upd_id_paket').val();
        jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                    url: ci_baseurl + "masterdata/paket_operasi/do_create_tindakan_paket",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateTindakanPaket').serialize()+ "&paketoperasi_id=" +paketoperasi_id,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delobat]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            $('#fmCreateTindakanPaket').find("input[type=text]").val("");
                            $('#fmCreateTindakanPaket').find("select").prop('selectedIndex',0);
                            get_table_tindakan(paketoperasi_id);
                            table_paket_operasi.columns.adjust().draw();
                        } else {
                            $('.upd_modal_notif').removeClass('green').addClass('red');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                        }
                    }
                });
            }
        });
    });

    function reloadTablePaketOperasi(){
        table_paket_operasi.columns.adjust().draw();
    }
    
    $('button#saveObatPaket').click(function(){
        paketoperasi_id = $('#upd_id_paket').val();
        jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                    url: ci_baseurl + "masterdata/paket_operasi/do_create_obat_paket",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateObatPaket').serialize()+ "&paketoperasi_id=" +paketoperasi_id,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delobat]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            $('#fmCreateObatPaket').find("input[type=text]").val("");
                            $('#fmCreateObatPaket').find("select").prop('selectedIndex',0);
                            get_table_obat(paketoperasi_id);
                            table_paket_operasi.columns.adjust().draw();
                        } else {
                            $('.upd_modal_notif').removeClass('green').addClass('red');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                        }
                    }
                });
            }
        });
    });
});

function editPaketOperasi(paket_id){
    if(paket_id != ''){    
        $.ajax({
        url: ci_baseurl + "masterdata/paket_operasi/ajax_get_paket_by_id",
        type: 'get',
        dataType: 'json',
        data: {paket_id:paket_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_paket').val(data.data.paketoperasi_id);
                    $('#upd_nama_paket').val(data.data.nama_paket);
                    $('#upd_kelaspelayanan').val(data.data.kelaspelayanan_id);
                    $('#upd_kelaspelayanan').select();
                    $('#upd_bpjs').val(data.data.is_bpjs);
                    $('#upd_bpjs').select();
                    $('#upd_jml_hari').val(data.data.jml_hari);
                    $('#upd_total_harga').val(data.data.total_harga);
                    $('#label_upd_nama_paket').addClass('active');
                    get_table_tindakan(data.data.paketoperasi_id);
                    getTindakan(data.data.kelaspelayanan_id, data.data.is_bpjs);
                    getHari(data.data.jml_hari);
                    get_table_obat(data.data.paketoperasi_id);
                    // $('#modal_update_paket_operasi').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function get_table_tindakan(paketoperasi_id){
    table_tindakan_paket = $('#table_tindakan_paket').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "paging": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "masterdata/paket_operasi/ajax_list_tindakan_paket",
            "type": "GET",
            "data" : function(d){
                d.paketoperasi_id = paketoperasi_id;
            }
        }
    });
}



function hapusPaketOperasi(paket_id){
    var jsonVariable = {};
    jsonVariable["paket_id"] = paket_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delope').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "masterdata/paket_operasi/do_delete_paket",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delope]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_paket_operasi.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }

function getTindakan(kelaspelayanan_id, type_pembayaran){
    $.ajax({
        url: ci_baseurl + "masterdata/paket_operasi/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id,type_pembayaran:type_pembayaran},
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    });
}

function getHari(jmlhari){
    $.ajax({
        url: ci_baseurl + "masterdata/paket_operasi/get_hari_list",
        type: 'GET',
        dataType: 'JSON',
        data: {jmlhari:jmlhari},
        success: function(data) {
            $("#harike").html(data.list);
            $("#harike_obat").html(data.list);
        }
    });
}

function getTarifTindakan(){
    var tariftindakan_id = $("#tindakan").val();
    $.ajax({
        url: ci_baseurl + "masterdata/paket_operasi/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {tariftindakan_id:tariftindakan_id},
        success: function(data) {
            $("#harga_tindakan").val(data.list.harga_tindakan);
        }
    });
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var subsidi = $("#subsidi").val();
    var jml_tindakan = $("#jml_tindakan").val();
    
    if(subsidi != ''){
        var tarif = parseInt(tarif_tindakan) - parseInt(subsidi);
    }else{
        var tarif = parseInt(tarif_tindakan);
    }
    
    var total = parseInt(tarif) * parseInt(jml_tindakan);
    
    total = (!isNaN(total)) ? total : 0;
    $("#totalharga").val(total);
}

function hapusTindakan(detailpaket_id,paketoperasi_id){
    var jsonVariable = {};
    jsonVariable["detailpaket_id"] = detailpaket_id;
    jsonVariable["paketoperasi_id"] = paketoperasi_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    if(paketoperasi_id != ''){
        jConfirm("Are you sure, you want to delete ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                url: ci_baseurl + "masterdata/paket_operasi/do_delete_tindakan_paket",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delobat]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            get_table_tindakan(paketoperasi_id);
                            table_paket_operasi.columns.adjust().draw();
                        } else {
                            $('.upd_modal_notif').removeClass('green').addClass('red');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                        }
                    }
                });
            }
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function get_table_obat(paketoperasi_id){
    table_obat_paket = $('#table_obat_paket').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "paging": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "masterdata/paket_operasi/ajax_list_obat_paket",
            "type": "GET",
            "data" : function(d){
                d.paketoperasi_id = paketoperasi_id;
            }
        }
    });
}

function getObat(){
    var jenisoa_id = $('#jenisobat').val();
    $.ajax({
        url: ci_baseurl + "masterdata/paket_operasi/get_obat_list",
        type: 'GET',
        dataType: 'JSON',
        data: {jenisoa_id:jenisoa_id},
        success: function(data) {
            $("#obat_id").html(data.list);
        }
    });
}

function getHargaObat(){
    var obat_id = $("#obat_id").val();
    $.ajax({
        url: ci_baseurl + "masterdata/paket_operasi/get_tarif_obat",
        type: 'GET',
        dataType: 'JSON',
        data: {obat_id:obat_id},
        success: function(data) {
            $("#harga_satuan").val(data.list.harga_jual);
        }
    });
}

function hitungHargaObat(){
    var harga_obat = $("#harga_satuan").val();
    var subsidi = $("#subsidi_obat").val();
    var jml_obat = $("#jml_obat").val();
    
    if(subsidi != ''){
        var tarif = parseInt(harga_obat) - parseInt(subsidi);
    }else{
        var tarif = parseInt(harga_obat);
    }
    
    var total = parseInt(tarif) * parseInt(jml_obat);
    
    total = (!isNaN(total)) ? total : 0;
    $("#totalhargaobat").val(total);
}

function hapusObat(paketoperasiobat_id,paketoperasi_id){
    var jsonVariable = {};
    jsonVariable["paketoperasiobat_id"] = paketoperasiobat_id;
    jsonVariable["paketoperasi_id"] = paketoperasi_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    if(paketoperasi_id != ''){
        jConfirm("Are you sure, you want to delete ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                url: ci_baseurl + "masterdata/paket_operasi/do_delete_obat_paket",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delobat]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            get_table_obat(paketoperasi_id);
                            table_paket_operasi.columns.adjust().draw();
                        } else {
                            $('.upd_modal_notif').removeClass('green').addClass('red');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                        }
                    }
                });
            }
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/paket_operasi/exportToExcel";
}

function showbtnimport() {
    var value = $('#file').val();
    console.log('isi : ' + value);
    if (value != '') {
        console.log('masuk if');
        $('#importData').fadeIn();
    }
} 