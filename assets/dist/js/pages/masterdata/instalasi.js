var table_instalasi;
$(document).ready(function(){
    table_instalasi = $('#table_instalasi_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/instalasi/ajax_list_instalasi",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_instalasi.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});

    $('button#importData').click(function(e){  
      var data = new FormData($(this)[0]);    
      var input = document.getElementById('file');
      var property = input.files[0];          
      var form_data = new FormData();
      form_data.append("file",property);  

          swal({    
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true, 
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',  
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
          }).then(function(){            
              $.ajax({     
                      url: ci_baseurl + "masterdata/instalasi/upload",  
                      data: form_data,                
                      method: 'POST',  
                      type: 'POST',            
                      processData: false,
                      contentType: false,  
                      cache:false,  
                      success: function(data) {  
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_import]').val(data.csrfHash);
                         
                              table_instalasi.columns.adjust().draw();     
                              swal(
                                  'Berhasil!',
                                  'Data Berhasil Diimport.',
                                  'success'
                                )
                           
                      } 
                  });
            
          })
    });

    
    $('button#saveInstalasi').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/instalasi/do_create_instalasi",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateInstalasi').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateInstalasi').find("input[type=text]").val("");
                            $('#fmCreateInstalasi').find("select").prop('selectedIndex',0);
                            $('#modal_add_instalasi').modal('hide');
                        table_instalasi.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.', 
                            'success',
                          ) 
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
            
          });
    });
    
    $('button#changeInstalasi').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/instalasi/do_update_instalasi",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateInstalasi').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            $('#modal_update_instalasi').modal('hide');                            
                        table_instalasi.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Diperbarui.',
                            'success' 
                          ) 
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              ) 
                        }
                    }
                });
            
          });
        
    });
});
function add_instalasi(){
    $('#modal_add_instalasi').modal('show');
} 

function editInstalasi(instalasi_id){
    if(instalasi_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/instalasi/ajax_get_instalasi_by_id",
        type: 'get',
        dataType: 'json',
        data: {instalasi_id:instalasi_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_instalasi').val(data.data.instalasi_id);
                    $('#upd_nama_instalasi').val(data.data.instalasi_nama);
                    $('#modal_update_instalasi').modal('show');
                } else {    
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}
function hapusInstalasi(instalasi_id){
    var jsonVariable = {};
    jsonVariable["instalasi_id"] = instalasi_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(instalasi_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/instalasi/do_delete_instalasi",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                            if(ret === true) {
                                $('.notif').removeClass('red').addClass('green');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            table_instalasi.columns.adjust().draw();
                            } else {
                                $('.notif').removeClass('green').addClass('red');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            }
                        }
                });
            swal( 
              'Berhasil!',
              'Data Berhasil Dihapus.', 
              'success',
            ) 
          }); 
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}
function showbtnimport(){
      var value = $('#file').val();
      console.log('isi : '+value);
      if(value != ''){   
        console.log('masuk if');                 
        $('#importData').fadeIn();  
      }
} 

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/instalasi/exportToExcel";
}