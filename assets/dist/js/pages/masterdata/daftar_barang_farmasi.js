var table_barang_farmasi;
$(document).ready(function(){
    table_barang_farmasi = $('#table_barang_farmasi_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/daftar_barang_farmasi/ajax_list_barang_farmasi",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_barang_farmasi.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    

   
    $('button#saveBarangFarmasi').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/daftar_barang_farmasi/do_create_barang_farmasi",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateBarangFarmasi').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            console.log("masuk benar");
                            $('#modal_add_barang_farmasi').modal('hide');
                            $('#fmCreateBarangFarmasi').find("input[type=text]").val("");
                            $('#fmCreateBarangFarmasi').find("select").prop('selectedIndex',0);
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                            )
                            table_barang_farmasi.columns.adjust().draw();
                       
                        } else {
                            console.log("masuk salah");
                            
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Data Gagal Disimpan ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

    $('button#changeBarangFarmasi').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/daftar_barang_farmasi/do_update_barang_farmasi",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateBarangFarmasi').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_barang_farmasi").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmUpdateBarangFarmasi').find("input[type=text]").val("");
                            $('#fmUpdateBarangFarmasi').find("select").prop('selectedIndex',0);
                            table_barang_farmasi.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });
});


function detailBarangFarmasi(id_barang){
    if(id_barang){    
        $.ajax({
        url: ci_baseurl + "masterdata/daftar_barang_farmasi/ajax_get_barang_farmasi_by_id_detail",
        type: 'get',
        dataType: 'json',
        data: {id_barang:id_barang},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#det_kode_obat').text(data.data.kode_obat);
                    $('#det_golongan').text(data.data.nama_golongan);
                    $('#det_kategori').text(data.data.nama_kategori);
                    $('#det_kelompok').text(data.data.nama_kelompok);
                    $('#det_indikasi').text(data.data.nama_indikasi);
                    $('#det_kontra_indikasi').text(data.data.nama_kontra_indikasi);
                    $('#det_interaksi').text(data.data.nama_interaksi);
                    $('#det_efek_samping').text(data.efeksamping);
                    console.log(data.efeksamping);

                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}


function editBarangFarmasi(id_barang){
    if(id_barang){    
        $.ajax({
        url: ci_baseurl + "masterdata/daftar_barang_farmasi/ajax_get_barang_farmasi_by_id",
        type: 'get',
        dataType: 'json',
        data: {id_barang:id_barang},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_barang').val(data.data.id_barang);
                    $('#upd_nama_barang').val(data.data.nama_barang);
                    $('#upd_kode_barang').val(data.data.kode_barang);
                    $('#upd_id_jenis_barang').val(data.data.id_jenis_barang);
                    $('#upd_id_jenis_barang').select();
                    // var is_bpjs = $('#upd_is_bpjs').val(data.data.is_bpjs); 
                    // $('#upd_is_bpjs').val(data.data.is_bpjs);
                    // if (data.data.status_aktif == 'BPJS') {     
                    if (data.data.is_bpjs == 'BPJS' ){
                        $('#upd_is_bpjs').val(1); 
                        $('#upd_is_bpjs').select();
                    }else{
                        $('#upd_is_bpjs').val(0); 
                        $('#upd_is_bpjs').select();
                    }
                    $('#upd_kode_obat').val(data.data.kode_obat);
                    $('#upd_golongan').val(data.data.id_golongan);
                    $('#upd_golongan').select2();
                    $('#upd_kategori').val(data.data.id_kategori);
                    $('#upd_kategori').select2();
                    $('#upd_kelompok').val(data.data.id_kelompok);
                    $('#upd_kelompok').select2();
                    $('#upd_indikasi').val(data.data.id_indikasi);
                    $('#upd_indikasi').select2();
                    $('#upd_kontra_indikasi').val(data.data.id_kontra_indikasi);
                    $('#upd_kontra_indikasi').select2();
                    $('#upd_interaksi').val(data.data.id_interaksi);
                    $('#upd_interaksi').select2();
                    $('#upd_efek_samping').select2(data.efeksamping);

                    // var det_efeksamping = data.efeksamping;
                    // console.lodet_efeksampingg(det_efelsamping);
                    // det_efeksamping.forEach(function(efek) {
                    //     $('#upd_efek_samping').val(efek);
                    
                    // // $('#upd_efek_samping').select2();
                    
                    // });
                    // $.each(values.split(","), function(i,e){
                    //     // console.log(e);
                    //     $("#upd_efek_samping option[value='" + e + "']").prop("selected", true);
                    // });
                    // $('#upd_efek_samping').val(det_efeksamping);
                    // $('#upd_efek_samping').select2();
                    

                    // $('#upd_efek_samping').val(data.data.id_efek_samping);
                    // $('#upd_efek_samping').select2();


                    // $('#modal_update_barang_farmasi').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusBarangFarmasi(id_barang){
    var jsonVariable = {};
    jsonVariable["id_barang"] = id_barang;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "masterdata/daftar_barang_farmasi/do_delete_barang_farmasi",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_barang_farmasi.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }

        function pilih(){
            var id_jenis_barang = $("#id_jenis_barang").val();
            var upd_id_jenis_barang = $("#upd_id_jenis_barang").val();
            if (id_jenis_barang == 1 || upd_id_jenis_barang == 1){
                $("#obat").show(500);        
                $("#upd_obat").show(500);        
            }else {
                $("#obat").hide(800);  
                $("#upd_obat").hide(800);  
                // $(".per_pieces").show(500);        
                // $(".per_pack").hide(500);   
                // $('.per_pack').find("input[type=number]").val("");       
                // $('.per_pack').find("input[type=text]").val("");         
                // $('.hitung').find("input[type=text]").val(""); 
                // $('.hitung').find("input[type=number]").val("");         
                
            }
            
        }

function tutup() {
    $('#modal_add_barang_farmasi').modal('hide');
    
}