var table_alasan_rujuk;
$(document).ready(function(){
    table_tindakan_lab = $('#table_alasan_rujuk').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/alasan_penolakan/ajax_list_alasan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_tindakan_lab.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    // $('button#importData').click(function (e) {
    //     var data = new FormData($(this)[0]);
    //     var input = document.getElementById('file');
    //     var property = input.files[0];
    //     var form_data = new FormData();
    //     form_data.append("file", property);
    //     swal({
    //         text: 'Apakah file import yang dimasukan sudah benar ?',
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         cancelButtonText: 'Tidak',
    //         confirmButtonText: 'Ya!'
    //     }).then(function () {
    //         $.ajax({
    //             url: ci_baseurl + "masterdata/tindakan_lab/upload",
    //             data: form_data,
    //             method: 'POST',
    //             type: 'POST',
    //             processData: false,
    //             contentType: false,
    //             cache: false,
    //             success: function (data) {
    //                 var ret = data.success;
    //                 $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
    //                 $('input[name=' + data.csrfTokenName + '_import]').val(data.csrfHash);
    //                 swal(
    //                     'Berhasil!',
    //                     'Data Berhasil Diimport.',
    //                     'success'
    //                 )
    //                 table_tindakan_lab.columns.adjust().draw();

    //             }
    //         });

    //     })
    // });

    $('button#saveAlasanRujuk').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/alasan_penolakan/do_create_alasan_rujuk",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateAlasanRujuk').serialize(),
                    
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_alasan_rujuk").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateAlasanRujuk').find("input[type=text]").val("");
                            // $('#fmCreateRencanaTindakan').find("input[type=number]").val("");
                            // $('#fmCreateAlasanRujuk').find("select").prop('selectedIndex',0);
                            table_tindakan_lab.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

    $('button#changeAlasanRujuk').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/alasan_penolakan/do_update_alasan_rujuk",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmEditAlasanRujuk').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_alasan_rujuk").modal('hide');
                            $('#fmEditAlasanRujuk').find("input[type=text]").val("");
                            // $('#fmUpdateRencanaTindakan').find("input[type=number]").val("");
                            table_tindakan_lab.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });
});

function editAlasanRujuk(alasan_rujuk_id){
    if(alasan_rujuk_id){
        $.ajax({
        url: ci_baseurl + "masterdata/alasan_penolakan/ajax_get_alasan_rujuk_by_id",
        type: 'get',
        dataType: 'json',
        data: {alasan_rujuk_id:alasan_rujuk_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_alasan_rujuk_id').val(data.data.alasan_penundaan_id);
                    $('#upd_nama_alasan').val(data.data.nama);
                    $('#upd_kode_alasan').val(data.data.code);
                    $('#upd_keterangan_alasan').val(data.data.keterangan);
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusAlasanRujuk(tindakan_lab_id){
    var jsonVariable = {};
    jsonVariable["alasan_rujuk_id"] = tindakan_lab_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            console.log("masuk delete");
            $.ajax({
                  url: ci_baseurl + "masterdata/alasan_penolakan/do_delete_rencana_tindakan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_tindakan_lab.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success'
            )
          });
        }