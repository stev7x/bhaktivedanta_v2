var table_kelas_poli_ruangan;
$(document).ready(function(){
    table_kelas_poli_ruangan = $('#table_kelas_poli_ruangan_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/kelas_poli_ruangan/ajax_list_kelas_poli_ruangan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_kelas_poli_ruangan.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#importData').click(function (e) {
        var data = new FormData($(this)[0]);
        var input = document.getElementById('file');
        var property = input.files[0];
        var form_data = new FormData();
        form_data.append("file", property);
        swal({
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "masterdata/kelas_poli_ruangan/upload",
                data: form_data,
                method: 'POST',
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_import]').val(data.csrfHash);
                    if (ret === true) {
                        swal(
                            'Berhasil!',
                            'Data Berhasil Diimport.',
                            'success'
                        )
                        table_kelas_poli_ruangan.columns.adjust().draw();

                    } else {
                        swal(
                            'Gagal!',
                            'Data Gagal Diimport.',
                            'error'
                        )
                    }

                }
            });

        })
    });

    $('button#saveKelas_poli_ruangan').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/kelas_poli_ruangan/do_create_kelas_poli_ruangan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateKelas_poli_ruangan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateKelas_poli_ruangan').find("input[type=text]").val("");
                            $('#fmCreateKelas_poli_ruangan').find("select").prop('selectedIndex',0);
                            $('#modal_add_kelas_poli_ruangan').modal('hide');
                        table_kelas_poli_ruangan.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.', 
                            'success',
                          ) 
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
            
          });
        
    });


    $('button#editKelas_poli_ruangan').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/kelas_poli_ruangan/do_update_kelas_poli_ruangan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateKelas_poli_ruangan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('#fmUpdateKelas_poli_ruangan').find("input[type=text]").val("");
                            $('#fmUpdateKelas_poli_ruangan').find("select").prop('selectedIndex',0);
                            $('#modal_update_kelas_poli_ruangan').modal('hide');


                        table_kelas_poli_ruangan.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Diubah.', 
                            'success',
                          ) 
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
            
          });
        
    });
    
    
});

function reloadTablePasien(){
    table_pasienrj.columns.adjust().draw();
}

function add_kelas_poli_ruangan(){
    $('#modal_add_kelas_poli_ruangan').modal('show');
}

function getKelasPelayanan(){ 
    var poliruangan_id = $("#select_poliruangan").val(); 
    // var kelaspelayanan_id = $("#kelas_pelayanan").val();  
    console.log("data :"+poliruangan_id); 
    $.ajax({ 
        url: ci_baseurl + "masterdata/kelas_poli_ruangan/get_kelas_pelayanan", 
        type: 'GET',  
        dataType: 'JSON', 
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {     
        console.log('masuk success'); 
            $("#select_kelaspelayanan").html(data.list); 
        }
    });  
} 

function editPoliRuangan(poliruangan_id,kelaspelayanan_id){
    var jsonVariable = {};
    jsonVariable["poliruangan_id"]      = poliruangan_id;
    jsonVariable["kelaspelayanan_id"]   = kelaspelayanan_id;
    console.log("poliruangan_id = " + poliruangan_id + " kelaspelayanan_id = " + kelaspelayanan_id);
    if(poliruangan_id){    
        $.ajax({  
        url: ci_baseurl + "masterdata/kelas_poli_ruangan/ajax_get_kelas_poli_ruangan_by_id",
        type: 'get',
        dataType: 'json',
        data: jsonVariable,
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);  
                    console.log("hadir = "+data.data.nama_poliruangan);
                    $('#upd_select_poliruangan').val(data.data.poliruangan_id);
                    $('#upd_select_poliruangan').select();
                    $('#upd_select_kelaspelayanan').val(data.data.kelaspelayanan_id);
                    $('#upd_select_kelaspelayanan').select();
                    $('#temp_id_poliruangan').val(data.data.poliruangan_id);
                    $('#temp_id_polikelas').val(data.data.kelaspelayanan_id);


                } else {
                    console.log("belum hadir");

                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}


function hapusKelas_poli_ruangan(poliruangan_id,kelaspelayanan_id){
    var jsonVariable = {};
    jsonVariable["poliruangan_id"]      = poliruangan_id;
    jsonVariable["kelaspelayanan_id"]   = kelaspelayanan_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(poliruangan_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/kelas_poli_ruangan/do_delete_kelas_poli_ruangan",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                            if(ret === true) {
                                $('.notif').removeClass('red').addClass('green');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            table_kelas_poli_ruangan.columns.adjust().draw();
                            } else {
                                $('.notif').removeClass('green').addClass('red');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            }
                        }
                });
            swal( 
              'Berhasil!',
              'Data Berhasil Dihapus.', 
              'success',
            ) 
          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/kelas_poli_ruangan/exportToExcel";
}

function showbtnimport() {
    var value = $('#file').val();
    console.log('isi : ' + value);
    if (value != '') {
        console.log('masuk if');
        $('#importData').fadeIn();
    }
} 