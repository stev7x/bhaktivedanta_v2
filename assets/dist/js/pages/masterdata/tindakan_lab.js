var table_tindakan_lab;
$(document).ready(function(){
    table_tindakan_lab = $('#table_tindakan_lab_list').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/tindakan_lab/ajax_list_tindakan_lab",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_tindakan_lab.columns.adjust().draw();

    $('.tooltipped').tooltip({delay: 50});

});

$('button#importData').click(function (e) {
    var data = new FormData($(this)[0]);
    var input = document.getElementById('file');
    var property = input.files[0];
    var form_data = new FormData();
    form_data.append("file", property);
    swal({
        text: 'Apakah file import yang dimasukan sudah benar ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya!'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/tindakan_lab/upload",
            data: form_data,
            method: 'POST',
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                $('input[name=' + data.csrfTokenName + '_import]').val(data.csrfHash);
                swal(
                    'Berhasil!',
                    'Data Berhasil Diimport.',
                    'success'
                )
                table_tindakan_lab.columns.adjust().draw();

            }
        });

    })
});

$('button#saveTindakanLab').click(function(){
    swal({
      text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText:'Tidak',
      confirmButtonText: 'Ya!'
    }).then(function(){
        $.ajax({
                url: ci_baseurl + "masterdata/tindakan_lab/do_create_tindakan_lab",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateTindakanLab').serialize(),

                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $("#modal_add_tindakan_lab").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmCreateTindakanLab').find("input[type=text]").val("");
                        $('#fmCreateTindakanLab').find("input[type=number]").val("");
                        $('#fmCreateTindakanLab').find("select").prop('selectedIndex',0);
                        table_tindakan_lab.columns.adjust().draw();
                        // clear hasil nilai rujukan
                        tbody_row = '';
                        nilai_rujukan = [];
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                          )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                         swal(
                            'Gagal!',
                            'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                            'error'
                          )
                    }
                }
            });

    })
});

var tbody_row;
var nilai_rujukan = [];
var upd_nilai_rujukan = [];

$('button#tambahNilaiRujukan').click(function(){
    var nilai_rujukan_jenis_kelamin = $('#nilai_rujukan_jenis_kelamin').val();
    var nilai_rujukan_kategori_usia = $('#nilai_rujukan_kategori_usia').val();
    var nilai_rujukan_jenis_cairan = $('#nilai_rujukan_jenis_cairan').val();
    var nilai_rujukan_batas_bawah = $('#nilai_rujukan_batas_bawah').val();
    var nilai_rujukan_batas_atas = $('#nilai_rujukan_batas_atas').val();
    var nilai_rujukan_satuan = $('#nilai_rujukan_satuan').val();
    if(
      nilai_rujukan_jenis_kelamin &&
      nilai_rujukan_kategori_usia &&
      nilai_rujukan_jenis_cairan &&
      nilai_rujukan_batas_bawah &&
      nilai_rujukan_batas_atas &&
      nilai_rujukan_satuan
    ){
      // insert data
      var tindakan_lab_id = $('input#tindakan_lab_id').val();
      nilai_rujukan.push({tindakan_lab_id:tindakan_lab_id,jenis_kelamin:nilai_rujukan_jenis_kelamin,kategori_usia:nilai_rujukan_kategori_usia,jenis_cairan:nilai_rujukan_jenis_cairan,nilai_rujukan_bawah:nilai_rujukan_batas_bawah,nilai_rujukan_atas:nilai_rujukan_batas_atas,satuan:nilai_rujukan_satuan});

      tbody_row += "<tr><td>"+nilai_rujukan_jenis_kelamin+"</td>";
      tbody_row += "<td>"+nilai_rujukan_kategori_usia+"</td>";
      tbody_row += "<td>"+nilai_rujukan_jenis_cairan+"</td>";
      if(nilai_rujukan_batas_bawah == nilai_rujukan_batas_atas){
        tbody_row += "<td>"+nilai_rujukan_batas_bawah+" ";
      }else{
        tbody_row += "<td>"+nilai_rujukan_batas_bawah+" - "+nilai_rujukan_batas_atas+" ";
      }
      tbody_row += nilai_rujukan_satuan+"</td></tr>";

      $("#tbody_nilai_rujukan").html(tbody_row);
      $('#nilai_rujukan_jenis_kelamin').prop('selectedIndex',0);
      $('#nilai_rujukan_kategori_usia').prop('selectedIndex',0);
      $('#nilai_rujukan_jenis_cairan').prop('selectedIndex',0);
      $("#nilai_rujukan_batas_bawah").val(" ");
      $('#nilai_rujukan_batas_atas').val(" ");
      $('#nilai_rujukan_satuan').prop('selectedIndex',0);
      swal('Nilai rujukan telah ditambah');
      $('input#nilai_rujukan_json').val(JSON.stringify(nilai_rujukan));
      console.log(nilai_rujukan);
    }else{
      swal('Data nilai rujukan yang diisi belum lengkap');
      console.log('fail');
    }
});

$('button#resetNilaiRujukan').click(function(){
    tbody_row = '<tr><td colspan=4 align=center>belum ada nilai rujukan</td></tr>';
    $("#tbody_nilai_rujukan").html(tbody_row);
    swal('Nilai rujukan telah dihapus');
    // clear hasil nilai rujukan
    tbody_row = '';
    nilai_rujukan = [];
});

// buat yang update
$('button#upd_tambahNilaiRujukan').click(function(){
    var upd_nilai_rujukan_jenis_kelamin = $('#upd_nilai_rujukan_jenis_kelamin').val();
    var upd_nilai_rujukan_jenis_cairan = $('#upd_nilai_rujukan_jenis_cairan').val();
    var upd_nilai_rujukan_kategori_usia = $('#upd_nilai_rujukan_kategori_usia').val();
    var upd_nilai_rujukan_batas_bawah = $('#upd_nilai_rujukan_batas_bawah').val();
    var upd_nilai_rujukan_batas_atas = $('#upd_nilai_rujukan_batas_atas').val();
    var upd_nilai_rujukan_satuan = $('#upd_nilai_rujukan_satuan').val();
    if(
      upd_nilai_rujukan_jenis_kelamin &&
      upd_nilai_rujukan_kategori_usia &&
      upd_nilai_rujukan_jenis_cairan &&
      upd_nilai_rujukan_batas_bawah &&
      upd_nilai_rujukan_batas_atas &&
      upd_nilai_rujukan_satuan
    ){
      // insert data
      var upd_tindakan_lab_id = $('input#upd_tindakan_lab_id').val();
      nilai_rujukan.push({tindakan_lab_id:upd_tindakan_lab_id,jenis_kelamin:upd_nilai_rujukan_jenis_kelamin,kategori_usia:upd_nilai_rujukan_kategori_usia,jenis_cairan:upd_nilai_rujukan_jenis_cairan,nilai_rujukan_bawah:upd_nilai_rujukan_batas_bawah,nilai_rujukan_atas:upd_nilai_rujukan_batas_atas,satuan:upd_nilai_rujukan_satuan});

      tbody_row += "<tr><td>"+upd_nilai_rujukan_jenis_kelamin+"</td>";
      tbody_row += "<td>"+upd_nilai_rujukan_kategori_usia+"</td>";
      tbody_row += "<td>"+upd_nilai_rujukan_jenis_cairan+"</td>";
      if(upd_nilai_rujukan_batas_bawah == upd_nilai_rujukan_batas_atas){
        tbody_row += "<td>"+upd_nilai_rujukan_batas_bawah+" ";
      }else{
        tbody_row += "<td>"+upd_nilai_rujukan_batas_bawah+" - "+upd_nilai_rujukan_batas_atas+" ";
      }
      tbody_row += upd_nilai_rujukan_satuan+"</td></tr>";

      $("#upd_tbody_nilai_rujukan").html(tbody_row);
      $('#upd_nilai_rujukan_jenis_kelamin').prop('selectedIndex',0);
      $('#upd_nilai_rujukan_kategori_usia').prop('selectedIndex',0);
      $('#upd_nilai_rujukan_jenis_cairan').prop('selectedIndex',0);
      $("#upd_nilai_rujukan_batas_bawah").val(" ");
      $('#upd_nilai_rujukan_batas_atas').val(" ");
      $('#upd_nilai_rujukan_satuan').prop('selectedIndex',0);
      swal('Nilai rujukan telah ditambah');
      $('input#upd_nilai_rujukan_json').val(JSON.stringify(nilai_rujukan));
      console.log(nilai_rujukan);
    }else{
      swal('Data nilai rujukan yang diisi belum lengkap');
      console.log('fail');
    }
});

$('button#upd_resetNilaiRujukan').click(function(){
    tbody_row = '<tr><td colspan=4 align=center>belum ada nilai rujukan</td></tr>';
    $("#upd_tbody_nilai_rujukan").html(tbody_row);
    swal('Nilai rujukan telah dihapus');
    // clear hasil nilai rujukan
    tbody_row = '';
    nilai_rujukan = [];
    $('input#upd_nilai_rujukan_json').val("");
});

$('button#changeTindakanLab').click(function(){
    swal({
      text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText:'Tidak',
      confirmButtonText: 'Ya!'
    }).then(function(){
        $.ajax({
                url: ci_baseurl + "masterdata/tindakan_lab/do_update_tindakan_lab",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmUpdateTindakanLab').serialize(),

                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $("#modal_edit_tindakan_lab").modal('hide');
                        $('#fmUpdateTindakanLab').find("input[type=text]").val("");
                        $('#fmUpdateTindakanLab').find("input[type=number]").val("");
                        $('#fmUpdateTindakanLab').find("select").prop('selectedIndex',0);
                        table_tindakan_lab.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Diperbarui.',
                            'success'
                          )
                    } else {
                        $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message1').html(data.messages);
                        $('#modal_notif1').show();
                        $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif1").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                         swal(
                            'Gagal!',
                            'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                            'error'
                          )
                    }
                }
            });

    })
});

function editTindakanLab(tindakan_lab_id){
    if(tindakan_lab_id){
        $.ajax({
            url: ci_baseurl + "masterdata/tindakan_lab/ajax_get_tindakan_lab_by_id",
            type: 'get',
            dataType: 'json',
            data: {tindakan_lab_id:tindakan_lab_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_tindakan_lab_id').val(data.data.tindakan_lab_id);
                    $('#upd_tindakan_lab_nama').val(data.data.tindakan_lab_nama);
                    $('#upd_kelompoktindakan_lab_id').val(data.data.kelompoktindakan_lab_id);
                    $('#upd_kelompoktindakan_lab_id').select();
                    $('#upd_harga_non_bpjs').val(data.data.harga_non_bpjs);
                    $('#upd_harga_bpjs').val(data.data.harga_bpjs);
                    $('#upd_harga_non_cyto').val(data.data.harga_non_cyto);
                    $('#upd_harga_cyto').val(data.data.harga_cyto);
                    $.ajax({ // ajax lagi buat dapet nilai rujukan
                        url: ci_baseurl + "masterdata/tindakan_lab/ajax_get_nilai_rujukan_tindakan_lab_by_id",
                        type: 'get',
                        dataType: 'json',
                        data: {tindakan_lab_id:tindakan_lab_id},
                        success: function(data) {
                            var ret = data.success;
                            if(ret === true) {
                              console.log(data.data);
                              // update table nilai rujukan di modal
                              tbody_row = '';
                              for (x in data.data) {
                                  console.log(data.data[x]);
                                  tbody_row += "<tr><td>"+data.data[x].jenis_kelamin+"</td>";
                                  tbody_row += "<td>"+data.data[x].kategori_usia+"</td>";
                                  tbody_row += "<td>"+data.data[x].jenis_cairan+"</td>";
                                  if(data.data[x].nilai_rujukan_bawah == data.data[x].nilai_rujukan_atas){
                                    tbody_row += "<td>"+data.data[x].nilai_rujukan_bawah+" ";
                                  }else{
                                    tbody_row += "<td>"+data.data[x].nilai_rujukan_bawah+" - "+data.data[x].nilai_rujukan_atas+" ";
                                  }
                                  tbody_row += data.data[x].satuan+"</td></tr>";
                              }
                              $('input#upd_nilai_rujukan_json').val(JSON.stringify(data.data)); // data di hidden input
                              $("#upd_tbody_nilai_rujukan").html(tbody_row); // draw table nilai rujukan
                              nilai_rujukan = data.data; // insert data ke variable buat update
                            } else {
                              console.log('nilai rujukan kosong');
                              tbody_row = '<tr><td colspan=4 align=center>belum ada nilai rujukan</td></tr>';
                              $("#upd_tbody_nilai_rujukan").html(tbody_row);
                              tbody_row = '';
                            }
                        }
                      });

                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusTindakanLab(tindakan_lab_id){
    var jsonVariable = {};
    jsonVariable["tindakan_lab_id"] = tindakan_lab_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            console.log("masuk delete");
            $.ajax({

                  url: ci_baseurl + "masterdata/tindakan_lab/do_delete_tindakan_lab",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_tindakan_lab.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }

function tambahTindakan(){
  $.ajax({
        url: ci_baseurl + "masterdata/tindakan_lab/get_tindakan_lab_id",
        type: 'get',
        success: function(data) {
          tbody_row = '';
          $("#tindakan_lab_id").val(data);
          // swal(data);
        }
  });
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/tindakan_lab/exportToExcel";
}

function showbtnimport() {
    var value = $('#file').val();
    console.log('isi : ' + value);
    if (value != '') {
        console.log('masuk if');
        $('#importData').fadeIn();
    }
}
