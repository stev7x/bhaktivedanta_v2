var table_kabupaten;
$(document).ready(function(){
    table_kabupaten = $('#table_kabupaten_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/kabupaten/ajax_list_kabupaten",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_kabupaten.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#saveKabupatenOperasi').click(function(){

        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){               
                $.ajax({
                    url: ci_baseurl + "masterdata/kabupaten/do_create_kabupaten",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateKabupatenOperasi').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateKabupatenOperasi').find("input[type=text]").val("");
                            $('#fmCreateKabupatenOperasi').find("select").prop('selectedIndex',0);
                            $('#modal_add_kabupaten').modal("hide");
                            swal( 
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success', 
                              ) 
                        table_kabupaten.columns.adjust().draw();
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
           
          });
    });
    
    $('button#changeKabupatenOperasi').click(function(){

        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){   
                $.ajax({
                    url: ci_baseurl + "masterdata/kabupaten/do_update_kabupaten",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateKabupatenOperasi').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                            $('#modal_update_kabupaten').modal('hide');
                            //$('#fmCreateKabupatenOperasi').find("input[type=text]").val("");
                            //$('#fmCreateKabupatenOperasi').find("select").prop('selectedIndex',0);
                            swal( 
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success', 
                              ) 
                        table_kabupaten.columns.adjust().draw();
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
            
          });
    });
});

function editKabupaten(kabupaten_id){
    if(kabupaten_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/kabupaten/ajax_get_kabupaten_by_id",
        type: 'get',
        dataType: 'json',
        data: {kabupaten_id:kabupaten_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_kabupaten').val(data.data.kabupaten_id);
                    $('#upd_nama_kabupaten').val(data.data.kabupaten_nama);
                    $('#upd_select_privinsi').val(data.data.propinsi_id);
                    // $('#upd_select_privinsi').material_select();
                    $('#label_upd__nama_kabupaten').addClass('active');
                    $('#modal_update_kabupaten').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}
function hapusKabupaten(kabupaten_id){
    var jsonVariable = {};
    jsonVariable["kabupaten_id"] = kabupaten_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(kabupaten_id){

        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
               
                $.ajax({
                    url: ci_baseurl + "masterdata/kabupaten/do_delete_kabupaten",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                            if(ret === true) {
                                $('.notif').removeClass('red').addClass('green');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            table_kabupaten.columns.adjust().draw();
                            } else {
                                $('.notif').removeClass('green').addClass('red');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            }
                        }
                });

            swal( 
              'Berhasil!',
              'Data Berhasil Dihapus.', 
              'success',
            ) 
          })

        
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}