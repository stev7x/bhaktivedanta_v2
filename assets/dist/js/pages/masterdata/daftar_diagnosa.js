var table_daftar_diagnosa;
$(document).ready(function(){
    table_daftar_diagnosa = $('#table_daftar_diagnosa_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true,
      "searching": true,  //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/Daftar_diagnosa/ajax_list_daftar_diagnosa",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      // "columnDefs": [  
      // { 
      //   "targets": [ -1,0 ], //last column
      //   "orderable": false //set not orderable
      // }
      // ]
 
    });
    table_daftar_diagnosa.columns.adjust().draw();
    
//     $('.tooltipped').tooltip({delay: 50});
    $('button#importData').click(function(e){   
      var data = new FormData($(this)[0]);    
      var input = document.getElementById('file');  
      var property = input.files[0];          
      var form_data = new FormData();
      form_data.append("file",property);   

          swal({     
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true,  
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',  
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!' 
          }).then(function(){            
              $.ajax({      
                      url: ci_baseurl + "masterdata/daftar_diagnosa/upload",  
                      data: form_data,                
                      method: 'POST',  
                      type: 'POST',        
                      processData: false,
                      contentType: false,      
                      cache:false,  
                      success: function(data) {  
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_import]').val(data.csrfHash);
                             
                              table_daftar_diagnosa.columns.adjust().draw();   
                              swal(
                                  'Berhasil!',
                                  'Data Berhasil Diimport.',
                                  'success'
                                )
                           
                      } 
                  });
            
          })
    });

    $('button#saveDaftarDiagnosa').click(function(){
         swal({     
            text: 'Apakah data yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true,  
            confirmButtonColor: '#3085d6', 
            cancelButtonColor: '#d33',   
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'     
          }).then(function(){            
              $.ajax({
                    url: ci_baseurl + "masterdata/daftar_diagnosa/do_create_daftar_diagnosa",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateDiagnosa').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        
                        if(ret === true) { 
                            $("#modal_add_daftar_diagnosa").modal('hide');
                            $('#fmCreateDiagnosa').find("input[type=text]").val("");
                            $('#fmCreateDiagnosa').find("select").prop('selectedIndex',0);
                            
                            table_daftar_diagnosa.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
            
          })

    });

    function reloadTableDaftarDiagnosa(){
        table_daftar_diagnosa.columns.adjust().draw();
    }

    $('button#changeDaftarDiagnosa').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/daftar_diagnosa/do_update_daftar_diagnosa",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateDiagnosa').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_daftar_diagnosa").modal('hide');
                       
                            $('#fmUpdateDiagnosa').find("input[type=text]").val("");
                            $('#fmUpdateDiagnosa').find("select").prop('selectedIndex',0);
                            table_daftar_diagnosa.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
        })
    });


});

function editDaftarDiagnosa(diagnosa_id){
    if(diagnosa_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/daftar_diagnosa/ajax_get_daftar_diagnosa_by_id",
        type: 'get',
        dataType: 'json',
        data: {diagnosa_id:diagnosa_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#diagnosa_id').val(data.data.diagnosa_id);
                    $('#upd_kode_diagnosa').val(data.data.kode_diagnosa);
                    $('#upd_nama_diagnosa').val(data.data.nama_diagnosa);
                    $('#upd_kelompokdiagnosa').val(data.data.kelompokdiagnosa_id);
                    $('#upd_kelompokdiagnosa').select2();
                    // $('#label_upd_nama_daftar_tindakan').addClass('active');
                    // $('#modal_update_daftar_tindakan').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    } 
} 

function showbtnimport(){
      var value = $('#file').val();
      console.log('isi : '+value);
      if(value != ''){   
        console.log('masuk if');                 
        $('#importData').fadeIn();  
      }
} 

function hapusDaftarDiagnosa(diagnosa_id){
    var jsonVariable = {};
    jsonVariable["diagnosa_id"] = diagnosa_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "masterdata/daftar_diagnosa/do_delete_daftar_diagnosa",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                           
                            table_daftar_diagnosa.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }
function exportToExcel(){ 
    console.log('masukexport');
      window.location.href = ci_baseurl + "masterdata/daftar_diagnosa/exportToExcel";
  }