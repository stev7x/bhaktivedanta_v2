var table_rencana_tindakan;
$(document).ready(function(){
    table_tindakan_lab = $('#table_rencana_tindakan_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/rencana_tindakan/ajax_list_tindakan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_tindakan_lab.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    // $('button#importData').click(function (e) {
    //     var data = new FormData($(this)[0]);
    //     var input = document.getElementById('file');
    //     var property = input.files[0];
    //     var form_data = new FormData();
    //     form_data.append("file", property);
    //     swal({
    //         text: 'Apakah file import yang dimasukan sudah benar ?',
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         cancelButtonText: 'Tidak',
    //         confirmButtonText: 'Ya!'
    //     }).then(function () {
    //         $.ajax({
    //             url: ci_baseurl + "masterdata/tindakan_lab/upload",
    //             data: form_data,
    //             method: 'POST',
    //             type: 'POST',
    //             processData: false,
    //             contentType: false,
    //             cache: false,
    //             success: function (data) {
    //                 var ret = data.success;
    //                 $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
    //                 $('input[name=' + data.csrfTokenName + '_import]').val(data.csrfHash);
    //                 swal(
    //                     'Berhasil!',
    //                     'Data Berhasil Diimport.',
    //                     'success'
    //                 )
    //                 table_tindakan_lab.columns.adjust().draw();

    //             }
    //         });

    //     })
    // });

    $('button#saveRencanaTindakan').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/rencana_tindakan/do_create_rencana_tindakan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateRencanaTindakan').serialize(),
                    
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_rencana_tindakan").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateRencanaTindakan').find("input[type=text]").val("");
                            // $('#fmCreateRencanaTindakan').find("input[type=number]").val("");
                            $('#fmCreateRencanaTindakan').find("select").prop('selectedIndex',0);
                            table_tindakan_lab.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

    $('button#changeRencanaTindakan').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/rencana_tindakan/do_update_rencana_tindakan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateRencanaTindakan').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_rencana_tindakan").modal('hide');
                            $('#fmUpdateRencanaTindakan').find("input[type=text]").val("");
                            // $('#fmUpdateRencanaTindakan').find("input[type=number]").val("");
                            $('#fmUpdateRencanaTindakan').find("select").prop('selectedIndex',0);
                            table_tindakan_lab.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });
});

function editRencanaTindakan(tindakan_lab_id){
    if(tindakan_lab_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/Rencana_tindakan/ajax_get_rencana_tindakan_by_id",
        type: 'get',
        dataType: 'json',
        data: {rencana_tindakan_id:tindakan_lab_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_rencana_tindakan_id').val(data.data.rencana_tindakan_id);
                    $('#upd_rencana_tindakan_nama').val(data.data.rencana_tindakan_nama);
                    $('#upd_kelompoktindakan_id').val(data.data.kelompoktindakan_id);
                    $('#upd_kelompoktindakan_id').select();
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusRencanaTindakan(tindakan_lab_id){
    var jsonVariable = {};
    jsonVariable["rencana_tindakan_id"] = tindakan_lab_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
            console.log("masuk delete");
            $.ajax({
                  url: ci_baseurl + "masterdata/Rencana_tindakan/do_delete_rencana_tindakan",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_tindakan_lab.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success'
            )
          });
        }