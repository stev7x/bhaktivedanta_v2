var table_dokter;
$(document).ready(function(){
    table_dokter = $('#table_dokter_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/dokter/ajax_list_dokter",
          "type": "GET",
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });
    table_dokter.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});

    $('button#importData').click(function(e){  
      var data = new FormData($(this)[0]);    
      var input = document.getElementById('file');
      var property = input.files[0];          
      var form_data = new FormData(); 
      form_data.append("file",property);  

          swal({    
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true, 
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',  
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
          }).then(function(){            
              $.ajax({      
                      url: ci_baseurl + "masterdata/dokter/upload",  
                      data: form_data,                
                      method: 'POST',  
                      type: 'POST',      
                      processData: false,
                      contentType: false,  
                      cache:false,   
                      success: function(data) {   
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_import]').val(data.csrfHash);
                           
                              table_dokter.columns.adjust().draw();   
                              swal(
                                  'Berhasil!',
                                  'Data Berhasil Diimport.',
                                  'success'
                                )
                           
                      } 
                  });
            
          })
    });
 

    $('button#saveDokter').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/dokter/do_create_dokter",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateDokter').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add_dokter").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateDokter').find("input[type=text]").val("");
                            $('#fmCreateDokter').find("select").prop('selectedIndex',0);
                            table_dokter.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                          console.log("masuk else");
                        }
                    }
                });
         
        })
    }); 

    $('button#changeDokter').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/dokter/do_update_dokter",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateDokter').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit_dokter").modal('hide');
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmUpdateDokter').find("input[type=text]").val("");
                            $('#fmUpdateDokter').find("select").prop('selectedIndex',0);
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )  
                            table_dokter.ajax.reload( null, false );
                            // table_dokter.columns.adjust().draw();
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
         
        })
    });
});


function editDokter(dokter_id){
    if(dokter_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/dokter/ajax_get_dokter_by_id",
        type: 'get',
        dataType: 'json',
        data: {dokter_id:dokter_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_dokter').val(data.data.id_M_DOKTER);
                    $('#upd_nama_dokter').val(data.data.NAME_DOKTER);
                    $('#lbl_upd_nama_dokter').addClass('active');
                    $('#upd_alamat').val(data.data.ALAMAT);
                    $('#lbl_upd_alamat').addClass('active');
                    $('#upd_kel_dokter').val(data.data.kelompokdokter_id);
                    $('#upd_kel_dokter').select();    
                    // $('#upd_jumlah_pasien').val(data.data.jumlah_pasien);
                    // $('#modal_update_dokter').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red'); 
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    } 
}
  

function showbtnimport(){
      var value = $('#file').val();
      console.log('isi : '+value);
      if(value != ''){   
        console.log('masuk if');                 
        $('#importData').fadeIn();  
      }
}


function hapusDokter(dokter_id){
    var jsonVariable = {};
    jsonVariable["dokter_id"] = dokter_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',  
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "masterdata/dokter/do_delete_dokter",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_dokter.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/dokter/exportToExcel";
}