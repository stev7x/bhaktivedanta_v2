var table_kamar;

  
$(document).ready(function(){ 
    table_kamar = $('#table_kamar_list').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/kamar/ajax_list_kamar",
          "type": "GET",
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });
    table_kamar.columns.adjust().draw();

    $('.tooltipped').tooltip({delay: 50});

    $('button#saveKamar').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/kamar/do_create_kamar",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateKamar').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $('#fmCreateKamar').find("input[type=text]").val("");
                            $('#fmCreateKamar').find("input[type=number]").val("");
                            $('#fmCreateKamar').find("select").prop('selectedIndex',0);
                            $('#modal_add_kamar').modal('hide');
                            table_kamar.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                            swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
         
        })
    });

    $('button#changeKamar').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "masterdata/kamar/do_update_kamar",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateKamar').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                                
                            });
                            $("#modal_edit_kamar").modal('hide');
                            $('#fmUpdatekamar').find("input[type=text]").val("");
                            $('#fmUpdatekamar').find("select").prop('selectedIndex',0);
                            table_kamar.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                            swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                          
                        }
                    }
                });
          
        })
    });
    
});


function reloadTableKamar(){
    table_kamar.columns.adjust().draw();
}

function getPelayanan(){
        var ruangan_id = $("#ruangan").val();
    $.ajax({
        url: ci_baseurl + "masterdata/kamar/get_pelayanan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {ruangan_id:ruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
}
function getPelayananid(id,pilih){
      if (id=='undifined') {
        var ruangan_id = $("#upd_ruangan").val();
      }else{
          var ruangan_id = id;
      }
    $.ajax({
        url: ci_baseurl + "masterdata/kamar/get_pelayanan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {ruangan_id:ruangan_id},
        success: function(data) {
            $("#upd_kelas_pelayanan").html(data.list);
            $('#upd_kelas_pelayanan').val(pilih);
            $('#upd_kelas_pelayanan').select();
        }
    });
}   
function isChecked() {
  return this.element.checked;
}; 
function editKamar(kamar_id){
    if(kamar_id){
        $.ajax({
        url: ci_baseurl + "masterdata/kamar/ajax_get_kamar_by_id",
        type: 'get',
        dataType: 'json',
        data: {kamar_id:kamar_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    $('#upd_id_kamar').val(data.data.kamarruangan_id);
                    $('#upd_ruangan').val(data.data.poliruangan_id);
                    $('#upd_ruangan').select();
                    getPelayananid(data.data.poliruangan_id,data.data.kelaspelayanan_id);
                    $('#upd_kelas_pelayanan').val(data.data.kelaspelayanan_id);
                    $('#upd_kelas_pelayanan').select();
                    $('#upd_usia').val(data.data.usia);
                    $('#upd_jenis_kelamin').val(data.data.jenis_kelamin);
                    $('#upd_lokasi').val(data.data.lokasi);      
                    $('#upd_instalasi').val(data.data.instalasi);
                    $('#upd_instalasi').select();
                    $('#upd_status_kamar').val(data.data.status);
                    $('#upd_penanggung_jawab').val(data.data.penanggung_jawab);
                    $('#upd_nomor_kamar').val(data.data.no_kamar);
                    $('#upd_nomor_bed').val(data.data.no_bed);
                    if(data.data.status_aktif=='1') {     
                        $('#upd_status_aktif').prop('checked',true);
                        $('#upd_status_tidak_aktif').prop('checked',false);
                          

                    }else{      
                        console.log('test');      
                        $('#upd_status_aktif').prop('checked',false);   
                        $('#upd_status_tidak_aktif').prop('checked',true);
                        // switchery["upd_status_aktif"].disable();
                        // $('#upd_status_aktif').Switchery('disable');  
                         // switchery["upd_status_aktif"].setPosition(false);
                        // $('#upd_status_aktif').disabled();
                        // $('#upd_status_aktif').removeAttr('checked');
                        // $('#switchery').trigger('click');
                        // $('#upd_status_aktif').Switchery();  
                    }      


                    if (data.data.status_bed=='1') {
                        $('#upd_bed_aktif').prop('checked',true);                        
                        $('#upd_bed_tidak_aktif').prop('checked',false);                        
                      // switchery["upd_status_bed"].setPosition(true);
                        // switchery["upd_status_bed"].enable();
                        // $('#upd_status_bed').switchery('enable');                        
                        // $('#upd_status_bed').enabled();                        
                        // $('#upd_status_bed').prop('checked',true);
                        // $('#upd_status_bed').Switchery('checked'); 
                    }else{                
                        $('#upd_bed_aktif').prop('checked',false);                        
                        $('#upd_bed_tidak_aktif').prop('checked',true);  
                      // switchery["upd_status_bed"].setPosition(false); 
                        // switchery["upd_status_bed"].disable();
                        // $('#upd_status_bed').switchery('disable');                        
                        // $('#upd_status_bed').disabled();
                        // $('#upd_status_bed').removeAttr('checked');
                        // $('#upd_status_bed').Switchery();
                    }
                    $('#upd_keterangan').val(data.data.keterangan);
                    $('#modal_update_kamar').modal('show');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusKamar(kamar_id){
    var jsonVariable = {};
    jsonVariable["kamar_id"] = kamar_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "masterdata/kamar/do_delete_kamar",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_kamar.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/kamar/exportToExcel";
}


