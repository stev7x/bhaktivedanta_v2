var table_list_metode_reservasi;
$(document).ready(function () {

    table_list_metode_reservasi = $('#table_metode_reservasi_list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        "ajax": {
            "url": ci_baseurl + "masterdata/metode_reservasi/ajax_list_metode_reservasi",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [-1, 0], //last column
                "orderable": false //set not orderable
            }
        ]

    });
    table_list_metode_reservasi.columns.adjust().draw();

});


function openmetode_reservasi() {
    $('#form_judul').text("Tambah Data Metode Reservasi");
    $('.form_aksi').removeAttr("id");
    $('.form_aksi').attr("id","fmCreatemetode_reservasi");
    $('.aksi_button').attr("onclick", "savemetode_reservasi()");
    $('#metode_nama').val("");
    $('#metode_persen').val("");
    $('#keterangan').val("");

}

function savemetode_reservasi() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/metode_reservasi/do_create_metode_reservasi",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmCreatemetode_reservasi').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                    );
                    $('#modal_metode_reservasi').modal('hide');
                    $('input[type=text]').val("");
                    table_list_metode_reservasi.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");

                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif").hide();
                    });
                    $("html, modal-content").animate({ scrollTop: 100 }, "fast");

                }
            }
        });
    });
}

function getEditmetode_reservasi(metode_id) {
    $.ajax({
        url: ci_baseurl + "masterdata/metode_reservasi/ajax_get_metode_reservasi_by_id",
        type: 'get',
        dataType: 'json',
        data: { metode_id: metode_id },
        success: function (data) {
            var ret = data.success;
            if (ret === true) {
                console.log("test");
                $('#form_judul').text("Edit Data Metode Reservasi");
                $('.form_aksi').removeAttr("id");
                $('.form_aksi').attr("id", "fmeditmetode_reservasi");
                $('.aksi_button').attr("onclick", "editmetode_reservasi()");
                $('#metode_id').val(data.data.metode_id);
                $('#metode_nama').val(data.data.metode_nama);
                $('#metode_persen').val(data.data.metode_persen);
                $('#keterangan').val(data.data.keterangan);
                $('#keterangan').select();

            } else {
                console.log("Data tidak ditemukan");
            }
        }
    });
}

function editmetode_reservasi() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/metode_reservasi/do_update_metode_reservasi",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmeditmetode_reservasi').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                    );
                    $('#modal_metode_reservasi').modal('hide');
                    table_list_metode_reservasi.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");
                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif").hide();
                    });
                }
            }
        });
    });
}


function hapusmetode_reservasi(metode_id) {
    var jsonVariable = {};
    jsonVariable["metode_id"] = metode_id;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_delda').val();

    swal({
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/metode_reservasi/do_delete_metode_reservasi",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Dihapus.',
                    );
                    table_list_metode_reservasi.columns.adjust().draw();

                } else {
                    console.log("gagal");
                }
            }
        })
    });
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/metode_reservasi/exportToExcel";
}

function formatAsPersen(angka) {
    angka.value =  angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")+ "%";
}