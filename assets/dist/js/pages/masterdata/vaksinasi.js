var table_list_vaksinasi;
$(document).ready(function () {

    table_list_vaksinasi = $('#table_vaksinasi_list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        "ajax": {
            "url": ci_baseurl + "masterdata/vaksinasi/ajax_list_vaksinasi",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [-1, 0], //last column
                "orderable": false //set not orderable
            }
        ]

    });
    table_list_vaksinasi.columns.adjust().draw();

});


function openVaksinasi() {
    $('#form_judul').text("Tambah Data vaksinasi");
    $('.form_aksi').removeAttr("id");
    $('.form_aksi').attr("id", "fmCreateVaksinasi");
    $('.aksi_button').attr("onclick", "saveVaksinasi()");
    $('#vaksinasi').val("");

}

function saveVaksinasi() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/vaksinasi/do_create_vaksinasi",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmCreateVaksinasi').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                    );
                    $('#modal_vaksinasi').modal('hide');
                    $('input[type=text]').val("");
                    table_list_vaksinasi.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");

                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif").hide();
                    });
                    $("html, modal-content").animate({ scrollTop: 100 }, "fast");

                }
            }
        });
    });
}

function getEditVaksinasi(vaksinasi_id) {
    $.ajax({
        url: ci_baseurl + "masterdata/vaksinasi/ajax_get_vaksinasi_by_id",
        type: 'get',
        dataType: 'json',
        data: { vaksinasi_id: vaksinasi_id },
        success: function (data) {
            var ret = data.success;
            if (ret === true) {
                console.log("test");
                $('#form_judul').text("Edit Data Vaksinasi");
                $('.form_aksi').removeAttr("id");
                $('.form_aksi').attr("id", "fmEditVaksinasi");
                $('.aksi_button').attr("onclick", "editVaksinasi()");
                $('#vaksinasi_id').val(data.data.vaksinasi_id);
                $('#vaksinasi_nama').val(data.data.vaksinasi_nama);
                $('#merk').val(data.data.vaksinasi_merk);
                $('#tarif').val(data.data.tarif);
                $('#type').val(data.data.type);
                $('#type').select();

            } else {
                console.log("Data tidak ditemukan");
            }
        }
    });
}

function editVaksinasi() {
    swal({
        text: "Apakah data yang anda masukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/vaksinasi/do_update_vaksinasi",
            type: 'POST',
            dataType: 'JSON',
            data: $('form#fmEditVaksinasi').serialize(),
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Ditambahkan.',
                    );
                    $('#modal_vaksinasi').modal('hide');
                    table_list_vaksinasi.columns.adjust().draw();

                } else {
                    console.log("data gagal ditambahkan");
                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        $("#modal_notif").hide();
                    });
                }
            }
        });
    });
}


function hapusVaksinasi(vaksinasi_id) {
    var jsonVariable = {};
    jsonVariable["vaksinasi_id"] = vaksinasi_id;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_delda').val();

    swal({
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "masterdata/vaksinasi/do_delete_vaksinasi",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);

                if (ret === true) {
                    swal(
                        'Berhasil!',
                        'Data Berhasil Dihapus.',
                    );
                    table_list_vaksinasi.columns.adjust().draw();

                } else {
                    console.log("gagal");
                }
            }
        })
    });
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/vaksinasi/exportToExcel";
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}