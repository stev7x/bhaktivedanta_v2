var table_harga_beli_obat;

function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

$(document).ready(function(){
    table_harga_beli_obat = $('#table_harga_beli_obat_list').DataTable({
        // "processing": true, //Feature control the processing indicator.
        // "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "search": true,
//      "scrollX": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "masterdata/farmasi/harga_beli_obat/ajax_list",
            "type": "GET",
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [ -1,0 ], //last column
                "orderable": false, //set not orderable
            },
        ],

    });

    table_harga_beli_obat.ajax.reload();

    $('.tooltipped').tooltip({delay: 50});

    $('button#saveHargaBeliObat').click(function(){
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "masterdata/farmasi/harga_beli_obat/create",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateHargaBeliObat').serialize(),
                success: function(data) {
                    console.log(data);
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $("#modal_add").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmCreateHargaBeliObat').find("input[type=text]").val("");
                        $('#fmCreateHargaBeliObat').find("select").prop('selectedIndex',0);
                        table_harga_beli_obat.ajax.reload();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                        swal(
                            'Gagal!',
                            'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                            'error'
                        )
                    }
                }
            });

        })
    });

    $('button#updateHargaBeliObat').click(function(){
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "masterdata/farmasi/harga_beli_obat/update",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmUpdateHargaBeliObat').serialize(),

                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $("#modal_edit").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmUpdateharga_beli_obat').find("input[type=text]").val("");
                        $('#fmUpdateharga_beli_obat').find("select").prop('selectedIndex',0);
                        table_harga_beli_obat.ajax.reload();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Diperbarui.',
                            'success'
                        )
                    } else {
                        $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message1').html(data.messages);
                        $('#modal_notif1').show();
                        $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                        swal(
                            'Gagal!',
                            'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                            'error'
                        )
                    }
                }
            });

        })
    });


});

function edit(id) {
    if(id){
        $.ajax({
            url: ci_baseurl + "masterdata/farmasi/harga_beli_obat/ajax_detail",
            type: 'get',
            dataType: 'json',
            data: {id:id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data);
                    var harga_beli_obat = data.data.data;
                    var persediaan = data.data.persediaan;
                    var obat = data.data.obat;
                    var batch = data.data.batch;
                    var suplier = data.data.suplier;

                    $('#edit_modal_label').text("Edit Harga Beli Obat");

                    $('#edit_id').val(harga_beli_obat.id);

                    $('#edit_batch_number').val(batch.number);
                    $('#edit_batch_id').val(batch.id);

                    $('#edit_nama_obat').val(obat.nama);
                    $('#edit_obat_id').val(obat.id);

                    $('#edit_suplier_id').val(suplier.id).change();

                    $('#edit_harga_persediaan_1').val(harga_beli_obat.harga_persediaan_1);
                    $('#edit_harga_persediaan_2').val(harga_beli_obat.harga_persediaan_2);
                    $('#edit_harga_persediaan_3').val(harga_beli_obat.harga_persediaan_3);
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapus(id){
    var jsonVariable = {};
    jsonVariable["id"] = id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
        text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        console.log("masuk delete");
        $.ajax({
            url: ci_baseurl + "masterdata/farmasi/harga_beli_obat/delete",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function(data) {
                var ret = data.success;
                $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                if(ret === true) {
                    $('.notif').removeClass('red').addClass('green');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    table_harga_beli_obat.ajax.reload();
                } else {
                    $('.notif').removeClass('green').addClass('red');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    console.log("gagal");
                }
            }
        });
        swal(
            'Berhasil!',
            'Data Berhasil Dihapus.',
            'success',
        )
    });
}

function setBatch(e) {
    $.ajax({
        url: ci_baseurl + "masterdata/farmasi/harga_beli_obat/ajax_detail_batch",
        type: 'get',
        dataType: 'json',
        data: {id:e.value},
        success: function(data) {
            var ret = data.success;
            if(ret === true) {
                console.log(data.data);
                var persediaan = data.data.persediaan;
                var obat = data.data.obat;

                $('#nama_obat').val(obat.nama);
                $('#obat_id').val(obat.id);

                $('#harga_persediaan_1').attr("placeholder", "Harga per 1 " + persediaan.persediaan_1);
                $('#harga_persediaan_2').attr("placeholder", "Harga per 1 " + persediaan.persediaan_2);
                $('#harga_persediaan_3').attr("placeholder", "Harga per 1 " + persediaan.persediaan_3);
            } else {
                $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                $('#notification_messages').html(data.messages);
                $('#notification_type').show();
            }
        }
    });
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/harga_beli_obat/exportToExcel";
}

function showbtnimport() {
    var value = $('#file').val();
    console.log('isi : ' + value);
    if (value != '') {
        console.log('masuk if');
        $('#importData').fadeIn();
    }
}