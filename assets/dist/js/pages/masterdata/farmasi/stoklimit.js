var table_stoklimit;

function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

$(document).ready(function(){
    table_stoklimit = $('#table_stoklimit_list').DataTable({
        // "processing": true, //Feature control the processing indicator.
        // "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "search": true,
//      "scrollX": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "masterdata/farmasi/stoklimit/ajax_list",
            "type": "GET",
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [ -1,0 ], //last column
                "orderable": false, //set not orderable
            },
        ],

    });
    table_stoklimit.ajax.reload();

    $('.tooltipped').tooltip({delay: 50});

    $('button#saveStoklimit').click(function(){
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "masterdata/farmasi/stoklimit/create",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateStoklimit').serialize(),
                success: function(data) {
                    console.log(data);
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $("#modal_add").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmCreateStoklimit').find("input[type=text]").val("");
                        $('#fmCreateStoklimit').find("select").prop('selectedIndex',0);
                        table_stoklimit.ajax.reload();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                        swal(
                            'Gagal!',
                            'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                            'error'
                        )
                    }
                }
            });

        })
    });

    $('button#updateStoklimit').click(function(){
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "masterdata/farmasi/stoklimit/update",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmUpdateStoklimit').serialize(),

                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $("#modal_edit").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmUpdateStoklimit').find("input[type=text]").val("");
                        $('#fmUpdateStoklimit').find("select").prop('selectedIndex',0);
                        table_stoklimit.ajax.reload();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Diperbarui.',
                            'success'
                        )
                    } else {
                        $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message1').html(data.messages);
                        $('#modal_notif1').show();
                        $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                        swal(
                            'Gagal!',
                            'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                            'error'
                        )
                    }
                }
            });

        })
    });


});

function edit(id) {
    if(id) {
        $.ajax({
            url: ci_baseurl + "masterdata/farmasi/stoklimit/ajax_detail",
            type: 'get',
            dataType: 'json',
            data: {id:id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data);
                    var obat = data.data.obat;
                    var persediaan = data.data.persediaan;
                    var stoklimit = data.data.data;

                    $('#edit_modal_label').text("Edit stoklimit " + obat.nama);

                    $('#edit_id').val(stoklimit.id);
                    $('#edit_obat_id').val(stoklimit.obat_id);
                    $('#edit_nama_obat').val(obat.nama);

                    var min_1 = Math.trunc(stoklimit.min / (obat.jumlah_persediaan_2 * obat.jumlah_persediaan_3));
                    var min_2 = Math.trunc(stoklimit.min % (obat.jumlah_persediaan_2 * obat.jumlah_persediaan_3) / obat.jumlah_persediaan_3);
                    var min_3 = stoklimit.min % obat.jumlah_persediaan_3;

                    var max_1 = Math.trunc(stoklimit.max / (obat.jumlah_persediaan_2 * obat.jumlah_persediaan_3));
                    var max_2 = Math.trunc(stoklimit.max % (obat.jumlah_persediaan_2 * obat.jumlah_persediaan_3) / obat.jumlah_persediaan_3);
                    var max_3 = stoklimit.max % obat.jumlah_persediaan_3;

                    $('#edit_stok_persediaan_1').val(persediaan.persediaan_1);
                    $('#edit_stok_persediaan_2').val(persediaan.persediaan_2);
                    $('#edit_stok_persediaan_3').val(persediaan.persediaan_3);

                    $('#edit_p1').val(min_1);
                    $('#edit_p2').val(min_2);
                    $('#edit_p3').val(min_3);

                    $('#edit_penyesuaian_1').val(max_1);
                    $('#edit_penyesuaian_2').val(max_2);
                    $('#edit_penyesuaian_3').val(max_3);

                    $('#edit_tf_persediaan_1').val(persediaan.persediaan_1);
                    $('#edit_tf_persediaan_2').val(persediaan.persediaan_2);
                    $('#edit_tf_persediaan_3').val(persediaan.persediaan_3);
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapus(id){
    var jsonVariable = {};
    jsonVariable["id"] = id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
        text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        console.log("masuk delete");
        $.ajax({
            url: ci_baseurl + "masterdata/farmasi/stoklimit/delete",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function(data) {
                var ret = data.success;
                $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                if(ret === true) {
                    $('.notif').removeClass('red').addClass('green');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    table_stoklimit.ajax.reload();
                } else {
                    $('.notif').removeClass('green').addClass('red');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    console.log("gagal");
                }
            }
        });
        swal(
            'Berhasil!',
            'Data Berhasil Dihapus.',
            'success',
        )
    });
}

function pilihObat(e){
    $.ajax({
        url: ci_baseurl + "masterdata/farmasi/stoklimit/ajax_obat",
        type: 'get',
        dataType: 'json',
        data: { id:e.value },
        success: function(data) {
            var ret = data.success;
            if (ret == true){
                console.log(data.data);
                var persediaan = data.data.persediaan;
                var data = data.data.data;

                // var stok_1 = Math.floor(data.stok_akhir / (obat.jumlah_persediaan_2 * obat.jumlah_persediaan_3));
                // var stok_2 = Math.floor(data.stok_akhir % (obat.jumlah_persediaan_2 * obat.jumlah_persediaan_3) / obat.jumlah_persediaan_3);
                // var stok_3 = data.stok_akhir % obat.jumlah_persediaan_3;

                // $('#stok_jml_brg_persediaan_1').val(stok_1);
                // $('#stok_jml_brg_persediaan_2').val(stok_2);
                // $('#stok_jml_brg_persediaan_3').val(stok_3);

                $('#stok_persediaan_1').val(persediaan.persediaan_1);
                $('#stok_persediaan_2').val(persediaan.persediaan_2);
                $('#stok_persediaan_3').val(persediaan.persediaan_3);

                $('#p1').attr("placeholder", "Jumlah " + persediaan.persediaan_1);
                $('#p2').attr("placeholder", "Jumlah " + persediaan.persediaan_2);
                $('#p3').attr("placeholder", "Jumlah " + persediaan.persediaan_3);

                $('#penyesuaian_1').attr("placeholder", "Jumlah " + persediaan.persediaan_1);
                $('#penyesuaian_2').attr("placeholder", "Jumlah " + persediaan.persediaan_2);
                $('#penyesuaian_3').attr("placeholder", "Jumlah " + persediaan.persediaan_3);

                $('#tf_persediaan_1').val(persediaan.persediaan_1);
                $('#tf_persediaan_2').val(persediaan.persediaan_2);
                $('#tf_persediaan_3').val(persediaan.persediaan_3);
            }
        }
    });
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/stoklimit/exportToExcel";
}

function showbtnimport() {
    var value = $('#file').val();
    console.log('isi : ' + value);
    if (value != '') {
        console.log('masuk if');
        $('#importData').fadeIn();
    }
}