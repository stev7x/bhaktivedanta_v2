var table_obat;

function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

$(document).ready(function(){
    table_obat = $('#table_obat_list').DataTable({
        // "processing": true, //Feature control the processing indicator.
        // "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "search": true,
        // "paging": true,
//      "scrollX": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "masterdata/farmasi/obat/ajax_list",
            "type": "GET",
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [ -1,0 ], //last column
                "orderable": true, //set not orderable
            },
        ],

    });
    table_obat.ajax.reload();

    $('.tooltipped').tooltip({delay: 50});

    $('button#importData').click(function (e) {
        var data = new FormData($(this)[0]);
        var input = document.getElementById('file');
        var property = input.files[0];
        var form_data = new FormData();
        form_data.append("file", property);
        swal({
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "masterdata/obat/upload",
                data: form_data,
                method: 'POST',
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_import]').val(data.csrfHash);
                    swal(
                        'Berhasil!',
                        'Data Berhasil Diimport.',
                        'success'
                    )
                    table_obat.ajax.reload();



                }
            });

        })
    });

    $('button#saveObat').click(function(){
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "masterdata/farmasi/obat/create",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateObat').serialize(),
                success: function(data) {
                    console.log(data);
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $("#modal_add").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmCreateObat').find("input[type=text]").val("");
                        $('#fmCreateObat').find("select").prop('selectedIndex',0);
                        table_obat.ajax.reload();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        // $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        // $('#card_message').html(data.messages);
                        // $('#modal_notif').show();
                        // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                        //     $("#modal_notif").hide();
                        // });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                        swal(
                            'Gagal!',
                            data.messages,
                            'error'
                        )
                    }
                }
            });

        })
    });

    $('button#updateObat').click(function(){
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "masterdata/farmasi/obat/update",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmUpdateObat').serialize(),

                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if(ret === true) {
                        $("#modal_edit").modal('hide');
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmUpdateObat').find("input[type=text]").val("");
                        $('#fmUpdateObat').find("select").prop('selectedIndex',0);
                        table_obat.ajax.reload();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Diperbarui.',
                            'success'
                        )
                    } else {
                        $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message1').html(data.messages);
                        $('#modal_notif1').show();
                        $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                        swal(
                            'Gagal!',
                            'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                            'error'
                        )
                    }
                }
            });

        })
    });


});

function detail(id) {
    if(id){
        $.ajax({
            url: ci_baseurl + "masterdata/farmasi/obat/ajax_detail",
            type: 'get',
            dataType: 'json',
            data: {id:id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data);

                    var obat = data.data.data;
                    var jenis_barang = data.data.jenis_barang;
                    var persediaan = data.data.persediaan;

                    $('#detail_modal_label').text(data.message + obat.nama);
                    $('#detail_nama_obat').val(obat.nama);
                    $('#detail_jenis_barang').val(jenis_barang.id);
                    $('#detail_persediaan').val(persediaan.id);
                    $('#detail_exp_date').val(obat.exp_date);
                    $('#detail_keterangan').val(obat.keterangan);

                    $('.label_persediaan_1').text(persediaan.persediaan_1);
                    $('.label_persediaan_2').text(persediaan.persediaan_2);
                    $('.label_persediaan_3').text(persediaan.persediaan_3);

                    $('#detail_jumlah_persediaan_1').val(obat.jumlah_persediaan_1);
                    $('#detail_jumlah_persediaan_2').val(obat.jumlah_persediaan_2);
                    $('#detail_jumlah_persediaan_3').val(obat.jumlah_persediaan_3);

                    $('#detail_het_persediaan_1').val(formatRupiah(obat.het_persediaan_1, 'Rp.') + " / 1 " + persediaan.persediaan_1);
                    $('#detail_het_persediaan_2').val(formatRupiah(obat.het_persediaan_2, 'Rp.') + " / 1 " + persediaan.persediaan_2);
                    $('#detail_het_persediaan_3').val(formatRupiah(obat.het_persediaan_3, 'Rp.') + " / 1 " + persediaan.persediaan_3);

                    $('#detail_harga_visit_local').val(formatRupiah(obat.harga_visit_local, 'Rp.'));
                    $('#detail_harga_visit_dome').val(formatRupiah(obat.harga_visit_dome, 'Rp.'));
                    $('#detail_harga_visit_asing').val(formatRupiah(obat.harga_visit_asing, 'Rp.'));

                    $('#detail_harga_oncall_local').val(formatRupiah(obat.harga_oncall_local, 'Rp.'));
                    $('#detail_harga_oncall_dome').val(formatRupiah(obat.harga_oncall_dome, 'Rp.'));
                    $('#detail_harga_oncall_asing').val(formatRupiah(obat.harga_oncall_asing, 'Rp.'));
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function edit(id) {
    if(id){
        $.ajax({
            url: ci_baseurl + "masterdata/farmasi/obat/ajax_detail",
            type: 'get',
            dataType: 'json',
            data: {id:id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data);

                    var obat = data.data.data;
                    var jenis_barang = data.data.jenis_barang;
                    var persediaan = data.data.persediaan;

                    $('#edit_modal_label').text("Edit Obat " + obat.nama);
                    $('#edit_id_obat').val(obat.id);
                    $('#edit_nama_obat').val(obat.nama);
                    $('#edit_jenis_barang').val(jenis_barang.id);
                    $('#edit_persediaan').val(persediaan.id);
                    // $('#edit_exp_date').val(obat.exp_date);
                    $('#edit_keterangan').val(obat.keterangan);

                    $('.label_persediaan_1').text(persediaan.persediaan_1);
                    $('.label_persediaan_2').text(persediaan.persediaan_2);
                    $('.label_persediaan_3').text(persediaan.persediaan_3);

                    $('#edit_jumlah_persediaan_1').val(obat.jumlah_persediaan_1);
                    $('#edit_jumlah_persediaan_2').val(obat.jumlah_persediaan_2);
                    $('#edit_jumlah_persediaan_3').val(obat.jumlah_persediaan_3);

                    $('#edit_het_persediaan_1').val(obat.het_persediaan_1);
                    $('#edit_het_persediaan_2').val(obat.het_persediaan_2);
                    $('#edit_het_persediaan_3').val(obat.het_persediaan_3);

                    $('#edit_harga_visit_local').val(obat.harga_visit_local);
                    $('#edit_harga_visit_dome').val(obat.harga_visit_dome);
                    $('#edit_harga_visit_asing').val(obat.harga_visit_asing);

                    $('#edit_harga_oncall_local').val(obat.harga_oncall_local);
                    $('#edit_harga_oncall_dome').val(obat.harga_oncall_dome);
                    $('#edit_harga_oncall_asing').val(obat.harga_oncall_asing);
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapus(id){
    var jsonVariable = {};
    jsonVariable["id"] = id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
        text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function(){
        console.log("masuk delete");
        $.ajax({
            url: ci_baseurl + "masterdata/farmasi/obat/delete",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function(data) {
                var ret = data.success;
                $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                if(ret === true) {
                    $('.notif').removeClass('red').addClass('green');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    table_obat.ajax.reload();
                } else {
                    $('.notif').removeClass('green').addClass('red');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    console.log("gagal");
                }
            }
        });
        swal(
            'Berhasil!',
            'Data Berhasil Dihapus.',
            'success',
        )
    });
}

function setPersediaan(e) {
    $.ajax({
        url: ci_baseurl + "masterdata/farmasi/persediaan/ajax_get_persediaan_by_id",
        type: 'get',
        dataType: 'json',
        data: {id:e.value},
        success: function(data) {
            var ret = data.success;
            if(ret === true) {
                console.log(data.data);
                $('.label_persediaan_1').text(data.data.persediaan_1);
                $('.label_persediaan_2').text(data.data.persediaan_2);
                $('.label_persediaan_3').text(data.data.persediaan_3);

                $('#jumlah_persediaan_1').val(1);
                $('#jumlah_persediaan_2').attr("placeholder", "jumlah " + data.data.persediaan_2 + " per 1 " + data.data.persediaan_1);
                $('#jumlah_persediaan_3').attr("placeholder", "jumlah " + data.data.persediaan_3 + " per 1 " + data.data.persediaan_2);

                $('#het_persediaan_1').attr("placeholder", "HET " + data.data.persediaan_1);
                $('#het_persediaan_2').attr("placeholder", "HET " + data.data.persediaan_2);
                $('#het_persediaan_3').attr("placeholder", "HET " + data.data.persediaan_3);
            } else {
                $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                $('#notification_messages').html(data.messages);
                $('#notification_type').show();
            }
        }
    });
}

function setPersediaanEdit(e) {
    $.ajax({
        url: ci_baseurl + "masterdata/farmasi/persediaan/ajax_get_persediaan_by_id",
        type: 'get',
        dataType: 'json',
        data: {id:e.value},
        success: function(data) {
            var ret = data.success;
            if(ret === true) {
                console.log(data.data);
                $('.label_persediaan_1').text(data.data.persediaan_1);
                $('.label_persediaan_2').text(data.data.persediaan_2);
                $('.label_persediaan_3').text(data.data.persediaan_3);

                // $('#edit_jumlah_persediaan_2').val("");
                // $('#edit_jumlah_persediaan_3').val("");
                //
                // $('#edit_het_persediaan_1').val("");
                // $('#edit_het_persediaan_2').val("");
                // $('#edit_het_persediaan_3').val("");

                $('#edit_jumlah_persediaan_1').val(1);
                $('#edit_jumlah_persediaan_2').attr("placeholder", "jumlah " + data.data.persediaan_2 + " per 1 " + data.data.persediaan_1);
                $('#edit_jumlah_persediaan_3').attr("placeholder", "jumlah " + data.data.persediaan_3 + " per 1 " + data.data.persediaan_2);

                $('#edit_het_persediaan_1').attr("placeholder", "HET " + data.data.persediaan_1);
                $('#edit_het_persediaan_2').attr("placeholder", "HET " + data.data.persediaan_2);
                $('#edit_het_persediaan_3').attr("placeholder", "HET " + data.data.persediaan_3);
            } else {
                $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                $('#notification_messages').html(data.messages);
                $('#notification_type').show();
            }
        }
    });
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/obat/exportToExcel";
}

function showbtnimport() {
    var value = $('#file').val();
    console.log('isi : ' + value);
    if (value != '') {
        console.log('masuk if');
        $('#importData').fadeIn();
    }
}