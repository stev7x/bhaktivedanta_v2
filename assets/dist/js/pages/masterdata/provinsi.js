var table_provinsi;
$(document).ready(function(){
    table_provinsi = $('#table_provinsi_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/provinsi/ajax_list_provinsi",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_provinsi.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#saveProvinsiOperasi').click(function(){
        swal({
          text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){  
            $.ajax({  
                    url: ci_baseurl + "masterdata/provinsi/do_create_provinsi",
                    type: 'POST',  
                    dataType: 'JSON',
                    data: $('form#fmCreateProvinsiOperasi').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            console.log('masuk');    
                            $("#modal_add_provinsi").modal('hide');    
                            // $("#nama_provinsi").clear();     
                            // $(".bs-example-modal-lg ").hide();        

  
                             
                            // $("#modal_notif").removeClass('hide').fadeTo(2000, 500).slideUp(500, function(){
                            //     $("#modal_notif").hide();
                            // }); 
                            // $('.modal_notif').removeClass('red').addClass('green');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();   
                            // $('#fmCreateProvinsiOperasi').find("input[type=text]").val("");
                            // $('#fmCreateProvinsiOperasi').find("select").prop('selectedIndex',0);
                        table_provinsi.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.', 
                            'success',
                          ) 
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }  
                }); 
          
        })
    });
    
    $('button#changeProvinsiOperasi').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',  
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya'
        }).then(function(){   
             $.ajax({
                    url: ci_baseurl + "masterdata/provinsi/do_update_provinsi",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateProvinsiOperasi').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            console.log('masuk');      
                            $("#modal_update_provinsi").modal('hide');      

                            // $(".modal-open").hide();             
                            // $("#nama_provinsi").clear();     

  
                             
                            // $("#modal_notif").removeClass('hide').fadeTo(2000, 500).slideUp(500, function(){
                            //     $("#modal_notif").hide();
                            // }); 
                            // $('.modal_notif').removeClass('red').addClass('green');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();   
                            // $('#fmCreateProvinsiOperasi').find("input[type=text]").val("");
                            // $('#fmCreateProvinsiOperasi').find("select").prop('selectedIndex',0);
                        table_provinsi.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Diperbarui.',  
                            'success',
                        )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Diperbarui. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });  
          
        })

    }); 
});

function add_provinsi(){
    $('')    
    $('#modal_add_provinsi').modal('show')
} 

function editProvinsi(propinsi_id){
    if(propinsi_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/provinsi/ajax_get_provinsi_by_id",
        type: 'get',
        dataType: 'json',
        data: {propinsi_id:propinsi_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_provinsi').val(data.data.propinsi_id);
                    $('#upd_nama_provinsi').val(data.data.propinsi_nama);
                    // $('#label_upd__nama_provinsi').addClass('active');
                    // $('#modal_update_provinsi').openModal('toggle');
                } else {
                    // $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    // $('#notification_messages').html(data.messages);
                    // $('#notification_type').show();
                }
            }
        });
    }
    else
    {  
        // $('.modal_notif').removeClass('green').addClass('red');
        // $('#modal_card_message').html(data.messages);
        // $('.modal_notif').show();
        // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
        //     $(".modal_notif").hide();
        // });  
    }
}
function hapusProvinsi(propinsi_id){
    var jsonVariable = {};
    jsonVariable["propinsi_id"] = propinsi_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(propinsi_id){
        swal({
          text: "Apakah anda yakin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya'
        }).then(function(){   
            $.ajax({
                url: ci_baseurl + "masterdata/provinsi/do_delete_provinsi",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {      
                            console.log('masuk'); 
                            // $("#modal_add_provinsi").removeClass('show').hide();
                            // $(".modal-backdrop").removeClass('show').hide(); 
                            // $("#nama_provinsi").clear();     
                            // $(".bs-example-modal-lg ").hide();        

  
                             
                            // $("#modal_notif").removeClass('hide').fadeTo(2000, 500).slideUp(500, function(){
                            //     $("#modal_notif").hide();
                            // }); 
                            // $('.modal_notif').removeClass('red').addClass('green');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();   
                            // $('#fmCreateProvinsiOperasi').find("input[type=text]").val("");
                            // $('#fmCreateProvinsiOperasi').find("select").prop('selectedIndex',0);
                        table_provinsi.columns.adjust().draw();
                        } else {
                            // $('.modal_notif').removeClass('green').addClass('red');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();
                            // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            //     $(".modal_notif").hide();
                            // });
                        }
                    }
                }); 
          swal( 
            'Berhasil!',
            'Data Berhasil Dihapus.',  
            'success',
          )
        })
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}