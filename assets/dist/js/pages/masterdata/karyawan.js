var table_karyawan;
$(document).ready(function(){
    table_karyawan = $('#table_list_karyawan').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/karyawan/ajax_list_karyawan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_karyawan.columns.adjust().draw();


});


    function setUmur(){
        var tgl_lahir = $("#tgl_lahir").val();
        $.ajax({         
            url: ci_baseurl + "rekam_medis/pendaftaran/hitung_umur",
            type: 'GET',
            dataType: 'JSON',   
            data: {tgl_lahir:tgl_lahir},
            success: function(data) {
                console.log('masuk');    
                $("#umur").val(data.umur);
                $("#umur").attr('readonly','true');
            }
        });
    }

    function saveKaryawan(){
      swal({
          text: "Apakah data yang anda masukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){
            $.ajax({
                url : ci_baseurl + "masterdata/karyawan/do_create_karyawan",
                type : 'POST',
                dataType : 'JSON',
                data : $('form#fmCreateKaryawan').serialize(),
                success : function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if (ret === true) {
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.', 
                        );
                        $('#modal_karyawan').modal('hide');
                        $('input[type=text]').val("");
                        $('input[type=number]').val("");
                        $('select').prop('selectedIndex','0');
                        table_karyawan.columns.adjust().draw();

                    }else{
                        console.log("data gagal ditambahkan");
                        
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                          $("#modal_notif").hide();
                        });
                        $("html, modal-content").animate({ scrollTop: 100 }, "fast");

                    }
                }
            });
        }); 
    }



function detailKaryawan(karyawan_id){
  $('#modal_detail_karyawan').modal('show');  
  var src = ci_baseurl + "masterdata/karyawan/detail_karyawan/"+karyawan_id;
    $("#modal_detail_karyawan iframe").attr({      
        'src': src,
        'height': 400,
        'width': '100%',
        'allowfullscreen':''
    });     
}

  function hapusKaryawan(karyawan_id){
    var jsonVariable = {};
        jsonVariable["karyawan_id"] = karyawan_id;
        jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();

    swal({
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){  
            $.ajax({
                url: ci_baseurl + "masterdata/karyawan/do_delete_karyawan",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                success : function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);

                    if(ret === true){
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Dihapus.', 
                        );
                        table_karyawan.columns.adjust().draw();

                    }else{
                        console.log("gagal");
                    }
                }
            })
        });
  }

  function tambahKaryawan() {
    $('#form_judul').text("Tambah Data Karyawan");
    $('.form_aksi').removeAttr("id");
    $('.form_aksi').attr("id","fmCreateKaryawan");
    $('.aksi_button').attr("onclick","saveKaryawan()");


    $.ajax({
      url: ci_baseurl + "masterdata/karyawan/get_no",
      type: 'get',
      dataType: 'json',
      success: function(data){
        console.log("i am here : "+ data.next_no_rm);
        // $('#no_rm').val(data.next_no_rm);
        // $('#no_karyawan').val(data.next_no_karyawan);
      }
    });

    $('#fmCreateKaryawan').find("input[type=text]").val("");
    $('#fmCreateKaryawan').find("input[type=email]").val("");
    $('#fmCreateKaryawan').find("input[type=number]").val("");
    $('#fmCreateKaryawan').find("select").prop('selectedIndex',0);

  }

  function editDetailKaryawan(karyawan_id) {
    $.ajax({
      url: ci_baseurl + "masterdata/karyawan/ajax_get_list_karyawan_by_id",
      type: 'get',
      dataType: 'json',
      data: {karyawan_id:karyawan_id},
      success: function(data){
        var ret = data.success;
        if (ret === true) {
          console.log("test");
          $('#form_judul').text("Edit Data Karyawan");
          $('.form_aksi').removeAttr("id");
          $('.form_aksi').attr("id","fmEditKaryawan");
          $('.aksi_button').attr("onclick","editKaryawan()");

          /* Data Karyawan */
          $('#karyawan_id').val(data.data.karyawan_id);
          $('#no_karyawan').val(data.data.no_karyawan);
          $('#no_rm').val(data.data.no_rekam_medis);
          $('#no_ktp').val(data.data.no_ktp);
          $('#nama_lengkap').val(data.data.nama_karyawan);
          $('#nama_panggilan').val(data.data.nama_panggil_karyawan);
          $('#tempat_lahir').val(data.data.tempat_lahir);
          $('#tgl_lahir').val(data.data.tanggal_lahir);
          $('#umur').val(data.data.umur);
          $('#jenis_kelamin').val(data.data.jenis_kelamin);
          $('#alamat').val(data.data.alamat);
          $('#agama').val(data.data.agama_id);
          $('#jabatan').val(data.data.jabatan);
          $('#no_rek_mandiri').val(data.data.no_rek_mandiri);
          $('#no_bpjs').val(data.data.no_bpjs);
          $('#no_bpjs_kerja').val(data.data.no_bpjs_ketker);
          $('#status_kawin').val(data.data.status_kawin);
          $('#email').val(data.data.email);

          /* Data Riwayat Pendidikan Umum */
          $('#strata').val(data.data.strata);
          $('#jurusan').val(data.data.jurusan);
          $('#tahun_lulus_sekolah').val(data.data.tahun_lulus);
          $('#nama_sekolah').val(data.data.nama_sekolah);
          $('#no_ijazah_sekolah').val(data.data.no_ijazah);
          $('#tgl_verif_sekolah').val(data.data.tgl_terverifikasi);

          /* Data Riwayat Pendidika Profesi */
          $('#profesi').val(data.data.profesi);
          $('#spesialis').val(data.data.spesialis);
          $('#tahun_lulus_profesi').val(data.data.tahun);
          $('#no_ijazah_profesi').val(data.data.no_ijazah);
          $('#tgl_verif_profesi').val(data.data.tgl_terverifikasi);

          /* Data Izin Profesi */
          $('#no_str').val(data.data.no_str);
          $('#tgl_terbit_izin').val(data.data.tgl_diterbitkan);
          $('#tgl_berlaku_izin').val(data.data.berlaku_sd);
          $('#tgl_verif_str').val(data.data.tgl_terverifikasi);
          $('#no_sip').val(data.data.no_sip);
          $('#tgl_terbit_sip').val(data.data.tgl_diterbitkan_sip);
          $('#tgl_berlaku_sip').val(data.data.berlaku_sd_sip);
          $('#tgl_verif_sip').val(data.data.tgl_terverifikasi_sip); 
        }else{
          console.log("Data tidak ditemukan");
        }
      } 
    });
  }

  function editKaryawan(){
    swal({
          text: "Apakah data yang anda masukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){
            $.ajax({
                url       : ci_baseurl + "masterdata/karyawan/do_update_karyawan",
                type      : 'POST',
                dataType  : 'JSON',
                data      : $('form#fmEditKaryawan').serialize(),
                success   : function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    // $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if (ret === true) {
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.', 
                        );
                        $('#modal_karyawan').modal('hide');
                        table_karyawan.columns.adjust().draw();

                    }else{
                        console.log("data gagal ditambahkan");
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages); 
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                    }
                }
            });
        }); 
  }

  $('#editKaryawan').click(function(){
        swal({
          text: "Apakah data yang anda masukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){
            $.ajax({
                url       : ci_baseurl + "masterdata/karyawan/do_update_karyawa",
                type      : 'POST',
                dataType  : 'JSON',
                data      : $('form#fmEditKaryawan').serialize(),
                success   : function(data){
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    // $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                    if (ret === true) {
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.', 
                        );
                        table_karyawan.columns.adjust().draw();

                    }else{
                        console.log("data gagal ditambahkan");
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages); 
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                    }
                }
            });
        }); 
  });

  function getRefKary(){
    var table_ref_kary;
    table_ref_kary = $('#table_ref_rm').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "retrieve": true, 


      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/karyawan/ajax_list_ref",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_ref_kary.columns.adjust().draw();
  }

  function pilihRefKaryawan(pasien_id){
    console.log("pasien_id : " + pasien_id);

    $.ajax({
      url: ci_baseurl + "masterdata/karyawan/ajax_get_list_ref_by_id",
      type: 'get',
      dataType: 'json',
      data: {pasien_id:pasien_id},
      success: function(data){
        var ret = data.success;
        if (ret === true) {
          console.log("test : "+ data.data.pasien_nama);
          $('#no_rm').val(data.data.no_rekam_medis);
          $('#no_ktp').val(data.data.no_identitas);
          $('#nama_lengkap').val(data.data.pasien_nama);
          $('#nama_panggilan').val(data.data.nama_panggilan);
          $('#tempat_lahir').val(data.data.tempat_lahir);
          $('#alamat').val(data.data.pasien_alamat);
          $('#tgl_lahir').val(data.data.tanggal_lahir);
          $('#umur').val(data.data.umur);
          $('#jenis_kelamin').val(data.data.jenis_kelamin);
          $('#no_bpjs').val(data.data.no_bpjs);
          $('#agama').val(data.data.agama_id);
          $('#status_kawin').val(data.data.status_kawin);

          $('#modal_ref_rm').modal('hide');
        }else{
          console.log("Data tidak ditemukan");
        }
      } 
    });
  }

  function showbtnimport(){
      var value = $('#file').val();
      console.log('isi : '+value);
      if(value != ''){   
        console.log('masuk if');                
        $('#importData').fadeIn();  
      }
    } 


    function hitung_masakerja() {
        var tgl1 = $("#tgl_berakhir").val();
        var tgl2 = $("#tmt").val();
        var date1 = new Date(tgl1);
        var date2 = new Date(tgl2);
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.round(timeDiff / (1000 * 3600 * 24));
        var tahun = Math.round(diffDays / 365);
        var sisahari = diffDays % 365;
        var bulan = Math.floor(sisahari / 30);
        var hari = sisahari % 30;
        // alert(diffDays);
        $("#masa_kerja").val(tahun+" Tahun "+bulan+" Bulan "+hari+" Hari ");

        
    }

    function hitung_orientasi() {
        var tgl1 = $("#tgl_mulai_orientasi").val();
        var tgl2 = $("#tgl_berakhir_orientasi").val();
        var date1 = new Date(tgl1);
        var date2 = new Date(tgl2);
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.round(timeDiff / (1000 * 3600 * 24));
        var minggu   = Math.round(diffDays / 7);
        var sisahari = diffDays % 7; 
        // var tahun = Math.round(diffDays / 365);
        // var sisahari = diffDays % 365;
        // var bulan = Math.floor(sisahari / 30);
        // var hari = sisahari % 30;
        alert(diffDays);
        $("#lama_orientasi").val(minggu + " Minggu "+sisahari+" Hari");
        // $("#lama_orientasi").val(tahun + " Tahun " + bulan + " Bulan " + hari + " Hari ");
    }

    function hitung_training() {
        var tgl1 = $("#tgl_mulai_training").val();
        var tgl2 = $("#tgl_berakhir_training").val();
        var date1 = new Date(tgl1);
        var date2 = new Date(tgl2);
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.round(timeDiff / (1000 * 3600 * 24));
        var bulan = Math.round(diffDays / 365);
        var sisahari = diffDays % 365;
        var bulan = Math.floor(sisahari / 30);
        var hari = sisahari % 30;
        $("#lama_training").val(tahun + " Tahun " + bulan + " Bulan " + hari + " Hari ");
        // $("#lama_training").val(bulan);
    }