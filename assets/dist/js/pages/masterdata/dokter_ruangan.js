var table_dokter_ruangan;
$(document).ready(function(){
    table_dokter_ruangan = $('#table_dokter_ruangan_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/dokter_ruangan/ajax_list_dokter_ruangan",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_dokter_ruangan.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});

    $('button#importData').click(function(e){  
      var data = new FormData($(this)[0]);    
      var input = document.getElementById('file');
      var property = input.files[0];          
      var form_data = new FormData();
      form_data.append("file",property);  

          swal({    
            text: 'Apakah file import yang dimasukan sudah benar ?',
            type: 'warning',
            showCancelButton: true, 
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',  
            cancelButtonText:'Tidak',
            confirmButtonText: 'Ya!'
          }).then(function(){            
              $.ajax({     
                      url: ci_baseurl + "masterdata/dokter_ruangan/upload",  
                      data: form_data,                 
                      method: 'POST',  
                      type: 'POST',       
                      processData: false,
                      contentType: false,  
                      cache:false,  
                      success: function(data) {  
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_import]').val(data.csrfHash);
                         
                              table_dokter_ruangan.columns.adjust().draw();    
                              swal(
                                  'Berhasil!',
                                  'Data Berhasil Diimport.',
                                  'success'
                                )
                           
                      } 
                  });
            
          })
    });
    
    $('button#savedokter_ruangan').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/dokter_ruangan/do_create_dokter_ruangan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreatedokter_ruangan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreatedokter_ruangan').find("input[type=text]").val("");
                            $('#fmCreatedokter_ruangan').find("select").prop('selectedIndex',0);
                            $('#select_dokter').select2("val", "");
                            $('#select_ruangan').select2("val", "");
                            
                            $('#modal_add_dokter_ruangan').modal('hide');
                        table_dokter_ruangan.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.', 
                            'success',
                        ) 
                        } else {
                           
                            swal( 
                                'Gagal!',
                                'Dokter Sudah menempati ruangan yang sama.', 
                                'error',
                            )
                        }
                    }
                });
            
          });
    });
    
    $('button#changedokter_ruangan').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/dokter_ruangan/do_update_dokter_ruangan",
                    type: 'POST',
                    dataType: 'JSON',
                    data : console.log('masuk sini'),
                    data: $('form#fmUpdatedokter_ruangan').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true){
                            console.log('masuk ah');
                            $('#modal_update_dokter_ruangan').modal('hide'); 
                            $('#upd_select_dokter').select2("val", "");
                            $('#upd_select_ruangan').select2("val", "");                          
                            table_dokter_ruangan.columns.adjust().draw();
                            swal( 
                                'Berhasil!',
                                'Berhasil Diperbarui.', 
                                'success',
                              ) 
                        } else {
                            console.log('masuk else');

                            swal( 
                                'Gagal!',
                                'Dokter Sudah menempati ruangan yang sama.', 
                                'error',
                              ) 
                        }
                    }
                });
            
          });
    });
});

function add_dokter_ruangan(){
    $('#modal_add_dokter_ruangan').modal('show');
}  

function editdokter_ruangan(dokterruangan_id, ruangan_id){
    if(dokterruangan_id && ruangan_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/dokter_ruangan/ajax_get_dokter_ruangan_by_id",
        type: 'get',
        dataType: 'json',
        data: {dokterruangan_id:dokterruangan_id, ruangan_id:ruangan_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);  
                    $('#upd_select_dokter').val(data.data.dokter_id);       
                    $('#upd_select_dokter').select2();                 
                    $('#upd_select_ruangan').val(data.data.poliruangan_id);
                    $('#upd_select_ruangan').select2();
                    $('#temp_dokterruangan_id').val(data.data.dokter_id);
                    $('#temp_ruangan_id').val(data.data.poliruangan_id);
                    $('#modal_update_dokter_ruangan').modal('show');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}


function hapusdokter_ruangan(dokterruangan_id,poliruangan_id){
    var jsonVariable = {};
    jsonVariable["dokterruangan_id"] = dokterruangan_id;
    jsonVariable["poliruangan_id"] = poliruangan_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(dokterruangan_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "masterdata/dokter_ruangan/do_delete_dokter_ruangan",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                            if(ret === true) {
                                $('.notif').removeClass('red').addClass('green');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            table_dokter_ruangan.columns.adjust().draw();
                            } else {
                                $('.notif').removeClass('green').addClass('red');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            }
                        }
                });
            swal( 
              'Berhasil!',
              'Data Berhasil Dihapus.', 
              'success',
            ) 
          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}  

function showbtnimport(){
      var value = $('#file').val();
      console.log('isi : '+value);
      if(value != ''){   
        console.log('masuk if');                 
        $('#importData').fadeIn();  
      }
} 

function exportToExcel() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "masterdata/dokter_ruangan/exportToExcel";
}