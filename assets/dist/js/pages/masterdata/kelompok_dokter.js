var table_kel_dokter;
$(document).ready(function(){
    table_kel_dokter = $('#table_kel_dokter_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "masterdata/kelompok_dokter/ajax_list_kel_dokter",
          "type": "GET",
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });
    table_kel_dokter.columns.adjust().draw();
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#saveKelDokter').click(function(){
        jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                    url: ci_baseurl + "masterdata/kelompok_dokter/do_create_kel_dokter",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateKeldokter').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateKeldokter').find("input[type=text]").val("");
                            $('#fmCreateKeldokter').find("select").prop('selectedIndex',0);
                        table_kel_dokter.columns.adjust().draw();
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                        }
                    }
                });
            }
        });
    });
    $('button#changeKelDokter').click(function(){
        jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                    url: ci_baseurl + "masterdata/kelompok_dokter/do_update_kel_dokter",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateKelDokter').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.upd_modal_notif').removeClass('red').addClass('green');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                        table_kel_dokter.columns.adjust().draw();
                        } else {
                            $('.upd_modal_notif').removeClass('green').addClass('red');
                            $('#upd_modal_card_message').html(data.messages);
                            $('.upd_modal_notif').show();
                            $(".upd_modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".upd_modal_notif").hide();
                            });
                        }
                    }
                });
            }
        });
    });
});


function editKelDokter(kel_dokter_id){
    if(kel_dokter_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/kelompok_dokter/ajax_get_kel_dokter_by_id",
        type: 'get',
        dataType: 'json',
        data: {kel_dokter_id:kel_dokter_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_kel_dokter').val(data.data.kelompokdokter_id);
                    $('#upd_nama_kel_dokter').val(data.data.kelompokdokter_nama);
                    $('#lbl_upd_nama_kel_dokter').addClass('active');
                    $('#modal_update_kel_dokter').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}
function hapusKelDokter(kel_dokter_id){
    var jsonVariable = {};
    jsonVariable["kel_dokter_id"] = kel_dokter_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(kel_dokter_id){
        jConfirm("Are you sure, you want to delete ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                url: ci_baseurl + "masterdata/kelompok_dokter/do_delete_kel_dokter",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                        table_kel_dokter.columns.adjust().draw();
                        } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                        }
                    }
                });
            }
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}