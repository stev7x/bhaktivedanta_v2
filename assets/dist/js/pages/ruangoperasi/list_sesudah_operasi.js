var table_pasienrd;
function cariPasien(){
    table_pasienrd = $('#table_list_pasienok').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "ruangoperasi/List_sesudah_operasi/ajax_list_pasieniok",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.dokter_id = $("#dokter_pj").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_pasienrd.columns.adjust().draw();
}

function reloadTablePasien(){
    table_pasienrd.columns.adjust().draw();
    console.log('tess')
}

function kirimRanap(pendaftaran_id){   
    console.log('jadi')
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "ruangoperasi/List_sesudah_operasi/kirim_ranap/"+pendaftaran_id;
    $("#modal_kirim_ranap_pasienrd iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function ubahKelas(pendaftaran_id){   
    console.log('jadi')
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "ruangoperasi/List_sesudah_operasi/ubah_kelas/"+pendaftaran_id;
    $("#modal_kirim_ubah_kelas iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}

 
$(document).ready(function(){
    table_pasienrd = $('#table_list_pasienok').DataTable({
         "destroy": true,
         "ordering": false,
         "bFilter": false, 
         "processing": true, //Feature control the processing indicator.
         "serverSide": true, //Feature control DataTables' server-side processing mode.
         // "sScrollX": "100%",
 
         // Load data for the table's content from an Ajax source
         "ajax": {
             "url": ci_baseurl + "ruangoperasi/List_sesudah_operasi/ajax_list_pasieniok",
             
             "type": "GET",
             "data": function(d){
                 d.tgl_awal = $("#tgl_awal").val();
                 d.tgl_akhir = $("#tgl_akhir").val();
                 d.poliklinik = $("#poliklinik").val();
                 d.status = $("#status").val();
                 d.nama_pasien = $("#nama_pasien").val();
                 d.dokter_id = $("#dokter_pj").val();
             } 
    
         }, 
         //Set column definition initialisation properties.
     });
 
     table_pasienrd.columns.adjust().draw();
    });

    function cobaTest2(pendaftaran_id){   
        var src = ci_baseurl + "ruangoperasi/list_sesudah_operasi/periksa_pasienrd/"+pendaftaran_id;
        $("#modal_periksa_pasienrd iframe").attr({
            'src': src,
            'height': 380,
            'width': "100%",
            'allowfullscreen':''
        });
    }