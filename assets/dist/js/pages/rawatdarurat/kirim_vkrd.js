var table_diagnosa_rd_vk;
var table_tindakan_rd_vk;
var table_background_rd_vk;
var table_obat_rd;
var table_assestment_rd_vk;
var theraphyArr = [];
var paketOperasiArr = [];
$(document).ready(function(){
    getTindakan();
    table_diagnosa_rd_vk = $('#table_diagnosa_pasienrd_vkrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,
 
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      { 
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_diagnosa_rd_vk.columns.adjust().draw();


    table_tindakan_rd_vk = $('#table_tindakan_pasienrd_vkrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_tindakan_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rd_vk.columns.adjust().draw();


    table_background_rd_vk = $('#table_background_pasienrd_vkrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_background_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_background_rd_vk.columns.adjust().draw();

 table_assestment_rd_vk = $('#table_assestment_pasienrd_vkrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_assestment_pasien_list",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_assestment_rd_vk.columns.adjust().draw();

});


    // var year = new Date().getFullYear();
    // $('.datepicker_diagnosa').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    
    // $('.datepicker_tindakan').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    // $('.datepicker_resep').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });

    function getKelasPoliruanganVk(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
}

function getKamarVk(){
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
}
    
    $('#nama_obat').keyup(function() {  
        var val_obat = $('#nama_obat').val();
        $.ajax({
            url: ci_baseurl + "rawatdarurat/pasien_rd/autocomplete_obat",
            type: 'get',
            async: false,
            dataType: 'json',
            data: {val_obat: val_obat},
            success: function(data) { /* console.log(data); */
                var ret = data.success;
                if(ret == true) {
                    // console.log(data.data);
                    $(".autocomplete").autocomplete({
                        source: data.data,
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 0");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index","999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#obat_id').val(ui.item.id);
                            $('#nama_obat').val(ui.item.value);
                            $('#satuan_obat').val(ui.item.satuan);
                            $('#harga_jual').val(ui.item.harga_jual);
                            $('#harga_netto').val(ui.item.harga_netto);
                            $('#current_stok').val(ui.item.current_stok);
                            $('#lbl_nama_obat').addClass('active');
                            $('#lbl_harga_jual').addClass('active');
                            $('#lbl_satuan_obat').addClass('active');
                        }
                  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                      return $( "<li>" )
                        .append( "<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.deskripsi +"- Stok :"+ item.current_stok +"</font></a>" )
                        .appendTo( ul );
                    };
                }else{
                  $("#harga_netto").val('');
                  $("#current_stok").val('');
                  $("#obat_id").val('');
                }
            }
        });
    });

    function saveDiagnosa(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{
            swal({
                text: "Apakah data yang dimasukan sudah benar ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'tidak',
                confirmButtonText: 'Ya' 
              }).then(function(){  
                    $.ajax({
                        url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_diagnosa_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                                $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
                            table_diagnosa_rd.columns.adjust().draw();
                            swal( 
                                   'Berhasil!',
                                   'Data Berhasil Disimpan.',
                                   'success' 
                                ) 
                            } else {
                                $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message1').html(data.messages);
                                $('#modal_notif1').show();
                                $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                                $("#modal_notif1").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                                // swal(
                                //     'Gagal!',
                                //     'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                //     'error'
                                //   )
                            }
                        }
                    });  
                
              });
            
        }
    }

    $('button#saveDiagnosa').click(function(){
        
    });
function hapusDiagnosa(diagnosapasien_id){
    var jsonVariable = {};
    jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if(diagnosapasien_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
            }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_diagnosa",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_diagnosa_rd.columns.adjust().draw();

                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.',
                                    'success' 
                                    ) 
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            }
                        }
                });
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}
function dialogObat(){
    // $('#modal_list_obat').openModal('toggle');
    if(table_obat_rd){
        table_obat_rd.destroy();
    }
    table_obat_rd = $('#table_list_obat').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false, 

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_obat",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ] 
    });
    table_obat_rd.columns.adjust().draw();
}

function pilihObat(kode_barang){
    if (kode_barang != ""){
        $.ajax({
            url: ci_baseurl + "rawatdarurat/pasien_rd/ajax_get_obat_by_id",
            type: 'get',
            dataType: 'json',
            data: { kode_barang: kode_barang },
            success: function(data) {
                var ret = data.success;
                if (ret == true){
                    $('#obat_id').val(data.data['kode_barang']);
                    $('#nama_obat').val(data.data['nama_barang']);
                    $('#satuan_obat').val(data.data['id_sediaan']);
                    $('#harga_jual').val(data.data['harga_satuan']);
                    $('#modal_reseptur').modal('hide');
                } else {
                    $("#harga_netto").val('');
                    $("#current_stok").val('');
                    $("#obat_id").val('');
                }
            }
        });
    } else {
        // $('#modal_list_obat').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}


function pilihRSRujukan(oid){
    
      if (oid != ""){
    
        $.ajax({
          url: ci_baseurl + "rekam_medis/pasienrj/ajax_get_rs_rujukan_by_id",
          type: 'get',
          dataType: 'json',
          data: { rs_rujukan_id:oid },
          success: function(data) {
            var ret = data.success;
    
            if (ret == true){
              
              $('#rs_rujukan_id').val(data.data['rs_rujukan_id']);
              $('#kode_rs').val(data.data['kode_rs']);
              $('#nama_rs').val(data.data['nama_rs']);
              $('#alamat').val(data.data['alamat']);
              $('#lbl_kode_rs').addClass('active');
              $('#lbl_nama_rs').addClass('active');
              $('#lbl_alamat').addClass('active');
              $("#modal_rs_rujukan").modal('hide');
            } else {
    
              // $("#harga_netto").val('');
              // $("#current_stok").val('');
              // $("#obat_id").val('');
            }
          }
        });
      } else {
        // $('#modal_diagnosaa').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html("RS Rujukan ID Kosong");
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
                $(".modal_notif").hide();
            });
      }
    }

function is_rujuk(){
    var carapulang = $("#status_pulang").val();
    if(carapulang == 'Rujuk'){
        $('#rujukan_rs').removeAttr('style');
    }else{
        $('#rujukan_rs').attr('style','display:none;');
    }
}

function changeDataTheraphyVk() {
    let 
        //id_rencana_tindakan = $("#pilihtindakan").val(),
        id_nama_paket = $("#nama_program").val();
         nama_paket= $("#nama_therapy").val();
       
    
    if (id_nama_paket == "" || nama_paket == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        theraphyArr.push({
            //id_rencana_tindakan:id_rencana_tindakan,
            id_nama_paket :id_nama_paket,
            nama_paket :nama_paket,
            //kiri table kana values table
        });
        updateDataTheraphyVk();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}


function updateDataTheraphyVk() {
    let i, 
    tablePaket = document.getElementById("table_theraphy_vkrd"),
    tr, td;
    tablePaket.innerHTML = "";
    if (theraphyArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tablePaket.appendChild(tr);
        return;
    }
    for (i = 0; i < theraphyArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = theraphyArr[i].nama_paket;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataTheraphyVk("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tablePaket.appendChild(tr);
    }
}

function removeDataTheraphyVk(position) {
    theraphyArr.splice(position, 1)
    updateDataTheraphyVk()
}



function changeDataRencanaTindakanVk() {
    let 
        //id_rencana_tindakan = $("#pilihtindakan").val(),
        id_nama_paket = $("#nama_paket").val();
         nama_paket= $("#paket_op").val();
       
    
    if (id_nama_paket == "" || nama_paket == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        paketOperasiArr.push({
            //id_rencana_tindakan:id_rencana_tindakan,
            id_nama_paket :id_nama_paket,
            nama_paket :nama_paket,
            //kiri table kana values table
        });
        updateDataRencanaTindakanVk();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);

}


function updateDataRencanaTindakanVk() {
    let i, 
    tablePaket = document.getElementById("table_paket_rencanatindak"),
    tr, td;
    tablePaket.innerHTML = "";
    if (paketOperasiArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tablePaket.appendChild(tr);
        return;
    }
    for (i = 0; i < paketOperasiArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = paketOperasiArr[i].nama_paket;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataRencanaTindakanVk("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tablePaket.appendChild(tr);
    }
}

function removeDataRencanaTindakanVk(position) {
    paketOperasiArr.splice(position, 1)
    updateDataRencanaTindakanVk()
}


function saveVkrd(){
  console.log('simpan')
          pendaftaran_id = $("#pendaftaran_id").val();
         poli_ruangan = $("#poli_ruangan").val();
         kelas_pelayanan = $("#kelas_pelayanan").val();
         kamar = $("#kamarruangan").val();
         catatan = $("#catatan").val();
         instalasi = $("#instalasi_id").val();
         jamaperiksa = $("#jam_periksa").val();

         //komen = $("#komen").val();
         var paketoperasi = JSON.stringify(paketOperasiArr);
         let formData = new FormData();
         formData.append("pendaftaran_id", pendaftaran_id);
         formData.append("poli_ruangan", poli_ruangan);
         formData.append("kelas_pelayanan", kelas_pelayanan);
         formData.append("kamarruangan", kamar);
         formData.append("catatan", catatan);
         formData.append("instalasi_id", instalasi);
         formData.append("jam_periksa", jamaperiksa);
         formData.append("paketOperasiArr", paketoperasi);
         console.log(poli_ruangan)
         console.log(kelas_pelayanan)
         console.log(kamar)
         console.log(catatan)
         console.log(instalasi)
         console.log(jamaperiksa)
         console.log(paketoperasi)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                console.log("jalan")
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_vkrd",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data) {
                        console.log('simpan')
                        data = JSON.parse(data);
                        console.log(data.ret);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                          paketOperasiArr = [];
                          document.getElementById("table_paket_rencanatindak").innerHTML = "";
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });
    
}



function saveAssestmentVk(){
  console.log('simpan')
          pendaftaran_id = $("#pendaftaran_id").val();
         tensi = $("#tensivk").val();
         nadi = $("#nadivk").val();
         suhu = $("#suhuvk").val();
         nadi_1 = $("#nadi_1vk").val();
         penggunaan = $("#penggunaanvk").val();
         saturasi = $("#saturasivk").val();
         nyeri = $("#nama_nyerivk").val();
         numeric = $("#numericvk").val();
         resiko = $("#resikovk").val();
         var theraphy = JSON.stringify(theraphyArr);
         let formData = new FormData();
         formData.append("pendaftaran_id", pendaftaran_id);
         formData.append("tensivk", tensi);
         formData.append("nadivk", nadi);
         formData.append("suhuvk", suhu);
         formData.append("nadi_1vk", nadi_1);
         formData.append("penggunaanvk", penggunaan);
         formData.append("saturasivk", saturasi);
         formData.append("nama_nyerivk", nyeri);
         formData.append("numericvk", numeric);
         formData.append("resikovk", resiko);
         formData.append("theraphyArr", theraphy);
         console.log(nadi)
         console.log(suhu)
         console.log(nadi_1)
         console.log(penggunaan)
         console.log(saturasi)
         console.log(numeric)
         console.log(resiko)
         console.log(theraphy)
         console.log(nyeri)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                console.log("jalan")
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_assestment_vk",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data) {
                        data = JSON.parse(data);
                        console.log(data);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                                 $("#tensivk").val("");
                                 $("#nadivk").val("");
                                $("#suhuvk").val("");
                                $("#nadi_1vk").val("");
                                $("#penggunaanvk").val("");
                                $("#saturasivk").val("");
                                $("#resikovk").val("");
                          theraphyArr = [];
                          document.getElementById("table_theraphy_vkrd").innerHTML = "";
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });


    
}
