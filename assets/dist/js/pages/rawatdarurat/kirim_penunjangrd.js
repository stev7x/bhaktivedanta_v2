var rencanaTindakanArr = [];
var theraphyArr = [];
var table_diagnosa_rd;
var table_tindakan_rd;
var table_background_rd;
var table_resep_rd;
var table_obat_rd;
var table_assestment_rd;
$(document).ready(function(){
    getTindakan();
    table_diagnosa_rd = $('#table_diagnosa_pasienrd_penunjangrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,
 
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      { 
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_diagnosa_rd.columns.adjust().draw();


    table_tindakan_rd = $('#table_tindakan_pasienrd_penunjangrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_tindakan_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rd.columns.adjust().draw();


    table_background_rd = $('#table_background_pasienrd_penunjangrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_background_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_background_rd.columns.adjust().draw();


    table_assestment_rd = $('#table_assestment_penunjangrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_assestment_pasien_list",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_assestment_rd.columns.adjust().draw();

});

    // var year = new Date().getFullYear();
    // $('.datepicker_diagnosa').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    
    // $('.datepicker_tindakan').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    // $('.datepicker_resep').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });  

    function getPermintaanTindakanPenunjang(){
  //console.log('adwa')
    var penunjang_id = $("#penunjang_id").val();
    console.log(penunjang_id);
    
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_permintaan_tindakan_list_penunjang",
        type: 'GET',
        dataType: 'JSON',
        data: {penunjang_id:penunjang_id},
        success: function(data) {
           $("#pilihtindakanpenunjang").html(data.list);
           // console.log(data.list)
        }
    });
    
           rencanaTindakanArr = [];
           updateDataRencanaTindakanPenunjangRd();
}


    function saveTindakann(){
        console.log(pendaftaran_id = $("#pendaftaran_id").val());
        console.log(pasien_id   = $("#pasien_id").val());   
        console.log(dokter = $("#dokter_periksa").val());
        console.log(kelas_pelayanan =$("#kelaspelayanan_id").val());
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        kelas_pelayanan =$("#kelaspelayanan_id").val();
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_tindakan_pasien_penunjang",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter+ "&kelas_pelayanan=" +kelas_pelayanan,
                    success: function(data) {
                        var ret = data.success;
                        console.log(data)
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                            $(".select2").val(null).trigger("change");
                            $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                            $('#fmCreateTindakanPasien').find("input[type=number]").val("");
                            $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',0);
                         var table_tindakan_rd = $('#table_tindakan_pasienrd').DataTable({ 
                          "processing": true, //Feature control the processing indicator.
                          "serverSide": true, //Feature control DataTables' server-side processing mode.
                          //"scrollX": true,
                          "paging": false,
                          "sDom": '<"top"l>rt<"bottom"p><"clear">',
                          "ordering": false,

                          // Load data for the table's content from an Ajax source
                          "ajax": {
                              "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_tindakan_pasien",
                              "type": "GET",
                              "data" : function(d){
                                  d.pendaftaran_id = $("#pendaftaran_id").val();
                              }
                          },
                        });
                        table_tindakan_rd.columns.adjust().draw();


                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });
    }


function getTindakan(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    });
}

function getTarifTindakan(){
    var daftartindakan_id = $("#tindakan").val();
   // console.log($("#tindakan").val())
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {daftartindakan_id:daftartindakan_id},
        success: function(data) {
            $("#harga_tindakan").val(data.list.harga_tindakan);
            $("#harga_cyto").val(data.list.cyto_tindakan);
        }
    });
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();
    
    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $("#subtotal").val(subtotal);
    $("#totalharga").val(total);
}
function changeDataRencanaTindakanPenunjangRd() {
    let 
        id_rencana_tindakan = $("#pilihtindakanpenunjang").val(),
        nama_rencana_tindakan = $("#tindakan").val();
       
    
    if (id_rencana_tindakan == "" || nama_rencana_tindakan == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        rencanaTindakanArr.push({
            id_rencana_tindakan:id_rencana_tindakan,
            nama_rencana_tindakan :nama_rencana_tindakan,
            //kiri table kana values table
        });
        updateDataRencanaTindakanPenunjangRd();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}
function updateDataRencanaTindakanPenunjangRd() {
    let i, 
    tableRencana = document.getElementById("table_penununjang_pasienrd"),
    tr, td;
    tableRencana.innerHTML = "";
    if (rencanaTindakanArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tableRencana.appendChild(tr);
        return;
    }
    for (i = 0; i < rencanaTindakanArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = rencanaTindakanArr[i].nama_rencana_tindakan;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataRencanaTindakanPenunjangRd("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tableRencana.appendChild(tr);
    }
}

function removeDataRencanaTindakanPenunjangRd(position) {
    rencanaTindakanArr.splice(position, 1)
    updateDataRencanaTindakanPenunjangRd()
}

function savePenunjangRd(){
  //console.log('simpan')
        pendaftaran_id = $("#pendaftaran_id_").val();
         penunjang_id = $("#penunjang_id").val();
         catatan = $("#catatan").val();
         instalasi_id = $("#instalasi_id").val();
         var rencanatindakan = JSON.stringify(rencanaTindakanArr);
         let formData = new FormData();
         formData.append("pendaftaran_id_", pendaftaran_id);
         formData.append("penunjang_id", penunjang_id);
         formData.append("catatan", catatan);
          formData.append("instalasi_id", instalasi_id);
         formData.append("rencanaTindakanArr", rencanatindakan);

         console.log(pendaftaran_id)
         console.log(penunjang_id)
         console.log(catatan)
          console.log(instalasi_id)
          console.log(rencanatindakan)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){
          console.log('jalan')  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/kirim_ke_penunjang",
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function(data) {
                        console.log('simpan')
                        data = JSON.parse(data);
                        console.log(data);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                          rencanaTindakanArr = [];
                          document.getElementById("table_penununjang_pasienrd").innerHTML = "";
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });
    
}


