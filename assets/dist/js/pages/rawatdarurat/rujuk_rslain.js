var table_diagnosa_rd_rujukrslain;
var table_tindakan_rd_rujukrslain;
var table_background_rd_rujukrslain;
var table_assestment_rd_rujukrslain;
var theraphyArr = [];
var paketOperasiArr = [];

$(document).ready(function(){
    getTindakan();
    table_diagnosa_rd_rujukrslain = $('#table_diagnosa_pasienrd_rujukrslain').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,
 
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      { 
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_diagnosa_rd_rujukrslain.columns.adjust().draw();


    table_tindakan_rd_rujukrslain = $('#table_tindakan_pasienrd_rujukrslain').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_tindakan_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rd_rujukrslain.columns.adjust().draw();


    table_background_rd_rujukrslain = $('#table_background_pasienrd_rujukrslain').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_background_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_background_rd_rujukrslain.columns.adjust().draw();
});


function changeDataTheraphyRujukrslain() {
  //console.log('rtrs')
    let 
        //id_rencana_tindakan = $("#pilihtindakan").val(),
        id_nama_paket = $("#nama_program").val();
         nama_paket= $("#nama_therapy").val();
       
    
    if (id_nama_paket == "" || nama_paket == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        theraphyArr.push({
            //id_rencana_tindakan:id_rencana_tindakan,
            id_nama_paket :id_nama_paket,
            nama_paket :nama_paket,
            //kiri table kana values table
        });
        updateDataTheraphyRujukrslain();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}


function updateDataTheraphyRujukrslain() {
    let i, 
    tablePaket = document.getElementById("table_theraphy_rujukrslain"),
    tr, td;
    tablePaket.innerHTML = "";
    if (theraphyArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tablePaket.appendChild(tr);
        return;
    }
    for (i = 0; i < theraphyArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = theraphyArr[i].nama_paket;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataTheraphyRujukrslain("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tablePaket.appendChild(tr);
    }
}

function removeDataTheraphyRujukrslain(position) {
    theraphyArr.splice(position, 1)
    updateDataTheraphyRujukrslain()
}


function saveAssestmentRujukrslain(){
  //console.log('simpan')
        pendaftaran_id = $("#pendaftaran_id").val();
         tensi = $("#tensirujuk").val();
         nadi = $("#nadirujuk").val();
         suhu = $("#suhurujuk").val();
         nadi_1 = $("#nadi_1rujuk").val();
         penggunaan = $("#penggunaanrujuk").val();
         saturasi = $("#saturasirujuk").val();
         nyeri = $("#nama_nyerirujuk").val();
         numeric = $("#numericrujuk").val();
         resiko = $("#resikorujuk").val();
         var theraphy = JSON.stringify(theraphyArr);
         let formData = new FormData();
         formData.append("pendaftaran_id", pendaftaran_id);
         formData.append("tensirujuk", tensi);
         formData.append("nadirujuk", nadi);
         formData.append("suhurujuk", suhu);
         formData.append("nadi_1rujuk", nadi_1);
         formData.append("penggunaanrujuk", penggunaan);
         formData.append("saturasirujuk", saturasi);
         formData.append("nama_nyerirujuk", nyeri);
         formData.append("numericrujuk", numeric);
         formData.append("resikorujuk", resiko);
         formData.append("theraphyArr", theraphy);
         console.log(tensi)
         console.log(nadi)
         console.log(suhu)
         console.log(nadi_1)
         console.log(penggunaan)
         console.log(saturasi)
         console.log(numeric)
         console.log(resiko)
         console.log(theraphy)
         console.log(nyeri)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                console.log("jalan")
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_assestment_rujukrs",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data) {
                    	console.log('Simpan');
                        data = JSON.parse(data);
                        console.log(data);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                                 $("#tensirujuk").val("");
                                 $("#nadirujuk").val("");
                                $("#suhurujuk").val("");
                                $("#nadi_1rujuk").val("");
                                $("#penggunaanrujuk").val("");
                                $("#saturasirujuk").val("");
                                $("#resikorujuk").val("");
                          theraphyArr = [];
                          document.getElementById("table_theraphy_rujukrslain").innerHTML = "";
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });   
}


function saveRujukrslain(){
 // console.log('simpan')
         pendaftaran_id = $("#pendaftaran_id").val();
         halperlu = $("#halperlu").val();
         catatan = $("#catatan").val();
         alasanrujuk = $("#alasanrujuk").val();
         instalasi = $("#instalasi_id").val();
        
         //komen = $("#komen").val();
         let formData = new FormData();
         formData.append("pendaftaran_id", pendaftaran_id);
         formData.append("halperlu", halperlu);
         formData.append("catatan", catatan);
         formData.append("alasanrujuk", alasanrujuk);
         formData.append("instalasi_id", instalasi);
         console.log(halperlu)
         console.log(catatan)
         console.log(alasanrujuk)
         console.log(instalasi)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                console.log("jalan")
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_rujukrslain",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data) {
                        console.log('simpan')
                        data = JSON.parse(data);
                        console.log(data.ret);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });
    
}



function coba() {
	// body...
	console.log('tesss')
}