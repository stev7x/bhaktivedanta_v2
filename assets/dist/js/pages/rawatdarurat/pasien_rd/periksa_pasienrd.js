// console.log('periksa_pasienrd LOADED');
var table_diagnosa_rd;
var table_asessment_therapy_rd;
var table_tindakan_rd;
var table_background_rd;
var table_resep_rd;
var table_obat_rd;
var table_list_dokter_rd;
var table_list_diagnosa_rd;
var table_list_diagnosa_icd10_rd;
var table_rs_rujukan;
var resepRacikanArr = [];

var table_list_user_tindakan;
var table_list_dokter_tindakan;
$(document).ready(function(){
    getTindakan();
    $("#obat_id").val('0#');
    table_diagnosa_rd = $('#table_diagnosa_pasienrd_periksa').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
    });
    table_diagnosa_rd.columns.adjust().draw();

    // table_asessment_therapy_rd = $('#table_asessment_therapy').DataTable({
    //   "processing": true, //Feature control the processing indicator.
    //   "serverSide": true, //Feature control DataTables' server-side processing mode.
    //   //"scrollX": true,
    //   "paging": false,
    //   "sDom": '<"top"l>rt<"bottom"p><"clear">',
    //   "ordering": false,
    //   "ajax": {
    //       "url": ci_baseurl + "rawatdarurat/pasien_rd/table_asessment_therapy_rd",
    //       "type": "GET",
    //       "data" : function(d){
    //           d.pendaftaran_id = $("#pendaftaran_id").val();
    //       }
    //   },
    // });
    // table_asessment_therapy_rd.columns.adjust().draw();

    table_background_rd = $('#table_background').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_background_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      "columnDefs": [
        { "width": "75px", "targets": 0 },
        { "width": "100px", "targets": 2 }
      ]
    });
    table_background_rd.columns.adjust().draw();

    table_rs_rujukan = $('#table_rs_rujukan').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
       "searching": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_rs_rujukan",
            "type": "GET",



        },
        "columnDefs": [{
              "targets": [ -1 ],
              "orderable": false,
          },],
      });
      table_rs_rujukan.columns.adjust().draw();

    table_tindakan_rd = $('#table_tindakan_pasienrd_periksa').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rd.columns.adjust().draw();

    table_resep_rd = $('#table_resep_pasienrd').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_resep_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_resep_rd.columns.adjust().draw();
    // var year = new Date().getFullYear();
    // $('.datepicker_diagnosa').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });

    // $('.datepicker_tindakan').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    // $('.datepicker_resep').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });

    $('#nama_obat').keyup(function() {
        var val_obat = $('#nama_obat').val();
        $.ajax({
            url: ci_baseurl + "rawatdarurat/pasien_rd/autocomplete_obat",
            type: 'get',
            async: false,
            dataType: 'json',
            data: {val_obat: val_obat},
            success: function(data) { /* console.log(data); */
                var ret = data.success;
                if(ret == true) {
                    // console.log(data.data);
                    $(".autocomplete").autocomplete({
                        source: data.data,
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 0");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index","999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#obat_id').val(ui.item.id);
                            $('#nama_obat').val(ui.item.value);
                            $('#satuan_obat').val(ui.item.satuan);
                            $('#harga_jual').val(ui.item.harga_jual);
                            $('#harga_netto').val(ui.item.harga_netto);
                            $('#current_stok').val(ui.item.current_stok);
                            $('#lbl_nama_obat').addClass('active');
                            $('#lbl_harga_jual').addClass('active');
                            $('#lbl_satuan_obat').addClass('active');
                        }
                  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                      return $( "<li>" )
                        .append( "<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.deskripsi +"- Stok :"+ item.current_stok +"</font></a>" )
                        .appendTo( ul );
                    };
                }else{
                  $("#harga_netto").val('');
                  $("#current_stok").val('');
                  $("#obat_id").val('');
                }
            }
        });
    });

    $('button#saveDiagnosa').click(function(){
        console.log('saveDiagnosa!');
        diagnosa = {
            "pendaftaran_id": $("#pendaftaran_id").val(),
            "pasien_id" : $("#pasien_id").val(),
            "dokter_id" : $("#dokter_id_1").val(),
            "dokter_id_2" : $("#dokter_id_2").val(),
            "dokter_id_3" : $("#dokter_id_3").val(),
            "kode_diagnosa" : $("#kode_diagnosa").val(),
            "nama_diagnosa" : $("#nama_diagnosa").val(),
            "kode_diagnosa_icd10" : $("#kode_diagnosa_icd10").val(),
            "nama_diagnosa_icd10" : $("#nama_diagnosa_icd10").val(),
            "jenis_diagnosa" : $("#jenis_diagnosa option:selected").val(),
            "catatan_diagnosa" : $("#catatan_diagnosa").val(),
          };
        console.log(JSON.stringify(diagnosa));
        if(diagnosa.dokter_id == '' || diagnosa.dokter_id == null || diagnosa.dokter_id == '0' || diagnosa.dokter_id == '0#'){
            swal('Data belum lengkap, tidak dapat menambahkan');
            return false;
        }else{
            swal({
                text: "Apakah data yang dimasukan sudah benar ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'tidak',
                confirmButtonText: 'Ya'
              }).then(function(){
                    $.ajax({
                        url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_diagnosa_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateDiagnosaPasien').serialize(),
                        // data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +diagnosa.pendaftaran_id+ "&pasien_id=" +diagnosa.pasien_id+ "&dokter_id=" +diagnosa.dokter_id+ "&dokter_id_2=" +diagnosa.dokter_id_2+ "&dokter_id_3=" +diagnosa.dokter_id_3,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                                $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
                                table_diagnosa_rd.columns.adjust().draw();
                                $('#modal_notif1').removeClass('alert-danger').addClass('alert-success');
                                $('#card_message1').html(data.messages);
                                $('#modal_notif1').show();
                                $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                                  $("#modal_notif1").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                                // swal(
                                //        'Berhasil!',
                                //        'Data Berhasil Disimpan.',
                                //        'success'
                                //     )
                            } else {
                                // console.log('fail');
                                $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message1').html(data.messages);
                                $('#modal_notif1').show();
                                $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                                  $("#modal_notif1").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                                // swal(
                                //     'Gagal!',
                                //     'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                //     'error'
                                //   )
                            }
                        }
                    });

              });

        }
    });

    $('button#saveAssessment').click(function(){
        console.log('saveAssessment!');
        assessment = {
            // "assestment_pasien_id" : $("").val(),
            "instalasi_id" : 2,
            "pendaftaran_id" : $("#pendaftaran_id").val(),
            "pasien_id" : $("#pasien_id").val(),
            "tensi" : $("#tensi").val(),
            "nadi_1" : $("#nadi_1").val(),
            "suhu" : $("#suhu").val(),
            "nadi_2" : $("#nadi_2").val(),
            "penggunaan" : $("#penggunaan").val(),
            "saturasi" : $("#saturasi").val(),
            "nyeri" : $("#nyeri").val(),
            "numeric_wong_baker" : $("#numeric_wong_baker").val(),
            "resiko_jatuh" : $("#resiko_jatuh").val(),
            "program_therapy" : [1,2,3,4]
            // "nama_diagnosa_icd10" : $("#nama_diagnosa_icd10").val(),
            // "jenis_diagnosa" : $("#jenis_diagnosa option:selected").val(),
          };
        console.log(JSON.stringify(assessment));
        if(assessment.tensi == '' || assessment.tensi == null || assessment.nyeri == '' || assessment.nyeri == null ){
            swal('Data belum lengkap, tidak dapat menambahkan');
            return false;
        }else{
            swal({
                text: "Apakah data yang dimasukan sudah benar ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'tidak',
                confirmButtonText: 'Ya'
              }).then(function(){
                    $.ajax({
                        url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_diagnosa_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateDiagnosaPasien').serialize(),
                        // data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +diagnosa.pendaftaran_id+ "&pasien_id=" +diagnosa.pasien_id+ "&dokter_id=" +diagnosa.dokter_id+ "&dokter_id_2=" +diagnosa.dokter_id_2+ "&dokter_id_3=" +diagnosa.dokter_id_3,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                                $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
                                table_diagnosa_rd.columns.adjust().draw();
                                $('#modal_notif1').removeClass('alert-danger').addClass('alert-success');
                                $('#card_message1').html(data.messages);
                                $('#modal_notif1').show();
                                $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                                  $("#modal_notif1").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                                // swal(
                                //        'Berhasil!',
                                //        'Data Berhasil Disimpan.',
                                //        'success'
                                //     )
                            } else {
                                // console.log('fail');
                                $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message1').html(data.messages);
                                $('#modal_notif1').show();
                                $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                                  $("#modal_notif1").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                                // swal(
                                //     'Gagal!',
                                //     'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                //     'error'
                                //   )
                            }
                        }
                    });

              });

        }
    });

    var tbody_row = '';
    var tbody_row_no = 0;
    var terapi_array = [];
    $('button#addTherapy').click(function(){
        // read existing table value
        program_therapy_id = $("#nama_program option:selected").val();
        if(program_therapy_id == '#' || program_therapy_id == null){
          swal('Pilih Therapy terlebih dahulu');
        }else{
          console.log('jos');
          ++tbody_row_no
          program_therapy = $("#nama_program option:selected").html();
          program_therapy_id = $("#nama_program option:selected").val();
          terapi_array.push(program_therapy_id);
          // program_therapy_id = $("#nama_program option:selected").val();
          if($('#table_asessment_therapy').find("td").html() == "No data to display"){
            tbody_row = ""
          }else{
            tbody_row = $('#table_asessment_therapy tbody').html()
          }
          tbody_row += "<tr id='terapi_id_"+tbody_row_no+"'><td>"+program_therapy+"</td>";
          tbody_row += '<td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus diagnosa" onclick="hapusTherapy('+tbody_row_no+')"><i class="fa fa-times"></i></button></td></tr>';

          // write to existing
          $("#table_asessment_therapy tbody").html(tbody_row);
        }
    });

    $('button#addTindakanObat').click(function(){
        tbody = '<tr><td><input class="tbody-input-left" type="text" value=""></td><td><input class="tbody-input" type="number" value=""></td><td><input class="tbody-input" type="text" value=""></td><td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus obat" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td></tr>';
        $('#table_tindakan_obat tbody').append(tbody);
    });

    $('button#addTindakanBHP').click(function(){
        tbody = '<tr><td><input class="tbody-input-left" type="text" value=""></td><td><input class="tbody-input" type="number" value=""></td><td><input class="tbody-input"type="text" value=""></td><td><button type="button" class="btn btn-danger btn-circle" title="Klik untuk menghapus BHP" onclick="$(this).parent().parent().remove()"><i class="fa fa-times"></i></button></td></tr>';
        $('#table_tindakan_bhp tbody').append(tbody);
    });



//     $('button#saveAssessment').click(function(){
//       console.log('saveAssessment!');
//       assestment = {
// //         `assestment_pasien_id` int(11) NOT NULL AUTO_INCREMENT,
// // `instalasi_id` int(11) DEFAULT NULL,
// // `pendaftaran_id` int(11) DEFAULT NULL,
// // `pasien_id` int(11) NOT NULL,
// // `tensi` decimal(5,2) DEFAULT NULL,
// // `nadi_1` decimal(5,2) DEFAULT NULL,
// // `suhu` decimal(5,2) DEFAULT NULL,
// // `nadi_2` decimal(5,2) DEFAULT NULL,
// // `penggunaan` decimal(5,2) DEFAULT NULL,
// // `saturasi` decimal(5,2) DEFAULT NULL,
// // `nyeri` varchar(250) DEFAULT NULL,
// // `numeric_wong_baker` varchar(250) DEFAULT NULL,
// // `resiko_jatuh` varchar(250) DEFAULT NULL,
//
//           "pendaftaran_id": $("#pendaftaran_id").val(),
//           "pasien_id" : $("#pasien_id").val(),
//           "tensi" : $("#tensi").val(),
//           "nadi_1" : $("#nadi_1").val(),
//           "nadi_2" : $("#nadi_2").val(),
//           "suhu" : $("#suhu").val(),
//           "penggunaan" : $("#penggunaan").val(),
//           "saturasi" : $("#saturasi").val(),
//           "nyeri" : $("#nyeri").val(),
//           "numeric_wong_baker" : $("#numeric_wong_baker").val(),
//           "resiko_jatuh" : $("#resiko_jatuh").val()
//           // "jenis_diagnosa" : $("#jenis_diagnosa option:selected").val(),
//         };
//       console.log(JSON.stringify(diagnosa));
//       if(assestment.dokter_id == '' || assestment.dokter_id == null || assestment.dokter_id == '0' || assestment.dokter_id == '0#'){
//           swal('Data belum lengkap, tidak dapat menambahkan');
//           return false;
//       }else{
//           swal({
//               text: "Apakah data yang dimasukan sudah benar ?",
//               type: 'warning',
//               showCancelButton: true,
//               confirmButtonColor: '#3085d6',
//               cancelButtonColor: '#d33',
//               cancelButtonText: 'tidak',
//               confirmButtonText: 'Ya'
//             }).then(function(){
//                   $.ajax({
//                       url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_assestment_pasien",
//                       type: 'POST',
//                       dataType: 'JSON',
//                       data: $('form#fmCreateDiagnosaPasien').serialize(),
//                       // data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +diagnosa.pendaftaran_id+ "&pasien_id=" +diagnosa.pasien_id+ "&dokter_id=" +diagnosa.dokter_id+ "&dokter_id_2=" +diagnosa.dokter_id_2+ "&dokter_id_3=" +diagnosa.dokter_id_3,
//                       success: function(data) {
//                           var ret = data.success;
//                           $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
//                           $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
//                           $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
//                           $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
//                           $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
//                           if(ret === true) {
//                               $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
//                               $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
//                               table_diagnosa_rd.columns.adjust().draw();
//                               $('#modal_notif1').removeClass('alert-danger').addClass('alert-success');
//                               $('#card_message1').html(data.messages);
//                               $('#modal_notif1').show();
//                               $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
//                                 $("#modal_notif1").hide();
//                               });
//                               $("html, body").animate({ scrollTop: 100 }, "fast");
//                               // swal(
//                               //        'Berhasil!',
//                               //        'Data Berhasil Disimpan.',
//                               //        'success'
//                               //     )
//                           } else {
//                               // console.log('fail');
//                               $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
//                               $('#card_message1').html(data.messages);
//                               $('#modal_notif1').show();
//                               $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
//                                 $("#modal_notif1").hide();
//                               });
//                               $("html, body").animate({ scrollTop: 100 }, "fast");
//                               // swal(
//                               //     'Gagal!',
//                               //     'Gagal Disimpan. Data Masih Ada Yang Kosong !',
//                               //     'error'
//                               //   )
//                           }
//                       }
//                   });
//
//             });
//
//       }
//     });

    $('button#saveTindakan').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_id").val();
        kelas_pelayanan =$("#kelaspelayanan_id").val();
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_tindakan_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter+ "&kelas_pelayanan=" +kelas_pelayanan,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $(".select2").val(null).trigger("change");
                            $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                            $('#fmCreateTindakanPasien').find("input[type=number]").val("");
                            $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',0);
                        table_tindakan_rd.columns.adjust().draw();
                        swal(
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success'
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });

          });
    });

    $('button#saveBackground').click(function(){
        console.log('saveBackground!');
        background = {
            "pendaftaran_id": $("#pendaftaran_id").val(),
            "pasien_id" : $("#pasien_id").val(),
            "paket_operasi" : $("#paket_operasi option:selected").val()
          };
        console.log(JSON.stringify(background));
        if(background.paket_operasi == '' || background.paket_operasi == null || background.paket_operasi == '0' || background.paket_operasi == '0#'){
            swal('Data belum lengkap, tidak dapat menambahkan');
            return false;
        }else{
            // swal({
            //     text: "Apakah data yang dimasukan sudah benar ?",
            //     type: 'warning',
            //     showCancelButton: true,
            //     confirmButtonColor: '#3085d6',
            //     cancelButtonColor: '#d33',
            //     cancelButtonText: 'tidak',
            //     confirmButtonText: 'Ya'
            //   }).then(function(){
                    $.ajax({
                        url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_background_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateBackgroundPasien').serialize(),
                        // data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +diagnosa.pendaftaran_id+ "&pasien_id=" +diagnosa.pasien_id+ "&dokter_id=" +diagnosa.dokter_id+ "&dokter_id_2=" +diagnosa.dokter_id_2+ "&dokter_id_3=" +diagnosa.dokter_id_3,
                        success: function(data) {
                            console.log('ajax success');
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('#fmCreateBackgroundPasien').find("input[type=text]").val("");
                                $('#fmCreateBackgroundPasien').find("select").prop('selectedIndex',0);
                                table_background_rd.columns.adjust().draw();
                                $('#modal_notif1').removeClass('alert-danger').addClass('alert-success');
                                $('#card_message1').html(data.messages);
                                $('#modal_notif1').show();
                                $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                                  $("#modal_notif1").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                                // swal(
                                //        'Berhasil!',
                                //        'Data Berhasil Disimpan.',
                                //        'success'
                                //     )
                            } else {
                                // console.log('fail');
                                $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                                $('#card_message1').html(data.messages);
                                $('#modal_notif1').show();
                                $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                                  $("#modal_notif1").hide();
                                });
                                $("html, body").animate({ scrollTop: 100 }, "fast");
                                // swal(
                                //     'Gagal!',
                                //     'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                //     'error'
                                //   )
                            }
                        }
                    });
              // });
        }
    });

    $('button#saveResep').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_id").val();
        let dataRacikan = "";
        if ($('#checkbox_racikan').is(':checked')){
            dataRacikan = JSON.stringify(resepRacikanArr)
        }
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
            }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_resep_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateResepPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter_id=" +dokter + "&dataRacikan=" + dataRacikan,
                    success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            resepRacikanArr = [];
                            updateDataResepRacikan()
                            $('#checkbox_racikan').prop('checked', false);
                            $('#fmCreateResepPasien').find("input[type=text]").val("");
                            $('#fmCreateResepPasien').find("input[type=number]").val("");
                            $('#fmCreateResepPasien').find("select").prop('selectedIndex',0);

                            $('button#printreseptur').attr('onclick','printReseptur('+data.reseptur_id+')');        
                        table_resep_rd.columns.adjust().draw();
                        swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                            )
                        } else {
                            $('#modal_notif3').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message3').html(data.messages);
                            $('#modal_notif3').show();
                            $("#modal_notif3").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif3").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                        }
                    }
                });

        });
    });
});


function changeDataResepRacikan() {
    let
        obat = $("#obat_racikan").val(),
        jumlah = $("#jumlah_racikan").val(),
        sediaan = $("#sediaan_racikan").val();

    if (obat == "" || jumlah == "" || sediaan == "" || jumlah <= 0) {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        resepRacikanArr.push({
            obat: {
                id: obat,
                name: $("#obat_racikan option:selected").text()
            },
            jumlah: jumlah,
            sediaan: {
                id: sediaan,
                name: $("#sediaan_racikan option:selected").text()
            }
        });
        updateDataResepRacikan();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}

function updateDataResepRacikan() {
    let i,
    tableRacikan = document.getElementById("table_racikan_pasienrd"),
    tr, td;
    tableRacikan.innerHTML = "";
    if (resepRacikanArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "5");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tableRacikan.appendChild(tr);
        return;
    }
    for (i = 0; i < resepRacikanArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = resepRacikanArr[i].obat.name;
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = resepRacikanArr[i].jumlah;
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = resepRacikanArr[i].sediaan.name;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataResepRacikan("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tableRacikan.appendChild(tr);
    }
}

function removeDataResepRacikan(position) {
    resepRacikanArr.splice(position, 1)
    updateDataResepRacikan()
}

function getTindakan(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    });
}

function getTarifTindakan(){
    var daftartindakan_id = $("#tindakan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {daftartindakan_id:daftartindakan_id},
        success: function(data) {
            $("#harga_tindakan").val(data.list.harga_tindakan);
            $("#harga_cyto").val(data.list.cyto_tindakan);
        }
    });
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();

    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $("#subtotal").val(subtotal);
    $("#totalharga").val(total);
}

function hapusDiagnosa(diagnosapasien_id){
    var jsonVariable = {};
    jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if(diagnosapasien_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
            }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_diagnosa",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_diagnosa_rd.columns.adjust().draw();

                                swal(
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.',
                                    'success'
                                    )
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            }
                        }
                });
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusTherapy(id){
    console.log('hapusterapi');
    $('#terapi_id_'+id).remove();
    // var jsonVariable = {};
    // jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    // jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    // if(diagnosapasien_id){
    //     swal({
    //         text: "Apakah anda yakin ingin menghapus data ini ?",
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         cancelButtonText: 'tidak',
    //         confirmButtonText: 'Ya'
    //         }).then(function(){
    //             $.ajax({
    //                 url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_diagnosa",
    //                 type: 'post',
    //                 dataType: 'json',
    //                 data: jsonVariable,
    //                     success: function(data) {
    //                         var ret = data.success;
    //                         $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
    //                         $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
    //                         $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
    //                         $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
    //                         $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
    //                         if(ret === true) {
    //                             $('.modal_notif').removeClass('red').addClass('green');
    //                             $('#modal_card_message').html(data.messages);
    //                             $('.notmodal_notifif').show();
    //                             $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                                 $(".modal_notif").hide();
    //                             });
    //                         table_diagnosa_rd.columns.adjust().draw();
    //
    //                             swal(
    //                                 'Berhasil!',
    //                                 'Data Berhasil Dihapus.',
    //                                 'success'
    //                                 )
    //                         } else {
    //                             $('.modal_notif').removeClass('green').addClass('red');
    //                             $('#modal_card_message').html(data.messages);
    //                             $('.modal_notif').show();
    //                             $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                                 $(".modal_notif").hide();
    //                             });
    //                         }
    //                     }
    //             });
    //     });
    // }
    // else
    // {
    //     $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
    //     $('#notification_messages').html('Missing Access code ID');
    //     $('#notification_type').show();
    // }
}

function hapusAlergi(alergi_pasien_id){
    var jsonVariable = {};
    jsonVariable["alergi_pasien_id"] = alergi_pasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if(alergi_pasien_id){
        // swal({
        //     text: "Apakah anda yakin ingin menghapus data ini ?",
        //     type: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     cancelButtonText: 'tidak',
        //     confirmButtonText: 'Ya'
        //     }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_alergi",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_background_rd.columns.adjust().draw();

                                swal(
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.',
                                    'success'
                                    )
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            }
                        }
                });
        // });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusTindakan(tindakanpasien_id){
    var jsonVariable = {};
    jsonVariable["tindakanpasien_id"] = tindakanpasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    if(tindakanpasien_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_tindakan",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_tindakan_rd.columns.adjust().draw();
                                swal(
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.',
                                    'success'
                                    )
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            }
                        }
                });

          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function hapusResep(reseptur_id){
    var jsonVariable = {};
    jsonVariable["reseptur_id"] = reseptur_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delres').val();
    if(reseptur_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_resep",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_resep_rd.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success'
                            )
                    } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                                swal(
                                    'Gagal!',
                                    'Resep Gagal Terhapus !',
                                    'error'
                                    )
                            }
                        }
                });

          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function dialogObat(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_obat_rd){
        table_obat_rd.destroy();
    }
    table_obat_rd = $('#table_list_obat').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_obat",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_obat_rd.columns.adjust().draw();
}

function pilihObat(kode_barang){
    if (kode_barang != ""){
		$.ajax({
			url: ci_baseurl + "rawatdarurat/pasien_rd/ajax_get_obat_by_id",
			type: 'get',
			dataType: 'json',
            data: { kode_barang: kode_barang },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
					$('#obat_id').val(data.data['kode_barang']);
					$('#nama_obat').val(data.data['nama_barang']);
					$('#satuan_obat').val(data.data['id_sediaan']);
					$('#harga_jual').val(data.data['harga_satuan']);
					$('#modal_reseptur').modal('hide');
				} else {
					$("#harga_netto").val('');
					$("#current_stok").val('');
					$("#obat_id").val('');
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}

function dialogTindakanCariDokter(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_list_dokter_tindakan){
        table_list_dokter_tindakan.destroy();
    }
    table_list_dokter_tindakan = $('#table_list_dokter_tindakan').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_dokter_tindakan",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_dokter_tindakan.columns.adjust().draw();
}

function dialogTindakanCariUser(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_list_user_tindakan){
        table_list_user_tindakan.destroy();
    }
    table_list_user_tindakan = $('#table_list_user_tindakan').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_user",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_user_tindakan.columns.adjust().draw();
}

function dialogListDokter1(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_list_dokter_rd){
        table_list_dokter_rd.destroy();
    }
    table_list_dokter_rd = $('#table_list_dokter_rd').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_dokter1",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_dokter_rd.columns.adjust().draw();
}

function dialogListDokter2(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_list_dokter_rd){
        table_list_dokter_rd.destroy();
    }
    table_list_dokter_rd = $('#table_list_dokter_rd').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_dokter2",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_dokter_rd.columns.adjust().draw();
}

function dialogListDokter3(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_list_dokter_rd){
        table_list_dokter_rd.destroy();
    }
    table_list_dokter_rd = $('#table_list_dokter_rd').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_dokter3",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_dokter_rd.columns.adjust().draw();
}

function dialogPilihKodeDiagnosa(){
  console.log('dialogPilihKodeDiagnosa');
	// $('#modal_list_obat').openModal('toggle');
	if(table_list_diagnosa_rd){
        table_list_diagnosa_rd.destroy();
    }
    table_list_diagnosa_rd = $('#table_list_diagnosa').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa",
            "type": "GET"
        },
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_diagnosa_rd.columns.adjust().draw();
}

function dialogPilihKodeDiagnosaICD10(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_list_diagnosa_icd10_rd){
        table_list_diagnosa_icd10_rd.destroy();
    }
    table_list_diagnosa_icd10_rd = $('#table_list_diagnosa_icd10_rd').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa_icd10",
            "type": "GET"
        },
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_list_diagnosa_icd10_rd.columns.adjust().draw();
}


function pilihListDokter(id_M_DOKTER,id){
    if (id_M_DOKTER != ""){
		$.ajax({
			url: ci_baseurl + "rawatdarurat/pasien_rd/ajax_get_dokter_by_id",
			type: 'get',
			dataType: 'json',
            data: { id_M_DOKTER:  id_M_DOKTER },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
					$('#nama_dokter'+id).val(data.data['NAME_DOKTER']);
                    $('#id_dokter'+id).val(data.data['id_M_DOKTER']);

                    console.log($('#nama_dokter'+id).val(data.data['NAME_DOKTER']));
                    console.log($('#id_dokter'+id).val(data.data['id_M_DOKTER']));
					// $('#nama_obat').val(data.data['nama_barang']);
					// $('#satuan_obat').val(data.data['id_sediaan']);
					// $('#harga_jual').val(data.data['harga_satuan']);
					$('#modal_dokter').modal('hide');
				} else {
					// $("#harga_netto").val('');
					// $("#current_stok").val('');
                   console.log('ajax failed : '+id_M_DOKTER+' kolom ke : '+id);
					$('#nama_dokter'+id).val('');
                   $('#id_dokter'+id).val('');
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Dokter ID invalid");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}

function pilihListDiagnosa(diagnosa2_id){
    if (diagnosa2_id != ""){
		    $.ajax({
			url: ci_baseurl + "rawatdarurat/pasien_rd/ajax_get_diagnosa_by_id",
			type: 'get',
			dataType: 'json',
            data: { diagnosa2_id: diagnosa2_id },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
          $('#diagnosa_id').val(data.data['diagnosa2_id']);
					$('#kode_diagnosa').val(data.data['diagnosa_kode']);
					$('#nama_diagnosa').val(data.data['diagnosa_nama']);
          $('#kode_diagnosa_icd10').val(data.data['kode_diagnosa']);
          $('#nama_diagnosa_icd10').val(data.data['nama_diagnosa']);
					$('#modal_list_diagnosa').modal('hide');
				} else {
          $('#diagnosa_id').val('');
					$("#kode_diagnosa").val('');
					$("#nama_diagnosa").val('');
          $("#kode_diagnosa_icd10").val('');
          $("#nama_diagnosa_icd10").val('');
				}
			}
		});
  	} else {
  		// $('#modal_list_obat').closeModal('toggle');
  		$('.modal_notif').removeClass('green').addClass('red');
          $('#modal_card_message').html("Diagnosa ID Invalid");
          $('.modal_notif').show();
          $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
              $(".modal_notif").hide();
          });
  	}
  }


function pilihListDiagnosaICD10(diagnosa_id){
      if (diagnosa_id != ""){
  		$.ajax({
  			url: ci_baseurl + "rawatdarurat/pasien_rd/ajax_get_diagnosa_icd10_by_id",
  			type: 'get',
  			dataType: 'json',
              data: { diagnosa_id: diagnosa_id },
  			success: function(data) {
  				var ret = data.success;
  				if (ret == true){
            $('#diagnosa_id').val(data.data['diagnosa2_id']);
            $('#kode_diagnosa').val(data.data['diagnosa_kode']);
  					$('#nama_diagnosa').val(data.data['diagnosa_nama']);
  					$('#kode_diagnosa_icd10').val(data.data['kode_diagnosa']);
  					$('#nama_diagnosa_icd10').val(data.data['nama_diagnosa']);
  					$('#modal_list_diagnosa_icd10').modal('hide');
  				} else {
            $("#kode_diagnosa").val('');
  					$("#nama_diagnosa").val('');
            $("#kode_diagnosa_icd10").val('');
            $("#nama_diagnosa_icd10").val('');
  				}
  			}
  		});
  	} else {
  		// $('#modal_list_obat').closeModal('toggle');
  		$('.modal_notif').removeClass('green').addClass('red');
          $('#modal_card_message').html("Diagnosa ID Invalid");
          $('.modal_notif').show();
          $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
              $(".modal_notif").hide();
          });
  	}
  }

function pilihRSRujukan(oid){

      if (oid != ""){

        $.ajax({
          url: ci_baseurl + "rekam_medis/pasienrj/ajax_get_rs_rujukan_by_id",
          type: 'get',
          dataType: 'json',
          data: { rs_rujukan_id:oid },
          success: function(data) {
            var ret = data.success;

            if (ret == true){

              $('#rs_rujukan_id').val(data.data['rs_rujukan_id']);
              $('#kode_rs').val(data.data['kode_rs']);
              $('#nama_rs').val(data.data['nama_rs']);
              $('#alamat').val(data.data['alamat']);
              $('#lbl_kode_rs').addClass('active');
              $('#lbl_nama_rs').addClass('active');
              $('#lbl_alamat').addClass('active');
              $("#modal_rs_rujukan").modal('hide');
            } else {

              // $("#harga_netto").val('');
              // $("#current_stok").val('');
              // $("#obat_id").val('');
            }
          }
        });
      } else {
        // $('#modal_diagnosaa').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html("RS Rujukan ID Kosong");
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
                $(".modal_notif").hide();
            });
      }
    }

function is_rujuk(){
    var carapulang = $("#status_pulang").val();
    if(carapulang == 'Rujuk'){
        $('#rujukan_rs').removeAttr('style');
    }else{
        $('#rujukan_rs').attr('style','display:none;');
    }
}


function printReseptur(pendaftaran_id){ 

     console.log("masuk print : "+ci_baseurl+"rawatdarurat/pasien_rd/print_resep/"+pendaftaran_id); 
     console.log(pendaftaran_id = $("#pendaftaran_id").val());
     console.log('tess')
 
    window.open(ci_baseurl+'rawatdarurat/pasien_rd/print_resep/'+pendaftaran_id,'printReseptur','top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
} 
