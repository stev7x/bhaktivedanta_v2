$(document).ready(function () {
    cariPasien();

    // setInterval(function () {
    //     cariPasien();
    // }, 5000);
    // cek pasien baru
    $("#cek_pasien").change(function () {
        if (this.checked) {
            $('#cari_no_rekam_medis').attr("disabled", true);
            $('#cari_no_rekam_medis').val("");
            $('#no_rm').val("1");
            $('#is_checked').val("1");
        } else {
            $('#cari_no_rekam_medis').removeAttr("disabled", true);
            $('#is_checked').val("0");
        }
    });

    // delay onkeyup
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    // cari autocomplete berdasarkan no rm/ nama
    $("#cari_no_rekam_medis").keyup(function () {
        delay(function () {
            $.ajax({
                url: ci_baseurl + "rawatdarurat/reservasi_igd/autocomplete_pasien",
                type: 'post',
                data: {
                    keyword: $("#cari_no_rekam_medis").val()
                },
                dataType: 'json',
                success: function (data) {
                    var ret = data.success;
                    if (ret == true) {
                        $(".autocomplete").autocomplete({
                            source: data.data,
                            minLength: 2,
                            select: function (e, ui) {
                                // $('#type_id').val(ui.item.type_id); // ga ketemu id apa ni
                                $('#no_rm').val(ui.item.no_rekam_medis);
                                $('#pasien_nama').val(ui.item.pasien_nama);
                            }
                        }).data("ui-autocomplete")._renderItem = function (ul, item) {
                            return $("<li>")
                                .append("<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.alamat + "</font></a>")
                                .appendTo(ul);
                        };
                    } else {
                        // $("#id_m_pasien").val('0'); // ini buat apa
                        // nyoba buat muncul opsi bila pasien belum ditemukan
                        // $(".autocomplete").autocomplete({
                        //     source: data.data,
                        //     minLength: 2,
                        //     select: function (e, ui) {
                        //         $('#type_id').val(ui.item.type_id);
                        //         $('#no_rm').val(ui.item.no_rekam_medis);
                        //         $('#pasien_nama').val(ui.item.pasien_nama);
                        //     }
                        // }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        //   return $("<li>")
                        //       .append("<a><b><font size=2>Pasien tidak ditemukan</font></b><br><font size=1>silahkan daftar sebagai pasien baru</font></a>")
                        //       .appendTo(ul);
                        // };
                    }
                }
            })
        }, 500);
    });
    // aksi save reservasi
    // $('a#PasienBaru').click(function () {
    //   $('#cari_no_rekam_medis').removeAttr("disabled", true);
    //   $('#is_checked').val("0");
    // });

    // untuk cari
    $(document).ready(function () {
        $('#change_option').on('change', function (e) {
            var valueSelected = this.value;
            $('.pilih').attr('id', valueSelected);
            $('.pilih').attr('name', valueSelected);
        });
    });

    $(document).ready(function () {
        $('#change_option_batal').on('change', function (e) {
            var valueSelected = this.value;
            $('.pilihbatal').attr('id', valueSelected);
            $('.pilihbatal').attr('name', valueSelected);
        });
    });

    $(document).ready(function () {
        $('#change_option_tolak').on('change', function (e) {
            var valueSelected = this.value;
            $('.pilihtolak').attr('id', valueSelected);
            $('.pilihtolak').attr('name', valueSelected);
        });
    });

    // aksi save reservasi
    $('button#btnSaveReservasiIGD').click(function () {
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rawatdarurat/reservasi_igd/do_create_reservasi_igd",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateReservasiIGD').serialize(),
                success: function (data) {
                    var ret = data.success;
                    console.log(data);
                    var display = data.display;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    if (ret === true) {
                        get_pasien_reservasi_igd();
                        $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $('#jumlah_pasien').text(data.no_urut);
                        $('#kode_booking').text(data.no_booking);
                        $("html, body").animate({
                            scrollTop: 100
                        }, "fast");
                        swal(
                            'Berhasil !',
                            'Reservasi pasien berhasil ditambahkan',
                            'success'
                        );
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $("html, body").animate({
                            scrollTop: 100
                        }, "fast");
                        console.log('failed'+data.messages);
                    }
                }
            });
        });
    });

    // aksi tolak reservasi
	// function dialogPerujuk(){
    $('button#btnTolakReservasiIGD').click(function (){
			$.ajax({
                url: ci_baseurl + "rawatdarurat/reservasi_igd/do_tolak_reservasi_igd/",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmTolakReservasiIGD').serialize(),
                success: function(data){
                    var ret = data.success;
                    // var display = data.display;
                    // $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    if (ret === true){
                        cariPasien();
                        // $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                        // $('#card_message').html(data.messages);
                        // $('#modal_notif').show();
                        // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            // $("#modal_notif").hide();
                        // });
                        // $('#jumlah_pasien').text(data.no_urut);
                        // $('#kode_booking').text(data.no_booking);
                        // $("html, body").animate({
                            // scrollTop: 100
                        // }, "fast");
                        $('#modal_tolak_reservasi_igd').modal('hide');
                        swal('Berhasil !','Reservasi pasien berhasil ditolak','success');
                    }else{
                        // $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        // $('#card_message').html(data.messages);
                        // $('#modal_notif').show();
                        // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            // $("#modal_notif").hide();
                        // });
                        // $("html, body").animate({
                            // scrollTop: 100
                        // }, "fast");
						            console.log('failed');
                    }
                }
            });
    });
});


// edit by kfsl
function reload() {
    location.reload();
}

var table_perujuk;
// datatable perujuk
function dialogPerujuk() {
    if (table_perujuk) {
        table_perujuk.destroy();
    }
    table_perujuk = $('#table_list_rujuk').DataTable({
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pendaftaran/ajax_list_perujuk",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1, 0], //last column
            "orderable": false //set not orderable
        }]
    });
    table_perujuk.columns.adjust().draw();
}

// pilih perujuk berdasarkan id
function pilihRujuk(id_perujuk) {
    if (id_perujuk) {
        $.ajax({
            url: ci_baseurl + "rawatdarurat/pendaftaran/ajax_get_perujuk_by_id",
            type: 'get',
            dataType: 'json',
            data: {
                id_perujuk: id_perujuk
            },
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    // swal(JSON.stringify(data.data));
                    // console.log(data.data);
                    $('#rm_namaperujuk').val(data.data.nama_perujuk);
                    $("[href='default.htm']")
                    $('[id="id_perujuk"]').val(data.data.id_perujuk);
                    $("#buttonEditTeleponPerujuk").show();
                    $("#rm_noteleponperujuk_edit").val(data.data.telp_hp);
                    $('#rm_noteleponperujuk').val(data.data.telp_hp);
                    $('#rm_alamatperujuk').val(data.data.alamat_kantor);
                    $("#modal_list_rujuk").modal('hide');
                }
            }
        });
    }
}

function simpanTeleponPerujuk(){
  // swal("jos");
  $.ajax({
      url: ci_baseurl + "rawatdarurat/reservasi_igd/update_telepon_perujuk",
      type: 'POST',
      dataType: 'JSON',
      data: $('form#fmEditTeleponPerujukReservasiIGD').serialize(),
      success: function (data) {
          var ret = data.success;
          $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
          if (ret === true) {
              swal(
                  'Berhasil !',
                  'Nomor telepon perujuk berhasil diperbarui',
                  'success'
              );
              var new_telepon = $('#rm_noteleponperujuk_edit').val();
              $('#rm_noteleponperujuk').val(new_telepon);
              // $('#modal_edit_no_telp_perujuk').modal('hide');
          } else {
              console.log('failed');
          }
      }
  });
}

// edit by kfsl
function cariPasien() {
    get_pasien_reservasi_igd();
    get_pasien_reservasi_igd_batal();
    get_pasien_reservasi_igd_tolak();
}

function get_pasien_reservasi_igd() {
    var jsonVariable = {};
    jsonVariable["tgl_res_awal"] = $('#tgl_res_awal').val();
    // jsonVariable["poli"] = $('#poli').val();
    // jsonVariable["dokter"] = $('#dokter_list').val();
    jsonVariable["nama_pasien"] = $('#nama_pasien').val();
    jsonVariable["no_rekam_medis"] = $('#no_rekam_medis').val();
    jsonVariable["kode_booking"] = $('#kode_booking').val();

    $.ajax({
        url: ci_baseurl + 'rawatdarurat/reservasi_igd/get_pasien_reservasi_igd',
        type: 'GET',
        dataType: 'json',
        data: jsonVariable,
        success: function (data) {
            $('.hadir-reservasi').html(data);
        }
    });
}


function batalReservasi(id) {
    var reservasi_id = id;
    if (reservasi_id) {
        swal({
            text: "Apakah anda yakin ingin membatalkan reservasi ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rawatdarurat/reservasi_igd/do_batal_reservasi/" + reservasi_id,
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateReservasi').serialize(),
                success: function (data) {
                    var ret = data.success;
                    var display = data.display;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    if (ret === true) {
                        cariPasien();
                        // $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                        // $('#card_message').html(data.messages);
                        // $('#modal_notif').show();
                        // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        //     $("#modal_notif").hide();
                        // });
                        // $("html, body").animate({
                        //     scrollTop: 100
                        // }, "fast");
                        swal(
                            'Berhasil !',
                            'Reservasi pasien berhasil di batalkan',
                            'success'
                        );
                    } else {
                        // $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        // $('#card_message').html(data.messages);
                        // $('#modal_notif').show();
                        // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                        //     $("#modal_notif").hide();
                        // });
                        // $("html, body").animate({
                        //     scrollTop: 100
                        // }, "fast");
                        console.log('failed');
                    }
                }
            });
        });
    } else {
        console.log(" id reservasi null");
    }
}

// pop up TOLAK
function dialogTolak(id){
	console.log(id);
	$('#modal_tolak_reservasi_igd').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
	$('#reservasi_igd_id').val(id);
}

// function dialogEditTelpPerujuk(){
// 	// $('#modal_edit_no_telp_perujuk').modal('show');
// }

function print(id) {
    // console.log(id)
    window.open(ci_baseurl + 'rawatdarurat/reservasi_igd/print_rm/' + id, 'Print_RM', 'top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

// onclick verify, data dari t_reservasi_IGD insert ke t_reservasi
function verify(id) {
  if (id) {
      swal({
          text: "Apakah anda yakin ingin mendaftarkan pasien?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Tidak'
      }).then(function () {
        // swal('AYO MASUKKKAN '+id);
        $.ajax({
            url: ci_baseurl + "rawatdarurat/reservasi_igd/do_reservasi_igd_verify/" + id,
            type: 'get',
            dataType: 'json',
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    var new_id = data.new_id;
                    var src = ci_baseurl + "rawatdarurat/reservasi/verifikasi_data/" + new_id;
                    window.location = src;
                }else{
                    console.log('data gagal diverigy');
                }
            }
        });
    })
  }else{
      console.log("verify id null");
  }
}

function get_pasien_reservasi_igd_batal() {
    var jsonVariable = {};
    jsonVariable["tgl_res_awal"] = $('#tgl_res_awal').val();
    // jsonVariable["poli"] = $('#poli').val();
    // jsonVariable["dokter"] = $('#dokter_list').val();
    jsonVariable["nama_pasien"] = $('#nama_pasien_batal').val();
    jsonVariable["no_rekam_medis"] = $('#no_rekam_medis_batal').val();
    jsonVariable["kode_booking"] = $('#kode_booking_batal').val();
    $.ajax({
        url: ci_baseurl + 'rawatdarurat/reservasi_igd/get_pasien_reservasi_igd_batal',
        type: 'GET',
        data: jsonVariable,
        dataType: 'json',
        success: function (data) {
            $('.batal-reservasi').html(data);
        }
    });
}

function get_pasien_reservasi_igd_tolak() {
    var jsonVariable = {};
    jsonVariable["tgl_res_awal"] = $('#tgl_res_awal').val();
    // jsonVariable["poli"] = $('#poli').val();
    // jsonVariable["dokter"] = $('#dokter_list').val();
    jsonVariable["nama_pasien"] = $('#nama_pasien_batal').val();
    jsonVariable["no_rekam_medis"] = $('#no_rekam_medis_batal').val();
    jsonVariable["kode_booking"] = $('#kode_booking_batal').val();
    $.ajax({
        url: ci_baseurl + 'rawatdarurat/reservasi_igd/get_pasien_reservasi_igd_tolak',
        type: 'GET',
        data: jsonVariable,
        dataType: 'json',
        success: function (data) {
            $('.tolak-reservasi').html(data);
        }
    });
}
