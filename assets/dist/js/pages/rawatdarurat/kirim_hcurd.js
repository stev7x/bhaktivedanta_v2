var table_diagnosa_rd_hcu;
var table_tindakan_rd_hcu;
var table_background_rd_hcu;
var table_obat_rd;
var table_assestment_rd_hcu;
var theraphyArr = [];
var paketOperasiArr = [];
$(document).ready(function(){
    getTindakan();
    table_diagnosa_rd_hcu = $('#table_diagnosa_pasienrd_hcurd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,
 
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      { 
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_diagnosa_rd_hcu.columns.adjust().draw();


    table_tindakan_rd_hcu = $('#table_tindakan_pasienrd_hcurd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_tindakan_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rd_hcu.columns.adjust().draw();


    table_background_rd_hcu = $('#table_background_pasienrd_hcurd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_background_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_background_rd_hcu.columns.adjust().draw();

 table_assestment_rd_ok = $('#table_assestment_pasienrd_okrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_assestment_pasien_list",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_assestment_rd_ok.columns.adjust().draw();


});





 function getKelasPoliruanganHcu(){
   // console.log('te')
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
}

function getKamarHcu(){
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
}

function changeDataTheraphyHcu() {
  //console.log('rtrs')
    let 
        //id_rencana_tindakan = $("#pilihtindakan").val(),
        id_nama_paket = $("#nama_program").val();
         nama_paket= $("#nama_therapy").val();
       
    
    if (id_nama_paket == "" || nama_paket == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        theraphyArr.push({
            //id_rencana_tindakan:id_rencana_tindakan,
            id_nama_paket :id_nama_paket,
            nama_paket :nama_paket,
            //kiri table kana values table
        });
        updateDataTheraphyHcu();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}


function updateDataTheraphyHcu() {
    let i, 
    tablePaket = document.getElementById("table_theraphy_hcu"),
    tr, td;
    tablePaket.innerHTML = "";
    if (theraphyArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tablePaket.appendChild(tr);
        return;
    }
    for (i = 0; i < theraphyArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = theraphyArr[i].nama_paket;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataTheraphyHcu("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tablePaket.appendChild(tr);
    }
}

function removeDataTheraphyHcu(position) {
    theraphyArr.splice(position, 1)
    updateDataTheraphyHcu()
}


function changeDataRencanaTindakanHcu() {
    //console.log('jalan')
    let 
        //id_rencana_tindakan = $("#pilihtindakan").val(),
        id_nama_paket = $("#nama_paket").val();
         nama_paket= $("#paket_op").val();
       
    
    if (id_nama_paket == "" || nama_paket == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        paketOperasiArr.push({
            //id_rencana_tindakan:id_rencana_tindakan,
            id_nama_paket :id_nama_paket,
            nama_paket :nama_paket,
            //kiri table kana values table
        });
        updateDataRencanaTindakanHcu();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);

}


function updateDataRencanaTindakanHcu() {
    let i, 
    tablePaket = document.getElementById("table_paket_rencanatindak_hcu"),
    tr, td;
    tablePaket.innerHTML = "";
    if (paketOperasiArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tablePaket.appendChild(tr);
        return;
    }
    for (i = 0; i < paketOperasiArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = paketOperasiArr[i].nama_paket;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataRencanaTindakanHcu("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tablePaket.appendChild(tr);
    }
}

function removeDataRencanaTindakanHcu(position) {
    paketOperasiArr.splice(position, 1)
    updateDataRencanaTindakanHcu()
}


function saveAssestmentHcu(){
  //console.log('simpan')
        pendaftaran_id = $("#pendaftaran_id").val();
         tensi = $("#tensihcu").val();
         nadi = $("#nadihcu").val();
         suhu = $("#suhuhcu").val();
         nadi_1 = $("#nadi_1hcu").val();
         penggunaan = $("#penggunaanhcu").val();
         saturasi = $("#saturasihcu").val();
         nyeri = $("#nama_nyerihcu").val();
         numeric = $("#numerichcu").val();
         resiko = $("#resikohcu").val();
         var theraphy = JSON.stringify(theraphyArr);
         let formData = new FormData();
         formData.append("pendaftaran_id", pendaftaran_id);
         formData.append("tensihcu", tensi);
         formData.append("nadihcu", nadi);
         formData.append("suhuhcu", suhu);
         formData.append("nadi_1hcu", nadi_1);
         formData.append("penggunaanhcu", penggunaan);
         formData.append("saturasihcu", saturasi);
         formData.append("nama_nyerihcu", nyeri);
         formData.append("numerichcu", numeric);
         formData.append("resikohcu", resiko);
         formData.append("theraphyArr", theraphy);
         console.log(nadi)
         console.log(suhu)
         console.log(nadi_1)
         console.log(penggunaan)
         console.log(saturasi)
         console.log(numeric)
         console.log(resiko)
         console.log(theraphy)
         console.log(nyeri)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                console.log("jalan")
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_assestment_hcu",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data) {
                        data = JSON.parse(data);
                        console.log(data);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                                 $("#tensihcu").val("");
                                 $("#nadihcu").val("");
                                $("#suhuhcu").val("");
                                $("#nadi_1hcu").val("");
                                $("#penggunaanhcu").val("");
                                $("#saturasihcu").val("");
                                $("#resikohcu").val("");
                          theraphyArr = [];
                          document.getElementById("table_theraphy_hcu").innerHTML = "";
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });   
}


function saveHcurd(){
 // console.log('simpan')
          pendaftaran_id = $("#pendaftaran_id").val();
         poli_ruangan = $("#poli_ruangan").val();
         kelas_pelayanan = $("#kelas_pelayanan").val();
         kamar = $("#kamarruangan").val();
         catatan = $("#catatan").val();
         instalasi = $("#instalasi_id").val();
         jamaperiksa = $("#jam_periksa").val();

         //komen = $("#komen").val();
         var paketoperasi = JSON.stringify(paketOperasiArr);
         let formData = new FormData();
         formData.append("pendaftaran_id", pendaftaran_id);
         formData.append("poli_ruangan", poli_ruangan);
         formData.append("kelas_pelayanan", kelas_pelayanan);
         formData.append("kamarruangan", kamar);
         formData.append("catatan", catatan);
         formData.append("instalasi_id", instalasi);
         formData.append("jam_periksa", jamaperiksa);
         formData.append("paketOperasiArr", paketoperasi);
         console.log(poli_ruangan)
         console.log(kelas_pelayanan)
         console.log(kamar)
         console.log(catatan)
         console.log(instalasi)
         console.log(jamaperiksa)
         console.log(paketoperasi)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                console.log("jalan")
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_hcurd",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data) {
                        console.log('simpan')
                        data = JSON.parse(data);
                        console.log(data.ret);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                          paketOperasiArr = [];
                          document.getElementById("table_paket_rencanatindak_hcu").innerHTML = "";
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });
    
}