var paketOperasiArr = [];
var theraphyArr = [];
var table_diagnosa_rd;
var table_tindakan_rd;
var table_background_rd;
var table_resep_rd;
var table_obat_rd;
var table_rs_rujukan;
$(document).ready(function(){
    getTindakan();
    $("#obat_id").val('0#');
    table_diagnosa_rd = $('#table_diagnosa_pasienrd_rawatinap').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,
 
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_diagnosa_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      { 
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_diagnosa_rd.columns.adjust().draw();


    table_tindakan_rd = $('#table_tindakan_pasienrd_rawatinap').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_tindakan_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rd.columns.adjust().draw();


    table_background_rd = $('#table_background_pasienrd_rawatinap').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_background_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_background_rd.columns.adjust().draw();


    table_resep_rd = $('#table_resep_pasienrd_ranap').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_resep_pasien_rawatinap",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_resep_rd.columns.adjust().draw();

});

    // var year = new Date().getFullYear();
    // $('.datepicker_diagnosa').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    
    // $('.datepicker_tindakan').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    // $('.datepicker_resep').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });



function changeDataRencanaTindakanRanap() {
    let 
        //id_rencana_tindakan = $("#pilihtindakan").val(),
        id_nama_paket = $("#nama_paket").val();
         nama_paket= $("#paket_op").val();
       
    
    if (id_nama_paket == "" || nama_paket == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        paketOperasiArr.push({
            //id_rencana_tindakan:id_rencana_tindakan,
            id_nama_paket :id_nama_paket,
            nama_paket :nama_paket,
            //kiri table kana values table
        });
        updateDataRencanaTindakanRanap();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}


function updateDataRencanaTindakanRanap() {
    let i, 
    tablePaket = document.getElementById("table_paket_operasi"),
    tr, td;
    tablePaket.innerHTML = "";
    if (paketOperasiArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tablePaket.appendChild(tr);
        return;
    }
    for (i = 0; i < paketOperasiArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = paketOperasiArr[i].nama_paket;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataRencanaTindakanRanap("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tablePaket.appendChild(tr);
    }
}

function removeDataRencanaTindakanRanap(position) {
    paketOperasiArr.splice(position, 1)
    updateDataRencanaTindakanRanap()
}

function cekk(){
    console.log('tes')
}


function changeDataTheraphy() {
    let 
        //id_rencana_tindakan = $("#pilihtindakan").val(),
        id_nama_paket = $("#nama_program").val();
         nama_paket= $("#nama_therapy").val();
       
    
    if (id_nama_paket == "" || nama_paket == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        theraphyArr.push({
            //id_rencana_tindakan:id_rencana_tindakan,
            id_nama_paket :id_nama_paket,
            nama_paket :nama_paket,
            //kiri table kana values table
        });
        updateDataTheraphy();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}


function updateDataTheraphy() {
    let i, 
    tablePaket = document.getElementById("table_theraphy"),
    tr, td;
    tablePaket.innerHTML = "";
    if (theraphyArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tablePaket.appendChild(tr);
        return;
    }
    for (i = 0; i < theraphyArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = theraphyArr[i].nama_paket;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataTheraphy("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tablePaket.appendChild(tr);
    }
}

function removeDataTheraphy(position) {
    theraphyArr.splice(position, 1)
    updateDataTheraphy()
}


function getKelasPoliruangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
}

function getKamar(){
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
}

function getTindakan(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    });
}

function hapusDiagnosa(diagnosapasien_id){
    var jsonVariable = {};
    jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if(diagnosapasien_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
            }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_diagnosa",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_diagnosa_rd.columns.adjust().draw();

                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.',
                                    'success' 
                                    ) 
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            }
                        }
                });
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}
function hapusTindakan(tindakanpasien_id){
    var jsonVariable = {};
    jsonVariable["tindakanpasien_id"] = tindakanpasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    if(tindakanpasien_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_tindakan",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_tindakan_rd.columns.adjust().draw();
                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.',
                                    'success' 
                                    ) 
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            }
                        }
                });
        
          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function saveAssestment(){
 // console.log('simpan')
          pendaftaran_id = $("#pendaftaran_id").val();
         tensi = $("#tensi").val();
         nadi = $("#nadi").val();
         suhu = $("#suhu").val();
         nadi_1 = $("#nadi_1").val();
         penggunaan = $("#penggunaan").val();
         saturasi = $("#saturasi").val();
         nyeri = $("#nama_nyeri").val();
         numeric = $("#numeric").val();
         resiko = $("#resiko").val();
         var theraphy = JSON.stringify(theraphyArr);
         let formData = new FormData();
         formData.append("pendaftaran_id", pendaftaran_id);
         formData.append("tensi", tensi);
         formData.append("nadi", nadi);
         formData.append("suhu", suhu);
         formData.append("nadi_1", nadi_1);
         formData.append("penggunaan", penggunaan);
         formData.append("saturasi", saturasi);
         formData.append("nama_nyeri", nyeri);
         formData.append("numeric", numeric);
         formData.append("resiko", resiko);
         formData.append("theraphyArr", theraphy);
         console.log(nadi)
         console.log(suhu)
         console.log(nadi_1)
         console.log(penggunaan)
         console.log(saturasi)
         console.log(numeric)
         console.log(resiko)
         console.log(theraphy)
         console.log(nyeri)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                console.log("jalan")
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_assestment",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data) {
                        data = JSON.parse(data);
                        console.log(data);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                                 $("#tensi").val("");
                                 $("#nadi").val("");
                                $("#suhu").val("");
                                $("#nadi_1").val("");
                                $("#penggunaan").val("");
                                $("#saturasi").val("");
                                $("#resiko").val("");
                          theraphyArr = [];
                          document.getElementById("table_theraphy").innerHTML = "";
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });
    
}


function saveRawatInap(){
 // console.log('simpan')
          pendaftaran_id = $("#pendaftaran_id").val();
         poli_ruangan = $("#poli_ruangan").val();
         kelas_pelayanan = $("#kelas_pelayanan").val();
         kamar = $("#kamarruangan").val();
         dokter = $("#dokter_id").val();
         catatan = $("#catatan").val();
         instalasi = $("#instalasi_id").val();

         //komen = $("#komen").val();
         var paketoperasi = JSON.stringify(paketOperasiArr);
         let formData = new FormData();
         formData.append("pendaftaran_id", pendaftaran_id);
         formData.append("poli_ruangan", poli_ruangan);
         formData.append("kelas_pelayanan", kelas_pelayanan);
         formData.append("kamarruangan", kamar);
         formData.append("dokter_id",dokter);
         formData.append("catatan", catatan);
         formData.append("instalasi_id", instalasi);
         formData.append("paketOperasiArr", paketoperasi);
         console.log(poli_ruangan)
         console.log(kelas_pelayanan)
         console.log(kamar)
         console.log(dokter)
         console.log(catatan)
         console.log(instalasi)
         console.log(paketoperasi)
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                console.log("jalan")
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_create_rawatinap",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(data) {
                        console.log('simpan')
                        data = JSON.parse(data);
                        console.log(data.ret);
                        var ret = data.success;
                        // $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        // $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret) {
                          paketOperasiArr = [];
                          document.getElementById("table_paket_operasi").innerHTML = "";


                            $('button#print').removeAttr('disabled').removeClass('btn-default').addClass('btn-info'); 
                             $('button#savePembayaran').attr('disabled','disabled').removeClass('btn-success').addClass('btn-default');    
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )

                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                            }
                    }
                });
           
          });
    
}
