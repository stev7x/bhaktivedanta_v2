var table_pasienrd;
$(document).ready(function(){
   table_pasienrd = $('#table_list_pasienrd').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_pasienrd",
            
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.dokter_id = $("#dokter_pj").val();
            } 
   
        }, 
        //Set column definition initialisation properties.
    });

    table_pasienrd.columns.adjust().draw();
   
   $('button#saveStatusPulang').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
        }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/set_status_pulang",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmStatusPulang').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal2_notif').removeClass('red').addClass('green');
                            $('#modal2_card_message').html(data.messages);
                            $('.modal2_notif').show();
                            $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal2_notif").hide();
                            });
                        table_pasienrd.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.', 
                            'success'
                            ) 
                        } else {
                            $('.modal2_notif').removeClass('green').addClass('red');
                            $('#modal2_card_message').html(data.messages);
                            $('.modal2_notif').show();
                            $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal2_notif").hide();
                            });
                            swal(
                                'Gagal!',
                                'Gagal Disimpan. Status Pulang Belum Diset !',
                                'error'
                            )
                        }
                    }
                });
            
        });
    });

    $('button#saveStatusPulang').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
          swal({
              text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText:'Tidak',
              confirmButtonText: 'Ya!'
          }).then(function(){
              $.ajax({
                      url: ci_baseurl + "rawatdarurat/pasien_rd/set_status_pulang",
                      type: 'POST',
                      dataType: 'JSON',
                      data: $('form#fmCreateStatusPulang').serialize()+ "&pendaftaran_id=" +pendaftaran_id,
                      success: function(data) {
                          var ret = data.success;
                            console.log("masuk sukses");
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                          if(ret === true) {
                            console.log("masuk sukses lagi");
                              $("#modal_status_pulang").modal('hide').removeClass('error');
                              // $("#nama_provinsi").clear();
                              // $(".bs-example-modal-lg ").hide();
  
  
  
                              // $("#modal_notif").removeClass('hide').fadeTo(2000, 500).slideUp(500, function(){
                              //     $("#modal_notif").hide();
                              // });
                              // $('.modal_notif').removeClass('red').addClass('green');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $('#fmCreateProvinsiOperasi').find("input[type=text]").val("");
                              // $('#fmCreateProvinsiOperasi').find("select").prop('selectedIndex',0);
                              table_pasienrd.columns.adjust().draw();
                              swal(
                                    'Berhasil!',
                                    'Data Berhasil Disimpan.',
                                    'success'
                                  )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                                swal(
                                'Gagal!',
                                'Gagal Disimpan. Status Pulang Belum Diset !',
                                'error'
                            )
                          }
                      }
                  });
            
          })
      });


    $('button#saveToPenunjang').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/kirim_ke_penunjang",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmKirimKePenunjang').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('#modal_kirim_penunjang').modal('hide');
                            $('.modal2_notif').removeClass('red').addClass('green');
                            $('#modal2_card_message').html(data.messages);
                            $('.modal2_notif').show();
                            $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal2_notif").hide();
                            });
                            $('#fmKirimKePenunjang').find("input[type=text]").val("");
                            $('#fmKirimKePenunjang').find("select").prop('selectedIndex',"");
                        table_pasienrd.columns.adjust().draw();
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.', 
                              'success'
                            ) 
                        } else {
                            $('.modal2_notif').removeClass('green').addClass('red');
                            $('#modal2_card_message').html(data.messages);
                            $('.modal2_notif').show();
                            $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal2_notif").hide();
                            });
                              swal(
                                'Gagal!',
                                'Gagal Disimpan. Penunjang Belum Dipilih !',
                                'error'
                            )
                        }
                    }
                });
            
        });
    });

    
    // var year = new Date().getFullYear();
    // $('.datepicker').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
});

function reloadTablePasien(){
    table_pasienrd.columns.adjust().draw();
    console.log('tess')
}

function periksaPasienRd(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/periksa_pasienrd/"+pendaftaran_id;
    $("#modal_periksa_pasienrd iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function RujukrsLain(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/rujuk_rslain/"+pendaftaran_id;
    console.log(src);
    $("#modal_rujuk_rslain iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}

function kirimPasienPenunjang(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/kirim_pasienrdpenunjang/"+pendaftaran_id;
    console.log(src);
    $("#modal_kirim_penunjang iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function kirimPasienHcuRd(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/kirim_hcurd/"+pendaftaran_id;
    console.log(src);
    $("#modal_kirim_hcurd iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function kirimPasienPathologiRd(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/kirim_pathologi/"+pendaftaran_id;
    console.log(src);
    $("#modal_kirim_pathologird iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function kirimPasienIsolasiRd(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/kirim_isolasird/"+pendaftaran_id;
    console.log(src);
    $("#modal_kirim_isolasird iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}

function permintaanRawatInap(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/kirim_permintaanrawat/"+pendaftaran_id;
    console.log(src);
    $("#modal_kirim_permintaanrawat iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function kirimRawatInap(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/kirim_rawatinap/"+pendaftaran_id;
    console.log(src);
    $("#modal_kirim_rawatinap iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function kirimPasienVk(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/kirim_vkrd/"+pendaftaran_id;
    console.log(src);
    $("#modal_kirim_vkrd iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function kirimPasienOk(pendaftaran_id){   
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rawatdarurat/pasien_rd/kirim_okrd/"+pendaftaran_id;
    console.log(src);
    $("#modal_kirim_okrd iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}


function batalPeriksa(pendaftaran_id){
      var jsonVariable = {};
      jsonVariable["pendaftaran_id"] = pendaftaran_id;
      jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
      if(pendaftaran_id){
        swal({
            text: "Apakah anda yakin ingin membatalkan periksa ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_batal_periksa",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.notif').removeClass('red').addClass('green');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                            table_pasienrd.columns.adjust().draw();
                                swal( 
                                      'Berhasil!',
                                      'Data Berhasil Dibatalkan.', 
                                      'success'
                                    ) 
                            } else {
                                $('.notif').removeClass('green').addClass('red');
                                $('#card_message').html(data.messages);
                                $('.notif').show();
                                $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".notif").hide();
                                });
                                swal(
                                'Gagal!',
                                'Gagal Disimpan. Pasien Gagal Batal Periksa !',
                                'error'
                            )
                            }
                        }
                });
        
          });
      }
      else
      {
          $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
          $('#notification_messages').html('Missing Access code ID');
          $('#notification_type').show();
      }
}

function rujukKeRI(pendaftaran_id, pasien_id, no_pendaftaran){
    // $('#modal_kirim_rawatinap').openModal('toggle');
    $('#pendaftaran_id').val(pendaftaran_id);
    $('#pasien_id').val(pasien_id);
    $('#no_pendaftaran').val(no_pendaftaran);
}

function getKelasPoliruangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
}

function getKamar(){
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rawatdarurat/pasien_rd/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
}

function saveAdmisi(){
    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya' 
      }).then(function(){  
            $.ajax({
                url: ci_baseurl + "rawatdarurat/pasien_rd/do_kirimke_rawatinap",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmKirimKeAdmisi').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                      $('#modal_kirim_rawatinap').modal('hide');
                        // $('.modal_notif_kirim').removeClass('red').addClass('green');
                        // $('#modal_card_message_kirim').html(data.messages);
                        // $('.modal_notif_kirim').show();
                        // $(".modal_notif_kirim").fadeTo(2000, 500).slideUp(500, function(){
                        // $(".modal_notif_kirim").modal('hide');
                        // });
                        $('#fmKirimKeAdmisi').find("input[type=text]").val("");
                        $('#fmKirimKeAdmisi').find("select").prop('selectedIndex',0);
                    table_pasienrd.columns.adjust().draw();
                      swal( 
                            'Berhasil!',
                            'Data Berhasil Disimpan.', 
                            'success'
                          )
                    } else {
                        $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message1').html(data.messages);
                        $('#modal_notif1').show();
                        $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif1").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada yang Kosong !',
                        //         'error'
                        //     )
                    }
                }
            });
      
      });
}

function cariPasien(){
    if(table_pasienrd){
        table_pasienrd.destroy();
    }
    
    table_pasienrd = $('#table_list_pasienrd').DataTable({
      "sDom": '<"top"l>rt<"bottom"ip><"clear">',
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      // "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_pasienrd",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
            }
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,0,12 ], //last column
        "orderable": false //set not orderable
      }
      ]
    });
    table_pasienrd.columns.adjust().draw();
}
function kirimKePenunjang(pendaftaran_id){
    // $('#modal_kirim_penunjang').openModal('toggle');
    $('#pendaftaran_id_').val(pendaftaran_id);
}

function rujukPaketOperasi(pendaftaran_id, pasien_id){
    // $('#modal_paket_operasi').openModal('toggle');
    $('#pendaftaran_id_po').val(pendaftaran_id);
    $('#pasien_id_po').val(pasien_id);
}

function getRuangan(){
    var paket_operasi = $("#paket_operasi_po").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_ruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {paket_operasi:paket_operasi},
        success: function(data) {
            $("#poli_ruangan_po").html(data.list);
        }
    });
}

function getKamarPo(){
    var poliruangan_id = $("#poli_ruangan_po").val();
    var paket_operasi = $("#paket_operasi_po").val();
    
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, paketoperasi_id:paket_operasi},
        success: function(data) {
            $("#kamarruangan_po").html(data.list);
        }
    });
    //getDokterRuangan();
}

function getDokterRuangan(){
    var poliruangan_id = $("#poli_ruangan_po").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#dokter_po").html(data.list);
        }
    });
}

function savePO(){
    swal({
        text: "Apakah data yang dimasukan sudah benar ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya' 
      }).then(function(){  
            $.ajax({
                url: ci_baseurl + "rawatdarurat/pasien_rd/do_paket_operasi",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmPaketOperasi').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        $('#modal_paket_operasi').modal('hide');
                        
                        $('#fmPaketOperasi').find("input[type=text]").val("");
                        $('#fmPaketOperasi').find("select").prop('selectedIndex',0);
                    table_pasienrd.columns.adjust().draw();
                    swal( 
                          'Berhasil!',
                          'Data Berhasil Disimpan.', 
                          'success'
                        )
                    } else {
                        $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message2').html(data.messages);
                        $('#modal_notif2').show();
                        $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif2").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada yang Kosong !',
                        //         'error'
                        //     )
                    }
                }
            });
         
      });
}


function pulangkanPasienRd(pendaftaran_id){
    // $('#modal_periksa_pasienrj').openModal('toggle');
    
    var src = ci_baseurl + "rawatdarurat/pasien_rd/keterangan_keluar/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $('#pendaftaran_id').val(pendaftaran_id);
    $("#modal_status_pulang iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function cariPasien(){
    table_pasienrd = $('#table_list_pasienrd').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_pasienrd",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.pasien_alamat = $("#pasien_alamat").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_pasienrd.columns.adjust().draw();
}

$(document).ready(function(){
    $('#change_option').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});

function riwayatPenyakitPasien(pasien_id) {
    var src = ci_baseurl + "rekam_medis/list_pasien/riwayatPenyakitPasien/" + pasien_id;
    console.log(pasien_id);
    $("#modal_riwayat_penyakit_pasien iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen': ''
    });
}