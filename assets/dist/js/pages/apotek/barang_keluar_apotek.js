var table_barang_keluar_apotek;
$(document).ready(function () {
    table_barang_keluar_apotek = $("#table_barang_keluar_apotek").DataTable({
        "processing": true,
        "serverSide": true,
        "searching" : false,
        "ajax": {
            "url": ci_baseurl + "apotek/barang_keluar_apotek/ajax_list_barang_keluar",
            "type": "GET"
        },
        "columnDefs": [{
            "targets": [-1, 0],
            "orderable": false
        }]
    });
    table_barang_keluar_apotek.columns.adjust().draw();
});

function cariBarangMasuk() {
    table_barang_keluar_apotek = $('#table_barang_keluar_apotek').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "apotek/barang_keluar_apotek/ajax_list_barang_keluar",
            "type": "GET",
            "data": function (d) {
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.kode_barang = $("#kode_barang").val();
                d.nama_barang = $("#nama_barang").val();
                d.jenis_barang = $("#jenis_barang").val();

            }
        },
        //Set column definition initialisation properties.
    });
    table_barang_keluar_apotek.columns.adjust().draw();
}


$(document).ready(function () {
    $('#change_option').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id', valueSelected);
        $('.pilih').attr('name', valueSelected);
    });
});