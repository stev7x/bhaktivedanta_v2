var table_stok_barang_apotek;
$(document).ready(function() {
    table_stok_barang_apotek = $("#table_stok_barang_apotek").DataTable({
        "processing" : true,
        "serverSide" : true,
        "searching" : false,
        "ajax"       : {
            "url"   : ci_baseurl + "apotek/stok_barang_apotek/ajax_list_barang_stok",
            "type"  : "GET"
        },
        "columnDefs": [{
            "targets"   : [-1,0],
            "orderable" : false
        }]        
    });
    table_stok_barang_apotek.columns.adjust().draw();
});


function cariBarangMasuk() {
    table_stok_barang_apotek = $('#table_stok_barang_apotek').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "apotek/stok_barang_apotek/ajax_list_barang_stok",
            "type": "GET",
            "data": function (d) {
                d.kode_barang = $("#kode_barang").val();
                d.nama_barang = $("#nama_barang").val();
                d.jenis_barang = $("#jenis_barang").val();

            }
        },
        //Set column definition initialisation properties.
    });
    table_stok_barang_apotek.columns.adjust().draw();
}


$(document).ready(function () {
    $('#change_option').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id', valueSelected);
        $('.pilih').attr('name', valueSelected);
    });
});