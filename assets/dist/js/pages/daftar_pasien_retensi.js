var table_list_pasien;
$(document).ready(function(){
    table_list_pasien = $('#table_list_pasien_list').DataTable({ 
      "processing": true,
      "serverSide": true,
      "searching": false,

      "ajax": {
          "url": ci_baseurl + "daftar_pasien_retensi/ajax_list_list_pasien",
          "type": "GET",
      },
    });
    table_list_pasien.columns.adjust().draw();  
});

function exportAllPasien() {
    console.log('masukexport');
    window.location.href = ci_baseurl + "daftar_pasien_retensi/exportToExcel";
}