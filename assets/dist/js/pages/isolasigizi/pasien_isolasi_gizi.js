var table_pasienrd;
function cariPasien(){
    table_pasienrd = $('#table_list_pasieniso').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "isolasigizi/pasien_isolasi_gizi/ajax_list_pasieniso",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.dokter_id = $("#dokter_pj").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_pasienrd.columns.adjust().draw();
}
