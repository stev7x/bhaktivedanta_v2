var table;
$(document).ready(function(){
    table = $('#table_laporan_radiologi').DataTable({

        // "footerCallback": function ( row, data, start, end, display ) {
        //     var api = this.api(), data;
 
        //     // Remove the formatting to get integer data for summation
        //     var intVal = function ( i ) {
        //         return typeof i === 'string' ?
        //             i.replace(/[\$,]/g, '')*1 :
        //             typeof i === 'number' ?
        //                 i : 0;
        //     };
 
        //     // Total over all pages
        //     total = api
        //         .column( 14 )
        //         .data()
        //         .reduce( function (a, b) {
        //             return intVal(a) + intVal(b);
        //         }, 0 );

        //     console.log(total);
 
        //     // Total over this page  
        //     pageTotal = api
        //         .column( 14, { page: 'current'} )
        //         .data()
        //         .reduce( function (a, b) {
        //             return intVal(a) + intVal(b);
        //         }, 0 );
 
        //     // Update footer
        //     $( api.column( 14 ).footer() ).html(
        //         pageTotal+' ('+total+' total)'
        //         formatNumber(pageTotal)
        //     );
        // },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",   
        "bFilter": false,
        //"sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "laporan/laporanrad/ajax_list_radiologi",
            "type": "GET",
            "data" : function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
            }
        },
      //Set column definition initialisation properties.
     // "columnDefs": [
     //  {
     //    "targets": [ -1,0,12 ], //last column
     //    "orderable": false //set not orderable  
     //  }
     //  ]
    });
    table.columns.adjust().draw();
    
});

function printToExcelRadiologi(){
    var tgl_awal_rj = $("#tgl_awal").val();
    var tgl_akhir_rj = $("#tgl_akhir").val();

    if (tgl_awal_rj == ""){ tgl_awal_rj = "undefined"; }
    if (tgl_akhir_rj == ""){ tgl_akhir_rj = "undefined"; }

    window.location.href = ci_baseurl + "laporan/laporanrad/export_excel_radiologi/"+ tgl_awal_rj +"/"+ tgl_akhir_rj; 
}

function formatNumber(toFormat) {
    return toFormat.toString().replace(
      /\B(?=(\d{3})+(?!\d))/g, ","
    );
}

function cariPasien(){
    table = $('#table_laporan_radiologi').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": ci_baseurl + "laporan/laporanrad/ajax_list_radiologi",
        "type": "GET",
        "data" : function(d){
            d.tgl_awal = $("#tgl_awal").val();
            d.tgl_akhir = $("#tgl_akhir").val();
        }
      },
      //Set column definition initialisation properties.
    });
    table.columns.adjust().draw();
}