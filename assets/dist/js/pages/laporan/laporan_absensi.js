var table_absensi_perorangan;
$(document).ready(function(){
    table_absensi_perorangan = $('#table_absensi_perorangan').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true,
      "searching": false, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "laporan/laporan_absensi/ajax_list_laporan_absensi",
          "type": "GET",
          "data"  : function(d){
                    d.tgl_awal       = $("#tgl_awal").val();
                    d.tgl_akhir      = $("#tgl_akhir").val();  
                }
      },
      //Set column definition initialisation properties.
      // "columnDefs": [
      // { 
      //   "targets": [ -1,0 ], //last column
      //   "orderable": false //set not orderable
      // }
      // ]

    });
    table_absensi_perorangan.columns.adjust().draw();  
    $("#nama_users").keyup(function () {
        $.ajax({
            type: "POST",
            url: ci_baseurl + "laporan/laporan_absensi/autocomplete_users",
            data: {
                keyword: $("#nama_users").val()
            },
            dataType: "json",
            success: function (data) {
                var ret = data.success;
                if (ret == true) {
                    // console.log(data.data);
                    // console.log('masuk success');
                    // console.log(data.data);
                    
                    $(".autocomplete").autocomplete({
                        max: 10,
                        scrollHeight: 250,
                        source: data.data,
                        open: function (event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 2");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index", "999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                          console.log(ui.item)
                          $('#id_user').attr("value", ui.item.id_user);
                            $('#id_user').val(ui.item.id_user);
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                            .append("<a><b><font size=2>" + item.value + "</font></b></a>")
                            .appendTo(ul);
                    };
                } else {
                    console.log('masuk else');
                    // $("#id_m_pasien").val('0#');
                }
            }
        });
    });
    $("#searchLaporan").on("click",function(){
        table_absensi_perorangan = $('#table_absensi_perorangan').DataTable({ 
          "destroy": true,
                "ordering": false,
          "processing": true, //Feature control the processing indicator.
          "serverSide": true,
          "searching": false, //Feature control DataTables' server-side processing mode.
    //      "scrollX": true,

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": ci_baseurl + "laporan/laporan_absensi/ajax_get_list_absen_user",
              "type": "GET",
              "data"  : function(d){
                        d.id_user = $("#id_user").val();
                        d.tgl_awal       = $("#tgl_awal").val();
                        d.tgl_akhir      = $("#tgl_akhir").val();  
                    }
          },

        });
        table_absensi_perorangan.columns.adjust().draw();
        
    })
    $("#tab_klinik").on("click",function(){
      table_absensi_klinik = $('#table_absensi_klinik').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": false, //Feature control DataTables' server-side processing mode.
  //      "scrollX": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "laporan/laporan_absensi/ajax_list_laporan_absensi_klinik",
            "type": "GET",
            "data"  : function(d){
                      d.tgl_awal       = $("#tgl_awal").val();
                      d.tgl_akhir      = $("#tgl_akhir").val();
                      d.nama_klinik      = $("#nama_klinik").val();  
                  }
        },
        //Set column definition initialisation properties.
        // "columnDefs": [
        // { 
        //   "targets": [ -1,0 ], //last column
        //   "orderable": false //set not orderable
        // }
        // ]

      });
      table_absensi_klinik.columns.adjust().draw();
    })
});

function exportToExcelAbsensiPerorangan() {
    var tgl_awal = $("#tgl_awal").val();
    var tgl_akhir = $("#tgl_akhir").val();
    window.location.href = ci_baseurl + "laporan/laporan_absensi/export_absensi_laporan_perorangan/"+tgl_awal+"/"+tgl_akhir;
}