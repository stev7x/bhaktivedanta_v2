var tb_laboratorium;
$(document).ready(function(){
    tb_laboratorium = $('#table_laporan_laboratorium').DataTable({

        // "footerCallback": function ( row, data, start, end, display ) {
        //     var api = this.api(), data;
 
        //     // Remove the formatting to get integer data for summation
        //     var intVal = function ( i ) {
        //         return typeof i === 'string' ?
        //             i.replace(/[\$,]/g, '')*1 :
        //             typeof i === 'number' ?
        //                 i : 0;
        //     };
 
        //     // Total over all pages
        //     total = api
        //         .column( 2 )
        //         .data()
        //         .reduce( function (a, b) {
        //             return intVal(a) + intVal(b);
        //         }, 0 );

        //     console.log(total);
 
        //     // Total over this page
        //     pageTotal = api
        //         .column( 2, { page: 'current'} )
        //         .data()
        //         .reduce( function (a, b) {
        //             return intVal(a) + intVal(b);
        //         }, 0 );
 
        //     // Update footer
        //     $( api.column( 2 ).footer() ).html(
        //         // pageTotal+' ('+total+' total)'
        //         formatNumber(total)
        //     );
        // },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",
        "bFilter": false,
        // "paging" : false,
       

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "laporan/laporanlab/ajax_list_laboratorium",
            "type": "GET",
            "data" : function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
            }
        },
       
    });
    tb_laboratorium.columns.adjust().draw();
    
//     $('button#searchLaporan').click(function(){
//         tb_laboratorium.columns.adjust().draw();
//     });

//     var year = new Date().getFullYear();
//     $('.datepicker_awal').pickadate({
//         selectMonths: true, // Creates a dropdown to control month
//         selectYears: 90, // Creates a dropdown of 10 years to control year
//         format: 'dd mmmm yyyy',
//         max: new Date(year,12),
//         closeOnSelect: true,
//         onSet: function (ele) {
//             if(ele.select){
//                    this.close();
//             }
//         }
//     });
//     $('.datepicker_akhir').pickadate({
//         selectMonths: true, // Creates a dropdown to control month
//         selectYears: 90, // Creates a dropdown of 10 years to control year
//         format: 'dd mmmm yyyy',
//         max: new Date(year,12),
//         closeOnSelect: true,
//         onSet: function (ele) {
//             if(ele.select){
//                    this.close();
//             }
//         }
//     });
});

// function formatNumber(toFormat) {
//     return toFormat.toString().replace(
//       /\B(?=(\d{3})+(?!\d))/g, ","
//     );
// }

// function printToExcelLab(){
// 	var sdate = $('#tgl_awal').val();
// 	var edate = $('#tgl_akhir').val();
	
// 	if (sdate == ""){ sdate = "undefined"; }
// 	if (edate == ""){ edate = "undefined"; }
	
// 	window.location.href = ci_baseurl + "laporan/laporanlab/export_excel_laboratorium/"+ sdate +"/"+ edate;
// }

function printToExcelLab(){
    var tgl_awal_rj = $("#tgl_awal").val();
    var tgl_akhir_rj = $("#tgl_akhir").val();

    if (tgl_awal_rj == ""){ tgl_awal_rj = "undefined"; }
    if (tgl_akhir_rj == ""){ tgl_akhir_rj = "undefined"; }

    window.location.href = ci_baseurl + "laporan/laporanlab/export_excel_laboratorium/"+ tgl_awal_rj +"/"+ tgl_akhir_rj; 
}

function formatNumber(toFormat) {
    return toFormat.toString().replace(
      /\B(?=(\d{3})+(?!\d))/g, ","
    );
}

function cariPasien(){
    tb_laboratorium = $('#table_laporan_laboratorium').DataTable({
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": ci_baseurl + "laporan/laporanlab/ajax_list_laboratorium",
        "type": "GET",
        "data" : function(d){
            d.tgl_awal = $("#tgl_awal").val();
            d.tgl_akhir = $("#tgl_akhir").val();
        }
      },
      //Set column definition initialisation properties.
    });
    tb_laboratorium.columns.adjust().draw();
}