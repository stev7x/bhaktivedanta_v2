var 
    table_rj,
    table_rd,
    table_ri,
    table_poli_obgyn,
    table_poli_keseluruhan,
    table_poli_rinci,
    table_rm_rj,
    table_hasil_vaksinasi,
    table_pendaftaran_poli,
    table_dinkes;

$(document).ready(function(){
    // table_rd = $('#table_rawat_darurat').DataTable({
    //     "processing": true, //Feature control the processing indicator.
    //     "serverSide": true, //Feature control DataTables' server-side processing mode.
    //     "bFilter"   : false,
    //     "ordering"  : false,
    //     // Load data for the table's content from an Ajax source
    //     "ajax": {
    //         "url"   : ci_baseurl + "laporan/laporan_rm/ajax_list_rawat_darurat",
    //         "type"  : "GET",  
    //         "data"  : function(d){
    //             d.tgl_awal_rd       = $("#tgl_awal_rd").val();
    //             d.tgl_akhir_rd      = $("#tgl_akhir_rd").val();  
    //             d.status_kunjungan  = $("#status_kunjungan_rd").val(); 
    //         }
    //     }
    // });
    // table_rd.columns.adjust().draw();
    
    $('button#searchLaporanRj').click(function(){
        table_rj.columns.adjust().draw();
    });
      
    $('button#searchLaporanRd').click(function(){
        if ($.fn.dataTable.isDataTable('#table_rawat_darurat')) table_rd.destroy();
        table_rd = $('#table_rawat_darurat').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "bFilter"   : false,
            "ordering"  : false,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"   : ci_baseurl + "laporan/laporan_rm/ajax_list_rawat_darurat",
                "type"  : "GET",  
                "data"  : function(d){
                    d.tgl_awal_rd       = $("#tgl_awal_rd").val();
                    d.tgl_akhir_rd      = $("#tgl_akhir_rd").val();  
                    d.status_kunjungan  = $("#status_kunjungan_rd").val(); 
                }
            }
        });
        table_rd.columns.adjust().draw();
    });   
    $('button#searchLaporanRi').click(function(){
        if ($.fn.dataTable.isDataTable('#table_rawat_inap')) table_ri.destroy();
        table_ri = $('#table_rawat_inap').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "bFilter"   : false,
            "ordering"  : false,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"   : ci_baseurl + "laporan/laporan_rm/ajax_list_rawat_inap",
                "type"  : "GET",  
                "data"  : function(d){
                    d.tgl_awal_rd       = $("#tgl_awal_ri").val();
                    d.tgl_akhir_rd      = $("#tgl_akhir_ri").val();  
                    d.status_kunjungan  = $("#status_kunjungan_ri").val(); 
                }
            }
        });
        table_ri.columns.adjust().draw();
    });

    //REPORT RAJAL
    $('button#searchLaporanPoliObgyn').click(function(){
        table_poli_obgyn = $('#table_poli_obgyn').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "bFilter"   : false,
            "ordering"  : false,
            "destroy"   : true,            
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"   : ci_baseurl + "laporan/laporan_rm/ajax_list_poli_obgyn",
                "type"  : "GET",  
                "data"  : function(d){
                    d.tgl_awal_obgyn  = $("#tgl_awal_rj").val();
                    d.tgl_akhir_obgyn = $("#tgl_akhir_rj").val();  
                }
            }
        });
        table_poli_obgyn.columns.adjust().draw();     
    });
    
    //REPORT KLINIK RINCI
    $('button#searchLaporanPoliKlinikRinci').click(function(){
            table_poli_rinci = $('#table_poli_rinci').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ordering"  : false, 
            "bFilter"   : false,  
            "destroy"   : true,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"       : ci_baseurl + "laporan/laporan_rm/ajax_list_poli_rinci",
                "type"    : "GET",
                "data"    : function(d){

                    d.tgl_awal_rj  = $("#tgl_awal_rj").val();
                    d.tgl_akhir_rj = $("#tgl_akhir_rj").val();
                    d.filterBy     = $("#berdasarkan").val();
                }
            }
        });    
        table_poli_rinci.columns.adjust().draw();
    });  

    //REPORT KLINIK KESELURUHAN
    $('button#searchLaporanPoliKlinikKeseluruhan').click(function(){
        table_poli_keseluruhan = $('#table_poli_keseluruhan').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ordering"  : false, 
            "bFilter"   : false,  
            "destroy"   : true,                        
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"       : ci_baseurl + "laporan/laporan_rm/ajax_list_poli_keseluruhan",
                  "type"    : "GET",
                  "data"    : function(d){
    
                     d.tgl_awal_rj  = $("#tgl_awal_rj").val();
                     d.tgl_akhir_rj = $("#tgl_akhir_rj").val();
                     d.filterBy     = $("#berdasarkan").val();
                  }
            }
        });
        table_poli_keseluruhan.columns.adjust().draw();    
    });

    //REPORT RM RJ
    $('button#searchLaporanRmRj').click(function(){
        table_rm_rj = $('#table_rm_rj').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ordering"  : false, 
            "bFilter"   : false,  
            "destroy"   : true,            
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"       : ci_baseurl + "laporan/laporan_rm/ajax_list_rm_rj",
                  "type"    : "GET",
                  "data"    : function(d){
    
                     d.tgl_awal_rj  = $("#tgl_awal_rj").val();
                     d.tgl_akhir_rj = $("#tgl_akhir_rj").val();
                     d.filterBy     = $("#berdasarkan").val();
                  }
            }
        });
        table_rm_rj.columns.adjust().draw();
    });

    //REPORT HASIL VAKSINASI
    $('button#searchLaporanHasilVaksinasi').click(function(){
        table_hasil_vaksinasi = $('#table_hasil_vaksinasi').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "bFilter"   : false,
            "ordering"  : false,
            "destroy"   : true,                         
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"   : ci_baseurl + "laporan/laporan_rm/ajax_list_hasil_vaksinasi",
                "type"  : "GET",  
                "data"  : function(d){
                    d.tgl_awal_rj       = $("#tgl_awal_rj").val();
                    d.tgl_akhir_rj      = $("#tgl_akhir_rj").val();  
                    d.status_kunjungan  = $("#status_kunjungan_rd").val(); 
                    d.tipe = 1; 
                }
            }
        });
        table_hasil_vaksinasi.columns.adjust().draw();     
    });

    //REPORT PENDAFTARAN POLI
    $('button#searchLaporanPendaftaranPoli').click(function(){
        table_pendaftaran_poli = $('#table_pendaftaran_poli').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ordering"  : false, 
            "bFilter"   : false,  
            "destroy"   : true,                        
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"       : ci_baseurl + "laporan/laporan_rm/ajax_list_pendaftaran_poli",
                  "type"    : "GET",
                  "data"    : function(d){
                     d.tgl_awal_rj  = $("#tgl_awal_rj").val();
                     d.tgl_akhir_rj = $("#tgl_akhir_rj").val();
                  }
            }
        });
        table_pendaftaran_poli.columns.adjust().draw();
    });

    //REPORT PENDAFTARAN DINKES KB
    $('button#searchLaporanDinkes').click(function(){
        table_dinkes = $('#table_dinkes').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ordering"  : false, 
            "bFilter"   : false,  
            "destroy"   : true,                        
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url"    : ci_baseurl + "laporan/laporan_rm/ajax_list_laporan_dinkes",
                  "type" : "GET",
                  "data" : function(d){
                     d.tgl_awal_rj  = $("#tgl_awal_rj").val();
                     d.tgl_akhir_rj = $("#tgl_akhir_rj").val();
                  }
            }
        });
        table_dinkes.columns.adjust().draw();    
    });
});
 
//FUNCTION PRINT 
// START
function printToExcelRj(){ 
    var 
        tgl_awal_rj     = $("#tgl_awal_rj").val(),
        tgl_akhir_rj    = $("#tgl_akhir_rj").val(),
        poliklinik_rj   = $("#poliklinik_rj").val(),
        rm_awal         = $("#rm_awal").val(),
        rm_akhir        = $("#rm_akhir").val();

    if (tgl_awal_rj == ""){ tgl_awal_rj = "undefined"; }
    if (tgl_akhir_rj == ""){ tgl_akhir_rj = "undefined"; }
    if (poliklinik_rj == null){ poliklinik_rj = "undefined"; }

    window.location.href = ci_baseurl + "laporan/laporan_rm/export_excel_rj/"+ tgl_awal_rj +"/"+ tgl_akhir_rj +"/"+ poliklinik_rj+ "/"+ rm_awal + "/" + rm_akhir;
}
function printToExcelRd(){
    var tgl_awal_rd = $("#tgl_awal_rd").val();
    var tgl_akhir_rd = $("#tgl_akhir_rd").val();

    if (tgl_awal_rd == ""){ tgl_awal_rd = "undefined"; }
    if (tgl_akhir_rd == ""){ tgl_akhir_rd = "undefined"; }

    window.location.href = ci_baseurl + "laporan/laporan_rm/export_excel_rd/"+ tgl_awal_rd +"/"+ tgl_akhir_rd;
}
function printToExcelRi(){
    var tgl_awal_ri = $("#tgl_awal_ri").val();
    var tgl_akhir_ri = $("#tgl_akhir_ri").val();
    var dokter_ri = $("#dokter_pj_ri").val();
    var nama_ri = $("#nama_pasien_ri").val();


    if (tgl_awal_ri == ""){ tgl_awal_ri = "undefined"; }
    if (tgl_akhir_ri == ""){ tgl_akhir_ri = "undefined"; }
    if (dokter_ri == null){ dokter_ri = "undefined"; }
    if (nama_ri == ""){ nama_ri = "undefined"; }

    window.location.href = ci_baseurl + "laporan/laporan_rm/export_excel_ri/"+ tgl_awal_ri +"/"+ tgl_akhir_ri+"/"+ dokter_ri+"/"+ nama_ri;
}

function exportToExcelBidan() {
    var tgl_awal_obgyn = $("#tgl_awal_rj").val();
    var tgl_akhir_obgyn = $("#tgl_akhir_rj").val();
    window.location.href = ci_baseurl + "laporan/laporan_rm/exportToExcelBidan/"+tgl_awal_obgyn+"/"+tgl_akhir_obgyn;
}

function exportToExcelPoliUmumKeseluruhan() {
    var tgl_awal_rj = $("#tgl_awal_rj").val();
    var tgl_akhir_rj = $("#tgl_akhir_rj").val();
    var para = $("#berdasarkan").val();
    window.location.href = ci_baseurl + "laporan/laporan_rm/exportToExcelPoliUmumKeseluruhan/"+tgl_awal_rj+"/"+tgl_akhir_rj+"/"+para;
}

function exportToExcelPoliUmumRinci() {
    var tgl_awal_rj = $("#tgl_awal_rj").val();
    var tgl_akhir_rj = $("#tgl_akhir_rj").val();
    var filterBy = $("#berdasarkan").val();    
    window.location.href = ci_baseurl + "laporan/laporan_rm/exportToExcelPoliUmumRinci/" + tgl_awal_rj + "/" + tgl_akhir_rj + "/" + filterBy;
}

function exportToExcelRM() {
    var tgl_awal_rj = $("#tgl_awal_rj").val();
    var tgl_akhir_rj = $("#tgl_akhir_rj").val();
    window.location.href = ci_baseurl + "laporan/laporan_rm/exportToExcelRMBaru/"+tgl_awal_rj+"/"+tgl_akhir_rj;
}

function exportToExcelPendaftaran() {
    var tgl_awal_rj = $("#tgl_awal_rj").val();
    var tgl_akhir_rj = $("#tgl_akhir_rj").val();
    window.location.href = ci_baseurl + "laporan/laporan_rm/exportToExcelPendaftaran/"+tgl_awal_rj+"/"+tgl_akhir_rj;
}

function exportToExcelDinkes() {
    var tgl_awal_rj = $("#tgl_awal_rj").val();
    var tgl_akhir_rj = $("#tgl_akhir_rj").val();
    window.location.href = ci_baseurl + "laporan/laporan_rm/exportToExcelDinkes/"+tgl_awal_rj+"/"+tgl_akhir_rj;
}

function exportToExcelHasilvaksinasi(tipe) {
    var tgl_awal_rj = $("#tgl_awal_rj").val();
    var tgl_akhir_rj = $("#tgl_akhir_rj").val();
    window.location.href = ci_baseurl + "laporan/laporan_rm/exportToExcelHasilvaksinasi/"+tgl_awal_rj+"/"+tgl_akhir_rj+"/" + tipe;
}
//END


function testingChart() {
    var tgl_awal_rj = $("#tgl_awal_rj").val();
    var tgl_akhir_rj = $("#tgl_akhir_rj").val();
    window.location.href = ci_baseurl + "laporan/laporan_rm/testing_chart/"+tgl_awal_rj+"/"+tgl_akhir_rj;
}


// function chart
function testing() {
    var charts_perbandingan1 = $("#charts_perbandingan1").val();
    var pilih_data = $("#pilih_data").val();
    var tahun_charts = $("#tahun_charts").val();
    var bulan_charts = $("#bulan_charts").val();
    var tgl_awal_charts = $("#tgl_awal_charts").val();
    var tgl_akhir_charts = $("#tgl_akhir_charts").val();
    $.ajax({
        url: ci_baseurl + "laporan/laporan_rm/getDataChart/"+ pilih_data+ "/" + charts_perbandingan1+"/" + pilih_data + "/" + tahun_charts + "/" + bulan_charts + "/" + tgl_awal_charts + "/" + tgl_akhir_charts,
        type : 'GET',
        dataType : 'JSON',
        success : function (data) {
            $('#morris-area-chart1').html("");
            $('h3').html("");
        console.log(data);
        if (pilih_data == 1){
            $("h3").append("<b>DATA PENYAKIT</b>");
            
            Morris.Bar({
                element         : 'morris-area-chart1',
                data            : data,
                xkey            : 'nama_icd10',
                ykeys           : ['jum'],
                labels          : ['JUMLAH'],
                xLabelAngle     : 60,
                behaveLikeLine  : true,
                lineWidth       : 0,
                smooth          : false,
                hideHover       : 'auto',
                resize          : true,
                
                barColors: function (row, series, type) {
                    if (type === 'bar') {
                        var red = Math.ceil(255 * row.y / this.ymax);
                        return 'rgb(' + red + ',0,0)';
                    }
                    else {
                        return '#000';
                    }
                }
            });  
        } else if (pilih_data == 2){
            $("h3").append("<b>DATA PEKERJAAN PASIEN</b>");
            Morris.Bar({
                element         : 'morris-area-chart1',
                data            : data,
                xkey            : 'nama_pekerjaan',
                ykeys           : ['jum'],
                labels          : ['JUMLAH'],
                xLabelAngle     : 60,
                behaveLikeLine  : true,
                lineWidth       : 0,
                smooth          : false,
                hideHover       : 'auto',
                resize          : true,
                
                barColors: function (row, series, type) {
                    if (type === 'bar') {
                        var red = Math.ceil(230 * row.y / this.ymax);
                        return 'rgb(' + red + ',0,0)';
                    }
                    else {
                        return '#000';
                    }
                }
            });  
        } else if (pilih_data == 3){
            $("h3").append("<b>DATA PENDIDIKAN PASIEN</b>");
            Morris.Bar({
                element         : 'morris-area-chart1',
                data            : data,
                xkey            : 'pendidikan_nama',
                ykeys           : ['jum'],
                labels          : ['JUMLAH'],
                xLabelAngle     : 60,
                behaveLikeLine  : true,
                lineWidth       : 0,
                smooth          : false,
                hideHover       : 'auto',
                resize          : true,
                
                barColors: function (row, series, type) {
                    if (type === 'bar') {
                        var red = Math.ceil(255 * row.y / this.ymax);
                        return 'rgb(' + red + ',0,0)';
                    }
                    else {
                        return '#000';
                    }
                }
            });  

        } else if (pilih_data == 4){
            $("h3").append("<b>DATA PEKERJAAN PJ</b>");
            Morris.Bar({
                element         : 'morris-area-chart1',
                data            : data,
                xkey            : 'nama_pekerjaan',
                ykeys           : ['jum'],
                labels          : ['JUMLAH'],
                xLabelAngle     : 60,
                behaveLikeLine  : true,
                lineWidth       : 0,
                smooth          : false,
                hideHover       : 'auto',
                resize          : true,
                
                barColors: function (row, series, type) {
                    if (type === 'bar') {
                        var red = Math.ceil(255 * row.y / this.ymax);
                        return 'rgb(' + red + ',0,0)';
                    }
                    else {
                        return '#000';
                    }
                }
            });  
        } else if (pilih_data == 5){
            $("h3").append("<b>DATA KUNJUNGAN PASIEN RJ</b>");
            Morris.Bar({
                element         : 'morris-area-chart1',
                data            : data,
                xkey            : 'status',
                ykeys           : ['jum'],
                labels          : ['JUMLAH'],
                xLabelAngle     : 30,
                behaveLikeLine  : true,
                lineWidth       : 0,
                smooth          : false,
                hideHover       : 'auto',
                resize          : true,
                
                barColors: function (row, series, type) {
                    if (type === 'bar') {
                        var red = Math.ceil(255 * row.y / this.ymax);
                        return 'rgb(' + red + ',0,0)';
                    }
                    else {
                        return '#000';
                    }
                }
            });  
        } else if (pilih_data == 6){
            $("h3").append("<b>DATA PEMBAYARAN PASIEN RJ TH 2018</b>");
            Morris.Bar({
                element         : 'morris-area-chart1',
                data            : data,
                xkey            : 'bulan',
                ykeys           : ['pribadi','asuransi','bpjs'],
                labels          : ['PRIBADI','ASURANSI','BPJS'],
                xLabelAngle     : 30,
                behaveLikeLine  : true,
                lineWidth       : 0,
                smooth          : false,
                hideHover       : 'auto',
                resize          : true,
                
                barColors: function (row, series, type) {
                    if (type === 'bar') {
                        var red = Math.ceil(255 * row.y / this.ymax);
                        return 'rgb(' + red + ',0,0)';
                    }
                    else {
                        return '#000';
                    }
                }
            });  

        }

        }
    });
    
}


function pilihBerdasarkanCharts() {
    var berdasarkan = $("#charts_perbandingan1").val();
    console.log(berdasarkan);
    if(berdasarkan == 1){
        $("#berdasarkan_tahun").show();
        $("#berdasarkan_data").show();
        $("#berdasarkan_tgl_awal").hide();
        $("#berdasarkan_tgl_akhir").hide();
        $("#berdasarkan_bulan").hide();
        console.log("masuk");
    }else if(berdasarkan == 2){
        $("#berdasarkan_tahun").show();
        $("#berdasarkan_bulan").show();
        $("#berdasarkan_data").show();
        $("#berdasarkan_tgl_awal").hide();
        $("#berdasarkan_tgl_akhir").hide();
    }else if(berdasarkan == 3){
        $("#berdasarkan_tahun").hide();
        $("#berdasarkan_bulan").hide();
        $("#berdasarkan_data").show();
        $("#berdasarkan_tgl_awal").show();
        $("#berdasarkan_tgl_akhir").show();

    }else{
        $("#berdasarkan_tahun").hide();
        $("#berdasarkan_bulan").hide();
        $("#berdasarkan_data").hide();
        $("#berdasarkan_tgl_awal").hide();
        $("#berdasarkan_tgl_akhir").hide();
    }

}


$("#tahun_charts").datepicker({
    format: "yyyy", // Notice the Extra space at the beginning
    viewMode: "years",
    minViewMode: "years"
});

$("#bulan_charts").datepicker({
    format: "mm", // Notice the Extra space at the beginning
    viewMode: "months",
    minViewMode: "months"
});







