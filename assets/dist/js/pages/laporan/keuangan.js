var table;
var table2;
$(document).ready(function(){
    table = $('#table_laporan_keuangan').DataTable({
   
        //
        // "processing": true, //Feature control the processing indicator.
        // "serverSide": true, //Feature control DataTables' server-side processing mode.
        // // "sScrollX": "100%",
        // "bFilter": false,
        // // "paging" : false,
        // "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,
        // "responsive": true,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "laporan/keuangan/ajax_list_pemasukan",
            "type": "GET",
            "data" : function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
            }
        },
        "columns": [
            { "width": "80px" },
            { "width": "80px" },
            { "width": "150px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "150px" },
            { "width": "80px" },
            { "width": "300px" },
            { "width": "300px" },
            { "width": "300px" },
            { "width": "300px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" },
            { "width": "80px" }
          ],
        rowsGroup: [
            // Always the array (!) of the column-selectors in specified order to which rows groupping is applied
            // (column-selector could be any of specified in https://datatables.net/reference/type/column-selector)
            0
        ]
    });
    // table.columns.adjust().draw();
    
    table2 = $('#table_laporan_pengeluaran').DataTable({

        // "footerCallback": function ( row, data, start, end, display ) {
        //     var api = this.api(), data;
 
        //     // Remove the formatting to get integer data for summation
        //     var intVal = function ( i ) {
        //         return typeof i === 'string' ?
        //             i.replace(/[\$,]/g, '')*1 :
        //             typeof i === 'number' ?
        //                 i : 0;
        //     };
 
        //     // Total over all pages
        //     total = api
        //         .column( 2 )
        //         .data()
        //         .reduce( function (a, b) {
        //             return intVal(a) + intVal(b);
        //         }, 0 );

        //     console.log(total);
 
        //     // Total over this page
        //     pageTotal = api
        //         .column( 2, { page: 'current'} )
        //         .data()
        //         .reduce( function (a, b) {
        //             return intVal(a) + intVal(b);
        //         }, 0 );
 
        //     // Update footer
        //     $( api.column( 2 ).footer() ).html(
        //         pageTotal+' ('+total+' total)'
        //         formatNumber(total)
        //     ); 
        // },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  
        "bFilter": false,
        "paging" : false,  
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "laporan/keuangan/ajax_list_pengeluaran",
            "type": "GET",
            "data" : function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
            }
        },
        rowsGroup: [
            // Always the array (!) of the column-selectors in specified order to which rows groupping is applied
            // (column-selector could be any of specified in https://datatables.net/reference/type/column-selector)
            0
        ]
    });
    table2.columns.adjust().draw();
    
//     $('button#searchLaporan').click(function(){
//         table.columns.adjust().draw();
//         table2.columns.adjust().draw();
//     });

//     var year = new Date().getFullYear();
//     $('.datepicker_awal').pickadate({
//         selectMonths: true, // Creates a dropdown to control month
//         selectYears: 90, // Creates a dropdown of 10 years to control year
//         format: 'dd mmmm yyyy',
//         max: new Date(year,12),
//         closeOnSelect: true,
//         onSet: function (ele) {
//             if(ele.select){
//                    this.close();
//             }
//         }
//     });
//     $('.datepicker_akhir').pickadate({
//         selectMonths: true, // Creates a dropdown to control month
//         selectYears: 90, // Creates a dropdown of 10 years to control year
//         format: 'dd mmmm yyyy',
//         max: new Date(year,12),
//         closeOnSelect: true,
//         onSet: function (ele) {
//             if(ele.select){
//                    this.close();
//             }
//         }
//     });
});

function exportToExcelLaporanKeuangan() {
    window.location.href = ci_baseurl + "laporan/keuangan/export_laporan_keuangan";
}

function formatNumber(toFormat) {
    return toFormat.toString().replace(
      /\B(?=(\d{3})+(?!\d))/g, ","
    );
}  