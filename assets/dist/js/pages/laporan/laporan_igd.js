var table_laporan;
function cariPasien(){
    table_laporan = $('#table_laporan_igd').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": ci_baseurl + "laporan/laporan_igd/ajax_list_laporan_igd",
        "type": "GET",
        "data" : function(d){
            d.tahun = $("#tahun").val();
            d.bulan = $("#bulan").val();
        }
      },
      //Set column definition initialisation properties.
    });
    table_laporan.columns.adjust().draw();
}

function exportToExcel() {
    var tgl_awal = $("#tgl_awal").val();
    var tgl_akhir = $("#tgl_akhir").val();

    if (tgl_awal == ""){ tgl_awal = "undefined"; }
    if (tgl_akhir == ""){ tgl_akhir = "undefined"; }
    window.location.href = ci_baseurl + "laporan/laporan_igd/exportToExcel/"+ tgl_awal +"/"+ tgl_akhir;
}