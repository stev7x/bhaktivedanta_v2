var table_group;
$(document).ready(function(){
	role_create();
    table_group = $('#table_group_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "configuration/group_management/ajax_list_group",
          "type": "GET",
      },
	  "order": [[1, 'asc']],
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });
    table_group.columns.adjust().draw();
    
    $('button#saveGroup').click(function(){
        swal({
          text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){ 
            var role_list = $('#role_list').jstree('get_selected');
            $.ajax({
                    url: ci_baseurl + "configuration/group_management/do_create_group",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateGroup').serialize() + '&tree_res=' + role_list,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#modal_add_group').modal('hide');
                            $('#fmCreateGroup').find("input[type=text]").val("");
                            role_create();
                            table_group.columns.adjust().draw();
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                        }
                    }
                }); 
            
          swal( 
            'Berhasil!',
            'Data Berhasil Disimpan.', 
          ) 
        })  
    });
	
	$('button#updateGroup').click(function(){
        swal({
          text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){ 
            var role_list = $('#upd_role_list').jstree('get_selected');
                $.ajax({
                    url: ci_baseurl + "configuration/group_management/do_update_group",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateGroup').serialize() + '&tree_res=' + role_list,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_upd_notif').removeClass('red').addClass('green');
                            $('#modal_upd_card_message').html(data.messages);
                            $('.modal_upd_notif').show();
                            $(".modal_upd_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_upd_notif").hide();
                            });  

                            table_group.columns.adjust().draw();
                        } else {
                            $('.modal_upd_notif').removeClass('green').addClass('red');
                            $('#modal_upd_card_message').html(data.messages);
                            $('.modal_upd_notif').show();
                            $(".modal_upd_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_upd_notif").hide();
                            });
                        }
                    }
                });
            
          swal( 
            'Berhasil!',
            'Data Berhasil Disimpan.', 
          ) 
        }) 
    });
});

function role_create(){
    $.ajax({
        url: ci_baseurl + "configuration/group_management/ajax_role_create",
        type: 'get', 
        dataType: 'json',
        data: {}, 
        success: function(data) {
            $('#role_list').html("");
            $.jstree.destroy();
            var ret = data.success;
            if(ret === true){ 
                jstree = $("#role_list").jstree({
                    "core" : {
                        "data" : data.data_valid
                        //"themes" : {
                        //    "variant" : "large"
                        //}
                    },
                    "checkbox" : {
                        "keep_selected_style" : false,
                        "three_state" : false,
                        //"tie_selection" : false
                    },
                    "plugins" : [ "checkbox" ]
                });
            }else{
                $('#role_list').html(data.messages);
            }
        }
    });
}

function add_group(){
    $('#modal_add_group').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });  
} 


function editGroup(group_id){
    if(group_id){    
        $.ajax({
        url: ci_baseurl + "configuration/group_management/get_groupby_id",
        type: 'get',
        dataType: 'json',
        data: {group_id:group_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    //console.log(data.data);
                    $('#upd_group_id').val(data.data.id);
                    $('#upd_group_name').val(data.data.name);
                    $('#upd_definition').val(data.data.definition);
                    $('#modal_update_group').modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    });
                    perm_update(group_id);  
                } else {
                    $('.modal_upd_notif').removeClass('green').addClass('red');
					$('#modal_upd_card_message').html(data.messages);
					$('.modal_upd_notif').show();
					$(".modal_upd_notif").fadeTo(2000, 500).slideUp(500, function(){
						$(".modal_upd_notif").hide();
					});
                }
            }
        });
    }
    else
    {
        $('.notif').removeClass('green').addClass('red');
		$('#card_message').html('Group ID tidak ditemukan');
		$('.notif').show();
		$(".notif").fadeTo(2000, 500).slideUp(500, function(){
			$(".notif").hide();
		});
    }
}

function perm_update(group_id){
    $.ajax({
        url: ci_baseurl + "configuration/group_management/ajax_role_update",
        type: 'get',
        dataType: 'json',
        data: {group_id:group_id}, 
        success: function(data) {
            $('#upd_role_list').html("");
            $.jstree.destroy();
            var ret = data.success;
            if(ret === true){
                jstree = $("#upd_role_list").jstree({
                    "core" : {
                        "data" : data.data_valid
                        //"themes" : {
                        //    "variant" : "large"
                        //}
                    },
                    "checkbox" : {
                        "keep_selected_style" : false,
                        "three_state" : false,
//                        "tie_selection" : false
                    },
                    "plugins" : [ "checkbox" ]
                });
            }else{
                $('#upd_role_list').html(data.messages);
            }
        }
    });
}

function deleteGroup(group_id){
    var jsonVariable = {};
    jsonVariable["group_id"] = group_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(group_id){

        swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'    
        }).then(function(){ 
            // var role_list = $('#role_list').jstree('get_selected');
           $.ajax({
                url: ci_baseurl + "configuration/group_management/do_delete_group",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_group.columns.adjust().draw();
                        } else {   
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                        }
                    }
                });      
             
          swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
        })  
    }
    else
    {
		$('.notif').removeClass('green').addClass('red');
		$('#card_message').html('Group ID tidak ditemukan');
		$('.notif').show();
		$(".notif").fadeTo(2000, 500).slideUp(500, function(){
			$(".notif").hide();
		});
    }
}