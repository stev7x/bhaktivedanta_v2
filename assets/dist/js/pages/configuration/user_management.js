var table_user;
$(document).ready(function(){
//    $('#table_user_list').DataTable();
    table_user = $('#table_user_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "configuration/user_management/ajax_list_user",
          "type": "GET",
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });
    table_user.columns.adjust().draw();
    
    // $('.tooltipped').tooltip({delay: 50});
    
    $('button#saveUser').click(function(){
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){ 
            $.ajax({
                url: ci_baseurl + "configuration/user_management/do_create_user",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateUser').serialize(),
                success: function(data) {    
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if(ret === true) {
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#modal_add_user').modal('hide');
                        $('#fmCreateUser').find("input[type=text]").val("");
                        $('#fmCreateUser').find("input[type=text]").val("");
                        $('#fmCreateUser').find("select").prop('selectedIndex',0);
                        table_user.columns.adjust().draw();  
                    } else {
                        $('.modal_notif').removeClass('green').addClass('red');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                    }
                }
            });
        swal(
            'Berhasil!',
            'Data Berhasil Disimpan.',
            )
        })
    });
});

function deleteUser(user_id){  
    var jsonVariable = {};
    jsonVariable["user_id"] = user_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    if(user_id){  

        swal({ 
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true, 
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'    
        }).then(function(){ 
            // var role_list = $('#role_list').jstree('get_selected');
           $.ajax({
                url: ci_baseurl + "configuration/user_management/do_delete_user",
                type: 'post', 
                dataType: 'json',
                data: jsonVariable,     
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                            console.log('masuk-if');                  
                        if(ret === true) {
                            console.log('masuk-if');                  
                            table_user.columns.adjust().draw();  
                              // $('.notif').removeClass('red').addClass('green');
                            // $('#card_message').html(data.messages);
                            // $('.notif').show();
                            // $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                            //     $(".notif").hide();
                            // });   
                        } else {
                            console.log('masuk-false');                  
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                        }
                    }
                });
             
          swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
        })  
    }
    else
    {
    $('.notif').removeClass('green').addClass('red');
    $('#card_message').html('Group ID tidak ditemukan');
    $('.notif').show();
    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
      $(".notif").hide();
    });
    }
}

function checkAllChange(e) {
    if (e.checked) {
        $('input:checkbox').each(function(){
            $(this).prop('checked', true);
        });
    } else {
        $('input:checkbox').each(function(){
            $(this).prop('checked', false);
        });
    }
}

function checkT1(e) {
    if ($('input[parent='+e.value+']').length != 0) {
        if (e.checked) {
            $('input[parent='+e.value+']').each(function() {
                $(this).prop('checked', true);
            });
        } else {
            $('input[parent='+e.val+']').each(function() {
                $(this).prop('checked', false);
            });
        }
    }

    if ($('.check:checked').length == $('.check').length) {
        $('#checkAll').prop('checked', true);
    }
}

function checkT2(e) {
    var parent = $('input[id='+e.value+']').attr('parent');
    var child  = e.value;

    if (e.checked) {
        $($('input[id='+parent+']')).prop('checked', true);

        if ($('input[parent='+child+']').length != 0) {
            $('input[parent='+child+']').each(function() {
                $(this).prop('checked', true);
            });
        }
    } else {
        if ($('input[parent='+parent+']:checked').length == 0) {
            $($('input[id='+parent+']')).prop('checked', false);
        }

        if ($('input[parent='+e.value+']').length != 0) {
            $('input[parent='+e.value+']').each(function() {
                $(this).prop('checked', false);
            });
        }
    }

    if ($('.check:checked').length == $('.check').length) {
        $('#checkAll').prop('checked', true);
    }
}

function checkT3(e) {
    var parent2 = $('input[id='+e.value+']').attr('parent');
    var parent1 = $('input[id='+parent2+']').attr('parent');

    if (e.checked) {
        $($('input[id='+parent2+']')).prop('checked', true);
        $($('input[id='+parent1+']')).prop('checked', true);
    } else {
        if ($('input[parent='+parent2+']:checked').length == 0) {
            $($('input[id='+parent2+']')).prop('checked', false);
        }

        if ($('input[parent='+parent1+']:checked').length == 0) {
            $($('input[id='+parent1+']')).prop('checked', false);
        }
    }

    if ($('.check:checked').length == $('.check').length) {
        $('#checkAll').prop('checked', true);
    }
}

// function checkChange(e) {
//     var parent = "";
//     var child = "";
//
//     if ($('input[id='+e.value+']').attr('parent')) {
//         parent = $('input[id='+e.value+']').attr('parent');
//     } else {
//         child = e.value;
//     }
//
//     if (e.checked) {
//         if (parent == "") {
//             $('input[parent='+child+']').each(function(){
//                 $(this).prop('checked', true);
//
//                 if ($('input[parent='+$(this).val()+']').length != 0) {
//                     $('input[parent='+$(this).val()+']').each(function(){
//                         $(this).prop('checked', true);
//                     });
//                 }
//             });
//         } else {
//             $('input[id='+parent+']').each(function(){
//                 $(this).prop('checked', true);
//             });
//
//             if ($('input[id='+parent+']').attr('parent')) {
//                 $('input[id='+$('input[id='+parent+']').attr('parent')+']').prop('checked', true);
//             }
//         }
//
//         if ($('.check:checked').length == $('.check').length) {
//             $('#checkAll').prop('checked', true);
//         }
//     } else {
//         if (child == "") {
//             if ($('input[parent='+parent+']:checked').length == 0) {
//                 $('input[id='+parent+']').prop('checked', false);
//
//                 if ($('input[parent='+$('input[parent='+parent+']').val()+']').length != 0) {
//                     $('input[parent='+$('input[parent='+parent+']').val()+']').each(function(){
//                         $(this).prop('checked', false);
//                     });
//                 }
//             }
//         } else {
//             $('input[parent='+child+']').each(function(){
//                 $(this).prop('checked', false);
//             });
//         }
//
//         $('#checkAll').prop('checked', false);
//     }
// }