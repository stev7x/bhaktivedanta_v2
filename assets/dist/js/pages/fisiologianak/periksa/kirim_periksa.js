var table_diagnosa_rd;
var table_tindakan_rd;
var table_resep_rd;
var table_obat_rd;
var table_rs_rujukan;
var table_background_pha;
var table_theraphy;
var resepRacikanArr = [];
var theraphyArr = [];
var table_list_diagnosa;
var table_list_diagnosa_icd10_rd;
$(document).ready(function(){
    getTindakan();
    table_diagnosa_rj = $('#table_diagnosa_pasienrj').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "fisiologianak/list_fisiologi_anak/ajax_list_diagnosa_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
//      "columnDefs": [
//      {
//        "targets": [ -1 ], //last column
//        "orderable": false //set not orderable
//      }
//      ]
    });
    table_diagnosa_rj.columns.adjust().draw();

    
    table_rs_rujukan = $('#table_rs_rujukan').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
       "searching": true,
  
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_rs_rujukan",
            "type": "GET",
            
        },
        "columnDefs": [{
              "targets": [ -1 ],
              "orderable": false,
          },],
      });
      table_rs_rujukan.columns.adjust().draw();
    
      table_background_pha = $('#table_background_phatologi_anak').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,
  
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "fisiologianak/list_fisiologi_anak/ajax_list_background_pasien",
            "type": "GET",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        }
      });
      table_background_pha.columns.adjust().draw();

    table_tindakan_rd = $('#table_tindakan_pasienrj').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "fisiologianak/list_fisiologi_anak/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rd.columns.adjust().draw();


    table_resep_rd = $('#table_resep_pasienrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_resep_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_resep_rd.columns.adjust().draw();
    // var year = new Date().getFullYear();
    // $('.datepicker_diagnosa').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    
    // $('.datepicker_tindakan').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    // $('.datepicker_resep').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });
    
    $('#nama_obat').keyup(function() {  
        var val_obat = $('#nama_obat').val();
        $.ajax({
            url: ci_baseurl + "rawatdarurat/pasien_rd/autocomplete_obat",
            type: 'get',
            async: false,
            dataType: 'json',
            data: {val_obat: val_obat},
            success: function(data) { /* console.log(data); */
                var ret = data.success;
                if(ret == true) {
                    // console.log(data.data);
                    $(".autocomplete").autocomplete({
                        source: data.data,
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 0");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index","999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#obat_id').val(ui.item.id);
                            $('#nama_obat').val(ui.item.value);
                            $('#satuan_obat').val(ui.item.satuan);
                            $('#harga_jual').val(ui.item.harga_jual);
                            $('#harga_netto').val(ui.item.harga_netto);
                            $('#current_stok').val(ui.item.current_stok);
                            $('#lbl_nama_obat').addClass('active');
                            $('#lbl_harga_jual').addClass('active');
                            $('#lbl_satuan_obat').addClass('active');
                        }
                  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                      return $( "<li>" )
                        .append( "<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.deskripsi +"- Stok :"+ item.current_stok +"</font></a>" )
                        .appendTo( ul );
                    };
                }else{
                  $("#harga_netto").val('');
                  $("#current_stok").val('');
                  $("#obat_id").val('');
                }
            }
        });
    });

    $('button#saveAssestment').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        tensi = $("#Tensi").val();
        nadi = $("#Nadi").val();
        suhu = $("#Suhu").val();
        nadi_1 = $("#Nadi2").val();
        penggunaan = $("#Penggunaan").val();
        saturasi = $("#Saturasi").val();
        nyeri = $("#Nyeri").val();
        numeric = $("#Numeric").val();
        resiko = $("#Resiko").val();
        var theraphy = JSON.stringify(theraphyArr);
           swal({
               text: "Apakah data yang dimasukan sudah benar ?",
               type: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               cancelButtonText: 'tidak',
               confirmButtonText: 'Ya' 
            }).then(function(){  
                   $.ajax({
                       url: ci_baseurl + "fisiologianak/list_fisiologi_anak/do_create_assestment",
                       type: 'POST',
                       dataType: 'JSON',
                       data: $('form#fmCreateAssesmentPasien').serialize()+"&nama_program="+theraphy,
                       success: function(data) {
                        //    data = JSON.parse(data);
                           var ret = data.success;
                           $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                           $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                           $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                           $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                           $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                           if(ret){
                               $("#Tensi").val("");
                               $("#Nadi").val("");
                               $("#Suhu").val("");
                               $("#Nadi_1").val("");
                               $("#Penggunaan").val("");
                               $("#Saturasi").val("");
                               $("#Resiko").val("");
                               theraphyArr = [];
                               document.getElementById("table_therapy_fisiologianak").innerHTML = "";
                           swal( 
                                 'Berhasil!',
                                 'Data Berhasil Disimpan.',
                                 'success' 
                               )
                           } else {
                               $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                               $('#card_message2').html(data.messages);
                               $('#modal_notif2').show();
                               $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                               $("#modal_notif2").hide();
                               });
                               $("html, body").animate({ scrollTop: 100 }, "fast");
                           swal(
                                   'Gagal!',
                                   'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                   'error'
                               )
                               }
                       }
                   });
             });
    });
    $('button#saveAlergi').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
            swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){  
                
                $.ajax({
                    url: ci_baseurl + "fisiologianak/list_fisiologi_anak/do_create_background_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateBackgroundPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            // $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                            // $('#card_message').html(data.messages);
                            // $('#modal_notif').show();
                            // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            //     $("#modal_notif").hide();
                            // });
                            swal( 
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.', 
                                'success'
                              ) 
                            $('select').prop('selectedIndex',0);
                            $('#fmCreateBackgroundPasien').find("input[type=text]").val("");
                            table_background_pha.columns.adjust().draw();
                        }else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                            
                        }
                    }
                });

            });
      table_tindakan_rd.columns.adjust().draw();
    });

    $('button#saveDiagnosa').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_id").val();
        if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
            alert('Silahkan pilih Dokter terlebih dahulu');
            return false;
        }else{
            swal({
              text: "Apakah data yang dimasukan sudah benar ?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'tidak',
              confirmButtonText: 'Ya'
            }).then(function(){  
                
                $.ajax({
                    url: ci_baseurl + "fisiologianak/list_fisiologi_anak/do_create_diagnosa_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter_id=" +dokter,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            // $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                            // $('#card_message').html(data.messages);
                            // $('#modal_notif').show();
                            // $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            //     $("#modal_notif").hide();
                            // });
                            swal( 
                                'Berhasil!',
                                'Data Berhasil Ditambahkan.', 
                                'success'
                              ) 
                            $('select').prop('selectedIndex',0);
                            $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                            table_diagnosa_rj.columns.adjust().draw();
                        }else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                            
                        }
                    }
                });

            });

        }
    });
    
    $('button#saveResep').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_id").val();
        let dataRacikan = "";
        if ($('#checkbox_racikan').is(':checked')){
            dataRacikan = JSON.stringify(resepRacikanArr)
        }
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
            }).then(function(){
                console.log("Jalan");
                $.ajax({
                    url: ci_baseurl + "fisiologianak/list_fisiologi_anak/do_create_resep_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateResepPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter_id=" +dokter + "&dataRacikan=" + dataRacikan,
                    success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            resepRacikanArr = [];
                            updateDataResepRacikan()
                            $('#checkbox_racikan').prop('checked', false);
                            $('#fmCreateResepPasien').find("input[type=text]").val("");
                            $('#fmCreateResepPasien').find("input[type=number]").val("");
                            $('#fmCreateResepPasien').find("select").prop('selectedIndex',0);
                        table_resep_rd.columns.adjust().draw();
                        swal( 
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success' 
                            ) 
                        } else {
                            $('#modal_notif3').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message3').html(data.messages);
                            $('#modal_notif3').show();
                            $("#modal_notif3").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif3").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //         'Gagal!',
                        //         'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //         'error'
                        //     )
                        }
                    }
                });
        });
    });

    $('button#saveTindakan').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_id").val();
        kelas_pelayanan =$("#kelaspelayanan_id").val();
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "fisiologianak/list_fisiologi_anak/do_create_tindakan_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter_id=" +dokter+ "&kelas_pelayanan=" +kelas_pelayanan,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $(".select2").val(null).trigger("change");
                            $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                            $('#fmCreateTindakanPasien').find("input[type=number]").val("");
                            $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',0);
                        table_tindakan_rd.columns.adjust().draw();
                        swal( 
                              'Berhasil!',
                              'Data Berhasil Disimpan.',
                              'success' 
                            )
                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                'error'
                            )
                            }
                    }
                });
           
          });
    });
});


function changeDataResepRacikan() {
    let 
        obat = $("#obat_racikan").val(),
        jumlah = $("#jumlah_racikan").val(),
        sediaan = $("#sediaan_racikan").val();
    
    if (obat == "" || jumlah == "" || sediaan == "" || jumlah <= 0) {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
            $("#modal_notif4").hide();
        });
    }
    else {
        resepRacikanArr.push({
            obat: {
                id: obat,
                name: $("#obat_racikan option:selected").text()
            },
            jumlah: jumlah,
            sediaan: {
                id: sediaan,
                name: $("#sediaan_racikan option:selected").text()
            }
        });
        updateDataResepRacikan();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}

function updateDataResepRacikan() {
    let i, 
    tableRacikan = document.getElementById("table_racikan_pasienrd"),
    tr, td;
    tableRacikan.innerHTML = "";
    if (resepRacikanArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "5");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tableRacikan.appendChild(tr);
        return;
    }
    for (i = 0; i < resepRacikanArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = resepRacikanArr[i].obat.name;
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = resepRacikanArr[i].jumlah;
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = resepRacikanArr[i].sediaan.name;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataResepRacikan("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tableRacikan.appendChild(tr);
    }
}

function removeDataResepRacikan(position) {
    resepRacikanArr.splice(position, 1)
    updateDataResepRacikan()
}

function getTindakan(){
    var kelaspelayanan_id = $("#kelaspelayanan_id").val();
    $.ajax({
        url: ci_baseurl + "fisiologianak/list_fisiologi_anak/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    });
}

function getTarifTindakan(){
    var daftartindakan_id = $("#tindakan").val();
    $.ajax({
        url: ci_baseurl + "fisiologianak/list_fisiologi_anak/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {daftartindakan_id:daftartindakan_id},
        success: function(data) {
            $("#harga_tindakan").val(data.list.harga_tindakan);
            $("#harga_cyto").val(data.list.cyto_tindakan);
        }
    });
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();
    
    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $("#subtotal").val(subtotal);
    $("#totalharga").val(total);
}

function hapusBackground(alergi_id){
    var jsonVariable = {};
    jsonVariable["alergi_id"] = alergi_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+"_deldiag").val();
    if(alergi_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "fisiologianak/list_fisiologi_anak/do_delete_background",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.notmodal_notifif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                            });
                            table_background_pha.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success' 
                                ) 
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                        }
                    }
            });
        });
    }
}

function hapusDiagnosa(diagnosapasien_id){
    var jsonVariable = {};
    jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
    if(diagnosapasien_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
            }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "fisiologianak/list_fisiologi_anak/do_delete_diagnosa",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                                table_diagnosa_rj.columns.adjust().draw();

                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.',
                                    'success' 
                                    ) 
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            }
                        }
                });
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}
function hapusTindakan(tindakanpasien_id){
    var jsonVariable = {};
    jsonVariable["tindakanpasien_id"] = tindakanpasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    if(tindakanpasien_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "isolasigizi/pasien_isolasi_gizi/do_delete_tindakan",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_tindakan_rd.columns.adjust().draw();
                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.',
                                    'success' 
                                    ) 
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            }
                        }
                });
        
          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}
function hapusResep(reseptur_id){
    var jsonVariable = {};
    jsonVariable["reseptur_id"] = reseptur_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delres').val();
    if(reseptur_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
          }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rawatdarurat/pasien_rd/do_delete_resep",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_resep_rd.columns.adjust().draw();
                        swal( 
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success' 
                            ) 
                    } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                                swal(
                                    'Gagal!',
                                    'Resep Gagal Terhapus !',
                                    'error'
                                    )
                            }
                        }
                });
          
          });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function dialogObat(){
	// $('#modal_list_obat').openModal('toggle');
	if(table_obat_rd){
        table_obat_rd.destroy();
    }
    table_obat_rd = $('#table_list_obat').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": true, //Feature control DataTables' server-side processing mode.
//        "paging": false, 

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rawatdarurat/pasien_rd/ajax_list_obat",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false //set not orderable
        }
        ] 
    });
    table_obat_rd.columns.adjust().draw();
}

function pilihObat(kode_barang){
    if (kode_barang != ""){
		$.ajax({
			url: ci_baseurl + "rawatdarurat/pasien_rd/ajax_get_obat_by_id",
			type: 'get',
			dataType: 'json',
            data: { kode_barang: kode_barang },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
					$('#obat_id').val(data.data['kode_barang']);
					$('#nama_obat').val(data.data['nama_barang']);
					$('#satuan_obat').val(data.data['id_sediaan']);
					$('#harga_jual').val(data.data['harga_satuan']);
					$('#modal_reseptur').modal('hide');
				} else {
					$("#harga_netto").val('');
					$("#current_stok").val('');
					$("#obat_id").val('');
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}


function pilihRSRujukan(oid){
    
      if (oid != ""){
    
        $.ajax({
          url: ci_baseurl + "rekam_medis/pasienrj/ajax_get_rs_rujukan_by_id",
          type: 'get',
          dataType: 'json',
          data: { rs_rujukan_id:oid },
          success: function(data) {
            var ret = data.success;
    
            if (ret == true){
              
              $('#rs_rujukan_id').val(data.data['rs_rujukan_id']);
              $('#kode_rs').val(data.data['kode_rs']);
              $('#nama_rs').val(data.data['nama_rs']);
              $('#alamat').val(data.data['alamat']);
              $('#lbl_kode_rs').addClass('active');
              $('#lbl_nama_rs').addClass('active');
              $('#lbl_alamat').addClass('active');
              $("#modal_rs_rujukan").modal('hide');
            } else {
    
              // $("#harga_netto").val('');
              // $("#current_stok").val('');
              // $("#obat_id").val('');
            }
          }
        });
      } else {
        // $('#modal_diagnosaa').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html("RS Rujukan ID Kosong");
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
                $(".modal_notif").hide();
            });
      }
    }

function is_rujuk(){
    var carapulang = $("#status_pulang").val();
    if(carapulang == 'Rujuk'){
        $('#rujukan_rs').removeAttr('style');
    }else{
        $('#rujukan_rs').attr('style','display:none;');
    }
}

function changeDataTheraphy() {
    let 
        id_nama_paket = $("#nama_program").val();
    if (id_nama_paket == "") {
        $("#modal_notif4").show();
        $('#modal_notif4').removeClass('alert-success').addClass('alert-danger');
        $('#card_message4').html("Harap isi atau periksa seluruh form");
        $("#modal_notif4").fadeTo(2000, 500).slideUp(500, function(){
        $("#modal_notif4").hide();
        });
    }
    else {
        theraphyArr.push({
            id_nama_paket :id_nama_paket,
            //kiri table kana values table
        });
        updateDataTheraphy();
    }
    $("#obat_racikan").prop('selectedIndex',0);
    $("#jumlah_racikan").val(0)
    $("#sediaan_racikan").prop('selectedIndex',0);
}


function updateDataTheraphy() {
    let i, 
    tablePaket = document.getElementById("table_therapy_fisiologianak"),
    tr, td;
    tablePaket.innerHTML = "";
    if (theraphyArr.length == 0) {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.setAttribute("colspan", "3");
        td.textContent = "No Data To Display"
        tr.appendChild(td);
        tablePaket.appendChild(tr);
        return;
    }
    for (i = 0; i < theraphyArr.length; i++) {
        tr = document.createElement("tr");

        td = document.createElement("td");
        td.textContent = (i + 1);
        tr.appendChild(td);

        td = document.createElement("td");
        td.textContent = theraphyArr[i].id_nama_paket;
        tr.appendChild(td);

        td = document.createElement("td");
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger btn-circle");
        button.setAttribute("onclick", "removeDataTheraphy("+ i +")")
        span = document.createElement("span");
        span.setAttribute("class", "fa fa-times")
        button.appendChild(span)
        td.appendChild(button)
        tr.appendChild(td);

        tablePaket.appendChild(tr);
    }
}

function removeDataTheraphy(position) {
    theraphyArr.splice(position, 1)
    updateDataTheraphy()
}

function pilihObat(kode_barang){
    if (kode_barang != ""){
		$.ajax({
			url: ci_baseurl + "fisiologianak/list_fisiologi_anak/ajax_get_obat_by_id",
			type: 'get',
			dataType: 'json',
            data: { kode_barang: kode_barang },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
					$('#obat_id').val(data.data['kode_barang']);
					$('#nama_obat').val(data.data['nama_barang']);
					$('#satuan_obat').val(data.data['id_sediaan']);
					$('#harga_jual').val(data.data['harga_satuan']);
					$('#modal_reseptur').modal('hide');
				} else {
					$("#harga_netto").val('');
					$("#current_stok").val('');
					$("#obat_id").val('');
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}

function dialogPilihKodeDiagnosa(){
      table_list_diagnosa = $('#table_list_diagnosaa').DataTable({
          "processing": true, //Feature control the processing indicator.
          "serverSide": true,
          "searching": true, //Feature control DataTables' server-side processing mode.
          "ajax": {
              "url": ci_baseurl + "fisiologianak/list_fisiologi_anak/ajax_list_diagnosa",
              "type": "GET"
          },
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false //set not orderable
          }
          ]
      });
      table_list_diagnosa.columns.adjust().draw();
  }
  
  function dialogPilihKodeDiagnosaICD10(){
      // $('#modal_list_obat').openModal('toggle');
      if(table_list_diagnosa_icd10_rd){
          table_list_diagnosa_icd10_rd.destroy();
      }
      table_list_diagnosa_icd10_rd = $('#table_list_diagnosa_icd10_rd').DataTable({
          "processing": true, //Feature control the processing indicator.
          "serverSide": true,
          "searching": true, //Feature control DataTables' server-side processing mode.
          "ajax": {
              "url": ci_baseurl + "fisiologianak/list_fisiologi_anak/ajax_list_diagnosa_icd10",
              "type": "GET"
          },
          "columnDefs": [
          {
              "targets": [ -1 ], //last column
              "orderable": false //set not orderable
          }
          ]
      });
      table_list_diagnosa_icd10_rd.columns.adjust().draw();
  }
  
  function pilihListDiagnosa(diagnosa2_id){
    if (diagnosa2_id != ""){
		    $.ajax({
			url: ci_baseurl + "fisiologianak/list_fisiologi_anak/ajax_get_diagnosa_by_id",
			type: 'get',
			dataType: 'json',
            data: { diagnosa2_id: diagnosa2_id },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
          $('#diagnosa_id').val(data.data['diagnosa2_id']);
					$('#kode_diagnosa').val(data.data['diagnosa_kode']);
					$('#nama_diagnosa').val(data.data['diagnosa_nama']);
          $('#kode_diagnosa_icd10').val(data.data['kode_diagnosa']);
          $('#nama_diagnosa_icd10').val(data.data['nama_diagnosa']);
					$('#modal_list_diagnosa').modal('hide');
				} else {
          $('#diagnosa_id').val('');
					$("#kode_diagnosa").val('');
					$("#nama_diagnosa").val('');
          $("#kode_diagnosa_icd10").val('');
          $("#nama_diagnosa_icd10").val('');
				}
			}
		});
  	} else {
  		// $('#modal_list_obat').closeModal('toggle');
  		$('.modal_notif').removeClass('green').addClass('red');
          $('#modal_card_message').html("Diagnosa ID Invalid");
          $('.modal_notif').show();
          $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
              $(".modal_notif").hide();
          });
  	}
  }
