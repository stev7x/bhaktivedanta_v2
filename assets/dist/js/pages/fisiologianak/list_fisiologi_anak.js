var table_list_fisiologi;
// $(document).ready(function(){
//    table_pasienrd = $('#table_list_pasienpha').DataTable({
//         "destroy": true,
//         "ordering": false,
//         "bFilter": false, 
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.
//         // "sScrollX": "100%",

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": ci_baseurl + "phatologi/list_fisiologi_anak/ajax_list_pasienrd",
            
//             "type": "GET",
//             "data": function(d){
//                 d.tgl_awal = $("#tgl_awal").val();
//                 d.tgl_akhir = $("#tgl_akhir").val();
//                 d.poliklinik = $("#poliklinik").val();
//                 d.status = $("#status").val();
//                 d.nama_pasien = $("#nama_pasien").val();
//                 d.dokter_id = $("#dokter_pj").val();
//             } 
   
//         }, 
//         //Set column definition initialisation properties.
//     });

//     table_pasienrd.columns.adjust().draw();
// });

function cariPasien(){
    table_list_fisiologi = $('#table_list_fisiologi').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "fisiologianak/list_fisiologi_anak/ajax_list_pasien_fisio",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.pasien_alamat = $("#pasien_alamat").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_list_fisiologi.columns.adjust().draw();
}

function cobaCoba(pendaftaran_id){   
    var src = ci_baseurl + "fisiologianak/list_fisiologi_anak/periksa_pasienrd/"+pendaftaran_id;
    $("#modal_periksa_pasienrd iframe").attr({
        'src': src,
        'height': 380,
        'width': "100%",
        'allowfullscreen':''
    });
}