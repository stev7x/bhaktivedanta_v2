var table_pasienrj;
var table_rs_rujukan;
$(document).ready(function(){
    // $('#modal_periksa_pasienrj').on('hidden.bs.modal', function(){
    //     $(this).find('iframe').html("");
    //     $(this).find('iframe').attr("src", "");
    // });

   table_pasienrj = $('#table_list_pasienrj').DataTable({
//      "sDom": '<"top"l>rt<"bottom"ip><"clear">',
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',// "destroy" : true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "filter" : false,
        
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_pasienrj",
          "type": "GET",
          "data": function(d){  
            d.tgl_awal = $("#tgl_awal").val();
            d.tgl_akhir = $("#tgl_akhir").val();
            d.poliklinik = $("#poliklinik").val();
            d.status = $("#status").val();
            d.nama_pasien = $("#nama_pasien").val();
            d.nama_dokter = $("#nama_dokter").val();
            d.nama_klinik = $("#nama_klinik").val();
            d.dokter_id = $("#dokter_pj").val();
            d.no_rekam_medis = $("#no_rekam_medis").val();
            d.pasien_alamat = $("#pasien_alamat").val();
            d.no_bpjs = $("#no_bpjs").val();
            d.no_pendaftaran = $("#no_pendaftaran").val();
            }
      },
      "columnDefs": [
      {
        "targets": [ -1,0,12 ], //last column
        "orderable": false //set not orderable
      }
      ]
      //Set column definition initialisation properties.

    });
    table_pasienrj.columns.adjust().draw();


    $('button#saveStatusPulang').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
          swal({
              text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText:'Tidak',
              confirmButtonText: 'Ya!'
          }).then(function(){
              $.ajax({
                      url: ci_baseurl + "rekam_medis/pasienrj/set_status_pulang",
                      type: 'POST',
                      dataType: 'JSON',
                      data: $('form#fmCreateStatusPulang').serialize()+ "&pendaftaran_id=" +pendaftaran_id,
                      success: function(data) {
                          var ret = data.success;
                            console.log("masuk sukses");
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                          if(ret === true) {
                            console.log("masuk sukses lagi");
                              $("#modal_keterangan_keluar").modal('hide').removeClass('error');
                              // $("#nama_provinsi").clear();
                              // $(".bs-example-modal-lg ").hide();
  
  
  
                              // $("#modal_notif").removeClass('hide').fadeTo(2000, 500).slideUp(500, function(){
                              //     $("#modal_notif").hide();
                              // });
                              // $('.modal_notif').removeClass('red').addClass('green');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $('#fmCreateProvinsiOperasi').find("input[type=text]").val("");
                              // $('#fmCreateProvinsiOperasi').find("select").prop('selectedIndex',0);
                              table_pasienrj.columns.adjust().draw();
                              swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                              swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                'error'
                              )
                          }
                      }
                  });
            
          })
      });
});

function reloadTablePasien(){
    table_pasienrj.columns.adjust().draw();
}

// function cariPasien(){
//     table_pasienrj = $('#table_list_pasienrj').DataTable({
//         "destroy": true,
//         "ordering": false,
//         "bFilter": false,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.
//         "sScrollX": "100%",

//       // Load data for the table's content from an Ajax source
//       "ajax": {
//           "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_pasienrj",
//           "type": "GET",
//           "data": function(d){
//                 d.tgl_awal = $("#tgl_awal").val();
//                 d.tgl_akhir = $("#tgl_akhir").val();
//                 d.poliklinik = $("#poliklinik").val();
//                 d.status = $("#status").val();
//                 d.nama_pasien = $("#nama_pasien").val();
//                 d.dokter_id = $("#dokter_pj").val();
//             }
//       }
//     });
//     table_pasienrj.columns.adjust().draw();
// }

function periksaPasienRj(pendaftaran_id){
    // $('#modal_periksa_pasienrj').openModal('toggle');
    var src = ci_baseurl + "rekam_medis/pasienrj/periksa_pasienrj/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_diagnosa iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function periksaPasienRjSudahPulang(pendaftaran_id){
    // $('#modal_periksa_pasienrj').openModal('toggle');
    
    var src = ci_baseurl + "rekam_medis/pasienrj/periksa_pasienrj_sudahpulang/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    
    $("#modal_diagnosa_sudahpulang iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function cariPasien(){
    table_pasienrj = $('#table_list_pasienrj').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_pasienrj",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.nama_klinik = $("#nama_klinik").val();
                d.dokter_id = $("#dokter_pj").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_pasienrj.columns.adjust().draw();
}



// function pulangkanPasienRj(pendaftaran_id){
//     // $('#modal_status_pulang').openModal('toggle');
//     $('#pendaftaran_id').val(pendaftaran_id);
// }


function pulangkanPasienRj(pendaftaran_id){
    // $('#modal_periksa_pasienrj').openModal('toggle');
    
    var src = ci_baseurl + "rekam_medis/pasienrj/keterangan_keluar/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $('#pendaftaran_id').val(pendaftaran_id);
    $("#modal_keterangan_keluar iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

$(document).ready(function(){
    $('#cari_pasien').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});

