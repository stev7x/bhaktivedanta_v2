var 
    table_riwayat_rawatjalan,
    table_riwayat_igd,
    table_riwayat_ri;
$(document).ready(function () {
    table_riwayat_rawatjalan = $('#table_riwayat_rawatjalan').DataTable({
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_riwayat_pasien_rj",
            "type": "GET",
            "data": function (d) {
                d.pasien_id = $("#pasien_id").val();
                // console.log(d.pasien_id);
            }
        },
    });
    table_riwayat_rawatjalan.columns.adjust().draw();

    $('button#saveDiagnosa').click(function () {
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasienrj/do_create_diagnosa_pasien",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateDiagnosaPasien').serialize() + "&pendaftaran_id=" + pendaftaran_id + "&pasien_id=" + pasien_id + "&dokter=" + dokter,

                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deldiag]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deltind]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delres]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);

                    if (ret === true) {
                        // $('#modal_notif2').removeClass('alert-danger').addClass('alert-success');
                        // $('#card_message2').html(data.messages);
                        // $('#modal_notif2').show();
                        // $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                        //     $("#modal_notif2").hide();
                        // });
                        $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                        $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex', 0);
                        table_diagnosa_rj.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                        //   swal(
                        //     'Gagal!',
                        //     'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //     'error'
                        //   )
                    }
                }
            });

        })
    });
});

function getRiwayatIGD() {
    table_riwayat_igd = $('#table_riwayat_igd').DataTable({
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_riwayat_pasien_igd",
            "type": "GET",
            "data": function (d) {
                d.pasien_id = $("#pasien_id").val();
                // console.log(d.pasien_id);
            }
        },
    });
    table_riwayat_igd.columns.adjust().draw();
}

function getRiwayatRI() {
    table_riwayat_ri = $('#table_riwayat_ri').DataTable({
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_riwayat_pasien_ri",
            "type": "GET",
            "data": function (d) {
                d.pasien_id = $("#pasien_id").val();
                // console.log(d.pasien_id);
            }
        },
    });
    table_riwayat_ri.columns.adjust().draw();
}

function detailDiagnosa(id) {
    var table_detail_diagnosa_rj = $('#table_detail_diagnosa_rj').DataTable({
        "destroy" :true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,
    
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_diagnosa_pasienrj",
            "type": "GET",
            "data": function (d) {
                d.pendaftaran_id = id;
                // console.log(d.pendaftaran_id);
            }
        },
    });
    table_detail_diagnosa_rj.columns.adjust().draw();
}

function detailTindakan(id) {
    var table_detail_tindakan_rj = $('#table_detail_tindakan_rj').DataTable({
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_tindakan_pasienrj",
            "type": "GET",
            "data": function (d) {
                d.pendaftaran_id = id;
                // console.log(d.pendaftaran_id);
            }
        },
    });
    table_detail_tindakan_rj.columns.adjust().draw();
}

function detailObat(id) {
    var table_detail_obat_rj = $('#table_detail_obat_rj').DataTable({
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_obat_pasienrj",
            "type": "GET",
            "data": function (d) {
                d.pendaftaran_id = id;
                // console.log(d.pendaftaran_id);
            }
        },
    });
    table_detail_obat_rj.columns.adjust().draw();
}

function detailAlergi(id) {
    var table_detail_alergi_rj = $('#table_detail_alergi_rj').DataTable({
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_alergi_pasienrj",
            "type": "GET",
            "data": function (d) {
                d.pendaftaran_id = id;
                // console.log(d.pendaftaran_id);
            }
        },
    });
    table_detail_alergi_rj.columns.adjust().draw();
}
