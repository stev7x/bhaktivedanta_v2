var table_pasienrd;
$(document).ready(function(){
   table_pasienrd = $('#table_list_pasienrd').DataTable({
        //      "sDom": '<"top"l>rt<"bottom"ip><"clear">',
        // "destroy": true,
        // "ordering": false,
        // "bFilter": false,
        // "processing": true, //Feature control the processing indicator.
        // "serverSide": true, //Feature control DataTables' server-side processing mode.
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "filter" : false,
        // "sScrollX": "100%",

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_pasienrd",
          "type": "GET",
         "data": function(d){
               d.tgl_awal = $("#tgl_awal").val();
               d.tgl_akhir = $("#tgl_akhir").val();
           }
      },
      //Set column definition initialisation properties.  
    });

    table_pasienrd.columns.adjust().draw();

    $('button#saveStatusPulang').click(function(){
        jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                    url: ci_baseurl + "rekam_medis/pasienrd/set_status_pulang",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmStatusPulang').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal2_notif').removeClass('red').addClass('green');
                            $('#modal2_card_message').html(data.messages);
                            $('.modal2_notif').show();
                            $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal2_notif").hide();
                            });
                        table_pasienrd.columns.adjust().draw();
                        } else {
                            $('.modal2_notif').removeClass('green').addClass('red');
                            $('#modal2_card_message').html(data.messages);
                            $('.modal2_notif').show();
                            $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal2_notif").hide();
                            });
                        }
                    }
                });
            }
        });
    });
 
    // var year = new Date().getFullYear();
    // $('.datepicker').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){ 
    //                this.close();
    //         }
    //     }
    // });
});
function reloadTablePasien(){
    table_pasienrd.columns.adjust().draw();   
}
function periksaPasienRd(pendaftaran_id){
    // $('#modal_periksa_pasienrd').openModal('toggle');
    var src = ci_baseurl + "rekam_medis/pasienrd/periksa_pasienrd/"+pendaftaran_id;
    $("#modal_diagnosa iframe").attr({
        'src': src,   
        'height': 960,
        'width': 1024, 
        'allowfullscreen':''
    });
}
function batalPeriksa(pendaftaran_id){
      var jsonVariable = {};
      jsonVariable["pendaftaran_id"] = pendaftaran_id;
      jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
      if(pendaftaran_id){
          jConfirm("Are you sure, you want to cancel ?","Ok","Cancel", function(r){
              if(r){
                  $.ajax({
                  url: ci_baseurl + "rekam_medis/pasienrd/do_batal_periksa",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              $('.notif').removeClass('red').addClass('green');
                              $('#card_message').html(data.messages);
                              $('.notif').show();
                              $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".notif").hide();
                              });
                          table_pasienrd.columns.adjust().draw();
                          } else {
                              $('.notif').removeClass('green').addClass('red');
                              $('#card_message').html(data.messages);
                              $('.notif').show();
                              $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".notif").hide();
                              });
                          }
                      }
                  });
              }
          });
      }
      else
      {
          $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
          $('#notification_messages').html('Missing Access code ID');
          $('#notification_type').show();
      }
}

function rujukKeRI(pendaftaran_id, pasien_id, no_pendaftaran){
    $('#modal_kirim_rawatinap').openModal('toggle');
    $('#pendaftaran_id').val(pendaftaran_id);
    $('#pasien_id').val(pasien_id);
    $('#no_pendaftaran').val(no_pendaftaran);
}

function getKelasPoliruangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasienrd/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
}

function getKamar(){
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasienrd/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
}

function saveAdmisi(){
    jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
        if(r){
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasienrd/do_kirimke_rawatinap",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmKirimKeAdmisi').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        $('.modal_notif_kirim').removeClass('red').addClass('green');
                        $('#modal_card_message_kirim').html(data.messages);
                        $('.modal_notif_kirim').show();
                        $(".modal_notif_kirim").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif_kirim").hide();
                        });
//                        $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
//                        $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
                    table_pasienrd.columns.adjust().draw();
                    } else {
                        $('.modal_notif_kirim').removeClass('green').addClass('red');
                        $('#modal_card_message_kirim').html(data.messages);
                        $('.modal_notif_kirim').show();
                        $(".modal_notif_kirim").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif_kirim").hide();
                        });
                    }
                }
            });
        }
    });
}

function cariPasien(){
    table_pasienrd = $('#table_list_pasienrd').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_pasienrd",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.dokter_id = $("#dokter_pj").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_pasienrd.columns.adjust().draw();
}

function pulangkanPasienRd(pendaftaran_id){
    $('#modal_status_pulang').openModal('toggle');
    $('#pendaftaran_id_').val(pendaftaran_id);
}

function periksaPasienRdSudahPulang(pendaftaran_id){
    // $('#modal_periksa_pasienrj').openModal('toggle');
    
    var src = ci_baseurl + "rekam_medis/pasienrd/periksa_pasienrd_sudahpulang/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    
    $("#modal_diagnosa_sudahpulang iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function is_rujuk(){
    var status_pulang = $("#status_pulang").val();
    if(status_pulang == 'Rujuk'){
        $('#rujukan_rs').removeAttr('style');
    }else{
        $('#rujukan_rs').attr('style','display:none;');
    }
}

$(document).ready(function(){
    $('#cari_pasien').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});