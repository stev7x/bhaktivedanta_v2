var table_pasienri;
$(document).ready(function(){
    table_pasienri = $('#table_list_pasienri').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasien_operasi/ajax_list_pasienri",
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.dokter_id = $("#dokter_pj").val();
            }
  
        }, 
        //Set column definition initialisation properties.
    });
    table_pasienri.columns.adjust().draw();
    
    // var year = new Date().getFullYear();
    // $('.datepicker').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     } 
    // });
    
    $('button#saveToPenunjang').click(function(){
        jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                    url: ci_baseurl + "rekam_medis/pasien_operasi/kirim_ke_penunjang",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmKirimKePenunjang').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal2_notif').removeClass('red').addClass('green');
                            $('#modal2_card_message').html(data.messages);
                            $('.modal2_notif').show();
                            $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal2_notif").hide();
                            });
                            $('#fmKirimKePenunjang').find("input[type=text]").val("");
                            $('#fmKirimKePenunjang').find("select").prop('selectedIndex',"");
                        table_pasienrj.columns.adjust().draw();
                        } else {
                            $('.modal2_notif').removeClass('green').addClass('red');
                            $('#modal2_card_message').html(data.messages);
                            $('.modal2_notif').show();
                            $(".modal2_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal2_notif").hide();
                            });
                        }
                    }
                });
            }
        });
    });
});

function periksaPasienRi(pendaftaran_id){
    $('#modal_periksa_pasienri').modal('show');
    var src = ci_baseurl + "rekam_medis/pasien_operasi/periksa_pasienoperasi/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_periksa_pasienri iframe").attr({
        'src': src,
        'height': 400,
        'width': '100%',
        'allowfullscreen':''  
    });
}
function batalRawat(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
    if(pendaftaran_id){
        swal({
          text: "Apakah anda yakin ingin membatalkan periksa ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya' 
        }).then(function(){  
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasien_operasi/do_batal_periksa",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                success: function(data) {
                var ret = data.success;
                $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                if(ret === true) {
                    $('.notif').removeClass('red').addClass('green');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                table_pasienri.columns.adjust().draw(); 
                swal( 
                    'Berhasil!',
                    'Data Berhasil Dibatalkan.',
                    'success', 
                  )    
                } else {   
                    $('.notif').removeClass('green').addClass('red');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                        $(".notif").hide();
                    });
                    swal( 
                        'Gagal!',
                        'Data Gagal Dibatalkan.',
                        'error', 
                      )    
                }
                }
            });
          
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}

function pulangkanPasienRi(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
    if(pendaftaran_id){
        jConfirm("Yakin akan memulangkan pasien ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                    url: ci_baseurl + "rekam_medis/pasien_operasi/do_pulangkan_pasien",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                        table_pasienri.columns.adjust().draw();
                        } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                        }
                    }
                });
            }
        });
    }
    else
    {
        $('.notif').removeClass('green').addClass('red');
        $('#card_message').html('Missing pendaftaran ID');
        $('.notif').show();
        $(".notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".notif").hide();
        });
    }
}

function pindahKamar(pendaftaran_id){
    // $('#modal_pindah_kamar').openModal('toggle');
    $('#pendaftaran_id').val(pendaftaran_id);
}

function getKelasPoliruangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasien_operasi/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
    getDokterRuangan();
}

function getDokterRuangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasien_operasi/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#dokter_pindah").html(data.list);
        }
    });
}

function getKamar(){
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasien_operasi/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
}

function savePindahKamar(){
    jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
        if(r){
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasien_operasi/do_pindah_kamar",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmPindahKamar').serialize(),
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        $('.modal_notif_kirim').removeClass('red').addClass('green');
                        $('#modal_card_message_kirim').html(data.messages);
                        $('.modal_notif_kirim').show();
                        $(".modal_notif_kirim").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif_kirim").hide();
                        });
//                        $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
//                        $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
                    table_pasienri.columns.adjust().draw();
                    } else {
                        $('.modal_notif_kirim').removeClass('green').addClass('red');
                        $('#modal_card_message_kirim').html(data.messages);
                        $('.modal_notif_kirim').show();
                        $(".modal_notif_kirim").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif_kirim").hide();
                        });
                    }
                }
            });
        }
    });
}


function cariPasien(){
    table_pasienrj = $('#table_list_pasienri').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%", 

      // Load data for the table's content from an Ajax source
      "ajax": {         
          "url": ci_baseurl + "rekam_medis/pasien_operasi/ajax_list_pasienri",   
          "type": "GET", 
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.nama_klinik = $("#nama_klinik").val();
                d.dokter_id = $("#dokter_pj").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
                d.lokasi = $("#lokasi").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_pasienrj.columns.adjust().draw();
}

function reloadTablePasien(){
    table_pasienri.columns.adjust().draw();
}

function kirimKePenunjang(pendaftaran_id){
    // $('#modal_kirim_penunjang').openModal('toggle');
    $('#pendaftaran_id_p').val(pendaftaran_id);
}

$(document).ready(function(){
    $('#cari_pasien').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);    
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});