var table_diagnosa_rj;
var table_tindakan_rj;
var table_daftar_diagnosa_rj;
var table_diagnosa_rj_sudahpulang;
var table_rs_rujukan;
var table_icd9;
$(document).ready(function(){
    table_diagnosa_rj = $('#table_diagnosa_pasienrj').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "processing": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_diagnosa_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
    });
    table_diagnosa_rj.columns.adjust().draw();


    table_icd9 = $('#table_icd9').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "processing": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_icd9",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
    });
    table_icd9.columns.adjust().draw();


    // table_diagnosa_rj_sudahpulang = $('#table_diagnosa_pasienrj_sudahpulang').DataTable({
    //     "processing": true, //Feature control the processing indicator.
    //     "serverSide": true, //Feature control DataTables' server-side processing mode.
    //     //"scrollX": true,
    //     "processing": true,
    //     "paging": false,
    //     "sDom": '<"top"l>rt<"bottom"p><"clear">',
    //     "ordering": false,
  
    //     // Load data for the table's content from an Ajax source
    //     "ajax": {
    //         "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_diagnosa_pasien_sudahpulang",
    //         "type": "GET",
    //         "data" : function(d){
    //             d.pendaftaran_id = $("#pendaftaran_id").val();
    //         }
    //     },
    //   });
    //   table_diagnosa_rj_sudahpulang.columns.adjust().draw();


    table_tindakan_rj = $('#table_tindakan_pasienrj').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rj.columns.adjust().draw();

    table_resep_rj = $('#table_resep_pasienrj').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_resep_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_resep_rj.columns.adjust().draw();

    table_rs_rujukan = $('#table_rs_rujukan').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
       "searching": true,
  
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_rs_rujukan",
            "type": "GET",
            
            
  
        },
        "columnDefs": [{
              "targets": [ -1 ],
              "orderable": false,
          },],
      });
      table_rs_rujukan.columns.adjust().draw();




      

     $('button#saveDiagnosa').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "rekam_medis/pasienrj/do_create_diagnosa_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                       
                        if(ret === true) {
                            // $('#modal_notif2').removeClass('alert-danger').addClass('alert-success');
                            // $('#card_message2').html(data.messages);
                            // $('#modal_notif2').show();
                            // $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            //     $("#modal_notif2").hide();
                            // });
                            $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                            $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
                        table_diagnosa_rj.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                          )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        //   swal(
                        //     'Gagal!',
                        //     'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //     'error'
                        //   )
                        }
                    }
                });
          
        })
    });

    // $('button#saveDiagnosa').click(function(){
    //     pendaftaran_id = $("#pendaftaran_id").val();
    //     pasien_id = $("#pasien_id").val();
    //     dokter = $("#dokter_periksa").val();
    //     //console.log(dokter);
    //     if(dokter == '' || dokter == null || dokter=='0' || dokter=='0#'){
    //         alert('Silahkan pilih Dokter terlebih dahulu');
    //         return false;
    //     }else{
    //         jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
    //             if(r){
    //                 $.ajax({
    //                     url: ci_baseurl + "rekam_medis/pasienrj/do_create_diagnosa_pasien",
    //                     type: 'POST',
    //                     dataType: 'JSON',
    //                     data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
    //                     success: function(data) {
    //                         var ret = data.success;
    //                         $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
    //                         $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
    //                         $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
    //                         $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
    //                         $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
    //                         if(ret === true) {
    //                             $('.modal_notif').removeClass('red').addClass('green');
    //                             $('#modal_card_message').html(data.messages);
    //                             $('.modal_notif').show();
    //                             $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                                 $(".modal_notif").hide();
    //                             });
    //                             $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
    //                             $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
    //                         table_diagnosa_rj.columns.adjust().draw();
    //                         } else {
    //                             $('.modal_notif').removeClass('green').addClass('red');
    //                             $('#modal_card_message').html(data.messages);
    //                             $('.modal_notif').show();
    //                             $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                                 $(".modal_notif").hide();
    //                             });
    //                         }
    //                     }
    //                 });
    //             }
    //         });
    //     }
    // });





});

// $('button#hapusDiagnosa').onclick(function(diagnosapasien_id){
function hapusDiagnosa(diagnosapasien_id,table){
  var jsonVariable = {};
  jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
  jsonVariable["table"] = table;
  jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
  swal({
          text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
          $.ajax({

                url: ci_baseurl + "rekam_medis/pasienrj/do_delete_diagnosa",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                    success: function(data) {
                        var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.notmodal_notifif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide().removeClass('error');
                            });

                            if (table == "t_tindakanpasien_icd9") {
                              table_icd9.columns.adjust().draw();
                            }else{
                              table_diagnosa_rj.columns.adjust().draw();
                            }

                        swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                          )
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error',
                              )
                        }
                    }
                });
         
        });
      }

      function dialogDiagnosa(icd){
        // $('#modal_list_obat').openModal('toggle');
        if(table_daftar_diagnosa_rj){
            table_daftar_diagnosa_rj.destroy();
        }
        table_daftar_diagnosa_rj = $('#table_diagnosa').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            //"scrollX": true,
           "searching": true,
      
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_diagnosa/"+icd,
                "type": "GET",
                "data" : function(d){
                  d.pendaftaran_id = $("#pendaftaran_id").val();
              }
      
            },
            "columnDefs": [{
                  "targets": [ -1 ],
                  "orderable": false,
              },],
          });
          table_daftar_diagnosa_rj.columns.adjust().draw();
      }

      function dialogDiagnosa10(){
        // $('#modal_list_obat').openModal('toggle');
        if(table_daftar_diagnosa_rj){
            table_daftar_diagnosa_rj.destroy();
        }
        table_daftar_diagnosa_rj = $('#table_diagnosa10').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            //"scrollX": true,
           "searching": true,
      
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": ci_baseurl + "rekam_medis/pasienrj/ajax_list_diagnosa10",
                "type": "GET",
                "data" : function(d){
                  d.pendaftaran_id = $("#pendaftaran_id").val();
              }
      
            },
            "columnDefs": [{
                  "targets": [ -1 ],
                  "orderable": false,
              },],
          });
          table_daftar_diagnosa_rj.columns.adjust().draw();
      }

function pilihDiagnosa10(diagnosa2_id) {
    if (diagnosa2_id != "") {
        $.ajax({
            url: ci_baseurl + "rekam_medis/pasienrj/ajax_get_diagnosa_by_id10",
            type: 'get',
            dataType: 'json',
            data: { diagnosa2_id: diagnosa2_id },
            success: function (data) {
                var ret = data.success;
                if (ret == true) {
                    $('#kode_diagnosa').val(data.data['diagnosa_kode']);
                    $('#nama_diagnosa').val(data.data['diagnosa_nama']);
                    $('#kode_diagnosa_icd10').val(data.data['kode_diagnosa']);
                    $('#nama_diagnosa_icd10').val(data.data['nama_diagnosa']);
                    // $('#obat_id').val(data.data['kode_barang']);
                    $("#modal_diagnosa10").modal('hide');
                } else {
                    $("#harga_netto").val('');
                    $("#current_stok").val('');
                    $("#obat_id").val('');
                }
            }
        });
    } else {
        // $('#modal_list_obat').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}


function pilihDiagnosa(oid,table){

  if (oid != ""){

    $.ajax({
      url: ci_baseurl + "rekam_medis/pasienrj/ajax_get_diagnosa_by_id/"+oid+"/"+table,
      type: 'get',
      dataType: 'json',
      // data: { diagnosa_id:oid  },
      success: function(data) {
        var ret = data.success;

        if (ret == true){

          if (table == "m_diagnosa_icd10") {

            // $('#diagnosa_id').val(data.data['diagnosa_id']);
            $('#kode_diagnosa').val(data.data['kode_diagnosa']);
            $('#nama_diagnosa').val(data.data['nama_diagnosa']);
            $('#lbl_kode_diagnosa').addClass('active');
            $('#lbl_nama-diagnosa').addClass('active');
            $("#modal_diagnosa").modal('hide');

          }else if (table == "m_icd_9_cm") {

            $('#kode_icd9').val(data.data['kode_diagnosa']);
            $('#nama_icd9').val(data.data['nama_diagnosa']);
            $("#modal_diagnosa").modal('hide');


          }

        } else {

          // $("#harga_netto").val('');
          // $("#current_stok").val('');
          // $("#obat_id").val('');
        }
      }
    });
  } else {
    // $('#modal_diagnosaa').closeModal('toggle');
    $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Diagnosa ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
  }
}

function pilihRSRujukan(oid){
    
      if (oid != ""){
    
        $.ajax({
          url: ci_baseurl + "rekam_medis/pasienrj/ajax_get_rs_rujukan_by_id",
          type: 'get',
          dataType: 'json',
          data: { rs_rujukan_id:oid },
          success: function(data) {
            var ret = data.success;
    
            if (ret == true){
              
              $('#rs_rujukan_id').val(data.data['rs_rujukan_id']);
              $('#kode_rs').val(data.data['kode_rs']);
              $('#nama_rs').val(data.data['nama_rs']);
              $('#alamat').val(data.data['alamat']);
              $('#lbl_kode_rs').addClass('active');
              $('#lbl_nama_rs').addClass('active');
              $('#lbl_alamat').addClass('active');
              $("#modal_rs_rujukan").modal('hide');
            } else {
    
              // $("#harga_netto").val('');
              // $("#current_stok").val('');
              // $("#obat_id").val('');
            }
          }
        });
      } else {
        // $('#modal_diagnosaa').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html("RS Rujukan ID Kosong");
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
                $(".modal_notif").hide();
            });
      }
    }

function is_rujuk(){
    var carapulang = $("#carapulang").val();
    if(carapulang == 'Rujuk'){
        $('#rujukan_rs').removeAttr('style');
    }else{
        $('#rujukan_rs').attr('style','display:none;');
    }
}

function saveICD9(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "rekam_medis/pasienrj/do_create_icd9",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateICD9').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id ,
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delICD9]').val(data.csrfHash);
                       
                        if(ret === true) {

                          console.log("sukses");
                            $('#fmCreateICD9').find("input[type=text]").val("");
                            $('#fmCreateICD9').find("select").prop('selectedIndex',0);
                            table_icd9.columns.adjust().draw();

                        // table_diagnosa_rj.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                          )
                        } else {
                          console.log("gagal");
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        //   swal(
                        //     'Gagal!',
                        //     'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                        //     'error'
                        //   )
                        }
                    }
                });
          
        });
    };