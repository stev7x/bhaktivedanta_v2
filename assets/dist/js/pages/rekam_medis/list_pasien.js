var table_list_pasien;
$(document).ready(function(){
    table_list_pasien = $('#table_list_pasien_list').DataTable({
      // "processing": true, //Feature control the processing indicator.
      // "serverSide": true,
      // "searching": false, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_list_pasien",
          "type": "GET",
      },
      //Set column definition initialisation properties.
      // "columnDefs": [
      // {
      //   "targets": [ -1,0 ], //last column
      //   "orderable": false //set not orderable
      // }
      // ]

    });
    // table_list_pasien.columns.adjust().draw();

    $('button#changeListPasien').click(function(){
        swal({
          text: 'Apakah data yang anda masukan sudah benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "rekam_medis/list_pasien/do_update_list_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdatelist_pasien').serialize(),

                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                        if(ret === true) {
                            $('select').prop('selectedIndex',0);
                            $('#fmUpdatelist_pasien').find("input[type=number]").val("");
                            $('#fmUpdatelist_pasien').find("input[type=text]").val("");
                            $('#fmUpdatelist_pasien').find("select").prop('selectedIndex',0);
                            table_list_pasien.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Diperbarui.',
                                'success'
                              );
                            $('#modal_update_list_pasien').modal('hide');


                        } else {
                            $('#modal_notif2').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message2').html(data.messages);
                            $('#modal_notif2').show();
                            $("#modal_notif2").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif2").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                          console.log("masuk else");
                        }
                    }
                });

        })
    });

    function getNORM2() {
        var val_pasien = $('#cari_no_rekam_medis').val();
        $.ajax({
            url: ci_baseurl + "rekam_medis/list_pasien/autocomplete_pasien",
            type: 'get',
            async: true,
            dataType: 'json',
            data: { val_pasien: val_pasien },
            success: function (data) { /* console.log(data); */
                var ret = data.success;
                if (ret == true) {
                    // console.log(data.data);
                    // console.log('masuk success');
                    $(".autocomplete").autocomplete({
                        max: 10,
                        scrollHeight: 250,
                        source: data.data,
                        open: function (event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 2");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index", "999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#type_id').val(ui.item.type_id);
                            $('#no_rm').attr("value", ui.item.no_rekam_medis);
                            $('#no_rm').val(ui.item.no_rekam_medis);
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                            .append("<a><b><font size=2>" + item.value + "</font></b></a>")
                            .appendTo(ul);
                    };
                } else {
                    // console.log('masuk else');
                    $("#id_m_pasien").val('0#');
                }
            }
        });
    }

    // $('#rm_awal').on('keydown',
    //     _.debounce(getNORM2, 1000));

    $("#no_rm_awal").keyup(function () {
        $.ajax({
            type: "POST",
            url: ci_baseurl + "rekam_medis/list_pasien/autocomplete_pasien",
            data: {
                keyword: $("#no_rm_awal").val()
            },
            dataType: "json",
            success: function (data) {
                var ret = data.success;
                if (ret == true) {
                    // console.log(data.data);
                    // console.log('masuk success');
                    // console.log(data.data);

                    $(".autocomplete").autocomplete({
                        max: 10,
                        scrollHeight: 250,
                        source: data.data,
                        open: function (event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 2");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index", "999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#type_id').val(ui.item.type_id);
                            $('#rm_awal').attr("value", ui.item.no_rekam_medis);
                            $('#rm_awal').val(ui.item.no_rekam_medis);
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                            .append("<a><b><font size=2>" + item.value + "</font></b></a>")
                            .appendTo(ul);
                    };
                } else {
                    // console.log('masuk else');
                    $("#id_m_pasien").val('0#');
                }
            }
        });
    });

    $("#no_rm_akhir").keyup(function () {
        $.ajax({
            type: "POST",
            url: ci_baseurl + "rekam_medis/list_pasien/autocomplete_pasien",
            data: {
                keyword: $("#no_rm_akhir").val()
            },
            dataType: "json",
            success: function (data) {
                var ret = data.success;
                if (ret == true) {
                    // console.log(data.data);
                    // console.log(data.data);
                    $(".autocomplete").autocomplete({
                        max: 10,
                        scrollHeight: 250,
                        source: data.data,
                        open: function (event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 2");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index", "999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#type_id').val(ui.item.type_id);
                            $('#rm_akhir').attr("value", ui.item.no_rekam_medis);
                            $('#rm_akhir').val(ui.item.no_rekam_medis);
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                            .append("<a><b><font size=2>" + item.value + "</font></b></a>")
                            .appendTo(ul);
                    };
                } else {
                    // console.log('masuk else');
                    $("#id_m_pasien").val('0#');
                }
            }
        });
    });




});


function periksaPasienRj(pendaftaran_id){
    // $('#modal_periksa_pasienrj').openModal('toggle');
    var src = ci_baseurl + "rawatjalan/pasien_rj/periksa_pasienrj/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_periksa_pasienrj iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

function editlist_pasien(pasien_id){
    if(pasien_id){
        $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/ajax_get_list_pasien_by_id",
        type: 'get',
        dataType: 'json',
        data: {pasien_id:pasien_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#modal_update_list_pasien').modal('show');

                    console.log(data.tgl_lahir);
                    $('#upd_type_id').val(data.data.type_identitas)
                    // $('#upd_type_id').material_select();
                    $('#upd_no_identitas').val(data.data.no_identitas);
                    $('#upd_no_identitas').addClass('active');
                    $('#upd_no_bpjs').val(data.data.no_bpjs);
                    $('#upd_no_bpjs').addClass('active');
                    $('#upd_no_rm').val(data.data.no_rekam_medis);
                    $('#label_upd_no_rm').addClass('active');
                    $('#upd_nama_pasien').val(data.data.pasien_nama);
                    $('#upd_nama_panggilan').val(data.data.nama_panggilan);
                    $('#label_upd_nama_pasien').addClass('active');
                    $('#upd_tempat_lahir').val(data.data.tempat_lahir);
                    $('#label_upd_tempat_lahir').addClass('active');
                    $('#upd_tgl_lahir').val(data.tgl_lahir);
                    $('#label_upd_tgl_lahir').addClass('active');

                    if(data.tgl_lahir != ""){
                        setUmur();
                    }

                    $('#propinsi').val(data.data.propinsi_id).select2();
                    $('#upd_propinsi_tinggal').val(data.data.provinsi_tinggal_id).select2();
                    // getDaerahAllTinggal(data.data.provinsi_tinggal_id,data.data.kabupaten_tinggal_id, data.data.kecamatan_tinggal_id, data.data.kelurahan_tinggal_id);

                    getDaerahAll(data.data.propinsi_id, data.data.kabupaten_id, data.data.kecamatan_id, data.data.kelurahan_id);
                    $('#upd_jenis_kelamin').val(data.data.jenis_kelamin);
                    $('#upd_status_kawin').val(data.data.status_kawin);
                    cekStatusKawin();
                    $('#upd_alamat').val(data.data.pasien_alamat);
                    $('#label_upd_alamat').addClass('active');
                    $('#upd_no_tlp_rmh').val(data.data.tlp_rumah);
                    $('#label_upd_no_tlp_rmh').addClass('active');
                    $('#upd_no_mobile').val(data.data.tlp_selular);
                    $('#label_upd_no_mobile').addClass('active');
                    $('#upd_pekerjaan').val(data.data.pekerjaan_id);
                    $('#upd_agama').val(data.data.agama_id);
                    $('#upd_bahasa').val(data.data.bahasa_id);
                    $('#upd_warga_negara').val(data.data.warga_negara);
                    $('#upd_pendidikan').val(data.data.pendidikan_id);
                    $('#upd_namakeluarga').val(data.data.nama_keluarga);
                    $('#label_upd_namakeluarga').addClass('active');
                    $('#upd_hubungankeluarga').val(data.data.status_hubungan);
                    $('#upd_nama_orangtua').val(data.data.nama_orangtua);
                    $('#upd_tgl_lahir_ortu').val(data.tgl_lahir_ortu);

                    if (data.tgl_lahir_ortu != "") {
                        setUmurOrtu();

                    }

                    // $('#upd_umur_ortu').val(data.data.umur_ortu);
                    $('#upd_catatan_khusus').val(data.data.catatan_khusus);
                    $('#upd_alamat_domisili').val(data.data.pasien_alamat_domisili);
                    $('#upd_pasien_id').val(data.data.pasien_id);
                    $('#upd_status_pasien').val(data.data.status);
                    $('#label_upd__nama_list_pasien').addClass('active');
                    $('#upd_nama_suami_istri').val(data.data.nama_suami_istri);
                    $('#upd_tgl_suami_istri').val(data.tgl_lahir_suami);
                    if(data.tgl_lahir_suami != ""){
                        setUmurSustri();
                    }
                    // $('#upd_umur_suami_istri').val(data.data.umur_suami_istri);
                    $('#upd_pasien_id').val(data.data.pasien_id);
                    $('#upd_status_pasien').val(data.data.status);

                    $('#upd_asuransi1').val(data.data_reservasi.asuransi1);
                    $('#upd_asuransi2').val(data.data_reservasi.asuransi2);
                    $('#upd_no_asuransi1').val(data.data_reservasi.no_asuransi1);
                    $('#upd_no_asuransi2').val(data.data_reservasi.no_asuransi2);

                    console.log(data.data_reservasi);

                    $('#upd_allergy_history').val(data.data.allergy_history);
                    $('#upd_pregnant').val(data.data.pregnant);
                    $('#upd_current_medication').val(data.data.current_medication);
                    $('#upd_weight').val(data.data.weight);
                    $('#upd_medical_history').val(data.data.medical_history);
                    $('#upd_medical_event').val(data.data.medical_event);
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}



function hapusListPasien(pasien_id){
    var jsonVariable = {};
        jsonVariable["pasien_id"] = pasien_id;
        jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({

                  url: ci_baseurl + "rekam_medis/list_pasien/do_delete_list_pasien",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);

                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            // table_list_pasien.columns.adjust().draw();
                            table_list_pasien.ajax.reload( null, false );
                            swal(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success',
                              )
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                              swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error',
                              )
                          }
                      }
                  });

          });
        }

function getDaerahAll(propinsi_id, kabupaten_id, kecamatan_id, kelurahan_id){
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/get_daerah_list",
        type: 'GET',
        dataType: 'JSON',
        data: {propinsi_id:propinsi_id, kabupaten_id:kabupaten_id, kecamatan_id:kecamatan_id, kelurahan_id:kelurahan_id},
        success: function(data) {
            // $("#kabupaten").html(data.list_kabupaten).trigger('change.select2');
            // $("#kecamatan").html(data.list_kecamatan).trigger('change.select2');
            // $("#kelurahan").html(data.list_kelurahan).trigger('change.select2');

            $("#kabupaten").html(data.list_kabupaten).select2();
            $("#kecamatan").html(data.list_kecamatan).select2();
            $("#kelurahan").html(data.list_kelurahan).select2();

        }
    });
}

function getDaerahAllTinggal(propinsi_id, kabupaten_id, kecamatan_id, kelurahan_id){
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/get_daerah_list",
        type: 'GET',
        dataType: 'JSON',
        data: {propinsi_id:propinsi_id, kabupaten_id:kabupaten_id, kecamatan_id:kecamatan_id, kelurahan_id:kelurahan_id},
        success: function(data) {
            $("#upd_kabupaten_tinggal").html(data.list_kabupaten).select2();
            // $("#upd_kabupaten_tinggal").select2();
            $("#upd_kecamatan_tinggal").html(data.list_kecamatan).select2();
            // $("#upd_kecamatan_tinggal").select2();
            $("#upd_kelurahan_tinggal").html(data.list_kelurahan).select2();
            // $("#upd_kelurahan_tinggal").select2();

        }
    });
}
function getKabupaten(id){
    if (id==null || id =='undifined' || id =='') {
        var propinsi_id = $("#propinsi").val();
    }else{
        var propinsi_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: {propinsi_id:propinsi_id},
        success: function(data) {
            $("#kabupaten").html(data.list);
            // $("#kabupaten").val(data.data.kabupaten_id);
            $('#kabupaten').select2();
            console.log("i am ready")
        }
    });
}
function getKabupatenTinggal(id){
    if (id==null || id =='undifined' || id =='') {
        var propinsi_id = $("#upd_propinsi_tinggal").val();
    }else{
        var propinsi_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: {propinsi_id:propinsi_id},
        success: function(data) {
            $("#upd_kabupaten_tinggal").html(data.list);
            // $("#kabupaten").val(data.data.kabupaten_id);
            $('#upd_kabupaten_tinggal').select2();
            console.log("i am ready")
        }
    });
}

function getKecamatan(id){
    if (id==null || id =='undifined' || id =='') {
        var kabupaten_id = $("#kabupaten").val();
    }else{
        var kabupaten_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/get_kecamatan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kabupaten_id:kabupaten_id},
        success: function(data) {
            $("#kecamatan").html(data.list);
            $('#kecamatan').select2();
        }
    });
}

function getKecamatanTinggal(id){
    if (id==null || id =='undifined' || id =='') {
        var kabupaten_id = $("#upd_kabupaten_tinggal").val();
    }else{
        var kabupaten_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/get_kecamatan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kabupaten_id:kabupaten_id},
        success: function(data) {
            $("#upd_kecamatan_tinggal").html(data.list);
            $('#upd_kecamatan_tinggal').select2();
        }
    });
}

function getKelurahan(id){
    if (id==null || id =='undifined' || id =='') {
        var kecamatan_id = $("#kecamatan").val();
    }else{
        var kecamatan_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/get_kelurahan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kecamatan_id:kecamatan_id},
        success: function(data) {
            $("#kelurahan").html(data.list);
            $('#kelurahan').select2();
        }
    });
}

function getKelurahanTinggal(id){
    if (id==null || id =='undifined' || id =='') {
        var kecamatan_id = $("#upd_kecamatan_tinggal").val();
    }else{
        var kecamatan_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/get_kelurahan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kecamatan_id:kecamatan_id},
        success: function(data) {
            $("#upd_kelurahan_tinggal").html(data.list);
            $('#upd_kelurahan_tinggal').select2();
        }
    });
}
function setUmur(){
    var tgl_lahir = $("#upd_tgl_lahir").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/hitung_umur",
        type: 'GET',
        dataType: 'JSON',
        data: {tgl_lahir:tgl_lahir},
        success: function(data) {
            $("#upd_umur").val(data.umur);
        }
    });
}

function setUmurSustri(){
    var tgl_lahir = $("#upd_tgl_suami_istri").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/list_pasien/hitung_umur",
        type: 'GET',
        dataType: 'JSON',
        data: { tgl_lahir: tgl_lahir},
        success: function(data) {
            console.log('masuk');
            $("#upd_umur_suami_istri").val(data.umur);
            $("#upd_umur_suami_istri").attr('readonly','true');
        }
    });
}

    function setUmurOrtu(){
        var tgl_lahir = $("#upd_tgl_lahir_ortu").val();
        $.ajax({
            url: ci_baseurl + "rekam_medis/list_pasien/hitung_umur",
            type: 'GET',
            dataType: 'JSON',
            data: {tgl_lahir:tgl_lahir},
            success: function(data) {
                console.log('masuk');
                $("#upd_umur_ortu").val(data.umur);
                $("#upd_umur_ortu").attr('readonly','true');

            }
        });
    }

// function setUmurOrtu(){
//     var tgl_lahir = $("#upd_tgl_lahir_ortu").val();
//     $.ajax({
//         url: ci_baseurl + "rekam_medis/list_pasien/hitung_umur",
//         type: 'GET',
//         dataType: 'JSON',
//         data: {tgl_lahir:tgl_lahir},
//         success: function(data) {
//             $("#upd_umur_ortu").val(data.umur);
//         }
//     });
// }
// function setUmurKary(){
//     var tgl_lahir = $("#tgl_lahir").val();
//     $.ajax({
//         url: ci_baseurl + "rekam_medis/list_pasien/hitung_umur",
//         type: 'GET',
//         dataType: 'JSON',
//         data: {tgl_lahir:tgl_lahir},
//         success: function(data) {
//             $("#umur").val(data.umur);
//         }
//     });
// }


function cekStatusKawin(){
    var type = $("#upd_status_kawin").val();
    var t = $('#Suami_istri');
    if(type == "Kawin"){
            console.log('masuk');
            t.removeClass('hide');

    }else{
        console.log('tidak');
        t.addClass('hide');
    }
}

var timer;
function cariPasien(){
    clearTimeout(timer);
    timer=setTimeout(
        function cariPasien2() {
            table_pasienri = $('#table_list_pasien_list').DataTable({
                "destroy": true,
                "ordering": false,
                "bFilter": false,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                // "sScrollX": "100%",

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": ci_baseurl + "rekam_medis/list_pasien/ajax_list_list_pasien",
                    "type": "GET",
                    "data": function (d) {

                        d.nama_pasien = $("#nama_pasien").val();
                        d.no_rekam_medis = $("#no_rekam_medis").val();
                        d.pasien_alamat = $("#pasien_alamat").val();
                        d.tlp_selular = $("#tlp_selular").val();
                        d.no_bpjs = $("#no_bpjs").val();
                        d.no_identitas = $("#no_identitas").val();

                    }

                },
                //Set column definition initialisation properties.
            });
            table_list_pasien.columns.adjust().draw();
        }
    ,1000);
}



$(document).ready(function(){
    $('#cari_pasien').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});

function savePasien() {
    swal({
          text: "Apakah data yang dimasukan sudah benar ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){
            console.log("masuk fungsi");
            $.ajax({
                url : ci_baseurl + "rekam_medis/list_pasien/do_create_pasien",
                type : 'POST',
                dataType : 'JSON',
                data : $('form#fmCreatePasien').serialize(),
                success : function(data){
                        console.log("sebelum sukses");

                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                    if (ret === true) {
                        console.log("sukses");
                        swal(
                            'Berhasil!',
                            'Data Berhasil Ditambahkan.',
                        );
                        $('#modal_karyawan').modal('hide');
                        $('input[type=text]').val("");
                        $('input[type=number]').val("");
                        $('textarea').val("");
                        $('select').prop('selectedIndex','0');
                        table_list_pasien.columns.adjust().draw();
                        $('#modal_karyawan').modal('hide');

                    }else{
                        console.log("data gagal ditambahkan");
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                        });
                    }
                }
            });
        });
}

function openModalKary(){
    $('#modal_karyawan').modal('show');
    $.ajax({
        url : ci_baseurl + "rekam_medis/list_pasien/get_no_rekam_medis",
        type : 'GET',
        dataType : 'JSON',
        data : $('form#fmCreatePasien').serialize(),
        success : function(data){
            console.log("hadir : " + data.no_rm);
            $('#no_rm').val(data.no_rm);

        }
    });
}

function form_print(pasien_id){
    $('#modal_print').modal('show');
    console.log("id_pasien : "+pasien_id);
    // $('button#printSurat').attr('onclick','printKartuBerobat('')');
    $('button#printKartu').attr('onclick','printData('+pasien_id+',"print_kartu")');
    $('button#printDetail').attr('onclick','printData('+pasien_id+',"print_data_pasien")');
    $('button#printCasemix').attr('onclick','printData('+pasien_id+',"print_casemix")');
    $('button#printSurat').attr('onclick','printData('+pasien_id+',"form_input_surat")');
    $('button#printKartuHilang').attr('onclick','printData('+pasien_id+',"print_hilang_kartu")');
    $('button#printSticker').attr('onclick','printData('+pasien_id+',"print_sticker")');
    $('button#printGelang').attr('onclick','printData('+pasien_id+',"print_gelang")');
    $('button#printResiko').attr('onclick', 'printData(' + pasien_id + ',"print_resiko")');
    $('button#printResiko2').attr('onclick', 'printData(' + pasien_id + ',"print_resiko2")');

}

// function printData(pasien_id,controller){
//     window.open(ci_baseurl+'rekam_medis/list_pasien/'+controller+'/'+pasien_id,'Print_Surat','top=100,left=320,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')

// }
function printData(pasien_id,view){
    window.open(ci_baseurl+'rekam_medis/list_pasien/print_data/'+pasien_id+'/'+view,'Print_Surat','top=100,left=320,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')

}


function exportAllPasien() {
    var rm_awal = $("#rm_awal").val();
    var rm_akhir = $("#rm_akhir").val();
    console.log(rm_awal);
    console.log(rm_akhir);

    window.location.href = ci_baseurl + "rekam_medis/list_pasien/exportAllPasien/" + rm_awal + "/" + rm_akhir ;
}


// function setUmur() {
//     var tgl_lahir = $("#upd_tgl_lahir").val();
//     $.ajax({
//         url: ci_baseurl + "rekam_medis/list_pasien/hitung_umur",
//         type: 'GET',
//         dataType: 'JSON',
//         data: { tgl_lahir: tgl_lahir },
//         success: function (data) {
//             console.log('masuk');
//             $("#upd_umur").val(data.umur);
//             $("#upd_umur").attr('readonly', 'true');
//         }
//     });
// }



function changeRM() {
    var no_rm_awal  = $("#no_rm_awal").val();
    var no_rm_akhir = $("#no_rm_akhir").val();
    $("#rm_awal").val(no_rm_awal);
    $("#rm_akhir").val(no_rm_akhir);
}


function riwayatPenyakitPasien(pasien_id) {
    var src = ci_baseurl + "rekam_medis/list_pasien/riwayatPenyakitPasien/" + pasien_id;
    console.log(pasien_id);
    $("#modal_riwayat_penyakit_pasien iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen': ''
    });
}

function saveRetensiPasien() {
    swal({
        text: "Apakah anda yakin mau diretension?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
      }).then(function(){
          console.log("yey")
      });
}