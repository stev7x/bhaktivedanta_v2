var table_pasienri;
$(document).ready(function(){
    table_pasienri = $('#table_list_pasienri').DataTable({
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienri/ajax_list_pasienri",
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.dokter_id = $("#dokter_pj").val();
            }
  
        },
        // "columnDefs": [
        //     {
        //       "targets": [ -1,0 ], //last column
        //       "orderable": false //set not orderable
        //     }
        //     ]
        //Set column definition initialisation properties.
    });
    table_pasienri.columns.adjust().draw();
     
    var year = new Date().getFullYear();
    // $('.datepicker').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 90, // Creates a dropdown of 10 years to control year
    //     format: 'dd mmmm yyyy',
    //     max: new Date(year,12),
    //     closeOnSelect: true,
    //     onSet: function (ele) {
    //         if(ele.select){
    //                this.close();
    //         }
    //     }
    // });   
     
    $('button#saveStatusPulang').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
          swal({
              text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText:'Tidak',
              confirmButtonText: 'Ya!'
          }).then(function(){
              $.ajax({
                      url: ci_baseurl + "rekam_medis/pasienrj/set_status_pulang",
                      type: 'POST',
                      dataType: 'JSON',
                      data: $('form#fmCreateStatusPulang').serialize()+ "&pendaftaran_id=" +pendaftaran_id,
                      success: function(data) {
                          var ret = data.success;
                            console.log("masuk sukses");
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                          if(ret === true) {
                            console.log("masuk sukses lagi");
                              $("#modal_keterangan_keluar").modal('hide').removeClass('error');
                              // $("#nama_provinsi").clear();
                              // $(".bs-example-modal-lg ").hide();
  
  
  
                              // $("#modal_notif").removeClass('hide').fadeTo(2000, 500).slideUp(500, function(){
                              //     $("#modal_notif").hide();
                              // });
                              // $('.modal_notif').removeClass('red').addClass('green');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $('#fmCreateProvinsiOperasi').find("input[type=text]").val("");
                              // $('#fmCreateProvinsiOperasi').find("select").prop('selectedIndex',0);
                              table_pasienri.columns.adjust().draw();
                              swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                              swal(
                                'Gagal!',
                                'Data Gagal Disimpan.',
                                'error'
                              )
                          }
                      }
                  });
            
          })
      });
});

function reloadTablePasien(){
    table_pasienri.columns.adjust().draw();
}

function periksaPasienRi(pendaftaran_id){
    // $('#modal_periksa_pasienri').openModal('toggle');
    var src = ci_baseurl + "rekam_medis/pasienri/periksa_pasienri/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_diagnosa iframe").attr({
        'src': src, 
        'height': 400,
        'width': '100%',
        'allowfullscreen':''
    });
}

function getKelasPoliruangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasienri/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#kelas_pelayanan").html(data.list);
        }
    });
    getDokterRuangan();
}

function getDokterRuangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasienri/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#dokter_pindah").html(data.list);
        }
    });
}

function cariPasien(){
    table_pasienri = $('#table_list_pasienri').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienri/ajax_list_pasienri",
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.nama_dokter = $("#nama_dokter").val();
                d.dokter_id = $("#dokter_pj").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.pasien_alamat = $("#pasien_alamat").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
                d.lokasi = $("#lokasi").val();
                
            }
  
        },
        //Set column definition initialisation properties.
    });
    table_pasienri.columns.adjust().draw();
}

function reloadTablePasien(){
    table_pasienri.columns.adjust().draw();
}

function setStatusPulang(pendaftaran_id){
    console.log(pendaftaran_id);
    $('#modal_status_pulang').openModal('toggle');
    $('#pendaftaran_id').val(pendaftaran_id);
}

function periksaPasienRi(pendaftaran_id){
    // $('#modal_periksa_pasienri').openModal('toggle');
    var src = ci_baseurl + "rekam_medis/pasienri/periksa_pasienri/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_diagnosa iframe").attr({
        'src': src, 
        'height': 400,
        'width': '100%',
        'allowfullscreen':''
    });
}

function periksaPasienRiSudahPulang(pendaftaran_id){
    // $('#modal_periksa_pasienrj').openModal('toggle');
    
    var src = ci_baseurl + "rekam_medis/pasienri/periksa_pasienri_sudahpulang/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    
    $("#modal_diagnosa_sudahpulang iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}


function pulangkanPasienRi(pendaftaran_id){
    // $('#modal_periksa_pasienrj').openModal('toggle');
    
    var src = ci_baseurl + "rekam_medis/pasienri/keterangan_keluar/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $('#pendaftaran_id').val(pendaftaran_id);
    $("#modal_keterangan_keluar iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen':''
    });
}

$(document).ready(function(){
    $('#cari_pasien').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});