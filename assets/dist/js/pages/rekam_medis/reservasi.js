$(document).ready(function () {
    console.log("js lol");

    get_pasien_reservasi();
    get_pasien_reservasi_batal();
    getDokter();

    // setInterval(function () {
    //     get_pasien_reservasi();
    //     get_pasien_reservasi_batal();
    // }, 5000);

    $('button#btnSaveReservasi').click(function () {
        swal({
            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/reservasi/do_create_reservasi",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateReservasi').serialize(),
                success: function (data) {
                    var ret = data.success;
                    var display = data.display;
                    // console.log(data.push);
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    if (ret === true) {
                        get_pasien_reservasi();
                        $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $('#jumlah_pasien').text(data.no_urut);
                        $('#kode_booking').text(data.no_booking);
                        $("html, body").animate({
                            scrollTop: 100
                        }, "fast");
                        swal(
                            'Berhasil !',
                            'Reservasi pasien berhasil ditambahkan',
                            'success'
                        );
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $("html, body").animate({
                            scrollTop: 100
                        }, "fast");
                    }
                }
            });
        });
    });

    $(document).ready(function () {
        $('#change_option').on('change', function (e) {
            var valueSelected = this.value;
            $('.pilih').attr('id', valueSelected);
            $('.pilih').attr('name', valueSelected);
        });
    });

    $(document).ready(function () {
        $('#change_option_batal').on('change', function (e) {
            var valueSelected = this.value;
            $('.pilihbatal').attr('id', valueSelected);
            $('.pilihbatal').attr('name', valueSelected);
        });
    });

    $("#cek_hotel").change(function () {
        if (this.checked) {
            $('#hotel').attr("disabled", true);
            $('#hotel_nama').removeAttr("disabled", true);
            // alert($('#cek_hotel').val());
        } else {
            $('#hotel').removeAttr("disabled", true);
            $('#hotel_nama').attr("disabled", true);
            $('#hotel_nama').val("");
            // alert("hurung hotel list");
        }
    });

    $("#cek_pasien").change(function () {
        if (this.checked) {
            $('#cari_no_rekam_medis').attr("disabled", true);
            $('#cari_no_rekam_medis').val("");
            $('#no_rm').val("1");
            $('#is_checked').val("1");
        } else {
            $('#cari_no_rekam_medis').removeAttr("disabled", true);
            $('#is_checked').val("0");
        }
    });

    $("#cari_no_rekam_medis").keyup(function () {
        $.ajax({
            url: ci_baseurl + "rekam_medis/reservasi/autocomplete_pasien",
            type: 'post',
            data: {
                keyword: $("#cari_no_rekam_medis").val()
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                var ret = data.success;
                if (ret == true) {
                    $(".autocomplete").autocomplete({
                        max: 10,
                        scrollHeight: 250,
                        source: data.data,
                        open: function (event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 2");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index", "999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#type_id').val(ui.item.type_id);
                            $('#no_rm').removeAttr("value");
                            $('#no_rm').attr("value", ui.item.no_rekam_medis);
                            $('#no_rm').val(ui.item.no_rekam_medis);
                            $('#pasien_nama').val(ui.item.pasien_nama);
                            $('#tanggal_lahir').val(ui.item.tanggal_lahir);
                            $('#jk').val(ui.item.jenis_kelamin);
                            $('#jk').select();
                            $('#alamat').val(ui.item.alamat);
                            $('#no_telp').val(ui.item.tlp_selular);

                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                            .append("<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.alamat + "</font></a>")
                            .appendTo(ul);
                    };
                } else {
                    $("#id_m_pasien").val('0#');
                }
            }
        });
    });
});

function reload() {
    location.reload();
}

function verify(id) {
    var src = ci_baseurl + "rekam_medis/reservasi/verifikasi_data/" + id;
    window.location = src;
}

function printDataPasien(pendaftaran_id) {
    window.open(ci_baseurl + 'rekam_medis/pendaftaran/print_pendaftaran/' + pendaftaran_id, 'Print_Surat', 'top=100,left=320,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

function get_pasien_reservasi() {
    var jsonVariable = {};
    jsonVariable["tgl_res_awal"] = $('#tgl_res_awal').val();
    // jsonVariable["poli"] = $('#poli').val();
    // jsonVariable["dokter"] = $('#dokter_list').val();
    // jsonVariable["nama_pasien"] = $('#nama_pasien').val();
    // jsonVariable["no_rekam_medis"] = $('#no_rekam_medis').val();
    // jsonVariable["kode_booking"] = $('#kode_booking').val();

    $.ajax({
        url: ci_baseurl + 'rekam_medis/reservasi/get_pasien_reservasi',
        type: 'GET',
        dataType: 'json',
        data: jsonVariable,
        success: function (data) {
            $('.hadir-reservasi').html(data);
        }
    });
}

function get_pasien_reservasi_batal() {
    var jsonVariable = {};
    jsonVariable["tgl_res_awal"] = $('#tgl_res_awal').val();
    jsonVariable["poli"] = $('#poli').val();
    jsonVariable["dokter"] = $('#dokter_list').val();
    jsonVariable["nama_pasien"] = $('#nama_pasien_batal').val();
    jsonVariable["no_rekam_medis"] = $('#no_rekam_medis_batal').val();
    jsonVariable["kode_booking"] = $('#kode_booking_batal').val();
    $.ajax({
        url: ci_baseurl + 'rekam_medis/reservasi/get_pasien_reservasi_batal',
        type: 'GET',
        data: jsonVariable,
        dataType: 'json',
        success: function (data) {
            $('.batal-reservasi').html(data);
        }
    });
}

function cariPasien() {
    get_pasien_reservasi();
    get_pasien_reservasi_batal()
}

function getDokter() {

    var poliruangan_id = $("#layanan").val();
    // alert(poliruangan_id);
    var tgl_reservasi = $("#tgl_reservasi").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/reservasi/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            poliruangan_id: poliruangan_id,
            tgl_reservasi: tgl_reservasi
        },
        success: function (data) {
            $("#dokter").html(data.list);
        }
    });


}

function setUpStatus(layanan) {
    if (layanan == 3 || layanan == "3" || layanan == 5 || layanan == "5") {
        $("#form-hotel-pic").removeClass("hidden");
    }
    else {
        $("#form-hotel-pic").addClass("hidden");
    }
}

function setUpDriver(layanan) {
    if (layanan >= 4) {
        $("#form-driver").removeClass("hidden");
    }
    else {
        $("#form-driver").addClass("hidden");
    }
}

function getHotel() {

    var layanan = $("#layanan_baru").val();
    setUpStatus(layanan);
    setUpDriver(layanan);

    $("#form-hotel").removeClass("hidden");

    if ((layanan == 3) || (layanan == 5)) {
        $("#lainnya").hide();
    } else {
        $("#lainnya").show();
    }

    $.ajax({
        url: ci_baseurl + "rekam_medis/reservasi/get_hotel_list",
        type: 'GET',
        dataType: 'JSON',
        data: {

        },
        success: function (data) {
            $("#hotel").html(data.list);
        }
    });
}

function getDokter2() {
    var poliruangan_id = $("#poli").val();
    var tgl_reservasi = $("#tgl_reservasi").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/reservasi/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            poliruangan_id: poliruangan_id,
            tgl_reservasi: tgl_reservasi
        },
        success: function (data) {
            $("#dokter_list").html(data.list);
        }
    });
}

function getJamDokter() {
    var dokter_id = $("#dokter").val();
    var tgl_reservasi = $("#tgl_reservasi").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/reservasi/get_jam_dokter",
        type: 'GET',
        dataType: 'JSON',
        data: {
            dokter_id: dokter_id,
            tgl_reservasi: tgl_reservasi
        },
        success: function (data) {
            $("#jam").html(data.list);
        }
    });
}

function batalReservasi(id) {
    var reservasi_id = id;
    if (reservasi_id) {
        swal({
            text: "Apakah anda yakin ingin membatalkan reservasi ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/reservasi/do_batal_reservasi/" + reservasi_id,
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateReservasi').serialize(),
                success: function (data) {
                    var ret = data.success;
                    var display = data.display;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    if (ret === true) {
                        get_pasien_reservasi();
                        get_pasien_reservasi_batal();
                        $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $("html, body").animate({
                            scrollTop: 100
                        }, "fast");
                        swal(
                            'Berhasil !',
                            'Reservasi pasien berhasil di batalkan',
                            'success'
                        );
                        get_pasien_reservasi();
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $("html, body").animate({
                            scrollTop: 100
                        }, "fast");
                    }
                }
            });
        });
    } else {
        console.log(" id reservasi null")
    }
}

function print(id) {
    window.open(ci_baseurl + 'rekam_medis/reservasi/print_rm/' + id, 'Print_RM', 'top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}


function getMetode() {
    var metode = $('#metode_regis').val();
    if (metode == "TELEPON") {
        $('#div_telp').removeAttr('hidden');
        $('#span_alamat').attr('hidden', true);
        $('#span_tgl').attr('hidden', true);
    } else {
        $('#div_telp').attr('hidden', true);
        $('#span_alamat').removeAttr('hidden');
        $('#span_tgl').removeAttr('hidden');
    }
}

function getKuota() {
    var dokter_id = $("#dokter").val();
    var tgl_reservasi = $("#tgl_reservasi").val();
    var jam = $("#jam").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/reservasi/get_kuota_dokter",
        type: 'GET',
        dataType: 'JSON',
        data: {
            dokter_id: dokter_id,
            tgl_reservasi: tgl_reservasi,
            jam: jam
        },
        success: function (data) {
            var count_pas = data.count_pasien;
            var count_dok = data.count_kuota_dokter;
            if (count_pas >= count_dok) {
                swal(
                    'Kuota Dokter Sudah Penuh !',
                    'harap hubungi dokter yang bersangkutan ',
                    'warning'
                );
            } else {
            }
        }
    });
}

function showJenisPembayaran(value) {
    if (value == 2 || value == '2') {
        $("#container-asuransi").removeClass("hidden");
    }
    else {
        $("#container-asuransi").addClass("hidden");
    }
}

var table_pasien;

function dialogPasien() {
    if (table_pasien) {
        table_pasien.destroy();
    }

    table_pasien = $('#table_list_pasien').DataTable({
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "retrieve": true,
        "autoWidth": false,
        // "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/reservasi/ajax_list_pasien",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1, 0], //last column
            "orderable": false //set not orderable
        }]
    });
    table_pasien.columns.adjust().draw();
}

function pilihPasien(id_m_pasien) {
    if (id_m_pasien) {
        $.ajax({
            url: ci_baseurl + "rekam_medis/reservasi/ajax_get_pasien_by_id",
            type: 'get',
            dataType: 'json',
            data: {
                id_m_pasien: id_m_pasien
            },
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    console.log(data.data);
                    $('#no_rekam_medis').val(data.data.no_rekam_medis);
                    $('#no_rm').val(data.data.no_rekam_medis);
                    $('#pasien_nama').val(data.data.pasien_nama);
                    $('#jk').val(data.data.jenis_kelamin);
                    $('#tanggal_lahir').val(data.tgl_lahir);
                    $('#jenis_pasien').val(data.data.jenis_pasien);
                    $('#alamat').val(data.data.pasien_alamat);

                    $("#modal_list_pasien").modal('hide');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    } else {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}