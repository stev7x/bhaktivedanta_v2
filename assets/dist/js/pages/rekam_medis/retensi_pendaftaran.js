$('#pasien_baru_umur').datepicker({
    autoclose: true
    , todayHighlight: true
    , format:'dd MM yyyy'
    , language :'id'
}); 

function getDataRMPasienLamaList() {
    $("#modal_retensi_pasien").modal('hide');
    var table_ortu = $('#table_list_pasien_lama').DataTable({
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "retrieve": true,
        // "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pendaftaran/ajax_list_pasien_lama",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1, 0], //last column
            "orderable": false //set not orderable
        }]
    });
    table_ortu.columns.adjust().draw();
}

function pilihPasienLama(id_m_pasien) {
    if (id_m_pasien) {
        $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran/ajax_get_pasien_by_id",
            type: 'get',
            dataType: 'json',
            data: {
                id_m_pasien: id_m_pasien
            },
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    data = data.data;
                    $("#pasien_lama_pasien_id").val(data.pasien_id);
                    $("#pasien_lama_nama").val(data.pasien_nama);
                    $("#pasien_lama_jk").val(data.jenis_kelamin);
                    $("#pasien_lama_umur").val(data.umur);
                    $("#pasien_lama_tanggal_kedatangan_terakhir").val(data.create_date);
                    $("#pasien_lama_alamat").val(data.pasien_alamat);

                    $("#modal_list_pasien_lama").modal('hide');
                }
            }
        });
    } else {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}

function saveRMPasienBaru(id_m_pasien) {
    if (id_m_pasien) {
        $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran/ajax_get_pasien_by_id",
            type: 'get',
            dataType: 'json',
            data: {
                id_m_pasien: id_m_pasien
            },
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    console.log(data.data);
                    // $('#pasien_id').val(data.data.pasien_id);
                    $("#no_rm").val(data.data.no_rekam_medis);
                    $("#nama_pasien").val($("#pasien_baru_nama").val())
                    $("#jenis_kelamin").val($("#pasien_baru_jk").val())
                    $("#tgl_lahir").val($("#pasien_baru_umur").val())
                    setUmur()
                    // setUmur();$('#umur_ortu').val(data.data.umur);
                    $('#alamat').val($("#pasien_baru_alamat").val());
                    $('#label_alamat').addClass('active');
                    $("#is_retension").val(1)
                    $("#modal_retensi_pasien").modal('hide');
                    // $('#modal_list_pasien').closeModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    } else {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}