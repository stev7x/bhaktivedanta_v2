$(document).ready(function () {
    getRuangan();

    $("#printDataPasien").click(function() {
        pendaftaran_id = $("#printDataPasien").val();
        window.open(ci_baseurl + 'rekam_medis/pendaftaran/print_pendaftaran/' + pendaftaran_id, 'Print_Surat', 'top=100,left=320,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
    });
});

function getType(e) {
    // alert($("#type_id").val());
    $("#no_identitas").val("");
    if (($("#type_id").val() == "KITAS") || ($("#type_id").val() == "Passpor")) {
        $("#no_identitas").prop('type', 'text');
    } else {
        $("#no_identitas").prop('type', 'number');
    }
}

var textSwal = "Apakah data yang dimasukan sudah benar ?";
$( "button#viewKSPR" ).hide();
// $( "span" ).show( 2000 );
function setUmurOrtu() {
    var tgl_lahir = $("#tgl_lahir_ortu").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/hitung_umur",
        type: 'GET',
        dataType: 'JSON',
        data: {
            tgl_lahir: tgl_lahir
        },
        success: function (data) {
            console.log('masuk');
            $("#umur_ortu").val(data.umur);
            $("#umur_ortu").attr('readonly', 'true');

        }
    });
}

function setUmur() {
    var tgl_lahir = $("#tgl_lahir").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/hitung_umur",
        type: 'GET',
        dataType: 'JSON',
        data: {
            tgl_lahir: tgl_lahir
        },
        success: function (data) {
            console.log('masuk');
            $("#umur").val(data.umur);
            $("#umur").attr('readonly', 'true');
        }
    });
}

function setUmurSustri() {
    var tgl_lahir = $("#tgl_suami_istri").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/hitung_umur",
        type: 'GET',
        dataType: 'JSON',
        data: {
            tgl_lahir: tgl_lahir
        },
        success: function (data) {
            console.log('masuk');
            $("#umur_suami_istri").val(data.umur);
            $("#umur_suami_istri").attr('readonly', 'true');
        }
    });
}



function cekStatusKawin() {
    var type = $(".status_kawin").val();
    var t = $('.Suami_istri');
    var n = $('.nama_suami');
    console.log(" cek status if : " + type);
    var jk = $('#jenis_kelamin').val();

    if (type == "Kawin") {
        console.log(" masuk if : " + type);
        console.log(" masuk if jenke : " + jk);

        t.removeClass('hide').show(500);
        n.show(500);
        var temp_nama;

        $('#nama_suami_istri').on('keyup', function (e) {
            temp_nama = this.value;
            $('#pj_nama').val(temp_nama);
        });

        var pj = "";

        if (jk == "Perempuan") {
            pj = "Suami";
        } else if (jk == "Laki-laki") {
            pj = "Istri";

        }

        $('#hubunganpj').val(pj).select();



        // t.show().fadeIn(2000);

    } else if (type == "Belum Kawin" || type == "Duda/Janda" || type == "Di Bawah Umur") {
        console.log(" masuk else : " + type);
        t.hide(300);
        n.hide(300);

        var temp_nama_ortu;

        $('#nama_orangtua').on('keyup', function (e) {
            temp_nama_ortu = this.value;
            $('#pj_nama').val(temp_nama_ortu);
        });

        $('#hubunganpj').val('Orang Tua').select();

    }
}

var table_pasien;

function dialogPasien() {
    if (table_pasien) {
        table_pasien.destroy();
    }
    table_pasien = $('#table_list_pasien').DataTable({
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "retrieve": true,
        "autoWidth": false,
        // "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pendaftaran/ajax_list_pasien",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1, 0], //last column
            "orderable": false //set not orderable
        }]
    });
    // table_pasien.columns.adjust().draw();
}

var table_ortu;

function dialogOrtu() {
    if (table_ortu) {
        table_ortu.destroy();
    }
    table_ortu = $('#table_list_ortu').DataTable({
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "retrieve": true,
        // "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pendaftaran/ajax_list_ortu",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1, 0], //last column
            "orderable": false //set not orderable
        }]
    });
    // table_ortu.columns.adjust().draw();
}

var table_perujuk;

function dialogPerujuk() {

    if (table_perujuk) {
        table_perujuk.destroy();
    }
    table_perujuk = $('#table_list_rujuk').DataTable({
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pendaftaran/ajax_list_perujuk",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [-1, 0], //last column
            "orderable": false //set not orderable
        }]
    });
    // table_perujuk.columns.adjust().draw();
}


// disable mousewheel on a input number field when in focus
// (to prevent Cromium browsers change the value when scrolling)
$('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault()
    })
})
$('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
})




// $(document).on('click', '#statuskawin', function (){
//             var t = $('#Suami_istri');
//             var h = $(this);
//             if (t.hasClass('hide')) {
//                 t.removeClass('hide');
//                 // h.html('<i class="fa fa-times"></i>');
//             }else{
//                 t.addClass('hide');
//                 // h.html('<i class="fa fa-pencil"></i> write review');
//             }
//         });



var table_pasien;
$(document).ready(function () {
    // getHotel();
    // getAsuransi();
    $('#cari_no_rekam_medis').keypress(function () {
        var val_pasien = $('#cari_no_rekam_medis').val();
        $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran/autocomplete_pasien",
            type: 'get',
            async: false,
            dataType: 'json',
            data: {
                val_pasien: val_pasien
            },
            success: function (data) { /* console.log(data); */
                var ret = data.success;
                if (ret == true) {
                    // console.log(data.data);
                    console.log('masuk success');
                    $(".autocomplete").autocomplete({
                        max: 10,
                        scrollHeight: 250,
                        source: data.data,
                        open: function (event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 2");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index", "999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#pasien_id').val(ui.item.id);
                            $('#type_id').val(ui.item.type_id);
                            $('#no_identitas').val(ui.item.no_identitas);
                            $('#no_rm').removeAttr("value");
                            $('#no_rm').attr("value", ui.item.no_rekam_medis);
                            $('#no_rm').val(ui.item.no_rekam_medis);
                            $('#no_bpjs').val(ui.item.no_bpjs);
                            $('#nama_pasien').val(ui.item.pasien_nama);
                            $('#nama_panggilan').val(ui.item.nama_panggilan);
                            $('#tempat_lahir').val(ui.item.tempat_lahir);
                            $('#tgl_lahir').val(ui.item.tanggal_lahir);
                            setUmur(); //$('#umur').val(ui.item.umur);
                            $('#jenis_kelamin').val(ui.item.jenis_kelamin);
                            $('.status_kawin').val(ui.item.status_kawin);
                            if (ui.item.status_kawin == "Kawin") {
                                $('.nama_suami').removeAttr('style', true);
                                $('.Suami_istri').removeAttr('style', true);
                                $('#nama_suami_istri').val(ui.item.nama_suami_istri);
                                $('#tgl_suami_istri').val(ui.item.tgl_lahir_suami_istri);
                                $('#umur_suami_istri').val(ui.item.umur_suami_istri);
                            }
                            $('#golongan_darah').val(ui.item.gol_darah);
                            if (ui.item.rhesus == '1') {
                                $('#rhesus1').prop('checked', true);
                            } else {
                                $('#rhesus0').prop('checked', true);
                            }
                            $('#alamat').val(ui.item.alamat);
                            $("#propinsi").val(ui.item.propinsi);
                            getDaerahAll(ui.item.propinsi, ui.item.kabupaten, ui.item.kecamatan, ui.item.kelurahan);
                            $('#no_tlp_rmh').val(ui.item.tlp_rumah);
                            $('#no_mobile').val(ui.item.tlp_selular);
                            $('#pekerjaan').val(ui.item.pekerjaan);
                            $('#pekerjaan').select2();
                            $('#bahasa').val(ui.item.bahasa);
                            $('#bahasa').select2();
                            $('#warga_negara').val(ui.item.warga_negara);
                            $('#agama').val(ui.item.agama);
                            $('#pendidikan').val(ui.item.pendidikan);
                            $('#nama_orangtua').val(ui.item.nama_orangtua);
                            $('#tgl_lahir_ortu').val(ui.item.tgl_lahir_ortu);
                            $('#umur_ortu').val(ui.item.umur_ortu);
                            $('#catatan_khusus').text(ui.item.catatan_khusus);

                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        return $("<li>")
                            .append("<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.alamat + "</font></a>")
                            .appendTo(ul);
                    };
                } else {
                    console.log('masuk else');
                    $("#id_m_pasien").val('0#');
                }
            }
        });
    });

    $('button#savePendaftaran').click(function () {
        swal({
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(function () {

            $.ajax({
                url: ci_baseurl + "rekam_medis/pendaftaran/do_create_pendaftaran",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreatePendaftaran').serialize(),
                success: function (data) {
                    var ret = data.success;
                    var display = data.display;
                    console.log(data);
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    if (ret === true) {
                        $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        //
                        console.log("i am here true : " + data.pendaftaran_id);
                        $('button#savePendaftaran').attr('hidden', 'true');
                        $('#reset').removeAttr('hidden');
                        $('button#printDataPasien').removeAttr('hidden');
                        $('button#printPendaftaran').removeAttr('hidden');
                        $('button#printPendaftaran').attr('onclick', 'print(' + data.pendaftaran_id + ')');
                        $('button#printKartu').removeAttr('hidden');
                        $('button#printDataPasien').val(data.pendaftaran_id);
                        $('button#printKartu').attr('onclick', 'printKartuBerobat(' + data.pendaftaran_id + ')');
                        $('button#printKartu').attr('onclick', 'printData(' + data.pendaftaran_id + ',"print_kartu")');
                        $('button#printDetail').attr('onclick', 'printData(' + data.pendaftaran_id + ',"print_data_pasien")');
                        $('button#printSurat').attr('onclick', 'printData(' + data.pendaftaran_id + ',"form_input_surat")');
                        $('button#printKartuHilang').attr('onclick', 'printData(' + data.pendaftaran_id + ',"print_hilang_kartu")');
                        $('button#printSticker').attr('onclick', 'printData(' + data.pendaftaran_id + ',"print_sticker")');
                        $('button#printGelang').attr('onclick', 'printData(' + data.pendaftaran_id + ',"print_gelang")');
                        $('button#printCasemix').attr('onclick', 'printData(' + data.pendaftaran_id + ',"print_casemix")');
                        $('button#printResiko').attr('onclick', 'printData(' + data.pendaftaran_id + ',"print_resiko")');
                        $('button#printResiko2').attr('onclick', 'printData(' + data.pendaftaran_id + ',"print_resiko2")');

                        $("html, body").animate({
                            scrollTop: 100
                        }, "fast");
                        // swal(
                        //     'No.Antrian     : '+ data.no_antri,
                        //     'No.RM Pasien   : '+ data.no_rm,
                        // );
                        swal(
                            'Berhasil !',
                            'Pendaftaran Pasien berhasil ditambahkan',
                            'success'

                        );
                    } else if (display === true) {
                        swal(
                            'Pendaftaran Gagal',
                            data.messages,
                            'error'
                        );

                    } else {
                        console.log("masuk false");
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $("html, body").animate({
                            scrollTop: 100
                        }, "fast");
                    }
                }
            });
        });
    });

    $("select[name='cek_tinggal']").on("change",function(){
        cek_tinggal = $(this).val();
        // alert(cek_tinggal);
        if(cek_tinggal == 'domisili'){
            $("#domisili").show();
            $("#hotel").hide();
            $('#alamat_domisili').removeAttr('readonly');
            $('#alamat_domisili').val();
        }
        else{
            $("#domisili").hide();
            getHotel();
            $("#hotel").show();
            $('#alamat_domisili').attr('readonly', 'TRUE');
            $('#alamat_domisili').val($('#alamat').val());

        }
    });
    $("#cek_hotel").change(function () {
        if (this.checked) {
            $('select[name="hotel"]').attr("disabled", true);
            $('#hotel_nama').removeAttr("disabled", true);
            $('#alamat_hotel').removeAttr("disabled", true);
            $('#tlp_hotel').removeAttr("disabled", true);
            // alert($('#cek_hotel').val());
        } else {
            $('select[name="hotel"]').removeAttr("disabled", true);
            $('#hotel_nama').attr("disabled", true);
            $('#hotel_nama').val("");
            $('#alamat_hotel').attr("disabled", true);
            $('#tlp_hotel').attr("disabled", true);
            $('#alamat_hotel').val("");
            $('#tlp_hotel').val("");
            // alert("hurung hotel list");
        }
    });
    $("#cek_insurance").change(function () {
        if (this.checked) {
            $('#div_insurance').show();
            tmp = '<option value="Asuransi">Asuransi</option>'
            $('#byr_type_bayar').append(tmp);
        } else{
            $('#div_insurance').hide();
            $('#byr_type_bayar').children('option:last').remove();
        }
    });
    $('#name_insurance').change(function(){
        // if(this.val == 'company'){
        //     // alert('company')
        //     getAsuransi(this.val)
        // }else{
        //     // alert('travel')
        // }
        getAsuransi($(this).val())
    });

});

var table_skor_kspr;
var table_faktor_resiko;
// FUNCTION KSPR jos
$('button#viewKSPR').click(function () {
    var pasien_kspr_id = $( "input#pasien_kspr_id" ).val();
    $("#modal_view_kspr").modal('show');
    console.log(pasien_kspr_id);
    skorKSPR();
    faktorResiko();
});

function cekSkorKSPR() {
    // alert('Total skor KSPR: ' + table_skor_kspr.column(3).data().sum());
    global_kspr = (table_skor_kspr.column(4).data().sum());
    swal({
        text: "Total skor KSPR: " + global_kspr + " + 2",
        // type: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        // cancelButtonText: 'Tutup',
        confirmButtonText: 'Tutup'
    })
}

function skorKSPR(){
    if(table_skor_kspr){table_skor_kspr.destroy();}
    table_skor_kspr = $('#table_skor_kspr').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pendaftaran/ajax_list_skor_kspr",
            "type": "GET",
            "data" : function(d){
                d.pasien_kspr_id = $("input#pasien_kspr_id").val();
            }
        }
    });
    table_skor_kspr.columns.adjust().draw();


    $.fn.dataTable.Api.register('column().data().sum()', function () {
        return this.reduce(function (a, b) {
            var x = parseFloat(a) || 0;
            var y = parseFloat(b) || 0;
            return x + y;
        }, 0);
    });


    global_kspr = (table_skor_kspr.column(4).data().sum());
    $("#jum_skor").val(global_kspr);
    console.log("table_skor_kspr done load");
}

function faktorResiko(){
    if(table_faktor_resiko){table_faktor_resiko.destroy();}
    table_faktor_resiko = $('#table_faktor_resiko').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pendaftaran/ajax_list_faktor_resiko",
            "type": "GET",
            "data": function (d) {
                d.pasien_kspr_id = $("input#pasien_kspr_id").val();
            }
        }
    });
    table_faktor_resiko.columns.adjust().draw();
    console.log("table_faktor_resiko done load");
}


function validasi_ktp() {
    var ktp = $('#no_identitas').val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/validasi_ktp",
        type: 'GET',
        dataType: 'JSON',
        data: {
            ktp: ktp
        },
        success: function (data) {
            var ret = data.display;
            if (ret === true) {
                console.log('success');
                console.log('messages : ' + data.message);
                $(".message_ktp").text(data.message).css('color', 'red').fadeTo(4000, 500);
            } else {
                console.log('not success');
                console.log('messages : ' + data.message);
                $(".message_ktp").text(data.message).css('color', 'green').fadeTo(4000, 500).slideUp(500, function () {
                    $(".message_ktp").hide();
                });
            }
        }
    });
}


function validasi_no_rm() {
    var no_rm = $('#no_rm').val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/validasi_no_rm",
        type: 'GET',
        dataType: 'JSON',
        data: {
            no_rm: no_rm
        },
        success: function (data) {
            var ret = data.display;
            if (ret == true) {
                console.log('bisa digunakan');
                console.log('messages : ' + data.message);
                $(".message_no_rm").text(data.message).css('color', 'green').fadeTo(4000, 500).slideUp(500, function () {
                    $(".message_no_rm").hide();
                });
                $("#is_no_rm").val(0);
                textSwal = "Apakah data yang dimasukan sudah benar ?";

            } else {

                console.log('sudah di pakai');
                console.log('messages : ' + data.message);
                $(".message_no_rm").text(data.message).css('color', 'red').fadeTo(4000, 500);
                $("#is_no_rm").val(1);
                textSwal = "Data dengan No RM tersebut sudah ada, apakah anda ingin menggantinya ?";

            }
        }
    });
}

function validasi_bpjs() {
    var bpjs = $('#no_bpjs').val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/validasi_bpjs",
        type: 'GET',
        dataType: 'JSON',
        data: {
            bpjs: bpjs
        },
        success: function (data) {
            var ret = data.display;
            if (ret === true) {
                console.log('success');
                console.log('messages : ' + data.message);
                $(".message_bpjs").text(data.message).css('color', 'red').fadeTo(4000, 500).slideUp(500, function () {
                    $(".message_bpjs").hide();
                });
            } else {
                console.log('not success');
                console.log('messages : ' + data.message);
                $(".message_bpjs").text(data.message).css('color', 'green').fadeTo(4000, 500).slideUp(500, function () {
                    $(".message_bpjs").hide();
                });
            }
        }
    });
}

function print(pendaftaran_id) {
    console.log("masuk print : " + ci_baseurl + "rekam_medis/pendaftaran/print_pendaftaran/" + pendaftaran_id);

    window.open(ci_baseurl + 'rekam_medis/pendaftaran/print_pendaftaran/' + pendaftaran_id, 'Print_Pendaftaran', 'top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

function printKartuBerobat(pendaftaran_id) {
    console.log("masuk print : " + ci_baseurl + "rekam_medis/pendaftaran/print_kartu/" + pendaftaran_id);

    window.open(ci_baseurl + 'rekam_medis/pendaftaran/print_kartu/' + pendaftaran_id, 'Print_Kartu', 'top=10,left=150,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}


function cekTypeBayar() {
    var type = $("#byr_type_bayar").val();
    if (type == "Asuransi") {
        $("#is_asuransi").removeAttr("style", true);
        $("#jenis_pembayaran").val("")
    } else {
        $("#is_asuransi").attr("style", "display:none");
        $("#jenis_pembayaran").val(type)
    }
}

function cekBpjs() {
    var asuransi1 = $("#asuransi1").val();
    var asuransi2 = $("#asuransi2").val();
    var temp_bpjs;
    temp_bpjs = $("#no_bpjs").val();
    // console.log("data asuransi1 : " + asuransi1 + " asuransi2 : " + asuransi2);

    if (asuransi1 == "BPJS Kesehatan" || asuransi1 == "BPJS Ketenagakerjaan") {
        $("#jenis_pembayaran").val("BPJS");
        $("#byr_nomorasuransi1").val(temp_bpjs);
        $("#byr_nomorasuransi2").val("");
    } else if (asuransi2 == "BPJS Kesehatan" || asuransi2 == "BPJS Ketenagakerjaan") {
        $("#jenis_pembayaran").val("BPJS");
        $("#byr_nomorasuransi1").val("");
        $("#byr_nomorasuransi2").val(temp_bpjs);

    } else {
        $("#jenis_pembayaran").val("Pribadi");
        $("#byr_nomorasuransi1").val("");
        $("#byr_nomorasuransi2").val("");

    }

}


function cekPJRJ(instalasi_id) {
    var nama_pasien = $('#nama_pasien').val();
    var no_tlp_rmh = $('#no_tlp_rmh').val();
    var no_tlp_mobile = $('#no_mobile').val();
    var alamat = $('#alamat').val();
    if (instalasi_id == '1') {
        $('#pj_nama').val(nama_pasien);
        $('#pj_no_tlp_rmh').val(no_tlp_rmh);
        $('#pj_no_mobile').val(no_tlp_mobile);
        $('#pj_alamat').val(alamat);
    } else {
        $('#pj_nama').val('');
        $('#pj_no_tlp_rmh').val('');
        $('#pj_no_mobile').val('');
        $('#pj_alamat').val('');
    }
}

function getRuangan() {
    var instalasi_id = $("#instalasi").val();
    // cekPJRJ(instalasi_id);f
    if (instalasi_id == '3') {
        $('#rowKamar').removeAttr('style');
        $('#rowDokterAnastesi').removeAttr('style');
        $('#rowPerawat').removeAttr('style');
    } else if(instalasi_id == '2'){
        $('#toggle_hide_dokter').attr('style', 'display:none;');
    } else{
        $('#toggle_hide_dokter').removeAttr('style');
        $('#rowKamar').attr('style', 'display:none;');
        $('#rowDokterAnastesi').attr('style', 'display:none;');
        $('#rowPerawat').attr('style', 'display:none;');
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_ruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            instalasi_id: instalasi_id
        },
        success: function (data) {
            $("#poli_ruangan").html(data.list);
            getKelasPoliruangan();
        }
    });
}

function getKelasPoliruangan() {
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kelasruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            poliruangan_id: poliruangan_id
        },
        success: function (data) {
            $("#kelas_pelayanan").html(data.list);
            getKamar();
        }
    });
    getDokterRuangan();
}

function getKamar() {
    var poliruangan_id = $("#poli_ruangan").val();
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    var instalasi_id = $("#instalasi").val();
    if (instalasi_id == '3') {
        $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran/get_kamarruangan",
            type: 'GET',
            dataType: 'JSON',
            data: {
                poliruangan_id: poliruangan_id,
                kelaspelayanan_id: kelaspelayanan_id
            },
            success: function (data) {
                $("#kamarruangan").html(data.list);
            }
        });
    }
}

function getDokterRuangan() {
    var poliruangan_id = $("#poli_ruangan").val();
    var instalasi_id = $("#instalasi").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            poliruangan_id: poliruangan_id,
            instalasi_id: instalasi_id
        },
        success: function (data) {
            $("#dokter").html(data.list);
            // $("#dokter").val("val2").change();
            if($("#dokter_id").val() != "") {
                $("#dokter").val($("#dokter_id").val()).change();
            }
        }
    });
}

function getKabupatenPj() {
    var propinsi_id = $("#pj_propinsi").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            propinsi_id: propinsi_id
        },
        success: function (data) {
            $("#pj_kabupaten").html(data.list);
        }
    });
}

function getKecamatanPj() {
    var kabupaten_id = $("#pj_kabupaten").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kecamatan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            kabupaten_id: kabupaten_id
        },
        success: function (data) {
            $("#pj_kecamatan").html(data.list);
        }
    });
}

function getKelurahanPj() {
    var kecamatan_id = $("#pj_kecamatan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kelurahan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            kecamatan_id: kecamatan_id
        },
        success: function (data) {
            $("#pj_kelurahan").html(data.list);
        }
    });
}

function pilihOrtu(id_m_pasien) {
    if (id_m_pasien) {
        $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran/ajax_get_pasien_by_id",
            type: 'get',
            dataType: 'json',
            data: {
                id_m_pasien: id_m_pasien
            },
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    console.log(data.data);
                    // $('#pasien_id').val(data.data.pasien_id);
                    $('#type_id').val(data.data.type_identitas);
                    $('#no_identitas').val(data.data.no_identitas);
                    $('#nama_pasien').val(data.data.pasien_nama);
                    $('#jenis_kelamin').val(data.data.jenis_kelamin);
                    $('#tempat_lahir').val(data.data.tempat_lahir);
                    $('#warga_negara').val(data.data.warga_negara);
                    $('#tgl_lahir').val(data.tgl_lahir);
                    $('#umur').val(data.data.umur);
                    $('#jenis_pasien').val(data.data.jenis_pasien);
                    $('#type_call').val(data.data.type_call);
                    $('#email').val(data.data.email);
                    $('#no_mobile').val(data.data.no_mobile);
                    $('#hotel').val(data.data.id_hotel);


                    $('#present_complain').val(data.data.present_complain);
                    var date_first_complain = new Date(data.data.date_first_complain);
                    var date_first_consultation = new Date(data.data.date_first_consultation);
                    var options = {  year: 'numeric', month: 'long', day: 'numeric' };


                    $('#date_first_complain').val(date_first_complain.toLocaleDateString('id-ID', options));
                    $('#date_first_consultation').val(date_first_consultation.toLocaleDateString('id-ID', options));
                    $('#allergy_history').val(data.data.allergy_history);
                    $('#pregnant').val(data.data.pregnant);
                    $('#current_medication').val(data.data.current_medication);
                    $('#weight').val(data.data.weight);
                    $('#medical_history').val(data.data.medical_history);
                    $('#medical_event').val(data.data.medical_event);

                    // setUmur();$('#umur_ortu').val(data.data.umur);

                    $('#alamat').val(data.data.pasien_alamat);
                    $('#alamat_domisili').val(data.data.pasien_alamat_domisili);
                    $('#label_alamat').addClass('active');
                    $('#propinsi').val(data.data.propinsi_id);
                    $('#propinsi').select2();
                    getDaerahAll(data.data.propinsi_id, data.data.kabupaten_id, data.data.kecamatan_id, data.data.kelurahan_id);
                    $('#kabupaten').val(data.data.kabupaten_id);
                    $("#kabupaten option[value='']").val(data.data.kabupaten_id);
                    $('#kabupaten').select2();
                    $('#kecamatan').val(data.data.kecamatan_id);
                    $("#kecamatan option[value='']").val(data.data.kecamatan_id);
                    $('#kecamatan').select2();
                    $("#kelurahan option[value='']").val(data.data.kelurahan_id);
                    $('#kelurahan').select2();
                    $('#no_tlp_rmh').val(data.data.tlp_rumah);
                    $('#no_mobile').val(data.data.tlp_selular);
                    $('#pekerjaan').val(data.data.pekerjaan_id);
                    $('#pekerjaan').select2();
                    $('#warga_negara').val(data.data.warga_negara);
                    $('#warga_negara').select2();
                    $('#agama').val(data.data.agama_id);
                    $('#suku').val(data.data.suku);
                    $('#pendidikan').val(data.data.pendidikan_id);

                    // data penanggung jawab
                    $('#pj_nama').val(data.data.pasien_nama);
                    $('#pj_no_tlp_rmh').val(data.data.tlp_rumah);
                    $('#pj_no_mobile').val(data.data.tlp_selular);
                    $('#pj_alamat').val(data.data.pasien_alamat);
                    $('#pj_propinsi').val(data.data.propinsi_id);
                    $('#pj_propinsi').select2();
                    getDaerahAll(data.data.propinsi_id, data.data.kabupaten_id, data.data.kecamatan_id, data.data.kelurahan_id);
                    $("#pj_kabupaten option[value='" + data.data.kabupaten_id + "']").val(data.data.kabupaten_id);
                    // $('#pj_kabupaten').select2();
                    $('#pj_kecamatan').val(data.data.kecamatan_id);
                    $('#pj_kecamatan').select2();
                    $('#pj_kelurahan').val(data.data.kelurahan_id);
                    $('#pj_kelurahan').select2();

                    $("#modal_list_ortu").modal('hide');

                    // $('#modal_list_pasien').closeModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    } else {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}

function pilihPasien(id_m_pasien) {
    if (id_m_pasien) {
        $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran/ajax_get_pasien_by_id",
            type: 'get',
            dataType: 'json',
            data: {
                id_m_pasien: id_m_pasien
            },
            success: function (data) {
                var ret = data.success;
                // console.log(data.data_reservasi);
                if (ret === true) {
                    // console.log(data.data);
                    console.log(data.data_reservasi);
                    $('#type_id').val(data.data.type_identitas);
                    $('#no_identitas').val(data.data.no_identitas);
                    $('#nama_pasien').val(data.data.pasien_nama);
                    $('#jenis_kelamin').val(data.data.jenis_kelamin);
                    $('#tempat_lahir').val(data.data.tempat_lahir);
                    $('#warga_negara').val(data.data.warga_negara);
                    $('#tgl_lahir').val(data.tgl_lahir);
                    $('#umur').val(data.data.umur);
                    $('#jenis_pasien').val(data.data.jenis_pasien);
                    $('#type_call').val(data.data.type_call);
                    $('#email').val(data.data.email);
                    $('#no_mobile').val(data.data.no_mobile);
                    $('#hotel').val(data.data.id_hotel);

                    $('#present_complain').val(data.data.present_complain);
                    var date_first_complain = new Date(data.data.date_first_complain);
                    var date_first_consultation = new Date(data.data.date_first_consultation);
                    var options = {  year: 'numeric', month: 'long', day: 'numeric' };

                    $('#date_first_complain').val(date_first_complain.toLocaleDateString('id-ID', options));
                    $('#date_first_consultation').val(date_first_consultation.toLocaleDateString('id-ID', options));
                    $('#allergy_history').val(data.data.allergy_history);
                    $('#pregnant').val(data.data.pregnant);
                    $('#current_medication').val(data.data.current_medication);
                    $('#weight').val(data.data.weight);
                    $('#medical_history').val(data.data.medical_history);
                    $('#medical_event').val(data.data.medical_event);

                    $('#jenis_kelamin').val(data.data.jenis_kelamin);
                    $('.status_kawin').val(data.data.status_kawin);
                    if (data.data.status_kawin == "Kawin") {
                        $('.nama_suami').removeAttr('style', true);
                        $('.Suami_istri').removeAttr('style', true);
                        $('#nama_suami_istri').val(data.data.nama_suami_istri);
                        $('#tgl_suami_istri').val(data.tgl_lahir_suami);
                        // $('#umur_suami_istri').val(data.data.umur_suami_istri);
                        $('#pj_nama').val(data.data.nama_suami_istri);

                        if (data.tgl_lahir_suami != "") {
                            setUmurSustri();
                        }
                    } else {
                        $('#pj_nama').val(data.data.nama_orangtua);
                    }

                    $('#alamat').val(data.data.pasien_alamat);
                    $('#alamat_domisili').val(data.data.pasien_alamat_domisili);
                    $('#label_alamat').addClass('active');
                    $('#propinsi').val(data.data.propinsi_id);
                    $('#propinsi').select2();
                    getDaerahAll(data.data.propinsi_id, data.data.kabupaten_id, data.data.kecamatan_id, data.data.kelurahan_id);

                    // $('#kabupaten').val(data.data.kabupaten_id);
                    // $('#kabupaten').trigger('change.select2');
                    // $('#kecamatan').val(data.data.kecamatanan_id);
                    // $('#kecamatan').trigger('change.select2');
                    // $('#kelurahan').val(data.data.kelurahan_id);
                    // $('#kelurahan').trigger('change');
                    $('#no_tlp_rmh').val(data.data.tlp_rumah);
                    $('#no_mobile').val(data.data.tlp_selular);
                    $('#pekerjaan').val(data.pekerjaan_id);
                    $('#pekerjaan').select2();
                    $('#warga_negara').val(data.data.warga_negara);
                    $('#agama').val(data.data.agama_id);
                    $('#bahasa').val(data.data.bahasa_id);
                    $('#bahasa').select2();

                    $('#suku').val(data.suku);

                    $('#asuransi1').val(data.data_reservasi.asuransi1);
                    $('#asuransi2').val(data.data_reservasi.asuransi2);
                    $('#no_asuransi1').val(data.data_reservasi.no_asuransi1);
                    $('#no_asuransi2').val(data.data_reservasi.no_asuransi2);

                    $('#reservasi_id').val(data.data_reservasi.reservasi_id);

                    $('#room_number').val(data.data_reservasi.room_number);

                    if ((data.data_reservasi.asuransi1 != "") || (data.data_reservasi.asuransi1 != "")) {
                        $('#cek_insurance').prop("checked", true);
                        $('#div_insurance').show();
                    } else {
                        $('#cek_insurance').prop("checked", false);
                        $('#div_insurance').hide();
                    }

                    if (data.data.jenis_pasien === "1") {
                        $('#others').text(" Others");
                        $('#others').css('color', '');
                    } else {
                        $('#others').text(" * Others");
                        $('#others').css('color', 'red');
                    }

                    if (data.data_reservasi.pic_hotel != "") {
                        $('#pj_nama').val(data.data_reservasi.pic_hotel);
                    } else {
                        $('#pj_nama').val("");
                    }

                    $('#provinsi_tinggal').val(data.provinsi_tinggal_id);
                    $('#provinsi_tinggal').select2();
                    // getDaerahAll(data.provinsi_tinggal_id, data.kabupaten_tinggal_id, data.kecamatan_tinggal_id, data.kelurahan_tinggal_id);
                    // $('#kabupaten_tinggal').val(data.data.kabupaten_tinggal_id).trigger('change.select2');
                    // $('#kabupaten_tinggal').select2();
                    // $('#kecamatan_tinggal').val(data.data.kecamatan_tinggal_id).trigger('change.select2');
                    // $('#kecamatan_tinggal').select2();
                    // $('#kelurahan_tinggal').val(data.data.kelurahan_tinggal_id).trigger('change.select2');
                    // $('#kelurahan_tinggal').select2();
                    $('#pendidikan').val(data.data.pendidikan_id);
                    $('#nama_orangtua').val(data.data.nama_orangtua);
                    $('#tgl_lahir_ortu').val(data.tgl_lahir_ortu);
                    if (data.tgl_lahir_ortu != "") {
                        setUmurOrtu();
                    }
                    $('#catatan_khusus').val(data.catatan_khusus);
                    // data penanggung jawab
                    // $('#pj_nama').val(data.pasien_nama);
                    $('#pj_no_tlp_rmh').val(data.tlp_rumah);
                    $('#pj_no_mobile').val(data.tlp_selular);
                    $('#pj_alamat').val(data.pasien_alamat);
                    $('#pj_propinsi').val(data.propinsi_id);
                    $('#pj_propinsi').select2();
                    // getDaerahAll(data.propinsi_id, data.kabupaten_id, data.kecamatan_id, data.kelurahan_id);/
                    // $("#pj_kabupaten option[value='"+data.data.kabupaten_id+"']").val(data.data.kabupaten_id);
                    // $('#pj_kecamatan').val(data.data.kecamatan_id);
                    // $('#pj_kecamatan').select2();
                    // $('#pj_kelurahan').val(data.data.kelurahan_id);
                    // $('#pj_kelurahan').select2();
                    $('#hubunganpj').val(data.hubunganpj);
                    $("#modal_list_pasien").modal('hide');

                    // keperluan view kspr
                    // $('#pasien_id_kspr').val(data.data.pasien_id);
                    $("input#pasien_kspr_id").val(data.data.pasien_id);
                    $("button#viewKSPR").show("slow");
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    } else {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}

function pilihRujuk(id_perujuk) {
    if (id_perujuk) {
        $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran/ajax_get_perujuk_by_id",
            type: 'get',
            dataType: 'json',
            data: {
                id_perujuk: id_perujuk
            },
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    console.log(data.data);
                    console.log("nama perujuk : " + data.data.nama_perujuk);
                    $('#rm_namaperujuk').val(data.data.nama_perujuk);
                    $('#rm_alamatperujuk').val(data.data.alamat_kantor);
                    $("#modal_list_rujuk").modal('hide');

                    // $('#modal_list_pasien').closeModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    } else {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}

$(document).ready(function () {
    var temp_alamat;
    $('#alamat').on('keyup', function (e) {
        temp_alamat = this.value;
        $('#alamat_domisili').val(temp_alamat);
        $('#pj_alamat').val(temp_alamat);
    });

    $("#cek_alamat").change(function () {
        if (this.checked) {
            $('#alamat_domisili').removeAttr('readonly');
            $('#alamat_domisili').val('');
            $('#alamat2').show(500);


        } else {
            $('#alamat_domisili').attr('readonly', 'TRUE');
            $('#alamat_domisili').val(temp_alamat);
            $('#alamat2').hide(300);

        }
    });


    $("#is_bayi").change(function () {
        if (this.checked) {
            $('#berat').removeAttr('hidden');
        } else {
            $('#berat').attr('hidden', true);
        }
    });


});

function getRingkasaMasuk(instalasi_id, poliruangan_id, dokter_id, tgl_reservasi, jam, kelas_pelayanan) {
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_ringkasan_masuk",
        type: 'GET',
        dataType: 'JSON',
        data: {
            instalasi: instalasi_id,
            poli_ruangan: poliruangan_id,
            dokter_id: dokter_id,
            tgl_reservasi: tgl_reservasi
        },
        success: function (data) {
            $('#poli_ruangan').html(data.ruangan).val(poliruangan_id);
            $('#kelas_pelayanan').html(data.kelas_pelayanan).val(kelas_pelayanan);
            $('#dokter').html(data.dokter).val(dokter_id);
            $('#jam_periksa').html(data.jam).val(jam);
        }
    });
}

function getDaerahAll(propinsi_id, kabupaten_id, kecamatan_id, kelurahan_id) {
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_daerah_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            propinsi_id: propinsi_id,
            kabupaten_id: kabupaten_id,
            kecamatan_id: kecamatan_id,
            kelurahan_id: kelurahan_id
        },
        success: function (data) {
            $("#kabupaten").html(data.list_kabupaten).select2();
            $("#kecamatan").html(data.list_kecamatan).select2();
            $("#kelurahan").html(data.list_kelurahan).select2();


            // $('#kabupaten').val(kabupaten_id).trigger('change.select2');
            // $('#kecamatan').val(kecamatan_id).trigger('change.select2');
            // $('#kelurahan').val(kelurahan_id).trigger('change.select2');

            // $("#kabupaten_tinggal").html(data.list_kabupaten).trigger('change.select2');
            // $("#kecamatan_tinggal").html(data.list_kecamatan).trigger('change.select2');
            // $("#kelurahan_tinggal").html(data.list_kelurahan).trigger('change.select2');

            // $("#pj_kabupaten").html(data.list_kabupaten).trigger('change.select2');
            // $("#pj_kecamatan").html(data.list_kecamatan).trigger('change.select2');
            // $("#pj_kelurahan").html(data.list_kelurahan).trigger('change.select2');
        }
    });
}

function getKabupaten(id) {
    if (id == null || id == 'undifined' || id == '') {
        var propinsi_id = $("#propinsi").val();
    } else {
        var propinsi_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            propinsi_id: propinsi_id
        },
        success: function (data) {
            $("#kabupaten").html(data.list);


            $("#pj_propinsi").val(propinsi_id);
            $("#pj_propinsi").select2();
            $("#pj_kabupaten").html(data.list);


        }
    });
}

function getKabupatenTinggal(id) {
    if (id == null || id == 'undifined' || id == '') {
        var propinsi_id = $("#provinsi_tinggal").val();
    } else {
        var propinsi_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            propinsi_id: propinsi_id
        },
        success: function (data) {
            $("#kabupaten_tinggal").html(data.list);

        }
    });
}

function getKecamatan(id) {
    if (id == null || id == 'undefined' || id == '') {
        var kabupaten_id = $("#kabupaten").val();
    } else {
        var kabupaten_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kecamatan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            kabupaten_id: kabupaten_id
        },
        success: function (data) {
            $("#kecamatan").html(data.list);
            $("#pj_kecamatan").html(data.list);
            $("#pj_kabupaten").val(kabupaten_id).select2();
        }
    });
}

function getKecamatanTinggal(id) {
    if (id == null || id == 'undefined' || id == '') {
        var kabupaten_id = $("#kabupaten_tinggal").val();
    } else {
        var kabupaten_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kecamatan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            kabupaten_id: kabupaten_id
        },
        success: function (data) {
            $("#kecamatan_tinggal").html(data.list);
        }
    });
}

function getKelurahan(id) {
    if (id == null || id == 'undefined' || id == '') {
        var kecamatan_id = $("#kecamatan").val();
    } else {
        var kecamatan_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kelurahan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            kecamatan_id: kecamatan_id
        },
        success: function (data) {
            $("#kelurahan").html(data.list);
            $("#pj_kelurahan").html(data.list);
            $("#pj_kecamatan").val(kecamatan_id).select2();

        }
    });
}

function getKelurahanTinggal(id) {
    if (id == null || id == 'undefined' || id == '') {
        var kecamatan_id = $("#kecamatan_tinggal").val();
    } else {
        var kecamatan_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kelurahan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            kecamatan_id: kecamatan_id
        },
        success: function (data) {
            $("#kelurahan_tinggal").html(data.list);
        }
    });
}

function getKel() {
    var kel = $("#kelurahan").val();
    $("#pj_kelurahan").val(kel).select2();
}

// function form_print(pasien_id) {
//     $('#modal_print').modal('show');
//     console.log("id_pasien : " + pasien_id);
//     // $('button#printSurat').attr('onclick','printKartuBerobat('')');
//     $('button#printKartu').attr('onclick', 'printData(' + pasien_id + ',"print_kartu")');
//     $('button#printDetail').attr('onclick', 'printData(' + pasien_id + ',"print_data_pasien")');
//     $('button#printSurat').attr('onclick', 'printData(' + pasien_id + ',"form_input_surat")');
//     $('button#printKartuHilang').attr('onclick', 'printData(' + pasien_id + ',"print_hilang_kartu")');
//     $('button#printSticker').attr('onclick', 'printData(' + pasien_id + ',"print_sticker")');
//     $('button#printGelang').attr('onclick', 'printData(' + pasien_id + ',"print_gelang")');

// }


function printData(pendaftaran_id, view) {
    window.open(ci_baseurl + 'rekam_medis/pendaftaran/print_data/' + pendaftaran_id + '/' + view, 'Print_Surat', 'top=100,left=320,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}

function getJamDokter2() {
    var dokter_id = $("#dokter").val();
    var tgl_reservasi = $("#tgl_reservasi").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/reservasi/get_jam_dokter",
        type: 'GET',
        dataType: 'JSON',
        data: {
            dokter_id: dokter_id,
            tgl_reservasi: tgl_reservasi
        },
        success: function (data) {
            $("#jam_periksa").html(data.list);
            if ($("#jam").val() != "") {
                $("#jam_periksa").val($("#jam").val()).change();
            }
        }
    });
}
function getHotel(){
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_hotel_list",
        type: 'GET',
        dataType: 'JSON',
        data: {

        },
        success: function (data) {
            $("select[name='hotel']").html(data.list);
            $("select[name='hotel']").val($("#id_hotel").val());
        }
    });
}
function getAsuransi(value) {
    // alert(value)
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_asuransi_list",
        type: 'GET',
        dataType: 'JSON',
        data: {
            type : value
        },
        success: function (data) {
            $("select[name='asuransi1']").html(data.list);
        }
    });
}

function patientType(e) {
    console.log(e.value);
    if (e.value == 3) {
        $("#mandatory-email").show();
    } else {
        $("#mandatory-email").hide();
    }
}

// function viewKSPR(){
//     // var src = ci_baseurl + "rawatjalan/pasien_rj/periksa_pasienrj/1";
//     swal('josss');
//     $("#modal_view_kspr").removeAttr( "style" )
//     // $("#modal_view_kspr").removeAttr("style", true);
// }

function printDataPasien(pendaftaran_id) {
    window.open(ci_baseurl + 'rekam_medis/pendaftaran/print_pendaftaran/' + pendaftaran_id, 'Print_Surat', 'top=100,left=320,width=720,height=500,menubar=no,toolbar=no,statusbar=no,scrollbars=yes,resizable=no')
}