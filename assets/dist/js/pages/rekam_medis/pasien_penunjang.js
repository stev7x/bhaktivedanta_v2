var table_pasien_penunjang;
$(document).ready(function(){

     $('#modal_periksa_pasien_penunjang').on('hidden.bs.modal', function(){
        $(this).find('iframe').html("");
        $(this).find('iframe').attr("src", "");
    });

   table_pasien_penunjang = $('#table_list_pasien_penunjang').DataTable({
       "sDom": '<"top"f>rt<"bottom"ip><"clear">',
      "processing": true, //Feature control the processing indicator.
      "serverSide": true,
      "searching": false, //Feature control DataTables' server-side processing mode.
      // "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasien_penunjang/ajax_list_pasien_penunjang",
          "type": "GET",
      },
      //Set column definition initialisation properties.
    //   "columnDefs": [
    //   {
    //     "targets": [ -1,0,12 ], //last column
    //     "orderable": false //set not orderable
    //   }
    //   ]
    });
    table_pasien_penunjang.columns.adjust().draw();

});


function reloadTablePasien(){
    table_pasien_penunjang.columns.adjust().draw();
}

function periksapasien_penunjang(pendaftaran_id){
    // $('#modal_periksa_pasien_penunjang').openModal('toggle');
    var src = ci_baseurl + "rekam_medis/pasien_penunjang/periksa_pasien_penunjang/"+pendaftaran_id;
//    var src = ci_baseurl + "dashboard";
    $("#modal_periksa_penunjang iframe").attr({
        'src': src, 
        'height': 960,
        'width': 1024,
        'allowfullscreen':''
    });
}


function batalPeriksa(pendaftaran_id){
    var jsonVariable = {};
    jsonVariable["pendaftaran_id"] = pendaftaran_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_delda').val();
    

      swal({
          text: "Apakah anda yakin ingin batal periksa ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'tidak',
          confirmButtonText: 'Ya'
        }).then(function(){  
            $.ajax({
                  url: ci_baseurl + "rekam_medis/pasien_penunjang/do_delete_pasien_penunjang",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                  success: function(data){
                          var ret = data.success;
                          console.log("masuk if");
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              // $('.modal_notif').removeClass('red').addClass('green');
                              // $('#modal_card_message').html(data.messages);
                              // $('.notmodal_notifif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                              table_pasien_penunjang.columns.adjust().draw();
                              swal( 
                                'Berhasil!',
                                'Berhasil Dibatalkan.',
                                'success', 
                              )  
                         }else{
                          console.log("masuk else");

                              // $('.modal_notif').removeClass('green').addClass('red');
                              // $('#modal_card_message').html(data.messages);
                              // $('.modal_notif').show();
                              // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              //     $(".modal_notif").hide();
                              // });
                              swal( 
                                'Gagal!',
                                'Berhasil Dibatalkan.',
                                'error', 
                              )
                          }
                      }
                  });
          
        });
    
    
}

function cariPasien(){
    table_pasien_penunjang = $('#table_list_pasien_penunjang').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasien_penunjang/ajax_list_pasien_penunjang",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.no_masukpenunjang = $("#no_masukpenunjang").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.pasien_nama = $("#pasien_nama").val();
                d.pasien_alamat = $("#pasien_alamat").val();
            }
      },
      //Set column definition initialisation properties.
    });
    table_pasien_penunjang.columns.adjust().draw();
}



$(document).ready(function(){
    $('#change_option').on('change', function (e) {
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});
