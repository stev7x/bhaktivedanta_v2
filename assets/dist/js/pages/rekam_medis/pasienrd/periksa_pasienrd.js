var table_diagnosa_rd;
var table_tindakan_rd;
var table_resep_rd;
var table_diagnosa_rd_sudahpulang;
var table_daftar_diagnosa_rd;
var table_icd9;
var table_daftar_icd9;
$(document).ready(function(){
    $("#obat_id").val('0#');
    table_diagnosa_rd = $('#table_diagnosa_pasienrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_diagnosa_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
    });
    table_diagnosa_rd.columns.adjust().draw();


    table_icd9 = $('#table_icd9').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_icd9",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
    });
    table_icd9.columns.adjust().draw();

    table_daftar_icd9 = $('#table_list_icd9').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_daftar_icd_9",
            "type": "GET",
            "data": function (d) {
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        },
    });
    table_daftar_icd9.columns.adjust().draw();
    
    table_diagnosa_rd_sudahpulang = $('#table_diagnosa_pasienrd_sudahpulang').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,
  
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_diagnosa_pasien_sudahpulang",
            "type": "GET",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        },
        //Set column definition initialisation properties.
      });
      table_diagnosa_rd_sudahpulang.columns.adjust().draw();

    table_tindakan_rd = $('#table_tindakan_pasienrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_rd.columns.adjust().draw();

    table_resep_rd = $('#table_resep_pasienrd').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_resep_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_resep_rd.columns.adjust().draw();
    
   

    $('button#saveIcd9').click(function () {
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        // dokter = $("#dokter_periksa").val();
        //console.log(dokter);

        swal({
            text: "Apakah data yang anda masukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasienrd/do_create_icd_9_pasien",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateIcd9').serialize() + "&pendaftaran_id=" + pendaftaran_id + "&pasien_id=" + pasien_id,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deldiag]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deltind]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delres]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);
                    if (ret === true) {
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
                            $(".modal_notif").hide();
                        });
                        $('#fmCreateIcd9').find("input[type=text]").val("");
                        $('#fmCreateIcd9').find("select").prop('selectedIndex', 0);


                        table_icd9.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('.modal_notif').removeClass('green').addClass('red');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
                            $(".modal_notif").hide();
                        });
                    }
                }
            });
        });
    });
    $('button#editIcd9').click(function () {
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        // dokter = $("#dokter_periksa").val();
        //console.log(dokter);

        swal({
            text: "Apakah data yang anda masukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasienrd/do_update_icd_9_pasien",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateIcd9').serialize() + "&pendaftaran_id=" + pendaftaran_id + "&pasien_id=" + pasien_id,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deldiag]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deltind]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delres]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);
                    if (ret === true) {
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
                            $(".modal_notif").hide();
                        });
                        $('#fmCreateIcd9').find("input[type=text]").val("");
                        $('#fmCreateIcd9').find("select").prop('selectedIndex', 0);
                        $('#edit_icd9').hide();
                        $('#simpan_icd9').show();
                        table_icd9.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('.modal_notif').removeClass('green').addClass('red');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function () {
                            $(".modal_notif").hide();
                        });
                    }
                }
            });
        });
    });

    $('button#saveDiagnosa').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasienrd/do_create_diagnosa_pasien",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateDiagnosaPasien').serialize() + "&pendaftaran_id=" + pendaftaran_id + "&pasien_id=" + pasien_id + "&dokter=" + dokter,

                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deldiag]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deltind]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delres]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);
                    if (ret === true) {
                        $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                        $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex', 0);
                        table_diagnosa_rd.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                    }
                }
            });
        })
    });


    $('button#editDiagnosa').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        swal({
            text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Tidak',
            confirmButtonText: 'Ya!'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasienrd/do_update_diagnosa_pasien",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateDiagnosaPasien').serialize() + "&pendaftaran_id=" + pendaftaran_id + "&pasien_id=" + pasien_id + "&dokter=" + dokter,

                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deldiag]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deltind]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delres]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);
                    if (ret === true) {
                        $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                        $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex', 0);
                        $('#edit_diagnosa').hide();
                        $('#simpan_diagnosa').show();
                        table_diagnosa_rd.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                        )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function () {
                            $("#modal_notif").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                    }
                }
            });
        })
    });

        

    $('button#saveResep').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        if ($("#obat_id")=='' || $("#obat_id")==null || $("#obat_id")=='0' || $("#obat_id")=='0#') {
                alert('Silahkan pilih Obat terlebih dahulu');
        }else{
                jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
                if(r){
                    $.ajax({
                        url: ci_baseurl + "rekam_medis/pasienrd/do_create_resep_pasien",
                        type: 'POST',
                        dataType: 'JSON',
                        data: $('form#fmCreateResepPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                        success: function(data) {
                            var ret = data.success;
                           $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                                $('#fmCreateResepPasien').find("input[type=text]").val("");
                                $('#fmCreateResepPasien').find("input[type=number]").val("");
                                $('#fmCreateResepPasien').find("select").prop('selectedIndex',0);
                            table_resep_rd.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                                swal(
                                    'Gagal!',
                                    'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                    'error'
                                  )
                            }
                        }
                    });
                }
            });
        }
    });
    $('button#saveTindakan').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
            if(r){
                $.ajax({
                    url: ci_baseurl + "rekam_medis/pasienrd/do_create_tindakan_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                            $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',0);
                        table_tindakan_rd.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success'
                          )
                        } else {
                            $('.modal_notif').removeClass('green').addClass('red');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".modal_notif").hide();
                            });
                            swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong !',
                                'error'
                              )
                        }
                    }
                });
            }
        });
    });
});

function getTindakan(){
    var kelaspelayanan_id = $("#kelas_pelayanan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasienrd/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kelaspelayanan_id:kelaspelayanan_id},
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    });
}

function getTarifTindakan(){
    var tariftindakan_id = $("#tindakan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasienrd/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON',
        data: {tariftindakan_id:tariftindakan_id},
        success: function(data) {
            $("#harga_tindakan").val(data.list.harga_tindakan);
            $("#harga_cyto").val(data.list.cyto_tindakan);
        }
    });
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();
    
    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $("#subtotal").val(subtotal);
    $("#totalharga").val(total);
}

function dialogDiagnosa() {
    // $('#modal_list_obat').openModal('toggle');
    if (table_daftar_diagnosa_rd) {
        table_daftar_diagnosa_rd.destroy();
    }
    table_daftar_diagnosa_rd = $('#table_diagnosa_rd').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "searching": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_diagnosa",
            "type": "GET",
            "data": function (d) {
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }

        },
        "columnDefs": [{
            "targets": [-1],
            "orderable": false,
        },],
    });
    table_daftar_diagnosa_rd.columns.adjust().draw();
}

function dialogIcd9() {
    // $('#modal_list_obat').openModal('toggle');
    if (table_daftar_icd9) {
        table_daftar_icd9.destroy();
    }
    table_daftar_icd9 = $('#table_list_icd_9').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "searching": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienrd/ajax_list_daftar_icd_9",
            "type": "GET",
            "data": function (d) {
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }

        },
        "columnDefs": [{
            "targets": [-1],
            "orderable": false,
        },],
    });
    table_daftar_icd9.columns.adjust().draw();
}

function pilihIcd9(oid) {

    if (oid != "") {

        $.ajax({
            url: ci_baseurl + "rekam_medis/pasienrd/ajax_get_icd9_by_id",
            type: 'get',
            dataType: 'json',
            data: { diagnosa_id: oid },
            success: function (data) {
                var ret = data.success;
                console.log("sukses masuk diagnosa pasien");
                if (ret == true) {
                    console.log("Masuk retun true");
                    $('#kode_icd_9_cm').val(data.data['kode_diagnosa']);
                    $('#nama_icd_9_cm').val(data.data['nama_diagnosa']);
                    $('#modal_icd9').modal('hide')
                    console.log("masuk close");
                } else {

                    // $("#harga_netto").val('');
                    // $("#current_stok").val('');
                    // $("#obat_id").val('');
                    console.log("masuk else");
                }
            }
        });
    } else {
        // $('#modal_diagnosaa').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Diagnosa ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}


function pilihDiagnosa(oid) {

    if (oid != "") {

        $.ajax({
            url: ci_baseurl + "rekam_medis/pasienrd/ajax_get_diagnosa_by_id",
            type: 'get',
            dataType: 'json',
            data: { diagnosa_id: oid },
            success: function (data) {
                var ret = data.success;
                console.log("sukses masuk diagnosa pasien");
                if (ret == true) {
                    console.log("Masuk retun true");
                    $('#kode_diagnosa').val(data.data['kode_diagnosa']);
                    $('#nama_diagnosa').val(data.data['nama_diagnosa']);
                    $('#modal_diagnosa').modal('hide')
                    console.log("masuk close");
                } else {

                    // $("#harga_netto").val('');
                    // $("#current_stok").val('');
                    // $("#obat_id").val('');
                    console.log("masuk else");
                }
            }
        });
    } else {
        // $('#modal_diagnosaa').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Diagnosa ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}


function editDiagnosa(oid) {

    if (oid != "") {

        $.ajax({
            url     : ci_baseurl + "rekam_medis/pasienrd/ajax_get_diagnosa_pasien_by_id",
            type    : 'get',
            dataType: 'json',
            data    : { diagnosapasien_id: oid },
            success : function (data) {
                var ret = data.success;
                console.log("sukses masuk diagnosa pasien");
                if (ret == true) {
                    console.log("Masuk retun true");
                    $('#diagnosapasien_id').val(data.data['diagnosapasien_id']);
                    $('#kode_diagnosa').val(data.data['kode_diagnosa']);
                    $('#nama_diagnosa').val(data.data['nama_diagnosa']);
                    $('#jenis_diagnosa').val(data.data['jenis_diagnosa']);
                    $('#edit_diagnosa').show();
                    $('#simpan_diagnosa').hide();
                    console.log("masuk close");
                } else {

                    // $("#harga_netto").val('');
                    // $("#current_stok").val('');
                    // $("#obat_id").val('');
                    console.log("masuk else");
                }
            }
        });
    } else {
        // $('#modal_diagnosaa').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Diagnosa ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}


function editIcd9(oid) {

    if (oid != "") {

        $.ajax({
            url: ci_baseurl + "rekam_medis/pasienrd/ajax_get_tindakan_icd_9_by_id",
            type    : 'get',
            dataType: 'json',
            data    : {tindakan_icd_id:oid },
            success : function (data) {
                var ret = data.success;
                console.log("sukses masuk diagnosa pasien");
                if (ret == true) {
                    console.log("Masuk retun true");
                    $('#tindakan_icd_id').val(data.data['tindakan_icd_id']);
                    $('#kode_icd_9_cm').val(data.data['kode_icd_9_cm']);
                    $('#nama_icd_9_cm').val(data.data['nama_tindakan']);
                    $('#edit_icd9').show();
                    $('#simpan_icd9').hide();
                    console.log("masuk sini");
                } else {
                    console.log("masuk else");
                }
            }
        });
    } else {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Diagnosa ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}


function hapusDiagnosa(diagnosapasien_id) {
    var jsonVariable = {};
    jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_deldiag').val();
    if (diagnosapasien_id) {
        console.log("masuk diagnosa");

        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasienrd/do_delete_diagnosa",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                success: function (data) {
                    var ret = data.success;
                    console.log("masuk if");
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deldiag]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deltind]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delres]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);
                    if (ret === true) {
                        // $('.modal_notif').removeClass('red').addClass('green');
                        // $('#modal_card_message').html(data.messages);
                        // $('.notmodal_notifif').show();
                        // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                        //     $(".modal_notif").hide();
                        // });
                        table_diagnosa_rd.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                        )
                    } else {

                        console.log("masuk else");
                        swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                        )
                    }
                }
            });

        });
    }
    else {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}


function hapusIcd9(tindakan_icd_id) {
    var jsonVariable = {};
    jsonVariable["tindakan_icd_id"] = tindakan_icd_id;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_deldiag').val();
    if (tindakan_icd_id) {
        console.log("masuk diagnosa");

        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
        }).then(function () {
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasienrd/do_delete_icd9",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                success: function (data) {
                    var ret = data.success;
                    console.log("masuk if");
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deldiag]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_deltind]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delres]').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);
                    if (ret === true) {
                        // $('.modal_notif').removeClass('red').addClass('green');
                        // $('#modal_card_message').html(data.messages);
                        // $('.notmodal_notifif').show();
                        // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                        //     $(".modal_notif").hide();
                        // });
                        table_icd9.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Dihapus.',
                            'success',
                        )
                    } else {

                        console.log("masuk else");
                        swal(
                            'Gagal!',
                            'Data Gagal Dihapus.',
                            'error',
                        )
                    }
                }
            });

        });
    }
    else {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}


