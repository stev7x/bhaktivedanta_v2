var table_tindakan_rj;

$(document).ready(function(){
    getTindakan();   
    table_tindakan_rj = $('#table_tindakan_pasien_penunjang').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasien_penunjang/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });  
    table_tindakan_rj.columns.adjust().draw();

    // $('.datepicker_tindakan').pickadate({
    //     selectMonths: true, // Creates a dropdown to control month
    //     selectYears: 5, // Creates a dropdown of 15 years to control year
    //     format: 'dd-mm-yyyy',
    //     max: true  
    // });     

    $('button#saveTindakan').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        if (dokter=='' || dokter== null || dokter=='undifined') {
            alert('Sorry! Please chose dokter');
        }else{
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,   
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                url: ci_baseurl + "rekam_medis/pasien_penunjang/do_create_tindakan_pasien",
                type: 'POST',
                dataType: 'JSON',
                data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                success: function(data) {
                    var ret = data.success;
                    $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                   $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                   $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                    if(ret === true) {
                        $('.modal_notif').removeClass('red').addClass('green');
                        $('#modal_card_message').html(data.messages);
                        $('.modal_notif').show();
                        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                        });
                        $('#fmCreateTindakanPasien').find("input[type=text]").val("");
                        $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',0);
                        $('#tindakan').select2("val"," ");
                    table_tindakan_rj.columns.adjust().draw();
                    swal(
                        'Berhasil!',
                        'Data Berhasil Disimpan.',
                        'success'
                      )
                    } else {
                        $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                        $('#card_message').html(data.messages);
                        $('#modal_notif').show();
                        $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif").hide();
                        });
                        $("html, body").animate({ scrollTop: 100 }, "fast");
                        // swal(
                        //     'Gagal!',
                        //     'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                        //     'error'
                        //   )
                    }
                }
            });
            
         
        })
    }
    });


    // $('button#saveTindakan').click(function(){
    //     pendaftaran_id = $("#pendaftaran_id").val();
    //     pasien_id = $("#pasien_id").val();
    //     dokter = $("#dokter_periksa").val();
    //     if (dokter=='' || dokter== null || dokter=='undifined') {
    //         alert('Sorry! Please chose dokter');
    //     }else{
    //     jConfirm("Are you sure want to submit ?","Ok","Cancel", function(r){
    //         if(r){
    //             $.ajax({
    //                 url: ci_baseurl + "rekam_medis/pasien_penunjang/do_create_tindakan_pasien",
    //                 type: 'POST',
    //                 dataType: 'JSON',
    //                 data: $('form#fmCreateTindakanPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
    //                 success: function(data) {
    //                     var ret = data.success;
    //                     $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
    //                    $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
    //                    $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
    //                     if(ret === true) {
    //                         $('.modal_notif').removeClass('red').addClass('green');
    //                         $('#modal_card_message').html(data.messages);
    //                         $('.modal_notif').show();
    //                         $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                             $(".modal_notif").hide();
    //                         });
    //                         $('#fmCreateTindakanPasien').find("input[type=text]").val("");
    //                         $('#fmCreateTindakanPasien').find("select").prop('selectedIndex',0);
    //                     table_tindakan_rj.columns.adjust().draw();
    //                     } else {
    //                         $('.modal_notif').removeClass('green').addClass('red');
    //                         $('#modal_card_message').html(data.messages);
    //                         $('.modal_notif').show();
    //                         $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
    //                             $(".modal_notif").hide();
    //                         });
    //                     }
    //                 }
    //             });
    //         }
    //     });
    //   }
    // });
});

function getTindakan(){
    var poliruangan = $("#poliruangan").val();
    $.ajax({   
        url: ci_baseurl + "rekam_medis/pasien_penunjang/get_tindakan_list",
        type: 'GET',
        dataType: 'JSON',    
        data: {poliruangan:poliruangan}, 
        success: function(data) {
            $("#tindakan").html(data.list);
        }
    }); 
} 
 
function getTarifTindakan(){
    var daftartindakan_id = $("#tindakan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pasien_penunjang/get_tarif_tindakan",
        type: 'GET',
        dataType: 'JSON', 
        data: {daftartindakan_id:daftartindakan_id},
        success: function(data) {  
            $("#harga_tindakan").val(data.list.harga_tindakan);
            $("#harga_cyto").val(data.list.cyto_tindakan);
        }
    });
}

function hitungHarga(){
    var tarif_tindakan = $("#harga_tindakan").val();
    var tarif_cyto = $("#harga_cyto").val();
    var jml_tindakan = $("#jml_tindakan").val();
    var is_cyto = $("#is_cyto").val();

    var subtotal = parseInt(tarif_tindakan) * parseInt(jml_tindakan);
    var total = 0;
    if(is_cyto == '1'){
        total = subtotal+parseInt(tarif_cyto);
    }else{
        total = subtotal;
    }
    subtotal = (!isNaN(subtotal)) ? subtotal : 0;
    total = (!isNaN(total)) ? total : 0;
    $("#subtotal").val(subtotal);
    $("#totalharga").val(total);
}

function hapusTindakan(tindakanpasien_penunjang_id){
    var jsonVariable = {};
    jsonVariable["tindakanpasien_penunjang_id"] = tindakanpasien_penunjang_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deltind').val();
    if(tindakanpasien_penunjang_id){
        swal({
            text: "Apakah anda yakin ingin menghapus data ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya' 
            }).then(function(){  
                $.ajax({
                    url: ci_baseurl + "rekam_medis/pasien_penunjang/do_delete_tindakan",
                    type: 'post',
                    dataType: 'json',
                    data: jsonVariable,
                        success: function(data) {
                            var ret = data.success;
                            $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                            $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                            if(ret === true) {
                                $('.modal_notif').removeClass('red').addClass('green');
                                $('#modal_card_message').html(data.messages);
                                $('.notmodal_notifif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                            table_tindakan_rj.columns.adjust().draw();
                            swal( 
                                'Berhasil!',
                                'Data Berhasil Dihapus.', 
                                'success',    
                            ) 
                            } else {
                                $('.modal_notif').removeClass('green').addClass('red');
                                $('#modal_card_message').html(data.messages);
                                $('.modal_notif').show();
                                $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                    $(".modal_notif").hide();
                                });
                                swal( 
                                    'Berhasil!',
                                    'Data Berhasil Dihapus.', 
                                    'error',    
                                ) 
                            }
                        }
                    });
                
        });
    }
    else
    {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }
}


