var table_pasien;
$(document).ready(function(){
    $('#cari_no_rekam_medis').keyup(function() {  
        var val_pasien = $('#cari_no_rekam_medis').val();
        $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran_operasi/autocomplete_pasien",
            type: 'get',
            async: false,
            dataType: 'json',
            data: {val_pasien: val_pasien},
            success: function(data) { /* console.log(data); */
                var ret = data.success;
                if(ret == true) {
                    // console.log(data.data);
                    $(".autocomplete").autocomplete({
                        source: data.data,
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("position: absolute");
                            $(".ui-autocomplete").css("top: 0");
                            $(".ui-autocomplete").css("left: 0");
                            $(".ui-autocomplete").css("cursor: default");
                            $(".ui-autocomplete").css("z-index","999999");
                            $(".ui-autocomplete").css("font-weight : bold");
                        },
                        select: function (e, ui) {
                            $('#pasien_id').val(ui.item.id);
                            $('#type_id').val(ui.item.type_id); 
                            $('#no_identitas').val(ui.item.no_identitas);
                            $('#no_rm').val(ui.item.no_rekam_medis);
                            $('#nama_pasien').val(ui.item.pasien_nama);
                            $('#tempat_lahir').val(ui.item.tempat_lahir);
                            $('#tgl_lahir').val(ui.item.tanggal_lahir);
                            setUmur();//$('#umur').val(ui.item.umur);
                            $('#jenis_kelamin').val(ui.item.jenis_kelamin);
                            $('#status_kawin').val(ui.item.status_kawin);
                            $('#golongan_darah').val(ui.item.gol_darah);
                            if(ui.item.rhesus == '1'){
                                $('#rhesus1').prop('checked',true);
                            }else{
                                $('#rhesus0').prop('checked',true);
                            }
                            $('#alamat').val(ui.item.alamat);
                            $("#propinsi").val(ui.item.propinsi);
                            getDaerahAll(ui.item.propinsi, ui.item.kabupaten, ui.item.kecamatan, ui.item.kelurahan);
                            $('#no_tlp_rmh').val(ui.item.tlp_rumah);
                            $('#no_mobile').val(ui.item.tlp_selular);
                            $('#pekerjaan').val(ui.item.pekerjaan);
                            $('#warga_negara').val(ui.item.warga_negara);
                            $('#agama').val(ui.item.agama);
                            $('#suku').val(ui.item.suku);
                            var bhs = ui.item.bahasa;
                            $.each(bhs.split(","), function(i,e){
                                $("#bahasa option[value='" + e + "']").prop("selected", true);
                            });
                            $('#pendidikan').val(ui.item.pendidikan);
                            $('#anakke').val(ui.item.anak_ke);
                            $('#namakeluarga').val(ui.item.nama_keluarga);
                            $('#hubungankeluarga').val(ui.item.status_hubungan);
                            $('#alergi').val(ui.item.alergi);
                            $('#status_pasien').val(ui.item.status);
                            $('#label_alergi').addClass('active');
                            $('#label_alamat').addClass('active');
                        }
                    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                      return $( "<li>" )
                        .append( "<a><b><font size=2>" + item.value + "</font></b><br><font size=1>" + item.alamat + "</font></a>" )
                        .appendTo( ul );
                    };
                }else{
                  $("#id_m_pasien").val('0#');
                }       
            }
        });
    });

    $('button#savePendaftaran').click(function(){

        swal({

            text: "Apakah data yang dimasukan sudah benar ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then(function () {
 
            $.ajax({
            url: ci_baseurl + "rekam_medis/pendaftaran_operasi/do_create_pendaftaran_operasi", 
            type: 'POST', 
            dataType: 'JSON',  
            data: $('form#fmCreatePendaftaran').serialize(),
            success: function(data) { 
                var ret = data.success;
                $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                if(ret === true){
                    $('#modal_notif').removeClass('alert-danger').addClass('alert-success');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif").hide();
                    });
                    console.log("i am here true : "+data.pendaftaran_id);
                    $('button#savePendaftaran').attr('hidden','true');
                    $('#reset').removeAttr('hidden'); 
                    $('button#printPendaftaran').removeAttr('hidden');
                    $('button#printPendaftaran').attr('onclick','print('+data.pendaftaran_id+')');
                    $('button#printKartu').removeAttr('hidden');
                    // $('button#printKartu').attr('onclick','printKartuBerobat('+data.pendaftaran_id+')');
                    // print(data.pendaftaran_id);
                    // printKartuBerobat(data.pendaftaran_id);
                    // $('#fmCreatePendaftaran').find("input[type=text]").val("");
                    // $('#fmCreatePendaftaran').find("select").prop('selectedIndex',"");
                    // $('#fmCreatePendaftaran').find("textarea").empty();
                    $("html, body").animate({ scrollTop: 100 }, "fast");
                } else {
                    console.log("masuk false");
                    $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                    $('#card_message').html(data.messages);
                    $('#modal_notif').show();
                    $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                        $("#modal_notif").hide();
                    });
                    $("html, body").animate({ scrollTop: 100 }, "fast");
                }
            }
        });
            

        });
    });
});

function cekTypeBayar(){
    var type = $("#byr_type_bayar").val();
    if(type == "Pribadi"){
        $("#byr_instansi").val('');
        $("#byr_instansi").attr('readonly','true');
        $("#byr_namaasuransi").val('');
        $("#byr_namaasuransi").attr('readonly','true');
        $("#byr_nomorasuransi").val('');
        $("#byr_nomorasuransi").attr('readonly','true');
    }else{
        $("#byr_instansi").removeAttr('readonly');
        $("#byr_namaasuransi").removeAttr('readonly');
        $("#byr_nomorasuransi").removeAttr('readonly');
    }
}
function getDaerahAll(propinsi_id, kabupaten_id, kecamatan_id, kelurahan_id){
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_daerah_list",
        type: 'GET',
        dataType: 'JSON',
        data: {propinsi_id:propinsi_id, kabupaten_id:kabupaten_id, kecamatan_id:kecamatan_id, kelurahan_id:kelurahan_id},
        success: function(data) {
            $("#kabupaten").html(data.list_kabupaten);
            $("#kecamatan").html(data.list_kecamatan);
            $("#kelurahan").html(data.list_kelurahan);
        }
    });
}

function getKabupaten(id){
    if (id==null || id =='undifined' || id =='') {
        var propinsi_id = $("#propinsi").val();
    }else{
        var propinsi_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: {propinsi_id:propinsi_id},
        success: function(data) {
            $("#kabupaten").html(data.list);
        }
    });
}
function getKabupatenTinggal(id){
    if (id==null || id =='undifined' || id =='') {
        var propinsi_id = $("#provinsi_tinggal").val();
    }else{
        var propinsi_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: {propinsi_id:propinsi_id},
        success: function(data) {
            $("#kabupaten_tinggal").html(data.list);

        }
    });
}

function getKecamatan(id){
    if (id==null || id =='undifined' || id =='') {
        var kabupaten_id = $("#kabupaten").val();
    }else{
        var kabupaten_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kecamatan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kabupaten_id:kabupaten_id},
        success: function(data) {
            $("#kecamatan").html(data.list);
        }
    });
}
function getKecamatanTinggal(id){
    if (id==null || id =='undefined' || id =='') {
        var kabupaten_id = $("#kabupaten_tinggal").val();
    }else{
        var kabupaten_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kecamatan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kabupaten_id:kabupaten_id},
        success: function(data) {
            $("#kecamatan_tinggal").html(data.list);
        }
    });
}

function getKelurahan(id){
    if (id==null || id =='undifined' || id =='') {
        var kecamatan_id = $("#kecamatan").val();
    }else{
        var kecamatan_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kelurahan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kecamatan_id:kecamatan_id},
        success: function(data) {
            $("#kelurahan").html(data.list);
        }
    });
}
function getKelurahanTinggal(id){
    if (id==null || id =='undefined' || id =='') {
        var kecamatan_id = $("#kecamatan_tinggal").val();
    }else{
        var kecamatan_id = id;
    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/get_kelurahan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kecamatan_id:kecamatan_id},
        success: function(data) {
            $("#kelurahan_tinggal").html(data.list);
        }
    });
}

function cekPJRJ(instalasi_id){
    var nama_pasien = $('#nama_pasien').val();
    if(instalasi_id == '1'){
        $('#pj_nama').val(nama_pasien);
    }else{
        $('#pj_nama').val('');
    }
}

function getRuangan(){
    var paket_operasi = $("#paket_operasi").val();
    //cekPJRJ(instalasi_id);
//    if(instalasi_id == '3'){
//        $('#rowKamar').removeAttr('style');
//        $('#rowDokterAnastesi').removeAttr('style');
//        $('#rowPerawat').removeAttr('style');
//    }else{
//        $('#rowKamar').attr('style','display:none;');
//        $('#rowDokterAnastesi').attr('style','display:none;');
//        $('#rowPerawat').attr('style','display:none;');
//    }
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_ruangan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {paket_operasi:paket_operasi},
        success: function(data) {
            $("#poli_ruangan").html(data.list);
        }
    });
}

//function getKelasPoliruangan(){
//    var poliruangan_id = $("#poli_ruangan").val();
//    $.ajax({
//        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kelasruangan_list",
//        type: 'GET',
//        dataType: 'JSON',
//        data: {poliruangan_id:poliruangan_id},
//        success: function(data) {
//            $("#kelas_pelayanan").html(data.list);
//        }
//    });
//    getDokterRuangan();
//}

function getKamar(){
    var poliruangan_id = $("#poli_ruangan").val();
    var paket_operasi = $("#paket_operasi").val();
    
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kamarruangan",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id, paketoperasi_id:paket_operasi},
        success: function(data) {
            $("#kamarruangan").html(data.list);
        }
    });
    getDokterRuangan();
}

function getDokterRuangan(){
    var poliruangan_id = $("#poli_ruangan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_dokter_list",
        type: 'GET',
        dataType: 'JSON',
        data: {poliruangan_id:poliruangan_id},
        success: function(data) {
            $("#dokter").html(data.list);
        }
    });
}

function getKabupatenPj(){
    var propinsi_id = $("#pj_propinsi").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kabupaten_list",
        type: 'GET',
        dataType: 'JSON',
        data: {propinsi_id:propinsi_id},
        success: function(data) {
            $("#pj_kabupaten").html(data.list);
        }
    });
}

function getKecamatanPj(){
    var kabupaten_id = $("#pj_kabupaten").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kecamatan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kabupaten_id:kabupaten_id},
        success: function(data) {
            $("#pj_kecamatan").html(data.list);
        }
    });
}

function getKelurahanPj(){
    var kecamatan_id = $("#pj_kecamatan").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/get_kelurahan_list",
        type: 'GET',
        dataType: 'JSON',
        data: {kecamatan_id:kecamatan_id},
        success: function(data) {
            $("#pj_kelurahan").html(data.list);
        }
    });
}

function setUmur(){
    var tgl_lahir = $("#tgl_lahir").val();
    $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran_operasi/hitung_umur",
        type: 'GET',
        dataType: 'JSON',
        data: {tgl_lahir:tgl_lahir},
        success: function(data) {
            $("#umur").val(data.umur);
        }
    });
}
var table_ortu;  
    function dialogOrtu(){
        if(table_ortu){       
            table_ortu.destroy();
        }
        table_pasien = $('#table_list_ortu').DataTable({
            "sDom": '<"top"f>rt<"bottom"ip><"clear">',
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "retrieve" : true,        
            // "paging": false,

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": ci_baseurl + "rekam_medis/pendaftaran/ajax_list_ortu",
                "type": "GET"   
            },
            //Set column definition initialisation properties.
            "columnDefs": [
            {
                "targets": [ -1,0 ], //last column
                "orderable": false //set not orderable
            }
            ] 
        });
        table_ortu.columns.adjust().draw();
    } 
 
function dialogPasien(){
    // $('#modal_list_pasien').openModal('toggle');
    if(table_pasien){
        table_pasien.destroy();
    }
    table_pasien = $('#table_list_pasien').DataTable({ 
        "sDom": '<"top"f>rt<"bottom"ip><"clear">',
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
//        "paging": false, 

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pendaftaran_operasi/ajax_list_pasien",
            "type": "GET"
        },
        //Set column definition initialisation properties.
    });
    table_pasien.columns.adjust().draw();
}  
  
function pilihOrtu(id_m_pasien){ 
    if(id_m_pasien){
        $.ajax({
        url: ci_baseurl + "rekam_medis/pendaftaran/ajax_get_pasien_by_id",
        type: 'get',
        dataType: 'json',
        data: {id_m_pasien:id_m_pasien},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#pasien_id').val(data.data.pasien_id);
                    $('#type_id').val(data.data.type_identitas);
                    $('#no_identitas').val(data.data.no_identitas);
                    $('#nama_orangtua').val(data.data.pasien_nama);
                    $('#tgl_lahir_ortu').val(data.tgl_lahir); 
                    setUmur();$('#umur_ortu').val(data.data.umur);
                    $('#alamat').val(data.data.pasien_alamat); 
                    $('#alamat_domisili').val(data.data.pasien_alamat_domisili);
                    $('#label_alamat').addClass('active');
                    $('#propinsi').val(data.data.propinsi_id);
                    $('#propinsi').select2();
                    getDaerahAll(data.data.propinsi_id, data.data.kabupaten_id, data.data.kecamatan_id, data.data.kelurahan_id);
                    $('#kabupaten').val(data.data.kabupaten_id);      
                    $("#kabupaten option[value='']").val(data.data.kabupaten_id);
                    $('#kabupaten').select2(); 
                    $('#kecamatan').val(data.data.kecamatan_id);
                    $("#kecamatan option[value='']").val(data.data.kecamatan_id);
                    $('#kecamatan').select2(); 
                    $("#kelurahan option[value='']").val(data.data.kelurahan_id);
                    $('#kelurahan').select2();   
                    $('#no_tlp_rmh').val(data.data.tlp_rumah);
                    $('#no_mobile').val(data.data.tlp_selular);
                    $('#pekerjaan').val(data.data.pekerjaan_id);      
                    $('#pekerjaan').select2();               
                    $('#warga_negara').val(data.data.warga_negara);
                    $('#warga_negara').select2();     
                    $('#agama').val(data.data.agama_id);    
                    $('#suku').val(data.data.suku);
                    $('#pendidikan').val(data.data.pendidikan_id);

                    // data penanggung jawab
                    $('#pj_nama').val(data.data.pasien_nama);
                    $('#pj_no_tlp_rmh').val(data.data.tlp_rumah);
                    $('#pj_no_mobile').val(data.data.tlp_selular);
                    $('#pj_alamat').val(data.data.pasien_alamat); 
                    $('#pj_propinsi').val(data.data.propinsi_id); 
                    $('#pj_propinsi').select2();  
                    getDaerahAll(data.data.propinsi_id, data.data.kabupaten_id, data.data.kecamatan_id, data.data.kelurahan_id);
                    $("#pj_kabupaten option[value='"+data.data.kabupaten_id+"']").val(data.data.kabupaten_id);  
                    // $('#pj_kabupaten').select2();            
                    $('#pj_kecamatan').val(data.data.kecamatan_id);  
                    $('#pj_kecamatan').select2(); 
                    $('#pj_kelurahan').val(data.data.kelurahan_id); 
                    $('#pj_kelurahan').select2();       

                    $("#modal_list_ortu").modal('hide');  

                    // $('#modal_list_pasien').closeModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
        $(".modal_notif").hide();
        });
    }
}
 
function pilihPasien(id_m_pasien){
    if(id_m_pasien){
        $.ajax({ 
        url: ci_baseurl + "rekam_medis/pendaftaran/ajax_get_pasien_by_id",
        type: 'get',
        dataType: 'json',
        data: {id_m_pasien:id_m_pasien},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#pasien_id').val(data.data.pasien_id);
                    $('#type_id').val(data.data.type_identitas);
                    // $('#type_id').material_select();
                    $('#no_identitas').val(data.data.no_identitas);
                    $('#no_rm').val(data.data.no_rekam_medis);
                    $('#no_rm').removeAttr("value");
                    $('#no_rm').attr("value",data.data.no_rekam_medis);
                    $('#nama_pasien').val(data.data.pasien_nama); 
                    $('#nama_panggilan').val(data.data.nama_panggilan);
                    $('#tempat_lahir').val(data.data.tempat_lahir); 
                    $('#tgl_lahir').val(data.tgl_lahir);
                    setUmur();//$('#umur').val(data.data.umur);

                    $('#jenis_kelamin').val(data.data.jenis_kelamin);
                    // $('#type_id').material_select();
                    $('#status_kawin').val(data.data.status_kawin);
                    // $('#type_id').material_select();
                    $('#golongan_darah').val(data.data.gol_darah);

                    $('#alamat').val(data.data.pasien_alamat);
                    $('#alamat_domisili').val(data.data.pasien_alamat_domisili);
                    $('#label_alamat').addClass('active');
                    $('#propinsi').val(data.data.propinsi_id);
                    $('#propinsi').select2();
                    getDaerahAll(data.data.propinsi_id, data.data.kabupaten_id, data.data.kecamatan_id, data.data.kelurahan_id);
                    $('#kabupaten').val(data.data.kabupaten_id);
                    $("#kabupaten option[value='1']").val(data.data.kabupaten_id);
                    $('#kabupaten').select2();
                    $('#kecamatan').val(data.data.kecamatan_id);
                    $('#kecamatan').select2();
                    $("#kelurahan option[value='']").val(data.data.kelurahan_id);
                    $('#kelurahan').select2();
                    $('#no_tlp_rmh').val(data.data.tlp_rumah);
                    $('#no_mobile').val(data.data.tlp_selular);
                    $('#pekerjaan').val(data.data.pekerjaan);
                    $('#warga_negara').val(data.data.warga_negara);
                    $('#agama').val(data.data.agama);
                    // $('#agama').material_select();
                    $('#suku').val(data.data.suku);
                    // $('#suku').material_select();
                    // var bhs = data.data.bahasa;
                    // $.each(bhs.split(","), function(i,e){
                    //     $("#bahasa option[value='" + e + "']").prop("selected", true);
                    // });
                    $('#pendidikan').val(data.data.pendidikan);
                    $('#anakke').val(data.data.anak_ke);
                    $('#namakeluarga').val(data.data.nama_keluarga);
                    $('#hubungankeluarga').val(data.data.status_hubungan);
                    // $('#hubungankeluarga').material_select();
                    $('#alergi').val(data.data.alergi);
                    $('#status_pasien').val(data.status_pasien);
                    $('#label_alergi').addClass('active');
                    $('#label_alergi').addClass('active');
                    $("#modal_list_pasien").modal('hide');  

                    // $('#modal_list_pasien').closeModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
        $(".modal_notif").hide();
        });
    }
}

$(document).ready(function(){ 
    var temp_alamat; 
    $('#alamat').on('keyup', function(e){
        temp_alamat = this.value;
        $('#alamat_domisili').val(temp_alamat);
    });

    $("#cek_alamat").change(function() {
        if(this.checked) {
            $('#alamat_domisili').removeAttr('readonly');
            $('#alamat_domisili').val('');
        }else{
            $('#alamat_domisili').attr('readonly','TRUE');
            $('#alamat_domisili').val(temp_alamat);
              
        }
    });

});

function cekStatusKawin(){
        var type = $(".status_kawin").val();
        var t = $('.Suami_istri');    
        if(type == "Kawin"){
                console.log('masuk');                   
                t.removeClass('hide').show(500);  
                // t.show().fadeIn(2000);                  

        }else{
            console.log('tidak');      
            t.hide(300);              
        }
    }