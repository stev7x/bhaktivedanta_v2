var table_diagnosa_ri;
var table_diagnosa_ri_sudahpulang;
var table_tindakan_ri;
var table_rs_rujukan;
var table_daftar_diagnosa_ri;
$(document).ready(function(){
    table_diagnosa_ri = $('#table_diagnosa_pasienri').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienri/ajax_list_diagnosa_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      },
      //Set column definition initialisation properties.
    });
    table_diagnosa_ri.columns.adjust().draw();

    table_diagnosa_ri_sudahpulang = $('#table_diagnosa_pasienri_sudahpulang').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "paging": false,
        "sDom": '<"top"l>rt<"bottom"p><"clear">',
        "ordering": false,
  
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienri/ajax_list_diagnosa_pasien_sudahpulang",
            "type": "GET",
            "data" : function(d){
                d.pendaftaran_id = $("#pendaftaran_id").val();
            }
        },
        //Set column definition initialisation properties.
      });
      table_diagnosa_ri_sudahpulang.columns.adjust().draw();


    table_tindakan_ri = $('#table_tindakan_pasienri').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      //"scrollX": true,
      "paging": false,
      "sDom": '<"top"l>rt<"bottom"p><"clear">',
      "ordering": false,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "rekam_medis/pasienri/ajax_list_tindakan_pasien",
          "type": "GET",
          "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
      }
    });
    table_tindakan_ri.columns.adjust().draw();

    $('button#saveDiagnosa').click(function(){
        pendaftaran_id = $("#pendaftaran_id").val();
        pasien_id = $("#pasien_id").val();
        dokter = $("#dokter_periksa").val();
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "rekam_medis/pasienri/do_create_diagnosa_pasien",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateDiagnosaPasien').serialize()+ "&pendaftaran_id=" +pendaftaran_id+ "&pasien_id=" +pasien_id+ "&dokter=" +dokter,
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $('.modal_notif').removeClass('red').addClass('green');
                            $('#modal_card_message').html(data.messages);
                            $('.modal_notif').show();
                            $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            $(".modal_notif").hide();
                            });
                            $('#fmCreateDiagnosaPasien').find("input[type=text]").val("");
                            $('#fmCreateDiagnosaPasien').find("select").prop('selectedIndex',0);
                        table_diagnosa_ri.columns.adjust().draw();
                        swal(
                            'Berhasil!',
                            'Data Berhasil Disimpan.',
                            'success',
                          )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, body").animate({ scrollTop: 100 }, "fast");
                        //   swal(
                        //     'Gagal!',
                        //     'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                        //     'error',
                        //   )
                        }
                    }
                });
        })
    });
});

function hapusDiagnosa(diagnosapasien_id){
    var jsonVariable = {};
    jsonVariable["diagnosapasien_id"] = diagnosapasien_id;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_deldiag').val();
                    console.log("masuk fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("masuk delete");
            $.ajax({
  
                  url: ci_baseurl + "rekam_medis/pasienri/do_delete_diagnosa",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                              $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_deldiag]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_deltind]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_delres]').val(data.csrfHash);
                              $('input[name='+data.csrfTokenName+'_delda]').val(data.csrfHash);
                          if(ret === true) {
                              $('.modal_notif').removeClass('red').addClass('green');
                              $('#modal_card_message').html(data.messages);
                              $('.notmodal_notifif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                              $(".modal_notif").hide().removeClass('error');
                              });
                              table_diagnosa_ri.columns.adjust().draw();
                              swal(
                                'Berhasil!',
                                'Data Berhasil Dihapus.',
                                'success',
                              )
                          } else {
                              $('.modal_notif').removeClass('green').addClass('red');
                              $('#modal_card_message').html(data.messages);
                              $('.modal_notif').show();
                              $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                                  $(".modal_notif").hide();
                              });
                              console.log("gagal");
                              swal(
                                'Gagal!',
                                'Data Gagal Dihapus.',
                                'error',
                              )
                          }
                      }
                  });
            
          });
        }
function dialogDiagnosa(){
    // $('#modal_list_obat').openModal('toggle');
    if(table_daftar_diagnosa_ri){
        table_daftar_diagnosa_ri.destroy();
    }
    table_daftar_diagnosa_ri = $('#table_diagnosa_ri').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
       "searching": true,
  
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienri/ajax_list_diagnosa",
            "type": "GET",
            "data" : function(d){
              d.pendaftaran_id = $("#pendaftaran_id").val();
          }
  
        },
        "columnDefs": [{
              "targets": [ -1 ],
              "orderable": false,
          },],
      });
      table_daftar_diagnosa_ri.columns.adjust().draw();
}

function pilihDiagnosa(oid){
    
      if (oid != ""){
    
        $.ajax({
          url: ci_baseurl + "rekam_medis/pasienri/ajax_get_diagnosa_by_id",
          type: 'get',
          dataType: 'json',
          data: { diagnosa_id:oid },
          success: function(data) {
            var ret = data.success;
            console.log("sukses");
    
            if (ret == true){
              console.log("Masuk retun true");
              
              $('#diagnosa_id').val(data.data['diagnosa_id']);
              $('#kode_diagnosa').val(data.data['kode_diagnosa']);
              $('#nama_diagnosa').val(data.data['nama_diagnosa']);
              $('#kelompokdiagnosa_nama').val(data.data['kelompokdiagnosa_nama']);
              $('#lbl_kode_diagnosa').addClass('active');
              $('#lbl_nama-diagnosa').addClass('active');
              $('#lbl_kelompokdiagnosa_nama').addClass('active');
              console.log("masuk close");
              $("#modal_diagnosa").modal('hide');
            } else {
    
              // $("#harga_netto").val('');
              // $("#current_stok").val('');
              // $("#obat_id").val('');
              console.log("masuk else");
            }
          }
        });
      } else {
        // $('#modal_diagnosaa').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
            $('#modal_card_message').html("Diagnosa ID Kosong");
            $('.modal_notif').show();
            $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
                $(".modal_notif").hide();
            });
      }
    }

    table_rs_rujukan = $('#table_rs_rujukan').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        //"scrollX": true,
        "processing": true,
       "searching": true,
  
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "rekam_medis/pasienri/ajax_list_rs_rujukan",
            "type": "GET",
            
            
  
        },
        "columnDefs": [{
              "targets": [ -1 ],
              "orderable": false,
          },],
      });
      table_rs_rujukan.columns.adjust().draw();

      function pilihRSRujukan(oid){
        
          if (oid != ""){
        
            $.ajax({
              url: ci_baseurl + "rekam_medis/pasienri/ajax_get_rs_rujukan_by_id",
              type: 'get',
              dataType: 'json',
              data: { rs_rujukan_id:oid },
              success: function(data) {
                var ret = data.success;
                console.log("sukses");
        
                if (ret == true){
                  console.log("Masuk retun true");
                  
                  $('#rs_rujukan_id').val(data.data['rs_rujukan_id']);
                  $('#kode_rs').val(data.data['kode_rs']);
                  $('#nama_rs').val(data.data['nama_rs']);
                  $('#alamat').val(data.data['alamat']);
                  $('#lbl_kode_rs').addClass('active');
                  $('#lbl_nama_rs').addClass('active');
                  $('#lbl_alamat').addClass('active');
                  console.log("masuk close");
                  $("#modal_rs_rujukan").modal('hide');
                } else {
        
                  // $("#harga_netto").val('');
                  // $("#current_stok").val('');
                  // $("#obat_id").val('');
                  console.log("masuk else");
                }
              }
            });
          } else {
            // $('#modal_diagnosaa').closeModal('toggle');
            $('.modal_notif').removeClass('green').addClass('red');
                $('#modal_card_message').html("RS Rujukan ID Kosong");
                $('.modal_notif').show();
                $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
                    $(".modal_notif").hide();
                });
          }
        }

      function is_rujuk(){
        var carapulang = $("#carapulang").val();
        if(carapulang == 'Rujuk'){
            $('#rujukan_rs').removeAttr('style');
        }else{
            $('#rujukan_rs').attr('style','display:none;');
        }
    }
