function periksaPasienHCU() {
    var src = ci_baseurl + "hcu/periksa_pasienhcu";
    // console.log(pasien_id);
    $("#modal_periksa_pasienhcu iframe").attr({
        'src': src,
        'height': 380,
        'width': '100%',
        'allowfullscreen': ''
    });
}

function cariPasien(){
    table_pasienhcu = $('#table_list_pasienhcu').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false, 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "hcu/ajax_list_pasienhcu",
            
            "type": "GET",
            "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.poliklinik = $("#poliklinik").val();
                d.status = $("#status").val();
            } 
   
        }, 
        //Set column definition initialisation properties.
    });

    table_pasienhcu.columns.adjust().draw();
}