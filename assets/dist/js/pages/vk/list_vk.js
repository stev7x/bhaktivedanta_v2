var apa;
function cariPasien(){
    apa = $('#table_list_pasienvk').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "vk/list_vk/ajax_list_pasienvk",
          "type": "GET",
          "data": function(d){
                d.tgl_awal = $("#tgl_awal").val();
                d.tgl_akhir = $("#tgl_akhir").val();
                d.nama_pasien = $("#nama_pasien").val();
                d.no_pendaftaran = $("#no_pendaftaran").val();
                d.no_rekam_medis = $("#no_rekam_medis").val();
                d.no_bpjs = $("#no_bpjs").val();
                d.pasien_alamat = $("#pasien_alamat").val();
            }
      },
      //Set column definition initialisation properties.
    });
    apa.columns.adjust().draw();
}