var table_barang_stok;
var table_daftar_barang;
$(document).ready(function(){
    table_barang_stok = $('#table_barang_stok_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true,
      "searching":false, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "farmasi/barang_stok/ajax_list_barang_stok",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_barang_stok.columns.adjust().draw();
    $('.tooltipped').tooltip({delay: 50});
});

$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});
$('button#save').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "farmasi/barang_stok/do_create_stok",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateBarangStok').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_add").modal('hide');
                            // $('.modal_notif').removeClass('red').addClass('green');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();
                            // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            //     $(".modal_notif").hide();
                            // });
                            // $('#fmCreateAutoStopObat').find("input[type=text]").val("");
                            // $('#fmCreateAutoStopObat').find("select").prop('selectedIndex',0);
                            table_barang_stok.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

$('button#update').click(function(){
        swal({
          text: 'Apakah Data Yang Anda Masukan Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "farmasi/barang_stok/do_update_stok",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmUpdateBarangStok').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $("#modal_edit").modal('hide');
                            // $('.modal_notif').removeClass('red').addClass('green');
                            // $('#modal_card_message').html(data.messages);
                            // $('.modal_notif').show();
                            // $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
                            //     $(".modal_notif").hide();
                            // });
                            // $('#fmCreateAutoStopObat').find("input[type=text]").val("");
                            // $('#fmCreateAutoStopObat').find("select").prop('selectedIndex',0);
                            table_barang_stok.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                             swal(
                                'Gagal!',
                                'Gagal Disimpan. Data Masih Ada Yang Kosong ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

//teu kapake
function dialogBarang(){
    if(table_daftar_barang){
        table_daftar_barang.destroy();
    }
    table_daftar_barang = $('#table_daftar_barang_list').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching" :true, 
        "iDisplayLength" : 5,
        "aLengthMenu": [[5, 10, 20, 50, -1], [5, 10, 20, 50, "All"]],//Feature control DataTables' server-side processing mode.
       // "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "farmasi/barang_stok/ajax_list_daftar_barang",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1,0 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_daftar_barang.columns.adjust().draw();
}
//sampe dieu

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
    
function pilihBarang(oid){
	if (oid != ""){
		$.ajax({
			url: ci_baseurl + "farmasi/barang_stok/ajax_get_daftar_barang_by_id",
			type: 'get',
			dataType: 'json',
			data: { kode_barang:oid },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
                    $('#kode_barang').val(data.data['kode_barang']);
					$('#nama_barang').val(data.data['nama_barang']);
					$('#id_jenis_barang').val(data.data['id_jenis_barang']);
					$('#kode_sediaan').val(data.data['kode_sediaan']);
                    $("#modal_list_barang").modal('hide'); 
				} else {
					
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}

function Perhitungan(){
    var jumlah_barang_per_pack = $("#jumlah_barang_per_pack").val();
    var jumlah_pack = $("#jumlah_pack").val();
    var harga_barang_per_item = $("#harga_barang_per_item").val();
    var harga_barang_per_pack = $("#harga_barang_per_pack").val();

    var jumlah_keseluruhan = parseInt(jumlah_barang_per_pack) * parseInt(jumlah_pack);
    var harga_keseluruhan = parseInt(harga_barang_per_pack) * parseInt(jumlah_pack);
    
    $("#jumlah_keseluruhan").val(jumlah_keseluruhan);
    $("#harga_keseluruhan").val(harga_keseluruhan);
}


function cariBarangStok(){
    table_barang_stok = $('#table_barang_stok_list').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "farmasi/barang_stok/ajax_list_barang_stok",
          "type": "GET",
          "data": function(d){
                d.kode_barang     = $("#kode_barang").val();
                d.nama_barang     = $("#nama_barang").val();
                d.jenis_barang    = $("#jenis_barang").val();
                d.kondisi_barang  = $("#kondisi_barang").val();
                d.expired         = $("#expired").val();
               
            }
      },
      //Set column definition initialisation properties.
    });
    table_barang_stok.columns.adjust().draw();
}


$(document).ready(function(){
    $('#change_option').on('change', function(e){
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});

function editBarang(id){
    if(id){    
        $.ajax({
        url: ci_baseurl + "farmasi/barang_stok/ajax_get_barang_id",
        type: 'get',
        dataType: 'json',
        data: {id:id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_kode_barang').val(data.data.kode_barang);
                    $('#upd_nama_barang').val(data.data.nama_barang);
                    $('#upd_stok_masuk').val(data.data.stok_masuk);
                    $('#upd_stok_keluar').val(data.data.stok_keluar);
                    $('#upd_stok_akhir').val(data.data.stok_akhir);
                    $('#upd_id_jenis_barang').val(data.data.id_jenis_barang);
                    $('#upd_id_sediaan').val(data.data.id_sediaan);
                    $('#upd_harga_satuan').val(data.data.harga_satuan);
                    $('#upd_harga_pack').val(data.data.harga_pack);
                    $('#upd_keterangan').val(data.data.keterangan);
                    $('#upd_type_call').val(data.data.type_call);
                    $('#upd_jenis_pasien').val(data.data.jenis_pasien);
                    $('#upd_harga_pokok').val(data.data.harga_pokok);

                    var event = new Date(data.data.expired);
                    var options = {  year: 'numeric', month: 'long', day: 'numeric' };
                    $('#upd_expired').val(event.toLocaleDateString('id-ID', options));
                    
                    // $('#modal_update_agama').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}

function hapusBarang(kode_barang) {
    var jsonVariable = {};
    jsonVariable["kode_barang"] = kode_barang;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_del').val();
    console.log("masuk fungsi");
    swal({
        text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'tidak',
        confirmButtonText: 'Ya'
    }).then(function () {
        $.ajax({
            url: ci_baseurl + "farmasi/barang_stok/do_delete_barang_stok",
            type: 'post',
            dataType: 'json',
            data: jsonVariable,
            success: function (data) {
                var ret = data.success;
                $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                $('input[name=' + data.csrfTokenName + '_del]').val(data.csrfHash);

                if (ret === true) {
                    $('.notif').removeClass('red').addClass('green');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function () {
                        $(".notif").hide();
                    });
                    table_barang_stok.columns.adjust().draw();
                } else {
                    $('.notif').removeClass('green').addClass('red');
                    $('#card_message').html(data.messages);
                    $('.notif').show();
                    $(".notif").fadeTo(2000, 500).slideUp(500, function () {
                        $(".notif").hide();
                    });
                    console.log("gagal");
                }
            }
        });
        swal(
            'Berhasil!',
            'Data Berhasil Dihapus.',
            'success',
        )
    });
}