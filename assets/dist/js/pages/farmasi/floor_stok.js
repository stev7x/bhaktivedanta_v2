var table_floor_stok;
var table_daftar_barang;
$(document).ready(function(){
    table_floor_stok = $('#table_floor_stok_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true,
      "searching":false, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "farmasi/floor_stok/ajax_list_floor_stok",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_floor_stok.columns.adjust().draw();
    $('.tooltipped').tooltip({delay: 50});
});

function editAgama(agama_id){
    if(agama_id){    
        $.ajax({
        url: ci_baseurl + "masterdata/agama/ajax_get_agama_by_id",
        type: 'get',
        dataType: 'json',
        data: {agama_id:agama_id},
            success: function(data) {
                var ret = data.success;
                if(ret === true) {
                    console.log(data.data);
                    $('#upd_id_agama').val(data.data.agama_id);
                    $('#upd_nama_agama').val(data.data.agama_nama);
                    $('#label_upd__nama_agama').addClass('active');
                    // $('#modal_update_agama').openModal('toggle');
                } else {
                    $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
                    $('#notification_messages').html(data.messages);
                    $('#notification_type').show();
                }
            }
        });
    }
    else
    {
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html(data.messages);
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(2000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
    }
}


function dialogBarang(){
    if(table_daftar_barang){
        table_daftar_barang.destroy();
    }
    table_daftar_barang = $('#table_daftar_barang_list').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching" :true, 
        "iDisplayLength" : 5,
        "aLengthMenu": [[5, 10, 20, 50, -1], [5, 10, 20, 50, "All"]],//Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "farmasi/floor_stok/ajax_list_daftar_barang",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1,0 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_daftar_barang.columns.adjust().draw();
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
    
function pilihBarang(oid){
	if (oid != ""){
		$.ajax({
			url: ci_baseurl + "farmasi/floor_stok/ajax_get_daftar_barang_by_id",
			type: 'get',
			dataType: 'json',
			data: { kode_barang:oid },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
                    $('#kode_barang').val(data.data['kode_barang']);
					$('#nama_barang').val(data.data['nama_barang']);
					$('#id_jenis_barang').val(data.data['id_jenis_barang']);
					$('#kode_sediaan').val(data.data['kode_sediaan']);
                    $("#modal_list_barang").modal('hide'); 
				} else {
					
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}

function Perhitungan(){
    var jumlah_barang_per_pack = $("#jumlah_barang_per_pack").val();
    var jumlah_pack = $("#jumlah_pack").val();
    var harga_barang_per_item = $("#harga_barang_per_item").val();
    var harga_barang_per_pack = $("#harga_barang_per_pack").val();

    var jumlah_keseluruhan = parseInt(jumlah_barang_per_pack) * parseInt(jumlah_pack);
    var harga_keseluruhan = parseInt(harga_barang_per_pack) * parseInt(jumlah_pack);
    
    $("#jumlah_keseluruhan").val(jumlah_keseluruhan);
    $("#harga_keseluruhan").val(harga_keseluruhan);
}


function cariBarangStok(){
    table_floor_stok = $('#table_floor_stok_list').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "sScrollX": "100%",  

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "farmasi/floor_stok/ajax_list_floor_stok",
          "type": "GET",
          "data": function(d){
                d.tgl_awal        = $("#tgl_awal").val();
                d.tgl_akhir       = $("#tgl_akhir").val();
                d.kode_barang     = $("#kode_barang").val();
                d.nama_barang     = $("#nama_barang").val();
                d.jenis_barang    = $("#jenis_barang").val();
                d.kondisi_barang  = $("#kondisi_barang").val();
                d.expired         = $("#expired").val();
               
            }
      },
      //Set column definition initialisation properties.
    });
    table_floor_stok.columns.adjust().draw();
}


$(document).ready(function(){
    $('#change_option').on('change', function(e){
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});