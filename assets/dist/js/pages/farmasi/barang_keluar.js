var table_barang_keluar;
var table_daftar_barang;
var table_barang_pengajuan;
$(document).ready(function(){
    table_barang_keluar = $('#table_barang_keluar_list').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true,
      "searching": false, //Feature control DataTables' server-side processing mode.
//      "scrollX": true,

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "farmasi/barang_keluar/ajax_list_barang_keluar",
          "type": "GET"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1,0 ], //last column
        "orderable": false //set not orderable
      }
      ]

    });
    table_barang_keluar.columns.adjust().draw();

    table_barang_pengajuan = $('#table_barang_pengajuan_list').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching": false, //Feature control DataTables' server-side processing mode.
        //      "scrollX": true,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "farmasi/barang_keluar/ajax_list_barang_pengajuan",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [-1, 0], //last column
                "orderable": false //set not orderable
            }
        ]

    });
    table_barang_pengajuan.columns.adjust().draw();


    
    
    $('.tooltipped').tooltip({delay: 50});
    
    $('button#saveBarangKeluar').click(function(){
        swal({
          text: 'Apakah Data Yang Anda keluaran Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "farmasi/barang_keluar/do_create_barang_keluar",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateBarangKeluar').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $('#modal_add_keluar_barang').modal('hide');
                            $('#fmCreateBarangKeluar').find("input[type=text]").val("");
                            $('#fmCreateBarangKeluar').find("input[type=number]").val("");
                            $('#fmCreateBarangKeluar').find("select").prop('selectedIndex',0);
                            $("#dari_gudang").select2("val", "");
                            $("#di_tujukan").select2("val", "");
                            table_barang_keluar.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message').html(data.messages);
                            $('#modal_notif').show();
                            $("#modal_notif").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                            swal(
                                'Gagal!',
                                'Gagal Disimpan ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

    $('button#savePengajuanBarang').click(function(){
        swal({
          text: 'Apakah Data Yang Anda keluaran Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "farmasi/barang_keluar/do_create_pengajuan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateBarangKeluar').serialize(),
                      
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                       
                        if(ret === true) {
                            $('#modal_approval').modal('hide');
                            $('#fmCreateBarangKeluar').find("input[type=text]").val("");
                            $('#fmCreateBarangKeluar').find("input[type=number]").val("");
                            $('#fmCreateBarangKeluar').find("select").prop('selectedIndex',0);
                            $("#dari_gudang").select2("val", "");
                            $("#di_tujukan").select2("val", "");
                            table_barang_keluar.columns.adjust().draw();
                            table_barang_pengajuan.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                            swal(
                                'Gagal!',
                                'Gagal Disimpan ! ',
                                'error'
                              )
                        }
                    }
                });
          
        })
    });

    $('button#tolakPengajuanBarang').click(function(){
        swal({
          text: 'Apakah Data Yang Anda keluaran Sudah Benar ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText:'Tidak',
          confirmButtonText: 'Ya!'
        }).then(function(){
            $.ajax({
                    url: ci_baseurl + "farmasi/barang_keluar/do_tolak_pengajuan",
                    type: 'POST',
                    dataType: 'JSON',
                    data: $('form#fmCreateBarangKeluar').serialize(),
                    success: function(data) {
                        var ret = data.success;
                        $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                        $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                        if(ret === true) {
                            $('#modal_approval').modal('hide');
                            $('#fmCreateBarangKeluar').find("input[type=text]").val("");
                            $('#fmCreateBarangKeluar').find("input[type=number]").val("");
                            $('#fmCreateBarangKeluar').find("select").prop('selectedIndex',0);
                            $("#dari_gudang").select2("val", "");
                            $("#di_tujukan").select2("val", "");
                            table_barang_keluar.columns.adjust().draw();
                            table_barang_pengajuan.columns.adjust().draw();
                            swal(
                                'Berhasil!',
                                'Data Berhasil Disimpan.',
                                'success'
                              )
                        } else {
                            $('#modal_notif1').removeClass('alert-success').addClass('alert-danger');
                            $('#card_message1').html(data.messages);
                            $('#modal_notif1').show();
                            $("#modal_notif1").fadeTo(4000, 500).slideUp(1000, function(){
                            $("#modal_notif1").hide();
                            });
                            $("html, modal-content").animate({ scrollTop: 100 }, "fast");
                            swal(
                                'Gagal!',
                                'Gagal Disimpan ! ',
                                'error'
                              )
                        }
                    }
                });
        })
    });


    

});



function hapusBarangKeluar(id_barang_keluar){
    var jsonVariable = {};
    jsonVariable["id_barang_keluar"] = id_barang_keluar;
    jsonVariable[csrf_name] = $('#'+csrf_name+'_del').val();
    console.log("keluar fungsi");
    swal({
            text: "Apakah Anda Yakin Ingin Menghapus Data Ini ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
          }).then(function(){
                          console.log("keluar delete");
            $.ajax({
  
                  url: ci_baseurl + "farmasi/barang_keluar/do_delete_barang_keluar",
                  type: 'post',
                  dataType: 'json',
                  data: jsonVariable,
                      success: function(data) {
                          var ret = data.success;
                          $('input[name='+data.csrfTokenName+']').val(data.csrfHash);
                          $('input[name='+data.csrfTokenName+'_del]').val(data.csrfHash);
                           
                          if(ret === true) {
                            $('.notif').removeClass('red').addClass('green');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                            table_barang_keluar.columns.adjust().draw();
                          } else {
                            $('.notif').removeClass('green').addClass('red');
                            $('#card_message').html(data.messages);
                            $('.notif').show();
                            $(".notif").fadeTo(2000, 500).slideUp(500, function(){
                                $(".notif").hide();
                            });
                              console.log("gagal");
                          }
                      }
                  });
            swal(
              'Berhasil!',
              'Data Berhasil Dihapus.',
              'success',
            )
          });
        }

function dialogBarang(){
    if(table_daftar_barang){
        table_daftar_barang.destroy();
    }
    table_daftar_barang = $('#table_daftar_barang_list').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true,
        "searching" :true, 
        "iDisplayLength" : 5,
        "aLengthMenu": [[5, 10, 20, 50, -1], [5, 10, 20, 50, "All"]],//Feature control DataTables' server-side processing mode.
//        "paging": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": ci_baseurl + "farmasi/barang_keluar/ajax_list_daftar_barang",
            "type": "GET"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1,0 ], //last column
            "orderable": false //set not orderable
        }
        ]
    });
    table_daftar_barang.columns.adjust().draw();
}

function formatAsRupiah(angka) {
    angka.value = 'Rp. ' + angka.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
    
function pilihBarang(oid){
	if (oid != ""){
		$.ajax({
			url: ci_baseurl + "farmasi/barang_keluar/ajax_get_daftar_barang_by_id",
			type: 'get',
			dataType: 'json',
			data: { kode_barang:oid },
			success: function(data) {
				var ret = data.success;
				if (ret == true){
                    $('#kode_barang').val(data.data['kode_barang']);
					$('#nama_barang').val(data.data['nama_barang']);
					$('#jenis_barang').val(data.data['id_jenis_barang']);
					$('#kode_sediaan').val(data.data['id_sediaan']);
					$('#expired').val(data.data['expired']);
                    $('#harga_satuan').val(data.data['harga_satuan']);
                    $('#harga_pack').val(data.data['harga_pack']);
                    $("#modal_list_barang").modal('hide'); 
				} else {
					
				}
			}
		});
	} else {
		// $('#modal_list_obat').closeModal('toggle');
		$('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function(){
            $(".modal_notif").hide();
        });
	}
}

function pilih(){
    var jenis_transaksi = $("#jenis_transaksi").val();
    if (jenis_transaksi == 1){
        $(".per_pack").show(500);        
        $(".per_pieces").hide(500); 
        $('.per_pieces').find("input[type=number]").val("");       
        $('.per_pieces').find("input[type=text]").val(""); 
        $('.hitung').find("input[type=text]").val(""); 
        $('.hitung').find("input[type=number]").val(""); 
              
    }else if(jenis_transaksi == 2) {
        $(".per_pieces").show(500);        
        $(".per_pack").hide(500);   
        $('.per_pack').find("input[type=number]").val("");       
        $('.per_pack').find("input[type=text]").val("");         
        $('.hitung').find("input[type=text]").val(""); 
        $('.hitung').find("input[type=number]").val("");         
        
    }else{
        $(".per_pieces").hide(500); 
        $(".per_pack").hide(500);   
    }
}

function perhitunganPack(){
    var jumlah_barang_per_pack = $("#jumlah_barang_per_pack").val();
    var jumlah_pack = $("#jumlah_pack").val();   
    var jumlah_barang_keseluruhan = parseInt(jumlah_barang_per_pack * jumlah_pack);
    $("#jumlah_barang_keseluruhan").val(jumlah_barang_keseluruhan);
}

function perhitunganPieces(){
    var jumlah_barang_per_pieces = $("#jumlah_barang_per_pieces").val();
    var jumlah_barang_keseluruhan = jumlah_barang_per_pieces
    $("#jumlah_barang_keseluruhan").val(jumlah_barang_keseluruhan);
}

function cariBarangKeluar(){
    table_barang_keluar = $('#table_barang_keluar_list').DataTable({
        "destroy": true,
        "ordering": false,
        "bFilter": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": ci_baseurl + "farmasi/barang_keluar/ajax_list_barang_keluar",
          "type": "GET",
          "data": function(d){
                d.tgl_awal        = $("#tgl_awal").val();
                d.tgl_akhir       = $("#tgl_akhir").val();
                d.kode_barang     = $("#kode_barang").val();
                d.nama_barang     = $("#nama_barang").val();
                d.jenis_barang    = $("#jenis_barang").val();
                d.dari_gudang     = $("#dari_gudang").val();
                d.di_tujukan      = $("#di_tujukan").val();
               
               
            }
      },
      //Set column definition initialisation properties.
    });
    table_barang_keluar.columns.adjust().draw();
}


$(document).ready(function(){
    $('#change_option').on('change', function(e){
        var valueSelected = this.value;

        console.log("data valuenya : " + valueSelected);
        // alert(valueSelected);
        $('.pilih').attr('id',valueSelected);
        $('.pilih').attr('name',valueSelected);
    });
});


function tolakPengajuan(id_approval) {
    var jsonVariable = {};
    jsonVariable["id_approval"] = id_approval;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_delda').val();
    if (id_approval) {
        swal({
            text: "Apakah Anda Yakin Ingin Menolak Pengajuan Ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
        }).then(function () {

            $.ajax({
                url: ci_baseurl + "farmasi/barang_keluar/do_tolak_pengajuan",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);
                    if (ret === true) {
                        swal(
                            'Berhasil!',
                            'Pengajuan Berhasil Ditolak.',
                            'success',
                        )
                        table_barang_pengajuan.columns.adjust().draw();
                        $("htmml,body").animate({ scrollTop: 50 }, "fast");
                    } else {
                        $('.notif').removeClass('green').addClass('red');
                        $('#card_message').html(data.messages);
                        $('.notif').show();
                        $(".notif").fadeTo(2000, 500).slideUp(500, function () {
                            $(".notif").hide();
                        });
                        swal(
                            'Gagal!',
                            'Pemeriksaan Gagal Dibatalkan.',
                            'error',
                        )
                    }
                }
            });


        });

    }
    else {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }

}

function setujuPengajuan(id_approval) {
    var jsonVariable = {};
    jsonVariable["id_approval"] = id_approval;
    jsonVariable[csrf_name] = $('#' + csrf_name + '_delda').val();
    if (id_approval) {
        swal({
            text: "Apakah Anda Yakin Ingin Menyetujui Pengajuan Ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'tidak',
            confirmButtonText: 'Ya'
        }).then(function () {

            $.ajax({
                url: ci_baseurl + "farmasi/barang_keluar/do_setuju_pengajuan",
                type: 'post',
                dataType: 'json',
                data: jsonVariable,
                success: function (data) {
                    var ret = data.success;
                    $('input[name=' + data.csrfTokenName + ']').val(data.csrfHash);
                    $('input[name=' + data.csrfTokenName + '_delda]').val(data.csrfHash);
                    if (ret === true) {
                        swal(
                            'Berhasil!',
                            'Pengajuan Berhasil Ditolak.',
                            'success',
                        )
                        table_barang_pengajuan.columns.adjust().draw();
                        $("htmml,body").animate({ scrollTop: 50 }, "fast");
                    } else {
                        $('.notif').removeClass('green').addClass('red');
                        $('#card_message').html(data.messages);
                        $('.notif').show();
                        $(".notif").fadeTo(2000, 500).slideUp(500, function () {
                            $(".notif").hide();
                        });
                        swal(
                            'Gagal!',
                            'Pemeriksaan Gagal Dibatalkan.',
                            'error',
                        )
                    }
                }
            });


        });

    }
    else {
        $('#notification_type').removeClass('alert alert-dismissable').addClass('alert alert-danger alert-dismissable');
        $('#notification_messages').html('Missing Access code ID');
        $('#notification_type').show();
    }

}

function pilihPengajuan(id_approval) {
    if (id_approval != "") {
        // var src = ci_baseurl + "farmasi/barang_keluar/ajax_get_pengajuan_by_id";
        // console.log(src);
        // $("#modal_approval").modal('show');
        $.ajax({
            url: ci_baseurl + "farmasi/barang_keluar/ajax_get_pengajuan_by_id",
            type: 'get',
            dataType: 'json',
            data: { id_approval: id_approval },
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    $("#modal_approval").modal('show');
                    // $('#kode_barang').text("masuk");
                    
                    $('#kode_barang_lbl').text(data.data['kode_barang']);
                    $('#nama_barang_lbl').text(data.data['nama_barang']);
                    $('#jumlah_barang_lbl').text(data.data['jumlah_barang']);
                    $('#alasan_pengajuan_lbl').text(data.data['alasan_pengajuan']);                    
                    $('#id_approval_input').val(data.data['id_approval']);
                    $('#id_approval_input2').val(data.data['id_approval']);
                    $('#kode_barang_input').val(data.data['kode_barang']);
                    $('#nama_barang_input').val(data.data['nama_barang']);
                    $('#jenis_barang_input').val(data.data['id_jenis_barang']);
                    $('#satuan_barang_input').val(data.data['id_sediaan']);
                    $('#harga_satuan_input').val(data.data['harga_satuan']);
                    $('#expired_input').val(data.data['expired']);
                    // $('#jumlah_barang_input').text(data.data['jumlah_barang']);
                    // $("#modal_approval").modal('toggle');
                } else {

                }
            }
        });
    } else {
        // $('#modal_list_obat').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}

function pilihDetailPengajuan(id_approval) {
    if (id_approval != "") {
        // var src = ci_baseurl + "farmasi/barang_keluar/ajax_get_pengajuan_by_id";
        // console.log(src);
        // $("#modal_approval").modal('show');
        $.ajax({
            url: ci_baseurl + "farmasi/barang_keluar/ajax_get_pengajuan_by_id",
            type: 'get',
            dataType: 'json',
            data: { id_approval: id_approval },
            success: function (data) {
                var ret = data.success;
                if (ret === true) {
                    $("#modal_detail_approval").modal('show');
                    // $('#kode_barang').text("masuk");
                    
                    $('#det_kode_barang_lbl').text(data.data['kode_barang']);
                    $('#det_nama_barang_lbl').text(data.data['nama_barang']);
                    $('#det_jumlah_barang_lbl').text(data.data['jumlah_barang']);
                    $('#det_alasan_pengajuan_lbl').text(data.data['alasan_pengajuan']);
                    $('#det_alasan_respon_lbl').text(data.data['alasan_respon']);
                    $('#det_status_lbl').text(data.data['status_approval']);
                    $('#det_kode_barang_lbl2').text(data.data['kode_barang']);
                    $('#det_nama_barang_lbl2').text(data.data['nama_barang']);
                    $('#det_jumlah_barang_lbl2').text(data.data['jumlah_barang_respon']);
                    $('#id_approval_input').val(data.data['id_approval']);
                    $('#id_approval_input2').val(data.data['id_approval']);
                    $('#kode_barang_input').val(data.data['kode_barang']);
                    $('#nama_barang_input').val(data.data['nama_barang']);
                    // $('#jumlah_barang_input').text(data.data['jumlah_barang']);
                    // $("#modal_approval").modal('toggle');
                } else {

                }
            }
        });
    } else {
        // $('#modal_list_obat').closeModal('toggle');
        $('.modal_notif').removeClass('green').addClass('red');
        $('#modal_card_message').html("Obat ID Kosong");
        $('.modal_notif').show();
        $(".modal_notif").fadeTo(5000, 500).slideUp(500, function () {
            $(".modal_notif").hide();
        });
    }
}


function pilihAksi() {
    var pilih = $('#pilih_aksi').val();
    if(pilih == 1 ){
        $('#terima').toggle(300);
        $('#tolak').hide();
        // $('#id_approval_input').removeAttr();
    }else if(pilih == 2){
        $('#tolak').toggle(300);
        $('#terima').hide();
        
    }else{
        $('#tolak').hide();
        $('#terima').hide();
        
    }
}